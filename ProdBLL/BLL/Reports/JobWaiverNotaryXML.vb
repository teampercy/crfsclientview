﻿Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Namespace Liens.Reports
    Public Class JobWaiverNotaryXML
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Dim mywaiverinfo As BLL.ClientView.Lien.JobWaiverInfo
        Dim myimage As New HDS.WINLIB.Controls.ImageViewer
        Public CurrentUser As CurrentUser
        Dim dummyTable As HDS.DAL.COMMON.TableView
        Dim flexfieldtext As String
        Public Sub New(ByVal aitemid As Integer)
            MyBase.New()
            CurrentUser = Globals.GetCurrentUser
            mywaiverinfo = New BLL.ClientView.Lien.JobWaiverInfo(aitemid)
            Me.OutputFolder = CurrentUser.OutputFolder & "\"
            Me.SettingsFolder = CurrentUser.SettingsFolder & "\"
            Me.UserName = CurrentUser.UserName

        End Sub
        Public Function ExecuteC1Report(ByVal MYDS As DataSet) As String
            dummyTable = New HDS.DAL.COMMON.TableView(MYDS.Tables(0))
            Me.ReportDefPath = Me.SettingsFolder & "\Reports\Waivers.FLXR"
            'The DataView property used only for to retrived Detail Section from Flex report. We not used any value from Table(0).
            Me.DataView = dummyTable
            If mywaiverinfo.JobWaiverLog.Notary = True Then
                If mywaiverinfo.JobWaiverLog.NotaryType = 2 Then
                    Me.ReportName = "NotaryCA"
                Else
                    Me.ReportName = "Notary"
                End If

                mywaiverinfo.JobWaiverLog.DatePrinted = Date.Now
                LienView.Provider.DAL.Update(mywaiverinfo.JobWaiverLog)
                With Me
                    .FilePrefix = GetFileName("Notary-" & mywaiverinfo.StateForm.FormCode)
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            End If
        End Function

        Public Shared Function GetFileName(ByVal areportname As String) As String
            Dim myuser As CurrentUser = Globals.GetCurrentUser
            Dim myprefix As String = myuser.ClientCode
            myprefix += "-" & myuser.LoginCode
            myprefix += "-" & myuser.Id
            myprefix += "-" & areportname
            Return Globals.TrimSpace(myprefix)
        End Function

        Public Function ExecuteC1Report(ByVal aitemid As String) As String
            Dim mysproc As New SPROCS.uspbo_ClientView_GetWaiverInfo
            mysproc.JobWaiverLogId = aitemid
            Return ExecuteC1Report(CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetDataSet(mysproc))
        End Function

        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender

CheckHeader:
            Try
                If HasField("JOBWAIVERLOGID", e) = True Then
                    Dim ZWAIVERLOGID As Field = C1R.Fields("JOBWAIVERLOGID")
                    If ZWAIVERLOGID.Section = e.Section Then
                        ZWAIVERLOGID.Calculated = True
                        'ZWAIVERLOGID.RTF = True
                        ZWAIVERLOGID.Visible = True
                        ZWAIVERLOGID.Value = GetJobWaiverLogId(ZWAIVERLOGID.Value)
                    End If
                End If

                If HasField("JOBWAIVERLOGIDBARCODE", e) = True Then
                    Dim ZWAIVERBARCODE As Field = C1R.Fields("JOBWAIVERLOGIDBARCODE")
                    If ZWAIVERBARCODE.Section = e.Section Then
                        ZWAIVERBARCODE.Calculated = True
                        'ZWAIVERBARCODE.RTF = True
                        ZWAIVERBARCODE.Visible = True
                        ZWAIVERBARCODE.Value = GetJobWaiverLogId(ZWAIVERBARCODE.Value)
                    End If
                End If

                If HasField("NOTARYTEXT", e) = True Then
                    Dim ZNOTARY As Field = C1R.Fields("NOTARYTEXT")
                    If ZNOTARY.Section = e.Section Then
                        If mywaiverinfo.JobWaiverLog.Notary = True Then
                            ZNOTARY.Visible = True
                            ZNOTARY.Calculated = True
                            ZNOTARY.CanGrow = True
                            ZNOTARY.CanShrink = True
                            ZNOTARY.Value = GetNotary(ZNOTARY.Value)
                            'ZNOTARY.RTF = True
                        Else
                            ZNOTARY.Visible = False
                            ZNOTARY.Value = ""
                        End If
                    End If
                End If

                If HasField("FIELD2", e) = True Then
                    Dim ZCATEXT As Field = C1R.Fields("FIELD2")
                    If ZCATEXT.Section = e.Section Then
                        ZCATEXT.BorderColor = Drawing.Color.Black
                        ZCATEXT.BorderStyle = BorderStyleEnum.Solid
                        ZCATEXT.LineWidth = 15
                        ZCATEXT.MarginLeft = 50
                        ZCATEXT.MarginRight = 50
                    End If
                End If


            Catch EX As Exception
                Throw New ApplicationException(EX.Message)

            End Try

        End Sub

        Private Function GetJobWaiverLogId(JobWaiverLogId) As String
            flexfieldtext = JobWaiverLogId
            ReplaceJobWaiverLogIdTokens()
            Return flexfieldtext
        End Function

        Public Function GetNotary(notarytext) As String
            flexfieldtext = notarytext
            ReplaceNotaryTextTokens()
            Return flexfieldtext
        End Function

        Public Function ReplaceJobWaiverLogIdTokens() As Boolean
            Replace("JobWaiverLogId", mywaiverinfo.JobWaiverLog.JobWaiverLogId)
        End Function

        Public Function ReplaceNotaryTextTokens() As Boolean
            Replace("[SIGNATORY]", mywaiverinfo.ClientSigner.Signer)
        End Function

        Private Sub Replace(ByVal atag As String, ByVal avalue As String)
            Try
                If mywaiverinfo.StateForm.StateCode = "GA" Then
                    flexfieldtext = Strings.Replace(flexfieldtext, atag, UCase(avalue))
                Else
                    flexfieldtext = Strings.Replace(flexfieldtext, atag, avalue)
                End If
            Catch
            End Try

        End Sub

        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try
        End Function

    End Class
End Namespace
