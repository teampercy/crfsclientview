Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports CRF.CLIENTVIEW.BLL.Tasks
Imports CRF.CLIENTVIEW.BLL
Namespace ScheduledReports
    '''<summary> 
    ''' Mail Job - Sending the mail message
    ''' or any other job you wish to be executed
    ''' Implements the ISchedulerJob interface 
    '''</summary> 
    Public Class TexasJobTracker
        Inherits Reports.SchedulerJob
        Dim myclrpt As Reports.ClientReport

        Dim fromdate As Date = Today
        Dim thrudate As Date = Today
        Dim mystatus As Integer = 2
        Public Overrides Function DoTask(Optional ByVal sql As String = "", Optional ByVal status As Integer = 3) As Boolean
            mystatus = status

            Dim myreq As New Tasks.request
            fromdate = DateAdd(DateInterval.Month, -1, Today)
            thrudate = DateAdd(DateInterval.Day, -0, Today)

            myreq.arguments("reportdef") = Settings.settingsfolder & "LIENREPORTS.XML"
            myreq.arguments("reportname") = "TexasJobAlert"
            myreq.arguments("outputfolder") = Settings.outputfolder
            myreq.arguments("sql") = "uspbo_LiensDayEnd_GetJobListTexasByEmail '" & fromdate & "','" & thrudate & "'"
            If sql.Length > 1 Then
                myreq.arguments("sql") = sql
                Dim s As String = Strings.Replace(sql, "uspbo_LiensDayEnd_GetJobListTexasByEmail ", "")
                s = Strings.Replace(s, "'", "")
                s = Strings.Replace(s, " ", "")

                Dim ss As String() = Strings.Split(s, ",")
                fromdate = ss(0)
                thrudate = ss(1)
            End If

            Render(myreq)

            Return True

        End Function
        Public Overrides Function DoRequest(ByVal xmlstring As String) As String

            Dim myreq As New request
            myreq.fromxml(xmlstring)
            Render(myreq)
            Return myreq.toxml

        End Function
        Public Function Render(ByRef myreq As request) As Boolean
            Dim myreportdef As String = myreq.arguments("reportdef")
            Dim myoutputfolder As String = System.Windows.Forms.Application.StartupPath & "\output\"
            If System.IO.Directory.Exists(myoutputfolder) = False Then
                System.IO.Directory.CreateDirectory(myoutputfolder)
            End If

            Try
                Dim mybatchid As String = ProviderBase.GetUniqueId

                Dim myds As DataSet = ProviderBase.DAL.GetDataSet(myreq.arguments("sql"))
                Dim DR As DataRow = Nothing
                Dim idx As Integer = 0
                For Each DR In myds.Tables(0).Rows

                    Dim myview As New DataView(myds.Tables(1))
                    myview.RowFilter = "ClientCode='" & DR("ClientCode") & "'"
                    Dim myclientid = myview.Item(0).Row.Item("ClientId")
                    Me.CreateHistory("LIEN-TX", mybatchid, myclientid, DR("ClientCode"))
                    Me.History.body = " Activity From " & Strings.FormatDateTime(fromdate, DateFormat.ShortDate)
                    idx += 1
                    Me.UpdateStatus(0, myds.Tables(0).Rows.Count, idx)

                    Dim myreportname As String = "TexasJobAlert"

                    Dim mypath As String = RenderC1Report(myview, myreportdef, myreportname, myoutputfolder, True, DR("ClientCode"), "", "")

                    Me.CreateAttachment("Texas Jobs", mypath)

                    Me.CreateRecipient(DR("emailrptsto"))

                    Me.CloseHistory(mystatus, "")
                Next

            Catch ex As Exception
                Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & "Texas Jobs")

                myreq.results.status = -1
                myreq.results.message = ex.Message

                Return False

            End Try

            Common.FileOps.DirectoryClean(myoutputfolder, DateAdd(DateInterval.Day, 1, Today))

            Return True

        End Function
    End Class
End Namespace

