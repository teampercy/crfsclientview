Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports System.Net.Mail
Namespace Reports
#Region "Core Functions"
    Public Class Provider
        Private Shared current As System.Web.HttpContext = System.Web.HttpContext.Current
        Private Shared _isInitialized As Boolean = False
        Private Shared schedulerThread As System.Threading.Thread = Nothing
        Private Shared scheduler As Reports.Scheduler
        Private Shared _DBO As DAL.Providers.IDAL
        Public Shared ReadOnly Property DAL() As DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Shared Function Execute() As Boolean
            If Not _isInitialized Then
                _isInitialized = True
                scheduler = New Reports.Scheduler()
                Dim schedulerThreadstart As New System.Threading.ThreadStart(AddressOf scheduler.Start)
                schedulerThread = New System.Threading.Thread(schedulerThreadstart)
                schedulerThread.Start()
            End If
            Return True
        End Function
        Public Shared Function StopIt() As Boolean
            If _isInitialized = True Then
                scheduler.StopIt()
            End If

        End Function
        Public Shared Function GetSettings() As TABLES.Report_Settings
            Dim myitem As New TABLES.Report_Settings
            DAL.Read(1, myitem)

            Return myitem

        End Function
        Public Shared Sub EmailToUser(ByVal email As String, ByVal subject As String, ByVal body As String)

            Dim MYSITE As TABLES.Report_Settings = GetSettings()

            Dim msg As New MailMessage
            msg.From = New MailAddress(MYSITE.adminemail, MYSITE.adminname)
            msg.Subject = subject
            msg.To.Add(email)
            msg.Subject = subject
            msg.IsBodyHtml = True
            msg.Body = body

            Try
                With MYSITE
                    Dim _smtp As New SmtpClient(.smtpserver)
                    If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                        _smtp.Port = .smtpport
                        _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                        _smtp.EnableSsl = .smtpenablessl
                    End If
                    _smtp.Send(msg)
                    _smtp.Dispose()
                End With
            Catch ex As Exception
                Dim mymsg As String = ex.Message
                If IsNothing(ex.InnerException) = False Then
                    mymsg += vbCrLf & ex.InnerException.Message
                End If
                MsgBox("error on email:" & vbCrLf & mymsg)

            End Try

        End Sub


    End Class
    Public Enum FrequencyTypes
        OneTime
        Minutes
        Daily
        Weekly
        Monthly
    End Enum
    Public Interface ISchedulerJob
        Property IsValid() As Boolean
        Property ErrorMessage() As String
        Function Execute(ByRef atask As TABLES.Report) As Boolean
        Function DoTask(Optional ByVal sql As String = "", Optional ByVal status As Integer = 3) As Boolean
        Function DoRequest(ByVal xmlstring As String) As String
    End Interface
    Public Class Scheduler
        Dim _eoj As Boolean = False
        Public Sub New()
            _eoj = False
        End Sub
        Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Sub Start()
            Do Until _eoj = True
                Dim _tasksettings As New TABLES.Report_Settings
                DAL.Read(1, _tasksettings)
                _tasksettings.homepageurl = Now.ToString
                DAL.Update(_tasksettings)

                Common.FileOps.DirectoryClean(_tasksettings.outputfolder, Now())

                Dim mysql As String = "Select * from Report where IsDisabled = 0 and NextRunDate <= '" & Today & "' "
                Dim dt As DataTable = DAL.GetDataSet(mysql).Tables(0)
                Dim dr As DataRow
                Dim mytask As New TABLES.Report
                Dim myworker As Reports.ISchedulerJob

                For Each dr In dt.Rows
                    Try

                        DAL.FillEntity(dr, mytask)
                        myworker = LoadModule(mytask.type, mytask.assemblyfile)
                        If IsNothing(myworker) = False Then
                            myworker.Execute(mytask)
                        Else
                            mytask.isdisabled = True
                            mytask.lastmessage = "Invalid Type"
                            Tasks.Provider.DAL.Update(mytask)
                            Common.LOGGER.WriteToErrorLog(mytask.lastmessage, "", "SCHEDULER:" & mytask.taskname)
                        End If


                    Catch ex As Exception

                        mytask.isdisabled = True
                        mytask.lastmessage = ex.Message
                        DAL.Update(mytask)

                        Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & mytask.taskname)

                    End Try

                Next

                Dim myemail As New ClientEmailSender
                myemail.Start()

                Thread.Sleep(5000)

            Loop

        End Sub
        Private Shared Function TaskIsValid(ByRef atask As TABLES.Report) As Boolean
        End Function
        Private Shared Function TaskExecute(ByRef atask As TABLES.Report) As Boolean
            If atask.isdisabled = False Then Return False

        End Function
        Public Sub StopIt()
            _eoj = True
        End Sub
        Public Shared Function LoadModule(ByVal atype As String, ByVal assemblyfile As String) As Object
            If IsNothing(atype) Then Return Nothing
            If atype.Length < 1 Then Return Nothing

            Dim mypath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
            mypath += "\CRF.CLIENTVIEW.BLL.DLL"
            mypath = Strings.Replace(mypath.ToLower, "file:\", "")

            Dim asm As System.Reflection.Assembly
            Dim args() As Object = Nothing
            Dim returnObj As Object = Nothing
            Dim s As String = Assembly.GetExecutingAssembly.FullName
            Dim Type As Type
            If assemblyfile.Length < 1 Then
                Type = Assembly.GetExecutingAssembly().GetType(atype)
            Else
                asm = Assembly.LoadFrom(mypath)
                If IsNothing(asm) = False Then
                    Type = asm.GetType(atype.Trim)
                    If Type IsNot Nothing Then
                        returnObj = asm.CreateInstance(atype.Trim)
                        '   returnObj = DirectCast(Activator.CreateInstance(Type, args), Tasks.SchedulerJob)
                        Return returnObj
                    End If
                End If
            End If


            Return returnObj

        End Function
    End Class
    Public Class SchedulerJob
        Implements ISchedulerJob
        Dim _isdisabled As Boolean = True
        Dim _reportname As String = ""
        Dim _errormessage As String = ""
        Dim _frequency As FrequencyTypes = FrequencyTypes.Daily
        Dim _nextrundate As Date = Now()
        Dim _lastrundate As Date = Now()
        Dim _interval As Integer = 1
        Dim _fromhour As Integer = 0
        Dim _thruhour As Integer = 24
        Dim _reportsettings As New TABLES.Report_Settings
        Dim _history As New TABLES.Report_History
        Dim _report As New TABLES.Report

        Dim _emailto As New List(Of System.Net.Mail.MailAddress)

        Public Sub New()
            ProviderBase.DAL.Read(1, _reportsettings)

        End Sub
        Public Function Execute(ByRef atask As TABLES.Report) As Boolean Implements ISchedulerJob.Execute
            Try
                _report = atask

                If IsScheduled = True Then
                    If DoTask("") = True Then
                        Schedule()
                    End If
                End If

            Catch ex As Exception

                _report.isdisabled = True
                _report.lastmessage = ex.Message
                Tasks.Provider.DAL.Update(_report)

            End Try


        End Function
        Public Overridable Function DoTask(Optional ByVal sql As String = "", Optional ByVal Status As Integer = 3) As Boolean Implements ISchedulerJob.DoTask
            Return False

        End Function
        Public Overridable Function DoRequest(ByVal xmlstring As String) As String Implements ISchedulerJob.DoRequest
            Return Nothing

        End Function
        Public ReadOnly Property Report() As TABLES.Report
            Get
                Return _report
            End Get
        End Property
        Public ReadOnly Property History() As TABLES.Report_History
            Get
                Return _history
            End Get
        End Property
        Public ReadOnly Property Settings() As TABLES.Report_Settings
            Get
                Return _reportsettings
            End Get
        End Property

        Public ReadOnly Property EmailTo() As List(Of System.Net.Mail.MailAddress)
            Get
                Return _emailto
            End Get
        End Property
        Public Property IsScheduled() As Boolean Implements ISchedulerJob.IsValid
            Get
                If _report.nextrundate >= Now Then
                    Return False
                End If

                Dim hh As Integer = DatePart(DateInterval.Hour, Now)
                If hh < _report.runfromhour Then
                    Return False
                End If
                If hh > _report.runuptohour Then
                    Return False
                End If
                Return True
            End Get

            Set(ByVal value As Boolean)
                _isdisabled = value
            End Set

        End Property
        Private Sub Schedule()

            _report.lastrundate = Now

            Select Case _report.frequency

                Case Reports.FrequencyTypes.Minutes
                    _report.nextrundate = DateAdd(DateInterval.Hour, _report.number, Now)
                    Dim hh As Integer = DatePart(DateInterval.Hour, _report.nextrundate)
                    If hh > _report.runuptohour Then
                        _report.nextrundate = DateAdd(DateInterval.Day, 1, Today)
                    End If

                Case Reports.FrequencyTypes.Daily
                    _report.nextrundate = DateAdd(DateInterval.Day, _report.number, Today)

                Case Reports.FrequencyTypes.Weekly
                    _report.nextrundate = DateAdd(DateInterval.Day, _report.number + 6, Today)

                Case Reports.FrequencyTypes.Monthly
                    _report.nextrundate = DateAdd(DateInterval.Month, _report.number, Today)

                Case Else
                    _isdisabled = True

            End Select

            With _report
                .lastrundate = Now
                .lastmessage = ""
            End With

            Tasks.Provider.DAL.Update(_report)


        End Sub
        Public Sub CreateHistory(ByVal areportname As String, ByVal Batchid As String, ByVal ClientId As Integer, ByVal clientcode As String, Optional ByVal subject As String = "", Optional ByVal BranchClientId As Integer = 0)
            _report = New TABLES.Report
            ProviderBase.DAL.GetItem(_report.TableObjectName, "taskname", areportname, _report)

            _history = New TABLES.Report_History
            _history.batchid = Batchid
            _history.reportid = _report.PKID
            _history.taskname = areportname
            _history.startdate = Now
            _history.clientid = ClientId
            _history.clientcode = clientcode
            _history.status = 0
            _history.subject = subject
            _history.BranchClientId = BranchClientId

            ProviderBase.DAL.Create(_history)

        End Sub
        Public Sub CreateAttachment(ByVal adescription As String, ByVal afilepath As String)
            Dim _reportattach As New TABLES.Report_HistoryAttachment

            _reportattach.datecreated = Now()
            _reportattach.description = adescription
            _reportattach.historyid = _history.PKID
            _reportattach.image = PDFLIB.GetStream(afilepath)
            _reportattach.url = System.IO.Path.GetFileName(afilepath)
            ProviderBase.DAL.Create(_reportattach)

        End Sub
        Public Sub UpdateStatus(ByVal altkeyid As Integer, ByVal totrecords As Integer, ByVal lastrecord As Integer)
            _history.altkeyid = altkeyid
            _history.totitems = totrecords
            _history.lastitem = lastrecord
            _history.enddate = Now()

            ProviderBase.DAL.Update(_history)

        End Sub
        Public Sub CreateRecipient(ByVal aemails As String)
            Dim ss As String() = Strings.Split(aemails, ",")
            For Each s As String In ss
                Dim myitem As New TABLES.Report_HistoryRecipient
                With myitem
                    .historyid = _history.PKID
                    .email = FixEmail(s)
                End With
                ProviderBase.DAL.Create(myitem)
            Next

        End Sub
        Public Sub CloseHistory(ByVal status As Integer, ByVal message As String)
            _history.enddate = Now
            _history.status = status
            _history.message = message
            _history.keyvalues = ""

            ProviderBase.DAL.Update(_history)

        End Sub
        Private Sub UpdateTask()
            With _report
                .lastrundate = Now
                .lastmessage = Me.ErrorMessage
                .isdisabled = _isdisabled
            End With
            Tasks.Provider.DAL.Update(_report)



        End Sub
        Public Property ErrorMessage() As String Implements ISchedulerJob.ErrorMessage
            Get
                Return _errormessage
            End Get
            Set(ByVal value As String)
                _errormessage = value
            End Set
        End Property
        Friend Shared Function FixEmail(ByVal emailaddr As String) As String
            If IsDBNull(emailaddr) = True Then
                Return String.Empty
            End If
            Dim s As String = Strings.Replace(emailaddr.Trim, vbCrLf, "")
            Return s

        End Function
        Public Shared Function RenderC1Report(ByVal aview As DataView, ByVal areportdef As String, ByVal areportname As String, ByVal aoutputfolder As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String

            Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
            Dim myreportpath As String = areportdef
            Dim myoutputfolder As String = aoutputfolder
            Dim MYAGENCY As New CRFDB.TABLES.Agency
            CRFDB.Provider.DBO.Read(1, MYAGENCY)
            Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
            mylogo.LoadImage(MYAGENCY.AgencyLogo)
            MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
            MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter
            Dim mymode As HDS.Reporting.COMMON.PrintMode = HDS.Reporting.COMMON.PrintMode.PrintToPDF
            Dim sreport As String = MYC1RPT.Render(areportprefix, myoutputfolder, myreportpath, areportname, aview, HDS.Reporting.COMMON.PrintMode.PrintToPDF, areportprefix, asubtitle1, asubtitle2)
            If System.IO.File.Exists(sreport) Then
                Return aoutputfolder & System.IO.Path.GetFileName(sreport)
            End If

            Return Nothing

        End Function

    End Class
    Public Class ClientReport
        Inherits ProviderBase
        Dim _history As New Report_History
        Dim _keyvalues As New Dictionary(Of String, String)
        Dim _attachments As New Dictionary(Of String, String)
        Dim _recipients As New Dictionary(Of String, String)
        Public Sub New(ByVal HistoryId As Integer)
            ProviderBase.DAL.Read(HistoryId, _history)

        End Sub
        Public Sub New(ByVal ClientId As Integer, ByVal clientcode As String, ByVal ReportId As Integer)

            _history = New TABLES.Report_History
            _history.reportid = ReportId
            _history.startdate = Now
            _history.clientid = ClientId
            _history.clientcode = clientcode
            _history.status = 0
            ProviderBase.DAL.Create(_history)

        End Sub
        Public ReadOnly Property KeyValues() As Dictionary(Of String, String)
            Get
                Return _keyvalues
            End Get
        End Property
        Public ReadOnly Property Attachments() As Dictionary(Of String, String)
            Get
                Return _attachments
            End Get
        End Property
        Public ReadOnly Property Recipients() As Dictionary(Of String, String)
            Get
                Return _recipients
            End Get
        End Property
        Public Sub CreateAttachment(ByVal adescription As String, ByVal afilepath As String)
            Dim _reportattach As New TABLES.Report_HistoryAttachment
            _reportattach.datecreated = Now()
            _reportattach.description = adescription
            _reportattach.historyid = _history.PKID
            _reportattach.image = PDFLIB.GetStream(afilepath)
            _reportattach.url = System.IO.Path.GetFileName(afilepath)
            ProviderBase.DAL.Create(_reportattach)

        End Sub
        Public Sub CreateRecipient(ByVal aemail As String, ByVal ausername As String)
            Dim myitem As New TABLES.Report_HistoryRecipient
            Dim ss As String() = Strings.Split(aemail, ",")
            For Each s As String In ss
                With myitem
                    .historyid = _history.PKID
                    .email = FixEmail(aemail)
                    .username = ausername
                End With
                ProviderBase.DAL.Create(myitem)
            Next

        End Sub
        Public Sub CloseHistory(ByVal status As Integer, ByVal message As String)
            _history.enddate = Now
            _history.status = status
            _history.message = message
            _history.keyvalues = ""
            For Each KEYV As KeyValuePair(Of String, String) In Me.KeyValues
                _history.keyvalues += KEYV.Key & vbTab & KEYV.Value & vbCrLf
            Next
            ProviderBase.DAL.Update(_history)

        End Sub
        Friend Shared Function FixEmail(ByVal emailaddr As String) As String
            If IsDBNull(emailaddr) = True Then
                Return String.Empty
            End If
            Dim s As String = Strings.Replace(emailaddr.Trim, vbCrLf, "")
            Return s

        End Function
    End Class
    Public Class ClientEmailSender
        Inherits ProviderBase
        Dim _history As New Report_History
        Dim _settings As New Report_Settings
        Public Sub New()
            DAL.Read(1, _settings)

        End Sub
        Public Sub Start()
            Dim mysql As String

            mysql = "UPDATE REPORT_HISTORY SET STATUS = 3 FROM REPORT_HISTORY "
            mysql += " Where Status = 2 and StartDate < ='" & Today.ToString & "' "
            DAL.ExecNonQuery(MYSQL)

            mysql = "Select * from Report_History Where Status = 3 "
            SendQuery(mysql)


        End Sub
        Public Function SendQuery(ByVal asql As String, Optional ByVal TestEmail As Boolean = False) As Integer
            Dim myds As DataSet = DAL.GetDataSet(asql)
            For Each dr As DataRow In myds.Tables(0).Rows
                DAL.FillEntity(dr, _history)
                If _history.status = 3 Then
                    _history.status = -3
                    DAL.Update(_history)
                    SendEmail(_history, TestEmail)
                    ' when testing client email
                    'SendEmail(_history, True)
                End If
                Thread.Sleep(1000)
            Next

            Return myds.Tables(0).Rows.Count

        End Function
        Public Function SendEmail(ByRef ahistory As Report_History, Optional ByVal TestEmail As Boolean = False) As Boolean
            _history = ahistory
            _settings = New Report_Settings
            DAL.Read(1, _settings)

            Dim msg As New MailMessage

            msg.To.Add("CRFIT@crfsolutions.com")

            Dim myreport As New TABLES.Report
            Dim myattach As New TABLES.Report_HistoryAttachment
            Dim myrecips As New TABLES.Report_HistoryRecipient
            Dim mysql As String
            Dim myClientName As String

            Try
                DAL.Read(_history.reportid, myreport)
                msg.From = New Net.Mail.MailAddress(_settings.adminemail, _settings.adminname)
                mysql = "Select * from Report_HistoryRecipient Where HistoryId = '" & _history.PKID & "' "

                Dim myds As DataSet = DAL.GetDataSet(mysql)
                If TestEmail = False Then
                    For Each dr As DataRow In myds.Tables(0).Rows
                        DAL.FillEntity(dr, myrecips)
                        msg.To.Add(myrecips.email)
                    Next
                End If

                Dim myclient As New TABLES.Client
                If _history.BranchClientId > 0 Then
                    DAL.Read(_history.BranchClientId, myclient)
                    myClientName = myclient.BranchNumber & " " & myclient.ClientName
                Else
                    DAL.Read(_history.clientid, myclient)
                    myClientName = myclient.ClientName
                End If

                Dim myview As DAL.COMMON.TableView
                If myreport.taskname = "LIEN-INVOICE" Then
                    mysql = "Select * from ClientLienInfo where ClientId = " & _history.clientid
                    myview = DAL.GetTableView(mysql)
                    myClientName = myview.RowItem("InvoiceName")
                End If
                If myreport.taskname = "COLL-INVOICE" Then
                    mysql = "Select * from ClientCollectionInfo where ClientId = " & _history.clientid
                    myview = DAL.GetTableView(mysql)
                    myClientName = myview.RowItem("InvoiceName")
                End If

                msg.IsBodyHtml = True
                msg.Subject = myreport.title
                If _history.subject.Length > 5 Then
                    msg.Subject = _history.subject
                End If

                msg.Body = _settings.emailtemplate
                msg.Body = Strings.Replace(msg.Body, "[REPORTTITLE]", myreport.title)
                msg.Body = Strings.Replace(msg.Body, "[PREPAREDON]", "Prepared On: " & Now.ToString)
                msg.Body = Strings.Replace(msg.Body, "[CLIENTCODE]", "ClientId: " & myclient.ClientCode & "-" & myClientName)
                msg.Body = Strings.Replace(msg.Body, "[PERIOD]", _history.body)
                msg.Body = Strings.Replace(msg.Body, "[SPECIALNOTE]", myreport.description)

                mysql = "Select * from Report_HistoryAttachment Where HistoryId = '" & _history.PKID & "' "
                myds = DAL.GetDataSet(mysql)
                msg.Body += "<p>"
                For Each dr As DataRow In myds.Tables(0).Rows
                    DAL.FillEntity(dr, myattach)

                    Dim myURL As String = ""
                    Dim mylink = _settings.reportserverurl
                    mylink = Strings.Replace(mylink, "[uid]", myattach.historyid)
                    mylink = Strings.Replace(mylink, "[itemid]", myattach.PKID)

                    myURL += "<a href=""[URL]"" title=""[DESCRIPTION]"">[DESCRIPTION]</a><br>"
                    myURL = Strings.Replace(myURL, "[URL]", mylink)
                    myURL = Strings.Replace(myURL, "[DESCRIPTION]", myattach.description)
                    msg.Body += myURL

                Next


                msg.Body += "</p>"
                If myds.Tables(0).Rows.Count > 0 Then

                    With _settings
                        Dim _smtp As New SmtpClient(.smtpserver)
                        If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                            _smtp.Port = .smtpport
                            _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                            _smtp.EnableSsl = .smtpenablessl
                        End If
                        _smtp.Send(msg)
                        _smtp.Dispose()
                    End With

                    If TestEmail = False Then
                        _history.status = 9
                    End If
                    _history.emaildate = Now()
                    _history.message = ""
                    DAL.Update(_history)

                Else
                    _history.status = -1
                    _history.emaildate = Now()
                    _history.message = "No Attachments"
                    DAL.Update(_history)

                End If


                Return True

            Catch ex As Exception
                _history.status = -1
                _history.emaildate = Now()
                _history.message = ex.Message
                DAL.Update(_history)
                Return False

            End Try

            Return True


        End Function

      
    End Class

#End Region


End Namespace
