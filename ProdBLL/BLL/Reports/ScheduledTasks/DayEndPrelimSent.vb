Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports CRF.CLIENTVIEW.BLL.Tasks
Imports CRF.CLIENTVIEW.BLL
Namespace ScheduledReports
    '''<summary> 
    ''' Mail Job - Sending the mail message
    ''' or any other job you wish to be executed
    ''' Implements the ISchedulerJob interface 
    '''</summary> 
    Public Class DayEndPrelimSent
        Inherits Reports.SchedulerJob
        Dim myreportdef As String
        Dim myoutputfolder As String
        Dim mybranchclientid As Integer
        Dim fromdate As Date
        Dim thrudate As Date
        Dim mybatchid As String
        Dim myclientid As String
        Dim mystatus As Integer = 2
        Public Overrides Function DoTask(Optional ByVal sql As String = "", Optional ByVal status As Integer = 3) As Boolean
            mystatus = status

            Dim myreq As New Tasks.request
            fromdate = DateAdd(DateInterval.Day, -1, Today)
            thrudate = DateAdd(DateInterval.Day, 0, Today)
            'fromdate = DateAdd(DateInterval.Day, -3, Today)
            'thrudate = DateAdd(DateInterval.Day, -2, Today)

            'myreq.arguments("reportdef") = Settings.settingsfolder & "LIENREPORTS.XML"
            myreq.arguments("reportdef") = Settings.settingsfolder & "LIENREPORTS.FLXR"
            myreq.arguments("reportname") = "ANotice"
            myreq.arguments("outputfolder") = Settings.outputfolder
            myreq.arguments("sql") = "uspbo_LiensDayEnd_GetPrelimSentByEmail '" & fromdate & "','" & thrudate & "'"
            If sql.Length > 1 Then
                myreq.arguments("sql") = sql
                Dim s As String = Strings.Replace(sql, "uspbo_LiensDayEnd_GetPrelimSentByEmail ", "")
                s = Strings.Replace(s, "'", "")
                s = Strings.Replace(s, " ", "")

                Dim ss As String() = Strings.Split(s, ",")
                fromdate = ss(0)
                thrudate = ss(1)
            End If

            Render(myreq)

            Return True

        End Function
        Public Overrides Function DoRequest(ByVal xmlstring As String) As String

            Dim myreq As New request
            myreq.fromxml(xmlstring)
            Render(myreq)
            Return myreq.toxml

        End Function
        Public Function Render(ByRef myreq As request) As Boolean

            myreportdef = myreq.arguments("reportdef")
            myoutputfolder = System.Windows.Forms.Application.StartupPath & "\output\"
            If System.IO.Directory.Exists(myoutputfolder) = False Then
                System.IO.Directory.CreateDirectory(myoutputfolder)
            End If

            Try
                mybatchid = ProviderBase.GetUniqueId
                Dim myds As DataSet = ProviderBase.DAL.GetDataSet(myreq.arguments("sql"))
                Dim DR As DataRow = Nothing
                Dim idx As Integer = 0
                For Each DR In myds.Tables(0).Rows
                    Try
                        Dim myview As New DataView(myds.Tables(1))
                        myview.RowFilter = "ClientCode='" & DR("ClientCode") & "'"

                        myclientid = myview.Item(0).Row.Item("ClientId")

                        ' 10/17/2014
                        If Not IsDBNull(DR("BranchClientId")) Then
                            mybranchclientid = DR("BranchClientId")
                        Else
                            mybranchclientid = 0
                        End If

                        Me.CreateHistory("LIEN-PRELIMSENT", mybatchid, myclientid, DR("ClientCode"), , mybranchclientid)

                        Me.History.body = " Activity From " & Strings.FormatDateTime(fromdate, DateFormat.ShortDate)
                        idx += 1
                        Me.UpdateStatus(0, myds.Tables(0).Rows.Count, idx)

                        Dim mylieninfo As New TABLES.ClientLienInfo
                        ProviderBase.DAL.GetItem("SELECT * FROM CLIENTLIENINFO WHERE CLIENTID = " & DR("CLIENTID"), mylieninfo)
                        If mylieninfo.IsIndividualNTO = False Then
                            Dim myreportname As String = "PrelimSentDaily"
                            If DR("UseBranchNTO") = 1 Then
                                myview.RowFilter = "ClientCode='" & DR("ClientCode") & "' AND BranchNum='" & DR("BranchNum") & "' "
                                myreportname = "PrelimSentBranchDaily"
                            End If
                            Dim mypath As String = RenderC1Report(myview, myreportdef, myreportname, myoutputfolder, True, DR("ClientCode"), "", "")
                            Me.CreateAttachment("Prelim Notices Sent", mypath)
                            Me.CreateRecipient(DR("emailrptsto"))

                            ' 1/7/2013 Added to handle branch notice copies
                            If DR("UseBranchNTO") = 1 Then
                                mypath = GetClientCopies(DR("clientid"), DR("clientcode"), DR("BranchNum"), fromdate, thrudate)
                            Else
                                mypath = GetClientCopies(DR("clientid"), DR("clientcode"), "", fromdate, thrudate)
                            End If
                            '
                            If mypath <> Nothing Then
                                Me.CreateAttachment("Prelim Notice Copies", mypath)
                            End If

                        Else
                            GetClientCopiesIndiv(DR("clientid"), DR("clientcode"), "", fromdate, thrudate, DR("emailrptsto"))
                        End If

                        Me.CloseHistory(mystatus, "")

                    Catch ex As Exception
                        Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & "Prelim Sent")
                    End Try
                    
                Next

            Catch ex As Exception
                Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & "Prelim Sent")

            End Try

            Common.FileOps.DirectoryClean(myoutputfolder, DateAdd(DateInterval.Day, 1, Today))

            Return True

        End Function
        Private Function GetClientCopies(ByVal clientid As String, ByVal clientcode As String, ByVal branchnum As String, ByVal fromdate As String, ByVal thrudate As String) As String

            Dim mydest As String = "\\10.0.2.2\e$\JobNoticeImages\"
            Dim mysql As String = "uspbo_LiensDayEnd_GetNotices1 " & clientid & ",0,'" & fromdate & "','" & thrudate & "'"

            Dim myds As New DataSet
            myds = ProviderBase.DAL.GetDataSet(mysql)

            Dim myview As New HDS.DAL.COMMON.TableView(myds.Tables(0))
            If branchnum.Length > 1 Then
                myview.RowFilter = "BranchNum='" & branchnum & "'"
            End If
            If myview.Count < 1 Then
                Dim s As String = myview.RowFilter
            End If

            Dim afiles As New ArrayList
            Dim myjobid As String = ""
            Do Until myview.EOF
                If myjobid <> myview.RowItem("JobId").ToString Then
                    Dim myfile As String = GetCopies(mydest, myview.RowItem("JobId"), myview.RowItem("JobNoticeHistoryId"))
                    If System.IO.File.Exists(myfile) = True Then
                        afiles.Add(myfile)
                    Else
                        Dim s As String = myfile
                    End If
                End If
                myjobid = myview.RowItem("JobId").ToString
                myview.MoveNext()
            Loop

            'Dim mydest1 As String = myoutputfolder & clientcode & branchnum & "-NoticeCopies-" & DatePart(DateInterval.Year, Today) & DatePart(DateInterval.Month, Today) & DatePart(DateInterval.Day, Today) & ".pdf"
            ' MM = month and mm = minutes
            Dim mydest1 As String = myoutputfolder & clientcode & branchnum & "-NoticeCopies-" & Today.ToString("yyyyMMdd") & ".pdf"
            If afiles.Count > 0 Then
                If combinepdfs(afiles, mydest1) = True Then
                    Return mydest1
                End If
            End If

            Return Nothing

        End Function
        Private Function GetClientCopiesIndiv(ByVal clientid As String, ByVal clientcode As String, ByVal branchnum As String, ByVal fromdate As String, ByVal thrudate As String, ByVal emailto As String) As String

            Dim mydest As String = "\\10.0.2.2\e$\JobNoticeImages\"

            Dim mysql As String = "uspbo_LiensDayEnd_GetNotices1 " & clientid & ",0,'" & fromdate & "','" & thrudate & "'"
            Dim mycust As New TABLES.ClientCustomer
            Dim myJOB As New TABLES.Job

            Dim myds As New DataSet
            myds = ProviderBase.DAL.GetDataSet(mysql)

            Dim myview As New HDS.DAL.COMMON.TableView(myds.Tables(0))

            Dim afiles As New ArrayList
            Dim myjobid As String = ""
            Do Until myview.EOF
                If myjobid <> myview.RowItem("JobId").ToString Then


                    ProviderBase.DAL.Read(myview.RowItem("JobId"), myJOB)
                    ProviderBase.DAL.Read(myJOB.CustId, mycust)
                    'Dim mydest1 As String = myoutputfolder & clientcode & "-" & mycust.ClientCustomer & "-" & myview.RowItem("JobId") & "-" & DatePart(DateInterval.Year, Today) & DatePart(DateInterval.Month, Today) & DatePart(DateInterval.Day, Today) & ".pdf"
                    Dim mydest1 As String = myoutputfolder & clientcode & "-" & mycust.ClientCustomer & "-" & myview.RowItem("JobId") & "-" & Today.ToString("yyyyMMdd") & ".pdf"

                    Dim myfile As String = GetCopies(mydest, myview.RowItem("JobId"), myview.RowItem("JobNoticeHistoryId"))
                    mydest1 = Strings.Replace(mydest1, " ", "-")
                    mydest1 = Strings.Replace(mydest1, "&", "-")
                    mydest1 = Strings.Replace(mydest1, ",", "-")
                    mydest1 = Strings.Replace(mydest1, "/", "-")
                    If System.IO.File.Exists(myfile) = True Then
                        System.IO.File.Copy(myfile, mydest1, True)
                        Dim s As String
                        If Len(myview.RowItem("CustJobNum")) > 0 Then
                            s = "Notice Sent CRF# ( [CRFNO] ) - CustName ( [CUSTNAME] ) - ( [CUSTJOBNUM] )"
                            s = Strings.Replace(s, "[CRFNO]", myview.RowItem("JobId"))
                            s = Strings.Replace(s, "[CUSTNAME]", mycust.ClientCustomer)
                            s = Strings.Replace(s, "[CUSTJOBNUM]", myview.RowItem("CustJobNum"))
                        Else
                            s = "Notice Sent CRF# ( [CRFNO] ) - CustName ( [CUSTNAME] )"
                            s = Strings.Replace(s, "[CRFNO]", myview.RowItem("JobId"))
                            s = Strings.Replace(s, "[CUSTNAME]", mycust.ClientCustomer)
                        End If

                        Me.CreateHistory("LIEN-PRELIMSENT", mybatchid, myclientid, myview.RowItem("ClientCode"), , mybranchclientid)
                        Me.History.body = " Activity From " & Strings.FormatDateTime(fromdate, DateFormat.ShortDate)
                        Me.CreateRecipient(emailto)
                        Me.CreateAttachment(s, mydest1)
                        Me.CloseHistory(mystatus, "")

                    End If
                End If

                myjobid = myview.RowItem("JobId").ToString
                myview.MoveNext()

            Loop
            Return Nothing

        End Function
        Private Function GetCopies(ByVal dest As String, ByVal ajobid As String, ByVal anoticeid As String) As String
            Dim myfilename As String = ""
            myfilename = dest
            myfilename += ajobid
            myfilename += "-" & anoticeid & ".PDF"

            Return myfilename

        End Function
        Public Function Combinex(ByVal afiles As ArrayList, ByVal adest As String) As Boolean

            Try
                Dim mypath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
                mypath = Strings.Replace(mypath, "file:\", "")


                Dim mytemp As String = System.Windows.Forms.Application.StartupPath & "\temp\"
                If System.IO.Directory.Exists(mytemp) Then
                    System.IO.Directory.Delete(mytemp, True)
                End If

                System.IO.Directory.CreateDirectory(mytemp)

                If System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(adest)) = False Then
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(adest))
                End If

                System.Threading.Thread.Sleep(1000)

                Dim FILE As String
                For Each FILE In afiles
                    Dim OUTFILE As String = mytemp & System.IO.Path.GetFileName(FILE)
                    If System.IO.File.Exists(FILE) = True Then
                        System.IO.File.Copy(FILE, OUTFILE)
                        System.Threading.Thread.Sleep(1500)
                    End If
                Next

                Dim S As String = System.Windows.Forms.Application.StartupPath & mypath & "\PDFTK  " & mypath & "\TEMP\*.PDF CAT OUTPUT " & mypath & "\TEMP\ONE.PDF"

                Shell(S, AppWinStyle.Hide, True)
                System.Windows.Forms.Application.DoEvents()

                System.IO.File.Copy(mytemp & "one.PDF", adest, True)
                System.Windows.Forms.Application.DoEvents()

            Catch EX As Exception
                Throw New Exception("ClientPrelimSent-Copies" & vbCrLf & EX.Message)
                Return False
            Finally
            End Try
            Return True

        End Function
        Public Function combinepdfs(ByVal afiles As ArrayList, ByVal adest As String) As Boolean
            Dim myoutputfolder As String = System.Windows.Forms.Application.StartupPath & "\temp"
            Common.FileOps.DirectoryClean(myoutputfolder, DateAdd(DateInterval.Day, 1, Today))

            For Each FILE As String In afiles
                Dim OUTFILE As String = myoutputfolder & "\" & System.IO.Path.GetFileName(FILE)
                If System.IO.File.Exists(FILE) = True Then
                    System.IO.File.Copy(FILE, OUTFILE)
                    System.Threading.Thread.Sleep(1500)
                End If
            Next

            Dim mypath As String = PDFMERGE.ProccessFolder(myoutputfolder)
            If System.IO.File.Exists(mypath) Then
                System.IO.File.Copy(mypath, adest, True)
                System.Windows.Forms.Application.DoEvents()
            Else
                Throw New Exception("Error On ClientPrelimSent-Copies:" & adest & vbCrLf)
                Return False
            End If
            Return True

        End Function
    End Class

End Namespace

