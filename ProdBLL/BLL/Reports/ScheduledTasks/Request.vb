Imports System.Collections.Generic
Imports System.Xml
Imports System.Configuration.ConfigurationManager
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.Net
Imports System.IO
Imports System
Imports System.Xml.Serialization
Imports System.Web
Imports System.Collections.Specialized
Imports System.Security.Cryptography
Imports System.Xml.XPath
Namespace Tasks
    Public Class request
        Public type As String = String.Empty
        Public accesskey As String = String.Empty
        Public results As New results
        Public arguments As New Dictionary(Of String, String)
        Public response As String = String.Empty
        Public Function toxml() As String

            Dim sb As New StringBuilder
            sb.AppendLine("<?xml version=""1.0""?>")
            sb.AppendLine("<Request>")
            sb.AppendLine("<AccessKey>" & Me.accesskey & "</AccessKey>")
            sb.AppendLine("<Type>" & Me.type & "</Type>")
            sb.AppendLine("<Arguments>")
            For Each KEYV As KeyValuePair(Of String, String) In Me.arguments
                sb.AppendLine("<" & KEYV.Key & ">" & KEYV.Value & "</" & KEYV.Key & ">")
            Next
            sb.AppendLine("</Arguments>")
            sb.AppendLine("<Result>")
            sb.AppendLine("<TotalPages>" & Me.results.totalpages & "</TotalPages>")
            sb.AppendLine("<TotalResults>" & Me.results.totalresults & "</TotalResults>")
            sb.AppendLine("<StartTime>" & Me.results.startitme & "</StartTime>")
            sb.AppendLine("<EndTime>" & Me.results.endtime & "</EndTime>")
            sb.AppendLine("<Message>" & Me.results.message & "</Message>")
            sb.AppendLine("</Result>")
            sb.AppendLine("<Attachments>")
            For Each attach As fileattachment In Me.results.attachments
                sb.AppendLine("<Atttachment>")
                sb.AppendLine("<Name>" & Me.results.totalpages & "</Name>")
                sb.AppendLine("<Path>" & Me.results.totalpages & "</Path>")
                sb.AppendLine("<ClientId>" & Me.results.totalpages & "</ClientId>")
                sb.AppendLine("</Attachment>")
            Next
            sb.AppendLine("</Attachments>")
            sb.AppendLine("<Reponse>")
            sb.AppendLine(response)
            sb.AppendLine("</Response>")

            sb.AppendLine("</Request>")
            Dim s As String = Strings.Replace(sb.ToString, "&", "^")
            Return sb.ToString

        End Function
        Public Function fromxml(ByVal xmlstring As String) As Boolean

            Dim mydoc As New XmlDocument
            mydoc.LoadXml(xmlstring)
            Dim myitem As XmlNodeList = mydoc.GetElementsByTagName("Request")

            For Each record As XmlNode In myitem

                For Each field As XmlNode In record.ChildNodes
                    Dim s As String = field.Name.ToLower
                    Select Case s
                        Case "accesskey"
                            accesskey = field.InnerText

                        Case "type"
                            type = field.InnerText

                        Case "arguments"
                            For Each node As XmlNode In field.ChildNodes
                                arguments.Add(node.Name, node.InnerText)
                            Next

                    End Select

                Next
            Next

            results.startitme = Now
            results.endtime = Now
            results.status = 0
            results.message = ""

            Return True

        End Function
    End Class
    Public Class results
        Public totalpages As Integer = 0
        Public totalresults As Integer = 0
        Public startitme As Date = Now()
        Public endtime As Date = Now()
        Public status As String = String.Empty
        Public message As String = String.Empty
        Public attachments As New List(Of fileattachment)
    End Class
    Public Class fileattachment
        Public name As String
        Public path As String
        Public clientid As String
    End Class
End Namespace
