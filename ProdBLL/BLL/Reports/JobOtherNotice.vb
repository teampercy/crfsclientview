Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Namespace Liens.Reports
    Public Class JobOtherNotice
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As CurrentUser
        Dim mynoticeinfo As HDS.DAL.COMMON.TableView
        Dim myform As HDS.DAL.COMMON.TableView
        Dim myclientimage As HDS.DAL.COMMON.TableView
        Dim myimage As New HDS.WINLIB.Controls.ImageViewer
        Dim flexfieldtext As String


        Public Sub New()
            MyBase.New()
            CurrentUser = Globals.GetCurrentUser
            'mynoticeinfo = New HDS.DAL.COMMON.TableView(MYDS.Tables(0))
            Me.OutputFolder = CurrentUser.OutputFolder & "\"
            Me.SettingsFolder = CurrentUser.SettingsFolder & "\"
            Me.UserName = CurrentUser.UserName
            Me.ReportDefPath = Me.SettingsFolder & "Reports\WebLienNotices.xml"

        End Sub
        Public Function ExecuteC1Report(ByVal MYDS As DataSet) As String
            mynoticeinfo = New HDS.DAL.COMMON.TableView(myds.Tables(0))
            myform = New HDS.DAL.COMMON.TableView(MYDS.Tables(1))
            myclientimage = New HDS.DAL.COMMON.TableView(MYDS.Tables(2))
            If mynoticeinfo.Count = 1 Then
                Me.ReportName = myform.RowItem("ReportName")
                Me.DataView = mynoticeinfo
                With Me
                    .FilePrefix = GetFileName("SFORM-" & mynoticeinfo.RowItem("JobId") & "-" & Me.ReportName)
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Shared Function GetFileName(ByVal areportname As String) As String
            Dim myuser As CurrentUser = Globals.GetCurrentUser
            Dim myprefix As String = myuser.ClientCode
            myprefix += "-" & myuser.LoginCode
            myprefix += "-" & myuser.Id
            myprefix += "-" & areportname
            Return Globals.TrimSpace(myprefix)

        End Function
        Public Function ExecuteC1Report(ByVal aitemid As String) As String
            Dim mysproc As New SPROCS.uspbo_Jobs_GetOtherNoticeInfo
            mysproc.Id = aitemid
            Return ExecuteC1Report(CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetDataSet(mysproc))


        End Function
        '        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

        '            Dim C1R As C1.C1Report.C1Report = sender

        'CheckHeader:
        '            'If C1R.ReportName = "NewProjectLetterGC" Or C1R.ReportName = "NewProjectLetterCust" Then
        '            Try
        '                    Dim ZLOGO As Field = C1R.Fields("ClientLogo")
        '                    If ZLOGO.Section = e.Section Then
        '                        ZLOGO.Picture = GetLogo()
        '                        ZLOGO.PictureScale = PictureScaleEnum.Scale

        '                        If IsNothing(ZLOGO.Picture) = True Then
        '                            ZLOGO.Calculated = True
        '                            ZLOGO.Visible = True
        '                        Else
        '                            ZLOGO.Calculated = True
        '                            ZLOGO.Visible = True
        '                        End If

        '                    End If

        '                Catch EX As Exception
        '                    Throw New ApplicationException(EX.Message)

        '                End Try
        '            'End If

        '        End Sub


        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender

CheckHeader:
            Try
                If HasField("ClientLogo", e) = True Then
                    Dim ZLOGO As Field = C1R.Fields("ClientLogo")
                    If (myclientimage.Count > 0) Then
                        ZLOGO.Picture = GetLogo()
                        ZLOGO.PictureScale = PictureScaleEnum.Scale
                        If IsNothing(ZLOGO.Picture) = True Then
                                ZLOGO.Calculated = True
                                ZLOGO.Visible = True
                            Else
                                ZLOGO.Calculated = True
                                ZLOGO.Visible = True
                            End If
                        End If
                    End If


                    If HasField("Text11", e) = True Then
                    Dim ZTHROUGHDATE As Field = C1R.Fields("Text11")
                    If (myclientimage.Count > 0) Then
                        If ZTHROUGHDATE.Section = e.Section Then
                            ZTHROUGHDATE.Calculated = True
                            ZTHROUGHDATE.Visible = True
                            ZTHROUGHDATE.Value = GetThroughDate(ZTHROUGHDATE.Value)
                        End If
                    End If
                End If


            Catch EX As Exception
                Throw New ApplicationException(EX.Message)
            End Try

        End Sub

        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try
        End Function




        Public Function GetLogo() As Object
            myimage.LoadImage(myclientimage.RowItem("ImageObject"))
            Return myimage.PictureBox1.Image()
        End Function

        Public Function GetThroughDate(throughdate) As String
            flexfieldtext = throughdate
            ReplaceThroughDateTokens()
            Return flexfieldtext
        End Function

        Public Function ReplaceThroughDateTokens() As Boolean
            If (myclientimage.Count > 0) Then
                ReplaceDate(" & [ThroughDate] & ", myclientimage.RowItem("ThroughDate"))
            Else
                ReplaceDate(" & [ThroughDate] & ", " ")
            End If
        End Function

        Private Sub ReplaceDate(ByVal atag As String, ByVal avalue As Object)
            Try
                If IsDate(avalue) Then
                    Dim mydate As String = Date.Parse(avalue).ToShortDateString
                    flexfieldtext = Strings.Replace(flexfieldtext, atag, " " & mydate)
                Else
                    flexfieldtext = Strings.Replace(flexfieldtext, atag, " ")
                End If

            Catch ex As Exception
                flexfieldtext = Strings.Replace(flexfieldtext, atag, " ")
            End Try

        End Sub

    End Class

End Namespace
