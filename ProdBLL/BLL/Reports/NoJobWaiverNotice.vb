Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Imports HDS.Reporting.C1Reports
Namespace Liens.Reports
    Public Class NoJobWaiverNotice
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As CurrentUser
        Dim myimage As New HDS.WINLIB.Controls.ImageViewer

        Dim mywaiverinfo As BLL.ClientView.Lien.NoJobWaiverInfo
        Public Sub New(ByVal aitemid As Integer)
            MyBase.New()
            CurrentUser = Globals.GetCurrentUser
            mywaiverinfo = New BLL.ClientView.Lien.NoJobWaiverInfo(aitemid)
            Me.OutputFolder = CurrentUser.OutputFolder & "\"
            Me.SettingsFolder = CurrentUser.SettingsFolder
            Me.UserName = CurrentUser.UserName

        End Sub
        Public Function ExecuteC1Report() As String
            'Me.ReportDefPath = Me.SettingsFolder & "\Reports\Waivers.flxr"
            Me.ReportDefPath = Me.SettingsFolder & "\Reports\Waivers.xml"
            Me.ReportName = "Waiver"

            With Me
                .FilePrefix = GetFileName("WAIVER-" & mywaiverinfo.StateForm.FormCode)
                .TableIndex = 0
                .RenderReport()
                .ReportPath = .ReportFileName
                Return .ReportFileName

            End With

        End Function
        Public Shared Function GetFileName(ByVal areportname As String) As String
            Dim myuser As CurrentUser = Globals.GetCurrentUser
            Dim myprefix As String = myuser.ClientCode
            myprefix += "-" & myuser.LoginCode
            myprefix += "-" & myuser.Id
            myprefix += "-" & areportname
            Return Globals.TrimSpace(myprefix)

            
        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender

CheckHeader:
            Try
                Dim ZLOGO As Field = C1R.Fields("CLIENTLOGO")
                If ZLOGO.Section = e.Section Then
                    Dim ZADDR As Field = C1R.Fields("CLIENTADDRESS")
                    ZLOGO.Picture = GetLogo()
                    ZLOGO.PictureScale = PictureScaleEnum.Scale

                    ZADDR.Calculated = True
                    ZADDR.RTF = True
                    ZADDR.Visible = True
                    ZADDR.Value = GetAddress()

                    If IsNothing(ZLOGO.Picture) = True Then
                        ZLOGO.Calculated = True
                        ZLOGO.Visible = True
                    Else
                        ZLOGO.Calculated = True
                        ZLOGO.Visible = True
                    End If

                End If

                '12/13/2016 Added
                Dim ZWAIVERLOGID As Field = C1R.Fields("JOBWAIVERLOGID")
                If ZWAIVERLOGID.Section = e.Section Then
                    ZWAIVERLOGID.Calculated = True
                    ZWAIVERLOGID.RTF = True
                    ZWAIVERLOGID.Visible = False
                    ZWAIVERLOGID.Value = GetJobWaiverLogId()
                End If

                '12/13/2017 added for barcode
                Dim ZWAIVERBARCODE As Field = C1R.Fields("JOBWAIVERLOGIDBARCODE")
                If ZWAIVERBARCODE.Section = e.Section Then
                    ZWAIVERBARCODE.Calculated = True
                    ZWAIVERBARCODE.RTF = True
                    ZWAIVERBARCODE.Visible = True
                    ZWAIVERBARCODE.BarCode = BarCodeEnum.Code_128auto
                    ZWAIVERBARCODE.Value = GetJobWaiverLogId()
                End If
                Dim ZWAIVER As Field = C1R.Fields("WAIVERTEXT")
                If ZWAIVER.Section = e.Section Then
                    ZWAIVER.Calculated = True
                    ZWAIVER.RTF = True
                    ZWAIVER.Visible = True
                    ZWAIVER.Value = GetWaiver()
                End If

                Dim ZSIGTEXT As Field = C1R.Fields("SIGNATURETEXT")
                If ZSIGTEXT.Section = e.Section Then
                    ZSIGTEXT.Calculated = True
                    ZSIGTEXT.RTF = True
                    ZSIGTEXT.Visible = True
                    ZSIGTEXT.Value = GetSignature()
                End If

                If IsNothing(mywaiverinfo.ClientSigner.Signature) = False Then
                    Dim ZSIGLOGO As Field = C1R.Fields("CLIENTSIGNATURELOGO")
                    If ZSIGLOGO.Section = e.Section Then
                        ZSIGLOGO.Picture = GetSignatureLogo()
                        ZSIGLOGO.Visible = True
                    End If
                End If


                Dim ZNOTARY As Field = C1R.Fields("NOTARYTEXT")
                If ZNOTARY.Section = e.Section Then
                    If mywaiverinfo.WaiverInfo.Notary = True Then
                        ZNOTARY.Visible = False
                        ZNOTARY.Calculated = True
                        ZNOTARY.Value = GetNotary()
                        ZWAIVER.RTF = True
                    Else
                        ZNOTARY.Visible = False
                        ZNOTARY.Value = ""
                    End If
                End If

            Catch EX As Exception

                '   Common.ErrorHandler.Logger(CurrentUser, "StartSection", EX)
                Throw New ApplicationException(EX.Message)

            End Try


        End Sub
        Private Function GetFileName() As String
            Return mywaiverinfo.StateForm.FormCode & "-" & mywaiverinfo.Client.ClientCode & "-" & mywaiverinfo.WaiverInfo.BatchNoJobWaiverId

        End Function
        Public Function GetLogo() As Object
            Dim myimage As New HDS.WINLIB.Controls.ImageViewer
            myimage.LoadImage(mywaiverinfo.ClientImage.ImageObject)
            Return myimage.PictureBox1.Image()

        End Function
        Public Function GetSignatureLogo() As Object
            myimage.LoadImage(mywaiverinfo.ClientSigner.Signature)
            Return myimage.PictureBox1.Image()

        End Function
        Public Function GetSignature() As String
            m_rtf = New Windows.Forms.RichTextBox
            m_filename = Me.SettingsFolder & "\Waivers\Signature\" & mywaiverinfo.StateForm.WaiverSigRTF

            Dim s As String = ""
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    MyStream.Dispose()
                    Try
                        Replace("[CLIENTNAME]", mywaiverinfo.Client.ClientName)
                        Replace("[SIGNATORY]", mywaiverinfo.ClientSigner.Signer)
                        Replace("[SIGNATORYTITLE]", mywaiverinfo.ClientSigner.SignerTitle)
                        ReplaceDate("[TODAY]", Today.ToShortDateString)
                        s = (mywaiverinfo.Client.AddressLine1.Trim & "," & mywaiverinfo.Client.City.Trim & " " & mywaiverinfo.Client.State.Trim)
                        Replace("[JOBID]", " " & "")
                        Replace("[SIGNEREMAIL]", mywaiverinfo.ClientSigner.SignerEmail)
                        Replace("[CLIENTINFO]", S)
                        Replace("[CLIENTPHONE]", HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.ClientLienInfo.ContactPhone))
                        Replace("[JOBWAIVERLOGID]", " " & mywaiverinfo.WaiverInfo.BatchNoJobWaiverId)
                        S = m_rtf.Text
                    Catch ex As Exception
                        s = "Error Reading File:" & ex.Message
                        m_rtf.Text = s

                    End Try
                Else
                    S = "File Not Found :" & m_filename
                End If
            Catch ex As Exception
                S = "Error On File :" & ex.Message
                m_rtf.Text = s

            End Try

            Return m_rtf.Rtf

        End Function
        Private Function GetJobWaiverLogId() As String
            m_rtf = New Windows.Forms.RichTextBox
            m_filename = Me.SettingsFolder & "\Waivers\JobWaiverLogId\JobWaiverLogId.rtf"

            Dim s As String = ""
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    MyStream.Dispose()
                    Try
                        Replace("[JOBWAIVERLOGID]", mywaiverinfo.WaiverInfo.BatchNoJobWaiverId)
                        Replace("[JOBID]", "")
                        s = m_rtf.Text
                    Catch ex As Exception
                        s = "Error Reading File:" & ex.Message
                        m_rtf.Text = s

                    End Try
                Else
                    s = "File Not Found :" & m_filename

                End If
            Catch ex As Exception
                s = "Error On File :" & ex.Message
                m_rtf.Text = s

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function GetAddress() As String

            m_rtf = New Windows.Forms.RichTextBox
            Dim S As String = Me.SettingsFolder & "\Waivers\Address\"
            If mywaiverinfo.ClientImage.WaiverFolder.Trim.Length > 1 Then
                If System.IO.Directory.Exists(S & mywaiverinfo.ClientImage.WaiverFolder & "\") Then
                    S += mywaiverinfo.ClientImage.WaiverFolder & "\"
                End If
            End If
            S += "WaiverAddress.rtf"
            m_filename = S
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    MyStream.Dispose()
                    Try
                        ReplaceAddressTokens()
                        S = m_rtf.Text
                    Catch ex As Exception
                        S = "Error Reading File:" & ex.Message
                        'Common.ErrorHandler.Logger(CurrentUser, "FileReadError", ex)
                        Throw New ApplicationException(ex.Message)

                    End Try
                Else
                    S = "File Not Found :" & m_filename
                    Throw New ApplicationException(S)

                End If
            Catch ex As Exception
                S = "ErrorOnFile"
                ' Common.ErrorHandler.Logger(CurrentUser, S, ex)
                Throw New ApplicationException(ex.Message)

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function GetWaiver() As String
            m_rtf = New Windows.Forms.RichTextBox
            GetWaiverRTF()
            Dim S As String = m_filename
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    MyStream.Dispose()
                    Try
                        ReplaceTokens()
                        S = m_rtf.Text
                    Catch ex As Exception
                        S = "Error Reading File:" & ex.Message
                        Throw New ApplicationException(ex.Message)

                    End Try
                Else
                    S = "File Not Found :" & m_filename
                End If
            Catch ex As Exception
                S = "Error On File :" & ex.Message
                Throw New ApplicationException(ex.Message)

            End Try

            Return m_rtf.Rtf

        End Function
       
        Public Function GetWaiverRTF() As String
            Dim S As String = Me.SettingsFolder & "\Waivers\Waiver\"

            ' If custom waiver
            If mywaiverinfo.ClientImage.WaiverFolder.Trim.Length > 1 Then
                If System.IO.Directory.Exists(S & mywaiverinfo.ClientImage.WaiverFolder & "\") Then
                    S += mywaiverinfo.ClientImage.WaiverFolder & "\"
                End If
            End If
            m_formfolder = S

            m_filename = m_formfolder & mywaiverinfo.StateForm.FormCode & ".RTF"

            '   Check for waiver in custom or standard folder
            If System.IO.File.Exists(m_filename) = False Then   ' Not found
                m_filename = m_formfolder & "MISC" & mywaiverinfo.StateForm.FormCode & ".RTF"
                If System.IO.File.Exists(m_filename) = False Then ' Not found

                    '   10/18/2017  If file is not found in ClientLogo.WaiverFolder use the regular folder
                    If mywaiverinfo.ClientImage.WaiverFolder.Trim.Length > 1 Then

                        m_formfolder = Me.SettingsFolder & "\Waivers\Waiver\"
                        m_filename = m_formfolder & mywaiverinfo.StateForm.FormCode & ".RTF"
                        '
                        If System.IO.File.Exists(m_filename) = False Then ' StateForm not in standard folder
                            m_filename = m_formfolder & "MISC" & mywaiverinfo.StateForm.FormCode & ".RTF"
                        End If
                    End If

                End If
            End If

            Return m_filename

        End Function
        Public Function GetNotary() As String

            Dim S As String = Me.SettingsFolder & "\Waivers\Notary\"
            If mywaiverinfo.ClientImage.WaiverFolder.Trim.Length > 1 Then
                If System.IO.Directory.Exists(S & mywaiverinfo.ClientImage.WaiverFolder & "\") Then
                    S += mywaiverinfo.ClientImage.WaiverFolder & "\"
                End If
            End If
            m_filename = S & "Notary.rtf"
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    MyStream.Dispose()
                    Try
                        ReplaceTokens()
                        S = m_rtf.Text
                    Catch ex As Exception
                        S = "Error Reading File:" & ex.Message
                        Throw New ApplicationException(ex.Message)

                    End Try
                Else
                    S = "File Not Found :" & m_filename
                End If
            Catch ex As Exception
                S = "Error On File :" & ex.Message
                Throw New ApplicationException(ex.Message)

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function ReplaceAddressTokens() As Boolean
            Dim m_addressline1 As String = mywaiverinfo.Client.ClientName
            Dim m_addressline2 As String = mywaiverinfo.Client.AddressLine1
            Dim m_addressline3 As String = mywaiverinfo.Client.City & ", " & mywaiverinfo.Client.State & " " & mywaiverinfo.Client.PostalCode
            Dim m_addressphone As String = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.Client.PhoneNo)
            Dim m_addressfax As String = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.Client.Fax)
            If mywaiverinfo.ClientImage.WaiverAddress1.Length > 0 Then
                m_addressline2 = mywaiverinfo.ClientImage.WaiverAddress1
                m_addressline3 = mywaiverinfo.ClientImage.WaiverAddress2
                m_addressphone = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.ClientImage.WaiverPhone)
                m_addressfax = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.ClientImage.WaiverFax)
            End If

            Replace("[ClientName]", m_addressline1)
            Replace("[WaiverAddress1]", m_addressline2)
            Replace("[WaiverAddress2]", m_addressline3)
            Replace("[WaiverPhone]", m_addressphone)
            Replace("[WaiverFax]", m_addressfax)

        End Function
        Public Function ReplaceTokens() As Boolean
            Dim m_checkissuer As String = ""
            Dim m_checkpayable As String = ""

            Dim m_addressline As String = ""
            Dim m_addressline1 As String = ""
            Dim m_addressline2 As String = ""
            Dim m_addressline3 As String = ""

            Dim MYJOBINFO As String = ""
            Dim MYJOBINFO1 As String = ""
            Dim MYJOBINFO2 As String = ""
            Dim MYJOBINFO3 As String = ""
            Dim MYJOBINFO4 As String = ""

            MYJOBINFO = mywaiverinfo.WaiverInfo.JobName & " " & mywaiverinfo.WaiverInfo.JobNum
            MYJOBINFO1 = mywaiverinfo.WaiverInfo.JobName & " " & mywaiverinfo.WaiverInfo.JobNum
            MYJOBINFO2 = mywaiverinfo.WaiverInfo.JobAdd1
            MYJOBINFO3 = mywaiverinfo.WaiverInfo.JobAdd2
            MYJOBINFO4 = mywaiverinfo.WaiverInfo.JobCity & ", " & mywaiverinfo.WaiverInfo.JobState & " " & mywaiverinfo.WaiverInfo.JobZip
            If MYJOBINFO3.Length < 1 Then
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If
            If MYJOBINFO2.Length < 1 Then
                MYJOBINFO2 = MYJOBINFO3
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If
            If MYJOBINFO1.Length < 1 Then
                MYJOBINFO1 = MYJOBINFO2
                MYJOBINFO2 = MYJOBINFO3
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If

            m_checkpayable = mywaiverinfo.WaiverInfo.CustName
            m_addressline1 = mywaiverinfo.WaiverInfo.CustName
            m_addressline2 = mywaiverinfo.WaiverInfo.CustAdd1
            m_addressline3 = mywaiverinfo.WaiverInfo.CustCity & ", " & mywaiverinfo.WaiverInfo.CustState & " " & mywaiverinfo.WaiverInfo.CustZip

         
            If mywaiverinfo.WaiverInfo.MailtoGC = True Then
                m_checkpayable = mywaiverinfo.WaiverInfo.GCName
                m_addressline1 = mywaiverinfo.WaiverInfo.GCName
                m_addressline2 = mywaiverinfo.WaiverInfo.GCAdd1
                m_addressline3 = mywaiverinfo.WaiverInfo.GCCity & ", " & mywaiverinfo.WaiverInfo.GCState & " " & mywaiverinfo.WaiverInfo.GCZip
            End If

            If mywaiverinfo.WaiverInfo.MailtoOW = True Then
                m_checkpayable = mywaiverinfo.WaiverInfo.OwnerName
                m_addressline1 = mywaiverinfo.WaiverInfo.OwnerName
                m_addressline2 = mywaiverinfo.WaiverInfo.OwnerAdd1
                m_addressline3 = mywaiverinfo.WaiverInfo.OwnerCity & ", " & mywaiverinfo.WaiverInfo.OwnerState & " " & mywaiverinfo.WaiverInfo.OwnerZip
            End If

            If mywaiverinfo.WaiverInfo.MailtoLE = True Then
                m_checkpayable = mywaiverinfo.WaiverInfo.LenderName
                m_addressline1 = mywaiverinfo.WaiverInfo.LenderName
                m_addressline2 = mywaiverinfo.WaiverInfo.LenderAdd1
                m_addressline3 = mywaiverinfo.WaiverInfo.LenderCity & ", " & mywaiverinfo.WaiverInfo.LenderState & " " & mywaiverinfo.WaiverInfo.LenderZip
            End If


            If mywaiverinfo.WaiverInfo.JointCheckAgreement = True Then
                m_checkpayable = mywaiverinfo.Client.ClientName & " And " & mywaiverinfo.WaiverInfo.CustName
                'm_checkpayable = mywaiverinfo.Client.ClientName & " And " & m_checkpayable
            Else
                m_checkpayable = mywaiverinfo.Client.ClientName
            End If

            If m_addressline2.Length < 1 Then
                m_addressline2 = m_addressline3
                m_addressline3 = ""
            End If

            m_checkissuer = mywaiverinfo.WaiverInfo.CustName
            If mywaiverinfo.WaiverInfo.CheckByGC = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.GCName
            End If
            If mywaiverinfo.WaiverInfo.CheckbyLE = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.LenderName
            End If
            If mywaiverinfo.WaiverInfo.CheckbyOW = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.OwnerName
            End If
            If mywaiverinfo.WaiverInfo.CheckByOT = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.OtherChkIssuerName
            End If

            Replace("[WAIVERCODE]", mywaiverinfo.StateForm.NoticeCode)
            Replace("[JOBINFO]", MYJOBINFO)
            Replace("[JOBINFO1]", MYJOBINFO1)
            Replace("[JOBINFO2]", MYJOBINFO2)
            Replace("[JOBINFO3]", MYJOBINFO3)
            Replace("[JOBINFO4]", MYJOBINFO4)

            Replace("[MAILINFO1]", m_addressline1)
            Replace("[MAILINFO2]", m_addressline2)
            Replace("[MAILINFO3]", m_addressline3)
            Replace("[MAILINFO4]", "")

            Replace("[CHECKISSUER]", m_checkissuer)
            Replace("[PAYABLETO]", m_checkpayable)
            Replace("[CLIENTNAME]", mywaiverinfo.Client.ClientName)
            Replace("[SIGNATORY]", mywaiverinfo.ClientSigner.Signer)
            Replace("[SIGNATORYTITLE]", mywaiverinfo.ClientSigner.SignerTitle)
            Replace("[SIGNEREMAIL]", mywaiverinfo.ClientSigner.SignerEmail)
            Replace("[TODAY]", Today.ToShortDateString)

            Replace("[JOBNAME]", " " & mywaiverinfo.WaiverInfo.JobName)
            Replace("[JOBINFO]", " " & MYJOBINFO)
            Replace("[JOBADDR1]", " " & mywaiverinfo.WaiverInfo.JobAdd1)
            Replace("[JOBADDR2]", " " & mywaiverinfo.WaiverInfo.JobAdd1)
            Replace("[JOBCITY]", " " & mywaiverinfo.WaiverInfo.JobCity)
            Replace("[JOBSTATE]", " " & mywaiverinfo.WaiverInfo.JobState)
            Replace("[JOBZIP]", " " & mywaiverinfo.WaiverInfo.JobZip)
            Replace("[JOBNUM]", " " & mywaiverinfo.WaiverInfo.JobNum)
            Replace("[JOBID]", " " & "")
            Replace("[INVPAYMENTAPPNUM]", " " & mywaiverinfo.WaiverInfo.InvoicePaymentNo)
            Replace("[JOBCOUNTY]", " " & mywaiverinfo.WaiverInfo.JobCounty)


            Replace("[CUSTOMERNAME]", " " & mywaiverinfo.WaiverInfo.CustName)
            Replace("[CUSTADD1]", " " & mywaiverinfo.WaiverInfo.CustAdd1)
            Replace("[CUSTADD2]", " " & mywaiverinfo.WaiverInfo.CustAdd2)
            Replace("[CUSTCITY]", " " & mywaiverinfo.WaiverInfo.CustCity)
            Replace("[CUSTCONTACT]", " " & mywaiverinfo.WaiverInfo.CustContact)
            Replace("[CUSTREFNUM]", " " & mywaiverinfo.WaiverInfo.CustNum)
            Replace("[CUSTSTATE]", " " & mywaiverinfo.WaiverInfo.CustState)
            Replace("[CUSTZIP]", " " & mywaiverinfo.WaiverInfo.CustZip)

            Replace("[WaiverInfo]", " " & mywaiverinfo.WaiverInfo.GCName)
            Replace("[GCADD1]", " " & mywaiverinfo.WaiverInfo.GCAdd1)
            Replace("[GCADD2]", " " & mywaiverinfo.WaiverInfo.GCAdd2)
            Replace("[GCCITY]", " " & mywaiverinfo.WaiverInfo.GCCity)
            Replace("[GCNUM]", " " & mywaiverinfo.WaiverInfo.GCNum)
            Replace("[GCSTATE]", " " & mywaiverinfo.WaiverInfo.GCState)
            Replace("[GCZIP]", " " & mywaiverinfo.WaiverInfo.GCZip)

            Replace("[OWNERNAMES]", " " & mywaiverinfo.WaiverInfo.OwnerName)

            Replace("[OWNERNAME]", " " & mywaiverinfo.WaiverInfo.OwnerName)
            Replace("[OWNERADD1]", " " & mywaiverinfo.WaiverInfo.OwnerAdd1)
            Replace("[OWNERADD2]", " " & mywaiverinfo.WaiverInfo.OwnerAdd2)
            Replace("[OWNERCITY]", " " & mywaiverinfo.WaiverInfo.OwnerCity)
            Replace("[OWNERSTATE]", " " & mywaiverinfo.WaiverInfo.OwnerState)
            Replace("[OWNERZIP]", " " & mywaiverinfo.WaiverInfo.OwnerZip)

            Replace("[LENDERNAME]", " " & mywaiverinfo.WaiverInfo.LenderName)
            Replace("[LENDERADD1]", " " & mywaiverinfo.WaiverInfo.LenderAdd1)
            Replace("[LENDERADD2]", " " & mywaiverinfo.WaiverInfo.LenderAdd2)
            Replace("[LENDERCITY]", " " & mywaiverinfo.WaiverInfo.LenderCity)
            Replace("[LENDERSTATE]", " " & mywaiverinfo.WaiverInfo.LenderState)
            Replace("[LENDERZIP]", " " & mywaiverinfo.WaiverInfo.LenderZip)

            If mywaiverinfo.WaiverInfo.IsPaidInFull Then
                Replace("[PAYMENTAMT]", " ( Paid To Date ) ")
            Else
                ReplaceAmt("[PAYMENTAMT]", mywaiverinfo.WaiverInfo.PaymentAmt)
            End If
           ReplaceAmt("[DISPUTEDAMT]", mywaiverinfo.WaiverInfo.DisputedAmt)
            ReplaceDate("[PRINTDATE]", Today.ToShortDateString)
            ReplaceDate("[STARTDATE]", mywaiverinfo.WaiverInfo.StartDate)
            ReplaceDate("[THROUGHDATE]", mywaiverinfo.WaiverInfo.ThroughDate)
            Replace("[LABORTYPE]", " " & mywaiverinfo.ClientLienInfo.LaborType)
            Replace("[NOTE]", mywaiverinfo.WaiverInfo.WaiverNote)
            Replace("[WAIVERDATES]", mywaiverinfo.WaiverInfo.WaiverDates)
            Replace("[WAIVERPAYMENTS]", mywaiverinfo.WaiverInfo.WaiverPayments)
            Return True


        End Function
        Private Sub Replace(ByVal atag As String, ByVal avalue As String)
            Try
                m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, avalue)
            Catch
            End Try

        End Sub
        Private Sub ReplaceDate(ByVal atag As String, ByVal avalue As Object)
            Try
                If IsDate(avalue) Then
                    Dim mydate As String = Date.Parse(avalue).ToShortDateString
                    m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " " & mydate)
                Else
                    m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " ")
                End If

            Catch ex As Exception
                m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " ")
            End Try

        End Sub
        Private Sub ReplaceAmt(ByVal atag As String, ByVal avalue As Object)
            Try
                Dim myamt As String = Strings.FormatCurrency(avalue)
                m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, myamt)
            Catch
                m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " " & Strings.FormatCurrency(0))
            End Try

        End Sub


    End Class

End Namespace
