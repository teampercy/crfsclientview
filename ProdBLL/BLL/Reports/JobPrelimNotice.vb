Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Namespace Liens.Reports
    Public Class JobPrelimNotice
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As CurrentUser
        Dim myds As DataSet
        Dim mynoticeinfo As HDS.DAL.COMMON.TableView
        Dim myform As HDS.DAL.COMMON.TableView
        Dim mysproc As New CRFDB.SPROCS.uspbo_Jobs_GetNoticeInfo
        Public Sub New()
            MyBase.New()
            CurrentUser = Globals.GetCurrentUser
            Me.OutputFolder = CurrentUser.OutputFolder & "\"
            Me.SettingsFolder = CurrentUser.SettingsFolder & "\"
            Me.UserName = CurrentUser.UserName
            Me.ReportDefPath = Me.SettingsFolder & "Reports\PrelimNotice.xml"

        End Sub
        Public Function ExecuteC1Report(ByVal aitemid As String) As String
            mysproc.Id = aitemid
            myds = LienView.Provider.DAL.GetDataSet(mysproc)
            mynoticeinfo = New HDS.DAL.COMMON.TableView(myds.Tables(0))

            If mynoticeinfo.Count = 1 Then
                Me.ReportName = mynoticeinfo.RowItem("FormCode")
                Me.DataView = mynoticeinfo
                With Me
                    .FilePrefix = GetFileName("PRELIM-" & mynoticeinfo.RowItem("JobId") & "-" & Me.ReportName)
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Shared Function GetFileName(ByVal areportname As String) As String
            Dim myuser As CurrentUser = Globals.GetCurrentUser
            Dim myprefix As String = myuser.ClientCode
            myprefix += "-" & myuser.LoginCode
            myprefix += "-" & myuser.Id
            myprefix += "-" & areportname
            Return Globals.TrimSpace(myprefix)


        End Function
        Public Function ExecuteC1Report(ByVal mynoticeinfo As HDS.DAL.COMMON.TableView, ByVal myfileprefix As String) As String
            If mynoticeinfo.Count = 1 Then
                Me.ReportName = mynoticeinfo.RowItem("FormCode")
                Me.DataView = mynoticeinfo
                With Me
                    .FilePrefix = myfileprefix
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender
            Try
                If HasField("Signature1", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("Signature1")
                    ZIMAGE.Picture = GetSignature1()
                End If
            Catch ex As Exception
                Dim s As String = String.Empty

            End Try

            Try
                If HasField("Signature2", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("Signature2")
                    ZIMAGE.Picture = GetSignature2()
                End If

            Catch ex As Exception
                Dim s As String = String.Empty

            End Try

        End Sub
        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try

        End Function
        Public Function GetSignature1() As Object
            Dim s As String = Me.SettingsFolder & "Reports\scan1.jpg"
            If System.IO.File.Exists(s) Then
                Dim myimage As New HDS.WINLIB.Controls.ImageViewer
                myimage.PictureBox1.Load(s)
                Return myimage.PictureBox1.Image()
            Else
                Return Nothing
            End If


        End Function
        Public Function GetSignature2() As Object
            Dim s As String = Me.SettingsFolder & "Reports\scan1a.jpg"
            If System.IO.File.Exists(s) Then
                Dim myimage As New HDS.WINLIB.Controls.ImageViewer
                myimage.PictureBox1.Load(s)
                Return myimage.PictureBox1.Image()
            Else
                Return Nothing
            End If
        End Function


    End Class

End Namespace
