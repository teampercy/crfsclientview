Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Namespace ClientView.Lien
    Public Class NoJobWaiverInfo
        Dim myvw0 As DAL.COMMON.TableView
        Dim myvw1 As DAL.COMMON.TableView
        Dim myvw2 As DAL.COMMON.TableView
        Dim myvw3 As DAL.COMMON.TableView
        Dim myvw4 As DAL.COMMON.TableView
        Dim myvw5 As DAL.COMMON.TableView
        Dim myvw6 As DAL.COMMON.TableView
        Dim myvw7 As DAL.COMMON.TableView
        Dim myvw8 As DAL.COMMON.TableView
        Dim myvw9 As DAL.COMMON.TableView
        Dim myvw10 As DAL.COMMON.TableView
        Dim myvw11 As DAL.COMMON.TableView

        Dim mytable0 As New TABLES.BatchNoJobWaiverNotice
        Dim mytable1 As New TABLES.Client
        Dim mytable2 As New TABLES.ClientLienInfo
        Dim mytable3 As New TABLES.ClientImage
        Dim mytable4 As New TABLES.StateForms
        Dim mytable5 As New TABLES.ClientSigner
        Dim mytable6 As New DataSet
        Dim myimage As New HDS.WINLIB.Controls.ImageViewer

        Dim MYDS As DataSet
        Public Sub New(ByVal aitemid As Integer)
            Dim mysproc As New SPROCS.uspbo_ClientView_GetNoJobWaiverInfo
            mysproc.BATCHNOJOBWAIVERNOTICEId = aitemid
            MYDS = Provider.DBO.GetDataSet(mysproc)
            myvw0 = New DAL.COMMON.TableView(MYDS.Tables(0), "")
            myvw1 = New DAL.COMMON.TableView(MYDS.Tables(1), "")
            myvw2 = New DAL.COMMON.TableView(MYDS.Tables(2), "")
            myvw3 = New DAL.COMMON.TableView(MYDS.Tables(3), "")
            myvw4 = New DAL.COMMON.TableView(MYDS.Tables(4), "")
            myvw5 = New DAL.COMMON.TableView(MYDS.Tables(5), "")
            myvw6 = New DAL.COMMON.TableView(MYDS.Tables(6), "")

            myvw0.FillEntity(mytable0)
            myvw1.FillEntity(mytable1)
            myvw2.FillEntity(mytable2)
            myvw3.FillEntity(mytable3)
            myvw4.FillEntity(mytable4)
            myvw5.FillEntity(mytable5)
            mytable6.Tables.Add(myvw6.ToTable())

        End Sub
        Public Function WaiverInfo() As TABLES.BatchNoJobWaiverNotice
            Return mytable0
        End Function
        Public Function Client() As Client
            Return mytable1
        End Function
        Public Function ClientLienInfo() As ClientLienInfo
            Return mytable2
        End Function
        Public Function ClientImage() As TABLES.ClientImage
            Return mytable3
        End Function
        Public Function StateForm() As TABLES.StateForms
            Return mytable4
        End Function
        Public Function ClientSigner() As TABLES.ClientSigner
            Return mytable5
        End Function

        Public Function clientntoaddressInfo() As DataSet
            Return mytable6

        End Function
    End Class
End Namespace
