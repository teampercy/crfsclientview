﻿Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Namespace Liens.Reports
    Public Class JobWaiverNotary
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Dim mywaiverinfo As BLL.ClientView.Lien.JobWaiverInfo
        Dim myimage As New HDS.WINLIB.Controls.ImageViewer
        Public CurrentUser As CurrentUser
        Public Sub New(ByVal aitemid As Integer)
            MyBase.New()
            CurrentUser = Globals.GetCurrentUser
            mywaiverinfo = New BLL.ClientView.Lien.JobWaiverInfo(aitemid)
            Me.OutputFolder = CurrentUser.OutputFolder & "\"
            Me.SettingsFolder = CurrentUser.SettingsFolder & "\"
            Me.UserName = CurrentUser.UserName

        End Sub
        Public Function ExecuteC1Report() As String

            Me.ReportDefPath = Me.SettingsFolder & "\Reports\Waivers.xml"

            If mywaiverinfo.JobWaiverLog.Notary = True Then
                If mywaiverinfo.JobWaiverLog.NotaryType = 2 Then
                    Me.ReportName = "NotaryCA"
                Else
                    Me.ReportName = "Notary"
                End If

                mywaiverinfo.JobWaiverLog.DatePrinted = Date.Now
                LienView.Provider.DAL.Update(mywaiverinfo.JobWaiverLog)
                With Me
                    .FilePrefix = GetFileName("Notary-" & mywaiverinfo.StateForm.FormCode)
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            End If
        End Function

        Public Shared Function GetFileName(ByVal areportname As String) As String
            Dim myuser As CurrentUser = Globals.GetCurrentUser
            Dim myprefix As String = myuser.ClientCode
            myprefix += "-" & myuser.LoginCode
            myprefix += "-" & myuser.Id
            myprefix += "-" & areportname
            Return Globals.TrimSpace(myprefix)


        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender

CheckHeader:
            Try
                'Dim ZLOGO As Field = C1R.Fields("CLIENTLOGO")
                'If ZLOGO.Section = e.Section Then
                '    Dim ZADDR As Field = C1R.Fields("CLIENTADDRESS")
                '    ZLOGO.Picture = GetLogo()
                '    ZLOGO.PictureScale = PictureScaleEnum.Scale
                '    ZADDR.Calculated = True
                '    ZADDR.RTF = True
                '    ZADDR.Visible = True
                '    ZADDR.Value = GetAddress()

                '    If IsNothing(ZLOGO.Picture) = True Then
                '        ZLOGO.Calculated = True
                '        ZLOGO.Visible = True
                '    Else
                '        ZLOGO.Calculated = True
                '        ZLOGO.Visible = True
                '    End If

                'End If

                '11/29/2016 commented out temporarily
                Dim ZWAIVERLOGID As Field = C1R.Fields("JOBWAIVERLOGID")
                If ZWAIVERLOGID.Section = e.Section Then
                    ZWAIVERLOGID.Calculated = True
                    ZWAIVERLOGID.RTF = True
                    ZWAIVERLOGID.Visible = True
                    ZWAIVERLOGID.Value = GetJobWaiverLogId()

                End If

                '10/18/2017 added for barcode
                Dim ZWAIVERBARCODE As Field = C1R.Fields("JOBWAIVERLOGIDBARCODE")
                If ZWAIVERBARCODE.Section = e.Section Then
                    ZWAIVERBARCODE.Calculated = True
                    ZWAIVERBARCODE.RTF = True
                    ZWAIVERBARCODE.Visible = True
                    ZWAIVERBARCODE.Value = GetJobWaiverLogId()

                End If

                'Dim ZWAIVER As Field = C1R.Fields("WAIVERTEXT")
                'If ZWAIVER.Section = e.Section Then
                '    ZWAIVER.Calculated = True
                '    ZWAIVER.RTF = True
                '    ZWAIVER.Visible = True
                '    ZWAIVER.Value = GetWaiver()
                'End If

                'Dim ZSIGTEXT As Field = C1R.Fields("SIGNATURETEXT")
                'If ZSIGTEXT.Section = e.Section Then
                '    ZSIGTEXT.Calculated = True
                '    ZSIGTEXT.RTF = True
                '    ZSIGTEXT.Visible = True
                '    ZSIGTEXT.Value = GetSignature()
                'End If

                'If IsNothing(mywaiverinfo.ClientSigner.Signature) = False Then
                '    Dim ZSIGLOGO As Field = C1R.Fields("CLIENTSIGNATURELOGO")
                '    If ZSIGLOGO.Section = e.Section Then
                '        ZSIGLOGO.Picture = GetSignatureLogo()
                '        ZSIGLOGO.Visible = True
                '    End If
                'End If


                Dim ZNOTARY As Field = C1R.Fields("NOTARYTEXT")
                If ZNOTARY.Section = e.Section Then
                    If mywaiverinfo.JobWaiverLog.Notary = True Then
                        ZNOTARY.Visible = True
                        ZNOTARY.Calculated = True
                        ZNOTARY.Value = GetNotary()
                        ZNOTARY.RTF = True
                    Else
                        ZNOTARY.Visible = False
                        ZNOTARY.Value = ""
                    End If
                End If

            Catch EX As Exception
                Throw New ApplicationException(EX.Message)

            End Try


        End Sub

        Private Function GetFileName() As String
            Return mywaiverinfo.StateForm.FormCode & "-" & mywaiverinfo.Client.ClientCode & "-" & mywaiverinfo.vwJobInfo.JobId

        End Function
        Private Function GetJobWaiverLogId() As String
            m_rtf = New Windows.Forms.RichTextBox
            m_filename = Me.SettingsFolder & "Waivers\JobWaiverLogId\JobWaiverLogId.rtf"

            Dim s As String = ""
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    Try
                        Replace("[JOBWAIVERLOGID]", mywaiverinfo.JobWaiverLog.JobWaiverLogId)
                        Replace("[JOBID]", mywaiverinfo.JobWaiverLog.JobId)
                        s = m_rtf.Text
                    Catch ex As Exception
                        s = "Error Reading File:" & ex.Message
                        m_rtf.Text = s

                    End Try
                Else
                    s = "File Not Found :" & m_filename

                End If
            Catch ex As Exception
                s = "Error On File :" & ex.Message
                m_rtf.Text = s

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function GetSignatureLogo() As Object
            myimage.LoadImage(mywaiverinfo.ClientSigner.Signature)
            Return myimage.PictureBox1.Image()

        End Function
        Public Function GetSignature() As String
            m_rtf = New Windows.Forms.RichTextBox
            m_filename = Me.SettingsFolder & "Waivers\Signature\" & mywaiverinfo.StateForm.WaiverSigRTF

            Dim s As String = ""
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    Try
                        Replace("[CLIENTNAME]", mywaiverinfo.Client.ClientName)
                        Replace("[SIGNATORY]", mywaiverinfo.ClientSigner.Signer)
                        Replace("[SIGNATORYTITLE]", mywaiverinfo.ClientSigner.SignerTitle)
                        ReplaceDate("[TODAY]", mywaiverinfo.JobWaiverLog.DateCreated.ToShortDateString)
                        s = (mywaiverinfo.Client.AddressLine1.Trim & "," & mywaiverinfo.Client.City.Trim & " " & mywaiverinfo.Client.State.Trim)
                        Replace("[JOBID]", " " & mywaiverinfo.vwJobInfo.JobId)
                        Replace("[SIGNEREMAIL]", mywaiverinfo.ClientSigner.SignerEmail)
                        Replace("[CLIENTINFO]", S)
                        Replace("[CLIENTPHONE]", HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.ClientLienInfo.ContactPhone))
                        Replace("[JOBWAIVERLOGID]", " " & mywaiverinfo.JobWaiverLog.JobWaiverLogId)
                        S = m_rtf.Text
                    Catch ex As Exception
                        s = "Error Reading File:" & ex.Message
                        m_rtf.Text = s

                    End Try
                Else
                    s = "File Not Found :" & m_filename

                End If
            Catch ex As Exception
                S = "Error On File :" & ex.Message
                m_rtf.Text = s

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function GetLogo() As Object
            myimage.LoadImage(mywaiverinfo.ClientImage.ImageObject)
            Return myimage.PictureBox1.Image()

        End Function
        Public Function GetAddress() As String

            m_rtf = New Windows.Forms.RichTextBox
            Dim S As String = Me.SettingsFolder & "Waivers\Address\"
            If mywaiverinfo.ClientImage.WaiverFolder.Trim.Length > 1 Then
                If System.IO.Directory.Exists(S & mywaiverinfo.ClientImage.WaiverFolder & "\") Then
                    S += mywaiverinfo.ClientImage.WaiverFolder & "\"
                End If
            End If
            S += "WaiverAddress.rtf"
            m_filename = S
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                   'MyStream.Dispose()
                    Try
                        ReplaceAddressTokens()
                        S = m_rtf.Text
                    Catch ex As Exception
                        S = "Error Reading File:" & ex.Message
                        Throw New ApplicationException(ex.Message)

                    End Try
                Else
                    S = "File Not Found :" & m_filename
                    Throw New ApplicationException(S)

                End If
            Catch ex As Exception
                S = "ErrorOnFile"
                Throw New ApplicationException(ex.Message)

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function GetWaiver() As String
            m_rtf = New Windows.Forms.RichTextBox
            GetWaiverRTF()
            Dim S As String = m_filename
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    Try
                        ReplaceTokens()
                        S = m_rtf.Text
                    Catch ex As Exception
                        S = "Error Reading File:" & ex.Message
                        Throw New ApplicationException(ex.Message)

                    End Try
                Else
                    S = "File Not Found :" & m_filename
                End If
            Catch ex As Exception
                S = "Error On File :" & ex.Message
                Throw New ApplicationException(ex.Message)

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function GetWaiverRTF() As String
            Dim S As String = Me.SettingsFolder & "Waivers\Waiver\"
            If mywaiverinfo.ClientImage.WaiverFolder.Trim.Length > 1 Then
                If System.IO.Directory.Exists(S & mywaiverinfo.ClientImage.WaiverFolder & "\") Then
                    S += mywaiverinfo.ClientImage.WaiverFolder & "\"
                End If
            End If
            m_formfolder = S

            ' 7/2/2012 Added logic to handle updated waiver forms
            If mywaiverinfo.StateForm.IsWaiverUpdated = True And mywaiverinfo.JobWaiverLog.DateCreated < mywaiverinfo.StateForm.WaiverUpdateDate Then
                m_filename = m_formfolder & mywaiverinfo.StateForm.FormCode & mywaiverinfo.Client.ClientCode & "Archive.RTF"
            Else
                m_filename = m_formfolder & mywaiverinfo.StateForm.FormCode & mywaiverinfo.Client.ClientCode & ".RTF"
            End If

            If System.IO.File.Exists(m_filename) = False Then
                ' 7/2/2012 Added logic to handle updated waiver forms
                If mywaiverinfo.StateForm.IsWaiverUpdated = True And mywaiverinfo.JobWaiverLog.DateCreated < mywaiverinfo.StateForm.WaiverUpdateDate Then
                    m_filename = m_formfolder & mywaiverinfo.StateForm.FormCode & "Archive.RTF"
                Else
                    m_filename = m_formfolder & mywaiverinfo.StateForm.FormCode & ".RTF"
                End If

                If System.IO.File.Exists(m_filename) = False Then
                    m_filename = m_formfolder & "MISC" & mywaiverinfo.StateForm.FormCode & mywaiverinfo.Client.ClientCode & ".RTF"
                    If System.IO.File.Exists(m_filename) = False Then
                        m_filename = m_formfolder & "MISC" & mywaiverinfo.StateForm.FormCode & ".RTF"
                    End If
                End If
            Else
                m_filename += ".NTF"
            End If

            Return m_filename

        End Function
        Public Function GetNotary() As String
            Dim S As String = Me.SettingsFolder & "Waivers\Notary\"
            If mywaiverinfo.ClientImage.WaiverFolder.Trim.Length > 1 Then
                If System.IO.Directory.Exists(S & mywaiverinfo.ClientImage.WaiverFolder & "\") Then
                    S += mywaiverinfo.ClientImage.WaiverFolder & "\"
                End If
            End If
            m_filename = S & "Notary.rtf"

            m_rtf = New Windows.Forms.RichTextBox
            Try
                If System.IO.File.Exists(m_filename) = True Then
                    Dim MyStream As New IO.FileStream(m_filename, IO.FileMode.Open)
                    Dim myreader As New IO.StreamReader(MyStream)
                    m_rtf.Rtf = myreader.ReadToEnd
                    MyStream.Close()
                    Try
                        ReplaceTokens()
                        S = m_rtf.Text
                    Catch ex As Exception
                        S = "Error Reading File:" & ex.Message
                        Throw New ApplicationException(ex.Message)

                    End Try
                Else
                    S = "File Not Found :" & m_filename
                End If
            Catch ex As Exception
                S = "Error On File :" & ex.Message
                Throw New ApplicationException(ex.Message)

            End Try

            Return m_rtf.Rtf

        End Function
        Public Function ReplaceAddressTokens() As Boolean
            Dim m_addressline1 As String = mywaiverinfo.Client.ClientName
            Dim m_addressline2 As String = mywaiverinfo.Client.AddressLine1
            Dim m_addressline3 As String = mywaiverinfo.Client.City & ", " & mywaiverinfo.Client.State & " " & mywaiverinfo.Client.PostalCode
            Dim m_addressphone As String = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.Client.PhoneNo)
            Dim m_addressfax As String = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.Client.Fax)
            If mywaiverinfo.ClientImage.WaiverAddress1.Length > 0 Then
                m_addressline2 = mywaiverinfo.ClientImage.WaiverAddress1
                m_addressline3 = mywaiverinfo.ClientImage.WaiverAddress2
                m_addressphone = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.ClientImage.WaiverPhone)
                m_addressfax = HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.ClientImage.WaiverFax)
            End If

            Replace("[ClientName]", m_addressline1)
            Replace("[WaiverAddress1]", m_addressline2)
            Replace("[WaiverAddress2]", m_addressline3)
            Replace("[WaiverPhone]", m_addressphone)
            Replace("[WaiverFax]", m_addressfax)

        End Function
        Public Function ReplaceTokens() As Boolean
            Dim m_checkissuer As String = ""
            Dim m_checkpayable As String = ""

            Dim m_addressline As String = ""
            Dim m_addressline1 As String = ""
            Dim m_addressline2 As String = ""
            Dim m_addressline3 As String = ""

            Dim MYJOBINFO As String = ""
            Dim MYJOBINFO1 As String = ""
            Dim MYJOBINFO2 As String = ""
            Dim MYJOBINFO3 As String = ""
            Dim MYJOBINFO4 As String = ""
            Dim MYJOBINFO5 As String = ""

            MYJOBINFO = mywaiverinfo.vwJobInfo.JobName & " " & mywaiverinfo.vwJobInfo.JobNum
            MYJOBINFO1 = mywaiverinfo.vwJobInfo.JobName & " " & mywaiverinfo.vwJobInfo.JobNum
            MYJOBINFO2 = mywaiverinfo.vwJobInfo.JobAdd1
            MYJOBINFO3 = mywaiverinfo.vwJobInfo.JobAdd2
            MYJOBINFO4 = mywaiverinfo.vwJobInfo.JobCity & ", " & mywaiverinfo.vwJobInfo.JobState & " " & mywaiverinfo.vwJobInfo.JobZip
            MYJOBINFO5 = mywaiverinfo.vwJobInfo.JobName & ", " & mywaiverinfo.vwJobInfo.PONum

            If MYJOBINFO3.Length < 1 Then
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If
            If MYJOBINFO2.Length < 1 Then
                MYJOBINFO2 = MYJOBINFO3
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If
            If MYJOBINFO1.Length < 1 Then
                MYJOBINFO1 = MYJOBINFO2
                MYJOBINFO2 = MYJOBINFO3
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If

            If MYJOBINFO5.Length < 1 Then
                MYJOBINFO5 = MYJOBINFO2
                MYJOBINFO2 = MYJOBINFO3
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If

            m_checkpayable = mywaiverinfo.ClientCustomer.ClientCustomer
            m_addressline1 = mywaiverinfo.ClientCustomer.ClientCustomer
            m_addressline2 = mywaiverinfo.ClientCustomer.AddressLine1
            m_addressline3 = mywaiverinfo.ClientCustomer.City & ", " & mywaiverinfo.ClientCustomer.State & " " & mywaiverinfo.ClientCustomer.PostalCode

            If mywaiverinfo.JobWaiverLog.MailtoGC = True Then
                m_checkpayable = mywaiverinfo.GeneralContractor.GeneralContractor
                m_addressline1 = mywaiverinfo.GeneralContractor.GeneralContractor
                m_addressline2 = mywaiverinfo.GeneralContractor.AddressLine1
                m_addressline3 = mywaiverinfo.GeneralContractor.City & ", " & mywaiverinfo.GeneralContractor.State & " " & mywaiverinfo.GeneralContractor.PostalCode
            End If

            If mywaiverinfo.JobWaiverLog.MailtoLE = True Then
                m_checkpayable = mywaiverinfo.JobLegalParties_Lender.AddressName
                m_addressline1 = mywaiverinfo.JobLegalParties_Lender.AddressName
                m_addressline2 = mywaiverinfo.JobLegalParties_Lender.AddressLine1
                m_addressline3 = mywaiverinfo.JobLegalParties_Lender.City & ", " & mywaiverinfo.JobLegalParties_Lender.State & " " & mywaiverinfo.JobLegalParties_Lender.PostalCode
            End If

            If mywaiverinfo.JobWaiverLog.MailtoOW = True Then
                m_checkpayable = mywaiverinfo.JobLegalParties_Owner.AddressName
                m_addressline1 = mywaiverinfo.JobLegalParties_Owner.AddressName
                m_addressline2 = mywaiverinfo.JobLegalParties_Owner.AddressLine1
                m_addressline3 = mywaiverinfo.JobLegalParties_Owner.City & ", " & mywaiverinfo.JobLegalParties_Owner.State & " " & mywaiverinfo.JobLegalParties_Owner.PostalCode
            End If

            If mywaiverinfo.JobWaiverLog.JointCheckAgreement = True Then
                m_checkpayable = mywaiverinfo.Client.ClientName & " and " & mywaiverinfo.ClientCustomer.ClientCustomer
                'm_checkpayable = mywaiverinfo.Client.ClientName & " & " & mywaiverinfo.ClientCustomer.ClientCustomer
            Else
                m_checkpayable = mywaiverinfo.Client.ClientName
            End If

            If m_addressline2.Length < 1 Then
                m_addressline2 = m_addressline3
                m_addressline3 = ""
            End If

            m_checkissuer = mywaiverinfo.ClientCustomer.ClientCustomer
            If mywaiverinfo.JobWaiverLog.CheckByGC = True Then
                m_checkissuer = mywaiverinfo.GeneralContractor.GeneralContractor
            End If
            If mywaiverinfo.JobWaiverLog.CheckbyLE = True Then
                m_checkissuer = mywaiverinfo.JobLegalParties_Lender.AddressName
            End If
            If mywaiverinfo.JobWaiverLog.CheckbyOW = True Then
                m_checkissuer = mywaiverinfo.JobLegalParties_Owner.AddressName
            End If
            If mywaiverinfo.JobWaiverLog.CheckByOT = True Then
                m_checkissuer = mywaiverinfo.JobWaiverLog.OtherChkIssuerName
            End If

            Replace("[WAIVERCODE]", mywaiverinfo.StateForm.NoticeCode)
            Replace("[JOBINFO]", MYJOBINFO)
            Replace("[JOBINFO1]", MYJOBINFO1)
            Replace("[JOBINFO2]", MYJOBINFO2)
            Replace("[JOBINFO3]", MYJOBINFO3)
            Replace("[JOBINFO4]", MYJOBINFO4)
            Replace("[JOBINFO5]", MYJOBINFO5)

            Replace("[MAILINFO1]", m_addressline1)
            Replace("[MAILINFO2]", m_addressline2)
            Replace("[MAILINFO3]", m_addressline3)
            Replace("[MAILINFO4]", "")

            Replace("[CHECKISSUER]", m_checkissuer)
            Replace("[PAYABLETO]", m_checkpayable)

            Replace("[CLIENTNAME]", mywaiverinfo.Client.ClientName)

            Dim S As String = mywaiverinfo.Client.AddressLine1.Trim & "," & mywaiverinfo.Client.City.Trim & " " & mywaiverinfo.Client.State.Trim

            Replace("[CLIENTINFO]", S)
            Replace("[CLIENTPHONE]", HDS.WINLIB.Common.Format.PhoneNo(mywaiverinfo.ClientLienInfo.ContactPhone))

            Replace("[SIGNATORY]", mywaiverinfo.ClientSigner.Signer)
            Replace("[SIGNATORYTITLE]", mywaiverinfo.ClientSigner.SignerTitle)
            Replace("[SIGNEREMAIL]", mywaiverinfo.ClientSigner.SignerEmail)

            Replace("[JOBNAME]", " " & mywaiverinfo.vwJobInfo.JobName)
            Replace("[JOBINFO]", " " & MYJOBINFO)
            Replace("[JOBADDR1]", " " & mywaiverinfo.vwJobInfo.JobAdd1)
            Replace("[JOBADDR2]", " " & mywaiverinfo.vwJobInfo.JobAdd1)
            Replace("[JOBCITY]", " " & mywaiverinfo.vwJobInfo.JobCity)
            Replace("[JOBSTATE]", " " & mywaiverinfo.vwJobInfo.JobState)
            Replace("[JOBZIP]", " " & mywaiverinfo.vwJobInfo.JobZip)
            Replace("[JOBNUM]", " " & mywaiverinfo.vwJobInfo.JobNum)
            Replace("[JOBID]", " " & mywaiverinfo.vwJobInfo.JobId)
            Replace("[INVPAYMENTAPPNUM]", " " & mywaiverinfo.JobWaiverLog.InvoicePaymentNo)
            Replace("[JOBCOUNTY]", " " & mywaiverinfo.vwJobInfo.JobCounty)

            Replace("[CUSTOMERNAME]", " " & mywaiverinfo.ClientCustomer.ClientCustomer)
            Replace("[CUSTADD1]", " " & mywaiverinfo.ClientCustomer.AddressLine1)
            Replace("[CUSTADD2]", " " & mywaiverinfo.ClientCustomer.AddressLine2)
            Replace("[CUSTCITY]", " " & mywaiverinfo.ClientCustomer.City)
            Replace("[CUSTCONTACT]", " " & mywaiverinfo.ClientCustomer.ContactName)
            Replace("[CUSTREFNUM]", " " & mywaiverinfo.ClientCustomer.RefNum)
            Replace("[CUSTSTATE]", " " & mywaiverinfo.ClientCustomer.State)
            Replace("[CUSTZIP]", " " & mywaiverinfo.ClientCustomer.PostalCode)

            Replace("[GENERALCONTRACTOR]", " " & mywaiverinfo.GeneralContractor.GeneralContractor)
            Replace("[GCADD1]", " " & mywaiverinfo.GeneralContractor.AddressLine1)
            Replace("[GCADD2]", " " & mywaiverinfo.GeneralContractor.AddressLine2)
            Replace("[GCCITY]", " " & mywaiverinfo.GeneralContractor.City)
            Replace("[GCNUM]", " " & mywaiverinfo.GeneralContractor.RefNum)
            Replace("[GCSTATE]", " " & mywaiverinfo.GeneralContractor.State)
            Replace("[GCZIP]", " " & mywaiverinfo.GeneralContractor.PostalCode)

            Replace("[OWNERNAMES]", " " & mywaiverinfo.OtherOwners)

            Replace("[OWNERNAME]", " " & mywaiverinfo.JobLegalParties_Owner.AddressName)
            Replace("[OWNERADD1]", " " & mywaiverinfo.JobLegalParties_Owner.AddressLine1)
            Replace("[OWNERADD2]", " " & mywaiverinfo.JobLegalParties_Owner.AddressLine2)
            Replace("[OWNERCITY]", " " & mywaiverinfo.JobLegalParties_Owner.City)
            Replace("[OWNERNUM]", " " & mywaiverinfo.JobLegalParties_Owner.RefNum)
            Replace("[OWNERSTATE]", " " & mywaiverinfo.JobLegalParties_Owner.State)
            Replace("[OWNERZIP]", " " & mywaiverinfo.JobLegalParties_Owner.PostalCode)

            Replace("[LENDERNAME]", " " & mywaiverinfo.JobLegalParties_Lender.AddressName)
            Replace("[LENDERADD1]", " " & mywaiverinfo.JobLegalParties_Lender.AddressLine1)
            Replace("[LENDERADD2]", " " & mywaiverinfo.JobLegalParties_Lender.AddressLine2)
            Replace("[LENDERCITY]", " " & mywaiverinfo.JobLegalParties_Lender.City)
            Replace("[LENDERNUM]", " " & mywaiverinfo.JobLegalParties_Lender.RefNum)
            Replace("[LENDERSTATE]", " " & mywaiverinfo.JobLegalParties_Lender.State)
            Replace("[LENDERZIP]", " " & mywaiverinfo.JobLegalParties_Lender.PostalCode)

            If mywaiverinfo.JobWaiverLog.IsPaidInFull Then
                Replace("[PAYMENTAMT]", " ( Paid To Date ) ")
            Else
                ReplaceAmt("[PAYMENTAMT]", mywaiverinfo.JobWaiverLog.PaymentAmt)
            End If

            ReplaceAmt("[DISPUTEDAMT]", mywaiverinfo.JobWaiverLog.DisputedAmt)
            ReplaceDate("[PRINTDATE]", mywaiverinfo.JobWaiverLog.DateCreated.ToShortDateString)
            ReplaceDate("[STARTDATE]", mywaiverinfo.JobWaiverLog.StartDate)
            ReplaceDate("[THROUGHDATE]", mywaiverinfo.JobWaiverLog.ThroughDate)
            Replace("[LABORTYPE]", " " & mywaiverinfo.ClientLienInfo.LaborType)
            Replace("[TODAY]", Today.ToShortDateString)
            Replace("[NOTE]", mywaiverinfo.JobWaiverLog.WaiverNote)
            Replace("[WAIVERDATES]", mywaiverinfo.JobWaiverLog.WaiverDates)
            Replace("[WAIVERPAYMENTS]", mywaiverinfo.JobWaiverLog.WaiverPayments)

            Return True


        End Function
        Private Sub Replace(ByVal atag As String, ByVal avalue As String)
            Try
                If mywaiverinfo.StateForm.StateCode = "GA" Then
                    m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, UCase(avalue))
                Else
                    m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, avalue)
                End If
            Catch
            End Try

        End Sub
        Private Sub ReplaceDate(ByVal atag As String, ByVal avalue As Object)
            Try
                If IsDate(avalue) Then
                    Dim mydate As String = Date.Parse(avalue).ToShortDateString
                    m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " " & mydate)
                Else
                    m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " ")
                End If

            Catch ex As Exception
                m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " ")
            End Try

        End Sub
        Private Sub ReplaceAmt(ByVal atag As String, ByVal avalue As Object)
            Try
                Dim myamt As String = Strings.FormatCurrency(avalue)
                m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, myamt)
            Catch
                m_rtf.Rtf = Strings.Replace(m_rtf.Rtf, atag, " " & Strings.FormatCurrency(0))
            End Try

        End Sub
    End Class
End Namespace
