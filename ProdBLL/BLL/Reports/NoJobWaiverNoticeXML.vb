﻿Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Imports HDS.Reporting.C1Reports
Namespace Liens.Reports
    Public Class NoJobWaiverNoticeXML
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As CurrentUser
        Dim myimage As New HDS.WINLIB.Controls.ImageViewer
        Dim flexfieldtext As String
        Dim mywaiverinfo As BLL.ClientView.Lien.NoJobWaiverInfo
        Dim dummyTable As HDS.DAL.COMMON.TableView
        Dim myview As DAL.COMMON.TableView

        Public Sub New(ByVal aitemid As Integer)
            MyBase.New()
            CurrentUser = Globals.GetCurrentUser
            mywaiverinfo = New BLL.ClientView.Lien.NoJobWaiverInfo(aitemid)
            Me.OutputFolder = CurrentUser.OutputFolder & "\"
            Me.SettingsFolder = CurrentUser.SettingsFolder & "\"
            Me.UserName = CurrentUser.UserName
        End Sub

        Public Function ExecuteC1Report(ByVal MYDS As DataSet) As String
            dummyTable = New HDS.DAL.COMMON.TableView(MYDS.Tables(0))
            Me.ReportDefPath = GetReportFile()
            Me.ReportName = GetReportName()
            'The DataView property used only for to retrived Detail Section from Flex report. We not used any value from Table(0).
            Me.DataView = dummyTable
            ''
            mywaiverinfo.WaiverInfo.DatePrinted = Date.Now
            LienView.Provider.DAL.Update(mywaiverinfo.WaiverInfo)
            With Me
                .FilePrefix = GetFileName("WAIVER-" & mywaiverinfo.StateForm.FormCode)
                .TableIndex = 0
                .RenderReport()
                .ReportPath = .ReportFileName
                Return .ReportFileName

            End With

        End Function

        Public Shared Function GetFileName(ByVal areportname As String) As String
            Dim myuser As CurrentUser = Globals.GetCurrentUser
            Dim myprefix As String = myuser.ClientCode
            myprefix += "-" & myuser.LoginCode
            myprefix += "-" & myuser.Id
            myprefix += "-" & areportname
            Return Globals.TrimSpace(myprefix)
        End Function

        Public Function ExecuteC1Report(ByVal aitemid As String) As String
            Dim mysproc As New SPROCS.uspbo_ClientView_GetNoJobWaiverInfo
            mysproc.BATCHNOJOBWAIVERNOTICEId = aitemid
            Return ExecuteC1Report(CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetDataSet(mysproc))
        End Function
        Public Function ExecuteC1Report1(ByVal aitemid As String) As String
            Dim mysproc As New SPROCS.uspbo_ClientView_GetNoJobWaiverInfo
            mysproc.BATCHNOJOBWAIVERNOTICEId = aitemid
            Return ExecuteC1Report2(CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetDataSet(mysproc))

        End Function
        'vv *** Created By Pooja 27/08/2020 *** vv
        Public Function ExecuteC1Report2(ByVal MYDS As DataSet) As String
            dummyTable = New HDS.DAL.COMMON.TableView(MYDS.Tables(0))
            Me.ReportDefPath = Me.SettingsFolder & "\Reports\Waivers.FLXR"
            'The DataView property used only for to retrived Detail Section from Flex report. We not used any value from Table(0).
            Me.DataView = dummyTable

            If mywaiverinfo.WaiverInfo.Notary = True Then
                If mywaiverinfo.WaiverInfo.NotaryType = 2 Then
                    Me.ReportName = "NotaryCA"
                Else
                    Me.ReportName = "Notary"
                End If

                mywaiverinfo.WaiverInfo.DatePrinted = Date.Now
                LienView.Provider.DAL.Update(mywaiverinfo.WaiverInfo)
                With Me
                    .FilePrefix = GetFileName("Notary-" & mywaiverinfo.StateForm.FormCode)
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            End If
        End Function

        Public Function GetReportName() As String
            Dim reportName As String = ""
            reportName = mywaiverinfo.StateForm.FormCode
            If reportName = "COFP" Or reportName = "COPP" Or reportName = "UCFP" Or reportName = "UCPP" Then
                reportName = "MISC" & reportName
            End If
            Return reportName
        End Function

        Public Function GetReportFile() As String
            Dim S As String = Me.SettingsFolder & "Reports\"
            Dim rptName As String
            Dim folderName As String
            folderName = mywaiverinfo.ClientImage.WaiverFolder
            rptName = mywaiverinfo.StateForm.FormCode

            If folderName.Trim.Length > 1 Then
                If System.IO.File.Exists(S & "Waivers (" & folderName & ").FLXR") Then
                    If (Me.ReportExists(S & "Waivers (" & folderName & ").FLXR", rptName)) Then
                        S = S & "Waivers (" & folderName & ").FLXR"
                    Else
                        S = S & "Waivers.FLXR"
                    End If
                Else
                    S = S & "Waivers.FLXR"
                End If
            Else
                S = S & "Waivers.FLXR"
            End If

            Return S
        End Function

        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender

CheckHeader:
            Try
                If HasField("CLIENTLOGO", e) = True Then
                    Dim ZLOGO As Field = C1R.Fields("CLIENTLOGO")
                    If ZLOGO.Section = e.Section Then
                        ZLOGO.Picture = GetLogo()
                        ZLOGO.PictureScale = PictureScaleEnum.Scale
                        If IsNothing(ZLOGO.Picture) = True Then
                            ZLOGO.Calculated = True
                            ZLOGO.Visible = True
                        Else
                            ZLOGO.Calculated = True
                            ZLOGO.Visible = True
                        End If

                    End If
                End If

                If HasField("CLIENTADDRESS", e) = True Then
                    Dim ZADDR As Field = C1R.Fields("CLIENTADDRESS")
                    If ZADDR.Section = e.Section Then
                        ZADDR.Calculated = True
                        'ZADDR.RTF = True
                        ZADDR.Visible = True
                        'ZADDR.Font.Bold = True
                        ZADDR.Value = GetAddress(ZADDR.Value)
                    End If
                End If

                If HasField("WAIVERADDRESS", e) = True Then
                    Dim ZWAIVERADDRESS As Field = C1R.Fields("WAIVERADDRESS")
                    If ZWAIVERADDRESS.Section = e.Section Then
                        ZWAIVERADDRESS.Calculated = True
                        'ZWAIVERADDRESS.RTF = True
                        ZWAIVERADDRESS.Visible = True
                        ZWAIVERADDRESS.Value = GetWaiverReportDetails(ZWAIVERADDRESS.Value)
                    End If
                End If

                'To get Report Heading
                If HasField("FIELD4", e) = True Then
                    Dim ZWAIVERHEADING As Field = C1R.Fields("FIELD4")
                    If ZWAIVERHEADING.Section = e.Section Then
                        ZWAIVERHEADING.Calculated = True
                        ZWAIVERHEADING.Visible = True
                        ZWAIVERHEADING.Value = GetWaiverHeading(ZWAIVERHEADING.Value)
                    End If
                End If

                'To get Report Heading- Report:GANP
                If HasField("SIGNATURETEXT1", e) = True Then
                    Dim ZWAIVERHEADING1 As Field = C1R.Fields("SIGNATURETEXT1")
                    If ZWAIVERHEADING1.Section = e.Section Then
                        ZWAIVERHEADING1.Calculated = True
                        ZWAIVERHEADING1.Visible = True
                        ZWAIVERHEADING1.Value = GetWaiverHeading(ZWAIVERHEADING1.Value)
                    End If
                End If

                'To get Report Details
                If HasField("FIELD2", e) = True Then
                    Dim ZWAIVERREPORTDETAILLS As Field = C1R.Fields("FIELD2")
                    If ZWAIVERREPORTDETAILLS.Section = e.Section Then
                        ZWAIVERREPORTDETAILLS.Calculated = True
                        ZWAIVERREPORTDETAILLS.Visible = True
                        ZWAIVERREPORTDETAILLS.CanGrow = True
                        ZWAIVERREPORTDETAILLS.CanShrink = True
                        ZWAIVERREPORTDETAILLS.Value = GetWaiverReportDetails(ZWAIVERREPORTDETAILLS.Value)
                    End If
                End If

                If HasField("FIELD3", e) = True Then
                    Dim ZCATEXT As Field = C1R.Fields("FIELD2")
                    If ZCATEXT.Section = e.Section Then
                        ZCATEXT.BorderColor = Drawing.Color.Black
                        ZCATEXT.BorderStyle = BorderStyleEnum.Solid
                        ZCATEXT.LineWidth = 15
                        ZCATEXT.MarginLeft = 50
                        ZCATEXT.MarginRight = 50
                    End If
                End If

                If HasField("JOBWAIVERLOGID", e) = True Then
                    Dim ZWAIVERLOGID As Field = C1R.Fields("JOBWAIVERLOGID")
                    If ZWAIVERLOGID.Section = e.Section Then
                        ZWAIVERLOGID.Calculated = True
                        ZWAIVERLOGID.Visible = False
                        ZWAIVERLOGID.Value = GetJobWaiverLogId(ZWAIVERLOGID.Value)
                    End If
                End If

                If HasField("JOBWAIVERLOGIDBARCODE", e) = True Then
                    Dim ZWAIVERBARCODE As Field = C1R.Fields("JOBWAIVERLOGIDBARCODE")
                    If ZWAIVERBARCODE.Section = e.Section Then
                        ZWAIVERBARCODE.Calculated = True
                        'ZWAIVERBARCODE.RTF = True
                        ZWAIVERBARCODE.Visible = True
                        ZWAIVERBARCODE.BarCode = BarCodeEnum.Code_128auto
                        ZWAIVERBARCODE.Value = GetJobWaiverLogId(ZWAIVERBARCODE.Value)
                    End If
                End If

                'To get Report Details
                If HasField("WAIVERTEXT", e) = True Then
                    Dim ZWAIVERREPORTDETAILLS1 As Field = C1R.Fields("WAIVERTEXT")
                    If ZWAIVERREPORTDETAILLS1.Section = e.Section Then
                        ZWAIVERREPORTDETAILLS1.Calculated = True
                        ZWAIVERREPORTDETAILLS1.Visible = True
                        ZWAIVERREPORTDETAILLS1.CanGrow = True
                        ZWAIVERREPORTDETAILLS1.CanShrink = True
                        ZWAIVERREPORTDETAILLS1.Value = GetWaiverReportDetails(ZWAIVERREPORTDETAILLS1.Value)
                    End If
                End If

                If HasField("SIGNATURETEXT", e) = True Then
                    Dim ZSIGTEXT As Field = C1R.Fields("SIGNATURETEXT")
                    If ZSIGTEXT.Section = e.Section Then
                        ZSIGTEXT.Calculated = True
                        ZSIGTEXT.Visible = True
                        ZSIGTEXT.Value = GetSignature(ZSIGTEXT.Value)
                    End If
                End If

                If HasField("CLIENTSIGNATURELOGO", e) = True Then
                    If IsNothing(mywaiverinfo.ClientSigner.Signature) = False Then
                        Dim ZSIGLOGO As Field = C1R.Fields("CLIENTSIGNATURELOGO")
                        If ZSIGLOGO.Section = e.Section Then
                            ZSIGLOGO.Picture = GetSignatureLogo()
                            ZSIGLOGO.Visible = True
                        End If
                    End If
                End If

                If HasField("NOTARYTEXT", e) = True Then
                    Dim ZNOTARY As Field = C1R.Fields("NOTARYTEXT")
                    If ZNOTARY.Section = e.Section Then
                        If mywaiverinfo.WaiverInfo.Notary = True Then
                            ZNOTARY.Visible = True
                            ZNOTARY.Calculated = True
                            ZNOTARY.Value = GetNotary(ZNOTARY.Value)
                        Else
                            ZNOTARY.Visible = False
                            ZNOTARY.Value = ""
                        End If
                    End If
                End If

                If HasField("CLIENTSIGNATURELOGO1", e) = True Then
                    If IsNothing(mywaiverinfo.ClientSigner.Signature) = False Then
                        Dim ZSIGLOGO As Field = C1R.Fields("CLIENTSIGNATURELOGO1")
                        If ZSIGLOGO.Section = e.Section Then
                            ZSIGLOGO.Picture = GetSignatureLogo()
                            ZSIGLOGO.Visible = True
                        End If
                    End If
                End If

            Catch EX As Exception
                Throw New ApplicationException(EX.Message)
            End Try

        End Sub
        Private Function GetFileName() As String
            Return mywaiverinfo.StateForm.FormCode & "-" & mywaiverinfo.Client.ClientCode & "-" & mywaiverinfo.WaiverInfo.BatchNoJobWaiverId

        End Function

        Public Function GetAddress(address) As String
            flexfieldtext = address
            ReplaceAddressTokens()
            Return flexfieldtext
        End Function

        Public Function GetWaiverHeading(waiverheading) As String
            flexfieldtext = waiverheading
            ReplaceWaiverHeadingTokens()
            Return flexfieldtext
        End Function

        Public Function GetWaiverReportDetails(waiverreportdetails) As String
            flexfieldtext = waiverreportdetails
            ReplaceWaiverReportDetailTokens()
            Return flexfieldtext
        End Function

        Public Function GetSignatureLogo() As Object
            myimage.LoadImage(mywaiverinfo.ClientSigner.Signature)
            Return myimage.PictureBox1.Image()
        End Function

        Public Function GetSignature(signaturetext) As String
            flexfieldtext = signaturetext
            ReplaceSignatureTextTokens()
            Return flexfieldtext
        End Function

        Public Function GetNotary(notarytext) As String
            flexfieldtext = notarytext
            ReplaceNotaryTextTokens()
            Return flexfieldtext
        End Function

        Public Function GetJobWaiverLogId(JobWaiverLogId) As String
            flexfieldtext = JobWaiverLogId
            ReplaceJobWaiverLogIdTokens()
            Return flexfieldtext
        End Function

        Public Function GetLogo() As Object
            myimage.LoadImage(mywaiverinfo.ClientImage.ImageObject)
            Return myimage.PictureBox1.Image()

        End Function

        Public Function ReplaceAddressTokens() As Boolean

            Dim m_addressline1 As String
            Dim m_addressline2 As String
            Dim m_addressline3 As String
            Dim m_addressphone As String
            Dim m_addressfax As String
            Dim MYDS As DataSet = mywaiverinfo.clientntoaddressInfo
            myview = New DAL.COMMON.TableView(MYDS.Tables(0), "")
            '^^*** Created by pooja 05/21/2021*** ^^***  *** ^^
            If mywaiverinfo.ClientLienInfo.UseBranchNTO = True Then
                'Dim s As String = Session("JobNoticeLogId")
                ' Dim mysproc As New SPROCS.uspbo_ClientView_GetClientNTOAddressInfo
                'mysproc.ID = mywaiverinfo.JobWaiverLog.JobWaiverLogId
                'Dim myview As DAL.COMMON.TableView
                ' Dim MYDS As DataSet = mywaiverinfo.clientntoaddressInfo
                'Dim MYDS As DataSet = mywaiverinfo.clientntoaddressInfo
                myview = New DAL.COMMON.TableView(MYDS.Tables(0), "")
                'myview = New DAL.COMMON.TableView(MYDS.Tables(0), "")
                'myview = New DAL.COMMON.TableView(MYDS.Tables(0), "")
                m_addressline1 = myview.RowItem("NTOName")
                m_addressline2 = myview.RowItem("NTOAdd1")
                m_addressline3 = myview.RowItem("NTOCity") & ", " & myview.RowItem("NTOState") & " " & myview.RowItem("NTOZip")
                m_addressphone = HDS.WINLIB.COMMON.Format.PhoneNo(myview.RowItem("NTOPhone"))
                m_addressfax = HDS.WINLIB.COMMON.Format.PhoneNo(myview.RowItem("NTOFax"))

                ' Replace("[NTOName]", m_addressline1)
                ' Dim MYDS1 As DataSet = mywaiverinfo.JobWaiverLog1
                'myview = New DAL.COMMON.TableView(MYDS1.Tables(0), "")
            Else
                m_addressline1 = mywaiverinfo.Client.ClientName
                m_addressline2 = mywaiverinfo.Client.AddressLine1
                m_addressline3 = mywaiverinfo.Client.City & ", " & mywaiverinfo.Client.State & " " & mywaiverinfo.Client.PostalCode
                m_addressphone = HDS.WINLIB.COMMON.Format.PhoneNo(mywaiverinfo.Client.PhoneNo)
                m_addressfax = HDS.WINLIB.COMMON.Format.PhoneNo(mywaiverinfo.Client.Fax)

            End If

            If mywaiverinfo.ClientImage.WaiverAddress1.Length > 0 Then
                m_addressline2 = mywaiverinfo.ClientImage.WaiverAddress1
                m_addressline3 = mywaiverinfo.ClientImage.WaiverAddress2
                m_addressphone = HDS.WINLIB.COMMON.Format.PhoneNo(mywaiverinfo.ClientImage.WaiverPhone)
                m_addressfax = HDS.WINLIB.COMMON.Format.PhoneNo(mywaiverinfo.ClientImage.WaiverFax)
            End If

            Replace("[NTOName]", myview.RowItem("NTOName"))
            Replace("[ClientName]", mywaiverinfo.Client.ClientName)
            Replace("[WaiverAddress1]", m_addressline2)
            Replace("[WaiverAddress2]", m_addressline3)
            Replace("[WaiverPhone]", m_addressphone)
            Replace("[WaiverFax]", m_addressfax)

        End Function

        Public Function ReplaceWaiverHeadingTokens() As Boolean
            Replace("[WAIVERCODE]", mywaiverinfo.StateForm.NoticeCode)
            Replace("[JOBCOUNTY]", mywaiverinfo.WaiverInfo.JobCounty)
        End Function

        Public Function ReplaceWaiverReportDetailTokens() As Boolean
            Dim m_checkissuer As String = ""
            Dim m_checkpayable As String = ""

            Dim MYJOBINFO As String = ""
            Dim MYJOBINFO1 As String = ""
            Dim MYJOBINFO2 As String = ""
            Dim MYJOBINFO3 As String = ""
            Dim MYJOBINFO4 As String = ""

            Dim m_addressline As String = ""
            Dim m_addressline1 As String = ""
            Dim m_addressline2 As String = ""
            Dim m_addressline3 As String = ""

            m_checkpayable = mywaiverinfo.WaiverInfo.CustName
            m_addressline1 = mywaiverinfo.WaiverInfo.CustName
            m_addressline2 = mywaiverinfo.WaiverInfo.CustAdd1
            m_addressline3 = mywaiverinfo.WaiverInfo.CustCity & ", " & mywaiverinfo.WaiverInfo.CustState & " " & mywaiverinfo.WaiverInfo.CustZip

            If mywaiverinfo.WaiverInfo.MailtoGC = True Then
                m_checkpayable = mywaiverinfo.WaiverInfo.GCName
                m_addressline1 = mywaiverinfo.WaiverInfo.GCName
                m_addressline2 = mywaiverinfo.WaiverInfo.GCAdd1
                m_addressline3 = mywaiverinfo.WaiverInfo.GCCity & ", " & mywaiverinfo.WaiverInfo.GCState & " " & mywaiverinfo.WaiverInfo.GCZip
            End If

            If mywaiverinfo.WaiverInfo.MailtoOW = True Then
                m_checkpayable = mywaiverinfo.WaiverInfo.OwnerName
                m_addressline1 = mywaiverinfo.WaiverInfo.OwnerName
                m_addressline2 = mywaiverinfo.WaiverInfo.OwnerAdd1
                m_addressline3 = mywaiverinfo.WaiverInfo.OwnerCity & ", " & mywaiverinfo.WaiverInfo.OwnerState & " " & mywaiverinfo.WaiverInfo.OwnerZip
            End If

            If mywaiverinfo.WaiverInfo.MailtoLE = True Then
                m_checkpayable = mywaiverinfo.WaiverInfo.LenderName
                m_addressline1 = mywaiverinfo.WaiverInfo.LenderName
                m_addressline2 = mywaiverinfo.WaiverInfo.LenderAdd1
                m_addressline3 = mywaiverinfo.WaiverInfo.LenderCity & ", " & mywaiverinfo.WaiverInfo.LenderState & " " & mywaiverinfo.WaiverInfo.LenderZip
            End If

            If mywaiverinfo.WaiverInfo.JointCheckAgreement = True Then
                m_checkpayable = mywaiverinfo.Client.ClientName & " And " & mywaiverinfo.WaiverInfo.CustName
            Else
                m_checkpayable = mywaiverinfo.Client.ClientName
            End If

            If m_addressline2.Length < 1 Then
                m_addressline2 = m_addressline3
                m_addressline3 = ""
            End If

            Replace("[MailInfo1]", m_addressline1)
            Replace("[MailInfo2]", m_addressline2)
            Replace("[MailInfo3]", m_addressline3)
            Replace("[MailInfo4]", "")

            m_checkissuer = mywaiverinfo.WaiverInfo.CustName
            If mywaiverinfo.WaiverInfo.CheckByGC = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.GCName
            End If
            If mywaiverinfo.WaiverInfo.CheckbyLE = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.LenderName
            End If
            If mywaiverinfo.WaiverInfo.CheckbyOW = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.OwnerName
            End If
            If mywaiverinfo.WaiverInfo.CheckByOT = True Then
                m_checkissuer = mywaiverinfo.WaiverInfo.OtherChkIssuerName
            End If

            Replace("[CHECKISSUER]", m_checkissuer)
            If mywaiverinfo.WaiverInfo.IsPaidInFull Then
                Replace("[PAYMENTAMT]", " ( Paid To Date ) ")
            Else
                ReplaceAmt("[PAYMENTAMT]", mywaiverinfo.WaiverInfo.PaymentAmt)
            End If

            Replace("[PAYABLETO]", m_checkpayable)
            Replace("[OWNERNAMES]", mywaiverinfo.WaiverInfo.OwnerName)
            Replace("[OWNERNAME]", mywaiverinfo.WaiverInfo.OwnerName)

            MYJOBINFO = mywaiverinfo.WaiverInfo.JobName & " " & mywaiverinfo.WaiverInfo.JobNum
            MYJOBINFO1 = mywaiverinfo.WaiverInfo.JobName & " " & mywaiverinfo.WaiverInfo.JobNum
            MYJOBINFO2 = mywaiverinfo.WaiverInfo.JobAdd1
            MYJOBINFO3 = mywaiverinfo.WaiverInfo.JobAdd2
            MYJOBINFO4 = mywaiverinfo.WaiverInfo.JobCity & ", " & mywaiverinfo.WaiverInfo.JobState & " " & mywaiverinfo.WaiverInfo.JobZip
            If MYJOBINFO3.Length < 1 Then
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If
            If MYJOBINFO2.Length < 1 Then
                MYJOBINFO2 = MYJOBINFO3
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If
            If MYJOBINFO1.Length < 1 Then
                MYJOBINFO1 = MYJOBINFO2
                MYJOBINFO2 = MYJOBINFO3
                MYJOBINFO3 = MYJOBINFO4
                MYJOBINFO4 = ""
            End If

            Replace("[JOBINFO]", MYJOBINFO)
            Replace("[JOBINFO1]", MYJOBINFO1)
            Replace("[JOBINFO2]", MYJOBINFO2)
            Replace("[JOBINFO3]", MYJOBINFO3)
            Replace("[JOBINFO4]", MYJOBINFO4)



            '^^*** commented by pooja 05/20/2021*** ^^
            Dim MYDS As DataSet = mywaiverinfo.clientntoaddressInfo
            myview = New DAL.COMMON.TableView(MYDS.Tables(0), "")

            Replace("[NTONAME]", myview.RowItem("NTOName"))
            '^^***  *** ^^
            Replace("[CLIENTNAME]", mywaiverinfo.Client.ClientName)
            Replace("[CUSTOMERNAME]", mywaiverinfo.WaiverInfo.CustName)
            ReplaceAmt("[DISPUTEDAMT]", mywaiverinfo.WaiverInfo.DisputedAmt)
            Replace("[NOTE]", mywaiverinfo.WaiverInfo.WaiverNote)
            Replace("[Note]", mywaiverinfo.WaiverInfo.WaiverNote)
            ReplaceDate("[PRINTDATE]", Today.ToShortDateString)
            ReplaceDate("[DatePrinted]", Today.ToShortDateString)
            ReplaceDate("[STARTDATE]", mywaiverinfo.WaiverInfo.StartDate)
            ReplaceDate("[THROUGHDATE]", mywaiverinfo.WaiverInfo.ThroughDate)
            Replace("[WAIVERDATES]", mywaiverinfo.WaiverInfo.WaiverDates)
            Replace("[WAIVERPAYMENTS]", mywaiverinfo.WaiverInfo.WaiverPayments)
            Replace("[LABORTYPE]", mywaiverinfo.ClientLienInfo.LaborType)
            Replace("[JOBCOUNTY]", mywaiverinfo.WaiverInfo.JobCounty)
            Replace("[JOBNAME]", mywaiverinfo.WaiverInfo.JobName)
            Replace("[JOBCITY]", mywaiverinfo.WaiverInfo.JobCity)
            Replace("[INVPAYMENTAPPNUM]", mywaiverinfo.WaiverInfo.InvoicePaymentNo)

        End Function

        Public Function ReplaceSignatureTextTokens() As Boolean
            '^^*** commented by pooja 05/21/2021*** ^^
            Dim MYDS As DataSet = mywaiverinfo.clientntoaddressInfo
            'Dim s As String = ""
            Replace("[NTONAME]", myview.RowItem("NTOName"))
            '^^***  *** ^^
            Dim s As String = ""
            Replace("[CLIENTNAME]", mywaiverinfo.Client.ClientName)
            Replace("[SIGNATORY]", mywaiverinfo.ClientSigner.Signer)
            Replace("[SIGNATORYTITLE]", mywaiverinfo.ClientSigner.SignerTitle)
            ReplaceDate("[TODAY]", Today.ToShortDateString)
            s = (mywaiverinfo.Client.AddressLine1.Trim & "," & mywaiverinfo.Client.City.Trim & " " & mywaiverinfo.Client.State.Trim)
            Replace("[JOBID]", "")
            Replace("[SIGNEREMAIL]", mywaiverinfo.ClientSigner.SignerEmail)
            Replace("[CLIENTINFO]", s)
            Replace("[CLIENTPHONE]", HDS.WINLIB.COMMON.Format.PhoneNo(mywaiverinfo.ClientLienInfo.ContactPhone))
            Replace("[JOBWAIVERLOGID]", mywaiverinfo.WaiverInfo.BatchNoJobWaiverId)

        End Function

        Public Function ReplaceNotaryTextTokens() As Boolean
            Replace("[SIGNATORY]", mywaiverinfo.ClientSigner.Signer)
        End Function
        'Test
        Public Function ReplaceJobWaiverLogIdTokens() As Boolean
            Replace("JobWaiverLogId", mywaiverinfo.WaiverInfo.BatchNoJobWaiverId)
        End Function

        Private Sub Replace(ByVal atag As String, ByVal avalue As String)
            Try
                flexfieldtext = Strings.Replace(flexfieldtext, atag, avalue)
            Catch
            End Try

        End Sub

        Private Sub ReplaceDate(ByVal atag As String, ByVal avalue As Object)
            Try
                If IsDate(avalue) Then
                    Dim mydate As String = Date.Parse(avalue).ToShortDateString
                    flexfieldtext = Strings.Replace(flexfieldtext, atag, " " & mydate)
                Else
                    flexfieldtext = Strings.Replace(flexfieldtext, atag, " ")
                End If

            Catch ex As Exception
                flexfieldtext = Strings.Replace(flexfieldtext, atag, " ")
            End Try

        End Sub

        Private Sub ReplaceAmt(ByVal atag As String, ByVal avalue As Object)
            Try
                Dim myamt As String = Strings.FormatCurrency(avalue)
                flexfieldtext = Strings.Replace(flexfieldtext, atag, myamt)
            Catch
                flexfieldtext = Strings.Replace(flexfieldtext, atag, " " & Strings.FormatCurrency(0))
            End Try

        End Sub

        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try
        End Function

    End Class

End Namespace
