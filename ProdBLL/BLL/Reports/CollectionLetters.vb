Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Namespace Collections.Letters
    Public Class CollectionLetters
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As CurrentUser
        Dim myletterinfo As HDS.DAL.COMMON.TableView
        Dim myform As HDS.DAL.COMMON.TableView

        Public Sub New(ByVal auser As BLL.CurrentUser, Optional ByVal areportdef As String = "Reports\CollectionLetters.xml")
            MyBase.New()
            Me.CurrentUser = auser
            Me.OutputFolder = auser.OutputFolder
            Me.SettingsFolder = auser.SettingsFolder
            Me.UserName = auser.UserName
            Me.ReportDefPath = Me.SettingsFolder & areportdef

        End Sub
        Public Function ExecuteC1Report(ByVal MYDS As DataSet) As String
            myletterinfo = New HDS.DAL.COMMON.TableView(MYDS.Tables(0))
            If myletterinfo.Count = 1 Then
                Me.ReportName = myletterinfo.RowItem("LetterCode")
                Me.DataView = myletterinfo
                With Me
                    .FilePrefix = myletterinfo.RowItem("DebtAccountId")
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If

        End Function

        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)



        End Sub


    End Class

End Namespace
