Option Strict Off
Imports System.Data.SqlClient
Imports System.Data
Imports System.IO
Imports System.Text.RegularExpressions
Public Class Export
    Public Shared Function ExportToCSV(ByRef adataview As DataView, ByRef afilename As String, ByRef acolumnnames As ExportColumns, ByVal AddHeader As Boolean) As String
        Dim ls As String = ""
        Dim scol As New ExportColumn
        Dim adatatable As DataTable = adataview.Table
        If acolumnnames.Count < 1 Then
            Dim col As DataColumn
            For Each col In adatatable.Columns
                acolumnnames.Add(col.ColumnName)
            Next
        End If

        Dim MYfilename As String = afilename
        If System.IO.File.Exists(MYfilename) Then
            System.IO.File.Delete(MYfilename)
        End If
        Dim s As String
        Dim losw As New StreamWriter(MYfilename, False)
        Try
            ls = ""
            If AddHeader = True Then

                For Each scol In acolumnnames
                    s = scol.ColumnName
                    If scol.FriendlyName.Length > 0 Then
                        s = scol.FriendlyName
                    End If
                    If Not Len(s) < 1 Then
                        ls = ls & Chr(34) & Trim$(s) & Chr(34) & ","
                    Else 'NOT NOT...
                        ls = ls & Chr(34) & "" & Chr(34) & ","
                    End If
                Next

                losw.WriteLine(ls)
            End If

            Dim dr As DataRowView
            Dim i As Int32
            Dim dv As DataView = adataview
            For i = 0 To dv.Count - 1
                dr = dv.Item(i)
                ls = ""
                Dim z As Integer = 0
                For Each scol In acolumnnames
                    s = scol.ColumnName
                    If Not Len(s) < 1 Then
                        ls = ls & Chr(34) & Trim$(HandleDbNull(dv.Table, s, dr(s))) & Chr(34) & ","
                    Else 'NOT NOT...
                        ls = ls & Chr(34) & "" & Chr(34) & ","
                    End If
                    z = z + 1
                Next
                losw.WriteLine(ls)
            Next
        Catch E As Exception
            MsgBox(E.Message, MsgBoxStyle.Critical)
            Return Nothing
        Finally
            losw.Close()
        End Try

        System.Threading.Thread.Sleep(1300)

        Return MYfilename

    End Function
    Public Shared Function ExportToCSV(ByRef adatatable As DataTable, ByRef afilename As String, ByRef acolumnnames As ExportColumns, ByVal AddHeader As Boolean) As String
        Dim ls As String = ""
        Dim scol As New ExportColumn

        If acolumnnames.Count < 1 Then
            Dim col As DataColumn
            For Each col In adatatable.Columns
                acolumnnames.Add(col.ColumnName)
            Next
        End If

        Dim MYfilename As String = afilename
        If System.IO.File.Exists(MYfilename) Then
            System.IO.File.Delete(MYfilename)
        End If
        Dim s As String
        Dim losw As New StreamWriter(MYfilename, False)
        Try
            ls = ""
            If AddHeader = True Then

                For Each scol In acolumnnames
                    s = scol.ColumnName
                    If scol.FriendlyName.Length > 0 Then
                        s = scol.FriendlyName
                    End If
                    If Not Len(s) < 1 Then
                        ls = ls & Chr(34) & Trim$(s) & Chr(34) & ","
                    Else 'NOT NOT...
                        ls = ls & Chr(34) & "" & Chr(34) & ","
                    End If
                Next

                losw.WriteLine(ls)
            End If

            Dim dr As DataRowView
            Dim i As Int32
            Dim dv As New DataView(adatatable)
            For i = 0 To dv.Count - 1
                dr = dv.Item(i)
                ls = ""
                Dim z As Integer = 0
                For Each scol In acolumnnames
                    s = scol.ColumnName
                    If Not Len(s) < 1 Then
                        ls = ls & Chr(34) & Trim$(HandleDbNull(dv.Table, s, dr(s))) & Chr(34) & ","
                    Else 'NOT NOT...
                        ls = ls & Chr(34) & "" & Chr(34) & ","
                    End If
                    z = z + 1
                Next
                losw.WriteLine(ls)
            Next
        Catch E As Exception
            MsgBox(E.Message, MsgBoxStyle.Critical)
            Return Nothing
        Finally
            losw.Close()
        End Try

        System.Threading.Thread.Sleep(1300)

        Return MYfilename

    End Function
    Public Shared Function HandleDbNull(ByVal adt As DataTable, ByVal ColumnName As String, ByVal VALUE As Object) As Object

        Try
            If DBNull.Value.Equals(VALUE) Then
                Select Case UCase(adt.Columns(ColumnName).DataType.Name)
                    Case "DECIMAL", "MONEY"
                        Return 0
                    Case "BOOLEAN"
                        Return "Y"
                    Case "FLOAT", "INT32", "DECIMAL", "INT"
                        Return 0
                    Case "DATE", "DATETIME"
                        Return ""
                    Case Else
                        Return ""
                End Select
            Else
                Dim MYVALUE As Object = Strings.Replace(VALUE, vbCrLf, "")

                Select Case UCase(adt.Columns(ColumnName).DataType.Name)
                    Case "BOOLEAN"
                        If VALUE = False Then
                            Return "N"
                        Else
                            Return "Y"
                        End If

                    Case "DATE", "DATETIME"
                        Dim str As String = SHORTDATE(MYVALUE)
                        Return str
                    Case Else
                        Return MYVALUE

                End Select
            End If
        Catch ex As Exception
            Return Nothing
        End Try

    End Function
    Public Shared Function SHORTDATE(ByVal mydate As String) As String
        Try
            Return Date.Parse(mydate).ToShortDateString
        Catch
            Return Nothing
        End Try

    End Function
    Public Function TrimSpace(ByRef strInput As String) As String
        ' This procedure trims extra space from any part of
        ' a string.
        Dim str As String = strInput.Trim
        Return Join(str.Split(" "), "")

    End Function
End Class
Public Class ExportColumn
    Public ColumnName As String = ""
    Public FriendlyName As String = ""
End Class
Public Class ExportColumns
    Inherits System.Collections.CollectionBase
    Public Sub New()
        MyBase.New()
    End Sub
    Public Sub AddColumnString(ByVal acolumnstring As String)
        Me.List.Clear()
        Dim scols As String = Strings.Replace(acolumnstring, vbCrLf, String.Empty)
        If acolumnstring.Length < 1 Then Exit Sub

        If scols.Length < 1 Then Exit Sub
        Dim mycol As New ExportColumn
        Dim ss1 As String() = Strings.Split(scols, ",")
        Dim s As String
        For Each s In ss1
            If s.Length > 0 Then
                Dim ss2 As String() = Strings.Split(s, "~")
                mycol = New ExportColumn
                mycol.ColumnName = ss2(0)
                If ss2.Length > 1 Then
                    mycol.FriendlyName = ss2(1)
                End If
                Me.Add(mycol)
            End If
        Next
    End Sub
    Public Function GetString() As String

        Dim mycol As New ExportColumn
        Dim scols As String = String.Empty
        For Each mycol In Me.List
            If scols.Length > 1 Then
                scols += "," & vbCrLf
            End If
            scols += mycol.ColumnName
            If mycol.FriendlyName.Length > 1 Then
                scols += "~" & mycol.FriendlyName
            End If
            
        Next
        scols += vbCrLf
        Return scols

    End Function
    Public Overridable Function Add(ByVal acolumnname As String) As Integer
        Dim mycol As New ExportColumn
        mycol.ColumnName = acolumnname
        mycol.FriendlyName = acolumnname
        MyBase.List.Add(mycol)
    End Function
    Public Overridable Function Add(ByVal acolumnname As String, ByVal afriendlyname As String) As Integer
        Dim mycol As New ExportColumn
        mycol.ColumnName = acolumnname
        mycol.FriendlyName = afriendlyname
        MyBase.List.Add(mycol)
    End Function

    Public Overridable Function Add(ByVal value As ExportColumn) As Integer
        MyBase.List.Add(value)
    End Function
    Default Public Overridable Property Item(ByVal index As Integer) As ExportColumn
        Get
            Return DirectCast(MyBase.List.Item(index), ExportColumn)
        End Get
        Set(ByVal value As ExportColumn)
            MyBase.List.Item(index) = value
        End Set
    End Property
End Class


