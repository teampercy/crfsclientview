Imports System.IO
Imports System.Text
Imports System.Windows.Forms
Namespace Common

    Public Class LOGGER
        Public Shared Sub WriteToErrorLog(ByVal msg As String, ByVal stkTrace As String, ByVal title As String)

            'check and make the directory if necessary; this is set to look in the application
            'folder, you may wish to place the error log in another location depending upon the
            'the user's role and write access to different areas of the file system
            Dim MYFILE As String = Application.StartupPath & "\errlog.txt"

            'check the file
            Dim fs As FileStream = New FileStream(MYFILE, FileMode.OpenOrCreate, FileAccess.ReadWrite)
            Dim s As StreamWriter = New StreamWriter(fs)
            s.Close()
            fs.Close()

            'log it
            Dim fs1 As FileStream = New FileStream(MYFILE, FileMode.Append, FileAccess.Write)
            Dim s1 As StreamWriter = New StreamWriter(fs1)
            s1.Write("Title: " & title & vbCrLf)
            s1.Write("Message: " & msg & vbCrLf)
            s1.Write("StackTrace: " & stkTrace & vbCrLf)
            s1.Write("Date/Time: " & DateTime.Now.ToString() & vbCrLf)
            s1.Write("===========================================================================================" & vbCrLf)

            s1.Close()
            fs1.Close()

        End Sub
        Public Shared Function WriteToEventLog(ByVal entry As String, _
                     Optional ByVal appName As String = "CompanyName", _
                     Optional ByVal eventType As _
                     EventLogEntryType = EventLogEntryType.Information, _
                     Optional ByVal logName As String = "ProductName") As Boolean

            Dim objEventLog As New EventLog

            Try

                'Register the Application as an Event Source
                If Not EventLog.SourceExists(appName) Then
                    EventLog.CreateEventSource(appName, logName)
                End If

                'log the entry
                objEventLog.Source = appName
                objEventLog.WriteEntry(entry, eventType)
                Return True

            Catch Ex As Exception

                Return False

            End Try

        End Function
    End Class

End Namespace
