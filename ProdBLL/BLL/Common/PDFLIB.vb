Imports System.Management
Imports System.IO
Imports System.Windows.Forms

Public Class PDFLIB

    Public Const TEMPPDFDIR As String = "TEMPDF\"
    Public Const TEMPDIR As String = "TEMP\"
    Public Const TEMPWORKDIR As String = "\TEMPWORK\"

    Public Shared Function TrimSpace(ByRef strInput As String) As String
        ' This procedure trims extra space from any part of
        ' a string.
        Dim str As String = strInput.Trim
        Return Join(str.Split(" "), "")

    End Function
    Public Shared Function SetDefaultPrinter(ByVal PrinterName As String) As String


        'Declare WMI Variables
        Dim MgmtObject As ManagementObject
        Dim MgmtCollection As ManagementObjectCollection
        Dim MgmtSearcher As ManagementObjectSearcher
        Dim ReturnBoolean As Boolean = False

        'Perform the search for printers and return the listing as a collection
        MgmtSearcher = New ManagementObjectSearcher("Select * from Win32_Printer")
        MgmtCollection = MgmtSearcher.Get

        'Enumerate Objects To Find Printer
        For Each MgmtObject In MgmtCollection
            'Look for a match
            If MgmtObject.Item("name").ToString = PrinterName Then
                'Set Default Printer
                Dim TempObject() As Object 'Temporary Object for InvokeMethod. Holds no purpose.
                MgmtObject.InvokeMethod("SetDefaultPrinter", TempObject)

                'Set Success Value and Exit For..Next Loop
                ReturnBoolean = True
                Exit For
            End If
        Next

        'Return Success Value
        Return ReturnBoolean


    End Function
    Public Shared Function CombinePDFS(ByVal afiles As ArrayList, ByVal adest As String) As Boolean

        Try
            If System.IO.File.Exists(adest) Then
                System.IO.File.Delete(adest)
            End If
           
            If System.IO.Directory.Exists(PDFLIB.TEMPDIR) Then
                System.IO.Directory.Delete(PDFLIB.TEMPDIR, True)
            End If

            System.IO.Directory.CreateDirectory(PDFLIB.TEMPDIR)

            If System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(adest)) = False Then
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(adest))
            End If

            Dim FILE As String
            Dim I As Integer = 0
            For Each FILE In afiles
                If System.IO.File.Exists(FILE) = True Then
                    I = I + 1
                    Dim OUTFILE = "TEMP\" & Strings.Right("000000" & I.ToString, 6) & ".PDF"
                    System.IO.File.Copy(FILE, OUTFILE)
                End If
            Next

            Dim S As String = Application.StartupPath & "\PDFTK  " & "TEMP\*.PDF CAT OUTPUT " & "TEMP\ONE.PDF"

            Shell(S, AppWinStyle.Hide, True)
            Application.DoEvents()

            System.IO.File.Copy("TEMP\one.PDF", adest)


        Catch EX As Exception
            MsgBox(EX.Message, MsgBoxStyle.Critical)
            Return False
        Finally

        End Try
        If System.IO.File.Exists(adest) = False Then
            Return False
        Else
            Return True
        End If

    End Function
    Public Shared Function PrintPDF(ByVal APRINTERNAME As String, ByVal AFILES As ArrayList) As Boolean
        Dim AFILENAME = PDFLIB.TEMPPDFDIR & System.IO.Path.GetFileName(System.IO.Path.GetTempFileName)
        AFILENAME = Strings.Replace(UCase(AFILENAME), ".TMP", ".PDF")
        Dim afile As String
        For Each afile In AFILES
            With New Process
                .StartInfo.Verb = "print"
                .StartInfo.CreateNoWindow = False
                .StartInfo.FileName = afile
                .StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                .Start()
            End With
        Next
        Return True

    End Function
    Public Shared Function PrintPDFS(ByVal APRINTERNAME As String, ByVal AFILES As ArrayList) As Boolean
        Dim AFILENAME = PDFLIB.TEMPPDFDIR & System.IO.Path.GetFileName(System.IO.Path.GetTempFileName)
        AFILENAME = Strings.Replace(UCase(AFILENAME), ".TMP", ".PDF")

        If CombinePDFS(AFILES, AFILENAME) = False Then
            Return False
        End If
        If System.IO.File.Exists(AFILENAME) = False Then
            Return False
        End If

        Return PrintPDF(APRINTERNAME, AFILENAME)

    End Function
    Public Shared Function PrintPDF(ByVal APRINTERNAME As String, ByVal AFILENAME As String) As Boolean
        If System.IO.File.Exists(AFILENAME) = False Then Exit Function

        With New Process
            .StartInfo.Verb = "print"
            .StartInfo.CreateNoWindow = False
            .StartInfo.FileName = AFILENAME
            .StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            .Start()
        End With
        Return True

    End Function
    Public Shared Sub ClearTempDirectories()

        If System.IO.Directory.Exists(PDFLIB.TEMPDIR) Then
            System.IO.Directory.CreateDirectory(PDFLIB.TEMPDIR)
        End If
        DirectoryClean(PDFLIB.TEMPDIR, Now())

        If System.IO.Directory.Exists(PDFLIB.TEMPPDFDIR) Then
            System.IO.Directory.CreateDirectory(PDFLIB.TEMPPDFDIR)
        End If
        DirectoryClean(PDFLIB.TEMPPDFDIR, Now())

        If System.IO.Directory.Exists(PDFLIB.TEMPWORKDIR) Then
            System.IO.Directory.CreateDirectory(PDFLIB.TEMPWORKDIR)
        End If
        DirectoryClean(PDFLIB.TEMPWORKDIR, Now())

    End Sub
    Public Shared Function DirectoryClean(ByVal aSourcePath As String, ByVal adate As Date) As Boolean

        Dim SourceDir As DirectoryInfo = New DirectoryInfo(aSourcePath)

        If SourceDir.Exists Then
            ' copy all the files of the current directory
            Dim ChildFile As FileInfo
            For Each ChildFile In SourceDir.GetFiles()
                If ChildFile.LastWriteTime < adate Then
                    System.IO.File.Delete(ChildFile.FullName)
                End If
            Next
            Dim SubDir As DirectoryInfo
            For Each SubDir In SourceDir.GetDirectories()
                DirectoryClean(SubDir.FullName, adate)
            Next
        End If

        Return True

    End Function
    Public Shared Function ViewPDF(ByVal ASTREAM As Byte()) As Boolean
        Dim myfilename As String = System.IO.Path.GetTempFileName
        If PDFLIB.WriteStream(ASTREAM, myfilename) = True Then
            PDFLIB.ViewPDF(myfilename)
        End If
        Return False
    End Function
    Public Shared Sub ViewPDF(ByVal AFILENAME As String)
        If System.IO.File.Exists(AFILENAME) = False Then Exit Sub

        Dim s As String = "explorer " & AFILENAME
        Shell(s, AppWinStyle.MaximizedFocus)

    End Sub
    Public Shared Function GetStream(ByVal afilename As String) As Byte()
        If System.IO.File.Exists(afilename) = False Then Return Nothing

        Dim FileStream1 As System.IO.FileStream
        Dim FileInfo1 As System.IO.FileInfo

        ' READ THE FILE INTO MEMORY
        FileInfo1 = New System.IO.FileInfo(afilename)
        FileStream1 = New System.IO.FileStream(afilename, IO.FileMode.Open)
        Dim Array1(CInt(FileInfo1.Length - 1)) As Byte
        FileStream1.Read(Array1, 0, CInt(FileInfo1.Length))
        FileStream1.Close()
        Return Array1

    End Function
    Public Shared Function WriteStream(ByVal astream As Byte(), ByVal afilename As String) As Boolean

        Dim FileStream1 As System.IO.FileStream
        Dim FileInfo1 As System.IO.FileInfo
        Try
            ' WRITE THE FILE FROM MEMORY
            FileInfo1 = New System.IO.FileInfo(afilename)
            FileStream1 = New System.IO.FileStream(afilename, IO.FileMode.Create)
            Dim Array1(CInt(FileInfo1.Length - 1)) As Byte
            FileStream1.Write(astream, 0, astream.Length)
            FileStream1.Close()
            Return True
        Catch
            Return False
        End Try

    End Function
End Class
