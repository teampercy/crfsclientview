Imports System
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel
Imports System.Text.RegularExpressions
Imports System.Reflection
Imports System.Data
Namespace Emails
    Friend Class KeyValueReplace
        Dim myvalues As Collection
        Public Sub New()
            myvalues = New Collection
        End Sub 'New
        Public Sub AddData(ByVal key As String, ByVal val As String, ByVal format As String)
            Dim newval As New KeyValue
            newval.Key = key
            newval.Value = val
            newval.Formatting = format
            myvalues.Add(newval, key)
        End Sub 'AddData
        Public Function Replace(ByVal atext As String) As String
            Dim pat As String = ""
            Dim newval As New KeyValue
            Dim s As String = atext
            For Each newval In myvalues
                s = Strings.Replace(s, "[" & UCase(newval.Key) & "]", newval.Value)
            Next
            Return s

        End Function
        Private Function GetValue(ByVal apropertyname As String, ByRef aentity As Object, ByVal adefault As String) As Object
            Try

                Dim t As Type = aentity.GetType()

                'Dim info As PropertyInfo = t.GetProperty(apropertyname)
                Dim info As PropertyInfo = aentity.GetType.GetProperty(apropertyname)

                Dim s As Object = info.GetValue(aentity, Nothing)
                Return s
            Catch EX As Exception
                Return adefault
            End Try

        End Function
        Private Class KeyValue
            Public Key As String
            Public Value As String
            Public Formatting As String
        End Class

    End Class
    Public Class EmailSender
        Dim _smtpserver As String
        Dim _message As System.Net.Mail.MailMessage
        Dim _smtpuserid As String
        Dim _smtppwd As String
        Dim _smtpport As String
        Dim _smtp As SmtpClient
        Dim _maxrecipients As Integer = 1
        Dim _sendreport As Boolean = True
        Dim _stmpclient As SmtpClient
        Public Sub New()

        End Sub
        Public Sub StartTask(ByVal message As System.Net.Mail.MailMessage, Optional ByVal SMTPSERVER As String = "", Optional ByVal SMTPPORT As String = "", Optional ByVal smtpuid As String = "", Optional ByVal smtppwd As String = "", Optional ByVal smtpenablessl As Boolean = False)
            _message = message
            _smtpserver = SMTPSERVER
            _smtpuserid = smtpuid
            _smtppwd = smtppwd
            _smtpport = SMTPPORT
            _smtp = New SmtpClient(_smtpserver)
            If _smtpuserid.Length > 1 Then
                _smtp.Credentials = New NetworkCredential(_smtpuserid, _smtppwd)
                _smtp.EnableSsl = smtpenablessl
            End If
            Dim threadStart As ThreadStart = Nothing
            Dim thread As Thread = Nothing
            Try
                threadStart = AddressOf _DoTheWork
                thread = New Thread(threadStart)
                '    thread.Name = "SendAsyncEmail " + )
                thread.Start()
                thread = Nothing
            Catch generatedExceptionVariable0 As Exception

            Finally
                thread = Nothing
                threadStart = Nothing
            End Try
        End Sub
        Private Sub _DoTheWork()
            Dim s As String = ""
            s += "MESSAGE FROM: " & _message.From.ToString & vbCrLf
            s += vbCrLf
            s += "MESSAGE TO: " & _message.To.ToString & vbCrLf
            s += vbCrLf
            s += "MESSAGE BCC: " & _message.Bcc.ToString & vbCrLf
            s += vbCrLf
            s += "MESSAGE SUBJECT: " & _message.Subject.ToString & vbCrLf
            s += vbCrLf
            s += "MESSAGE BODY: " & _message.Body.ToString & vbCrLf
            s += vbCrLf & "***************************" & vbCrLf
            s += "MESSAGE START: " & Now.ToLongTimeString & vbCrLf
            Try
                _smtp.Send(_message)
                _smtp.Dispose()
            Catch ex1 As Exception
                ' Throw New Exception(ex1.Message & vbCrLf & s)
                Exit Sub
            End Try
        End Sub
        Public Sub PingServer()
            Try


                Dim http As New WebClient()

                Dim pingurl As String = String.Empty
                Dim Result As String = http.DownloadString(pingurl)
            Catch ex As Exception

                Dim Message As String = ex.Message
            End Try

        End Sub

    End Class
End Namespace
