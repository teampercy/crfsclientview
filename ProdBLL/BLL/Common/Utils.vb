Imports System.Web.UI.WebControls
Imports System.Text.StringBuilder
Namespace CRFVIEW
    Public Class Globals
        Public Shared DBO As HDS.DAL.Providers.IDAL = HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", "")
    End Class
    Public Class Utils
        Public Shared Function GetDecimal(ByVal aamt As Object) As Decimal
            Try
                Return Decimal.Parse(aamt)
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function GetPhoneNo(ByVal avalue As Object) As String
            Try
                '    Return Common.UnFormat.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function GetDate(ByVal adate As String) As Date
            Try
                If IsDate(adate) Then
                    Return Date.Parse(adate)
                Else
                    Return Nothing
                End If
            Catch ex As Exception
                Return Nothing
            End Try
        End Function
        Public Shared Function FormatPhoneNo(ByVal avalue As Object) As String
            Try
                '   Return HDS.WEBLIB.Common.Format.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function FormatDate(ByVal adate As Object) As String

            Try
                If IsDate(adate) And adate <> Date.MinValue Then
                    Dim s As String() = Strings.Split(Date.Parse(adate).ToShortDateString, "/")
                    Dim y As String = Right("00" & s(0), 2) & "/" & Right("00" & s(1), 2) & "/" & Right("00" & s(2), 2)
                    Return y
                Else
                    Return String.Empty
                End If
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function
        Public Shared Function FormatDecimal(ByVal aamt As Object) As String

            Try
                Return Strings.FormatCurrency(Decimal.Parse(aamt), 2)
            Catch ex As Exception
                Return Strings.FormatCurrency(Decimal.Parse(0), 2)
            End Try
        End Function
       
        Public Shared Sub DownloadFile(ByVal FileLoc As String)
            Dim objFile As New System.IO.FileInfo(FileLoc)
            Dim objResponse As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            If objFile.Exists Then
                objResponse.ClearContent()
                objResponse.ClearHeaders()
                objResponse.AppendHeader("content-disposition", "attachment; filename=" + objFile.Name.ToString)
                objResponse.AppendHeader("Content-Length", objFile.Length.ToString())

                Dim strContentType As String
                Select Case Strings.LCase(objFile.Extension)
                    Case ".txt" : strContentType = "text/plain"
                    Case ".htm", ".html" : strContentType = "text/html"
                    Case ".rtf" : strContentType = "text/richtext"
                    Case ".jpg", ".jpeg" : strContentType = "image/jpeg"
                    Case ".gif" : strContentType = "image/gif"
                    Case ".bmp" : strContentType = "image/bmp"
                    Case ".mpg", ".mpeg" : strContentType = "video/mpeg"
                    Case ".avi" : strContentType = "video/avi"
                    Case ".pdf" : strContentType = "application/pdf"
                    Case ".doc", ".dot" : strContentType = "application/msword"
                    Case ".csv", ".xls", ".xlt" : strContentType = "application/vnd.msexcel"
                    Case Else : strContentType = "application/octet-stream"
                End Select
                objResponse.ContentType = strContentType
                'objResponse.WriteFile(objFile.FullName)
                WriteFile(objFile.FullName)

                objResponse.Flush()
                objResponse.Close()
            End If
        End Sub
        Public Shared Sub WriteFile(ByVal strFileName As String)
            Dim objResponse As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            Dim objStream As System.IO.Stream = Nothing

            ' Buffer to read 10K bytes in chunk:
            Dim bytBuffer(10000) As Byte

            ' Length of the file:
            Dim intLength As Integer

            ' Total bytes to read:
            Dim lngDataToRead As Long

            Try
                ' Open the file.
                objStream = New System.IO.FileStream(strFileName, System.IO.FileMode.Open, IO.FileAccess.Read, IO.FileShare.Read)

                ' Total bytes to read:
                lngDataToRead = objStream.Length

                objResponse.ContentType = "application/octet-stream"

                ' Read the bytes.
                While lngDataToRead > 0
                    ' Verify that the client is connected.
                    If objResponse.IsClientConnected Then
                        ' Read the data in buffer
                        intLength = objStream.Read(bytBuffer, 0, 10000)

                        ' Write the data to the current output stream.
                        objResponse.OutputStream.Write(bytBuffer, 0, intLength)

                        ' Flush the data to the HTML output.
                        objResponse.Flush()

                        ReDim bytBuffer(10000)       ' Clear the buffer
                        lngDataToRead = lngDataToRead - intLength
                    Else
                        'prevent infinite loop if user disconnects
                        lngDataToRead = -1
                    End If
                End While

            Catch ex As Exception
                ' Trap the error, if any.
                objResponse.Write("Error : " & ex.Message)
            Finally
                If IsNothing(objStream) = False Then
                    ' Close the file.
                    objStream.Close()
                End If
            End Try
        End Sub
        Public Shared Function GetFileName(ByVal Request As HttpRequest, ByVal ausername As String) As String
            Dim sfilename As String = Request.ServerVariables("REMOTE_ADDR")
            If ausername.Length > 1 Then
                sfilename += "-" & ausername
            End If

            sfilename += "-" & Format(Now(), "yyyyMMddhhmmss")
            sfilename += ".HTML"

            Dim s As String = Request.MapPath("~/App_Data/ErrorLog/" & sfilename)
            If System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(s)) = False Then
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(s))
            End If

            Return s

        End Function
        Public Shared Function BuildMessage(ByVal Request As HttpRequest, ByVal ex As Exception) As String

            Dim strMessage As New System.Text.StringBuilder
            strMessage.Append("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"">")
            strMessage.Append("<html>")
            strMessage.Append("<head>")
            strMessage.Append("<title>Page Error</title>")
            strMessage.Append("<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">")
            strMessage.Append("<style type=""text/css"">")
            strMessage.Append("<!--")
            strMessage.Append(".basix {")
            strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
            strMessage.Append("font-size: 12px;")
            strMessage.Append("}")
            strMessage.Append(".header1 {")
            strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
            strMessage.Append("font-size: 12px;")
            strMessage.Append("font-weight: bold;")
            strMessage.Append("color: #000099;")
            strMessage.Append("}")
            strMessage.Append(".tlbbkground1 {")
            strMessage.Append("background-color:  #000099;")
            strMessage.Append("}")
            strMessage.Append("-->")
            strMessage.Append("</style>")
            strMessage.Append("</head>")
            strMessage.Append("<body>")
            strMessage.Append("<table width=""85%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""1"" class=""tlbbkground1"">")
            strMessage.Append("<tr bgcolor=""#eeeeee"">")
            strMessage.Append("<td colspan=""2"" class=""header1"">Page Error</td>")
            strMessage.Append("</tr>")
            strMessage.Append("<tr>")
            strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>IP Address</td>")
            strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & Request.ServerVariables("REMOTE_ADDR") & "</td>")
            strMessage.Append("</tr>")
            strMessage.Append("<tr>")
            strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>User Agent</td>")
            strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & Request.ServerVariables("HTTP_USER_AGENT") & "</td>")
            strMessage.Append("</tr>")
            strMessage.Append("<tr>")
            strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Page</td>")
            strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & Request.Url.AbsoluteUri & "</td>")
            strMessage.Append("</tr>")
            strMessage.Append("<tr>")
            strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Time</td>")
            strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & System.DateTime.Now & " EST</td>")
            strMessage.Append("</tr>")
            strMessage.Append("<tr>")
            strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Error Message</td>")
            strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.Message.ToString & "</td>")
            strMessage.Append("</tr>")

            If ex.Source.Length > 1 Then
                strMessage.Append("<tr>")
                strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Source</td>")
                strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.Source.ToString & "</td>")
                strMessage.Append("</tr>")
            End If

            If ex.StackTrace.Length > 1 Then
                strMessage.Append("<tr>")
                strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Stack Trace</td>")
                strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.StackTrace.ToString & "</td>")
                strMessage.Append("</tr>")
            End If

            If IsNothing(ex.InnerException) = False Then
                strMessage.Append("<tr>")
                strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Inner Exception</td>")
                strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.InnerException.ToString & "</td>")
                strMessage.Append("</tr>")
            End If

            strMessage.Append("</table>")
            strMessage.Append("</body>")
            strMessage.Append("</html>")
            Return strMessage.ToString

        End Function
        Public Shared Sub ReportViewer(ByVal apage As Page, ByVal areportpath As String)
            Dim myurl As String = apage.ResolveUrl("~/UserData/Output/") & System.IO.Path.GetFileName(areportpath)
            If InStr(areportpath, ".CSV") Then
                Dim s As String = GetOutputFolderURL(apage) & System.IO.Path.GetFileName(areportpath)
                apage.Response.Redirect(s)

            End If

            Dim popupScript As String = "<script language='javascript'>" & _
        "window.open('[PAGENAME]', 'ReportViewer', " & _
        "'width=700, height=500, scrollbars=yes, menubar=yes, resizable=YES')" & _
        "</script>"
            popupScript = Strings.Replace(popupScript, "[PAGENAME]", myurl)
            apage.ClientScript.RegisterStartupScript(apage.GetType(), "PopupScript", popupScript)

        End Sub
        Public Shared Sub AlertMessage(ByVal apage As Page, ByVal amsg As String)
            ' HELPER FUNCTION TO POPUP MESSAGES
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & amsg & """);"
            strScript += "</script>"
            If (Not apage.ClientScript.IsStartupScriptRegistered("pagealertmessage")) Then
                apage.ClientScript.RegisterStartupScript(apage.GetType(), "AlertMessage", strScript)
            End If

        End Sub
        Public Shared Sub SetSafeButton(ByVal thisPage As Page, ByVal button As System.Web.UI.WebControls.ImageButton)
            Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ")
            sb.Append("if (Page_ClientValidate() == false) { return false; }} ")
            sb.Append("this.value = 'Please wait...';")
            sb.Append("this.disabled = true;")
            sb.Append("thisPage.Page.GetPostBackEventReference(button)")
            sb.Append(";")
            button.Attributes.Add("onclick", sb.ToString)
        End Sub
        Public Shared Sub SetSafeButton(ByVal thisPage As Page, ByVal button As System.Web.UI.WebControls.Button)
            Dim sb As System.Text.StringBuilder = New System.Text.StringBuilder
            sb.Append("if (typeof(Page_ClientValidate) == 'function') { ")
            sb.Append("if (Page_ClientValidate() == false) { return false; }} ")
            sb.Append("this.value = 'Please wait...';")
            sb.Append("this.disabled = true;")
            sb.Append("thisPage.Page.GetPostBackEventReference(button)")
            sb.Append(";")
            button.Attributes.Add("onclick", sb.ToString)
        End Sub
        Public Shared Function GetAbsoluteUrl(ByVal apagename As String) As String
            Dim incoming As HttpContext = HttpContext.Current

            Dim Port As String = incoming.Request.ServerVariables("SERVER_PORT")
            If Port Is Nothing OrElse Port = "80" OrElse Port = "443" Then
                Port = ""
            Else
                Port = ":" + Port
            End If

            Dim Protocol As String = incoming.Request.ServerVariables("SERVER_PORT_SECURE")
            If Protocol Is Nothing OrElse Protocol = "0" Then
                Protocol = "http://"
            Else
                Protocol = "https://"
            End If
            Dim s As String = Protocol + incoming.Request.ServerVariables("SERVER_NAME") + Port + incoming.Request.ApplicationPath
            s += apagename
            Return s

        End Function
        Public Shared Function GetAbsoluteServerPath() As String
            Dim incoming As HttpContext = HttpContext.Current
            Dim strServerPath As String
            strServerPath = incoming.Request.MapPath(incoming.Request.ApplicationPath)
            If Not strServerPath.EndsWith("\") Then
                strServerPath += "\"
            End If
            Return strServerPath
        End Function
        Public Shared Function GetSettingsFolderURL(ByVal apage As System.Web.UI.Page) As String
            Dim s As String = apage.ResolveUrl("~/App_Data/Settings/")
            Return s
        End Function
        Public Shared Function GetErrorLogFolderPath() As String
            Dim incoming As HttpContext = HttpContext.Current
            Dim s As String = GetAbsoluteServerPath() & "App_Data\ErrorLog\"
            If System.IO.Directory.Exists(s) = False Then
                System.IO.Directory.CreateDirectory(s)
            End If
            Return s
        End Function
        Public Shared Function GetErrorLogFolderURL(ByVal apage As System.Web.UI.Page) As String
            Dim s As String = apage.ResolveUrl("~/App_Data/ErrorLog/")
            Return s
        End Function

        Public Shared Function GetSettingsFolderPath() As String
            Dim incoming As HttpContext = HttpContext.Current
            Dim s As String = GetAbsoluteServerPath() & "App_Data\Settings\"
            If System.IO.Directory.Exists(s) = False Then
                System.IO.Directory.CreateDirectory(s)
            End If
            Return s
        End Function
        Public Shared Function GetOutputFolderURL(ByVal apage As System.Web.UI.Page) As String
            Dim s As String = apage.ResolveUrl("~/UserData/Output/")
            Return s
        End Function
        Public Shared Function GetOutputFolderPath() As String
            Dim s As String = GetAbsoluteServerPath() & "UserData\Output\"
            If System.IO.Directory.Exists(s) = False Then
                System.IO.Directory.CreateDirectory(s)
            End If
            Return s
        End Function
        Public Shared Function GetUploadFolderURL(ByVal apage As System.Web.UI.Page) As String
            Dim s As String = apage.ResolveUrl("~/UserData/UpLoad/")
            Return s
        End Function
        Public Shared Function GetUploadFolderPath() As String
            Dim s As String = GetAbsoluteServerPath() & "UserData\UpLoad\"
            If System.IO.Directory.Exists(s) = False Then
                System.IO.Directory.CreateDirectory(s)
            End If
            Return s

        End Function

        Public Shared Function CombinePDFS(ByVal afiles As ArrayList, ByVal adest As String) As Boolean

            Try
                Dim STEMP As String = System.Web.HttpContext.Current.Request.MapPath("~/app_data/TEMP")
                STEMP += "\"
                If System.IO.File.Exists(adest) Then
                    System.IO.File.Delete(adest)
                End If

                If System.IO.Directory.Exists(STEMP) Then
                    System.IO.Directory.Delete(STEMP, True)
                End If

                System.IO.Directory.CreateDirectory(STEMP)

                If System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(adest)) = False Then
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(adest))
                End If

                Dim FILE As String
                Dim I As Integer = 0
                For Each FILE In afiles
                    If System.IO.File.Exists(FILE) = True Then
                        I = I + 1
                        Dim OUTFILE = STEMP & Strings.Right("000000" & I.ToString, 6) & ".PDF"
                        System.IO.File.Copy(FILE, OUTFILE)
                    End If
                Next

                Dim S As String = "C:\PDFTK" & STEMP & "*.PDF CAT OUTPUT " & STEMP & "ONE.PDF"

                Shell(S, AppWinStyle.Hide, True)

                System.IO.File.Copy(STEMP & "one.PDF", adest)


            Catch EX As Exception
                MsgBox(EX.Message, MsgBoxStyle.Critical)
                Return False
            Finally

            End Try
            If System.IO.File.Exists(adest) = False Then
                Return False
            Else
                Return True
            End If

        End Function
    End Class
End Namespace
