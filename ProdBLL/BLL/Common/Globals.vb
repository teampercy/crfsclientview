Imports System.Data
Imports CRF.CLIENTVIEW.BLL.CRFDB
Public Class Globals
    Public CurrentUser As New CurrentUser
    Public Shared MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
    Public MYDTRPT As New HDS.Reporting.DataReports.DataReport
    Public Shared Function GetCurrentUser() As CurrentUser
        Dim objReq As System.Web.HttpContext = System.Web.HttpContext.Current

        Dim userid As String = TryCast(System.Web.HttpContext.Current.Session("UserId"), Object)
        Dim curruser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(objReq.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
        If IsNothing(curruser) Then
            curruser = New CRF.CLIENTVIEW.BLL.ClientUserInfo(userid)
        End If
     
        Dim MYUSER As New CurrentUser
        With System.Configuration.ConfigurationManager.AppSettings()
            MYUSER.Id = curruser.UserInfo.ID
            MYUSER.LoginCode = curruser.UserInfo.LoginCode
            MYUSER.ClientCode = curruser.Client.ClientCode
            MYUSER.ClientName = curruser.Client.ClientName
            MYUSER.UserName = curruser.UserInfo.UserName
            MYUSER.Email = curruser.UserInfo.Email
            MYUSER.OutputFolder = objReq.Request.MapPath("~/userdata/output")
            MYUSER.SettingsFolder = objReq.Request.MapPath("~/app_data/settings")
            MYUSER.UploadFolder = objReq.Request.MapPath("~/userdata/upload")
        End With
        Return MYUSER

    End Function
    Public Shared Function GetFileName(ByVal areportname As String) As String
        Dim myuser As CurrentUser = Globals.GetCurrentUser
        Dim myprefix As String = myuser.ClientCode
        myprefix += "-" & myuser.LoginCode
        myprefix += "-" & myuser.Id
        myprefix += "-" & areportname
        If System.IO.Directory.Exists(myuser.OutputFolder) = False Then
            System.IO.Directory.CreateDirectory(myuser.OutputFolder)
        End If
        Dim s As String = Common.FileOps.TrimSpace(myuser.OutputFolder & "\" & myprefix & "-" & Strings.Format(Now(), "yyyyMMddhhmmss") & ".csv")
        s = Strings.Replace(s, "\\", "\")
        Return s

    End Function
    Private Shared Sub SetC1RptLogo()
        Dim MYAGENCY As New TABLES.Agency
        CRFDB.Provider.DBO.Read(1, MYAGENCY)
        Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
        mylogo.LoadImage(MYAGENCY.AgencyLogo)
        MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
        MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter

    End Sub
    Public Shared Function RenderC1Report(ByVal adatatable As DataView, ByVal areportdef As String, ByVal areportname As String, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
        Dim myuser As New CurrentUser
        Dim myreportpath As String = myuser.SettingsFolder & "REPORTS\" & areportdef
        Dim myoutputfolder As String = myuser.OutputFolder
        SetC1RptLogo()
        Return MYC1RPT.Render("", myoutputfolder, myreportpath, areportname, adatatable, HDS.Reporting.COMMON.PrintMode.PrintToPDF, areportprefix, asubtitle1, asubtitle2)

    End Function
    Public Shared Function RenderDataReport(ByVal adatatable As DataTable, ByVal areportdef As String, ByVal areportname As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
        Dim objReq As System.Web.HttpContext = System.Web.HttpContext.Current
        Dim myreportpath As String = objReq.Request.MapPath("~\app_data\settings\REPORTS\")
        Dim myoutputfolder As String = objReq.Request.MapPath("~\userdata\output\")
        Dim myuser As CurrentUser = Globals.GetCurrentUser
        Dim myprefix As String = myuser.ClientCode
        myprefix += "-" & myuser.LoginCode
        myprefix += "-" & myuser.Id
        Dim MYDTRPT As New HDS.Reporting.DataReports.DataReport
        With MYDTRPT
            .ConfigFolder = myreportpath
            .ReportLayout = areportdef
            .OutputFolder = myoutputfolder
            .ReportName = areportname
            .FilePrefix = myprefix
            .ReportFormat = HDS.Reporting.ReportFormats.PDF
            .ExportFormat = HDS.Reporting.ExportFormats.NONE

        End With
        MYDTRPT.Render(adatatable)
        Dim sreport As String = MYDTRPT.ReportPath
        sreport = Strings.Replace(sreport, "\\", "\")

        If System.IO.File.Exists(sreport) Then
            Return "~\userdata\output\" & System.IO.Path.GetFileName(sreport)
        End If

        Return Nothing


    End Function
    Public Shared Function RenderC1Report(ByVal adatatable As DataTable, ByVal areportdef As String, ByVal areportname As String, ByVal aoutputfolder As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String

        Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
        Dim myreportpath As String = areportdef
        Dim myoutputfolder As String = aoutputfolder
        Dim MYAGENCY As New CRFDB.TABLES.Agency
        CRFDB.Provider.DBO.Read(1, MYAGENCY)
        Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
        mylogo.LoadImage(MYAGENCY.AgencyLogo)
        MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
        MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter
        Dim mymode As HDS.Reporting.COMMON.PrintMode = HDS.Reporting.COMMON.PrintMode.PrintToPDF
        Dim myview As New DataView(adatatable)
        Dim sreport As String = MYC1RPT.Render(areportprefix, myoutputfolder, myreportpath, areportname, myview, HDS.Reporting.COMMON.PrintMode.PrintToPDF, areportprefix, asubtitle1, asubtitle2)
        If System.IO.File.Exists(sreport) Then
            Return aoutputfolder & System.IO.Path.GetFileName(sreport)
        End If

        Return Nothing


    End Function
    Public Shared Function RenderC1Report(ByVal adatatable As DataTable, ByVal areportdef As String, ByVal areportname As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
        Dim objReq As System.Web.HttpContext = System.Web.HttpContext.Current
        Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider

        Dim myreportpath As String = objReq.Request.MapPath("~\app_data\settings\REPORTS\" & areportdef)
        Dim myoutputfolder As String = objReq.Request.MapPath("~\userdata\output\")

        Dim MYAGENCY As New CRFDB.TABLES.Agency
        CRFDB.Provider.DBO.Read(1, MYAGENCY)
        Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
        mylogo.LoadImage(MYAGENCY.AgencyLogo)
        MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
        MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter
        Dim mymode As HDS.Reporting.COMMON.PrintMode = HDS.Reporting.COMMON.PrintMode.PrintToPDF
        Dim myview As New DataView(adatatable)
        Dim sreport As String = MYC1RPT.Render(Globals.GetCurrentUser.ClientCode & "-" & Left(Globals.GetCurrentUser.UserName, 10), myoutputfolder, myreportpath, areportname, myview, HDS.Reporting.COMMON.PrintMode.PrintToPDF, areportprefix, asubtitle1, asubtitle2)
        If System.IO.File.Exists(sreport) Then
            Return "~\userdata\output\" & System.IO.Path.GetFileName(sreport)
        End If

        Return Nothing


    End Function
    Private Shared Function GetFileName(ByRef auser As CurrentUser, ByVal akeyvalue As String, ByVal aprefix As String, Optional ByVal aext As String = ".CSV", Optional ByVal suppressusername As Boolean = False) As String

        Dim mydir As String = auser.OutputFolder
        Dim S As String
        If suppressusername = True Then
            S = aprefix
        Else
            S = auser.UserName & "-" & aprefix
        End If
        Return mydir & "\" & TrimSpace(S & "-" & Format(Now(), "yyyyMMddHHmmss") & aext)

    End Function
    Friend Shared Function TrimSpace(ByRef strInput As String) As String
        ' This procedure trims extra space from any part of
        ' a string.
        Dim str As String = strInput.Trim
        Return Join(str.Split(" "), "")

    End Function
    'Public Function GetEntity(ByVal sql As String, ByRef myitem As HDS.DAL.Providers.ITable) As Boolean
    '    Dim myvw As DAL.COMMON.TableView = Provider.DBO.GetTableView(sql)
    '    If myvw.Count = 1 Then
    '        myvw.FillEntity(myitem)
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function
    'Public Function ExportToCSV(ByVal adatatable As DataTable, ByVal areportname As String, ByVal columns As ExportColumns) As String
    '    Return ExportToCSV(New DataView(adatatable), areportname, columns)
    'End Function
    'Public Function ExportToCSV(ByVal adatatable As DataView, ByVal areportname As String, ByVal columns As ExportColumns) As String
    '    'Dim myspreadsheetname As String = CRFView.GetFileName(GetCurrentUser, "", areportname)
    '    'Return Export.ExportToCSV(adatatable, myspreadsheetname, columns, True)

    'End Function
End Class
Public Class CurrentUser
    Public Id As Integer = -1
    Public UserName As String = "Guest"
    Public LoginCode As String = ""
    Public ClientCode As String = ""
    Public ClientName As String = ""
    Public Email As String = ""
    Public EmailServer As String = ""
    Public SettingsFolder As String
    Public OutputFolder As String
    Public UploadFolder As String
    Public ModuleList As String
    Public Roles As String
End Class
