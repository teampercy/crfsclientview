Imports System.Management
Imports System.IO
Imports System.Windows.Forms
Imports iTextSharp.text.pdf
Public Class PDFMERGE

    Public Shared Function ProccessFolder(ByVal sFolderPath As String) As String
        Dim oFolderInfo As New System.IO.DirectoryInfo(sFolderPath)
        Dim sOutFilePath As String = sFolderPath & "\" & oFolderInfo.Name & ".pdf"
        If IO.File.Exists(sOutFilePath) Then
            Try
                IO.File.Delete(sOutFilePath)
            Catch ex As Exception
                '  txtOutput.Text += sFolderPath & vbTab & ex.Message & vbCrLf
                'Exit Sub
            End Try
        End If

        Dim oFiles As String() = Directory.GetFiles(sFolderPath)

        Dim oPdfDoc As New iTextSharp.text.Document()
        Dim oPdfWriter As PdfWriter = PdfWriter.GetInstance(oPdfDoc, New FileStream(sOutFilePath, FileMode.Create))
        'If e Then
        '    oPdfWriter.SetEncryption(PdfWriter.STRENGTH40BITS, txtPassword.Text, txtPassword.Text, PdfWriter.AllowCopy)
        'End If
        oPdfDoc.Open()

        For i As Integer = 0 To oFiles.Length - 1
            Dim sFromFilePath As String = oFiles(i)
            Dim oFileInfo As New FileInfo(sFromFilePath)
            Dim sFileType As String = "ALL"
            Dim sExt As String = UCase(oFileInfo.Extension).Substring(1, 3)

            Try
                Select Case sFileType
                    Case "ALL"
                        If sExt = "PDF" Then
                            AddPdf(sFromFilePath, oPdfDoc, oPdfWriter)
                        ElseIf sExt = "JPG" Or sExt = "TIF" Then
                            AddImage(sFromFilePath, oPdfDoc, oPdfWriter)
                        End If

                    Case "PDF"
                        If sExt = "PDF" Then
                            AddPdf(sFromFilePath, oPdfDoc, oPdfWriter)
                        End If

                    Case "JPG", "TIF"
                        If sExt = "JPG" Or sExt = "TIF" Then
                            AddImage(sFromFilePath, oPdfDoc, oPdfWriter)
                        End If

                End Select

            Catch ex As Exception
                Throw New Exception("PDF MERGE ERROR:" & vbCrLf & ex.Message)
            End Try

        Next

        Try
            oPdfDoc.Close()
            oPdfWriter.Close()
        Catch ex As Exception
            Throw New Exception("PDF MERGE ERROR:" & vbCrLf & ex.Message)
            Try
                IO.File.Delete(sOutFilePath)
            Catch ex2 As Exception
                Throw New Exception("PDF MERGE ERROR:" & vbCrLf & ex.Message)
            End Try
        End Try


        Dim oFolders As String() = Directory.GetDirectories(sFolderPath)
        For i As Integer = 0 To oFolders.Length - 1
            Dim sChildFolder As String = oFolders(i)
            Dim iPos As Integer = sChildFolder.LastIndexOf("\")
            Dim sFolderName As String = sChildFolder.Substring(iPos + 1)
            ProccessFolder(sChildFolder)
        Next

        Return sOutFilePath

    End Function

    Private Shared Sub AddPdf(ByVal sInFilePath As String, ByRef oPdfDoc As iTextSharp.text.Document, ByVal oPdfWriter As PdfWriter)

        Dim oDirectContent As iTextSharp.text.pdf.PdfContentByte = oPdfWriter.DirectContent
        Dim oPdfReader As iTextSharp.text.pdf.PdfReader = New iTextSharp.text.pdf.PdfReader(sInFilePath)
        Dim iNumberOfPages As Integer = oPdfReader.NumberOfPages
        Dim iPage As Integer = 0

        Do While (iPage < iNumberOfPages)
            iPage += 1
            oPdfDoc.SetPageSize(oPdfReader.GetPageSizeWithRotation(iPage))
            oPdfDoc.NewPage()

            Dim oPdfImportedPage As iTextSharp.text.pdf.PdfImportedPage = oPdfWriter.GetImportedPage(oPdfReader, iPage)
            Dim iRotation As Integer = oPdfReader.GetPageRotation(iPage)
            If (iRotation = 90) Or (iRotation = 270) Then
                oDirectContent.AddTemplate(oPdfImportedPage, 0, -1.0F, 1.0F, 0, 0, oPdfReader.GetPageSizeWithRotation(iPage).Height)
            Else
                oDirectContent.AddTemplate(oPdfImportedPage, 1.0F, 0, 0, 1.0F, 0, 0)
            End If
        Loop

    End Sub

    Private Shared Sub AddImage(ByVal sInFilePath As String, ByRef oPdfDoc As iTextSharp.text.Document, ByVal oPdfWriter As PdfWriter)
        Dim oImage As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance(sInFilePath)
        Dim oDirectContent As iTextSharp.text.pdf.PdfContentByte = oPdfWriter.DirectContent

        Dim iWidth As Single = oImage.Width
        Dim iHeight As Single = oImage.Height

        oImage.SetAbsolutePosition(1, 1)
        oPdfDoc.SetPageSize(New iTextSharp.text.Rectangle(iWidth, iHeight))
        oPdfDoc.NewPage()
        oDirectContent.AddImage(oImage)
    End Sub

End Class
