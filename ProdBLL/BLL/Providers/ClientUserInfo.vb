Imports CRF.CLIENTVIEW
Imports CRF.CLIENTVIEW.BLL.CRFDB
Public Class ClientUserInfo
    Dim myds As DataSet
    Dim _client As New TABLES.Client
    Dim _mainclient As New TABLES.Client
    Dim _collinfo As New TABLES.ClientCollectionInfo
    Dim _lieninfo As New TABLES.ClientLienInfo
    Dim _user As New TABLES.Portal_Users
    Dim _contract1 As New TABLES.ClientContract
    Dim _contract2 As New TABLES.ClientContract
    Dim _contracts As DAL.COMMON.TableView
    Public Sub New(ByVal auserid)
        MyBase.New()
        Dim mysproc As New SPROCS.uspbo_ClientView_GetClientInfoForUser
        mysproc.UserId = auserid
        myds = Provider.DBO.GetDataSet(mysproc)

        Dim myview As New HDS.DAL.COMMON.TableView(myds.Tables(0))
        myview.FillEntity(_client)

        myview = New HDS.DAL.COMMON.TableView(myds.Tables(1))
        myview.FillEntity(_collinfo)
        myview = New HDS.DAL.COMMON.TableView(myds.Tables(2))
        myview.FillEntity(_lieninfo)
        _contracts = New HDS.DAL.COMMON.TableView(myds.Tables(3))

        myview = New HDS.DAL.COMMON.TableView(myds.Tables(4))
        myview.FillEntity(_mainclient)

        myview = New HDS.DAL.COMMON.TableView(myds.Tables(5))
        myview.FillEntity(_user)


        _user.RoleList = ""
        If _user.InternalUser = 1 And _user.IsClientViewManager = 1 Then
            _user.RoleList = "COLLECTVIEW,LIENVIEW,ADMIN,JOBVIEW"
            _user.ClientId = 10706
        End If
        If _user.InternalUser = 1 And _user.IsClientViewManager = 0 Then
            _user.RoleList = "COLLECTVIEW,LIENVIEW,JOBVIEW"
            _user.ClientId = 10706
        End If

        If _user.InternalUser = 0 And Me.HasLienService = True Then
            _user.RoleList = "LIENVIEW"
        End If
        If _user.InternalUser = 0 And Me.HasCollectionService = True Then
            _user.RoleList = _user.RoleList & "," & "COLLECTVIEW"
        End If
        If _user.InternalUser = 0 And Me.HasJobViewService = True Then
            _user.RoleList = _user.RoleList & "," & "JOBVIEW"
        End If

        '   10/24/2017  Added ADMIN to RoleList
        If _user.InternalUser = 0 And _user.IsClientViewAdmin = True Then
            _user.RoleList = _user.RoleList & "," & "ADMIN"
        End If

        If Left(_user.RoleList, 1) = "," Then
            _user.RoleList = Mid(_user.RoleList, 2)
        End If

        If _user.isinactive = 1 Then
            _user.RoleList = ""
        End If

        Provider.DBO.Update(_user)

    End Sub

    Public ReadOnly Property UserClientName() As String
        Get
            Dim s As String = _client.ClientCode & " - " & _user.UserName
            Return s
        End Get
    End Property
    Public ReadOnly Property Client() As TABLES.Client
        Get
            Return _client
        End Get
    End Property
    Public ReadOnly Property MainClientInfo() As TABLES.Client
        Get
            Return _mainclient
        End Get
    End Property
    Public ReadOnly Property LienInfo() As TABLES.ClientLienInfo
        Get
            Return _lieninfo
        End Get
    End Property
    Public ReadOnly Property CollInfo() As TABLES.ClientCollectionInfo
        Get
            Return _collinfo
        End Get
    End Property

    Public ReadOnly Property UserInfo() As TABLES.Portal_Users
        Get
            Return _user
        End Get

    End Property

    Public Function HasCollectionService() As Boolean
        _contracts.RowFilter = ""
        If _contracts.Count = 0 Then
            Return False
        End If
        _contracts.RowFilter = " ContractTypeId = 1 And IsClientViewCollection = 1 "
        If _contracts.Count < 1 Then
            Return False
        End If
        Return True

    End Function
    Public Function CollectionContractId() As TABLES.ClientContract
        _contracts.RowFilter = ""
        If _contracts.Count = 0 Then
            Return Nothing
        End If
        _contracts.RowFilter = " ContractTypeId = 1 And IsClientViewCollection = 1 "
        If _contracts.Count < 1 Then
            Return Nothing
        End If
        Dim myobj As New TABLES.ClientContract
        Return _contracts.FillEntity(myobj)

    End Function
    Public Function HasLienService() As Boolean
        _contracts.RowFilter = ""
        If _contracts.Count = 0 Then
            Return 0
        End If
        _contracts.RowFilter = " ContractTypeId = 2 And IsClientViewLiens = 1 "
        If _contracts.Count < 1 Then
            Return False
        End If
        Return True

    End Function
    Public Function HasJobViewService() As Boolean
        _contracts.RowFilter = ""
        If _contracts.Count = 0 Then
            Return 0
        End If
        _contracts.RowFilter = " ContractTypeId = 2 And IsClientViewJob = 1 "
        If _contracts.Count < 1 Then
            Return False
        End If
        Return True

    End Function
    Public Function LienContractId() As TABLES.ClientContract
        _contracts.RowFilter = ""
        If _contracts.Count = 0 Then
            Return Nothing
        End If
        _contracts.RowFilter = " ContractTypeId = 2 And IsClientViewLiens = 1 "
        If _contracts.Count < 1 Then
            Return Nothing
        End If

        Dim myobj As New TABLES.ClientContract
        Return _contracts.FillEntity(myobj)

    End Function

End Class
