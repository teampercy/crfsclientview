Imports Microsoft.VisualBasic
Imports System.Text
Imports System.Net.Mail
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Net
Imports System.IO
Imports System.Globalization
Imports System.Xml
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.VIEWS
Imports CRF.CLIENTVIEW.BLL.Common
Imports System.Configuration.ConfigurationManager
Namespace CollectView
    Public Class Provider
        Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Shared Function GetNotesForAccount(ByVal itemid As String, ByVal pdf As Boolean) As String
            Dim myreportname As String = "AccountNotes"
            Dim sql As String = "Select * from vwDebtAccountNotes where NoteTypeId Not In (2, 3, 4) and DebtAccountId = '" & itemid & "'"
            sql += " ORDER BY NOTEDATE DESC "
            Dim DS As System.Data.DataSet = DAL.GetDataSet(sql)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSCOLLECTIONS.XML", myreportname)
        End Function
        Public Shared Function GetClientContractInfo(ByVal acontractId As String) As CRFDB.TABLES.ClientContract
            Dim sql As String = "Select * from ClientContract Where Id = '" & acontractId & "' "
            Dim myview As HDS.DAL.COMMON.TableView = DAL.GetTableView(sql)
            Dim myitem As New CRFDB.TABLES.ClientContract
            myview.FillEntity(myitem)
            Return myitem

        End Function
        Public Shared Function GetNewAccountReport(ByVal userid As String) As String
            Dim myreportname As String = "AccountsSubmitted"
            Dim Dt As System.Data.DataTable = GetNewAccountList(userid, "", "")
            If Dt.Rows.Count = 0 Then Return Nothing
            Return Globals.RenderC1Report(Dt, "WEBREPORTSCOLLECTIONS.XML", myreportname)

        End Function
        Public Shared Function GetAccountList(ByVal userid As String, ByVal filter As Object, ByVal sort As String) As System.Data.DataTable
            Dim myclient As String = New ClientUserInfo(userid).Client.ClientId
            'Dim mysql As String = "Select * from vwAccountList where ClientId = '" & myclient & "' "
            'If IsNothing(filter) = False Then
            '    If filter.Length > 5 Then
            '        mysql += " And " & filter
            '    End If
            'End If
            Dim mysproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetAccountList
            mysproc = TryCast(filter, CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetAccountList)
            If IsNothing(mysproc) Then
                mysproc = New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetAccountList
                mysproc.UserId = userid
            End If
            Return DAL.GetDataSet(mysproc).Tables(0)

        End Function
        Public Shared Function GetAccountInfo(ByVal accountid As String) As System.Data.DataSet

            Dim mysproc As New SPROCS.cview_Clientview_GetAccountInfo
            mysproc.AccountId = accountid
            Return DAL.GetDataSet(mysproc)

        End Function
        Public Shared Function GetCollectionStatus() As HDS.DAL.COMMON.TableView
            Dim sql As String = "Select * from CollectionStatus order by CollectionStatus"
            Return DAL.GetTableView(sql)

        End Function

        Public Shared Function GetNewAccountList(ByVal userid As String, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim myitem As New TABLES.BatchDebtAccount
            Dim mysql As String = "Select * from vwbatchdebtaccount where BatchId = -1 and SubmittedByUserId= '" & userid & "' "
            If IsNothing(filter) = False Then
                If filter.Length > 5 Then
                    mysql += " And ( " & filter & " ) "
                End If
            End If
            Return DAL.GetDataSet(mysql).Tables(0)

        End Function
        Public Shared Function PrintCollectionLetter(ByVal auser As CurrentUser, ByVal aitemid As Integer, ByVal aaddressid As Integer, ByVal akeyid As Integer, ByVal alocationid As Integer) As String
            Dim mysproc As New SPROCS.cview_Processing_GetLetterInfo
            mysproc.DebtAccountId = akeyid
            mysproc.DebtAccountLetterId = aitemid
            mysproc.AddressId = aaddressid
            mysproc.LocationId = alocationid

            Dim myreport As New Collections.Letters.CollectionLetters(auser, "\Reports\CollectionLetters.xml")
            myreport.ExecuteC1Report(DAL.GetDataSet(mysproc))

            Dim s As String = "~/UserData/Output/" & Mid(myreport.ReportPath, InStrRev(myreport.ReportPath, "\") + 1)
            myreport.ReportPath = s
            Return myreport.ReportPath

        End Function
        Public Shared Function GetMasterReport(ByVal mysproc As SPROCS.cview_ClientView_GetAccountInventories, ByVal pdf As Boolean) As String
            Dim myreportname As String = "AccountsMaster"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))

            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSCOLLECTIONS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                Dim mycols As New ExportColumns

                mycols.Add(vwAccountList.ColumnNames.ClientCode)
                mycols.Add(vwAccountList.ColumnNames.FileNumber)
                mycols.Add(vwAccountList.ColumnNames.BranchNum)
                mycols.Add(vwAccountList.ColumnNames.AccountNo)
                mycols.Add(vwAccountList.ColumnNames.DebtorName)
                mycols.Add(vwAccountList.ColumnNames.ReferalDate)
                mycols.Add(vwAccountList.ColumnNames.LastServiceDate)
                mycols.Add(vwAccountList.ColumnNames.TotAsgAmt)
                mycols.Add(vwAccountList.ColumnNames.TotAsgRcvAmt)
                mycols.Add(vwAccountList.ColumnNames.TotCollFeeRcvAmt)
                mycols.Add(vwAccountList.ColumnNames.TotCollFeeAmt)
                mycols.Add(vwAccountList.ColumnNames.TotAdjNoCFAmt)
                mycols.Add(vwAccountList.ColumnNames.TotBalNoCFAmt)
                mycols.Add(vwAccountList.ColumnNames.CollectionStatusDescr)
                mycols.Add(vwAccountList.ColumnNames.CollectorName)

                Export.ExportToCSV(myview.Table, myspreadsheetpath, mycols, True)

                Return myspreadsheetpath
            End If

        End Function
        Public Shared Function GetAccountUpdateReport(ByVal mysproc As SPROCS.cview_ClientView_GetAccountNotes, ByVal pdf As Boolean) As String
            Dim myreportname As String = "AccountsUpdate"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))

            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSCOLLECTIONS.XML", myreportname)

        End Function
        Public Shared Function GetPaymentReport(ByVal mysproc As SPROCS.cview_ClientView_GetPaymentList, ByVal pdf As Boolean) As String
            Dim myreportname As String = "AccountsPayment"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSCOLLECTIONS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                Dim mycols As New ExportColumns

                mycols.Add(vwAccountLedger.ColumnNames.ClientCode)
                mycols.Add(vwAccountLedger.ColumnNames.FileNumber)
                mycols.Add(vwAccountLedger.ColumnNames.BranchNum)
                mycols.Add(vwAccountLedger.ColumnNames.AccountNo)
                mycols.Add(vwAccountLedger.ColumnNames.DebtorName)
                mycols.Add(vwAccountLedger.ColumnNames.TrxDate)
                mycols.Add(vwAccountLedger.ColumnNames.RcvByAgencyAmt)
                mycols.Add(vwAccountLedger.ColumnNames.FeeByAgencyAmt)
                mycols.Add(vwAccountLedger.ColumnNames.RcvByClientAmt)
                mycols.Add(vwAccountLedger.ColumnNames.FeeByClientAmt)
                mycols.Add(vwAccountLedger.ColumnNames.PrincDueAmt)
                mycols.Add(vwAccountLedger.ColumnNames.CollectorName)
                mycols.Add(vwAccountLedger.ColumnNames.RemitDate)

                Export.ExportToCSV(myview.Table, myspreadsheetpath, mycols, True)

                Return myspreadsheetpath

            End If

        End Function
        Public Shared Function GetClosedReport(ByVal mysproc As SPROCS.cview_ClientView_GetAccountInventories, ByVal pdf As Boolean) As String
            Dim myreportname As String = "AccountsClosed"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSCOLLECTIONS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                Dim mycols As New ExportColumns

                mycols.Add(vwAccountList.ColumnNames.ClientCode)
                mycols.Add(vwAccountList.ColumnNames.FileNumber)
                mycols.Add(vwAccountList.ColumnNames.BranchNum)
                mycols.Add(vwAccountList.ColumnNames.AccountNo)
                mycols.Add(vwAccountList.ColumnNames.DebtorName)
                mycols.Add(vwAccountList.ColumnNames.ReferalDate)
                mycols.Add(vwAccountList.ColumnNames.TotAsgAmt)
                mycols.Add(vwAccountList.ColumnNames.TotRcvAmt)
                mycols.Add(vwAccountList.ColumnNames.TotAdjAmt)
                mycols.Add(vwAccountList.ColumnNames.TotBalAmt)
                mycols.Add(vwAccountList.ColumnNames.CollectionStatusDescr)
                mycols.Add(vwAccountList.ColumnNames.CloseDate)
                mycols.Add(vwAccountList.ColumnNames.CollectorName)
                Export.ExportToCSV(myview.Table, myspreadsheetpath, mycols, True)

                Return myspreadsheetpath

            End If
        End Function
        Public Shared Function GetAckReport(ByVal itemid As String, ByVal pdf As Boolean) As String
            Dim mysql As String = "SELECT * FROM VWBatchDebtAccount WHERE BatchDebtAccountId='" & itemid & "'"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysql)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSCOLLECTIONS.XML", "AccountsAck")
        End Function
        Public Shared Function GetAccSubmittedReport(ByVal mysproc As SPROCS.cview_ClientView_GetAccountInventories, ByVal pdf As Boolean) As String
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSCOLLECTIONS.XML", "AccountsSubmitted")
        End Function

    End Class
End Namespace
