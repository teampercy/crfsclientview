Imports System.Collections.Generic
Imports System.Xml
Imports System.Configuration.ConfigurationManager
Imports System.Text.RegularExpressions
Imports System.Text
Imports System.Net
Imports System.IO
Imports System
Imports System.Xml.Serialization
Imports System.Web
Imports System.Collections.Specialized
Imports System.Security.Cryptography
Imports System.Xml.XPath
Imports CRF.CLIENTVIEW.BLL.CRFDB
Namespace WebService
    Public Class Provider
        Public Shared Function DoTask(ByVal xmlstring As String) As String
             Return DoTask(request.GetRequestFromXML(xmlstring))
        End Function
        Public Shared Function DoTask(ByRef arequest As request) As String
            Select Case arequest.type.ToLower

                Case Else
                    Return False

            End Select

        End Function
    End Class
    Public Class request
        Public type As String = String.Empty
        Public starttime As Date
        Public endtime As Date
        Public status As String
        Public message As String
        Public arguments As New Dictionary(Of String, String)
        Public values As New Dictionary(Of String, String)
        Public response As String = String.Empty
        Public Shared Function GetRequestinXML(ByVal arequest As request) As String
            Dim sb As New StringBuilder
            sb.AppendLine("<?xml version=""1.0""?>")
            sb.AppendLine("<Request>")
            sb.AppendLine("<StartTime>" & arequest.starttime & "</StartTime>")
            sb.AppendLine("<EndTime>" & arequest.endtime & "</EndTime>")
            sb.AppendLine("<Status>" & arequest.status & "</Status>")
            sb.AppendLine("<Message>" & arequest.message & "</Message>")
            sb.AppendLine("<Arguments>")
            For Each KEYV As KeyValuePair(Of String, String) In arequest.arguments
                sb.AppendLine("<" & KEYV.Key.ToLower & ">" & KEYV.Value & "</" & KEYV.Key.ToLower & ">")
            Next
            sb.AppendLine("</Arguments>")

            sb.AppendLine("<Results>")
            For Each KEYV As KeyValuePair(Of String, String) In arequest.values
                sb.AppendLine("<" & KEYV.Key.ToLower & ">!CDATA[" & KEYV.Value & "]]</" & KEYV.Key.ToLower & ">")
            Next
            sb.AppendLine("</Results>")

            sb.AppendLine("</Request>")
            Return sb.ToString

        End Function
        Public Shared Function GetRequestFromXML(ByRef xml As String) As request
            Dim myreq As New request
            Dim xmldoc As New XmlDocument
            xmldoc.LoadXml(xml)
            Dim myitem As XmlNodeList = xmldoc.GetElementsByTagName("Arguments")
            For Each record As XmlNode In myitem
                For Each field As XmlNode In record.ChildNodes
                    myreq.arguments.Add(field.Name.ToLower, field.InnerText)
                Next
            Next
            Return myreq
        End Function
    
    End Class
End Namespace
