Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.VIEWS
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES

Imports Microsoft.VisualBasic
Imports System.Collections.Generic
Imports System.Text
Imports System.Web
Imports System.Configuration
Imports System.Web.Configuration
Imports System.Globalization
Imports System.Configuration.ConfigurationManager
Imports System
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel
Imports System.Text.RegularExpressions
Imports System.Reflection
Imports System.Data
Imports System.Data.Common
Namespace Providers
    Public Class WebSite
        Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Shared Function GetReport(ByVal auserid As Integer, ByVal areportid As Integer, ByVal aoutputfolder As String) As String
            Dim MYUSER As New Portal_Users
            DAL.Read(auserid, MYUSER)
            Dim MYREPORT As New Tasks_HistoryAttachment
            DAL.Read(areportid, MYREPORT)
            Dim MYFILE As String = aoutputfolder & MYREPORT.url
            If System.IO.File.Exists(MYFILE) Then
                Return MYFILE
            End If
            PDFLIB.WriteStream(MYREPORT.image, MYFILE)
            If System.IO.File.Exists(MYFILE) Then
                Return MYFILE
            End If
            Return Nothing

        End Function



    End Class
End Namespace
