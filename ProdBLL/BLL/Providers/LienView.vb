Imports Microsoft.VisualBasic
Imports System.Text
Imports System.Net.Mail
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Net
Imports System.IO
Imports System.Globalization
Imports System.Data
Imports System.Xml
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.VIEWS
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Namespace LienView
    Public Class Provider
        Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Shared ReadOnly Property UserInfo() As ClientUserInfo
            Get
                ' Return New ClientUserInfo(GetCurrentUser.Id)

            End Get
        End Property
        Public Shared Function GetNewJobList(ByVal userid As String, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim myitem As New TABLES.BatchDebtAccount
            Dim mysql As String = "Select * from vwbatchjobs where BatchId = -1 and SubmittedByUserId= '" & userid & "' "
            If IsNothing(filter) = False Then
                If filter.Length > 0 Then
                    mysql += " And ( " & filter & " ) "
                End If
            End If
            '   2/7/2017
            If IsNothing(sort) = False Then
                If sort.Length > 0 Then
                    mysql += " Order By " & sort
                End If
            End If
            Return DAL.GetDataSet(mysql).Tables(0)

        End Function
        Public Shared Function GetJobList(ByVal userid As String, ByVal sproc As Object) As System.Data.DataTable
            Dim mysproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV
            mysproc = DirectCast(sproc, CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV)
            If IsNothing(mysproc) Then
                Return Nothing
            End If
            Return DAL.GetDataSet(mysproc).Tables(0)
            
        End Function
        Public Shared Function GetJobById(ByVal id As String) As TABLES.Job
            Dim myitem As New TABLES.Job
            DAL.Read(id, myitem)
            Return myitem

        End Function
        Public Shared Function GetNoteById(ByVal id As String) As TABLES.JobHistory
            Dim myitem As New TABLES.JobHistory
            DAL.Read(id, myitem)
            Return myitem

        End Function
        Public Shared Function GetJobByInfo(ByVal id As String) As System.Data.DataSet
            Dim mysproc As New SPROCS.uspbo_ClientView_GetJobInfo
            mysproc.JobId = id
            Return DAL.GetDataSet(mysproc)

        End Function
        Public Shared Function GetJobViewByInfo(ByVal id As String) As System.Data.DataSet
            Dim mysproc As New SPROCS.uspbo_ClientView_GetJobInfoJV
            mysproc.JobId = id
            Return DAL.GetDataSet(mysproc)

        End Function
        Public Shared Function GetAdminUserList(ByVal auserid As String) As HDS.DAL.COMMON.TableView
            Dim mysproc As New SPROCS.uspbo_ClientView_GetAdminUserList
            mysproc.UserId = auserid
            Return DAL.GetTableView(mysproc)

        End Function
        Public Shared Function GetNewJobLegalParties(ByVal batchjobid As String) As DataTable
            Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetBatchJobLegalParties
            mysproc.BatchJobId = batchjobid
            '   Dim sql As String = "Select * from BatchJobLegalParties Where BatchJobId = '" & batchjobid & "' "
            Return DAL.GetDataSet(mysproc).Tables(0)

        End Function
        Public Shared Function GetClientLienInfo(ByVal aclientid As String) As CRFDB.TABLES.ClientLienInfo
            Dim sql As String = "Select * from ClientLienInfo Where ClientId = '" & aclientid & "' "
            Dim myview As HDS.DAL.COMMON.TableView = DAL.GetTableView(sql)
            Dim myitem As New CRFDB.TABLES.ClientLienInfo
            myview.FillEntity(myitem)
            Return myitem

        End Function
        Public Shared Function GetClientInfo(ByVal aclientid As String) As CRFDB.TABLES.Client
            Dim sql As String = "Select * from Client Where ClientId = '" & aclientid & "' "
            Dim myview As HDS.DAL.COMMON.TableView = DAL.GetTableView(sql)
            Dim myitem As New CRFDB.TABLES.Client
            myview.FillEntity(myitem)
            Return myitem

        End Function
        Public Shared Function GetStateInfo(ByVal astate As String) As CRFDB.TABLES.StateInfo
            Dim sql As String = "Select * from StateInfo Where StateInitials = '" & astate & "' "
            Dim myview As HDS.DAL.COMMON.TableView = DAL.GetTableView(sql)
            Dim myitem As New CRFDB.TABLES.StateInfo
            myview.FillEntity(myitem)
            Return myitem

        End Function

        Public Shared Function GetClientListForUser(ByVal auserid As String) As HDS.DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetClientListForUser
            mysproc.UserId = auserid
            Return DAL.GetTableView(mysproc)

        End Function
        Public Shared Function GetStateTable() As HDS.DAL.COMMON.TableView
            'vv *** Commented By Pooja 25/02/2021 *** vv
            ' Dim sql As String = "Select * from StateInfo "
            'vv ***   *** vv
            'vv *** Created By Pooja 25/02/2021 *** vv
            Dim sql As String = "Select * from StateInfo where ISNULL(Statealert,'') <>'Canada' order by StateName"
            'vv ***   *** vv
            Return DAL.GetTableView(sql)

        End Function
        Public Shared Function GetJVStatusList() As DAL.COMMON.TableView
            Dim mysql As String = "select *,JOBVIEWSTATUS + '-' + Description as FriendlyName from jobviewstatus"
            Return DAL.GetTableView(mysql)

        End Function
        Public Shared Function GetJVDeskList() As DAL.COMMON.TableView
            Dim mysql As String = "select *,DESKNUM + '-' + DeskName as FriendlyName from jobviewdesks"
            Return DAL.GetTableView(mysql)

        End Function
        Public Shared Function GetJVStatusClientList(ByVal uClientId As String) As DAL.COMMON.TableView
            Dim mysql As String = "select JobviewStatus+'-'+Description as FriendlyName,* from vwJobViewStatusClient where ClientId='" & uClientId & "'"
            Return DAL.GetTableView(mysql)

        End Function
        Public Shared Function GetJVDeskClientList(ByVal uClientId As String) As DAL.COMMON.TableView
            Dim mysql As String = "select DeskNum +'-'+ DeskName as FriendlyName,* from vwJobViewDeskClient where ClientId='" & uClientId & "'"
            Return DAL.GetTableView(mysql)

        End Function
        Public Shared Function GetWaiverStateForms(ByVal astate As String) As HDS.DAL.COMMON.TableView
            Dim sql As String = "Select * from StateForms Where FormType = 'W' "
            sql += " And StateCode = '" & astate & "' "
            Return DAL.GetTableView(sql)

        End Function
        Public Shared Function GetOtherStateForms(ByVal astate As String) As HDS.DAL.COMMON.TableView
            Dim sql As String = "Select * from StateFormNotices Where ClientView = 1 "
            sql += " And StateCode = '" & astate & "' "
            Return DAL.GetTableView(sql)

        End Function

        Public Shared Function GetClientSigners() As HDS.DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetClientSignersForUser
            mysproc.UserId = Globals.GetCurrentUser.Id
            Return DAL.GetTableView(mysproc)

        End Function
        Public Shared Function GetJobSubmittedReport(ByVal mysproc As SPROCS.uspbo_CLIENTVIEW_GetJobInventory, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobsPlaced"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCode)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCity)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobState)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobZip)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCounty)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.APNNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.GCName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.OwnerName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.LenderName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.BranchNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.StatusCode)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.StartDate)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.DateAssigned)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.NoticeSent)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobStatusDescr)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.PlacementSource)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.UserName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.SendAsIsBox)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.ResidentialBox)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
                Else
                    Return Nothing
                End If

            End If
        End Function


        Public Shared Function GetJobReviewReport(ByVal mysproc As SPROCS.uspbo_CLIENTVIEW_GetJobReview, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobsReviewed"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing
            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            Dim myspreadsheetpath As String = Globals.GetFileName("JobsReviewed")
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.JobId)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.CustNum)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.CustName)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.JobNum)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.JobName)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.JobAdd1)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.JobState)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.DateCreated)
            myview.ExportColumns.Add(vwReviewJobsList.ColumnNames.RequestedBy)


            If myview.ExportToCSV(myspreadsheetpath) = True Then
                Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
            Else
                Return Nothing
            End If

        End Function



        Public Shared Function JobListExport(ByVal mysproc As SPROCS.uspbo_ClientView_GetJobList1, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobListExport"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)

                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobState)
                'myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobBalance)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.StartDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.EndDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.NOCDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.LienDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.BondDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.SNDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.ForeclosureDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.BondSuitDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.FilterKey)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.ResidentialBox)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.UserName)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.BranchNum)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
                Else
                    Return Nothing
                End If

            End If
        End Function
        Public Shared Function JobListExportJV(ByVal mysproc As SPROCS.uspbo_ClientView_GetJobListJV, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobListExport"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)

                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobState)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobBalance)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.StartDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.EndDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.NOCDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.LienDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.BondDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.SNDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.ForeclosureDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.BondSuitDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.FilterKey)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JVStatusCode)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.StatusCode)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.ResidentialBox)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.UserName)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.ClientName)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.CollectorName)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.CollectorPhoneNo)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.BranchNum)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
                Else
                    Return Nothing
                End If

            End If
        End Function
        Public Shared Function JobListExportJVMonthly(ByVal mysproc As SPROCS.uspbo_ClientView_GetMonthlyJobListJV, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobListExport"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)

                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobState)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JobBalance)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.StartDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.EndDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.NOCDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.LienDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.BondDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.SNDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.ForeclosureDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.BondSuitDeadlineDate)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.FilterKey)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.JVStatusCode)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.StatusCode)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.ResidentialBox)
                myview.ExportColumns.Add(vwJobsListJV.ColumnNames.UserName)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
                Else
                    Return Nothing
                End If

            End If
        End Function

        Public Shared Function JobListExportTXJobReview(ByVal mysproc As SPROCS.uspbo_ClientView_GetTXReviewJobList, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobListExport"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)

                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.JobState)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.StartDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.EndDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.NOCDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.LienDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.BondDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.SNDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.ForeclosureDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.BondSuitDeadlineDate)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.FilterKey)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.StatusCode)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.ResidentialBox)
                myview.ExportColumns.Add(vwJobsList1.ColumnNames.UserName)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
                Else
                    Return Nothing
                End If

            End If
        End Function
        Public Shared Function GetLienAlert30Days(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As System.Data.DataTable
            Return GetLienAlert(userid, fromdate, thrudate).Tables(1)
        End Function
        Public Shared Function GetLienAlert60Days(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As System.Data.DataTable
            Return GetLienAlert(userid, fromdate, thrudate).Tables(2)
        End Function
        Public Shared Function GetLienAlertAll(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As System.Data.DataTable
            Return GetLienAlert(userid, fromdate, thrudate).Tables(0)
        End Function
        Public Shared Function GetLienAlert30DaysReport(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As String
            Return Globals.RenderC1Report(GetLienAlertForReport(userid, fromdate, thrudate).Tables(1), "WebReportsLiens.XML", "LienDaysRemaining30")
        End Function
        Public Shared Function GetLienAlert60DaysReport(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As String
            Return Globals.RenderC1Report(GetLienAlertForReport(userid, fromdate, thrudate).Tables(2), "WebReportsLiens.XML", "LienDaysRemaining60")
        End Function
        Public Shared Function GetLienAlertReport(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As String
            Return Globals.RenderC1Report(GetLienAlertForReport(userid, fromdate, thrudate).Tables(0), "WebReportsLiens.XML", "LienDaysRemaining")
        End Function
        Public Shared Function GetLienAlert(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String) As System.Data.DataSet
            Dim mysproc As New SPROCS.uspbo_ClientView_GetLienAlerts
            mysproc.FromDate = fromdate
            mysproc.ThruDate = thrudate
            mysproc.UserId = userid
            Return DAL.GetDataSet(mysproc)
        End Function
        Public Shared Function GetLienAlertForReport(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String) As System.Data.DataSet
            Dim mysproc As New SPROCS.uspbo_ClientView_GetLienAlerts_Report
            mysproc.FromDate = fromdate
            mysproc.ThruDate = thrudate
            mysproc.UserId = userid
            Return DAL.GetDataSet(mysproc)
        End Function
        Public Shared Function GetLienDaysRemainingWithEndDates(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As System.Data.DataTable
            Return GetLienDaysRemaining(userid, fromdate, thrudate).Tables(0)
        End Function
        Public Shared Function GetLienDaysRemainingWithEndDatesReport(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As String
            Return Globals.RenderC1Report(GetLienDaysRemaining(userid, fromdate, thrudate).Tables(0), "WebReportsLiens.XML", "LienDaysRemainingEndDate")
        End Function
        Public Shared Function GetLienDaysRemainingNoEndDates(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As System.Data.DataTable
            Return GetLienDaysRemaining(userid, fromdate, thrudate).Tables(1)
        End Function
        Public Shared Function GetLienDaysRemainingWithNoEndDatesReport(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As String
            Return Globals.RenderC1Report(GetLienDaysRemaining(userid, fromdate, thrudate).Tables(1), "WebReportsLiens.XML", "LienDaysRemainingStartDate")
        End Function
        Public Shared Function GetLienDaysRemaining(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String) As System.Data.DataSet
            Dim mysproc As New SPROCS.uspbo_ClientView_GetLiensDaysRemaining
            mysproc.FromDate = fromdate
            mysproc.ThruDate = thrudate
            mysproc.UserId = userid
            Return DAL.GetDataSet(mysproc)
        End Function
        Public Shared Function GetLienDaysRemainingReport(ByVal userid As String, ByVal fromdate As String, ByVal thrudate As String, ByVal pdf As Boolean) As String
            Dim mysproc As New SPROCS.uspbo_ClientView_GetAccountInventories
            mysproc.CloseDateFROM = fromdate
            mysproc.CloseDateTO = thrudate
            mysproc.UserId = userid
            mysproc.SelectByReferalDate = True
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", "AccountsMaster")

        End Function
        Public Shared Function GetWaiverDaysRemaining(ByVal userid As String) As System.Data.DataSet
            Dim mysproc As New SPROCS.uspbo_ClientView_GetWaiverDaysRemaining
            mysproc.UserId = userid
            Return DAL.GetDataSet(mysproc)
        End Function
        Public Shared Function GetWaiverDaysRemainingReport(ByVal userid As String) As String
            Dim mysproc As New SPROCS.uspbo_ClientView_GetWaiverDaysRemaining
            mysproc.UserId = userid
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", "WaiverAlert")

        End Function
        Public Shared Function GetInvoiceList(ByVal userid As String) As System.Data.DataSet
            Dim mysproc As New SPROCS.uspbo_ClientView_GetInvoiceAlertList
            mysproc.UserId = userid
            Return DAL.GetDataSet(mysproc)
        End Function
        Public Shared Function GetInvoiceListReport(ByVal userid As String) As String
            Dim mysproc As New SPROCS.uspbo_ClientView_GetInvoiceAlertList
            mysproc.UserId = userid
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", "AZInvoiceList")

        End Function
        Public Shared Function GetInvoiceReport(ByVal ajobid As String) As String
            Dim MYSQL As String = "SELECT * FROM vwJobInvoiceDetail WHERE JOBID = " & ajobid
            Dim DS As System.Data.DataSet = DAL.GetDataSet(MYSQL)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", "AZInvoiceDetail")

        End Function
        Public Shared Function GetNewJobAck(ByVal userid As String, ByVal id As String, ByVal pdf As Boolean) As String
            Dim mysproc As New CRFDB.SPROCS.uspbo_ClientView_GetJobsSubmitted
            With mysproc
                .SubmittedByUserId = userid
                .BatchJobId = id
            End With
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", "NewJobAck")

        End Function
        Public Shared Function GetNewJobsSubmitted(ByVal userid As String, ByVal pdf As Boolean) As String
            Dim mysproc As New CRFDB.SPROCS.uspbo_ClientView_GetJobsSubmitted
            With mysproc
                .SubmittedByUserId = userid
                .BatchJobId = 0
            End With
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", "JobsSubmitted")

        End Function
        Public Shared Function GetNoticesSentReport(ByVal mysproc As SPROCS.uspbo_CLIENTVIEW_GetJobNoticeSent, ByVal pdf As Boolean) As String
            Dim myreportname As String = "NoticeSent"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.ClientCode)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobId)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobName)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobCity)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobState)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobZip)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.JobCounty)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.APNNum)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.GCName)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.OwnerName)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.LenderName)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.BranchNum)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.StatusCode)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.DatePrinted)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.IsTX60Day)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.IsTX90Day)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.ResidentialBox)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.UserName)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If
        End Function
        Public Shared Function GetVerifiedReport(ByVal mysproc As SPROCS.uspbo_CLIENTVIEW_GetJobVerifiedSent, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobsVerified"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.ClientCode)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobId)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobName)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobCity)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobState)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobZip)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.JobCounty)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.APNNum)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.GCName)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.OwnerName)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.LenderName)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.BranchNum)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.StatusCode)
                myview.ExportColumns.Add(vwPrelimVerified.ColumnNames.VerifiedDate)


                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If
        End Function
        Public Shared Function GetFinalWaiverReport(ByVal mysproc As SPROCS.uspbo_CLIENTVIEW_GetFinalWaiver, ByVal pdf As Boolean) As String
            'Dim myreportname As String = "FinalWaivers"
            Dim myreportname As String = "WaiverReview"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd2)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCity)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobState)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobZip)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.PrivateJob)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.CustName)
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.CustRefNum)
                myview.ExportColumns.Add("DateCreated")
                myview.ExportColumns.Add("DatePrinted")
                myview.ExportColumns.Add("RequestedBy")
                myview.ExportColumns.Add("PaymentAmt")
                myview.ExportColumns.Add("StartDate")
                myview.ExportColumns.Add("ThroughDate")
                myview.ExportColumns.Add("IsPaidInFull")
                myview.ExportColumns.Add(vwPrelimSent.ColumnNames.FormCode)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If
        End Function
        Public Shared Function GetOpentItemsReport(ByVal mysproc As SPROCS.uspbo_CLIENTVIEW_GetJobInventory, ByVal pdf As Boolean) As String
            Dim myreportname As String = "OpenItem"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCode)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCity)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobState)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobZip)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCounty)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.APNNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.GCName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.OwnerName)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.BranchNum)
                myview.ExportColumns.Add(vwJobInventory.ColumnNames.StatusCode)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If
        End Function
        Public Shared Function GetTexasRequestAck(ByVal aitemid As Integer) As String
            Dim sql As String = "Select * from vwJobTexasRequests Where RequestId ='" & aitemid & "'"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(sql)
            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", "TexasRequestAck")

        End Function
        Public Shared Function GetTexasPendingReport(ByVal mysproc As SPROCS.uspbo_ClientView_GetTexasRequests, ByVal pdf As Boolean) As String
            Dim myreportname As String = "TexasReport"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.ClientCode)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.BranchNumber)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobId)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobName)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobState)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.APNNum)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.CustName)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.DateCreated)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.IsTX60Day)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.IsTX90Day)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.AmountOwed)
                myview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.MonthDebtIncurred)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

        End Function
        'Public Shared Function GetNotesForJob(ByVal itemid As String, ByVal pdf As Boolean) As String
        '    Dim myreportname As String = "JobNotes"
        '    Dim sql As String = "Select * from vwJobNoteHistory where JobId = '" & itemid & "'"
        '    sql = "select * from vwjobnotehistory where jobid =  " & itemid & vbCrLf
        '    sql += " union " & vbCrLf
        '    sql += " select * from vwjobnotehistoryAuto where jobid =  " & itemid & vbCrLf

        '    Dim DS As System.Data.DataSet = DAL.GetDataSet(sql)
        '    If DS.Tables(0).Rows.Count = 0 Then Return Nothing

        '    Dim S As String = Globals.RenderDataReport(DS.Tables(0), "JOBNOTES.DRL", myreportname)
        '    S = Strings.Replace(S, "\\", "\")
        '    Return S

        'End Function

        Public Shared Function GetNotesForJob(ByVal itemid As String, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobNotes"
            Dim sql As String = "select * from vwjobnotehistory where NoteTypeId Not In (2,3) AND jobid =  " & itemid & vbCrLf
            sql += " union " & vbCrLf
            sql += " select * from vwjobnotehistoryAuto where jobid =  " & itemid & vbCrLf

            Dim DS As System.Data.DataSet = DAL.GetDataSet(sql)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
        End Function
        Public Shared Function GetTexasStatementReport(ByVal MYSPROC As SPROCS.uspbo_ClientView_GetTexasStatements, ByVal pdf As Boolean) As String
            Dim myreportname As String = "TexasStatement"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(MYSPROC)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim S As String = Globals.RenderDataReport(DS.Tables(0), "TEXASSTATEMENT.DRL", myreportname)
            S = Strings.Replace(S, "\\", "\")
            Return S

        End Function
        Public Shared Function PrintNoJobWaiver(ByVal aitemid As Integer) As String
            'Dim myreport As New Liens.Reports.NoJobWaiverNotice(aitemid)
            'myreport.ExecuteC1Report()
            Dim myreport As New Liens.Reports.NoJobWaiverNoticeXML(aitemid)
            myreport.ExecuteC1Report(aitemid)
            Return myreport.ReportPath

        End Function
        Public Shared Function PrintJobWaiver(ByVal aitemid As Integer) As String
            'Dim myreport As New Liens.Reports.JobWaiverNotice(aitemid)
            'myreport.ExecuteC1Report()

            Dim myreport As New Liens.Reports.JobWaiverNoticeXML(aitemid)
            myreport.ExecuteC1Report(aitemid)
            Return myreport.ReportPath

            'PrelimAckBranchDaily
            'Dim fromdate As Date = "05/09/2019"
            'Dim thrudate As Date = "05/10/2019"

            'PrelimAckDaily
            'Dim fromdate As Date = "05/28/2019"
            'Dim thrudate As Date = "05/29/2019"

            'Dim report As New BLL.ScheduledReports.DayEndPrelimAcks(fromdate, thrudate)
            'report.DoTask("", 3)

            'DayEndPrelimSentReport
            'Dim fromdate As Date = "03/25/2019"
            'Dim thrudate As Date = "03/26/2019"
            'Dim DayEndPrelimSentReport As New BLL.ScheduledReports.DayEndPrelimSent
            'DayEndPrelimSentReport.DoTask("uspbo_LiensDayEnd_GetPrelimSentByEmail '" & fromdate & "','" & thrudate & "'", 3)

            'Return True

        End Function
        Public Shared Function PrintJobNotary(ByVal aitemid As Integer) As String
            'Dim myreport As New Liens.Reports.JobWaiverNotary(aitemid)
            'myreport.ExecuteC1Report()
            Dim myreport As New Liens.Reports.JobWaiverNotaryXML(aitemid)
            myreport.ExecuteC1Report(aitemid)
            Return myreport.ReportPath

        End Function
        'vv *** Created By Pooja 27/08/2020 *** vv
        Public Shared Function PrintNoJobNotary(ByVal aitemid As Integer) As String
            'Dim myreport As New Liens.Reports.JobWaiverNotary(aitemid)
            'myreport.ExecuteC1Report()
            Dim myreport As New Liens.Reports.NoJobWaiverNoticeXML(aitemid)
            'Dim myreport As New LienView.Provider.PrintNoJobWaiver(aitemid)

            myreport.ExecuteC1Report1(aitemid)
            Return myreport.ReportPath

        End Function
        Public Shared Function PrintStateForm(ByVal aitemid As Integer) As String
            Dim myreport As New Liens.Reports.JobOtherNotice()
            myreport.ExecuteC1Report(aitemid)
            Return myreport.ReportPath

        End Function
        Public Shared Function PrintPrelimNotice(ByVal aitemid As Integer) As String
            Dim myreport As New Liens.Reports.JobPrelimNotice
            myreport.ExecuteC1Report(aitemid)
            Return myreport.ReportPath
        End Function
        'Public Shared Function PrintOtherNotice(ByVal aitemid As Integer) As String
        '    Dim myreport As New Liens.Reports.JobOtherNotice(GetCurrentUser)
        '    myreport.ExecuteC1Report(aitemid)
        '    Return myreport.ReportPath
        'End Function
        'Public Shared Function GetCurrentUser() As CRF.BLL.Users.CurrentUser

        '    Dim incoming As HttpContext = HttpContext.Current
        '    Dim s As String() = Nothing
        '    Dim myuser As New CRF.BLL.Users.CurrentUser
        '    s = Strings.Split(incoming.Session("UserId"), "~")
        '    myuser.Id = s(0)
        '    myuser.UserName = s(1)
        '    myuser.Email = s(2)
        '    myuser.OutputFolder = Utils.GetOutputFolderPath
        '    myuser.SettingsFolder = Utils.GetSettingsFolderPath
        '    myuser.UploadFolder = Utils.GetUploadFolderPath

        '    Return myuser

        'End Function

        'Public Shared Function GetFileName(ByVal areportname As String) As String
        '    Dim myuser As New ClientUserInfo(GetCurrentUser.Id)
        '    Dim myprefix As String = myuser.Client.ClientCode
        '    myprefix += "-" & myuser.UserInfo.LoginCode
        '    myprefix += "-" & myuser.UserInfo.ID
        '    myprefix += "-" & areportname
        '    If System.IO.Directory.Exists(GetCurrentUser.OutputFolder) = False Then
        '        System.IO.Directory.CreateDirectory(GetCurrentUser.OutputFolder)
        '    End If
        '    Dim s As String = Common.FileOps.TrimSpace(GetCurrentUser.OutputFolder & myprefix & "-" & Strings.Format(Now(), "yyyyMMddhhmmss") & ".csv")
        '    Return s

        'End Function

        Public Shared Function GetServiceSummaryReportJV(ByVal mysproc As SPROCS.uspbo_ClientView_GetServiceSummaryJV, ByVal pdf As Boolean) As String
            Dim myreportname As String = "ServiceSummary"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)

            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim DR As DataRow = DS.Tables(0).NewRow
            DR("JobState") = "Grand Total"
            DR("VerifyAndSend") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyAndSend)", String.Empty))
            DR("VerifyOnly") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyOnly)", String.Empty))
            DR("SendAsIs") = Convert.ToInt32(DS.Tables(0).Compute("SUM(SendAsIs)", String.Empty))
            DR("VerifyOwnerOnly") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyOwnerOnly)", String.Empty))
            DR("VerifyOwnerSend") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyOwnerSend)", String.Empty))
            DR("VerifiedSent") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifiedSent)", String.Empty))
            DR("CancelledJob") = Convert.ToInt32(DS.Tables(0).Compute("SUM(CancelledJob)", String.Empty))
            DS.Tables(0).Rows.Add(DR)

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.JobState)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyAndSend)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyOnly)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.SendAsIs)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyOwnerOnly)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyOwnerSend)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifiedSent)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.CancelledJob)

            If myview.ExportToCSV(myspreadsheetpath) = True Then
                Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function GetServiceSummaryReportLV(ByVal mysproc As SPROCS.uspbo_ClientView_GetServiceSummaryLV, ByVal pdf As Boolean) As String
            Dim myreportname As String = "ServiceSummary"
            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)

            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim DR As DataRow = DS.Tables(0).NewRow
            DR("JobState") = "Grand Total"
            DR("VerifyAndSend") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyAndSend)", String.Empty))
            DR("VerifyOnly") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyOnly)", String.Empty))
            DR("SendAsIs") = Convert.ToInt32(DS.Tables(0).Compute("SUM(SendAsIs)", String.Empty))
            DR("VerifyOwnerOnly") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyOwnerOnly)", String.Empty))
            DR("VerifyOwnerSend") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifyOwnerSend)", String.Empty))
            DR("VerifiedSent") = Convert.ToInt32(DS.Tables(0).Compute("SUM(VerifiedSent)", String.Empty))
            DR("CancelledJob") = Convert.ToInt32(DS.Tables(0).Compute("SUM(CancelledJob)", String.Empty))
            DS.Tables(0).Rows.Add(DR)

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.JobState)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyAndSend)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyOnly)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.SendAsIs)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyOwnerOnly)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifyOwnerSend)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.VerifiedSent)
            myview.ExportColumns.Add(vwServiceSummaryList.ColumnNames.CancelledJob)

            If myview.ExportToCSV(myspreadsheetpath) = True Then
                Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function GetPortalUsers(ByVal UserId As Int32) As CRFDB.TABLES.Portal_Users
            Dim sql As String = "Select * from Portal_Users Where ID = " & UserId
            Dim myview As HDS.DAL.COMMON.TableView = DAL.GetTableView(sql)
            Dim myitem As New CRFDB.TABLES.Portal_Users
            myview.FillEntity(myitem)
            Return myitem

        End Function

        Public Shared Function GetLegalPartyDetails(ByVal LegalPartyId As Int32, ByVal JobLegalPartiesTypeCode As String) As CRFDB.TABLES.JobLegalParties
            Dim sql As String = "SELECT  top(1) Id,1 As IsPrimary,'CU' as TypeCode,ClientCustomer As AddressName,AddressLine1,AddressLine2,City,State,PostalCode,Telephone1
                                            ,Email As Email 
                                            FROM dbo.ClientCustomer cu WITH (NOLOCK) INNER JOIN
                                            dbo.Job (NOLOCK) ON cu.CustId = dbo.Job.CustId
                                            where  dbo.Job.Id = " & LegalPartyId & " and 'CU' = '" & JobLegalPartiesTypeCode & "' 
                                            UNION
                                            SELECT top(1)  Id,1 As IsPrimary,'GC' as TypeCode,GeneralContractor As AddressName,AddressLine1,AddressLine2,City,State,PostalCode,Telephone1 
                                            ,Email As Email 
                                            FROM dbo.ClientGeneralContractor gc WITH (NOLOCK) INNER JOIN
                                            dbo.Job (NOLOCK) ON gc.GenId = dbo.Job.GenId
                                            where  dbo.Job.Id = " & LegalPartyId & " and 'GC' = '" & JobLegalPartiesTypeCode & "' 
                                            UNION
                                            SELECT top(1) Id,1 As IsPrimary,'GC' as TypeCode,GeneralContractor As AddressName, gc.AddressLine1,gc.AddressLine2, gc.City,gc.State,gc.PostalCode,gc.Telephone1 
                                            ,Email As Email
                                            FROM dbo.ClientGeneralContractor gc WITH (NOLOCK) INNER JOIN
                                           dbo.JobLegalParties (NOLOCK) ON gc.GenId = dbo.JobLegalParties.SecondaryId
                                            where  dbo.JobLegalParties.Id = " & LegalPartyId & " and 'GC' = '" & JobLegalPartiesTypeCode & "'  and TypeCode = 'GC' and IsPrimary = 0
                                            UNION
                                            SELECT top(1) [Id] ,0 , TYPECODE = CASE MLAGENT 
                                              WHEN '1' THEN 'ML Agent'
                                              ELSE TypeCode
                                            END
                                            ,[AddressName]
                                            ,[AddressLine1]
                                            ,[AddressLine2]
                                            ,[City]
                                            ,[State]
                                            ,[PostalCode]
                                            ,[Telephone1] 
                                            ,'' As Email 
                                            FROM JOBLEGALPARTIES WITH (NOLOCK)
                                            where  Id = " & LegalPartyId & " and TypeCode <> 'GC' and TypeCode = '" & JobLegalPartiesTypeCode & "' "

            Dim myview As HDS.DAL.COMMON.TableView = DAL.GetTableView(Sql)
            Dim myitem As New CRFDB.TABLES.JobLegalParties
            myview.FillEntity(myitem)
            Return myitem
        End Function

        Public Shared Function GetStateForms(ByVal StateCode As String) As CRFDB.TABLES.StateForms
            Dim sql As String = "select top(1)* from StateForms where FormCode= 'RECENSION' and statecode = '" & StateCode & "' "
            Dim myview As HDS.DAL.COMMON.TableView = DAL.GetTableView(sql)
            Dim myitem As New CRFDB.TABLES.StateForms
            myview.FillEntity(myitem)
            Return myitem

        End Function

        Public Shared Function GetJobWaiverLogByInfo(ByVal JobId As Int32, ByVal FormId As Int32, ByVal AddressName As String) As System.Data.DataTable
            Dim DateCreated As DateTime = Today

            Dim mysql As String = "select * from JobWaiverLog
                                                where JobId = " & JobId & " 
                                                and DateCreated = '" & DateCreated & "' 
                                                and FormId = " & FormId & " 
                                                and MailToName = '" & AddressName & "' 
                                                and (PaymentAmt Is null or PaymentAmt='0.00') 
                                                and  (DisputedAmt Is null or DisputedAmt='0.00')"

            Return DAL.GetDataSet(mysql).Tables(0)

        End Function

        Public Shared Function GetJobWaiverLogByRecensionLetter(ByVal JobId As Int32, ByVal FormId As Int32, ByVal AddressName As String) As System.Data.DataTable
            Dim DateCreated As DateTime = Today

            Dim mysql As String = "select * from JobWaiverLog
                                                where JobId = " & JobId & " 
                                                and FormId = " & FormId & " 
                                                and MailToName = '" & AddressName & "' 
                                                and (PaymentAmt Is null or PaymentAmt='0.00') 
                                                and  (DisputedAmt Is null or DisputedAmt='0.00')"

            Return DAL.GetDataSet(mysql).Tables(0)

        End Function

        Public Shared Function GetLegalPartyDetailsDataTable(ByVal LegalPartyId As Int32, ByVal JobLegalPartiesTypeCode As String) As System.Data.DataTable

            Dim sql As String = "SELECT  top(1) Id,1 As IsPrimary,'CU' as TypeCode,ClientCustomer As AddressName,AddressLine1,AddressLine2,City,State,PostalCode,Telephone1
                                            ,Email As Email 
                                            FROM dbo.ClientCustomer cu WITH (NOLOCK) INNER JOIN
                                            dbo.Job (NOLOCK) ON cu.CustId = dbo.Job.CustId
                                            where  dbo.Job.Id = " & LegalPartyId & " and 'CU' = '" & JobLegalPartiesTypeCode & "' 
                                            UNION
                                            SELECT top(1)  Id,1 As IsPrimary,'GC' as TypeCode,GeneralContractor As AddressName,AddressLine1,AddressLine2,City,State,PostalCode,Telephone1 
                                            ,Email As Email 
                                            FROM dbo.ClientGeneralContractor gc WITH (NOLOCK) INNER JOIN
                                            dbo.Job (NOLOCK) ON gc.GenId = dbo.Job.GenId
                                            where  dbo.Job.Id = " & LegalPartyId & " and 'GC' = '" & JobLegalPartiesTypeCode & "' 
                                            UNION
                                            SELECT top(1) Id,1 As IsPrimary,'GC' as TypeCode,GeneralContractor As AddressName, gc.AddressLine1,gc.AddressLine2, gc.City,gc.State,gc.PostalCode,gc.Telephone1 
                                            ,Email As Email
                                            FROM dbo.ClientGeneralContractor gc WITH (NOLOCK) INNER JOIN
                                           dbo.JobLegalParties (NOLOCK) ON gc.GenId = dbo.JobLegalParties.SecondaryId
                                            where  dbo.JobLegalParties.Id = " & LegalPartyId & " and 'GC' = '" & JobLegalPartiesTypeCode & "'  and TypeCode = 'GC' and IsPrimary = 0
                                            UNION
                                            SELECT top(1) [Id] ,0 , TYPECODE = CASE MLAGENT 
                                              WHEN '1' THEN 'ML Agent'
                                              ELSE TypeCode
                                            END
                                            ,[AddressName]
                                            ,[AddressLine1]
                                            ,[AddressLine2]
                                            ,[City]
                                            ,[State]
                                            ,[PostalCode]
                                            ,[Telephone1] 
                                            ,'' As Email 
                                            FROM JOBLEGALPARTIES WITH (NOLOCK)
                                            where  Id = " & LegalPartyId & " and TypeCode <> 'GC' and TypeCode = '" & JobLegalPartiesTypeCode & "' "


            Return DAL.GetDataSet(sql).Tables(0)

        End Function

        Public Shared Function JobListExportJV(ByVal mysproc As SPROCS.uspbo_ClientView_GetRequiresReviewListJV, ByVal pdf As Boolean) As String
            Dim myreportname As String = "JobListExport"

            Dim DS As System.Data.DataSet = DAL.GetDataSet(mysproc)
            If DS.Tables(0).Rows.Count = 0 Then Return Nothing

            Dim myview As New HDS.DAL.COMMON.TableView(DS.Tables(0))
            If pdf = True Then
                Return Globals.RenderC1Report(DS.Tables(0), "WEBREPORTSLIENS.XML", myreportname)
            Else
                Dim myspreadsheetpath As String = Globals.GetFileName(myreportname)

                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.JobId)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.JobName)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.JobNum)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.ClientCustomer)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.CustRefNum)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.JobAdd1)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.JobState)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.JobBalance)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.EstBalance)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.StartDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.EndDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.NOCDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.LienDeadlineDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.BondDeadlineDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.SNDeadlineDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.ForeclosureDeadlineDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.BondSuitDeadlineDate)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.FilterKey)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.JVStatusCode)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.StatusCode)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.PublicJob)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.FederalJob)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.ResidentialBox)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.UserName)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.ClientName)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.CollectorName)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.CollectorPhoneNo)
                myview.ExportColumns.Add(vwRequiresReviewListJV.ColumnNames.BranchNum)

                If myview.ExportToCSV(myspreadsheetpath) = True Then
                    Return "~/UserData/Output/" & System.IO.Path.GetFileName(myspreadsheetpath)
                Else
                    Return Nothing
                End If

            End If
        End Function
   '^^*** Created by pooja 04/24/2021*** ^^
        Public Shared Function GetClientContractList(ByVal ClientId As String) As DAL.COMMON.TableView
            Dim mysql As String = "select IsNull(UseJobViewMenu, 0) as UseJobViewMenu,IsClientViewJob from ClientContract cc join Client c on cc.BillingClientId = c.ParentClientId where cc.ContractTypeId = 2 and c.ClientId  = '" & ClientId & "' "
            Return DAL.GetTableView(mysql)
            'Return DAL.GetDataSet(mysql).Tables
            '^^***   *** ^^

        End Function
    End Class
End Namespace
