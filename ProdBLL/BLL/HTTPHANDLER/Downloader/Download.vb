Imports System
Imports System.Web
Imports System.Web.HttpApplication
Imports System.Text
Imports System.Xml
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES

Namespace HTTPHandlers.Download
    Public Class Provider
        Implements IHttpHandler
        Public Sub New()
        End Sub
        Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property
        Public Sub ProcessRequest(ByVal context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest

            Dim response As HttpResponse = context.Response
            Dim request As HttpRequest = context.Request
            If request.QueryString.Count < 1 Then
                If IsNothing(request.UrlReferrer) = False Then
                    response.Redirect(request.UrlReferrer.ToString)
                Else
                    response.Redirect(ProviderBase.GetAbsoluteUrl("?LOGIN"))
                End If
            End If

            If request.QueryString.Count > 1 Then
                Dim MYITEMID As String = request.QueryString("itemid")
                Dim myuserid As String = request.QueryString("uid")
                If IsNothing(myuserid) = False And IsNothing(MYITEMID) = False Then
                    GetFile(context, myuserid, MYITEMID, "~/userdata/output/")
                Else
                    response.Redirect(ProviderBase.GetAbsoluteUrl("?LOGIN"))

                End If
            Else
                Dim S As String = request.QueryString(0)
                Dim myfilename As String = "~/userdata/output/" & S
                If System.IO.File.Exists(request.MapPath(myfilename)) = False Then
                    response.Redirect(request.UrlReferrer.ToString)
                End If

                DownloadFile(request.MapPath(myfilename))

            End If

        End Sub
        Public Sub DownloadFile(ByVal FileLoc As String)

            Dim objFile As New System.IO.FileInfo(FileLoc)
            Dim objResponse As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            If objFile.Exists Then
                objResponse.ClearContent()
                objResponse.ClearHeaders()
                objResponse.AppendHeader("content-disposition", "attachment; filename=" + objFile.Name.ToString)
                objResponse.AppendHeader("Content-Length", objFile.Length.ToString())

                Dim strContentType As String
                Select Case Strings.LCase(objFile.Extension)
                    Case ".txt" : strContentType = "text/plain"
                    Case ".htm", ".html" : strContentType = "text/html"
                    Case ".rtf" : strContentType = "text/richtext"
                    Case ".jpg", ".jpeg" : strContentType = "image/jpeg"
                    Case ".gif" : strContentType = "image/gif"
                    Case ".bmp" : strContentType = "image/bmp"
                    Case ".mpg", ".mpeg" : strContentType = "video/mpeg"
                    Case ".avi" : strContentType = "video/avi"
                    Case ".zip" : strContentType = "application/zip"
                    Case ".pdf" : strContentType = "application/pdf"
                    Case ".doc", ".dot" : strContentType = "application/msword"
                    Case ".csv", ".xls", ".xlt" : strContentType = "application/vnd.msexcel"
                    Case Else : strContentType = "application/octet-stream"
                End Select
                objResponse.ContentType = strContentType
                '   objResponse.WriteFile(objFile.FullName)
                objResponse.TransmitFile(objFile.FullName)
                objResponse.Flush()
                objResponse.Close()

            End If


        End Sub
        Public Sub GetReport(ByVal context As System.Web.HttpContext, ByVal ahistoryid As Integer, ByVal attachid As Integer, ByVal aoutputfolder As String)

            Dim response As HttpResponse = context.Response
            Dim request As HttpRequest = context.Request

             Dim myhistory As New Tasks_HistoryAttachment
            ProviderBase.DAL.Read(attachid, myhistory)

            Dim MYURL As String = ProviderBase.GetAbsoluteUrl("userdata/output/" & System.IO.Path.GetFileName(myhistory.url))
            If myhistory.historyid = ahistoryid Then
                Dim myfilename As String = request.MapPath("~/userdata/output/" & System.IO.Path.GetFileName(myhistory.url))
                If System.IO.File.Exists(myfilename) = False Then
                    PDFLIB.WriteStream(myhistory.image, myfilename)
                    If System.IO.File.Exists(myfilename) Then
                        myhistory.dateviewed = Now()
                        ProviderBase.DAL.Update(myhistory)

                        DownloadFile(request.MapPath("~/userdata/output/" & System.IO.Path.GetFileName(myhistory.url)))
                    End If
                Else
                    myhistory.dateviewed = Now()
                    ProviderBase.DAL.Update(myhistory)
                    DownloadFile(request.MapPath("~/userdata/output/" & System.IO.Path.GetFileName(myhistory.url)))
                End If

            Else
                response.Redirect(ProviderBase.GetAbsoluteUrl("?LOGIN"))
            End If

        End Sub

        Public Sub GetFile(ByVal context As System.Web.HttpContext, ByVal ahistoryid As Integer, ByVal aattachid As Integer, ByVal aoutputfolder As String)

            Dim response As HttpResponse = context.Response
            Dim request As HttpRequest = context.Request
            Dim MYhistory As New Report_History

            Dim MYREPORT As New Report_HistoryAttachment
            ProviderBase.DAL.Read(aattachid, MYREPORT)
            Dim MYURL As String = ProviderBase.GetAbsoluteUrl("userdata/output/" & System.IO.Path.GetFileName(MYREPORT.url))
            If MYREPORT.historyid = ahistoryid Then
                Dim myfilename As String = request.MapPath("~/userdata/output/" & System.IO.Path.GetFileName(MYREPORT.url))
                If System.IO.File.Exists(myfilename) = False Then
                    PDFLIB.WriteStream(MYREPORT.image, myfilename)
                    If System.IO.File.Exists(myfilename) Then
                        MYREPORT.dateviewed = Now()
                        ProviderBase.DAL.Update(MYREPORT)

                        ProviderBase.DAL.Read(MYREPORT.historyid, MYhistory)
                        MYhistory.dateviewed = Now()
                        ProviderBase.DAL.Update(MYhistory)

                        DownloadFile(request.MapPath("~/userdata/output/" & System.IO.Path.GetFileName(MYREPORT.url)))
                    End If
                Else
                    MYREPORT.dateviewed = Now()
                    ProviderBase.DAL.Update(MYREPORT)

                    ProviderBase.DAL.Read(MYREPORT.historyid, MYhistory)
                    MYhistory.dateviewed = Now()
                    ProviderBase.DAL.Update(MYhistory)

                    DownloadFile(request.MapPath("~/userdata/output/" & System.IO.Path.GetFileName(MYREPORT.url)))
                End If

            Else
                response.Redirect(ProviderBase.GetAbsoluteUrl("?LOGIN"))
            End If


        End Sub

    End Class
End Namespace