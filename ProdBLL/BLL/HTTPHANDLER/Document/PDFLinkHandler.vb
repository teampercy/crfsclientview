Imports System
Imports System.Web
Imports System.Web.HttpApplication
Imports System.Text
Imports System.Xml
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports System.IO
Imports System.Web.SessionState

Namespace HTTPHandlers.Document
    Public Class PDFLinkHandler
        Implements IHttpHandler, IReadOnlySessionState
        Public Sub New()
        End Sub
        Public ReadOnly Property IsReusable() As Boolean Implements System.Web.IHttpHandler.IsReusable
            Get
                Return False
            End Get
        End Property
        Public Sub ProcessRequest(ByVal context As System.Web.HttpContext) Implements System.Web.IHttpHandler.ProcessRequest

            Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(context.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
            If myuser Is Nothing Then
                context.Response.Redirect(ProviderBase.GetAbsoluteUrl("?LOGIN"))
            End If

            Dim requestedFile = context.Server.MapPath(context.Request.FilePath)
            SendContentTypeAndFile(context, requestedFile)

        End Sub


        Private Function SendContentTypeAndFile(ByVal context As HttpContext, ByVal strFile As String) As HttpContext
            context.Response.ContentType = GetContentType(strFile)
            context.Response.TransmitFile(strFile)
            context.Response.[End]()
            Return context
        End Function

        Private Function GetContentType(ByVal filename As String) As String
            Dim res As String = Nothing
            Dim fileinfo As FileInfo = New FileInfo(filename)

            If fileinfo.Exists Then

                Select Case fileinfo.Extension.Remove(0, 1).ToLower()
                    Case "pdf"
                        res = "application/pdf"
                        Exit Select
                End Select

                Return res
            End If

            Return Nothing
        End Function
    End Class



End Namespace