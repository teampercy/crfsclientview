Imports Microsoft.VisualBasic
Imports System
Imports System.Configuration.ConfigurationManager
Imports System.Xml.Serialization
Imports System.Collections.Generic
Imports System.IO
Imports System.Net
Imports System.Xml
Imports System.Web
Imports System.Collections.Specialized
Imports System.Text
Imports System.Security.Cryptography
Imports System.Xml.XPath
Imports System.Configuration
Imports System.Globalization
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel
Imports System.Text.RegularExpressions
Imports System.Reflection
Imports System.Data
Imports System.Windows.Forms
Imports CRF.CLIENTVIEW.BLL.CRFDB
Public Class ProviderBase
    Private Shared subscriptionid As String
    Private Shared associateTag As String
    Private Shared _userdocs As String = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) & "\Bookster"
    Private Shared _isInitialized As Boolean = False
    Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
        Get
            Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
        End Get
    End Property
   
    Public Shared Function GetUniqueId() As String
        Dim MYID As Guid = System.Guid.NewGuid()
        Dim S As String = DateTime.Now.ToString().GetHashCode().ToString("x").ToUpper

        Return S


    End Function
    Public Shared Function GetSite() As TABLES.Portal
        Dim myitem As New TABLES.Portal
        DAL.Read(1, myitem)

        Return myitem

    End Function
     Public Shared Function GetDataTable(ByVal ObjectName As String, ByVal MaxRows As Integer, ByVal afilter As String, ByVal asort As String) As System.Data.DataTable
        Dim MYSQL As String = DAL.GetSQL(ObjectName, MaxRows, afilter, asort)
        Return DAL.GetDataSet(MYSQL).Tables(0)

    End Function
    Friend Shared Function DownLoadImage(ByVal remoteurl As String, ByVal filename As String) As String
        Dim FileSavePath As String = AppSettings.Get("STORE_IMAGESPATH") & Strings.Right("000000" & filename, 10)
        If System.IO.File.Exists(FileSavePath) = True Then
            Return FileSavePath
        End If

        If remoteurl.Length > 1 Then
            Dim WebC As New System.Net.WebClient()
            Try
                Dim g As Drawing.Bitmap = New Drawing.Bitmap(WebC.OpenRead(remoteurl))
                g.Save(FileSavePath, Drawing.Imaging.ImageFormat.Jpeg)
                g.Dispose()

            Catch ex As Exception
                ' Whatever you are going to do for error handling
            Finally
                WebC.Dispose()
            End Try
        End If

        If System.IO.File.Exists(FileSavePath) = True Then
            Return FileSavePath
        Else
            Return AppSettings.Get("STORE_IMAGESPATH") & "NOIMAGE.GIF"
        End If

    End Function
    Friend Shared Function GetImageURL(ByVal remoteurl As String, ByVal filename As String) As String
        Dim myimagepath As String = AppSettings.Get("STORE_IMAGESPATH") & Strings.Right("000000" & filename, 10)
        If System.IO.File.Exists(myimagepath) = False Then
            DownLoadImage(remoteurl, filename)
        End If

        If System.IO.File.Exists(myimagepath) = True Then
            myimagepath = AppSettings.Get("STORE_URL") & "/IMAGES/" & Strings.Right("000000" & filename, 10)
            Return myimagepath
        End If

        myimagepath = AppSettings.Get("STORE_URL") & "/IMAGES/" & "NOIMAGE.GIF"
        Return myimagepath

    End Function
    Friend Shared Function GetImage(ByVal remoteurl As String, ByVal filename As String) As System.Drawing.Image
        Dim myimagepath As String = AppSettings.Get("STORE_IMAGESPATH") & Strings.Right("000000" & filename, 10)
        If System.IO.File.Exists(myimagepath) = False Then
            DownLoadImage(remoteurl, filename)
        End If

        If System.IO.File.Exists(myimagepath) = True Then
            Dim g1 As New System.Drawing.Bitmap(myimagepath)
            Return g1
        End If

        myimagepath = AppSettings.Get("STORE_IMAGESPATH") & "NOIMAGE.GIF"
        Dim G2 As New System.Drawing.Bitmap(myimagepath)
        Return G2

    End Function
    Public Shared Function GetAbsoluteUrl(ByVal apagename As String) As String
        Dim incoming As HttpContext = HttpContext.Current

        Dim Port As String = incoming.Request.ServerVariables("SERVER_PORT")
        If Port Is Nothing OrElse Port = "80" OrElse Port = "443" Then
            Port = ""
        Else
            Port = ":" + Port
        End If

        Dim Protocol As String = incoming.Request.ServerVariables("SERVER_PORT_SECURE")
        If Protocol Is Nothing OrElse Protocol = "0" Then
            Protocol = "http://"
        Else
            Protocol = "https://"
        End If
        Dim s As String = Protocol + incoming.Request.ServerVariables("SERVER_NAME") + Port + incoming.Request.ApplicationPath
        If apagename.Length < 1 Then Return s

        If Right(s, 1) = "/" Then
            s += apagename
        Else
            s += "/" & apagename
        End If

        Return s

    End Function
    Public Shared Sub SendEmailToUs(ByVal subject As String, ByVal body As String)

        Dim MYSITE As TABLES.Portal = GetSite()

        Dim msg As New MailMessage
        msg.From = New MailAddress(MYSITE.contactemail, MYSITE.contactname)
        msg.Subject = subject
        msg.To.Add(MYSITE.contactemail)
        msg.IsBodyHtml = False
        msg.Body = body

        Dim mymail As New Emails.EmailSender
        mymail.StartTask(msg, MYSITE.SMTPServer, MYSITE.smtpport, MYSITE.smtplogin, MYSITE.smtppassword, MYSITE.smtpenablessl)

    End Sub
    Public Shared Sub SendEmailToUser(ByVal email As String, ByVal subject As String, ByVal body As String)

        Dim MYSITE As TABLES.Portal = GetSite()

        Dim msg As New MailMessage
        msg.From = New MailAddress(MYSITE.contactemail, MYSITE.contactname)
        msg.Subject = subject
        msg.To.Add(email)
        msg.IsBodyHtml = False
        msg.Body = body

        msg.Subject = subject
        msg.IsBodyHtml = False
        msg.Body = body

        Dim mymail As New Emails.EmailSender
        mymail.StartTask(msg, MYSITE.SMTPServer, MYSITE.smtpport, MYSITE.smtplogin, MYSITE.smtppassword, MYSITE.smtpenablessl)

    End Sub
      Public Shared ReadOnly Property ErrorLogFolder() As String
        Get

            Dim MYPATH As String = AppSettings.Get("STORE_APPDATAPATH") & "\ERRORLOG\"
            If System.IO.Directory.Exists(MYPATH) = False Then
                System.IO.Directory.CreateDirectory(MYPATH)
            End If

            If System.IO.Directory.Exists(MYPATH) = True Then
                Return MYPATH
            End If
            Return Nothing

        End Get
    End Property
    Public Shared ReadOnly Property ImagesFolder() As String
        Get
            Dim MYPATH As String = AppSettings.Get("STORE_USERDATAPATH") & "\REPORTS\"
            If System.IO.File.Exists(MYPATH) = True Then
                Return MYPATH
            End If
            Return Nothing

        End Get
    End Property
    Public Shared ReadOnly Property UserDocFolder() As String
        Get
            Dim MYPATH As String = _userdocs
            If System.IO.Directory.Exists(MYPATH) = False Then
                System.IO.Directory.CreateDirectory(MYPATH)
            End If
            Return MYPATH

        End Get
    End Property
    Public Shared ReadOnly Property ReportOutputFolder() As String
        Get
            Dim MYPATH As String = _userdocs & "\PDFS\"
            If System.IO.Directory.Exists(MYPATH) = False Then
                System.IO.Directory.CreateDirectory(MYPATH)
            End If
            Return MYPATH

        End Get
    End Property
    Public Shared ReadOnly Property FileOutputFolder() As String
        Get
            Dim MYPATH As String = _userdocs & "\FilesOutput\"
            If System.IO.Directory.Exists(MYPATH) = False Then
                System.IO.Directory.CreateDirectory(MYPATH)
            End If
            Return MYPATH

        End Get
    End Property
    Public Shared ReadOnly Property FileInputFolder() As String
        Get
            Dim MYPATH As String = _userdocs & "\FilesInput\"
            If System.IO.Directory.Exists(MYPATH) = False Then
                System.IO.Directory.CreateDirectory(MYPATH)
            End If
            Return MYPATH

        End Get
    End Property
    Public Shared ReadOnly Property SQLQueriesFolder() As String
        Get
            Dim MYPATH As String = _userdocs & "\SQL\"
            If System.IO.File.Exists(MYPATH) = True Then
                Return MYPATH
            End If
            Return Nothing
        End Get
    End Property
    Public Shared Sub WriteToErrorLog(ByVal msg As String, ByVal stkTrace As String, ByVal title As String)

        'check and make the directory if necessary; this is set to look in the application
        'folder, you may wish to place the error log in another location depending upon the
        'the user's role and write access to different areas of the file system
        Dim MYFILE As String = ErrorLogFolder & "\errlog.txt"

        'check the file
        Dim fs As FileStream = New FileStream(MYFILE, FileMode.OpenOrCreate, FileAccess.ReadWrite)
        Dim s As StreamWriter = New StreamWriter(fs)
        s.Close()
        fs.Close()

        'log it
        Dim fs1 As FileStream = New FileStream(MYFILE, FileMode.Append, FileAccess.Write)
        Dim s1 As StreamWriter = New StreamWriter(fs1)
        s1.Write("Title: " & title & vbCrLf)
        s1.Write("Message: " & msg & vbCrLf)
        s1.Write("StackTrace: " & stkTrace & vbCrLf)
        s1.Write("Date/Time: " & DateTime.Now.ToString() & vbCrLf)
        s1.Write("===========================================================================================" & vbCrLf)
        s1.Close()
        fs1.Close()

    End Sub
    Public Shared Function WriteToEventLog(ByVal entry As String, _
                 Optional ByVal appName As String = "CompanyName", _
                 Optional ByVal eventType As _
                 EventLogEntryType = EventLogEntryType.Information, _
                 Optional ByVal logName As String = "ProductName") As Boolean

        Dim objEventLog As New EventLog

        Try

            'Register the Application as an Event Source
            If Not EventLog.SourceExists(appName) Then
                EventLog.CreateEventSource(appName, logName)
            End If

            'log the entry
            objEventLog.Source = appName
            objEventLog.WriteEntry(entry, eventType)
            Return True

        Catch Ex As Exception

            Return False

        End Try

    End Function
    Friend Shared Function GetLinesFromFile(ByVal afilename As String, ByVal deletefile As Boolean) As StringCollection
        Dim mylines As New StringCollection
        If System.IO.File.Exists(afilename) = False Then Return mylines

        HDS.DAL.COMMON.FileOps.ReadFromFile(afilename, mylines)
        If deletefile = True Then
            System.IO.File.Delete(afilename)
        End If
        Return mylines

    End Function
    Public Shared Function LogEvent(ByVal userid As String, ByVal category As String, ByVal message As String) As Integer

        Dim request As HttpRequest = HttpContext.Current.Request
        Dim mylog As New TABLES.Portal_EventLogs
        With mylog
            .categorytype = category
            .logdate = Now
            .ipaddress = request.ServerVariables("REMOTE_ADDR")
            .urlreferrer = request.UrlReferrer.ToString
            .url = request.RawUrl.ToString
            .userid = userid
            .message = message
            .url = request.Url.ToString
            .urlreferrer = request.UrlReferrer.ToString
        End With

        DAL.Create(mylog)
        Return mylog.PKID

    End Function
End Class
