Imports Microsoft.VisualBasic
Imports System.Configuration.ConfigurationManager
Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Namespace CRFDB
    Public Class Provider
        Private Shared _isInitialized As Boolean = False
        Private Shared _DBO As DAL.Providers.IDAL
        Public Shared ReadOnly Property DBO() As DAL.Providers.IDAL
            Get
                Initialize()
                Return _DBO
            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _DBO = DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
                _isInitialized = True
            End If
        End Sub
    End Class
End Namespace
