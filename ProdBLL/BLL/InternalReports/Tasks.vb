Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports System.Net.Mail
Namespace Tasks
#Region "Core Functions"
    Public Class Provider
        Private Shared current As System.Web.HttpContext = System.Web.HttpContext.Current
        Private Shared _isInitialized As Boolean = False
        Private Shared schedulerThread As System.Threading.Thread = Nothing
        Private Shared scheduler As Tasks.Scheduler
        Private Shared _DBO As DAL.Providers.IDAL
        Public Shared ReadOnly Property DAL() As DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Shared Function Execute() As Boolean
            If Not _isInitialized Then
                _isInitialized = True
                scheduler = New Tasks.Scheduler()
                Dim schedulerThreadstart As New System.Threading.ThreadStart(AddressOf scheduler.Start)
                schedulerThread = New System.Threading.Thread(schedulerThreadstart)
                schedulerThread.Start()
            End If
            Return True
        End Function
        Public Shared Function StopIt() As Boolean
            If _isInitialized = True Then
                scheduler.StopIt()
            End If
        End Function
        Public Shared Function GetTasks(ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim myitem As New VIEWS.vwTasksList
            Dim MYSQL As String = DAL.GetSQL("vwTasksList", maxrows, filter, sort)
            Return DAL.GetDataSet(MYSQL).Tables(0)
        End Function
        Public Shared Function GetTaskDefs(ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim myitem As New Tasks_TaskDef
            Dim MYSQL As String = DAL.GetSQL(myitem.TableObjectName, maxrows, filter, sort)
            Return DAL.GetDataSet(MYSQL).Tables(0)
        End Function
      

    End Class
    Public Enum FrequencyTypes
        OneTime
        Minutes
        Daily
        Weekly
        Monthly
    End Enum
    Public Interface ISchedulerJob
        Property IsValid() As Boolean
        Property ErrorMessage() As String
        Function Execute(ByRef atask As TABLES.Tasks_Task) As Boolean
        Function DoTask(Optional ByVal sql As String = "", Optional ByVal status As Integer = 3) As Boolean
        Function DoRequest(ByVal xmlstring As String) As String
    End Interface
    Public Class Scheduler
        Dim _eoj As Boolean = False
        Public Sub New()
            _eoj = False
        End Sub
        Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Sub Start()
            Do Until _eoj = True
                Dim mysql As String = "Select * from tasks_task where IsDisabled = 0 and NextRunDate <= '" & Today.ToString & "' "
                Dim dt As DataTable = DAL.GetDataSet(mysql).Tables(0)
                Dim dr As DataRow
                Dim mytask As New TABLES.Tasks_Task
                Dim mytaskdef As New TABLES.Tasks_TaskDef
                Dim myworker As Tasks.ISchedulerJob

                For Each dr In dt.Rows
                    Try

                        DAL.FillEntity(dr, mytask)
                        DAL.Read(mytask.PKID, mytaskdef)

                        myworker = LoadModule(mytask.type, mytask.assemblyfile)
                        If IsNothing(myworker) = False Then
                            myworker.Execute(mytask)
                        Else
                            mytask.isdisabled = True
                            mytask.lastmessage = "Invalid Type"
                            Tasks.Provider.DAL.Update(mytask)
                            Common.LOGGER.WriteToErrorLog(mytask.lastmessage, "", "SCHEDULER:" & mytask.taskname)
                        End If


                    Catch ex As Exception

                        mytask.isdisabled = True
                        mytask.lastmessage = ex.Message
                        DAL.Update(mytask)
                        Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & mytask.taskname)

                    End Try

                Next

                Thread.Sleep(30000)

            Loop

        End Sub
        Private Shared Function TaskIsValid(ByRef atask As TABLES.Tasks_Task) As Boolean
        End Function
        Private Shared Function TaskExecute(ByRef atask As TABLES.Tasks_Task) As Boolean
            If atask.isdisabled = False Then Return False

        End Function
        Public Sub StopIt()
            _eoj = True
        End Sub
        'Public Shared Function LoadModule(ByVal atype As String, ByVal assemblyfile As String) As Object
        '    If IsNothing(atype) Then Return Nothing
        '    If atype.Length < 1 Then Return Nothing

        '    Dim asm As System.Reflection.Assembly
        '    Dim args() As Object = Nothing
        '    Dim returnObj As Object = Nothing
        '    Dim s As String = Assembly.GetExecutingAssembly.FullName
        '    Dim Type As Type
        '    If assemblyfile.Length < 1 Then
        '        Type = Assembly.GetExecutingAssembly().GetType(atype)
        '    Else
        '        asm = Assembly.LoadFrom(assemblyfile)
        '        If IsNothing(asm) = False Then
        '            Type = asm.GetType(atype)
        '            If Type IsNot Nothing Then
        '                returnObj = asm.CreateInstance(atype)
        '                '   returnObj = DirectCast(Activator.CreateInstance(Type, args), Tasks.SchedulerJob)
        '                Return returnObj
        '            End If
        '        End If
        '    End If


        '    Return returnObj

        'End Function
        Public Shared Function LoadModule(ByVal atype As String, ByVal assemblyfile As String) As Object
            If IsNothing(atype) Then Return Nothing
            If atype.Length < 1 Then Return Nothing

            Dim mypath As String = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
            mypath += "\CRF.CLIENTVIEW.BLL.DLL"
            mypath = Strings.Replace(mypath.ToLower, "file:\", "")

            Dim asm As System.Reflection.Assembly
            Dim args() As Object = Nothing
            Dim returnObj As Object = Nothing
            Dim s As String = Assembly.GetExecutingAssembly.FullName
            Dim Type As Type
            If assemblyfile.Length < 1 Then
                Type = Assembly.GetExecutingAssembly().GetType(atype)
            Else
                asm = Assembly.LoadFrom(mypath)
                If IsNothing(asm) = False Then
                    Type = asm.GetType(atype.Trim)
                    If Type IsNot Nothing Then
                        returnObj = asm.CreateInstance(atype.Trim)
                        '   returnObj = DirectCast(Activator.CreateInstance(Type, args), Tasks.SchedulerJob)
                        Return returnObj
                    End If
                End If
            End If


            Return returnObj

        End Function
    End Class
    Public Class SchedulerJob
        Implements ISchedulerJob
        Dim _isdisabled As Boolean = True
        Dim _taskname As String = ""
        Dim _errormessage As String = ""
        Dim _frequency As FrequencyTypes = FrequencyTypes.Daily
        Dim _nextrundate As Date = Now()
        Dim _lastrundate As Date = Now()
        Dim _interval As Integer = 1
        Dim _fromhour As Integer = 0
        Dim _thruhour As Integer = 24
        Dim _task As New TABLES.Tasks_Task
        Dim _taskdef As New TABLES.Tasks_TaskDef
        Dim _taskhistory As New TABLES.Tasks_History
        Dim _taskattach As New TABLES.Tasks_HistoryAttachment
        Dim _tasksubscriber As New TABLES.Tasks_SubscriberHistory
        Dim _tasksettings As New TABLES.Tasks_Settings

        Dim _emailto As New List(Of System.Net.Mail.MailAddress)

        Public Sub New()
            Tasks.Provider.DAL.Read(1, _tasksettings)

        End Sub
        Public Function Execute(ByRef atask As TABLES.Tasks_Task) As Boolean Implements ISchedulerJob.Execute
            Dim myReturn As Boolean
            Try
                _task = atask

                ' Tasks_History
                _taskhistory.taskid = _task.PKID
                _taskhistory.startdate = Now
                Tasks.Provider.DAL.Create(_taskhistory)

                myReturn = DoTask()

                If myReturn = True Then
                    Schedule()
                End If

                ' Email
                If myReturn = True Then
                    SendEmailToUser()
                End If

            Catch ex As Exception

                _task.isdisabled = True
                _task.lastmessage = ex.Message
                Tasks.Provider.DAL.Update(_task)

            End Try


        End Function
        Public Overridable Function DoTask(Optional ByVal sql As String = "", Optional ByVal Status As Integer = 3) As Boolean Implements ISchedulerJob.DoTask
            Return False

        End Function
        Public Overridable Function DoRequest(ByVal xmlstring As String) As String Implements ISchedulerJob.DoRequest
            Return Nothing

        End Function
        Public ReadOnly Property Task() As TABLES.Tasks_Task
            Get
                Return _task
            End Get
        End Property
        Public ReadOnly Property Settings() As TABLES.Tasks_Settings
            Get
                Return _tasksettings
            End Get
        End Property

        Public ReadOnly Property EmailTo() As List(Of System.Net.Mail.MailAddress)
            Get
                Return _emailto
            End Get
        End Property

        'Public Sub SendEmailToUs(ByVal amessage As String, ByVal attachs As List(Of System.Net.Mail.Attachment))
        '    Dim mysettings As New TABLES.Tasks_Settings
        '    CRF.CLIENTVIEW.BLL.Providers.WebSite.DAL.Read(1, mysettings)

        '    Dim msg As New MailMessage
        '    msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)
        '    msg.To.Add(mysettings.adminemail)
        '    If Me.Task.ccemail.Length > 10 Then
        '        msg.To.Add(Me.Task.ccemail)
        '    End If

        '    msg.IsBodyHtml = False
        '    msg.Subject = Me.Task.subject
        '    msg.Body = Me.Task.description & vbCrLf & amessage

        '    If IsNothing(attachs) = False Then
        '        For Each item As Attachment In attachs
        '            msg.Attachments.Add(item)
        '        Next
        '    End If

        '    With mysettings
        '        Dim _smtp As New SmtpClient(.smtpserver)
        '        If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
        '            _smtp.Port = .smtpport
        '            _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
        '            _smtp.EnableSsl = .smtpenablessl
        '        End If
        '        _smtp.Send(msg)
        '        _smtp.Dispose()
        '    End With


        'End Sub

        Public Sub SendEmailToUser()
            Dim myrecips As New TABLES.Tasks_SubscriberHistory
            Dim mysettings As New TABLES.Tasks_Settings
            CRF.CLIENTVIEW.BLL.Providers.WebSite.DAL.Read(1, mysettings)

            Dim msg As New MailMessage
            msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)

            Dim mysql As String = "Select * from Tasks_SubscriberHistory Where HistoryId = '" & _taskhistory.PKID & "' "
            Dim myds As DataSet = Provider.DAL.GetDataSet(mysql)
            '
            If myds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In myds.Tables(0).Rows
                    Provider.DAL.FillEntity(dr, myrecips)
                    msg.To.Add(myrecips.Email)
                Next

                msg.IsBodyHtml = False
                msg.Subject = Me.Task.subject

                msg.Body += vbCrLf
                msg.Body += "The Report was Prepared on:" & Now.ToString
                msg.Body += vbCrLf
                msg.Body += "Click on the Link(s) below to retrieve your report"
                msg.Body += vbCrLf
                msg.Body += mysettings.reportserverurl & System.IO.Path.GetFileName(Me._taskattach.url)

                With mysettings
                    Dim _smtp As New SmtpClient(.smtpserver)
                    If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                        _smtp.Port = .smtpport
                        _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                        _smtp.EnableSsl = .smtpenablessl
                    End If
                    _smtp.Send(msg)
                    _smtp.Dispose()
                End With

            End If
        End Sub
        Public ReadOnly Property TaskDef() As TABLES.Tasks_TaskDef
            Get
                Return _taskdef
            End Get
        End Property

        Public Property IsDisabled() As Boolean Implements ISchedulerJob.IsValid
            Get
                If _task.isdisabled = True Then
                    Return True
                End If

                If _task.nextrundate > Now Then
                    Return False
                End If

                Dim hh As Integer = DatePart(DateInterval.Hour, Now)
                If hh >= _task.runfromhour And hh < _task.runuptohour Then
                    Return False
                Else
                    Return True
                End If

            End Get

            Set(ByVal value As Boolean)
                _isdisabled = value
            End Set

        End Property
        Private Sub Schedule()

            _task.lastrundate = Now

            Select Case _task.dateinterval

                Case FrequencyTypes.Minutes
                    _task.nextrundate = DateAdd(DateInterval.Hour, _task.number, Now)
                    Dim hh As Integer = DatePart(DateInterval.Hour, _task.nextrundate)
                    If hh > _task.runuptohour Then
                        _task.nextrundate = DateAdd(DateInterval.Day, 1, Today)
                    End If

                Case FrequencyTypes.Daily
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number, Today)

                Case FrequencyTypes.Monthly
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number + 6, Today)

                Case FrequencyTypes.Weekly
                    _task.nextrundate = DateAdd(DateInterval.Month, _task.number, Today)

                Case Else
                    _isdisabled = True

            End Select

            With _task
                .lastrundate = Now
                .lastmessage = ""
            End With

            Tasks.Provider.DAL.Update(_task)
            'UpdateTask()


        End Sub
 
        Public Sub CreateAttachment(ByVal afilepath As String)
            _taskattach = New TABLES.Tasks_HistoryAttachment
            _taskattach.datecreated = Now()
            _taskattach.historyid = _taskhistory.PKID
            '_taskattach.url = System.IO.Path.GetFileName(afilepath)
            _taskattach.url = afilepath
            Tasks.Provider.DAL.Create(_taskattach)

        End Sub

        Public Sub CreateRecipient()
            _tasksubscriber = New TABLES.Tasks_SubscriberHistory
            _tasksubscriber.historyid = _taskhistory.PKID

            Dim SS As String()
            Dim s As String

            SS = Strings.Split(_task.internaluserlist, ",")
            For Each s In SS
                If IsNumeric(s) = True Then
                    _tasksubscriber.subscriberid = s
                    '
                    Dim myuser As New TABLES.Portal_Users
                    ProviderBase.DAL.GetItem(myuser.TableObjectName, "ID", s, myuser)
                    _tasksubscriber.Email = myuser.Email
                    Tasks.Provider.DAL.Create(_tasksubscriber)
                End If

            Next

        End Sub
        Private Sub UpdateTask()
            With _task
                .lastrundate = Now
                .lastmessage = Me.ErrorMessage
                .isdisabled = _isdisabled
            End With
            Tasks.Provider.DAL.Update(_task)

            _taskhistory.taskid = _task.PKID
            _taskhistory.enddate = Now()
            _taskhistory.message = Me.ErrorMessage
            Tasks.Provider.DAL.Update(_taskhistory)

        End Sub
        Public Property ErrorMessage() As String Implements ISchedulerJob.ErrorMessage
            Get
                Return _errormessage
            End Get
            Set(ByVal value As String)
                _errormessage = value
            End Set
        End Property
        Friend Shared Function FixEmail(ByVal emailaddr As String) As String
            If IsDBNull(emailaddr) = True Then
                Return String.Empty
            End If
            Dim s As String = Strings.Replace(emailaddr.Trim, vbCrLf, "")
            Return s

        End Function
        Public Shared Function RenderC1Report(ByVal aview As DataView, ByVal areportdef As String, ByVal areportname As String, ByVal aoutputfolder As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String

            Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
            Dim myreportpath As String = areportdef
            Dim myoutputfolder As String = aoutputfolder
            Dim MYAGENCY As New CRFDB.TABLES.Agency
            CRFDB.Provider.DBO.Read(1, MYAGENCY)
            Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
            mylogo.LoadImage(MYAGENCY.AgencyLogo)
            MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
            MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter

            Dim mymode As HDS.Reporting.COMMON.PrintMode
            If pdf = True Then
                mymode = HDS.Reporting.COMMON.PrintMode.PrintToPDF
            Else
                mymode = HDS.Reporting.COMMON.PrintMode.PrinttoExcel
            End If

            'Dim sreport As String = MYC1RPT.Render(areportprefix, myoutputfolder, myreportpath, areportname, aview, HDS.Reporting.COMMON.PrintMode.PrintToPDF, areportprefix, asubtitle1, asubtitle2)
            Dim sreport As String = MYC1RPT.Render(areportprefix, myoutputfolder, myreportpath, areportname, aview, mymode, areportprefix, asubtitle1, asubtitle2)

            If System.IO.File.Exists(sreport) Then
                Return aoutputfolder & System.IO.Path.GetFileName(sreport)
            End If

            Return Nothing


        End Function
        'Public Function RenderC1Report(ByVal myview As DataView, ByVal areportdef As String, ByVal areportname As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
        '    Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
        '    Dim myreportpath As String = Settings.settingsfolder & areportdef
        '    Dim myoutputfolder As String = Settings.outputfolder

        '    Dim MYAGENCY As New CRFDB.TABLES.Agency
        '    CRFDB.Provider.DBO.Read(1, MYAGENCY)
        '    Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
        '    mylogo.LoadImage(MYAGENCY.AgencyLogo)
        '    MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
        '    MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter
        '    Dim mymode As HDS.Reporting.COMMON.PrintMode = HDS.Reporting.COMMON.PrintMode.PrintToPDF
        '    Dim sreport As String = MYC1RPT.Render(areportprefix, myoutputfolder, myreportpath, areportname, myview, HDS.Reporting.COMMON.PrintMode.PrintToPDF, "", asubtitle1, asubtitle2)
        '    If System.IO.File.Exists(sreport) Then
        '        Return sreport
        '    End If
        '    Return Nothing


        'End Function

       
    End Class
#End Region
   

End Namespace
