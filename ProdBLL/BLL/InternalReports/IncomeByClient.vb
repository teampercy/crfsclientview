Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports CRF.CLIENTVIEW.BLL.Tasks
Imports CRF.CLIENTVIEW.BLL
Namespace InternalReports
    '''<summary> 
    ''' Mail Job - Sending the mail message
    ''' or any other job you wish to be executed
    ''' Implements the ISchedulerJob interface 
    '''</summary> 
    Public Class IncomeByClient
        Inherits Tasks.SchedulerJob

        Dim fromdate As Date = Today
        Dim thrudate As Date = Today

        Dim mystatus As Integer = 2
        Public Overrides Function DoTask(Optional ByVal sql As String = "", Optional ByVal status As Integer = 3) As Boolean
            mystatus = status

            Dim myreq As New Tasks.request

            myreq.arguments("reportdef") = Settings.settingsfolder & "\Reports\InternalReports.XML"
            myreq.arguments("reportname") = "IncomeByClient"
            myreq.arguments("outputfolder") = Settings.outputfolder
            myreq.arguments("sql") = "cview_Reports_InternalReports 'IncomeByClient'"

            Render(myreq)

            Return True

        End Function

        Public Function Render(ByRef myreq As request) As Boolean
            Dim myreportdef As String = myreq.arguments("reportdef")
            Dim myreportname As String = myreq.arguments("reportname")
            Dim myoutputfolder As String = myreq.arguments("outputfolder")
            If System.IO.Directory.Exists(myoutputfolder) = False Then
                System.IO.Directory.CreateDirectory(myoutputfolder)
            End If

            Try
                Dim myds As DataSet = ProviderBase.DAL.GetDataSet(myreq.arguments("sql"))
                Dim myview As New DataView(myds.Tables(0))
                If myview.Count > 0 Then
                    Dim mypath As String = RenderC1Report(myview, myreportdef, myreportname, myoutputfolder, False, "", "", "")
                    Me.CreateAttachment(mypath)
                    Me.CreateRecipient()
                End If


            Catch ex As Exception
                Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & "Income By Client")

                Return False

            End Try
            'Common.FileOps.DirectoryClean(myoutputfolder, DateAdd(DateInterval.Day, 1, Today))

            Return True

        End Function

    End Class
End Namespace

