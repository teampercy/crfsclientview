Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports CRF.CLIENTVIEW.BLL.Tasks
Imports CRF.CLIENTVIEW.BLL
Namespace InternalReports
    '''<summary> 
    ''' Mail Job - Sending the mail message
    ''' or any other job you wish to be executed
    ''' Implements the ISchedulerJob interface 
    '''</summary> 
    Public Class JobsPlacedByState
        Inherits Tasks.SchedulerJob

        Dim fromdate As Date = Today
        Dim thrudate As Date = Today

        Dim mystatus As Integer = 2
        Public Overrides Function DoTask(Optional ByVal sql As String = "", Optional ByVal status As Integer = 3) As Boolean
            mystatus = status

            Dim myreq As New Tasks.request
            fromdate = DateAdd(DateInterval.Day, -3, Today)
            'fromdate = "05/01/2015"
            thrudate = Today

            myreq.arguments("reportdef") = Settings.settingsfolder & "\Reports\InternalReports.XML"

            myreq.arguments("reportname") = "PrelimPlacedByState"
            myreq.arguments("outputfolder") = Settings.outputfolder
            myreq.arguments("sql") = "select * from vwJobInfo Where DateAssigned >= '" & fromdate & "' And DateAssigned < '" & thrudate & "'"
            'If sql.Length > 1 Then
            '    myreq.arguments("sql") = sql
            '    Dim s As String = Strings.Replace(sql, "select * from vwJobInfo Where ", "")
            '    s = Strings.Replace(s, "'", "")
            '    s = Strings.Replace(s, " ", "")

            '    Dim ss As String() = Strings.Split(s, ",")
            '    thrudate = ss(1)
            'End If

            Render(myreq)

            Return True

        End Function
 
        Public Function Render(ByRef myreq As request) As Boolean
            Dim myreportdef As String = myreq.arguments("reportdef")
            Dim myreportname As String = myreq.arguments("reportname")
            Dim myoutputfolder As String = myreq.arguments("outputfolder")
            If System.IO.Directory.Exists(myoutputfolder) = False Then
                System.IO.Directory.CreateDirectory(myoutputfolder)
            End If

            Try
                Dim myds As DataSet = ProviderBase.DAL.GetDataSet(myreq.arguments("sql"))
                Dim myview As New DataView(myds.Tables(0))
                If myview.Count > 0 Then
                    Dim mypath As String = RenderC1Report(myview, myreportdef, myreportname, myoutputfolder, True, "", "", "")
                    Me.CreateAttachment(mypath)
                    Me.CreateRecipient()
                End If


            Catch ex As Exception
                Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & "Prelim Placed By State")

                Return False

            End Try

            Return True

        End Function

    End Class
End Namespace

