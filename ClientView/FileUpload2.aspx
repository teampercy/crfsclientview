﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FileUpload2.aspx.vb" Inherits="FileUpload2" %>

<%@ Register Src="~/App_Controls/_Custom/LienView/FileUpload.ascx" TagName="uploadFile" TagPrefix="uc7" %>
<%@ Register Src="~/App_Controls/_Custom/CollectView/debtaccountfileupload.ascx" TagName="debtaccountfileupload" TagPrefix="uc8" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
 
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <meta name="generator" content="hds vbjuice" />
    <meta id="metaKeywords" name="Keywords" content="" />
    <meta id="metaDescr" name="Description" content="" />
    <meta id="metaCopyRight" name="copyright" content="" />
   
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css">
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="assets/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="global/vendor/blueimp-file-upload/jquery.fileupload.css">
    <link rel="stylesheet" href="global/vendor/dropify/dropify.css">

    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
       <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <!-- Scripts -->
    <script src="global/vendor/modernizr/modernizr.js"></script>
    <script src="global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
        function MaintainMenuOpen()
        { }
  </script>

</head>
<body>
    <%--  <form id="myform" runat="server" method="post" enctype="multipart/form-data" >
    <div>
           <input type="file" id="myfile1" data-plugin="dropify" data-default-file="" />
  
         <asp:Button ID="btnSave" runat="server" Text="File Upload" OnClick="btnSave_Click" />
    </div>
            <div class="file-wrap container-fluid">
            <div class="file-list row"></div>
        </div>
    </form>--%>
    <form id="myform" enctype="multipart/form-data" runat="server" method="post" autocomplete="on" style="width: 97%; height: 100%;">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div>
            <uc7:uploadFile ID="UploadFile7" runat="server" />
        </div>
          <div>
            <uc8:debtaccountfileupload ID="uploadfile8" runat="server" />
        </div>
    
    </form>
    <!-- Core  -->
    <script src="global/vendor/jquery/jquery.js"></script>
    <script src="global/vendor/bootstrap/bootstrap.js"></script>
    <script src="global/vendor/animsition/animsition.js"></script>
    <script src="global/vendor/asscroll/jquery-asScroll.js"></script>
    <script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
    <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    <!-- Plugins -->
    <script src="global/vendor/switchery/switchery.min.js"></script>
    <script src="global/vendor/intro-js/intro.js"></script>
    <script src="global/vendor/screenfull/screenfull.js"></script>
    <script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
    <script src="global/vendor/jquery-ui/jquery-ui.js"></script>
    <script src="global/vendor/blueimp-tmpl/tmpl.js"></script>
    <script src="global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
    <script src="global/vendor/blueimp-load-image/load-image.all.min.js"></script>
    <script src="global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
    <script src="global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
    <script src="global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
    <script src="global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
    <script src="global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
    <script src="global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
    <script src="global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
    <script src="global/vendor/dropify/dropify.min.js"></script>
    <!-- Scripts -->
    <script src="global/js/core.js"></script>
    <script src="assets/js/site.js"></script>
    <script src="assets/js/sections/menu.js"></script>
    <script src="assets/js/sections/menubar.js"></script>
    <script src="assets/js/sections/sidebar.js"></script>
    <script src="global/js/configs/config-colors.js"></script>
    <script src="assets/js/configs/config-tour.js"></script>
    <script src="global/js/components/asscrollable.js"></script>
    <script src="global/js/components/animsition.js"></script>
    <script src="global/js/components/slidepanel.js"></script>
    <script src="global/js/components/switchery.js"></script>
    <script src="global/js/components/dropify.js"></script>
    <script src="assets/examples/js/forms/uploads.js"></script>
</body>
</html>
