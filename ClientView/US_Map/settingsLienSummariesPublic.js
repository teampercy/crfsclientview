var map_cfg = {
    "mapWidth": "75%",
    "mapHeight": "75%",
    
    "shadowWidth": 2,
    "shadowOpacity": 0.2,
    "shadowColor": "black",
    "shadowX": 0,
    "shadowY": 0,
    
    "iPhoneLink": true,
    "isNewWindow": true,
    
    "zoomEnable": false,
    "zoomEnableControls": false,
    "zoomIgnoreMouseScroll": false,
    "zoomMax": 8,
    "zoomStep": 0.8,
    
    "borderColor": "#ffffff",
    "borderColorOver": "#ffffff",
    "borderOpacity": 0.5,
    
    "nameColor": "#ffffff",
    "nameColorOver": "#ffffff",
    "nameFontSize": "10px",
    "nameFontWeight": "bold",
    
    "nameStroke": false,
    "nameStrokeColor": "#000000",
    "nameStrokeColorOver": "#000000",
    "nameStrokeWidth": 1.5,
    "nameStrokeOpacity": 0.5,
    
    "overDelay": 300,
    "nameAutoSize": false,
    
    "tooltipOnHighlightIn": false,
    "freezeTooltipOnClick": false,
    
    "mapId": "vcg",
    
    "map_data": {

    "st1": {
        "id": 1,
        "name": "Alabama",
        "shortname": "AL",
        "link": "Images/ResourceImages/SummaryPublic/ALPublicSum.pdf",
        "comment": "Alabama<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Alabama.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st2": {
        "id": 2,
        "name": "Alaska",
        "shortname": "AK ",
        "link": "Images/ResourceImages/SummaryPublic/AKPublicSum.pdf",
        "comment": "Alaska<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Alaska.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st3": {
        "id": 3,
        "name": "Arizona",
        "shortname": "AZ ",
        "link": "Images/ResourceImages/SummaryPublic/AZPublicSum.pdf",
        "comment": " Arizona<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Arizona.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st4": {
        "id": 4,
        "name": "Arkansas",
        "shortname": "AR",
        "link": "Images/ResourceImages/SummaryPublic/ARPublicSum.pdf",
        "comment": "Arkansas<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Arkansas.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st5": {
        "id": 5,
        "name": "California",
        "shortname": "CA",
        "link": "Images/ResourceImages/SummaryPublic/CAPublicSum.pdf",
        "comment": "California<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/California.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st6": {
        "id": 6,
        "name": "Colorado",
        "shortname": "CO",
        "link": "Images/ResourceImages/SummaryPublic/COPublicSum.pdf",
        "comment": "Colorado<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Colorado.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st7": {
        "id": 7,
        "name": "Connecticut",
        "shortname": "CT",
        "link": "Images/ResourceImages/SummaryPublic/CTPublicSum.pdf",
        "comment": "Connecticut<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Connecticut.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st8": {
        "id": 8,
        "name": "Delaware",
        "shortname": "DE",
        "link": "Images/ResourceImages/SummaryPublic/DEPublicSum.pdf",
        "comment": "Delaware<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Delaware.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st9": {
        "id": 9,
        "name": "District of Columbia",
        "shortname": "DC",
        "link": "Images/ResourceImages/SummaryPublic/DCPublicSum.pdf",
        "comment": "District of Columbia<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/DCFlag.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st10": {
        "id": 10,
        "name": "Florida",
        "shortname": "FL",
        "link": "Images/ResourceImages/SummaryPublic/FLPublicSum.pdf",
        "comment": "Florida<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Florida.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st11": {
        "id": 11,
        "name": "Georgia",
        "shortname": "GA",
        "link": "Images/ResourceImages/SummaryPublic/GAPublicSum.pdf",
        "comment": "Georgia<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Georgia.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st12": {
        "id": 12,
        "name": "Hawaii",
        "shortname": "HI",
        "link": "Images/ResourceImages/SummaryPublic/HIPublicSum.pdf",
        "comment": "Hawaii<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Hawaii.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st13": {
        "id": 13,
        "name": "Idaho",
        "shortname": "ID",
        "link": "Images/ResourceImages/SummaryPublic/IDPublicSum.pdf",
        "comment": "Idaho<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Idaho.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st14": {
        "id": 14,
        "name": "Illinois",
        "shortname": "IL",
        "link": "Images/ResourceImages/SummaryPublic/ILPublicSum.pdf",
        "comment": "Illinois<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Illinois.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st15": {
        "id": 15,
        "name": "Indiana",
        "shortname": "IN",
        "link": "Images/ResourceImages/SummaryPublic/INPublicSum.pdf",
        "comment": "Indiana<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Indiana.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st16": {
        "id": 16,
        "name": "Iowa",
        "shortname": "IA",
        "link": "Images/ResourceImages/SummaryPublic/IAPublicSum.pdf",
        "comment": "Iowa<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Iowa.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st17": {
        "id": 17,
        "name": "Kansas",
        "shortname": "KS",
        "link": "Images/ResourceImages/SummaryPublic/KSPublicSum.pdf",
        "comment": "Kansas<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Kansas.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st18": {
        "id": 18,
        "name": "Kentucky",
        "shortname": "KY",
        "link": "Images/ResourceImages/SummaryPublic/KYPublicSum.pdf",
        "comment": "Kentucky<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Kentucky.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st19": {
        "id": 19,
        "name": "Louisiana",
        "shortname": "LA",
        "link": "Images/ResourceImages/SummaryPublic/LAPublicSum.pdf",
        "comment": "Louisiana<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Louisiana.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st20": {
        "id": 20,
        "name": "Maine",
        "shortname": "ME",
        "link": "Images/ResourceImages/SummaryPublic/MEPublicSum.pdf",
        "comment": "Maine<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Maine.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st21": {
        "id": 21,
        "name": "Maryland",
        "shortname": "MD",
        "link": "Images/ResourceImages/SummaryPublic/MDPublicSum.pdf",
        "comment": "Maryland<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Maryland.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st22": {
        "id": 22,
        "name": "Massachusetts",
        "shortname": "MA",
        "link": "Images/ResourceImages/SummaryPublic/MAPublicSum.pdf",
        "comment": "Massachusetts<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Massachusetts.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st23": {
        "id": 23,
        "name": "Michigan",
        "shortname": "MI",
        "link": "Images/ResourceImages/SummaryPublic/MIPublicSum.pdf",
        "comment": "Michigan<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Michigan.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st24": {
        "id": 24,
        "name": "Minnesota",
        "shortname": "MN",
        "link": "Images/ResourceImages/SummaryPublic/MNPublicSum.pdf",
        "comment": "Minnesota<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Minnesota.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st25": {
        "id": 25,
        "name": "Mississippi",
        "shortname": "MS",
        "link": "Images/ResourceImages/SummaryPublic/MSPublicSum.pdf",
        "comment": "Mississippi<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Mississippi.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st26": {
        "id": 26,
        "name": "Missouri",
        "shortname": "MO",
        "link": "Images/ResourceImages/SummaryPublic/MOPublicSum.pdf",
        "comment": "Missouri<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Missouri.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st27": {
        "id": 27,
        "name": "Montana",
        "shortname": "MT",
        "link": "Images/ResourceImages/SummaryPublic/MTPublicSum.pdf",
        "comment": "Montana<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Montana.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st28": {
        "id": 28,
        "name": "Nebraska",
        "shortname": "NE",
        "link": "Images/ResourceImages/SummaryPublic/NEPublicSum.pdf",
        "comment": "Nebraska<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Nebraska.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st29": {
        "id": 29,
        "name": "Nevada",
        "shortname": "NV",
        "link": "Images/ResourceImages/SummaryPublic/NVPublicSum.pdf",
        "comment": "Nevada<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Nevada.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st30": {
        "id": 30,
        "name": "New Hampshire",
        "shortname": "NH",
        "link": "Images/ResourceImages/SummaryPublic/NHPublicSum.pdf",
        "comment": "New Hampshire<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/NewHampshire.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st31": {
        "id": 31,
        "name": "New Jersey",
        "shortname": "NJ",
        "link": "Images/ResourceImages/SummaryPublic/NJPublicSum.pdf",
        "comment": "New Jersey<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/NewJersey.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st32": {
        "id": 32,
        "name": "New Mexico",
        "shortname": "NM",
        "link": "Images/ResourceImages/SummaryPublic/NMPublicSum.pdf",
        "comment": "New Mexico<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/NewMexico.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st33": {
        "id": 33,
        "name": "New York",
        "shortname": "NY",
        "link": "Images/ResourceImages/SummaryPublic/NYPublicSum.pdf",
        "comment": "New York<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/NewYork.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st34": {
        "id": 34,
        "name": "North Carolina",
        "shortname": "NC",
        "link": "Images/ResourceImages/SummaryPublic/NCPublicSum.pdf",
        "comment": "North Carolina<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/NorthCarolina.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st35": {
        "id": 35,
        "name": "North Dakota",
        "shortname": "ND",
        "link": "Images/ResourceImages/SummaryPublic/NDPublicSum.pdf",
        "comment": "North Dakota<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/NorthDakota.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st36": {
        "id": 36,
        "name": "Ohio",
        "shortname": "OH",
        "link": "Images/ResourceImages/SummaryPublic/OHPublicSum.pdf",
        "comment": "Ohio<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Ohio.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st37": {
        "id": 37,
        "name": "Oklahoma",
        "shortname": "OK",
        "link": "Images/ResourceImages/SummaryPublic/OKPublicSum.pdf",
        "comment": "Oklahoma<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Oklahoma.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st38": {
        "id": 38,
        "name": "Oregon",
        "shortname": "OR",
        "link": "Images/ResourceImages/SummaryPublic/ORPublicSum.pdf",
        "comment": "Oregon<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Oregon.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st39": {
        "id": 39,
        "name": "Pennsylvania",
        "shortname": "PA",
        "link": "Images/ResourceImages/SummaryPublic/PAPublicSum.pdf",
        "comment": "Pennsylvania<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Pennsylvania.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st40": {
        "id": 40,
        "name": "Rhode Island",
        "shortname": "RI",
        "link": "Images/ResourceImages/SummaryPublic/RIPublicSum.pdf",
        "comment": "Rhode Island<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/RhodeIsland.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st41": {
        "id": 41,
        "name": "South Carolina",
        "shortname": "SC",
        "link": "Images/ResourceImages/SummaryPublic/SCPublicSum.pdf",
        "comment": "South Carolina<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/SouthCarolina.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st42": {
        "id": 42,
        "name": "South Dakota",
        "shortname": "SD",
        "link": "Images/ResourceImages/SummaryPublic/SDPublicSum.pdf",
        "comment": "South Dakota<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/SouthDakota.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st43": {
        "id": 43,
        "name": "Tennessee",
        "shortname": "TN",
        "link": "Images/ResourceImages/SummaryPublic/TNPublicSum.pdf",
        "comment": "Tennessee<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Tennessee.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st44": {
        "id": 44,
        "name": "Texas",
        "shortname": "TX",
        "link": "Images/ResourceImages/SummaryPublic/TXPublicSum.pdf",
        "comment": "Texas<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Texas.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st45": {
        "id": 45,
        "name": "Utah",
        "shortname": "UT",
        "link": "Images/ResourceImages/SummaryPublic/UTPublicSum.pdf",
        "comment": "Utah<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Utah.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st46": {
        "id": 46,
        "name": "Vermont",
        "shortname": "VT",
        "link": "Images/ResourceImages/SummaryPublic/VTPublicSum.pdf",
        "comment": "Vermont<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Vermont.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st47": {
        "id": 47,
        "name": "Virginia",
        "shortname": "VA",
        "link": "Images/ResourceImages/SummaryPublic/VAPublicSum.pdf",
        "comment": "Virginia<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Virginia.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st48": {
        "id": 48,
        "name": "Washington",
        "shortname": "WA",
        "link": "Images/ResourceImages/SummaryPublic/WAPublicSum.pdf",
        "comment": "Washington<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Washington.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st49": {
        "id": 49,
        "name": "West Virginia",
        "shortname": "WV",
        "link": "Images/ResourceImages/SummaryPublic/WVPublicSum.pdf",
        "comment": "West Virginia<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/WestVirginia.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st50": {
        "id": 50,
        "name": "Wisconsin",
        "shortname": "WI",
        "link": "Images/ResourceImages/SummaryPublic/WIPublicSum.pdf",
        "comment": "Wisconsin<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Wisconsin.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st51": {
        "id": 51,
        "name": "Wyoming",
        "shortname": "WY",
        "link": "Images/ResourceImages/SummaryPublic/WYPublicSum.pdf",
        "comment": "Wyoming<br>Lien Summary<br>(Public Job)",
        "image": "Images/ResourceImages/Flags_TB/Wyoming.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    }
}
}