var map_cfg = {
    "mapWidth": "75%",
    "mapHeight": "75%",
    
    "shadowWidth": 2,
    "shadowOpacity": 0.2,
    "shadowColor": "black",
    "shadowX": 0,
    "shadowY": 0,
    
    "iPhoneLink": true,
    "isNewWindow": true,
    
    "zoomEnable": false,
    "zoomEnableControls": false,
    "zoomIgnoreMouseScroll": false,
    "zoomMax": 8,
    "zoomStep": 0.8,
    
    "borderColor": "#ffffff",
    "borderColorOver": "#ffffff",
    "borderOpacity": 0.5,
    
    "nameColor": "#ffffff",
    "nameColorOver": "#ffffff",
    "nameFontSize": "10px",
    "nameFontWeight": "bold",
    
    "nameStroke": false,
    "nameStrokeColor": "#000000",
    "nameStrokeColorOver": "#000000",
    "nameStrokeWidth": 1.5,
    "nameStrokeOpacity": 0.5,
    
    "overDelay": 300,
    "nameAutoSize": false,
    
    "tooltipOnHighlightIn": false,
    "freezeTooltipOnClick": false,
    
    "mapId": "vcg",
    
    "map_data": {
    "st1": {
        "id": 1,
        "name": "Alabama",
        "shortname": "AL",
        "link": "Images/ResourceImages/Statute/AL.pdf",
        "comment": " ALA. CODE <br>&sect;&sect; 35-11-1<br> et seq.",
        "image": "Images/ResourceImages/Seals_TB/AL_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st2": {
        "id": 2,
        "name": "Alaska",
        "shortname": "AK ",
        "link": "Images/ResourceImages/Statute/AK.pdf",
        "comment": " ALASKA STAT <br> &sect;&sect; 34.35.005 <br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/AK_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st3": {
        "id": 3,
        "name": "Arizona",
        "shortname": "AZ ",
        "link": "Images/ResourceImages/Statute/AZ.pdf",
        "comment": " ARIZ. REV. STAT<br>&sect;&sect; 33-981<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/AZ_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st4": {
        "id": 4,
        "name": "Arkansas",
        "shortname": "AR",
        "link": "Images/ResourceImages/Statute/AR.pdf",
        "comment": "ARK.CODE ANN<br>&sect;&sect; 18-44-101<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/AR_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st5": {
        "id": 5,
        "name": "California",
        "shortname": "CA",
        "link": "Images/ResourceImages/Statute/CA.pdf",
        "comment": " CAL. CIV. CODE<br>&sect;&sect; 3082<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/CA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st6": {
        "id": 6,
        "name": "Colorado",
        "shortname": "CO",
        "link": "Images/ResourceImages/Statute/CO.pdf",
        "comment": "COLO. REV. STAT<br>&sect;&sect; 38-22-101<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/CO_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st7": {
        "id": 7,
        "name": "Connecticut",
        "shortname": "CT",
        "link": "Images/ResourceImages/Statute/CT.pdf",
        "comment": " CONN. GEN. STAT<br>&sect;&sect; 49-33<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/CT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st8": {
        "id": 8,
        "name": "Delaware",
        "shortname": "DE",
        "link": "Images/ResourceImages/Statute/DE.pdf",
        "comment": " DEL. CODE ANN<br>tit. 25,<br>&sect;&sect; 2701<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/DE_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st9": {
        "id": 9,
        "name": "District of Columbia",
        "shortname": "DC",
        "link": "Images/ResourceImages/Statute/DC.pdf",
        "comment": "  D.C. CODE ANN.<br>&sect;&sect; 40-301.01<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/DC_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st10": {
        "id": 10,
        "name": "Florida",
        "shortname": "FL",
        "link": "Images/ResourceImages/Statute/FL.pdf",
        "comment": "FLA. STAT.<br>&sect;&sect; 713.001<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/FL_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st11": {
        "id": 11,
        "name": "Georgia",
        "shortname": "GA",
        "link": "Images/ResourceImages/Statute/GA.pdf",
        "comment": " GA. CODE ANN.<br>&sect;&sect; 44-14-360<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/GA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st12": {
        "id": 12,
        "name": "Hawaii",
        "shortname": "HI",
        "link": "Images/ResourceImages/Statute/HI.pdf",
        "comment": " HAW. REV. STAT.<br>&sect;&sect; 507-42<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/HI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st13": {
        "id": 13,
        "name": "Idaho",
        "shortname": "ID",
        "link": "Images/ResourceImages/Statute/ID.pdf",
        "comment": " IDAHO CODE<br>&sect;&sect; 45-501<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/ID_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st14": {
        "id": 14,
        "name": "Illinois",
        "shortname": "IL",
        "link": "Images/ResourceImages/Statute/IL.pdf",
        "comment": " 770 ILCS 60/0.01<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/IL_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st15": {
        "id": 15,
        "name": "Indiana",
        "shortname": "IN",
        "link": "Images/ResourceImages/Statute/IN.pdf",
        "comment": "IND.CODE<br>&sect;&sect;32-28-3-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/IN_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st16": {
        "id": 16,
        "name": "Iowa",
        "shortname": "IA",
        "link": "Images/ResourceImages/Statute/IA.pdf",
        "comment": "IOWA CODE<br>&sect;&sect; 572.1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/IA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st17": {
        "id": 17,
        "name": "Kansas",
        "shortname": "KS",
        "link": "Images/ResourceImages/Statute/KS.pdf",
        "comment": "KAN. STAT. ANN.<br>&sect;&sect; 60-1101<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/KS_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st18": {
        "id": 18,
        "name": "Kentucky",
        "shortname": "KY",
        "link": "Images/ResourceImages/Statute/KY.pdf",
        "comment": " KY. REV. STAT. ANN.<br>&sect;&sect; 376.010<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/KY_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st19": {
        "id": 19,
        "name": "Louisiana",
        "shortname": "LA",
        "link": "Images/ResourceImages/Statute/LA.pdf",
        "comment": "LA. REV. STAT. ANN.<br>&sect;&sect; 9:4801<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/LA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st20": {
        "id": 20,
        "name": "Maine",
        "shortname": "ME",
        "link": "Images/ResourceImages/Statute/ME.pdf",
        "comment": " ME. REV. STAT. ANN.<br>tit. 10,<br>&sect;&sect; 3251<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/ME_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st21": {
        "id": 21,
        "name": "Maryland",
        "shortname": "MD",
        "link": "Images/ResourceImages/Statute/MD.pdf",
        "comment": "  MD. CODE ANN.,<br>REAL PROP.<br>&sect;&sect; 9-10<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/MD_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st22": {
        "id": 22,
        "name": "Massachusetts",
        "shortname": "MA",
        "link": "Images/ResourceImages/Statute/MA.pdf",
        "comment": "MASS. GEN. LAWS<br>ch. 254,<br>&sect;&sect; 1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/MA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st23": {
        "id": 23,
        "name": "Michigan",
        "shortname": "MI",
        "link": "Images/ResourceImages/Statute/MI.pdf",
        "comment": " M.C.L.A.<br>&sect;&sect; 570.1101<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/MI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st24": {
        "id": 24,
        "name": "Minnesota",
        "shortname": "MN",
        "link": "Images/ResourceImages/Statute/MN.pdf",
        "comment": "  MINN. STAT.<br>&sect;&sect; 514.01",
        "image": "Images/ResourceImages/Seals_TB/MN_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st25": {
        "id": 25,
        "name": "Mississippi",
        "shortname": "MS",
        "link": "Images/ResourceImages/Statute/MS.pdf",
        "comment": " MISS. CODE ANN.<br>&sect;&sect; 85-7-131<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/MS_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st26": {
        "id": 26,
        "name": "Missouri",
        "shortname": "MO",
        "link": "Images/ResourceImages/Statute/MO.pdf",
        "comment": " MO. REV. STAT.<br>&sect;&sect; 429.010<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/MO_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st27": {
        "id": 27,
        "name": "Montana",
        "shortname": "MT",
        "link": "Images/ResourceImages/Statute/MT.pdf",
        "comment": "MONT. CODE<br>ANN. &sect;&sect; 71-3-521<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/MT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st28": {
        "id": 28,
        "name": "Nebraska",
        "shortname": "NE",
        "link": " Images/ResourceImages/Statute/NE.pdf",
        "comment": " NEB. REV. STAT.<br>&sect;&sect; 52-125<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/NE_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st29": {
        "id": 29,
        "name": "Nevada",
        "shortname": "NV",
        "link": "Images/ResourceImages/Statute/NV.pdf",
        "comment": "NEV. REV. STAT.<br>&sect;&sect; 108.221<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/NV_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st30": {
        "id": 30,
        "name": "New Hampshire",
        "shortname": "NH",
        "link": "Images/ResourceImages/Statute/NH.pdf",
        "comment": "N.H. REV. STAT. ANN.<br>&sect;&sect; 447:1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/NH_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st31": {
        "id": 31,
        "name": "New Jersey",
        "shortname": "NJ",
        "link": "Images/ResourceImages/Statute/NJ.pdf",
        "comment": "N.J. STAT. ANN.<br>&sect;&sect; 2A:44A-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/NJ_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st32": {
        "id": 32,
        "name": "New Mexico",
        "shortname": "NM",
        "link": "Images/ResourceImages/Statute/NM.pdf",
        "comment": "N.M.STAT. ANN.<br>&sect;&sect; 48-2-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/NM_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st33": {
        "id": 33,
        "name": "New York",
        "shortname": "NY",
        "link": "Images/ResourceImages/Statute/NY.pdf",
        "comment": "N.Y. LIEN LAW<br>&sect;&sect; 1 et seq.",
        "image": "Images/ResourceImages/Seals_TB/NY_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st34": {
        "id": 34,
        "name": "North Carolina",
        "shortname": "NC",
        "link": "Images/ResourceImages/Statute/NC.pdf",
        "comment": "N.C. GEN. STAT.<br>&sect;&sect; 44A-7<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/NC_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st35": {
        "id": 35,
        "name": "North Dakota",
        "shortname": "ND",
        "link": "Images/ResourceImages/Statute/ND.pdf",
        "comment": "N.D. CENT. CODE<br>&sect;&sect; 35-27-01<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/ND_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st36": {
        "id": 36,
        "name": "Ohio",
        "shortname": "OH",
        "link": "Images/ResourceImages/Statute/OH.pdf",
        "comment": " OHIO REV. CODE<br>ANN. &sect;&sect; 1311.01<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/OH_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st37": {
        "id": 37,
        "name": "Oklahoma",
        "shortname": "OK",
        "link": "Images/ResourceImages/Statute/OK.pdf",
        "comment": "OKLA. STAT.<br>tit. 42, &sect;&sect; 141<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/OK_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st38": {
        "id": 38,
        "name": "Oregon",
        "shortname": "OR",
        "link": "Images/ResourceImages/Statute/OR.pdf",
        "comment": "OR. REV. STAT.<br>&sect;&sect; 87.001<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/OR_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st39": {
        "id": 39,
        "name": "Pennsylvania",
        "shortname": "PA",
        "link": "Images/ResourceImages/Statute/PA.pdf",
        "comment": "49 PA. CONS. STAT.<br>&sect;&sect; 1101<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/PA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st40": {
        "id": 40,
        "name": "Rhode Island",
        "shortname": "RI",
        "link": "Images/ResourceImages/Statute/RI.pdf",
        "comment": "R.I. GEN. LAWS<br>&sect;&sect; 34-28-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/RI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st41": {
        "id": 41,
        "name": "South Carolina",
        "shortname": "SC",
        "link": "Images/ResourceImages/Statute/SC.pdf",
        "comment": "S.C. CODE ANN.<br>&sect;&sect; 29-5-10<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/SC_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st42": {
        "id": 42,
        "name": "South Dakota",
        "shortname": "SD",
        "link": "Images/ResourceImages/Statute/SD.pdf",
        "comment": "S.D. CODIFIED LAWS<br>&sect;&sect; 44-1-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/SD_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st43": {
        "id": 43,
        "name": "Tennessee",
        "shortname": "TN",
        "link": "Images/ResourceImages/Statute/TN.pdf",
        "comment": "TENN. CODE<br>ANN. 66-11-101<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/TN_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st44": {
        "id": 44,
        "name": "Texas",
        "shortname": "TX",
        "link": "Images/ResourceImages/Statute/TX.pdf",
        "comment": "TEX. PROP. CODE<br>&sect;&sect; 53.001<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/TX_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st45": {
        "id": 45,
        "name": "Utah",
        "shortname": "UT",
        "link": "Images/ResourceImages/Statute/UT.pdf",
        "comment": "UTAH CODE ANN.<br>&sect;&sect; 38-1-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/UT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st46": {
        "id": 46,
        "name": "Vermont",
        "shortname": "VT",
        "link": "Images/ResourceImages/Statute/VT.pdf",
        "comment": "VT. STAT. ANN.<br>tit. 9, <br>&sect;&sect; 1921<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/VT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st47": {
        "id": 47,
        "name": "Virginia",
        "shortname": "VA",
        "link": "Images/ResourceImages/Statute/VA.pdf",
        "comment": "VA.CODE ANN.<br>&sect;&sect; 43-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/VA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st48": {
        "id": 48,
        "name": "Washington",
        "shortname": "WA",
        "link": "Images/ResourceImages/Statute/WA.pdf",
        "comment": " WASH. REV. CODE<br>&sect;&sect; 60.04.011<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/WA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st49": {
        "id": 49,
        "name": "West Virginia",
        "shortname": "WV",
        "link": "Images/ResourceImages/Statute/WV.pdf",
        "comment": "W.VA. CODE<br>&sect;&sect; 38-2-1<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/WV_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st50": {
        "id": 50,
        "name": "Wisconsin",
        "shortname": "WI",
        "link": "Images/ResourceImages/Statute/WI.pdf",
        "comment": "WIS. STAT.<br>&sect;&sect; 779.01<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/WI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st51": {
        "id": 51,
        "name": "Wyoming",
        "shortname": "WY",
        "link": "Images/ResourceImages/Statute/WY.pdf",
        "comment": "WYO. STAT. ANN.<br>&sect;&sect; 29-1-201<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/WY_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    }
}
}