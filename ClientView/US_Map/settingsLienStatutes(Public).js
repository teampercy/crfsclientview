var map_cfg = {
    "mapWidth": "75%",
    "mapHeight": "75%",
    
    "shadowWidth": 2,
    "shadowOpacity": 0.2,
    "shadowColor": "black",
    "shadowX": 0,
    "shadowY": 0,
    
    "iPhoneLink": true,
    "isNewWindow": true,
    
    "zoomEnable": false,
    "zoomEnableControls": false,
    "zoomIgnoreMouseScroll": false,
    "zoomMax": 8,
    "zoomStep": 0.8,
    
    "borderColor": "#ffffff",
    "borderColorOver": "#ffffff",
    "borderOpacity": 0.5,
    
    "nameColor": "#ffffff",
    "nameColorOver": "#ffffff",
    "nameFontSize": "10px",
    "nameFontWeight": "bold",
    
    "nameStroke": false,
    "nameStrokeColor": "#000000",
    "nameStrokeColorOver": "#000000",
    "nameStrokeWidth": 1.5,
    "nameStrokeOpacity": 0.5,
    
    "overDelay": 300,
    "nameAutoSize": false,
    
    "tooltipOnHighlightIn": false,
    "freezeTooltipOnClick": false,
    
    "mapId": "vcg",
    
    "map_data": {
    "st1": {
        "id": 1,
        "name": "Alabama",
        "shortname": "AL",
        "link": "Images/ResourceImages/Statute/AL1.pdf",
        "comment": "Title 39<br>Public Works<br>Section 39-1-1",
        "image": "Images/ResourceImages/Seals_TB/AL_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st2": {
        "id": 2,
        "name": "Alaska",
        "shortname": "AK ",
        "link": "Images/ResourceImages/Statute/AK1.pdf",
        "comment": "Alaska Statutes, Title 36<br>Public Contracts, Chapter 36.25<br>Contractors' Bonds<br>Sections 36.25.010 through 36.25.025",
        "image": "Images/ResourceImages/Seals_TB/AK_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st3": {
        "id": 3,
        "name": "Arizona",
        "shortname": "AZ ",
        "link": "Images/ResourceImages/Statute/AZ1.pdf",
        "comment": "Title 34,<br>Public Buildings and Improvements<br>Article 2, Contracts<br>Sections 34�222 through 34-224",
        "image": "Images/ResourceImages/Seals_TB/AZ_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st4": {
        "id": 4,
        "name": "Arkansas",
        "shortname": "AR",
        "link": "Images/ResourceImages/Statute/AR1.pdf",
        "comment": " Title 22, Public Property<br>Chapter 9, Public Works<br>Subchapter 4, Contractors' Bonds<br>Sections 22-9-401 through 22-9-405",
        "image": "Images/ResourceImages/Seals_TB/AR_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st5": {
        "id": 5,
        "name": "California",
        "shortname": "CA",
        "link": "Images/ResourceImages/Statute/CA1.pdf",
        "comment": "CA. CIVIL CODE<br>&sect;&sect; 9000-9566<br>et seq.",
        "image": "Images/ResourceImages/Seals_TB/CA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st6": {
        "id": 6,
        "name": "Colorado",
        "shortname": "CO",
        "link": "Images/ResourceImages/Statute/CO1.pdf",
        "comment": " Title 24, Government, State, Article 105,<br>Colorado Procurement Code � Construction Contracts,<br>Sections 24-105-201 through 24-105-203<br><br><br>Title 38, Property � Real and Personal,<br>Article 26, Liens � Contractors' Bonds and Lien on Funds,<br>Sections 38-26-101 and 38-26-105 through 38-26-110",
        "image": "Images/ResourceImages/Seals_TB/CO_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st7": {
        "id": 7,
        "name": "Connecticut",
        "shortname": "CT",
        "link": "Images/ResourceImages/Statute/CT1.pdf",
        "comment": "Title 49,<br>Mortgages and Liens, Chapter 847,<br>Liens, Sections 49-41 through 49-43",
        "image": "Images/ResourceImages/Seals_TB/CT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st8": {
        "id": 8,
        "name": "Delaware",
        "shortname": "DE",
        "link": "Images/ResourceImages/Statute/DE1.pdf",
        "comment": "Title 29,<br>State Government, Budget, Fiscal,<br>Procurement and Contracting Regulations,<br>Chapter 69, State Procurement, Subchapter IV,<br>Public Works Contracting, Section 6962",
        "image": "Images/ResourceImages/Seals_TB/DE_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st9": {
        "id": 9,
        "name": "District of Columbia",
        "shortname": "DC",
        "link": "Images/ResourceImages/Statute/DC1.pdf",
        "comment": " Title 2, Government Administration,<br>Chapter 2, Contracts, Subchapter 1, Bonding Requirement,<br>Sections 2-201.01 through 2-201.03, and 2-201.11",
        "image": "Images/ResourceImages/Seals_TB/DC_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st10": {
        "id": 10,
        "name": "Florida",
        "shortname": "FL",
        "link": "Images/ResourceImages/Statute/FL1.pdf",
        "comment": "Title XVIII,<br>Public Lands and Property, Chapter 255,<br>Public Property and Publicly Owned Buildings,<br>Section 255.05",
        "image": "Images/ResourceImages/Seals_TB/FL_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st11": {
        "id": 11,
        "name": "Georgia",
        "shortname": "GA",
        "link": "Images/ResourceImages/Statute/GA1.pdf",
        "comment": "  Title 13,Contracts,<br>Chapter 10, Contracts for Public Works,<br>Sections 13-10-1 � 13-10-2 and 13-10-40 <br>through 13-10-65<br><br>Title 36,<br>Local Government Provisions Appliable to Counties,<br>Municipal Corporations, and Other Governmental Entities,<br>Chapter 91, Public Works Bidding<br>Sections 36-91-1 � 36-91-2, 36-91-40 <br>and 36-91-70 through 36-91-95",
        "image": "Images/ResourceImages/Seals_TB/GA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st12": {
        "id": 12,
        "name": "Hawaii",
        "shortname": "HI",
        "link": "Images/ResourceImages/Statute/HI1.pdf",
        "comment": "Chapter 103D,<br>Hawaii Public Procurement Code, Part III,<br>Source Selection and Contract Formation,<br>Sections 103D-323 through 103D-325",
        "image": "Images/ResourceImages/Seals_TB/HI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st13": {
        "id": 13,
        "name": "Idaho",
        "shortname": "ID",
        "link": "Images/ResourceImages/Statute/ID1.pdf",
        "comment": "Title 54,<br>Professions, Vocations and Businesses,<br>Chapter 19, Public Works Contractors,<br>Sections 54-1925 through 54-1930",
        "image": "Images/ResourceImages/Seals_TB/ID_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st14": {
        "id": 14,
        "name": "Illinois",
        "shortname": "IL",
        "link": "Images/ResourceImages/Statute/IL1.pdf",
        "comment": "  Illinois Compiled Statutes<br>Government, Chapter 30<br>Finance � Purchases and Contracts<br>Sections 550/0.01 through 550/3<br>Public Construction Bond Act",
        "image": "Images/ResourceImages/Seals_TB/IL_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st15": {
        "id": 15,
        "name": "Indiana",
        "shortname": "IN",
        "link": "Images/ResourceImages/Statute/IN1.pdf",
        "comment": " Title 4,<br>State Offices and Administration, Article 13.6,<br>State Public Works, Chapter 7, Bonding, Escrow and Retainages,<br>Sections 4-13.6-7-5 through 4-13.6-7-12<br>Title 5,<br>State and Local Administration, Article 16,<br>Public Works, Chapter 5,<br>Withholding and Bond to Secure Payment of Subcontractors,<br>Labor and Materialmen; Chapter 5.5, Retainage, Bonds,<br>and Payment of Contractors and Subcontractors,<br>Sections 5-16-5.5-1 through 5-16-5.5-8",
        "image": "Images/ResourceImages/Seals_TB/IN_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st16": {
        "id": 16,
        "name": "Iowa",
        "shortname": "IA",
        "link": "Images/ResourceImages/Statute/IA1.pdf",
        "comment": " Title XIV,<br>Property, Subtitle 3, Liens,<br>Chapter 573,<br>Labor and Material on Public Improvements,<br>sections 573.1 through 573.227<br>Title XV,<br>Judicial Branch and Judicial Procedures,<br>Subtitle 3,<br>Judicial Procedure, Chapter 616,<br>Place of Bringing Actions, section 616.15,<br>Surety Companies",
        "image": "Images/ResourceImages/Seals_TB/IA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st17": {
        "id": 17,
        "name": "Kansas",
        "shortname": "KS",
        "link": "Images/ResourceImages/Statute/KS1.pdf",
        "comment": "Chapter 16<br>Contracts and Promises, Article 19,<br>Kansas Fairness in Public Construction Contract Act<br>Sections 16-1901 through 16-1908<br><br>Chapter 60<br>Civil Procedure, Article 11, <br>Liens for Labor and Material<br>Sections 60-1110 & 60-1112<br><br>Chapter 68<br>Roads and Bridges, Part I, Roads, Article 5,<br>County and Township Roads<br><br>Sections 68-410, 68-521 and 68-527a",
        "image": "Images/ResourceImages/Seals_TB/KS_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st18": {
        "id": 18,
        "name": "Kentucky",
        "shortname": "KY",
        "link": "Images/ResourceImages/Statute/KY1.pdf",
        "comment": "  Kentucky Revised Statutes,<br>Chapter 45A,<br>Kentucky Model Procurement Code,<br>sections 45A.185, 45A.190, 45A.195, 45A.225<br>through 45A.265, and 45A.430 through 45A.440<br>See also:<br>Kentucky Revised Statutes,<br>Title XXVII, Chapter 341,<br>Unemployment Compensation,<br>section 341.317",
        "image": "Images/ResourceImages/Seals_TB/KY_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st19": {
        "id": 19,
        "name": "Louisiana",
        "shortname": "LA",
        "link": "Images/ResourceImages/Statute/LA1.pdf",
        "comment": "TITLE 38.<br>PUBLIC CONTRACTS, WORKS AND IMPROVEMENTS<br>CHAPTER 10. PUBLIC CONTRACTS<br>PART I. GENERAL PROVISIONS",
        "image": "Images/ResourceImages/Seals_TB/LA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st20": {
        "id": 20,
        "name": "Maine",
        "shortname": "ME",
        "link": "Images/ResourceImages/Statute/ME1.pdf",
        "comment": " Title 14, Court Procedure � Civil, Part 2,<br>Proceedings Before Trial, Chapter 205,<br>Limitation of Actions, Subchapter 3,<br>Miscellaneous Actions, Section 871",
        "image": "Images/ResourceImages/Seals_TB/ME_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st21": {
        "id": 21,
        "name": "Maryland",
        "shortname": "MD",
        "link": "Images/ResourceImages/Statute/MD1.pdf",
        "comment": " State Finance and Procurement Law, Division II,<br>General Procurement Law, Title 17,<br>Special Provisions � State and Local Subdivisions,<br>Subtitle 1, Security for Construction Contracts,<br>Sections 101 through 111",
        "image": "Images/ResourceImages/Seals_TB/MD_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st22": {
        "id": 22,
        "name": "Massachusetts",
        "shortname": "MA",
        "link": "Images/ResourceImages/Statute/MA1.pdf",
        "comment": " Massachusetts General Laws,<br>Part I, Administration of the Government,<br>Title XXI, Labor and Industries,<br>Chapter 149, Labor and Industries, Section 29",
        "image": "Images/ResourceImages/Seals_TB/MA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st23": {
        "id": 23,
        "name": "Michigan",
        "shortname": "MI",
        "link": "Images/ResourceImages/Statute/MI1.pdf",
        "comment": "MCLA&sect;&sect; 129.201�129.212 and<br>&sect;&sect; 570.101�570.101 (2008)<br>Chapter 129, Public Funds, <br>Act 213 of 1963 (as amended),<br>Contractor's Bond for Public <br>Buildings or Works; Chapter 570,<br>Liens, Act 187 of 1905 (as amended),<br>Public Buildings and Public Works; <br>Bond of Contractor",
        "image": "Images/ResourceImages/Seals_TB/MI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st24": {
        "id": 24,
        "name": "Minnesota",
        "shortname": "MN",
        "link": "Images/ResourceImages/Statute/MN1.pdf",
        "comment": "Chapter 574,<br>Bonds, Fines, Forfeitures,<br>Sections 26 through 32",
        "image": "Images/ResourceImages/Seals_TB/MN_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st25": {
        "id": 25,
        "name": "Mississippi",
        "shortname": "MS",
        "link": "Images/ResourceImages/Statute/MS1.pdf",
        "comment": " Title 31, Public Business, Bonds and Obligations,<br>Chapter 5, Public Works Contracts, <br>Sections 31-5-51 through 31-5-57,<br>Bonds Securing Public Works Contracts<br><br>See also, Miss. Code <br>&sect;&sect; 31-5-25 through 31-5-31<br>(prompt payment provisions)",
        "image": "Images/ResourceImages/Seals_TB/MS_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st26": {
        "id": 26,
        "name": "Missouri",
        "shortname": "MO",
        "link": "Images/ResourceImages/Statute/MO1.pdf",
        "comment": "Title VIII, Chapter 107, Section 107.170<br>Title XIV, Chapter 227,<br>Sections 227.100, 227.600 and 227.633;<br>Chapter 229, Sections 229.050, 229.060 and 229.070<br>Title XXXVI, Chapter 522, Section 522.300",
        "image": "Images/ResourceImages/Seals_TB/MO_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st27": {
        "id": 27,
        "name": "Montana",
        "shortname": "MT",
        "link": "Images/ResourceImages/Statute/MT1.pdf",
        "comment": "Title 18, Public Contracts,<br>Chapter 2, Construction Contracts,<br>Part 2, Performance, Labor and Materials Bond,<br>Sections 18-2-201through 18-2-208",
        "image": "Images/ResourceImages/Seals_TB/MT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st28": {
        "id": 28,
        "name": "Nebraska",
        "shortname": "NE",
        "link": "Images/ResourceImages/Statute/NE1.pdf",
        "comment": " Chapter 52, Liens,<br>Sections 52-118 through 52-118.02",
        "image": "Images/ResourceImages/Seals_TB/NE_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st29": {
        "id": 29,
        "name": "Nevada",
        "shortname": "NV",
        "link": "Images/ResourceImages/Statute/NV1.pdf",
        "comment": "Title 28, Public Works and Planning,<br>Chapter 339, Contractors' Bonds on Public Works,<br>Sections 339.015 through 339.065",
        "image": "Images/ResourceImages/Seals_TB/NV_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st30": {
        "id": 30,
        "name": "New Hampshire",
        "shortname": "NH",
        "link": "Images/ResourceImages/Statute/NH1.pdf",
        "comment": "Title XLI, Liens, Chapter 447,<br>Liens for Labor and Materials; Public Works,<br>Sections 447:15 through 447:18",
        "image": "Images/ResourceImages/Seals_TB/NH_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st31": {
        "id": 31,
        "name": "New Jersey",
        "shortname": "NJ",
        "link": "Images/ResourceImages/Statute/NJ1.pdf",
        "comment": "Title 2A,<br>Administration of Civil and Criminal Justice,<br>Chapter 44, Sections 2A:44-143 through 2A:44-148",
        "image": "Images/ResourceImages/Seals_TB/NJ_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st32": {
        "id": 32,
        "name": "New Mexico",
        "shortname": "NM",
        "link": "Images/ResourceImages/Statute/NM1.pdf",
        "comment": " Chapter 13, Public Purchases and Property,<br>Article 4, Public Works Contracts,<br>Sections 13-4-18 through 13-4-20",
        "image": "Images/ResourceImages/Seals_TB/NM_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st33": {
        "id": 33,
        "name": "New York",
        "shortname": "NY",
        "link": "Images/ResourceImages/Statute/NY1.pdf",
        "comment": " Article 9, Contracts, Section 137",
        "image": "Images/ResourceImages/Seals_TB/NY_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st34": {
        "id": 34,
        "name": "North Carolina",
        "shortname": "NC",
        "link": "Images/ResourceImages/Statute/NC1.pdf",
        "comment": " Chapter 44A, Statutory Liens and Charges,<br>Article 3, Model Payment and Performance Bond,<br>Sections 44A-25 through 44A-35",
        "image": "Images/ResourceImages/Seals_TB/NC_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st35": {
        "id": 35,
        "name": "North Dakota",
        "shortname": "ND",
        "link": "Images/ResourceImages/Statute/ND1.pdf",
        "comment": "Title 48, <br>Public Buildings,<br>Chapter 48-01.2,<br>Public Improvement Bids and Contract,<br>Sections 48-01.2-01, 48-01.2-09 <br>through 48-01.2-12 and 48-01.2-23",
        "image": "Images/ResourceImages/Seals_TB/ND_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st36": {
        "id": 36,
        "name": "Ohio",
        "shortname": "OH",
        "link": "Images/ResourceImages/Statute/OH1.pdf",
        "comment": " Title I, State Government,<br>Chapter 153, Public Improvements,<br>Sections 153.54 through 153.581",
        "image": "Images/ResourceImages/Seals_TB/OH_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st37": {
        "id": 37,
        "name": "Oklahoma",
        "shortname": "OK",
        "link": "Images/ResourceImages/Statute/OK1.pdf",
        "comment": "Title 61,<br>Public Buildings and Public Works,<br>Sections 61-1, 61-2, 61-13 and 61-15<br>Title 61,<br>Public Competitive Bidding Act of 1974 (as amended),<br>Section 61-112",
        "image": "Images/ResourceImages/Seals_TB/OK_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st38": {
        "id": 38,
        "name": "Oregon",
        "shortname": "OR",
        "link": "Images/ResourceImages/Statute/OR1.pdf",
        "comment": "Title 26, Public Facilities,<br>Contracting and Insurance,<br>Chapter 279C, Public Contracting &not;<br>Public Improvements and Related Contracts,<br>Sections 279C.380 through 279C.390, 279C.515,<br>and 279C.600 through 279C.625",
        "image": "Images/ResourceImages/Seals_TB/OR_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st39": {
        "id": 39,
        "name": "Pennsylvania",
        "shortname": "PA",
        "link": "Images/ResourceImages/Statute/PA1.pdf",
        "comment": " Title 8, Bonds and Recognizances,<br>Chapter 13, Public Works Contractors' Bonds,<br>Sections 191 through 202",
        "image": "Images/ResourceImages/Seals_TB/PA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st40": {
        "id": 40,
        "name": "Rhode Island",
        "shortname": "RI",
        "link": "Images/ResourceImages/Statute/RI1.pdf",
        "comment": "Title 37, Public Property and Works,<br>Chapter 37-12, Contractors' Bond,<br>Sections 37-12-1 through 37-12-11",
        "image": "Images/ResourceImages/Seals_TB/RI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st41": {
        "id": 41,
        "name": "South Carolina",
        "shortname": "SC",
        "link": "Images/ResourceImages/Statute/SC1.pdf",
        "comment": "Title 11, Public Finance,<br>Chapter 35, South Carolina Consolidated Procurement Code,<br>Article 9, Construction, Architect-Engineer, <br>Construction Management, and Land Surveying Services,<br>Subarticle 3, Construction Services, Section 11-35-3030",
        "image": "Images/ResourceImages/Seals_TB/SC_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st42": {
        "id": 42,
        "name": "South Dakota",
        "shortname": "SD",
        "link": "Images/ResourceImages/Statute/SD1.pdf",
        "comment": "Title 5, Public Property,<br>Purchases and Contracts,<br>Chapter 21, <br>Performance Bonds for Public Improvement Contracts,<br>Sections 5-21-1 through 5-21-8",
        "image": "Images/ResourceImages/Seals_TB/SD_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st43": {
        "id": 43,
        "name": "Tennessee",
        "shortname": "TN",
        "link": "Images/ResourceImages/Statute/TN1.pdf",
        "comment": "Title 12, Public Property, Printing and Contracts,<br>Chapter 4, Public Contracts, Part 2 � Surety Bonds,<br>Sections 12-4-201 through 12-4-206",
        "image": "Images/ResourceImages/Seals_TB/TN_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st44": {
        "id": 44,
        "name": "Texas",
        "shortname": "TX",
        "link": "Images/ResourceImages/Statute/TX1.pdf",
        "comment": " Title 10, General Government,<br>Subtitle F, State and Local Contracts and Fund Management,<br>Chapter 2253, Sections 2253.001 through 2253.076<br><br>Texas Property Code, Title 5, Exempt Property and Liens,<br>Subtitle B, Liens, Chapter 53,<br>Mechanic's, Contractor's or Materialman's Lien,<br>Subchapter J, Lien or Money Due Public Works Contractor,<br>Sections 53.231 through 53.237",
        "image": "Images/ResourceImages/Seals_TB/TX_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st45": {
        "id": 45,
        "name": "Utah",
        "shortname": "UT",
        "link": "Images/ResourceImages/Statute/UT1.pdf",
        "comment": "Title 14,<br>Contrators'Bonds, Chapter 1,Public Contracts,<br>Sections 14-1-18 through 14-1-20<br><br>Title 38,<br>Liens, Chapter 1, Mechanics' Liens,<br>Section 38-1-32<br><br>Title 63G,<br>General Government, Chapter 6, Utah Procurement Code,<br>Sections 63G-6-504 through 63G-6-507",
        "image": "Images/ResourceImages/Seals_TB/UT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st46": {
        "id": 46,
        "name": "Vermont",
        "shortname": "VT",
        "link": "Images/ResourceImages/Statute/VT1.pdf",
        "comment": "Title 19,<br>Highways, Chapter 1, State Highway Law,<br>Section 10, Duties<br><br>See also, Title 16,<br>Education, Chapter 123, <br>State Aid for Capital Construction Costs,<br>&sect; 3448",
        "image": "Images/ResourceImages/Seals_TB/VT_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st47": {
        "id": 47,
        "name": "Virginia",
        "shortname": "VA",
        "link": "Images/ResourceImages/Statute/VA1.pdf",
        "comment": "Title 2.2, Administration of Government,<br>Chapter 43, Virginia Public Procurement Act,<br>Sections 2.2-4336 through 2.2-4342",
        "image": "Images/ResourceImages/Seals_TB/VA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st48": {
        "id": 48,
        "name": "Washington",
        "shortname": "WA",
        "link": "Images/ResourceImages/Statute/WA1.pdf",
        "comment": " Title 39,<br>Public Contracts and Indebtedness,<br>Chapter 39.08, Contractor's Bond,<br>Sections 39.08.010 through 39.08.100<br>Title 60<br>Lien For Labor, Materials,Taxes On Public Works<br>Chapter 60.28 RCW",
        "image": "Images/ResourceImages/Seals_TB/WA_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st49": {
        "id": 49,
        "name": "West Virginia",
        "shortname": "WV",
        "link": "Images/ResourceImages/Statute/WV1.pdf",
        "comment": "Chapter 5,<br>General Powers and Authority of the Governor,<br>Secretary of State and Attorney General; Board of Public Works;<br>Miscellaneous Agencies, Commissions, Offices, Programs, Etc.,<br>Article 22, Government Construction Contracts,<br>Sections 5-22-1 and 5-22-2<br><br>Chapter 38, Liens,<br>Article 22, Mechanics' Liens,<br>Section 38-2-39",
        "image": "Images/ResourceImages/Seals_TB/WV_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st50": {
        "id": 50,
        "name": "Wisconsin",
        "shortname": "WI",
        "link": "Images/ResourceImages/Statute/WI1.pdf",
        "comment": "Title 16,<br>City, County, State and Local Powers,<br>Chapter 6, Public Property, Article 1, <br>Public Works and Contracts,<br>Sections 16-6-101 through 16-6-121",
        "image": "Images/ResourceImages/Seals_TB/WI_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    },
    "st51": {
        "id": 51,
        "name": "Wyoming",
        "shortname": "WY",
        "link": "Images/ResourceImages/Statute/WY1.pdf",
        "comment": "Title 16,<br>City, County, State and Local Powers,<br>Chapter 6, Public Property, <br>Article 1, Public Works and Contracts,<br>Sections 16-6-101 through 16-6-121",
        "image": "Images/ResourceImages/Seals_TB/WY_Seal.jpg",
        "color_map": "#7798BA",
        "color_map_over": "#366CA3"
    }
}
}