﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace ClientViewApi.Models
{
    public class JobDeadLineDates
    {
        #region Basic Functionality

        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public JobDeadLineDates()
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
        }

        public JobDeadLineDates(string JobId)
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
            this.JobId = JobId;
        }
        #endregion

        #region Properties
        public string JobId
        {
            get;
            set;
        }
        public string StartDate
        {
            get;
            set;
        }
        public string EndDate
        {
            get;
            set;
        }
        public string AssignedDate
        {
            get;
            set;
        }
        public string NoticeSent
        {
            get;
            set;
        }
        public string NoticeDeadline
        {
            get;
            set;
        }
        public string LienDeadline
        {
            get;
            set;
        }
        public string BondDeadline
        {
            get;
            set;
        }
        public string LienDate
        {
            get;
            set;
        }
        public string BondDate
        {
            get;
            set;
        }
        public string ForecloseDeadlineDate
        {
            get;
            set;
        }
        public string BondSuitDeadlineDate
        {
            get;
            set;
        }
        public string ForecloseDate
        {
            get;
            set;
        }
        public string BondSuitDate
        {
            get;
            set;
        }
        public string SNDeadlineDate
        {
            get;
            set;
        }
        public string NOIDeadlineDate
        {
            get;
            set;
        }
       
        #endregion

        #region Functions
        // GET api/<controller>/<action>/5
        public bool GetDeadLienDatesByJobId()
        {
            try
            {
                if (this.JobId != "")
                {
                    DbCommand com = this.db.GetStoredProcCommand("uspbo_ClientView_GetJobInfoJV");
                    this.db.AddInParameter(com, "JobId", DbType.String, this.JobId);
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.StartDate = dt.Rows[0]["StartDate"]!=DBNull.Value? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["StartDate"])):"";
                        this.EndDate = dt.Rows[0]["EndDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["EndDate"])) : "";
                        this.AssignedDate =dt.Rows[0]["DateAssigned"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["DateAssigned"])) : ""; 
                        this.NoticeSent =  dt.Rows[0]["NoticeSent"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["NoticeSent"])) : ""; 
                        this.NoticeDeadline = dt.Rows[0]["NoticeDeadlineDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["NoticeDeadlineDate"])) : "";
                        this.LienDeadline = dt.Rows[0]["LienDeadlineDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["LienDeadlineDate"])) : ""; 
                        this.BondDeadline = dt.Rows[0]["BondDeadlineDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["BondDeadlineDate"])) : ""; 
                        this.LienDate =dt.Rows[0]["LienDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["LienDate"])) : "";
                        this.BondDate =dt.Rows[0]["BondDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["BondDate"])) : "";
                        this.ForecloseDeadlineDate =  dt.Rows[0]["ForeclosureDeadlineDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["ForeclosureDeadlineDate"])) : ""; 
                        this.BondSuitDeadlineDate = dt.Rows[0]["BondSuitDeadlineDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["BondSuitDeadlineDate"])) : ""; 
                        this.ForecloseDate = dt.Rows[0]["ForeclosureDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["ForeclosureDate"])) : ""; 
                        this.BondSuitDate =  dt.Rows[0]["BondSuitDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["BondSuitDate"])) : "";
                        this.SNDeadlineDate = dt.Rows[0]["SNDeadlineDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["SNDeadlineDate"])) : ""; 
                        this.NOIDeadlineDate = dt.Rows[0]["NOIDeadlineDate"] != DBNull.Value ? string.Format("{0:MM-dd-yyyy}", Convert.ToDateTime(dt.Rows[0]["NOIDeadlineDate"])) : ""; 
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
     

        #endregion
        #endregion
    }
}