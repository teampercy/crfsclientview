﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace ClientViewApi.Models
{
    public class LenderDetails
    {
        #region Basic Functionality

        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public LenderDetails()
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
        }

        public LenderDetails(string JobId)
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
            this.JobId = JobId;
        }
        #endregion

        #region Properties
        public string JobId
        {
            get;
            set;
        }
         
        public string LenderName
        {
            get;
            set;
        }
        public string LenderAdd1
        {
            get;
            set;
        }
        public string LenderAdd2
        {
            get;
            set;
        }
        public string LenderCity
        {
            get;
            set;
        }
        public string LenderState
        {
            get;
            set;
        }
        
        public string LenderZip
        {
            get;
            set;
        }

        #endregion

        #region Functions
       
        public bool GetLenderByJobId()
        {
            try
            {
                if (this.JobId != "")
                {
                    DbCommand com = this.db.GetStoredProcCommand("uspbo_ClientView_GetPrimaryJobLegalParties");
                    this.db.AddInParameter(com, "JobId", DbType.String, this.JobId);
                    this.db.AddInParameter(com, "TypeCode", DbType.String, "LE");
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.LenderName = Convert.ToString(dt.Rows[0]["AddressName"]);
                        this.LenderAdd1 = Convert.ToString(dt.Rows[0]["AddressLine1"]);
                        this.LenderAdd2 = Convert.ToString(dt.Rows[0]["AddressLine2"]);
                        this.LenderCity = Convert.ToString(dt.Rows[0]["City"]);
                        this.LenderState =  Convert.ToString(dt.Rows[0]["State"]);
                        this.LenderZip = Convert.ToString(dt.Rows[0]["PostalCode"]);
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                ErrorLog.SendErrorToText(ex);
                return false;
            }


            
        }


        #endregion
        #endregion
    }
}