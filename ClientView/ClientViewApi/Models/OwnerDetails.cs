﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace ClientViewApi.Models
{
    public class OwnerDetails
    {
        #region Basic Functionality

        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public OwnerDetails()
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
        }

        public OwnerDetails(string JobId)
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
            this.JobId = JobId;
        }
        #endregion

        #region Properties
        public string JobId
        {
            get;
            set;
        }
     
        public string OwnerName
        {
            get;
            set;
        }
        public string OwnerAdd1
        {
            get;
            set;
        }
        public string OwnerAdd2
        {
            get;
            set;
        }
        public string OwnerCity
        {
            get;
            set;
        }
        public string OwnerState
        {
            get;
            set;
        }
        public string OwnerZip
        {
            get;
            set;
        }


        #endregion

        #region Functions
  
        public bool GetOwnerByJobId()
        {
            try
            {
                if (this.JobId != "")
                {
                    DbCommand com = this.db.GetStoredProcCommand("uspbo_ClientView_GetPrimaryJobLegalParties");
                    this.db.AddInParameter(com, "JobId", DbType.String, this.JobId);
                    this.db.AddInParameter(com, "TypeCode", DbType.String, "OW");
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.OwnerName = Convert.ToString(dt.Rows[0]["AddressName"]);
                        this.OwnerAdd1 = Convert.ToString(dt.Rows[0]["AddressLine1"]);
                        this.OwnerAdd2 = Convert.ToString(dt.Rows[0]["AddressLine2"]);
                        this.OwnerCity = Convert.ToString(dt.Rows[0]["City"]);
                        this.OwnerState =  Convert.ToString(dt.Rows[0]["State"]);
                        this.OwnerZip = Convert.ToString(dt.Rows[0]["PostalCode"]);

                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                ErrorLog.SendErrorToText(ex);
                return false;
            }


            
        }


        #endregion
        #endregion
    }
}