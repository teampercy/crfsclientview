﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Configuration;

namespace ClientViewApi.Models
{
    public class UserSecurity
    {
        #region Basic Functionality

        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public UserSecurity()
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
        }
        #endregion

        #region Properties
        public string Username
        {
            get;
            set;
        }
        public string Password
        {
            get;
            set;
        }
        #endregion

        #region Functions


        #endregion
        #endregion
    }
}