﻿using System;
using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace ClientViewApi.Models
{
    public class GCDetails
    {
        #region Basic Functionality

        #region Variable Declaration

        private Database db;

        #endregion

        #region Constructors

        public GCDetails()
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
        }

        public GCDetails(string JobId)
        {
            this.db = DatabaseFactory.CreateDatabase("conStr");
            this.JobId = JobId;
        }
        #endregion

        #region Properties
        public string JobId
        {
            get;
            set;
        }
        public string GCName
        {
            get;
            set;
        }
        public string GCAdd1
        {
            get;
            set;
        }
        public string GCAdd2
        {
            get;
            set;
        }
        public string GCCity
        {
            get;
            set;
        }
        public string GCState
        {
            get;
            set;
        }
        public string GCZip
        {
            get;
            set;
        }
        public string GCPhone
        {
            get;
            set;
        }
      

        #endregion

        #region Functions
       
    
        public bool GetGCByJobId()
        {
            try
            {
                if (this.JobId != "")
                {
                    DbCommand com = this.db.GetStoredProcCommand("uspbo_ClientView_GetPrimaryJobLegalParties");
                    this.db.AddInParameter(com, "JobId", DbType.String, this.JobId);
                    this.db.AddInParameter(com, "TypeCode", DbType.String, "GC");
                    DataSet ds = this.db.ExecuteDataSet(com);
                    if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        DataTable dt = ds.Tables[0];
                        this.GCName = Convert.ToString(dt.Rows[0]["AddressName"]);
                        this.GCAdd1 = Convert.ToString(dt.Rows[0]["AddressLine1"]);
                        this.GCAdd2 = Convert.ToString(dt.Rows[0]["AddressLine2"]);
                        this.GCCity = Convert.ToString(dt.Rows[0]["City"]);
                        this.GCState =  Convert.ToString(dt.Rows[0]["State"]);
                        this.GCZip = Convert.ToString(dt.Rows[0]["PostalCode"]);
                        this.GCPhone =  Convert.ToString(dt.Rows[0]["Telephone1"]);
                     
                        return true;
                    }
                }

                return false;
            }
            catch (Exception ex)
            {
                // To Do: Handle Exception
                ErrorLog.SendErrorToText(ex);
                return false;
            }


            
        }


        #endregion
        #endregion
    }
}