﻿using Microsoft.Owin.Security.OAuth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ClientViewApi.OwinSecurity
{
    public class ClientViewAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated(); // 
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var identity = new ClaimsIdentity(context.Options.AuthenticationType);                                                                                                  
            string _Username = System.Configuration.ConfigurationManager.AppSettings["ApiUsername"];
            string _Password = System.Configuration.ConfigurationManager.AppSettings["ApiUserPassword"];

            if (context.UserName == _Username && context.Password == _Password)
            {
                identity.AddClaim(new Claim(ClaimTypes.Role, "admin"));
                identity.AddClaim(new Claim("username", _Username));
                identity.AddClaim(new Claim(ClaimTypes.Name, "ClientView"));
                context.Validated(identity);
            }
            else
            {
                context.SetError("invalid_grant", "Provided username and password is incorrect");
                return;
            }
        }
    }
}