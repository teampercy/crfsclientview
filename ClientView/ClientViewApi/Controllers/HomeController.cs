﻿using ClientViewApi.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace ClientViewApi.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Login(UserSecurity objUserSecurity)
        {
            string username = objUserSecurity.Username.Trim();
            string password = objUserSecurity.Password.Trim();
            string TokenURL = System.Configuration.ConfigurationManager.AppSettings["TokenURL"];
            string BaseUrl = System.Configuration.ConfigurationManager.AppSettings["BaseURL"];
            string UserDetails = "username=" + username + "&password=" + password + "&grant_type=password";
            string UserToken = "";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(BaseUrl);
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    string strbody = UserDetails;
                    StringContent stringContent = new StringContent(strbody, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage Res = client.PostAsync(TokenURL, stringContent).Result;
                     
                    //Check if sucesses
                    if (Res.IsSuccessStatusCode)
                    {
                        var tokenResponse = Res.Content.ReadAsStringAsync().Result;
                        UserToken = tokenResponse;
                    }
                    else
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
              
            }
            ViewBag.token = UserToken;
            return Json(UserToken,JsonRequestBehavior.AllowGet);
        }
    }
}
