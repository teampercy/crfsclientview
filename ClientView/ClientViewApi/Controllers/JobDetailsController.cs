﻿using System;
using System.Web.Http;
using ClientViewApi.Models;

namespace ClientViewApi.Controllers
{

    [Authorize]
    [RoutePrefix("api/JobDetails")]
    public class JobDetailsController : ApiController
    {
        //Get Deadline dates according to JobId
        public JobDeadLineDates GetDeadLienDates(string id)
        {
            JobDeadLineDates objJob = new JobDeadLineDates();
            try
            {
                objJob.JobId = id;
                objJob.GetDeadLienDatesByJobId();
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
            }
            return objJob;
        }

        //Get Primary Legal Party GC details according to JobId
        public GCDetails GetGCDetails(string id)
        {
            GCDetails objGCDetails= new GCDetails();
            try
            {
                objGCDetails.JobId = id;
                objGCDetails.GetGCByJobId();

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
            }
            return objGCDetails;
        }
        //Get Primary Legal Party OW details according to JobId
        public OwnerDetails GetOwnerDetails(string id)
        {
            OwnerDetails objOwnerDetails = new OwnerDetails();
            try
            {
                objOwnerDetails.JobId = id;
                objOwnerDetails.GetOwnerByJobId();

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
            }
            return objOwnerDetails;
        }
        //Get Primary Legal Party LE details according to JobId
        public LenderDetails GetLenderDetails(string id)
        {
            LenderDetails objLenderDetails = new LenderDetails();
            try
            {
                objLenderDetails.JobId = id;
                objLenderDetails.GetLenderByJobId();

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
            }     
            return objLenderDetails;
        }


    }
}