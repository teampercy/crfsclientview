﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ClientViewApi.Controllers
{
    [Authorize]
    [RoutePrefix("api/test")]
    public class TestController : ApiController
    {
        [HttpGet]
        [AllowAnonymous]
        [Route("Test")]
        public IHttpActionResult Test()
        {
            return Ok("This is unauthorized test message..!");
        }

        [HttpGet]
        [Route("Hello")]
        public IHttpActionResult Hello()
        {
            return Ok("This is authorized test message..!");
        }
    }
}
