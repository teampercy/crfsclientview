﻿<%@ Page Language="VB" ValidateRequest="False" Inherits=" HDS.WEBSITE.UI.DefaultPage" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">




<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <%--<meta http-equiv="X-UA-Compatible" content="IE=11; IE=10; IE=9; IE=8; IE=7; IE=EDGE" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=EDGE">--%>
    <meta name="generator" content="hds vbjuice" />
    <meta id="metaKeywords" name="Keywords" content="" />
    <meta id="metaDescr" name="Description" content="" />
    <meta id="metaCopyRight" name="copyright" content="" />
    <link id="stylesheet" rel="stylesheet" href="app_themes/VBJUICE/bluejuice.css" type="text/css" />
    <link rel="shortcut icon" href="images/favicon.ico" />
    <link rel="alternate" type="application/rss+xml" title="gohds.com RSS Feed Description"
        href="HTTP://www.gohds.net/feeds.axd?BLOG" />
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css" type="text/css"">
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css" type="text/css"">
    <link rel="stylesheet" href="assets/css/site.min.css" type="text/css"">
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css" type="text/css"">
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css" type="text/css"">
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css" type="text/css"">
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css" type="text/css"">
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css" type="text/css"">
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css" type="text/css"">
    <%-- <link rel="stylesheet" href="global/vendor/chartist-js/chartist.css">--%>
    <%--   <link rel="stylesheet" href="global/vendor/jvectormap/jquery-jvectormap.css">--%>
    <%--  <link rel="stylesheet" href="global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">--%>
    <link rel="stylesheet" href="assets/examples/css/dashboard/v1.css" type="text/css"">
    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/weather-icons/weather-icons.css" type="text/css"">
    <link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css" type="text/css"">
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css" type="text/css"">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="global/vendor/modernizr/modernizr.js" type="text/javascript"></script>
    <script src="global/vendor/breakpoints/breakpoints.js" type="text/javascript"></script>
    <script src="Content/assets/plugins/fullcalendar/jquery/jquery-1.9.1.min.js"></script>
    <script type="text/javascript">
        Breakpoints();
        $(window).load(function () {
            // alert($('#divSidemenubar').height());
           // alert('hi');
            //alert(screen.height);
            $("#divAnimation").css("min-height", (screen.height-250));

            // alert($('#divAnimation').height());
        });
  </script>
    <title>CRF Solutions - ClientView 2009</title>
    <style type="text/css">
        #myform {
            overflow: hidden;
            position: relative;
        }

        .modal, .modal-backdrop {
            position: absolute !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function GoTo(url) {
            window.open(url, "_blank", "fullscreen=no,scrollbars=auto,resizable=1,menubar=1,titlebar=1,toolbar=1 ,width=660,height=500,top=243,left=25");

        }
    </script>
</head>
<body class="dashboard" style="width: 100%;">
    <div id="wrap123">
        <form id="myform" runat="server" autocomplete="on" enctype="multipart/form-data">
            <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
            <%--<ajaxToolkit:ToolkitScriptManager  ID="ScriptManager1" runat="server" >
            </ajaxToolkit:ToolkitScriptManager>--%>
            <%--<script type="text/javascript" language="javascript">
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
                function EndRequestHandler(sender, args) {
                    if (args.get_error() != undefined) {
                        args.set_errorHandled(true);
                    }
                }
            </script>--%>

            <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega">
           <div id="header">
                <SiteControls:Header ID="Header1" runat="server" />
              
            </div>
                
        </nav>

            <div style="height: 40px; width: 100%; background-color: #3a6dae;">
            </div>

            <!--Content Wrap -->
            <div class="pagea animsitiona" id="divAnimation">
                <div class="page-content padding-30 container-fluid">

                    <%--    <div class="row" data-plugin="matchHeight" data-by-row="true">
                        <div class="col-xlg-12 col-md-12">--%>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <asp:PlaceHolder ID="ContentPlaceHolder" runat="server"></asp:PlaceHolder>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <%--   </div>
                    </div>--%>
                </div>
            </div>
         <%--   <footer class="site-footer">
           <SiteControls:Footer ID="Footer2" runat="server" />
        </footer>--%>
            <div id="footer" style="background:#3a6dae;height:43px;">
                <SiteControls:Footer ID="Footer1" runat="server" />
            </div>
        </form>
    </div>
    <!-- Core  -->
    <script src="global/vendor/jquery/jquery.js" type="text/javascript"></script>
    <script src="global/vendor/bootstrap/bootstrap.js" type="text/javascript"></script>
    <script src="global/vendor/animsition/animsition.js" type="text/javascript"></script>
    <script src="global/vendor/asscroll/jquery-asScroll.js" type="text/javascript"></script>
    <script src="global/vendor/mousewheel/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="global/vendor/asscrollable/jquery.asScrollable.all.js" type="text/javascript"></script>
    <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js" type="text/javascript"></script>
    <!-- Plugins -->
    <script src="global/vendor/switchery/switchery.min.js" type="text/javascript"></script>
    <script src="global/vendor/intro-js/intro.js" type="text/javascript"></script>
    <script src="global/vendor/screenfull/screenfull.js" type="text/javascript"></script>
    <script src="global/vendor/slidepanel/jquery-slidePanel.js" type="text/javascript"></script>
    <script src="global/vendor/skycons/skycons.js" type="text/javascript"></script>
    <%--  <script src="global/vendor/chartist-js/chartist.min.js"></script>
    <script src="global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js"></script>--%>
    <script src="global/vendor/aspieprogress/jquery-asPieProgress.min.js" type="text/javascript"></script>
    <%--  <script src="global/vendor/jvectormap/jquery.jvectormap.min.js"></script>
    <script src="global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js"></script>--%>
    <script src="global/vendor/matchheight/jquery.matchHeight-min.js" type="text/javascript"></script>
    <!-- Scripts -->
    <script src="global/js/core.js" type="text/javascript"></script>
    <script src="assets/js/site.js" type="text/javascript"></script>
    <script src="assets/js/sections/menu.js" type="text/javascript"></script>
    <script src="assets/js/sections/menubar.js" type="text/javascript"></script>
    <script src="assets/js/sections/gridmenu.js" type="text/javascript"></script>
    <script src="assets/js/sections/sidebar.js" type="text/javascript"></script>
    <script src="global/js/configs/config-colors.js" type="text/javascript"></script>
    <script src="assets/js/configs/config-tour.js" type="text/javascript"></script>
    <script src="global/js/components/asscrollable.js" type="text/javascript"></script>
    <script src="global/js/components/animsition.js" type="text/javascript"></script>
    <script src="global/js/components/slidepanel.js" type="text/javascript"></script>
    <script src="global/js/components/switchery.js" type="text/javascript"></script>
    <script src="global/js/components/matchheight.js" type="text/javascript"></script>
    <%-- <script src="global/js/components/jvectormap.js" type="text/javascript"></script>--%>
    <script src="assets/examples/js/dashboard/v1.js" type="text/javascript"></script>
</body>
</html>
