﻿<%@ Page Language="VB" Inherits="HDS.WEBSITE.UI.DefaultPage" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" EnableEventValidation="false" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">



<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <meta name="generator" content="hds vbjuice" />
    <meta id="metaKeywords" name="Keywords" content="" />
    <meta id="metaDescr" name="Description" content="" />
    <meta id="metaCopyRight" name="copyright" content="" />
    <link id="stylesheet" rel="stylesheet" href="app_themes/VBJUICE/bluejuice.css" type="text/css" />
    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <link rel="alternate" type="application/rss+xml" title="gohds.com RSS Feed Description"
        href="HTTP://www.gohds.net/feeds.axd?BLOG" />

    <title>CRF Solutions - ClientView 2009</title>


    <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->

    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png" />
    <link rel="shortcut icon" href="assets/images/favicon.ico" />
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css" type="text/css" />
    <link rel="stylesheet" href="assets/css/site.min.css" type="text/css" />
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css" type="text/css" />
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css" type="text/css" />
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css" type="text/css" />
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css" type="text/css" />
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css" type="text/css" />
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css" type="text/css" />


    <link href="assets/js/editableDDL/jquery-editable-select.min.css" rel="stylesheet" />
    <%--   <link rel="stylesheet" href="global/vendor/chartist-js/chartist.css" type="text/css" />--%>

    <%-- <link rel="stylesheet" href="global/vendor/jvectormap/jquery-jvectormap.css" type="text/css">--%>
    <%--  <link rel="stylesheet" href="global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css" type="text/css" />--%>
    <link rel="stylesheet" href="assets/examples/css/dashboard/v1.css" type="text/css" />
    <link rel="stylesheet" href="global/vendor/jquery-mmenu/jquery-mmenu.css" type="text/css">

    <link rel="stylesheet" href="global/vendor/bootstrap-sweetalert/sweet-alert.css">
    <link rel="stylesheet" href="global/vendor/toastr/toastr.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/weather-icons/weather-icons.css" type="text/css" />
    <link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css" type="text/css" />
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css" type="text/css" />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic' />
    <link rel="stylesheet" href="global/vendor/datatables-bootstrap/dataTables.bootstrap.css" type="text/css" />
    <link href="assets/examples/css/uikit/icon.min.css" rel="stylesheet" type="text/css" />
    <link href="global/fonts/themify/themify.min.css" rel="stylesheet" type="text/css" />

    <script src="jquery-1.9.1.js" type="text/javascript"></script>
    <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="global/vendor/modernizr/modernizr.js" type="text/javascript"></script>
    <script src="global/vendor/breakpoints/breakpoints.js" type="text/javascript"></script>
    <script src="Content/CRFSCommon.js" type="text/javascript"></script>
    <%--  <link href="assets/css/site.css" type="text/css" rel="stylesheet" />
  
    <script src="global/vendor/ace/worker-coffee.js" type="text/javascript"></script>--%>
    <style type="text/css">
        .site-menubar-unfold .site-menu-item:not(.active)>.site-menu-sub {
    display: none;
}
                .site-menubar-unfold .site-menu-item.hover>.site-menu-sub {
    display: block;
}
        .form-control {
            border: 1px solid darkgray !important;
            color: black !important;
        }

        input[type="radio" i], input[type="checkbox" i] {
            background-color: initial;
            margin: 3px 0.5ex;
            padding: initial;
            border: 1px solid darkgray !important;
        }

        .site-menu-item a {
            display: block;
            color: #fff;
        }

        .site-menu .site-menu-sub .site-menu-item.active > a {
            color: darkgray; /*rgba(163,175,183,.9);*/
        }

        .site-menu > .site-menu-item.active > a {
            color: darkgray;
            background: 0 0;
        }

        .site-menu > .site-menu-item.open > a {
            color: darkgray;
            background: 0 0;
        }

        .checkbox-custom input[type=checkbox] {
            /* z-index: 1; */
            width: 20px;
            height: 20px;
            opacity: 0.5;
            /*//  outline: 1px solid darkgray;*/
            /* border-radius: 12px; */
        }



        .radio-custom input[type=radio] {
            z-index: 1;
            width: 20px;
            height: 20px;
            opacity: 0.5 !important;
            /* border: 1px solid black !important; */
        }

        .dataTables_length {
            margin-top: 10px !important;
          
        }
         .dataTables_length select {
           font-weight:500;
        }

         #modcontainer{
             min-width:800px;
         }
            .align-lable{
            text-align:right;
        }
    </style>
    <script type="text/javascript">
        //function BindFileUoloadList() {
        //    alert('hi...MainDefa');

        //}

        //window.onload = function () {

        //    $('#divSideMenuHtml').find('a').each(function () {
        //        //SubMenuToggle($(this).parent().attr('id'));
        //        $(this).bind("click", function () {
        //          //  SetHeaderBreadCrumb('RENTALVIEW', 'Resources', 'Lien Resources');
        //         //   SetHeaderBreadCrumb('RENTALVIEW', '', '');
        //           // MainMenuToggle($(this).parents('.has-sub').attr('id'));
        //           //   SubMenuToggle($(this).parent().attr('id'));

        //              var menuObj = {
        //                  MainMenuTitle: $(this).parents('.site-menu-category').text()
        //                 , MainMenuName: $(this).parents('.has-sub').children('.site-menu-title').attr('title')
        //                 , MainSubMenuName: $(this).children('.site-menu-title').attr('title')
        //                 , MainMenuToggle: $(this).parents('.has-sub').attr('id')
        //                 ,SubMenuToggle:$(this).parent().attr('id')
        //              }
        //              sessionStorage.removeItem("menuObj");
        //              sessionStorage.setItem('menuObj', menuObj);
        //        });
        //    });

        //    var menuObj = sessionStorage.getItem('menuObj');
        //    if (typeof menuObj != undefined)
        //    {
        //        SetHeaderBreadCrumb(menuObj.MainMenuTitle, menuObj.MainMenuName, menuObj.MainSubMenuName);
        //        MainMenuToggle(menuObj.MainMenuToggle);
        //        SubMenuToggle(menuObj.SubMenuToggle);
        //    }

        //}
        function modalHideShow(id, state) {
            $("#" + id).modal(state);
        }
        function SetDatePicker() {
            $('.datepicker').datepicker({
                format: "mm/dd/yyyy",
                changeMonth: true,
                changeYear: true,
                autoclose: true,
                showButtonPanel: true,
            })
            .on('hide', function () {
                if (!this.firstHide) {
                    if (!$(this).is(":focus")) {
                        this.firstHide = true;
                        // this will inadvertently call show (we're trying to hide!)
                        this.focus();
                    }
                } else {
                    this.firstHide = false;
                }
            })
              .on('show', function () {
                  if (this.firstHide) {
                      // careful, we have an infinite loop!
                      $(this).datepicker('hide');
                  }
              })
        }
        Breakpoints();
        $(window).load(function () {

            // alert($('#divSidemenubar').height());

            $("#divAnimation").css("min-height", $('#divSidemenubar').height());

            // alert($('#divAnimation').height());
        });
        //function Alertpopup() {
        //    alert('hicall');
        //}
        function HideModalPopup() {
            //alert('HideModalPopup');
            $("body.modal-open").animate({ paddingRight: '0px' });
            $("body").removeClass("modal-open");
            $(".modal-backdrop").remove();

        }
        $(document).ready(function () {

            SetDatePicker();
            PageMethods.GetSidemenuHtml(LoadGetSidemenuHtml);

        });
        function LoadGetSidemenuHtml(result) {
            // alert('hi');
            // alert(result);
            $("#divSideMenuHtml").html(result);
            MaintainMenuOpen();
            // Menubar setup
            // =============
            if (typeof $.site.menu !== 'undefined') {
                $.site.menu.init();
            }
            //
            // alert('2nd call');

        }
        function CallSuccessFuncDelete() {
            swal({
                title: "Deleted!",
                text: "Your file has been deleted!",
                type: "success",
                showCancelButton: false
            });
        }
        function OpenPdfWindow(filename) {
            window.open(filename, '_blank');
            return false;
        }
        function OpenPdfWindowNew(filename) {
            filename = filename.replace("~", "'")
            window.open(filename, '_blank');
            return false;
        }
        function ReplaceSpecChar(Textdata) {
            //alert(Textdata);
            var result = Textdata;
            var ResultReplace = result.replace("'", "`");
            //alert("ResultReplace" + ResultReplace);
            return ResultReplace;
        }
    </script>
    <script runat="server">
        Public Function GetLoginOrLogoffLIteral() As String
            If IsAuthenticated = True Then
                Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(Me.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
                If Not myuser Is Nothing Then
                    'Me.litUserINfo.Text = myuser.UserClientName

                    Me.litUserINfo.Text = Me.CurrentUser.UserName
                    'Me.' "(" + Me.CurrentUser.UserName + ")" ' Me.UserInfo.UserClientName
                End If
               
                Return "Logoff"
           
            Else
                Response.Redirect("Login.aspx", False)
                Return "Login"
            End If
        End Function

        <System.Web.Services.WebMethod(EnableSession:=True)> _
        Public Shared Function FindMergeJob(CurrentJobId As String, MergeJobId As String) As String
            Try
                If CurrentJobId IsNot Nothing Then
                    Dim mymergejob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(MergeJobId, mymergejob)
                    If mymergejob.Id = 0 Then
                        Return "This job id does not exist in our system."
                    End If
                    If mymergejob.Id = CurrentJobId Then
                        Return "Same Current Job cannot be Merge."
                    End If
                    If mymergejob.ParentJobId > 0 Then
                        Return "This job is already Merged with other job.Fail to Merge"
                    Else
                        Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(CurrentJobId, myjob)
                        If myjob.CustId <> mymergejob.CustId Then
                            Return "Cust# is different for both Job. Fail to Merge."
                        Else
                            Dim mycustomer As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomer
                            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myjob.CustId, mycustomer)
                            Return mymergejob.JobName & "~" & mymergejob.JobAdd1 & "~" & mycustomer.RefNum & "~" & mycustomer.ClientCustomer & "~"
                        End If
                    End If
                End If
                Return ""
            Catch ex As Exception
                Return "Fail to Merge."
            End Try
            
        End Function
        <System.Web.Services.WebMethod(EnableSession:=True)> _
        Public Shared Function SaveMergeJob(CurrentJobId As String, MergeJobId As String) As String
            Try
                If CurrentJobId IsNot Nothing Then
                    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(CurrentJobId, myjob)
                    If myjob.ParentJobId = 0 Then
                        myjob.Id = CurrentJobId
                        myjob.ParentJobId = myjob.Id
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
                    End If
                    Dim mymergejob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(MergeJobId, mymergejob)
                    mymergejob.Id = MergeJobId
                    mymergejob.ParentJobId = myjob.ParentJobId
                    Dim vwJobStatus As HDS.DAL.COMMON.TableView
                    vwJobStatus = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVStatusList()
                    Dim strquery As String = ""
                    strquery = "JOBVIEWSTATUS = 'JBIM'"
                    vwJobStatus.RowFilter = strquery
                    If vwJobStatus.Count > 0 Then
                        mymergejob.JVStatusCodeId = vwJobStatus.RowItem("Id")
                        mymergejob.JVStatusCode = vwJobStatus.RowItem("JobviewStatus")
                    End If
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(mymergejob)
                    Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
                    Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(HttpContext.Current.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
                    With mynote
                        .JobId = MergeJobId
                        .DateCreated = Now()
                        .Note += "Job has been merged with " & CurrentJobId
                        .UserId = Strings.Left(myuser.UserInfo.UserName, 10)
                        .ClientView = True
                        .EnteredByUserId =myuser.UserInfo.ID
                        .ActionTypeId = 0
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
                    End With
                    Dim jvPostMergeBalances As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_JobView_PostMergedBalances
                    'jvPostMergeBalances.JobId = CurrentJobId
                    jvPostMergeBalances.JobId = MergeJobId
                    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecNonQuery(jvPostMergeBalances)
                    
                    Return "Success"
                End If
                Return "Fail to Merge Job."
            Catch ex As Exception
                Return "Fail to Merge Job."
            End Try
            
        End Function

        '=======================================================
        'Service provided by Telerik (www.telerik.com)
        'Conversion powered by NRefactory.
        'Twitter: @telerik
        'Facebook: facebook.com/telerik
        '=======================================================

        'Protected Sub btn1_Click(sender As Object, e As EventArgs)
        '    Dim s As String
        '    s = "Jaywanti"
        
        'End Sub
    </script>
    <style type="text/css">
        .table a {
            text-decoration: none;
            color: #76838f;
        }
    </style>
</head>

<body class="dashboard" style="width: 100%; min-width:1000px;">

    <form id="myform" runat="server" autocomplete="on" enctype="multipart/form-data">
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
        <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle hamburger hamburger-close navbar-toggle-left hided"
                    data-toggle="menubar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="hamburger-bar"></span>
                </button>
                <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-collapse"
                    data-toggle="collapse">
                    <i class="icon wb-more-horizontal" aria-hidden="true" style="color:#62a8ea;"></i>
                </button>
                <div class="navbar-brand navbar-brand-center site-gridmenu-toggle" data-toggle="gridmenu" style="padding-left:0px;padding-right:0px;padding-bottom:0px;padding-top:10px;">
                <%--    <img class="navbar-brand-logo" src="assets/images/logo-blue.png" title="CRFS">CRFMasterlogo--%>
                        <img class="navbar-brand-logo" src="images/header1.jpg" title="CRFS" width="100%" style="height:auto;max-height:61px;">
                   <%-- <span class="navbar-brand-text">CRFS</span>--%>
                </div>
                <button type="button" class="navbar-toggle collapsed" data-target="#site-navbar-search"
                    data-toggle="collapse">
                    <span class="sr-only">Toggle Search</span>
                    <i class="icon wb-search" aria-hidden="true"></i>
                </button>
            </div>
            <div class="navbar-container container-fluid">
                <!-- Navbar Collapse -->
                <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
                    <!-- Navbar Toolbar -->
                    <ul class="nav navbar-toolbar">
                    <%--    <li class="hidden-float" id="toggleMenubar" >
                            <a data-toggle="menubar" href="#" role="button">
                                <i class="icon hamburger hamburger-arrow-left">
                                    <span class="sr-only">Toggle menubar</span>
                                    <span class="hamburger-bar"></span>
                                </i>
                            </a>
                        </li>--%>
                        <li class="hidden-xs" id="toggleFullscreen">
                            <a class="icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                                <span class="sr-only">Toggle fullscreen</span>
                            </a>
                        </li>
                   
                    </ul>
                    <!-- End Navbar Toolbar -->
                    <!-- Navbar Toolbar Right -->
                    <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right">
                        <li role="presentation" style="color:#FFF;font-weight:bold;font-weight:500;">
                            <div style="float:right;flex-align:center;padding:20px 30px 30px 0px;">                            
                            <%--  <a href="<%= GetAbsoluteUrl("?login")%>"><span style="color:#FFF;"><%=Me.GetLoginorLogoffLiteral%></span></a>--%>
                                  <a href="Login.aspx"><span style="color:#FFF;"><%=Me.GetLoginorLogoffLiteral%></span></a>
                                
                                </div>
                           <div style="float:left;flex-align:center;padding:20px 30px 30px 0px;">                            
                             <asp:Literal ID="litUserINfo" runat="server" ></asp:Literal>   </div>
                           
                        </li>
                    </ul>
                </div>
                <!-- End Navbar Collapse -->
                <!-- Site Navbar Seach -->
              <%--  <div class="collapse navbar-search-overlap" id="site-navbar-search">
                    <form role="search">
                        <div class="form-group">
                            <div class="input-search">
                                <i class="input-search-icon wb-search" aria-hidden="true"></i>
                                <input type="text" class="form-control" name="site-search" placeholder="Search...">
                                <button type="button" class="input-search-close icon wb-close" data-target="#site-navbar-search"
                                    data-toggle="collapse" aria-label="Close">
                                </button>
                            </div>
                        </div>
                    </form>
                </div>--%>
                <!-- End Site Navbar Seach -->
            </div>
        </nav>
        <div class="site-menubar" id="divSidemenubar" style="background-color: #152e4f !important;">
            <div class="site-menubar-body">
                <div>
                    <div id="divSideMenuHtml">
                        <%--   <SiteControls:TopMenu ID="topmenu1" runat="server" />--%>
                    </div>
                </div>
            </div>
            <%--      <div class="site-menubar-footer">
                <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
                    data-original-title="Settings">
                    <span class="icon wb-settings" aria-hidden="true"></span>
                </a>
                <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
                    <span class="icon wb-eye-close" aria-hidden="true"></span>
                </a>
                <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
                    <span class="icon wb-power" aria-hidden="true"></span>
                </a>
            </div>--%>
        </div>
        <%-- <div class="site-gridmenu">
            <div>
                <div>
                    <ul>
                        <li>
                            <a href="apps/mailbox/mailbox.html">
                                <i class="icon wb-envelope"></i>
                                <span>Mailbox</span>
                            </a>
                        </li>
                        <li>
                            <a href="apps/calendar/calendar.html">
                                <i class="icon wb-calendar"></i>
                                <span>Calendar</span>
                            </a>
                        </li>
                        <li>
                            <a href="apps/contacts/contacts.html">
                                <i class="icon wb-user"></i>
                                <span>Contacts</span>
                            </a>
                        </li>
                        <li>
                            <a href="apps/media/overview.html">
                                <i class="icon wb-camera"></i>
                                <span>Media</span>
                            </a>
                        </li>
                        <li>
                            <a href="apps/documents/categories.html">
                                <i class="icon wb-order"></i>
                                <span>Documents</span>
                            </a>
                        </li>
                        <li>
                            <a href="apps/projects/projects.html">
                                <i class="icon wb-image"></i>
                                <span>Project</span>
                            </a>
                        </li>
                        <li>
                            <a href="apps/forum/forum.html">
                                <i class="icon wb-chat-group"></i>
                                <span>Forum</span>
                            </a>
                        </li>
                        <li>
                            <a href="index.html">
                                <i class="icon wb-dashboard"></i>
                                <span>Dashboard</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>--%>
        <!-- Page -->

        <div class="page animsition" id="divAnimation" >
            <div class="page-header" style="text-align: left;" id="divBreadcrumb">
                <ol class="breadcrumb" style="padding-top: 30px;" id="olBreadCrumb">
                    <li id="liMainMenuTitle"><a href="javascript:void(0)">LienView</a></li>
                    <li id="liMainMenu" class="active">Jobs</li>
                </ol>
                <h1 class="page-title" id="liMainSubMenuName">Place Jobs</h1>
            </div>
            <div class="page-content padding-30 container-fluid" style="margin-top: -58px;">
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-xlg-12 col-md-12">
                        <div class="widget widget-shadow widget-responsive" id="widgetLineareaColor">
                            <div class="widget-content">
                                <div class="padding-top-30 padding-30" style="height: calc(100% - 250px);overflow-x:hidden; padding-left: 5px !important; padding-right: 5px !important;">

                                    <div class="row">
                                        <div class="col-xlg-12 col-md-12">
                                            <div>
                                                <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="3600" EnablePageMethods="true"></asp:ScriptManager>
                                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                    <ContentTemplate>
                                                        <asp:PlaceHolder ID="ContentPlaceHolder" runat="server"></asp:PlaceHolder>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <%--   <div class="page-header" style="text-align:left;">
                <ol class="breadcrumb" style="padding-top:30px;">
                    <li ><a href="../index.html">Home</a></li>
                    <li class="active">Basic UI</li>
                </ol>
                <h1 class="page-title">Tabs &amp; Accordions</h1>             
            </div>
            <div class="page-content padding-30 container-fluid" style="margin-top:-58px;">
                <div class="row" data-plugin="matchHeight" data-by-row="true">
                    <div class="col-xlg-12 col-md-12">
                        <div class="widget widget-shadow widget-responsive" id="widgetLineareaColor">
                            <div class="widget-content">
                                <div class="padding-top-30 padding-30" style="height: calc(100% - 250px);">

                                    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="true"></asp:ScriptManager>
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>
                                            <asp:PlaceHolder ID="ContentPlaceHolder" runat="server"></asp:PlaceHolder>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>--%>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Page -->
        <!-- Footer -->
        <footer class="site-footer" style="margin-left: 0px; width: 100%;">
            <%--   <center>
<br /><span class="style1">Copyright &copy; 2005-2015 CRF Solutions | All Rights Reserved
    </span>
<br class="style2" />
</center>--%>

            <div class="site-footer-right" style="width: 100%;">
                Copyright &copy; 2005-2018 CRF Solutions | All Rights Reserved
            </div>
        </footer>
    </form>
    <%--  --%>
    <!-- Core  -->
    <script src="global/vendor/jquery/jquery.js" type="text/javascript"></script>
    <script src="global/vendor/bootstrap/bootstrap.js" type="text/javascript"></script>
    <script src="global/vendor/animsition/animsition.js" type="text/javascript"></script>
    <script src="global/vendor/asscroll/jquery-asScroll.js" type="text/javascript"></script>
    <script src="global/vendor/mousewheel/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="global/vendor/asscrollable/jquery.asScrollable.all.js" type="text/javascript"></script>
    <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js" type="text/javascript"></script>
    <!-- Plugins -->
    <script src="global/vendor/switchery/switchery.min.js" type="text/javascript"></script>
    <script src="global/vendor/intro-js/intro.js" type="text/javascript"></script>
    <script src="global/vendor/screenfull/screenfull.js" type="text/javascript"></script>
    <script src="global/vendor/slidepanel/jquery-slidePanel.js" type="text/javascript"></script>
    <script src="global/vendor/skycons/skycons.js" type="text/javascript"></script>
    <script src="global/vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="global/vendor/datatables-bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="global/vendor/asrange/jquery-asRange.min.js" type="text/javascript"></script>
    <script src="global/vendor/formatter-js/jquery.formatter.js" type="text/javascript"></script>


    <script type="text/javascript" src="assets/js/editableDDL/jquery-editable-select.min.js"></script>

    <%--  <script src="global/vendor/chartist-js/chartist.min.js"></script>
    <script src="global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js"></script>--%>
    <script src="global/vendor/aspieprogress/jquery-asPieProgress.min.js" type="text/javascript"></script>
    <%--  <script src="global/vendor/jvectormap/jquery.jvectormap.min.js"></script>
    <script src="global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js"></script>--%>
    <script src="global/vendor/matchheight/jquery.matchHeight-min.js" type="text/javascript"></script>
    <!-- Scripts -->
    <script src="global/js/core.js" type="text/javascript"></script>
    <%--<script src="assets/js/site.js" type="text/javascript"></script>--%>
    <script src="assets/js/site.min.js" type="text/javascript"></script>
    <script src="assets/js/sections/menu.js" type="text/javascript"></script>

    <script src="assets/js/sections/menubar.js" type="text/javascript"></script>
    <script src="assets/js/sections/gridmenu.js" type="text/javascript"></script>
    <script src="assets/js/sections/sidebar.js" type="text/javascript"></script>
    <script src="global/js/configs/config-colors.js" type="text/javascript"></script>
    <script src="assets/js/configs/config-tour.js" type="text/javascript"></script>
    <script src="global/js/components/asscrollable.js" type="text/javascript"></script>
    <script src="global/js/components/animsition.js" type="text/javascript"></script>
    <script src="global/js/components/slidepanel.js" type="text/javascript"></script>
    <script src="global/js/components/switchery.js" type="text/javascript"></script>
    <script src="global/js/components/matchheight.js" type="text/javascript"></script>
    <%--    <script src="global/js/components/jvectormap.js"></script>--%>
    <%--  <script src="assets/examples/js/dashboard/v1.js" type="text/javascript"></script>--%>
    <script src="global/js/components/formatter-js.js" type="text/javascript"></script>
    <script src="global/js/components/datatables.js" type="text/javascript"></script>
    <script src="global/js/plugins/responsive-tabs.js" type="text/javascript"></script>
    <script src="global/js/plugins/closeable-tabs.js" type="text/javascript"></script>
    <script src="global/js/components/tabs.js" type="text/javascript"></script>
    <script src="assets/examples/js/uikit/icon.js" type="text/javascript"></script>
    <script src="global/js/components/panel.js" type="text/javascript"></script>
    <script src="assets/examples/js/uikit/panel-actions.js" type="text/javascript"></script>
    <script src="global/vendor/bootstrap-sweetalert/sweet-alert.js"></script>
    <script src="global/vendor/toastr/toastr.js"></script>
    <script src="global/js/components/bootbox.js"></script>
    <script src="global/js/components/bootstrap-sweetalert.js"></script>
    <script src="global/js/components/toastr.js"></script>
    <script src="assets/examples/js/advanced/bootbox-sweetalert.js"></script>
    <script src="assets/examples/js/uikit/icon.min.js" type="text/javascript"></script>

    <link href="upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet" />
    <%--    <link href="upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" />--%>
    <script src="upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <%--    <script src="upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>--%>
</body>

</html>

