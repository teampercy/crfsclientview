<%@ Control Language="VB" ClassName="ReportsView" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<script runat="server">

    Protected WithEvents btnPrintItem As LinkButton
       
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim myuser As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users
        Me.HiddenField1.Value = Me.MyPage.CurrentUser.Id
        
        CRF.CLIENTVIEW.BLL.ProviderBase.DAL.Read(Me.MyPage.CurrentUser.Id, myuser)
        Me.HiddenField1.Value = myuser.ClientId
        Me.MultiView1.SetActiveView(Me.View1)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
         
    End Sub
    Protected Sub gvItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvItemList.DataBind()
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)
   
    End Sub
    Private Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrintItem = TryCast(e.Row.FindControl("btnPrintItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrintItem"), LinkButton).Click, AddressOf btnPrintItem_Click
        End If
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnPrintItem = TryCast(e.Row.FindControl("btnPrintItem"), LinkButton)
            btnPrintItem.Attributes("rowno") = dr("AttachmentId")

        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
        End If
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)        'If e.Row.RowType = DataControlRowType.Pager Then
        '    Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
        '   lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

        '  Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
        ' txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

        ' im ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
        ' lPageSize.SelectedValue = gridView.PageSize.ToString()
        ' End If'E
    End Sub
    Private Sub btnPrintItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintItem.Click
        btnPrintItem = TryCast(sender, LinkButton)
        HiddenField1.Value = btnPrintItem.Attributes("rowno")
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Report_HistoryAttachment
        CRF.CLIENTVIEW.BLL.ProviderBase.DAL.Read(Me.HiddenField1.Value, myitem)
        
        Me.btnDownLoad.Visible = False
        Me.plcReportViewer.Controls.Clear()
        
        Dim s As String = "~/UserData/output/" & myitem.url
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.image, Me.MapPath(s))
       
        If System.IO.File.Exists(Me.MapPath(s)) = True Then
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Add(Me.NotAvailable)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        
        
     
    End Sub
    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    
    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
      
    End Sub
</script>
<style type="text/css">
    #divBreadcrumb {
        padding-top: 0px;
    }

    .breadcrumb > li + li:before {
        padding: 0 5px;
        color: #62a8ea;
        content: none;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        //  $('#liJobs').addClass('site-menu-item has-sub active open');
        //  $('#lisubJobList').addClass('site-menu-item active');
        SetHeaderBreadCrumb('', 'My Account', 'Reports');
        //MainMenuToggle('liSubEmployeeMonth');
        SubMenuToggle('liSubReports');
        CallSuccessFunc();
        return false;

    }
    function CallSuccessFunc() {
       // alert('hi');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
             
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },
           
        };
        var options = $.extend(true, {}, defaults, $(this).data());
       // alert($('#' + '<%=gvItemList.ClientID%> tr').length);
        if ($('#' + '<%=gvItemList.ClientID%> tr').length > 1) {
            $('#' + '<%=gvItemList.ClientID%>').dataTable(options);

        }
       
    }
    
</script>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetReports"
    TypeName="Data.ReportsProvider">
    <SelectParameters>
        <asp:Parameter DefaultValue="5" Name="maxrows" Type="Int32" />
        <asp:ControlParameter ControlID="HiddenField1" DefaultValue="1" Name="afilter" PropertyName="Value"
            Type="String" />
        <asp:Parameter Name="asort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:MultiView ID="MultiView1" runat="server">
    <asp:View ID="View1" runat="server">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-12">
                    <p style="font-weight: bold">List of Reports Emailed in the Last 10 days</p>
                </div>
            </div>
            <div class="form-group">

                <div class="col-md-12">
                    <div class="col-md-12" style="text-align: left;">
                        <div style="width: auto; height: auto; overflow: auto;">
                            <asp:GridView ID="gvItemList" runat="server" DataSourceID="ItemsDataSource"
                                CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                BorderWidth="1px" Width="100%">
                                <%--PageSize="100"  <RowStyle CssClass="rowstyle"/>
            <AlternatingRowStyle CssClass="altrowstyle" />
            <HeaderStyle  CssClass="headerstyle" />
            <PagerStyle CssClass="pagerstyle" />
                        <PagerTemplate>
                           
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                            <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                            <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                            <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                        </PagerTemplate>--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="Print">
                                        <ItemTemplate>
                                            &nbsp;
                                             <asp:LinkButton ID="btnPrintItem" CssClass="icon ti-printer" runat="server" />
            <%--	<asp:ImageButton ID="btnPrintItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                            &nbsp;
                                        </ItemTemplate>
                                        <ItemStyle Width="35px" Wrap="False" />
                                    </asp:TemplateField>


                                    <asp:BoundField DataField="StartDate" SortExpression="StartDate" HeaderText="Date Sent">
                                        <ItemStyle Width="80px" Height="15px" Wrap="False" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client Code">
                                        <ItemStyle  Height="15px" Width="120px" Wrap="False" />
                                    </asp:BoundField>

                                    <asp:BoundField DataField="Description" SortExpression="ReportName" HeaderText="Report Name">
                                        <ItemStyle Height="15px" Wrap="False" />
                                    </asp:BoundField>

                                </Columns>
                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </asp:View>
    <asp:View ID="View2" runat="server">
        <div class="body">
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-3" align="left">
                        <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Selection" OnClick="Linkbutton2_Click"></asp:LinkButton>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-12" >
                        <div style="width: auto; height: auto; overflow: auto;">
                            <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-3" align="center">
                        <asp:Button ID="btnDownLoad" Text="DownLoad SpreadSheet" runat="server" OnClick="btnDownLoad_Click" />
                    </div>
                </div>
            </div>


        </div>
    </asp:View>
</asp:MultiView>

<asp:HiddenField ID="HiddenField1" runat="server" />
