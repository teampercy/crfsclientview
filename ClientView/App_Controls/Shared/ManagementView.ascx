<%@ Control Language="VB" ClassName="PeopleView" %>

<style type="text/css">
    .style1
    {
        font-family: Arial, Helvetica, sans-serif;
    }
    .style2
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: small;
    }
    .style3
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: medium;
    }
</style>

<script runat="server">


    Protected Sub GridViewAllMgmt_RowCreated(ByVal [source] As Object, ByVal e As GridViewRowEventArgs)
        If e.Row Is Nothing Then
            Return
        End If
        ' display if 'Visible' = true
        'If e.Row.RowType = DataControlRowType.DataRow And Not (e.Row.DataItem Is Nothing) Then
        '    Dim visible As Boolean = CBool(DataBinder.Eval(e.Row.DataItem, "IsVisible"))
        '    e.Row.Visible = visible
        'End If

        ' display image if a url is specifid
        Dim imageUrl As String = CStr(DataBinder.Eval(e.Row.DataItem, "ImageUrl"))
        If [String].Empty.Equals(imageUrl) Then
            Dim newsImage As Image = CType(e.Row.FindControl("Image2"), Image)
            If Not (newsImage Is Nothing) Then
                newsImage.Visible = False
            End If
        End If
    End Sub
    
    Protected Function GetImage(ByVal itemId As Integer) As String
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Image
        CRF.CLIENTVIEW.BLL.ProviderBase.DAL.Read(itemId, myitem)
        
        Dim s As String = "~/images/" & myitem.PKID & "-" & myitem.UniqueId & ".bmp"
        If System.IO.File.Exists(Me.MapPath(s)) = False Then
            CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.Image, Me.MapPath(s))
        End If
        
        Return s
        
    End Function
    
</script>

<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
		OldValuesParameterFormatString="original_{0}" SelectMethod="GetEmployees"
		TypeName="Data.EmployeeProvider">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="maxrows" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="afilter" SessionField="employeefilter"
                Type="String" />
            <asp:Parameter Name="asort" Type="String" />
        </SelectParameters>
</asp:ObjectDataSource>
<asp:GridView ID="GridViewAllMgmt" runat="server" AutoGenerateColumns="False" DataSourceID="ItemsDataSource"
                 AllowPaging="True" PageSize="4" OnRowCreated="GridViewAllMgmt_RowCreated" BorderWidth="0"
                 BorderColor="White" DataKeyNames="PKId">
                 <Columns>
                     <asp:TemplateField>
                         <ItemTemplate>
                            <div class="line"></div>
                            <asp:Image ID="Image2" Width="200px" Height="150px" runat="server" ImageUrl='<%# getImage(Eval("ImageId")) %>'
                                AlternateText='<%# Eval("ImageAltText")%>' CssClass="photo-float-left photo-border" />
                             <h2 class="style3">
                                <%#Eval("FullName")%>
                            </h2>
                            <h3 class="style2">                                
                                    <%# Eval("Title")%>                                
                            </h3>
                            <p class="style2">
                                <%# Eval("Description")%>
                            </p>
                            <ul>
                                <li>Phone:
                                    <%#Eval("Phone")%>
                                </li>
                                <li>Fax:
                                    <%#Eval("Fax")%>
                                </li>
                                <li>Email:
                                    <%# Eval("Email")%>
                                </li>
                            </ul>
                            <p />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerSettings Mode="NumericFirstLast" />
            </asp:GridView>
