Imports HDS.WEBSITE
Partial Class App_Controls_Common_SideMenu
    Inherits UI.BaseUserControl
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.Literal1.Text = GetHTML()
        End If
    End Sub
    Private Function GetHTML() As String
        If Me.Page.Request.QueryString.Count > 0 Then
            Dim ss As String() = Strings.Split(Me.Page.Request.QueryString(0), ".")
            Dim pagename As String = "?" & ss(0)
            Dim myuser As UI.CurrentUser = Me.MyPage.CurrentUser
            For Each oItem As UI.MenuItem In myuser.usermenu.Items
                If oItem.Link = pagename Then
                    Return GetSubMenuHtml(oItem)
                End If
                'If oItem.Items.Count > 0 Then
                '    Dim s As String = GetPageItems(pagename, oItem)
                '    If s <> String.Empty Then
                '        Return s
                '    End If
                'End If
            Next
        End If
        Return String.Empty
    End Function
    Private Function GetSubMenuHtml(ByVal amenuitem As UI.MenuItem) As String

        Dim oBuilder As New System.Text.StringBuilder()
        Dim s As String = "<h1>[TITLE]</h1>"
        s = Strings.Replace(s, "[TITLE]", amenuitem.Text)
        oBuilder.AppendLine(s)
        oBuilder.AppendLine("<ul class='site-menu'>")

        For Each oItem As UI.MenuItem In amenuitem.Items
            If oItem.Items.Count > 0 Then
                s = "<li class='site-menu-item has-sub'><a href=""[URL]"">[TITLE]</a>"
            Else
                s = "<li class='site-menu-item'><a href=""[URL]"">[TITLE]</a>"
            End If

            s = Strings.Replace(s, "[URL]", oItem.Link)
            s = Strings.Replace(s, "[TITLE]", oItem.Text)
            oBuilder.AppendLine(s)
            If oItem.Items.Count > 0 Then
                oBuilder.AppendLine(GetSubMenuHtml1(oItem))
            End If
            oBuilder.AppendLine("</li>")
        Next

        oBuilder.AppendLine("</ul>")
        Return oBuilder.ToString()

    End Function
    Public Function GetSubMenuHtml1(ByVal Item As UI.MenuItem) As String
        Dim oBuilder As New System.Text.StringBuilder()
        Dim myidx As Integer = 0
        oBuilder.AppendLine("<ul class='site-menu-sub'>")
        For Each oItem As UI.MenuItem In Item.Items
            myidx += 1
            Dim s As String
            If oItem.Items.Count > 0 Then
                s = "<li class='site-menu-item has-sub'><a href=""[URL]"">[TITLE]</a>"
            Else
                s = "<li class='site-menu-item'><a href=""[URL]"">[TITLE]</a>"
            End If
            s = Strings.Replace(s, "[URL]", oItem.Link)
            s = Strings.Replace(s, "[TITLE]", oItem.Text)
            If oItem.Items.Count > 0 Then
                oBuilder.AppendLine(s)
                oBuilder.AppendLine("<ul class='site-menu-sub'>")
                oBuilder.AppendLine(GetSubMenuHtml2(oItem))
                oBuilder.AppendLine("</ul>")
            Else
                oBuilder.Append(s)
            End If
            oBuilder.AppendLine("</li>")

        Next
        oBuilder.AppendLine("</ul>")
        Return oBuilder.ToString()

    End Function
    Private Function GetSubMenuHtml2(ByVal item As UI.MenuItem) As String
        Dim oBuilder As New System.Text.StringBuilder()
        For Each oItem As UI.MenuItem In item.Items
            Dim s As String = "<li class='site-menu-item'><a href=""[URL]"">[TITLE]</a>"
            s = Strings.Replace(s, "[URL]", oItem.Link)
            s = Strings.Replace(s, "[TITLE]", oItem.Text)
            oBuilder.AppendLine(s)
            oBuilder.AppendLine("</li>")
        Next
        Return oBuilder.ToString()
    End Function

End Class
