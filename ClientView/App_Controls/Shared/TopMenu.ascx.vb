Imports HDS.WEBSITE
Partial Class App_Controls_Common_TopMenu
    Inherits UI.BaseUserControl
     Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.Literal1.Text = GetHtml()
        End If
    End Sub
    Public Property IsDropDown() As String
        Get
            Return Me.HiddenField1.Value
        End Get
        Set(ByVal value As String)
            Me.HiddenField1.Value = value
        End Set
    End Property
    'Public Function GetHtml() As String
    '    'Dim myuser As HDS.WEBLIB.Providers.Membership.CurrentUser = HDS.WEBLIB.Providers.Membership.GetCurrentUser
    '    Dim myuser As UI.CurrentUser = Me.MyPage.CurrentUser
    '    Dim oBuilder As New System.Text.StringBuilder()
    '    oBuilder.AppendLine("<div class=""menu"" >")

    '    oBuilder.AppendLine("<ul>")
    '    Dim myidx As Integer = 0
    '    For Each oItem As UI.MenuItem In myuser.usermenu.Items
    '        myidx += 1
    '        Dim s As String = "<li class=""menulink"" ><a href=""[URL]"" rel=""[SUBID]"">[TITLE]</a>"
    '        s = Strings.Replace(s, "[URL]", oItem.Link)
    '        s = Strings.Replace(s, "[TITLE]", oItem.Text)
    '        s = Strings.Replace(s, "[SUBID]", "submenu-" & myidx)
    '        oBuilder.Append(s)
    '        If oItem.Items.Count > 0 And Boolean.Parse(IsDropDown) = True Then
    '            oBuilder.AppendLine(GetSubMenuHtml1(oItem))
    '        End If

    '        oBuilder.AppendLine("</li>")

    '    Next
    '    oBuilder.AppendLine("</ul>")
    '    oBuilder.AppendLine("</div>")

    '    Return oBuilder.ToString()

    'End Function
    'Public Function GetSubMenuHtml1(ByVal Item As UI.MenuItem) As String
    '    Dim oBuilder As New System.Text.StringBuilder()
    '    Dim myidx As Integer = 0
    '    oBuilder.AppendLine("<ul  class=""menusublink"">")
    '    For Each oItem As UI.MenuItem In Item.Items
    '        myidx += 1
    '        myidx += 1
    '        Dim s As String = "<li class=""topline"" ><a href=""[URL]"" rel=""[SUBID]"">[TITLE]</a>"
    '        If oItem.Items.Count > 0 Then
    '            s = "<li class=""sub"" ><a href=""[URL]"" rel=""[SUBID]"">[TITLE]</a>"
    '        End If
    '        s = Strings.Replace(s, "[URL]", oItem.Link)
    '        s = Strings.Replace(s, "[TITLE]", oItem.Text)
    '        s = Strings.Replace(s, "[SUBID]", "submenu-" & myidx)
    '        oBuilder.AppendLine(s)
    '        If oItem.Items.Count > 0 Then
    '            oBuilder.AppendLine("<ul>")
    '            oBuilder.AppendLine(GetSubMenuHtml2(oItem))
    '            oBuilder.AppendLine("</ul>")
    '        End If
    '        oBuilder.AppendLine("</li>")

    '    Next
    '    oBuilder.AppendLine("</ul>")
    '    Return oBuilder.ToString()

    'End Function
    'Private Function GetSubMenuHtml2(ByVal item As UI.MenuItem) As String
    '    Dim oBuilder As New System.Text.StringBuilder()
    '    Dim myidx As Integer = 0
    '    For Each oItem As UI.MenuItem In item.Items
    '        myidx += 1
    '        Dim s As String = "<li class=""topline"" ><a href=""[URL]"" rel=""[SUBID]"">[TITLE]</a>"
    '        s = Strings.Replace(s, "[URL]", oItem.Link)
    '        s = Strings.Replace(s, "[TITLE]", oItem.Text)
    '        s = Strings.Replace(s, "[SUBID]", "submenu-" & myidx)
    '        oBuilder.AppendLine(s)
    '        oBuilder.AppendLine("</li>")
    '    Next
    '    Return oBuilder.ToString()
    'End Function

    Public Function GetHtml() As String
        'Dim myuser As HDS.WEBLIB.Providers.Membership.CurrentUser = HDS.WEBLIB.Providers.Membership.GetCurrentUser
        Dim myuser As UI.CurrentUser = Me.MyPage.CurrentUser
        Dim oBuilder As New System.Text.StringBuilder()
        'oBuilder.AppendLine("<div class=""menu"" >")

        oBuilder.AppendLine("<ul class=""site-menu"">")
        Dim myidx As Integer = 0
        For Each oItem As UI.MenuItem In myuser.usermenu.Items
            myidx += 1
            Dim s As String
            If oItem.Items.Count > 0 Then
                ''s = "<li class=""site-menu-item has-sub"" id=""[Linkid]""><a href=""javascript:void(0)""><i class=""site-menu-icon wb-layout"" aria-hidden=""true""></i> <span class=""site-menu-title"">[TITLE]</span><span class=""site-menu-arrow""></span></a>"
                ''Modified by kedar

                s = "<li class=""site-menu-item has-sub"" id=""[Linkid]""><a href=""[LINK]""><i class=""site-menu-icon wb-layout"" aria-hidden=""true""></i> <span class=""site-menu-title"">[TITLE]</span><span class=""site-menu-arrow""></span></a>"
            Else
                If oItem.LinkId <> "" Then
                    s = "<li class=""site-menu-item"" id=""[Linkid]""><a href=""[LINK]"" class=""animsition-link""><i class=""site-menu-icon wb-layout"" aria-hidden=""true""></i> <span class=""site-menu-title"">[TITLE]</span></a>"
                Else
                    s = "<li class=""site-menu-category"" id=""[Linkid]"">[TITLE]"
                End If
              

            End If
            s = Strings.Replace(s, "[TITLE]", oItem.Text)
            s = Strings.Replace(s, "[Linkid]", oItem.LinkId)
            s = Strings.Replace(s, "[LINK]", oItem.Link)
            oBuilder.Append(s)
            If oItem.Items.Count > 0 And Boolean.Parse(IsDropDown) = True Then
                oBuilder.AppendLine(GetSubMenuHtml1(oItem))
            End If

            oBuilder.AppendLine("</li>")

        Next
        oBuilder.AppendLine("</ul>")
        'oBuilder.AppendLine("</div>")

        Return oBuilder.ToString()

    End Function
    Public Function GetSubMenuHtml1(ByVal Item As UI.MenuItem) As String
        Dim oBuilder As New System.Text.StringBuilder()
        Dim myidx As Integer = 0
        oBuilder.AppendLine("<ul  class=""site-menu-sub"">")
        For Each oItem As UI.MenuItem In Item.Items
            myidx += 1
            myidx += 1
            Dim s As String = "<li class=""site-menu-item"" id=""[Linkid]"" ><a class=""animsition-link"" href=""[URL]"" rel=""[SUBID]""><span class=""site-menu-title"">[TITLE]</span></a>"
            If oItem.Items.Count > 0 Then
                s = "<li class=""site-menu-item has-sub""  id=""[Linkid]""><a class=""animsition-link"" href=""[URL]"" rel=""[SUBID]""><span class=""site-menu-title"">[TITLE]</span></a>"
            End If
            s = Strings.Replace(s, "[URL]", oItem.Link)
            s = Strings.Replace(s, "[TITLE]", oItem.Text)
            s = Strings.Replace(s, "[Linkid]", oItem.LinkId)
            s = Strings.Replace(s, "[SUBID]", "submenu-" & myidx)
            oBuilder.AppendLine(s)
            If oItem.Items.Count > 0 Then
                oBuilder.AppendLine("<ul class=""site-menu-sub"">")
                oBuilder.AppendLine(GetSubMenuHtml2(oItem))
                oBuilder.AppendLine("</ul>")
            End If
            oBuilder.AppendLine("</li>")

        Next
        oBuilder.AppendLine("</ul>")
        Return oBuilder.ToString()

    End Function
    Private Function GetSubMenuHtml2(ByVal item As UI.MenuItem) As String
        Dim oBuilder As New System.Text.StringBuilder()
        Dim myidx As Integer = 0
        For Each oItem As UI.MenuItem In item.Items
            myidx += 1
            Dim s As String = "<li class=""site-menu-item"" id=""[Linkid]""><a class=""animsition-link"" href=""[URL]"" rel=""[SUBID]""><span class=""site-menu-title"">[TITLE]</span></a>"
            s = Strings.Replace(s, "[URL]", oItem.Link)
            s = Strings.Replace(s, "[TITLE]", oItem.Text)
            s = Strings.Replace(s, "[Linkid]", oItem.LinkId)
            s = Strings.Replace(s, "[SUBID]", "submenu-" & myidx)
            oBuilder.AppendLine(s)
            oBuilder.AppendLine("</li>")
        Next
        Return oBuilder.ToString()
    End Function

    
  
End Class
