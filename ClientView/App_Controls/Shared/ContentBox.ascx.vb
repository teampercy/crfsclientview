Imports System
Imports System.Drawing
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Text.RegularExpressions
Imports HDS.WEBLIB.Controls
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBSITE
Partial Class App_Controls_Common_ContentBox
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim _templatename As String = String.Empty
    Dim MYITEM As New TABLES.Portal_Content
    Public Property TemplateName() As String
        Get
            Return Me.ViewState("content_" & Me.ID)
        End Get
        Set(ByVal value As String)
            Me.ViewState("content_" & Me.ID) = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.htmledit1.Visible = False
            If Me.CurrentUser.IsInRole("ADMIN") = True Then
                Me.htmledit1.Visible = True
            End If
            
            ProviderBase.DAL.GetItem(MYITEM.TableObjectName, "contentname", Me.TemplateName, MYITEM)
            Me.htmledit1.Value = MYITEM.content
            Me.Literal1.Text = MYITEM.content

          
        End If
     
    End Sub
    Private Sub htmledit1_SaveContentClicked() Handles htmledit1.SaveContentClicked
        ProviderBase.DAL.GetItem(MYITEM.TableObjectName, "contentname", Me.TemplateName, MYITEM)
        MYITEM.content = htmledit1.Value
        MYITEM.contentname = Me.TemplateName
        If MYITEM.PKID < 1 Then
            ProviderBase.DAL.Create(MYITEM)
        Else
            ProviderBase.DAL.Update(MYITEM)
        End If
        Me.Literal1.Text = MYITEM.content

    End Sub
End Class
