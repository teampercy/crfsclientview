<%@ Control Language="VB" ClassName="EmployeeProfileView" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.HiddenField1.Value = Me.Page.Request.QueryString("ItemId")
        Me.MultiView1.SetActiveView(Me.View1)
        If Me.HiddenField1.Value.Length > 0 Then
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        
        If gvItemDetail.Rows.Count > 0 Then
            Me.gvItemDetail.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
         ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "CallSuccessFuncDetail();", True)
    End Sub
    
    Protected Sub gvItemList_RowCreated(ByVal [source] As Object, ByVal e As GridViewRowEventArgs)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        
        If gvItemDetail.Rows.Count > 0 Then
            Me.gvItemDetail.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "CallSuccessFuncDetail();", True)
        If e.Row Is Nothing Then
            Return
        End If
        ' display if 'Visible' = true
        'If e.Row.RowType = DataControlRowType.DataRow And Not (e.Row.DataItem Is Nothing) Then
        '    Dim visible As Boolean = CBool(DataBinder.Eval(e.Row.DataItem, "IsVisible"))
        '    e.Row.Visible = visible
        'End If

        ' display image if a url is specifid
        Dim imageUrl As String = CStr(DataBinder.Eval(e.Row.DataItem, "ImageUrl"))
        If [String].Empty.Equals(imageUrl) Then
            Dim newsImage As Image = CType(e.Row.FindControl("Image2"), Image)
            If Not (newsImage Is Nothing) Then
                newsImage.Visible = False
            End If
        End If
    End Sub
    Protected Sub gvItemDetail_RowCreated(ByVal [source] As Object, ByVal e As GridViewRowEventArgs)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        
        If gvItemDetail.Rows.Count > 0 Then
            Me.gvItemDetail.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "CallSuccessFuncDetail();", True)
        If e.Row Is Nothing Then
            Return
        End If
        ' display if 'Visible' = true
        'If e.Row.RowType = DataControlRowType.DataRow And Not (e.Row.DataItem Is Nothing) Then
        '    Dim visible As Boolean = CBool(DataBinder.Eval(e.Row.DataItem, "IsVisible"))
        '    e.Row.Visible = visible
        'End If

        ' display image if a url is specifid
        Dim imageUrl As String = CStr(DataBinder.Eval(e.Row.DataItem, "ImageUrl"))
        If [String].Empty.Equals(imageUrl) Then
            Dim newsImage As Image = CType(e.Row.FindControl("Image2"), Image)
            If Not (newsImage Is Nothing) Then
                newsImage.Visible = False
            End If
        End If
    End Sub
    Protected Function GetImage(ByVal itemId As Integer) As String
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Image
        CRF.CLIENTVIEW.BLL.ProviderBase.DAL.Read(itemId, myitem)
        
        Dim s As String = "~/images/" & myitem.PKID & "-" & myitem.UniqueId & ".bmp"
        If System.IO.File.Exists(Me.MapPath(s)) = False Then
            CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.Image, Me.MapPath(s))
        End If
        Return s
        
    End Function

</script>
<style type="text/css">
    #divBreadcrumb {
        padding-top: 0px;
    }

    .breadcrumb > li + li:before {
        padding: 0 5px;
        color: #62a8ea;
        content: none;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        //  $('#liJobs').addClass('site-menu-item has-sub active open');
        //  $('#lisubJobList').addClass('site-menu-item active');
        SetHeaderBreadCrumb('', 'My Account', 'Employees Of The Month');
        //MainMenuToggle('liSubEmployeeMonth');
        SubMenuToggle('liSubEmployeeMonth');
        CallSuccessFunc();
        return false;

    }
    function CallSuccessFunc() {

        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [5, 10, 25, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvItemList.ClientID%>').dataTable(options);


    }
    function CallSuccessFuncDetail() {

        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [5, 10, 25, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvItemDetail.ClientID%>').dataTable(options);


    }

</script>
<div class="form-horizontal">
    <div class="form-group">

        <div class="col-md-12">
            <h2 style="font-family: Arial, Helvetica, sans-serif; font-size: x-large">Employee Profiles</h2>
        </div>
    </div>


    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="form-group">

                <div class="col-md-12">
                    <div class="col-md-12" style="text-align: left;">
                         <div style="width: auto; height: auto; overflow: auto;">
                    <asp:GridView ID="gvItemList" runat="server" AutoGenerateColumns="False" DataSourceID="ItemsDataSource"
                         OnRowCreated="gvItemList_RowCreated" 
                        BorderColor="white" CssClass="table dataTable table-striped">
                        <Columns>
                            <asp:TemplateField HeaderText=" ">
                                <ItemTemplate>
                                    <br />
                                    <asp:Image ID="Image1" Width="160px" Height="120px" runat="server" ImageUrl='<%# getImage(Eval("ImageId")) %>'
                                        AlternateText='<%# Eval("ImageAltText")%>' CssClass="photo-float-left photo-border" />
                                    <h2><%#Eval("Title")%></h2>
                                    <p><strong><%#Eval("shortdescription")%></strong></p>
                                    <p><%#Eval("Description")%></p>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
<%--                        <PagerSettings Mode="NumericFirstLast" Position="Bottom" />--%>
                    </asp:GridView></div></div>
                </div>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="form-group">

                <div class="col-md-12">
                    <div class="col-md-12" style="text-align: left;">
                    <asp:GridView ID="gvItemDetail" runat="server" AutoGenerateColumns="False" DataSourceID="ItemsDetailDataSource"
                         OnRowCreated="gvItemDetail_RowCreated" 
                        BorderColor="White">
                        <Columns>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <h2>
                                        <%#Eval("Title")%>
                                    </h2>
                                    <p class="style3">
                                        <strong><%#strings.FormatDateTime(Eval("DateCreated"),DateFormat.ShortDate)%></strong><br />
                                    </p>
                                    <p>
                                        <asp:Image ID="Image1" Width="150px" Height="100px" runat="server" ImageUrl='<%# getImage(Eval("ImageId")) %>'
                                            AlternateText='<%# Eval("ImageAltText")%>' CssClass="photo-float-left photo-border" />
                                        <%#Eval("description")%>&nbsp;&nbsp;<br />
                                        <br />
                                        <a href="MainDefault.aspx?myaccount.employeeprofileview">Back to List...</a>
                                    </p>

                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                      <%--  <PagerSettings Mode="NumericFirstLast" Position="TopAndBottom" />--%>
                    </asp:GridView></div>
                </div>
            </div>

        </asp:View>
    </asp:MultiView>
    <asp:ObjectDataSource ID="ItemsDataSource" runat="server"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetEmployees"
        TypeName="Data.NewsProvider">
        <SelectParameters>
            <asp:Parameter DefaultValue="5" Name="maxrows" Type="Int32" />
            <asp:Parameter DefaultValue="" Name="afilter" Type="String" />
            <asp:Parameter Name="asort" Type="String" DefaultValue="DATECREATED DESC" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ItemsDetailDataSource" runat="server"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetEmployeeItem"
        TypeName="Data.NewsProvider">
        <SelectParameters>
            <asp:ControlParameter ControlID="HiddenField1" DefaultValue="0" Name="pkid" PropertyName="Value"
                Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:HiddenField ID="HiddenField1" runat="server" />

</div>
