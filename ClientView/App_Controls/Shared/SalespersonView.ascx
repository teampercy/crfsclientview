<%@ Control Language="VB" ClassName="PeopleView" %>
<script runat="server">


    Protected Sub gvItemList_RowCreated(ByVal [source] As Object, ByVal e As GridViewRowEventArgs)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        If e.Row Is Nothing Then
            Return
        End If
        ' display if 'Visible' = true
        'If e.Row.RowType = DataControlRowType.DataRow And Not (e.Row.DataItem Is Nothing) Then
        '    Dim visible As Boolean = CBool(DataBinder.Eval(e.Row.DataItem, "IsVisible"))
        '    e.Row.Visible = visible
        'End If

        ' display image if a url is specifid
        Dim imageUrl As String = CStr(DataBinder.Eval(e.Row.DataItem, "ImageUrl"))
        If [String].Empty.Equals(imageUrl) Then
            Dim newsImage As Image = CType(e.Row.FindControl("Image2"), Image)
            If Not (newsImage Is Nothing) Then
                newsImage.Visible = False
            End If
        End If
    End Sub
    
    Protected Function GetImage(ByVal itemId As Integer) As String
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Image
        CRF.CLIENTVIEW.BLL.ProviderBase.DAL.Read(itemId, myitem)
        
        Dim s As String = "~/images/" & myitem.PKID & "-" & myitem.UniqueId & ".bmp"
        If System.IO.File.Exists(Me.MapPath(s)) = False Then
            CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.Image, Me.MapPath(s))
        End If
        
        Return s
        
    End Function
</script>
<style type="text/css">
    #divBreadcrumb {
        padding-top: 0px;
    }

    .breadcrumb > li + li:before {
        padding: 0 5px;
        color: #62a8ea;
        content: none;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        //  $('#liJobs').addClass('site-menu-item has-sub active open');
        //  $('#lisubJobList').addClass('site-menu-item active');
        SetHeaderBreadCrumb('', 'My Account', 'Marketing Team');
        //MainMenuToggle('liSubEmployeeMonth');
        SubMenuToggle('liSubMarketing');
        CallSuccessFunc();
        return false;

    }
    function CallSuccessFunc() {

        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [5, 10, 25, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvItemList.ClientID%>').dataTable(options);


    }


</script>
<div class="form-horizontal">
  <%--  <div class="form-group">

        <div class="col-md-12">
            <h2 style="font-family: Arial, Helvetica, sans-serif; font-size: x-large">Management Team</h2>
        </div>
    </div>--%>


   <asp:ObjectDataSource ID="ItemsDataSource" runat="server"
		OldValuesParameterFormatString="original_{0}" SelectMethod="GetEmployees"
		TypeName="Data.SalesProvider">
        <SelectParameters>
            <asp:Parameter DefaultValue="0" Name="maxrows" Type="Int32" />
            <asp:SessionParameter DefaultValue="" Name="afilter" SessionField="employeefilter"
                Type="String" />
            <asp:Parameter Name="asort" Type="String" />
        </SelectParameters>
</asp:ObjectDataSource>
    <div class="form-group">

        <div class="col-md-12">
            <div class="col-md-12" style="text-align: left;">
                 <div style="width: auto; height: auto; overflow: auto;">
               <asp:GridView ID="gvItemList" runat="server" AutoGenerateColumns="False" DataSourceID="ItemsDataSource"
                OnRowCreated="gvItemList_RowCreated" CssClass="table dataTable table-striped"
                BorderColor="white" DataKeyNames="PKId">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                        <br />
                            <asp:Image ID="Image2" Width="120px" Height="160px" runat="server" ImageUrl='<%# getImage(Eval("ImageId")) %>'
                                AlternateText='<%# Eval("ImageAltText")%>' CssClass="photo-float-left photo-border" />
                            <h2><%#Eval("FullName")%>
                            </h2>
                            <p><strong><%# Eval("Title")%></strong></p>
                            <p>
                                <%# Eval("Description")%>
                            </p>
                            <ul>
                                <li>Phone:
                                    <%#Eval("Phone")%>
                                </li>
                                <li>Fax:
                                    <%#Eval("Fax")%>
                                </li>
                                <li>Email:
                                    <a href="<%# Eval("Email")%>"><%# Eval("Email")%></a>
                                </li>
                            </ul>
                            <p />
                         
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                   <EmptyDataTemplate>No Records Found</EmptyDataTemplate>
               <%-- <PagerSettings Mode="NumericFirstLast" Position="Bottom" />--%>
            </asp:GridView></div>
            </div>
        </div>
    </div>
</div>




