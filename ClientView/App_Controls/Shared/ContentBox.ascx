<%@ Control Language="VB" AutoEventWireup="false" CodeFile="ContentBox.ascx.vb" Inherits="App_Controls_Common_ContentBox" %>
<%@ Register TagPrefix="cc1" Namespace="HDS.WEBLIB.Controls" Assembly="HDS.WEBLIB" %>
<style type="text/css">
    #divBreadcrumb {
        padding-top: 0px;
    }

    .breadcrumb > li + li:before {
        padding: 0 5px;
        color: #62a8ea;
        content: none;
    }
    table {
    margin:auto;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        var index = location.href.lastIndexOf(".");
        var View = location.href.substring(index + 1, location.href.length);
        if (View == "contactus") {
            SetHeaderBreadCrumb('', 'My Account', 'Contact Us');
            SubMenuToggle('liSubContactUs');
        }
        else {
            SetHeaderBreadCrumb('', '', '');
        }

        // CallSuccessFunc();
        return false;
    }


</script>
<asp:HiddenField ID="HiddenField1" runat="server" />
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-12" >
            <SiteControls:HTMLEditor ID="htmledit1" runat="server" />
            <!--content goes here -->
            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
        </div>
    </div>
</div>
