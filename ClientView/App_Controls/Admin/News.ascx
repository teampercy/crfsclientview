<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<script type="text/javascript">
    $(document).ready(function () {
        CallSuccessFunc();
    });
    function MaintainMenuOpen() {
        //  $('#liJobs').addClass('site-menu-item has-sub active open');
        //  $('#lisubJobList').addClass('site-menu-item active');
        SetHeaderBreadCrumb('Admin', 'Site Settings', 'News');
        MainMenuToggle('liSiteSetting');
        SubMenuToggle('liSubNews');
        return false;

    }
    function CallSuccessFunc() {
        // StartLoading();
        //debugger;
        //alert('jayaa');
        //debugger;
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [15, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvItemList.ClientID%>').dataTable(options);

        // EndLoading()
    }
</script>
<script runat="server">
    Protected WithEvents btnDeleteItem As LinkButton
    Protected WithEvents btnEditItem As LinkButton
    Dim myitem As New TABLES.Portal_Post
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ValidationSummary1.Visible = False
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Session("filter") = ""
            Me.gvItemList.DataBind()
            If gvItemList.Rows.Count > 0 Then
                Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If

    End Sub
    Protected Sub btnFindText_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindText.Click
        Dim myfilter As New HDS.DAL.COMMON.FilterBuilder
        Dim s As String = ""
        myfilter.Clear()
        myfilter.SearchAnyWords("title", Me.txtFindText.Text, "OR")
        myfilter.SearchAnyWords("pagename", Me.txtFindText.Text, "")
        s = myfilter.GetFilter
        Me.Session("filter") = s
        Me.gvItemList.DataBind()
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.Session("filter") = ""
        Me.gvItemList.DataBind()
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteItem"), LinkButton).Click, AddressOf btnDeleteItem_Click
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= Me.gvItemList.PageCount Then
            Me.gvItemList.PageIndex = pageNumber - 1
        Else
            Me.gvItemList.PageIndex = 0
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As TABLES.Portal_Post = e.Row.DataItem
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteItem.Attributes("rowno") = dr.PKID
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = dr.PKID

        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
            lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

            Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
            txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

            Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
            ddlPageSize.SelectedValue = gridView.PageSize.ToString()
        End If
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub gvItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvItemList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)
   
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.ValidationSummary1.Visible = True
        If Page.IsValid = False Then Exit Sub
        If Me.HiddenField1.Value < 1 Then
            HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
            myitem.content = Me.htmledit1.Value
            myitem.datecreated = Now()
            If myitem.category.Length < 1 Then
                myitem.category = "NEWS"
            End If
            ProviderBase.DAL.Create(myitem)
        Else
            ProviderBase.DAL.Read(Me.HiddenField1.Value, myitem)
            HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
            myitem.content = Me.htmledit1.Value
            myitem.datecreated = Now()
            If myitem.category.Length < 1 Then
                myitem.category = "NEWS"
            End If
          
            ProviderBase.DAL.Update(myitem)
        End If

        Me.gvItemList.DataBind()
        Me.MultiView1.SetActiveView(Me.View1)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.MultiView1.SetActiveView(Me.View1)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        HiddenField1.Value = -1

        Me.MultiView1.SetActiveView(Me.View2)

    End Sub
    Private Sub btnDeleteItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteItem.Click
        btnDeleteItem = TryCast(sender, LinkButton)
        Dim keyval As String = btnDeleteItem.Attributes("rowno")
        ProviderBase.DAL.Delete(myitem)
        Me.gvItemList.DataBind()
        Me.MultiView1.SetActiveView(Me.View1)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        HiddenField1.Value = btnEditItem.Attributes("rowno")
        ProviderBase.DAL.Read(Me.HiddenField1.Value, myitem)
        HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        Me.htmledit1.Value = myitem.content
        Me.MultiView1.SetActiveView(Me.View2)
      
    End Sub
</script>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetNews"
    TypeName="Data.NewsProvider">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="maxrows" Type="Int32" />
        <asp:Parameter Name="afilter" Type="String" />
        <asp:Parameter Name="asort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
&nbsp;
<div id="modcontainer" style="Width: 100%;">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1>Pages</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-2">
                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="New Post" />
                        </div>
                        <div class="col-md-2">
                            <cc1:DataEntryBox ID="txtFindText" runat="server" Text="" />
                        </div>
                        <div class="col-md-2">
                            <asp:LinkButton ID="btnFindText" runat="server" Text="Search" CssClass="btn btn-primary" />
                        </div>
                        <div class="col-md-2">
                            <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-primary" />
                        </div>
                    </div>
                </div>
                <div class="form-group">

                    <div class="col-md-12">
                        <asp:UpdatePanel ID="udpChannelDetails" runat="server">
                            <ContentTemplate>
                                <div style="width: auto; height: auto; overflow: auto;">

                                    <asp:GridView ID="gvItemList" runat="server" DataSourceID="ItemsDataSource"
                                        CssClass="table dataTable table-striped" AllowPaging="True" AutoGenerateColumns="False"
                                        BorderWidth="1px" Width="100%" OnSelectedIndexChanged="gvItemList_SelectedIndexChanged">
                                        <%--   <RowStyle CssClass="rowstyle" />
                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                        <HeaderStyle CssClass="headerstyle" />
                                        <PagerStyle CssClass="pagerstyle" />
                                        <PagerTemplate>
                                            <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Value="5" />
                                                <asp:ListItem Value="10" />
                                                <asp:ListItem Value="15" />
                                                <asp:ListItem Value="20" />
                                            </asp:DropDownList>
                                            &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                            of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                            &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                            <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                            <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                            <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                                        </PagerTemplate>--%>
                                        <Columns>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                      <asp:LinkButton ID="btnEditItem" CssClass="icon ti-write" runat="server" />
                                                  <%--  <asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                    &nbsp;&nbsp;&nbsp;
                                                </ItemTemplate>
                                                <ItemStyle Width="20px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="title" HeaderText="title" SortExpression="title">
                                                <ItemStyle Width="100px" />
                                            </asp:BoundField>

                                            <asp:BoundField DataField="title" HeaderText="title" SortExpression="title"></asp:BoundField>

                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                     <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')" />
                                                 <%--   <asp:ImageButton ID="btnDeleteItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" />--%>
                                                    &nbsp;&nbsp;&nbsp;
                                                </ItemTemplate>
                                                <ItemStyle Width="20px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="btnFindText" EventName="Click" />
                            </Triggers>
                        </asp:UpdatePanel>
                        <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="udpChannelDetails">
                            <Animations>
                    <OnUpdating>
                        <Parallel Duration="0">
                            <EnableAction AnimationTarget="btnFindText" Enabled="false" />
                            <FadeOut MinimumOpacity=".5" />
                        </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel Duration="0">
                            <FadeIn MinimumOpacity=".5" />
                            <EnableAction AnimationTarget="btnFindText" Enabled="true" />
                        </Parallel>                    
                    </OnUpdated>
                            </Animations>
                        </ajaxToolkit:UpdatePanelAnimationExtender>
                    </div>
                </div>


            </div>

        </asp:View>
        <asp:View ID="View2" runat="server">
            <h1>Edit Page</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12" align="left">
                            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Back to List"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Title:</label>
                        <div class="col-md-7">
                            <cc1:DataEntryBox ID="title" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Tags:</label>
                        <div class="col-md-7">
                            <cc1:DataEntryBox ID="category" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Image:</label>
                        <div class="col-md-7">
                            <cc1:DataEntryBox ID="imageurl" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Image Text:</label>
                        <div class="col-md-7">
                            <cc1:DataEntryBox ID="imagealttext" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Content:</label>
                        <div class="col-md-7">
                            <SiteControls:HTMLEditor ID="htmledit1" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12" align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
                            <br />
                            <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>



            </div>
            <div class="footer">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />&nbsp;
         <br />
            </div>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
