<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<script type="text/javascript">

   
    function MaintainMenuOpen() {
        MainMenuToggle('liUsers');
        SubMenuToggle('liUsers');
        SetHeaderBreadCrumb('Admin', 'Users', '');
    }
    function CallSuccessFunc() {
        //alert('call');
        debugger;
        var Columns = [
           { "bSortable": false },
           { "bSortable": false },
           { "bSortable": false },
           { "bSortable": false },
           { "bSortable": false }

        ];
        
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [15, 25, 35, 50, 100],
            "searching": false,
          
            responsive: true,
            //"bSort": false,
            //"aaSorting": false,
            "aoColumns": Columns
            //language: {
            //    "sSearchPlaceholder": "Search..",
            //    "lengthMenu": "_MENU_",
            //    "search": "_INPUT_",
            //    "paginate": {
            //        "previous": '<i class="icon wb-chevron-left-mini"></i>',
            //        "next": '<i class="icon wb-chevron-right-mini"></i>'
            //    }
            //}

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $("#<%=gvItemList.ClientID %>").dataTable(options);


    }

   function DeleteAccount(id) {
      <%-- // document.getElementById('<%=hdnEmployeeId.ClientID%>').value = $(id).attr("rowno");--%>
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteItem.ClientID%>').click();
       });
         return false;
     }
   
    $(document).ready(function () {
      
      CallSuccessFunc();

    });


</script>
<script runat="server">

    Dim myitem As New TABLES.Portal_Users
    Dim userds As New System.Data.DataSet
    Protected WithEvents btnDeleteItem As LinkButton
    Protected WithEvents btnEditItem As LinkButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ValidationSummary1.Visible = False
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Session("filter") = ""
            '  Me.gvItemList.DataBind()
            getuserslist()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If

    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "load", "loadingshow();", True)

        Me.Session("filter") = ""
        ' Me.gvItemList.DataBind()
        getuserslist()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteItem"), LinkButton).Click, AddressOf btnDeleteItem_Click
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub

    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteItem.Attributes("rowno") = dr("ID")
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = dr("ID")
        End If
        Dim gridView As GridView = DirectCast(sender, GridView)
        If (e.Row.RowType = DataControlRowType.Header) Then
            e.Row.TableSection = TableRowSection.TableHeader
        End If


    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.ValidationSummary1.Visible = True
        If Page.IsValid = False Then Exit Sub
        If Me.HiddenField1.Value < 1 Then
            HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
            ProviderBase.DAL.Create(myitem)
        Else
            ProviderBase.DAL.Read(Me.HiddenField1.Value, myitem)
            myitem.UserName = Me.UserName.Value
            myitem.LoginCode = Me.LoginCode.Value
            myitem.LoginPassword = Me.Password.Value
            myitem.nickname = Me.NickName.Value
            If Me.chkactive.Checked = False Then
                myitem.isinactive = True
            Else
                myitem.isinactive = False
            End If
            ProviderBase.DAL.Update(myitem)
        End If

        '  Me.gvItemList.DataBind()
        getuserslist()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub
    Private Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.UserName.Value = ""
        Me.Password.Value = ""
        Me.LoginCode.Value = ""
        Me.NickName.Value = ""
        Me.chkactive.Checked = False
        getuserslist()
        '  Me.gvItemList.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub
    Private Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        HiddenField1.Value = -1

        Me.MultiView1.SetActiveView(Me.View2)

    End Sub
    Private Sub btnDeleteItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteItem.Click
        btnDeleteItem = TryCast(sender, LinkButton)
        Dim keyval As String = btnDeleteItem.Attributes("rowno")
        ProviderBase.DAL.Delete(myitem)
        ' Me.gvItemList.DataBind()
        getuserslist()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub
    Private Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        HiddenField1.Value = btnEditItem.Attributes("rowno")
        ProviderBase.DAL.Read(HiddenField1.Value, myitem)
        Me.UserName.Value = myitem.UserName
        Me.Password.Value = myitem.LoginPassword
        Me.LoginCode.Value = myitem.LoginCode
        Me.NickName.Value = myitem.nickname
        If myitem.isinactive = True Then
            Me.chkactive.Checked = False
        Else
            Me.chkactive.Checked = True
        End If

        Me.MultiView1.SetActiveView(Me.View2)

    End Sub
    Private Sub getuserslist()
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetAdminUserList
        Dim mydata As System.Data.DataSet
        mysproc.UserId = Me.CurrentUser.Id
        mydata = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetDataSet(mysproc)
        Me.gvItemList.DataSource = mydata
        Me.gvItemList.DataBind()

    End Sub
</script>

<%--<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetUsers"
    TypeName="Data.UsersProvider">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="maxrows" Type="Int32" />
        <asp:SessionParameter DefaultValue="" Name="filter" SessionField="filter" Type="String" />
        <asp:SessionParameter Name="sort" SessionField="sort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>--%>


<div id="modcontainer" style="width: 100%;">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1>Users</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-4" style="text-align: left;">
                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="New" />
                                           
                            <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-primary" />
                        </div>
                  

                    </div>

                    <div class="form-group">
                        <div class="col-md-12" style="text-align: left;">
                            <asp:UpdatePanel ID="upItemList" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvItemList" runat="server" ShowHeader="true"
                                        CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                        BorderWidth="1px" >
                                      
                                        <Columns>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEditItem" CssClass="icon ti-write" runat="server" />
                                                    <%-- <asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
				  
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="username" HeaderText="username" SortExpression="username">
                                                <ItemStyle Width="30%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="logincode" HeaderText="logincode" SortExpression="logincode">
                                                <ItemStyle Width="30%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="email" HeaderText="email" SortExpression="email" >
                                                <ItemStyle Width="30%" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                    <%--<asp:ImageButton ID="btnDeleteItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" />--%>
				  
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                    </asp:GridView>
                             </ContentTemplate>
                               <%-- <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnFindText" EventName="Click" />
                                </Triggers>--%>
                            </asp:UpdatePanel>
                            <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="upItemList">
                                <Animations>
                    <OnUpdating>
                        <Parallel Duration="0">
                            <EnableAction AnimationTarget="btnFindText" Enabled="false" />
                            <FadeOut MinimumOpacity=".5" />
                        </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel Duration="0">
                            <FadeIn MinimumOpacity=".5" />
                            <EnableAction AnimationTarget="btnFindText" Enabled="true" />
                        </Parallel>                    
                    </OnUpdated>
                                </Animations>
                            </ajaxToolkit:UpdatePanelAnimationExtender>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>
        <asp:View ID="View2" runat="server">
            <h1>Edit User</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12" style="text-align: left;">
                            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Back to List"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">UserName:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="UserName" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">LoginCode:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="LoginCode" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Password:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="Password" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">NickName:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="NickName" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Active:</label>
                        <div class="col-md-4">
                            <asp:CheckBox ID="chkactive" CssClass="checkbox-custom" runat="server" />
                        </div>
                    </div>

                    <br />
                    <div class="form-group">
                        <div class="col-md-12" align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
                            <br />
                            <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />&nbsp;
         <br />
            </div>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
                              
      
   