﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseAdminUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<script runat="server">

    Dim myitem As New TABLES.Portal
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ProviderBase.DAL.Read(1, myitem)

        If Page.IsPostBack = False Then
            HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
            HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = False Then Exit Sub

        ProviderBase.DAL.Read(1, myitem)

        HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
        HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
        ProviderBase.DAL.Update(myitem)
        Session("Settings") = Nothing

    End Sub
</script>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div id="modcontainer" style="WIDTH: 700px">
    <h1>Settings</h1>
    <div class="body">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-12">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li id="SiteSetting" class="active" role="presentation"><a data-toggle="tab"  href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Site Settings</a></li>
                            <li id="EmailSetting" role="presentation" class=""><a data-toggle="tab"  href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Email Settings</a></li>
                        </ul>
                        <div class="tab-content padding-top-20">
                            <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">

                                <div class="form-group">
                                    <label class="col-md-2 control-label">Site Name</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="title" runat="server" CssClass="form-control"
                                            DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Description</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="description" runat="server" CssClass="form-control" Height="56px"
                                            DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""
                                            TextMode="MultiLine"></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Keywords</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="Keywords" runat="server" CssClass="form-control" Height="56px"
                                            DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""
                                            TextMode="MultiLine"></cc1:DataEntryBox>
                                    </div>
                                </div>


                            </div>
                            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">

                                <div class="form-group">
                                    <label class="col-md-2 control-label">SMTPServer</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="smtpserver" runat="server" CssClass="form-control" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">SMTPPort</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="smtpport" runat="server" CssClass="form-control" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Enable SSL</label>
                                    <div class="col-md-8">
                                        <div class="checkbox-custom checkbox-default">
                                            <asp:CheckBox ID="smtpenablessl" runat="server" Text=" " />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">SMTPLogin</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="smtplogin" runat="server" CssClass="form-control" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">SMTPPassword</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="smtppassword" runat="server" CssClass="form-control" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">ContactName</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="contactname" runat="server" CssClass="form-control" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">ContactEmail</label>
                                    <div class="col-md-8">
                                        <cc1:DataEntryBox ID="contactemail" runat="server" CssClass="form-control" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--  <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="tabs" ActiveTabIndex="0">
            <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Site Settings">
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>
            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Email Settings">
                <ContentTemplate>
                </ContentTemplate>
            </ajaxToolkit:TabPanel>

        </ajaxToolkit:TabContainer>--%>
        <p>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
            <div class="form-group">
                <div class="col-md-12">
                    <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>
                </div>
            </div>

        </p>
    </div>
    <div class="footer">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-12">
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>


    </div>
</div>
