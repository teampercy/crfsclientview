﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="System.Net.Mime" %>
<script runat="server">
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Tasks_TaskDef
    Protected WithEvents btnDelete As ImageButton
    Protected WithEvents btnEditItem As ImageButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ValidationSummary1.Visible = False
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Session("filter") = ""
            Me.Session("sort") = ""
            Me.example.DataBind()
            If example.Rows.Count > 0 Then
                Me.example.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
           ' Me.example.HeaderRow.TableSection = TableRowSection.TableHeader
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If
        
    End Sub
    'Protected Sub btnFindText_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindText.Click
    '    Dim myfilter As New HDS.DAL.COMMON.FilterBuilder
    '    Dim s As String = ""
    '    myfilter.Clear()
    '    myfilter.SearchAnyWords("taskname", Me.txtFindText.Text, "")
    '    s = myfilter.GetFilter
    '    Me.Session("filter") = s
    '    Me.example.DataBind()
    '    Me.example.HeaderRow.TableSection = TableRowSection.TableHeader
    '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    'End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.Session("filter") = ""
        Me.example.DataBind()
        If example.Rows.Count > 0 Then
            Me.example.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        'Me.example.HeaderRow.TableSection = TableRowSection.TableHeader
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub example_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles example.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnDelete = TryCast(e.Row.FindControl("btnDelete"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnDelete"), ImageButton).Click, AddressOf btnDelete_Click
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), ImageButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
  
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.ValidationSummary1.Visible = True
        If Page.IsValid = False Then Exit Sub
        If Me.HiddenField1.Value < 1 Then
            HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
        
            ProviderBase.DAL.Create(myitem)
        Else
            ProviderBase.DAL.Read(Me.HiddenField1.Value, myitem)
            HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
            
            ProviderBase.DAL.Update(myitem)
        End If

        Me.example.DataBind()
        Me.MultiView1.SetActiveView(Me.View1)
        If example.Rows.Count > 0 Then
            Me.example.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)

    End Sub
    Private Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.MultiView1.SetActiveView(Me.View1)
        If example.Rows.Count > 0 Then
            Me.example.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)

    End Sub
    Private Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        HiddenField1.Value = -1

        Me.MultiView1.SetActiveView(Me.View2)

    End Sub
    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDelete.Click
        btnDelete = TryCast(sender, ImageButton)
        myitem.PKID = btnDelete.Attributes("rowno")
        ProviderBase.DAL.Delete(myitem)
        Me.example.DataBind()
        If example.Rows.Count > 0 Then
            Me.example.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        Me.MultiView1.SetActiveView(Me.View1)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)

    End Sub
    Private Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, ImageButton)
        HiddenField1.Value = btnEditItem.Attributes("rowno")
        ProviderBase.DAL.Read(HiddenField1.Value, myitem)
        HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        
        Me.MultiView1.SetActiveView(Me.View2)

    End Sub
    'Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
    '    Me.example.PageSize = Integer.Parse(dropDown.SelectedValue)
    'End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= Me.example.PageCount Then
            Me.example.PageIndex = pageNumber - 1
        Else
            Me.example.PageIndex = 0
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub example_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles example.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnDelete = TryCast(e.Row.FindControl("btnDelete"), ImageButton)
            btnDelete.Attributes("rowno") = dr("PKID")
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), ImageButton)
            btnEditItem.Attributes("rowno") = dr("PKID")

        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
            lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

            Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
            txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

            Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
            ddlPageSize.SelectedValue = gridView.PageSize.ToString()
        End If
    End Sub
    Protected Sub example_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.example.DataBind()
        If example.Rows.Count > 0 Then
            Me.example.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.example.PageSize = Integer.Parse(dropDown.SelectedValue)
   
    End Sub

    Protected Sub Linkbutton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ProviderBase.SendEmailToUser("ralonso@gohds.com", "test email", "test body")
        
    End Sub
    Public Shared Sub SendEmailToUser(ByVal email As String, ByVal subject As String, ByVal body As String)

        Dim MYSITE As New TABLES.Portal
        ProviderBase.DAL.Read(1, MYSITE)
        

        Dim msg As New MailMessage
        msg.From = New MailAddress(MYSITE.contactemail, MYSITE.contactname)
        msg.Subject = subject
        msg.To.Add(email)
        msg.IsBodyHtml = False
        msg.Body = body

        msg.Subject = subject
        msg.IsBodyHtml = False
        msg.Body = body

        'Try
        With MYSITE
            Dim _smtp As New SmtpClient(.SMTPServer)
            _smtp.Port = .smtpport
            If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                _smtp.EnableSsl = .smtpenablessl
            End If
            _smtp.Send(msg)
        End With
        'Catch ex As Exception
        '    Dim s As String = ex.Message
            
        'End Try
        
        
    End Sub
</script>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetTaskDefs"
    TypeName="CRF.CLIENTVIEW.BLL.Tasks.Provider">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="maxrows" Type="Int32" />
        <asp:SessionParameter DefaultValue="" Name="filter" SessionField="taskfilter" Type="String" />
        <asp:SessionParameter Name="sort" SessionField="tasksort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<div class="form-horizontal">
    <div id="modcontainer" style="MARGIN: 10px  0px 0px 0px; width: 700px;">
        <asp:MultiView ID="MultiView1" runat="server">
            <asp:View ID="View1" runat="server">
                <h1>Schedule Tasks</h1>
                <div class="body">


                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="New Item" />
                        </div>
                        <%-- <div class="col-md-6" align="right">
                            <cc1:DataEntryBox ID="txtFindText" runat="server" Text="" CSSClass="form-control" />
                        </div>
                        <div class="col-md-2" align="right">
                            <asp:LinkButton ID="btnFindText" runat="server" Text="Search" CssClass="btn btn-primary" />
                        </div>--%>
                        <div class="col-md-2" align="left">
                            <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-primary" />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <asp:UpdatePanel ID="upItemList" runat="server">
                                <ContentTemplate>
                                    <div id="expPanel" class="panel panel-body" style="color: black !important; padding: 15px 5px;" data-loader-type="circle">
                                        <asp:GridView ID="example" runat="server" DataSourceID="ItemsDataSource"
                                            CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                            BorderWidth="1px" Width="100%">
                                            <%-- <RowStyle CssClass="rowstyle" />
                            <AlternatingRowStyle CssClass="altrowstyle" />
                            <HeaderStyle CssClass="headerstyle" />
                            <PagerStyle CssClass="pagerstyle" />
                            <PagerTemplate>
                                <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                    <asp:ListItem Value="5" />
                                    <asp:ListItem Value="10" />
                                    <asp:ListItem Value="15" />
                                    <asp:ListItem Value="20" />
                                </asp:DropDownList>
                                &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                            </PagerTemplate>--%>
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" Width="15px" />
                                                        &nbsp;&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="taskname" HeaderText="TaskName" SortExpression="taskname"></asp:BoundField>
                                                <asp:BoundField DataField="type" HeaderText="Type" SortExpression="nextrundate">
                                                    <ItemStyle Width="150px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="assemblyfile" HeaderText="Assembly" SortExpression="isvalid">
                                                    <ItemStyle Width="150px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnDelete" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" Width="15px" />
                                                        &nbsp;&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <%-- <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnFindText" EventName="Click" />
                    </Triggers>--%>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                    <%--      <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="upItemList">
                    <Animations>
                    <OnUpdating>
                        <Parallel Duration="0">
                            <EnableAction AnimationTarget="btnFindText" Enabled="false" />
                            <FadeOut MinimumOpacity=".5" />
                        </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel Duration="0">
                            <FadeIn MinimumOpacity=".5" />
                            <EnableAction AnimationTarget="btnFindText" Enabled="true" />
                        </Parallel>                    
                    </OnUpdated>
                    </Animations>
                </ajaxToolkit:UpdatePanelAnimationExtender>--%>
                </div>
                <div class="footer">
                    <br />
                </div>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <h1>Edit Schedule Taks</h1>
                <div class="body">

                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-2">
                                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Back to List"></asp:LinkButton>
                            </div>
                            <div class="col-md-2">
                                <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="btn btn-primary" OnClick="Linkbutton1_Click"
                                    Text="Test Email"></asp:LinkButton>
                            </div>
                        </div>
                        <br />
                        <div class="form-group">
                            <label class="col-md-2 control-label">TaskName</label>
                            <div class="col-md-7">
                                <cc1:DataEntryBox ID="taskname" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Type</label>
                            <div class="col-md-7">
                                <cc1:DataEntryBox ID="type" runat="server" CssClass="form-control" TextMode="SingleLine"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">AssemblyFile</label>
                            <div class="col-md-7">
                                <cc1:DataEntryBox ID="assemblyfile" runat="server" CssClass="form-control" TextMode="SingleLine"></cc1:DataEntryBox>
                            </div>
                        </div>
                    </div>

                    <br />
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
                    <br />
                    <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>
                </div>
                <div class="footer">
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />
                        </div>
                    </div>

                </div>
            </asp:View>
        </asp:MultiView>
    </div>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
