<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<script type="text/javascript">
    function MaintainMenuOpen() {
        MainMenuToggle('liSiteSetting');
        SubMenuToggle('lisubEmployees');
        SetHeaderBreadCrumb('Admin', 'Site Settings', 'Employees');
    }

    function OpenEditorModal() {
        $("#ModalEditor").modal('show');
    }

    $(function () {
      
        CallSuccessFunc();
    });


    function CallSuccessFunc() {
        // alert('call');
        var Columns = [
             { "bSortable": false},
             { "bSortable": false },
             { "bSortable": false }
          
        ]      
        MakeDataTable('#<%=gvItemList.ClientID%>', Columns, null, null);

    }

    function DeleteAccount(id) {
        document.getElementById('<%=hdnEmployeeId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteEmployeeId.ClientID%>').click();
       });
         return false;
     }

</script>
<script runat="server">
    Protected WithEvents btnDeleteItem As LinkButton
    Protected WithEvents btnEditItem As LinkButton
    Dim myitem As New TABLES.Portal_Employee
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.ValidationSummary1.Visible = False
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Session("employeefilter") = Nothing
            Me.gvItemList.DataBind()
        End If

    End Sub
    Protected Sub btnFindText_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindText.Click
        Dim myfilter As New HDS.DAL.COMMON.FilterBuilder
        Dim s As String = ""
        myfilter.Clear()
        myfilter.SearchAnyWords("firstname", Me.txtFindText.Text, "OR")
        myfilter.SearchAnyWords("lastname", Me.txtFindText.Text, "OR")
        myfilter.SearchAnyWords("description", Me.txtFindText.Text, "")
        s = myfilter.GetFilter
        Me.Session("employeefilter") = s
        Me.gvItemList.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        
    End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.Session("filter") = ""
        Me.gvItemList.DataBind()
         ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteItem"), LinkButton).Click, AddressOf btnDeleteItem_Click
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= Me.gvItemList.PageCount Then
            Me.gvItemList.PageIndex = pageNumber - 1
        Else
            Me.gvItemList.PageIndex = 0
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As TABLES.Portal_Employee = e.Row.DataItem
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteItem.Attributes("rowno") = dr.PKID
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = dr.PKID

        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
            lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

            Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
            txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

            Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
            ddlPageSize.SelectedValue = gridView.PageSize.ToString()
        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.TableSection = TableRowSection.TableHeader
            
        End If
    End Sub
    Protected Sub gvItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvItemList.DataBind()
         ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)
   
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.ValidationSummary1.Visible = True
        If Page.IsValid = False Then Exit Sub
        If Me.HiddenField1.Value < 1 Then
            HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
            myitem.description = Me.htmledit1.Value
            ProviderBase.DAL.Create(myitem)
        Else
            ProviderBase.DAL.Read(Me.HiddenField1.Value, myitem)
            HDS.WEBLIB.Common.Mapper.SetEntityValues(Me, myitem)
               myitem.description = Me.htmledit1.Value
            ProviderBase.DAL.Update(myitem)
        End If

        Me.gvItemList.DataBind()
         ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub
    Private Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
       
        Me.MultiView1.SetActiveView(Me.View1)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
 
    End Sub
    Private Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        HiddenField1.Value = -1

        Me.MultiView1.SetActiveView(Me.View2)

    End Sub
    Private Sub btnDeleteItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteItem.Click
        btnDeleteItem = TryCast(sender, LinkButton)
        Dim keyval As String = btnDeleteItem.Attributes("rowno")
        myitem.PKID =keyval
        ProviderBase.DAL.Delete(myitem)
        Me.gvItemList.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub
    Private Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        HiddenField1.Value = btnEditItem.Attributes("rowno")
        ProviderBase.DAL.Read(Me.HiddenField1.Value, myitem)
        HDS.WEBLIB.Common.Mapper.GetEntityValues(Me, myitem)
        Me.htmledit1.Value = myitem.description
        Me.MultiView1.SetActiveView(Me.View2)
     
    End Sub
   
    Protected Sub btnDeleteEmployeeId_Click(sender As Object, e As EventArgs)
        Dim keyval As String = hdnEmployeeId.Value
        myitem.PKID = keyval
        ProviderBase.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)
        Me.gvItemList.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
</script>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetEmployees"
    TypeName="Data.EmployeeProvider">
    <SelectParameters>
        <asp:Parameter DefaultValue="0" Name="maxrows" Type="Int32" />
        <asp:SessionParameter DefaultValue="" Name="afilter" SessionField="employeefilter"
            Type="String" />
        <asp:Parameter Name="asort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<div id="modcontainer" style="Width: 100%;">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1>Employees</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-4" style="text-align: left;">
                            <asp:LinkButton ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="New" />
                       
                            <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh" CssClass="btn btn-primary" />
                        </div>
                        <div class="col-md-2" style="text-align: left;">
                            <cc1:DataEntryBox ID="txtFindText" runat="server" Text="" style="display:none;"/>

                        </div>
                        <div class="col-md-2" style="text-align: left;">
                            <asp:LinkButton ID="btnFindText" runat="server" Text="Search" CssClass="button" style="display:none;"/>
                        </div>
                     
                    </div>

                    <div class="form-group">
                        <div class="col-md-12" style="text-align: left;">
                            <asp:UpdatePanel ID="udpChannelDetails" runat="server">
                                <ContentTemplate>
                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:GridView ID="gvItemList" runat="server" DataSourceID="ItemsDataSource"
                                            CssClass="table  dataTable table-striped" AutoGenerateColumns="False"
                                            BorderWidth="1px" Width="100%" OnSelectedIndexChanged="gvItemList_SelectedIndexChanged">
                                          
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnEditItem" CssClass="icon ti-write" runat="server" />
                                                       <%-- <asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                        &nbsp;&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# "~/images/" + Eval("ImageUrl") %>'
                                                            AlternateText='<%# Eval("ImageAltText")%>' CssClass="photo-float-left photo-border" Height="60px" Width="60px"/>
                                                        <h2>
                                                            <%--  <%#Eval("FirstName") + " "  + Eval("LastName")%>--%>
                                                            <%#Eval("fullname") %>
                                                        </h2>
                                                        <h3>
                                                            <i>
                                                                <%# Eval("Title")%>
                                                            </i>
                                                        </h3>
                                                        <p>
                                                            <%# Eval("Description")%>
                                                        </p>
                                                        <ul>
                                                            <li>Phone:
                                    <%#Eval("Phone")%>
                                                            </li>
                                                            <li>Fax:
                                    <%#Eval("Fax")%>
                                                            </li>
                                                            <li>Email:
                                    <%# Eval("Email")%>
                                                            </li>
                                                        </ul>
                                                        <p />
                                                        <hr />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                         <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                        <%--<asp:ImageButton ID="btnDeleteItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" />--%>
                                                        &nbsp;&nbsp;&nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnFindText" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="udpChannelDetails">
                                <Animations>
                    <OnUpdating>
                        <Parallel Duration="0">
                            <EnableAction AnimationTarget="btnFindText" Enabled="false" />
                            <FadeOut MinimumOpacity=".5" />
                        </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel Duration="0">
                            <FadeIn MinimumOpacity=".5" />
                            <EnableAction AnimationTarget="btnFindText" Enabled="true" />
                        </Parallel>                    
                    </OnUpdated>
                                </Animations>
                            </ajaxToolkit:UpdatePanelAnimationExtender>
                        </div>
                    </div>

                </div>
            </div>
            <%-- <div class="footer">
                <br />
            </div>--%>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <h1>Edit Employee</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12" style="text-align: left;">
                            <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Back to List"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">FirstName:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="firstname" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">LastName:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="lastname" runat="server"  CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Title:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="title" runat="server"  CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Phone:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="phone" runat="server"  CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Fax:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="Fax" runat="server" CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Email:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="email" runat="server"  CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Image:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="imageurl" runat="server"  CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Image Text:</label>
                        <div class="col-md-4">
                            <cc1:DataEntryBox ID="imagealttext" runat="server"  CssClass="form-control"></cc1:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Description:</label>
                        <div class="col-md-2" align="left">
                            <SiteControls:HTMLEditor ID="htmledit1" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12" align="left">
                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
                            <br />
                            <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>




            </div>
            <div class="footer">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" />&nbsp;
         <br />
            </div>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
  <asp:HiddenField ID="hdnEmployeeId" runat="server" Value="0" />
   <asp:Button ID="btnDeleteEmployeeId" runat="server" Style="display: none;" OnClick="btnDeleteEmployeeId_Click" />