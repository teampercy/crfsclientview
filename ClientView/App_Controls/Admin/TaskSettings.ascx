<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<script runat="server">
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Tasks_Settings
     Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ProviderBase.DAL.Read(1, myitem)

        If Page.IsPostBack = False Then
            HDS.WEBLIB.Common.Mapper.GetEntityValues(Me.TabPanel1, myitem)
            HDS.WEBLIB.Common.Mapper.GetEntityValues(Me.TabPanel2, myitem)
        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = False Then Exit Sub

        ProviderBase.DAL.Read(1, myitem)
        HDS.WEBLIB.Common.Mapper.SetEntityValues(Me.TabPanel1, myitem)
       
        HDS.WEBLIB.Common.Mapper.SetEntityValues(Me.TabPanel2, myitem)
        ProviderBase.DAL.Update(myitem)
        Session("Settings") = Nothing

    End Sub
</script>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server">
</asp:ScriptManagerProxy>
<div id="modcontainer" style="WIDTH: 700px">
   <h1>Settings</h1>
   <div class="body">
    <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="tabs" ActiveTabIndex="0">
     <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Site Settings">
    <ContentTemplate>
    <table>
			<tr>
				<td class="row-a" style="width: 100px">
                    Name</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="sitename" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
			<tr>
				<td class="row-a">
                    HomePageURL</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="homepageurl" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
			 <tr>
				<td class="row-a">
                    SettingsFolder</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="settingsfolder" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
			<tr>
				<td class="row-a">
                    ErrorFolder</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="errorfolder" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
			<tr>
				<td class="row-a">
                    OutputFolder</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="outputfolder" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
			
		</table>
    </ContentTemplate>
    </ajaxToolkit:TabPanel>
     <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Email Settings">
      <ContentTemplate>
        <table>
        <tr>
				<td class="row-a" valign="top" style="width: 100px">
                    SMTPServer</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="smtpserver" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
		      <tr>
				<td class="row-a" valign="top">
                    SMTPPort</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="smtpport" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
		    <tr>
				<td class="row-a" valign="top">
                    Enable SSL</td>
				<td class="row-b" >
                    <asp:CheckBox ID="smtpenablessl" runat="server" /></td>
			</tr>
		
			<tr>
				<td class="row-a" valign="top">
                    SMTPLogin</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="smtplogin" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
			<tr>
				<td class="row-a" valign="top">
                    SMTPPassword</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="smtppassword" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
		
				</td>
			</tr>
			 <tr>
				<td class="row-a" valign="top">
                    AdminName</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="adminname" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
		    <tr>
				<td class="row-a" valign="top">
                    AdminEmail</td>
				<td class="row-b" >
					<cc1:DataEntryBox id="adminemail" runat="server" CssClass="textbox" Width="400px" Height="18px" DataType="Any" EnableClientSideScript="True" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
				</td>
			</tr>
        </table>
        
      </ContentTemplate>
     </ajaxToolkit:TabPanel>
     </ajaxToolkit:TabContainer>
       <p>
     <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
    <br/>
    </p>
		 </div>
     <div class="footer">
    	<asp:LinkButton ID="btnSave" runat="server" CssClass="button" Text="Submit" OnClick="btnSave_Click" />  <BR/>
     </div>
</div>