<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style2
    {
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold
    }
    .style3
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }
    .style5
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: small;
        font-weight: bold;
        font-style: italic;
    }
</style>
        

        <tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" width="50%" align="center">
							<tr>
								<td align="center" class="style3">
									State Lien Statutes (Private Jobs)</td>

							</tr>
							
						</table>
					</td>
        </tr>
        <br />
<tr>
			<td align="center" valign="top">
				<table align="center" cellpadding="5" cellspacing="0" border="1">
				<tr>
				    <td align="center"><img src="Images/ResourceImages/Seals/AL_Seal.png" width="60" height="60" border="0">
					</td>
				    <td align="center" width="32%" class="style2" bgcolor="#edf0ff"><a href="Images/ResourceImages/Statute/AL.pdf" target="_blank" title="Alabama Lien Statute">Alabama</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/MT_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/MT.pdf" target="_blank" title="Montana Lien Statute">Montana</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/AK_Seal.jpg" width="60" height="60" border="0">
					</td>    
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/AK.pdf" target="_blank" title="Alaska Lien Statute">Alaska</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/NE_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/NE.pdf" target="_blank" title="Nebraska Lien Statute">Nebraska</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/AZ_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/AZ.pdf" target="_blank" title="Arizona Lien Statute">Arizona</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/NV_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/NV.pdf" target="_blank" title="Nevada Lien Statute">Nevada</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/AR_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/AR.pdf" target="_blank" title="Arkansas Lien Statute">Arkansas</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/NH_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/NH.pdf" target="_blank" title="New Jersey Lien Statute">New Hampshire</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/CA_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/CA.pdf" target="_blank" title="California Lien Statute">California</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/NJ_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/NJ.pdf" target="_blank" title="New Jersey Lien Statute">New Jersey</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/CO_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/CO.pdf" target="_blank" title="Colorado Lien Statute">Colorado</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/NM_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/NM.pdf" target="_blank" title="New Mexico Lien Statute">New Mexico</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/CT_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/CT.pdf" target="_blank" title="Connecticut Lien Statute">Connecticut</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/NY_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/NY.pdf" target="_blank" title="New York Lien Statute">New York</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/DE_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/DE.pdf" target="_blank" title="Delaware Lien Statute">Delaware</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/NC_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/NC.pdf" target="_blank" title="North Carolina Lien Statute">North Carolina</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/DC_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/DC.pdf" target="_blank" title="District of Columbia Lien Statute">District of Columbia</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/ND_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/ND.pdf" target="_blank" title="North Dakota Lien Statute">North Dakota</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/FL_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/FL.pdf" target="_blank" title="Florida Lien Statute">Florida</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/OH_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/OH.pdf" target="_blank" title="Ohio Lien Statute">Ohio</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/GA_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/GA.pdf" target="_blank" title="Georgia Lien Statute">Georgia</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/OK_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/OK.pdf" target="_blank" title="Oklahoma Lien Statute">Oklahoma</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/HI_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/HI.pdf" target="_blank" title="Hawaii Lien Statute">Hawaii</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/OR_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/OR.pdf" target="_blank" title="Oregon Lien Statute">Oregon</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/ID_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/ID.pdf" target="_blank" title="Idaho Lien Statute">Idaho</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/PA_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/PA.pdf" target="_blank" title="Pennsylvania Lien Statute">Pennsylvania</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/IL_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/IL.pdf" target="_blank" title="Illinois Lien Statute">Illinois</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/RI_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/RI.pdf" target="_blank" title="Rhode Island Lien Statute">Rhode Island</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/IN_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/IN.pdf" target="_blank" title="Iowa Lien Statute">Indiana</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/SC_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/SC.pdf" target="_blank" title="South Dakota Lien Statute">South Carolina</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/IA_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/IA.pdf" target="_blank" title="Iowa Lien Statute">Iowa</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/SD_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/SD.pdf" target="_blank" title="South Dakota Lien Statute">South Dakota</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/KS_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/KS.pdf" target="_blank" title="Kansas Lien Statute">Kansas</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/TN_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="" target="_blank" title="Tennessee Lien Statute">Tennessee</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/KY_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/KY.pdf" target="_blank" title="Kentucky Lien Statute">Kentucky</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/TX_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/TX.pdf" target="_blank" title="Texas Lien Statute">Texas</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/LA_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/LA.pdf" target="_blank" title="Louisiana Lien Statute">Louisiana</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/UT_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/UT.pdf" target="_blank" title="Utah Lien Statute">Utah</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/ME_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/ME.pdf" target="_blank" title="Maine Lien Statute">Maine</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/VT_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/VT.pdf" target="_blank" title="Vermont Lien Statute">Vermont</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/MD_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/MD.pdf" target="_blank" title="Maryland Lien Statute">Maryland</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/VA_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/VA.pdf" target="_blank" title="Virginia Lien Statute">Virginia</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/MA_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/MA.pdf" target="_blank" title="Massachusetts Lien Statute">Massachusetts</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/WA_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/WA.pdf" target="_blank" title="Washington Lien Statute">Washington</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/MI_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/MI.pdf" target="_blank" title="Michigan Lien Statute">Michigan</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/WV_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/WV.pdf" target="_blank" title="West Virginia Lien Statute">West Virginia</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/MN_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/MN.pdf" target="_blank" title="Minnesota Lien Statute">Minnesota</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/WI_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/WI.pdf" target="_blank" title="Wisconsin Lien Statute">Wisconsin</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/MS_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/MS.pdf" target="_blank" title="Mississippi Lien Statute">Mississippi</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Seals/WY_Seal.jpg" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/WY.pdf" target="_blank" title="Wyoming Lien Statute">Wyoming</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Seals/MO_Seal.gif" width="60" height="60" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Statute/MO.pdf" title="Missouri Lien Statute">Missouri</a>
			        </td>
			        
			    </tr>
			    
			        
			    </table>
			</td>
</tr>
<tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" width="60%" align="center">							
							<tr>
								<td align="left" bgcolor="#f1fbff" class="style5">
									Information and documents provided in ClientView™, to our clients free of charge, 
									are provided without any warranty, express or implied, as to their legal effect 
									and completeness. CRF Solutions accepts no liability in connection with reliance 
									thereon or any loss sustained by anyone using or relying on the information contained 
									herein. The information and documents offered by CRF Solutions are not a substitute 
									for the advice of an attorney. CRF Solutions strongly recommends you seek assistance 
									from an experienced construction attorney for any legal questions you might have.
								</td>

							</tr>
						</table>
					</td>
</tr>				    
<br />				    
