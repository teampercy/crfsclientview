<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style2
    {
        font-family: Arial, Helvetica, sans-serif;
    }
    .style3
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }
    .style4
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: x-small;
    }
    .style5
    {
        font-size: x-small;
    }
</style>
        <tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" width="75%" align="center" class="style4">
							<tr>
								<td align="center" valign="top"><img src="Images/ResourceImages/Flags/StateFlags.jpg" width="500" height="150" alt="State-Specific Statutes"></td>

							</tr>
						</table>
					</td>
        </tr>
        <br />

        <tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" width="75%" align="center">
						
							<tr>
								<td align="center" class="style3">
									State Lien Statutes (Private Jobs
								</td>

							</tr>
							
						</table>
					</td>
        </tr>
        <br />
<tr>
			<td valign="top">
				<table align="center" cellpadding="5" cellspacing="0" border="1">
				<tr>
				    <td align="center" width=30% class="style2"><a href="http://alisondb.legislature.state.al.us/acas/CodeOfAlabama/1975/136141.htm" target="_blank" title="Alabama Statute">Alabama Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Montana Statute">Montana Statute</a>
			        </td>
			    </tr>
			    <tr>
				    <td align="center" width=30% class="style2"><a href="http://www.legis.state.ak.us/cgi-bin/folioisa.dll/stattx05/query=[jump!3A!27as3435050!27]/doc/{@13860}?" target="_blank" title="Alaska Statute">Alaska Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Nebraska Statute">Nebraska Statute</a>
			        </td>
			    </tr>			    
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://www.azleg.state.az.us/FormatDocument.asp?inDoc=/ars/33/00981.htm&Title=33&DocType=ARS" target="_blank" title="Arizona Statute">Arizona Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Nevada Statute">Nevada Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://web.lexisnexis.com/research/retrieve?_m=9355d829eb1716be976f5221e0ed84a2&_fmtstr=TOC&_startdoc=&wchp=dGLzVlb-zSkAB&_md5=008b4615f2baba17940b0a460cf40862#TAATAAEAAFAAC" title="Arkansas Statute">Arkansas Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New Jersey Statute">New Hampshire Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="California Statute">California Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New Jersey Statute">New Jersey Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Colorado Statute">Colorado Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New Mexico Statute">New Mexico Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Connecticut Statute">Connecticut Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New York Statute">New York Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Delaware Statute">Delaware Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="North Carolina Statute">North Carolina Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="District of Columbia Statute">District of Columbia Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="North Dakota Statute">North Dakota Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Florida Statute">Florida Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Ohio Statute">Ohio Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Georgia Statute">Georgia Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Oklahoma Statute">Oklahoma Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Hawaii Statute">Hawaii Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Oregon Statute">Oregon Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Idaho Statute">Idaho Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Pennsylvania Statute">Pennsylvania Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Illinois Statute">Illinois Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Rhode Island Statute">Rhode Island Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Iowa Statute">Indiana Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="South Dakota Statute">South Carolina Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Iowa Statute">Iowa Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="South Dakota Statute">South Dakota Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Kansas Statute">Kansas Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Tennessee Statute">Tennessee Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Kentucky Statute">Kentucky Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Texas Statute">Texas Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Louisianna Statute">Louisianna Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Utah Statute">Utah Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Maine Statute">Maine Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Vermont Statute">Vermont Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Maryland Statute">Maryland Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Virginia Statute">Virginia Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Massachusetts Statute">Massachusetts Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Washington Statute">Washington Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Michigan Statute">Michigan Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="West Virginia Statute">West Virginia Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Minnesota Statute">Minnesota Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Wisconsin Statute">Wisconsin Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Mississippi Statute">Mississippi Statute</a>
			        </td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Wyoming Statute">Wyoming Statute</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Missouri Statute">Missouri Statute</a>
			        </td>
			        
			    </tr>
			    
			        
			    </table>
			</td>
</tr>
				    
