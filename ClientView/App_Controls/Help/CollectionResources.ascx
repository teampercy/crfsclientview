<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style3 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }

    .style5 {
        font-family: Arial, Helvetica, sans-serif;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('CollectView', 'Resources', 'Collection Resources');
        MainMenuToggle('liCollectViewResources');
        SubMenuToggle('liCollectionRes');
        //$('#liCollectViewResources').addClass('site-menu-item has-sub active open');
        //$('#liCollectionRes').addClass('site-menu-item active');
        return false;

    }
  
</script>

                                 
                                  <div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-12 style3" style="text-align: center;">
            Collection Resources
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12" style="text-align: left; background-color: #f1fbff;">
            Information and documents provided in ClientView™, to our clients free of charge, are provided without any warranty, express or implied, as to their legal effect and completeness. CRF Solutions accepts no liability in connection with reliance thereon or any loss sustained by anyone using or relying on the information contained herein. The information and documents offered by CRF Solutions are not a substitute for the advice of an attorney. CRF Solutions strongly recommends you seek assistance from an experienced construction attorney for any legal questions you might have.
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style3" style="text-align: left;">
            General Information
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.uscourts.gov/allinks.html" target="_blank" title="BK Court Addresses">Bankruptcy Court Addresses</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/BankruptcySummary.pdf" target="_blank" title="Bankruptcy Summary">Bankruptcy Summary</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.ftc.gov/os/statutes/fcra.htm" target="_blank" title="FCRA">Federal Credit Reporting Act</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.ftc.gov/bcp/edu/pubs/consumer/credit/cre27.pdf" target="_blank" title="FDCPA">Federal Debt Collection Practices Act</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/LegalTerms.pdf" target="_blank" title="Legal Terminology">Legal Terminology</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/LitigationMadeEasy.pdf" target="_blank" title="Litigation Made Easy">Litigation Made Easy</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/StateLaws.pdf" target="_blank" title="State Collection Summary">State Collection Law Summary</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.law.cornell.edu/ucc/ucc.table.html" target="_blank" title="UCC">Uniform Commercial Code Articles 1 - 9</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style3" style="text-align: left;">
            Sample Forms
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/CreditApplication.pdf" target="_blank" title="Credit Application">Credit Application</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/MechanicLienRequest.doc" target="_blank" title="Mech Lien Request(WORD)">Mechanics Lien Request Form (WORD)</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/MechanicLienRequest.pdf" target="_blank" title="Mech Lien Request(PDF)">Mechanics Lien Request Form (PDF)</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/PersonalGuaranty.pdf" target="_blank" title="Personal Guaranty">Personal Guaranty</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="Images/ResourceImages/Collections/PromissoryNote.pdf" target="_blank" title="Promisory Note">Prommisory Note</a><br />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style3" style="text-align: left;">
            Industry Association Links
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.ccaacollect.com/" target="_blank" title="Commercial Collection Agency Association">Commercial Collection Agency Association (CCAA)</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.commercialcollector.com/iacc/default" target="_blank" title="International Association of Commercial Collectors">International Association of Commercial Collectors (IACC)</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.cccacollect.com/" target="_blank" title="California Commercial Collection Association">California Commercial Collection Association (CCCA)</a>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 style5" style="text-align: left;">
            <a href="http://www.calcollectors.net/" target="_blank" title="California Association of Collectors">California Association of Collectors(CAC)</a>
        </div>
    </div>

</div>
                            


