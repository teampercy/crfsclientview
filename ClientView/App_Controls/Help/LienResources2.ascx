<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style2 {
        font-family: Arial, Helvetica, sans-serif;
    }

    .style3 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }

    .style7 {
        font-weight: bold;
        font-style: italic;
        font-family: Arial, Helvetica, sans-serif;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        var PageName = window.location.search.substring(1);
        if (PageName == "help.lienresources2.JobView") {
            SetHeaderBreadCrumb('RentalView', 'Resources', 'State Statutes (Public)');
            MainMenuToggle('liRentalViewResources');
            SubMenuToggle('liLienPublicJobView');
        }
        else {
            SetHeaderBreadCrumb('LienView', 'Resources', 'State Statutes (Public)');
            MainMenuToggle('liLienViewResources');
            SubMenuToggle('liLienPublic');
        }
       
        //$('#liLienViewResources').addClass('site-menu-item has-sub active open');
        //$('#liLienPublic').addClass('site-menu-item active');
        return false;

    }
    
</script>


                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: center;">
                                                State Lien Statutes (Public) 
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12 style7" style="text-align: left;">
                                                To access the statutes, just click on any of the States on the map below,or you can 
									also use the state links listed below the map.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: center;">
                                                        <!-- start Fla-shop.com HTML5 Map -->
                                                <div style="text-align: center"><div id='map-container' style="margin-left: 447px"></div></div>
                                                <link href="/US_Map/map.css" rel="stylesheet">
                                                <script src="/US_Map/raphael.min.js"></script>
                                                <script src="/US_Map/settingsLienStatutes(Public).js"></script>
                                                <script src="/US_Map/paths.js"></script>
                                                <script src="/US_Map/map.js"></script>
                                                <script>
                                                  var map = new FlaMap(map_cfg);
                                                  map.drawOnDomReady('map-container');
                                                </script>
                                                <!-- end HTML5 Map -->
                                              <%--  <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
                                                    codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
                                                    width="530" height="410" id="usa_locator" align="center">
                                                    <param name="movie" value="usa_locator.swf">
                                                    <param name="FlashVars" value="xmlfile1=usa_locator_stat1.xml">
                                                    <param name="quality" value="high">
                                                    <param name="wmode" value="transparent">
                                                    <param name="bgcolor" value="#F5F5F5">
                                                    <embed src="usa_locator.swf?xmlfile1=usa_locator_stat1.xml" quality="high" wmode="transparent" bgcolor="#F5F5F5" width="530" height="410" name="usa_locator" align=""
                                                        type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                                                </object>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: center;">
                                                <table align="center" cellpadding="5" cellspacing="0" border="1" style="margin-left: 415px;">
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AL1.pdf" target="_blank" title="Alabama Public Statute">Alabama Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MT1.pdf" title="Montana Public Statute">Montana Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AK1.pdf" target="_blank" title="Alaska Public Statute">Alaska Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NE1.pdf" target="_blank" title="Nebraska Public Statute">Nebraska Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AZ1.pdf" target="_blank" target="_blank" title="Arizona Public Statute">Arizona Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NV1.pdf" target="_blank" title="Nevada Public Statute">Nevada Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AR1.pdf" target="_blank" title="Arkansas Public Statute">Arkansas Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NH1.pdf" target="_blank" title="New Jersey Public Statute">New Hampshire Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/CA1.pdf" target="_blank" title="California Public Statute">California Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NJ1.pdf" target="_blank" title="New Jersey Public Statute">New Jersey Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/CO1.pdf" target="_blank" title="Colorado Public Statute">Colorado Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NM1.pdf" target="_blank" title="New Mexico Public Statute">New Mexico Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/CT1.pdf" target="_blank" title="Connecticut Public Statute">Connecticut Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NY1.pdf" target="_blank" title="New York Public Statute">New York Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/DE1.pdf" target="_blank" title="Delaware Public Statute">Delaware Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NC1.pdf" target="_blank" title="North Carolina Public Statute">North Carolina Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/DC1.pdf" target="_blank" title="District of Columbia Public Statute">District of Columbia Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/ND1.pdf" target="_blank" title="North Dakota Public Statute">North Dakota Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/FL1.pdf" target="_blank" title="Florida Public Statute">Florida Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/OH1.pdf" target="_blank" title="Ohio Public Statute">Ohio Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/GA1.pdf" target="_blank" title="Georgia Public Statute">Georgia Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/OK1.pdf" target="_blank" title="Oklahoma Public Statute">Oklahoma Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/HI1.pdf" target="_blank" title="Hawaii Public Statute">Hawaii Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/OR1.pdf" target="_blank" title="Oregon Public Statute">Oregon Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/ID1.pdf" target="_blank" title="Idaho Public Statute">Idaho Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/PA1.pdf" target="_blank" title="Pennsylvania Public Statute">Pennsylvania Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/IL1.pdf" target="_blank" title="Illinois Public Statute">Illinois Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/RI1.pdf" target="_blank" title="Rhode Island Public Statute">Rhode Island Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/IN1.pdf" target="_blank" title="Iowa Public Statute">Indiana Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/SC1.pdf" target="_blank" title="South Dakota Public Statute">South Carolina Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/IA1.pdf" target="_blank" title="Iowa Public Statute">Iowa Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/SD1.pdf" target="_blank" title="South Dakota Public Statute">South Dakota Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/KS1.pdf" target="_blank" title="Kansas Public Statute">Kansas Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/TN1.pdf" target="_blank" title="Tennessee Public Statute">Tennessee Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/KY1.pdf" target="_blank" title="Kentucky Public Statute">Kentucky Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/TX1.pdf" target="_blank" title="Texas Public Statute">Texas Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/LA1.pdf" target="_blank" title="Louisiana  Public Statute">Louisiana  Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/UT1.pdf" target="_blank" title="Utah Public Statute">Utah Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/ME1.pdf" target="_blank" title="Maine Public Statute">Maine Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/VT1.pdf" target="_blank" title="Vermont Public Statute">Vermont Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MD1.pdf" target="_blank" title="Maryland Public Statute">Maryland Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/VA1.pdf" target="_blank" title="Virginia Public Statute">Virginia Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MA1.pdf" target="_blank" title="Massachusetts Public Statute">Massachusetts Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WA1.pdf" target="_blank" title="Washington Public Statute">Washington Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MI1.pdf" target="_blank" title="Michigan Public Statute">Michigan Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WV1.pdf" target="_blank" title="West Virginia Public Statute">West Virginia Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MN1.pdf" target="_blank" title="Minnesota Public Statute">Minnesota Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WI1.pdf" target="_blank" title="Wisconsin Public Statute">Wisconsin Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MS1.pdf" target="_blank" title="Mississippi Public Statute">Mississippi Public Statute</a>
                                                        </td>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WY1.pdf" target="_blank" title="Wyoming Public Statute">Wyoming Public Statute</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MO1.pdf" target="_blank" title="Missouri Public Statute">Missouri Public Statute</a>
                                                        </td>

                                                    </tr>


                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style7" style="text-align: left; background-color: #f1fbff;">
                                                Information and documents provided in ClientView™, to our clients free of charge, 
									are provided without any warranty, express or implied, as to their legal effect 
									and completeness. CRF Solutions accepts no liability in connection with reliance 
									thereon or any loss sustained by anyone using or relying on the information contained 
									herein. The information and documents offered by CRF Solutions are not a substitute 
									for the advice of an attorney. CRF Solutions strongly recommends you seek assistance 
									from an experienced construction attorney for any legal questions you might have.
                                            </div>
                                        </div>



                                    </div>
                              

