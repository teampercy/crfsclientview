<head>
    <style type="text/css">
        .style1 {
            font-family: Arial, Helvetica, sans-serif;
        }

        .style2 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-small;
            color: #000000;
        }

        .style3 {
            font-size: x-small;
        }

        .style4 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: medium;
            color: #000099;
        }
    </style>
</head>
<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Help', 'Filter Job');
        MainMenuToggle('liLienTrainingVideos');
        SubMenuToggle('liLienFindjobs');
        //$('#liLienTrainingVideos').addClass('site-menu-item has-sub active open');
        //$('#liLienFindjobs').addClass('site-menu-item active');
        return false;

    }

</script>
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-12" style="text-align: left;">
            <h2 class="style4">Finding A Job In LienView</h2>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12" style="text-align: left;">
            <div style="width: auto; height: auto; overflow: auto;">
                <object id="scPlayer" class="embeddedObject" width="800" height="525" type="application/x-shockwave-flash" data="http://content.screencast.com/users/CRFSolutions/folders/JobFilter/media/ca66618d-6a20-4faa-8a60-60e66ca52825/flvplayer.swf">
                    <param name="movie" value="http://content.screencast.com/users/CRFSolutions/folders/JobFilter/media/ca66618d-6a20-4faa-8a60-60e66ca52825/flvplayer.swf" />
                    <param name="quality" value="high" />
                    <param name="bgcolor" value="#FFFFFF" />
                    <param name="flashVars" value="thumb=http://content.screencast.com/users/CRFSolutions/folders/JobFilter/media/ca66618d-6a20-4faa-8a60-60e66ca52825/FirstFrame.jpg&containerwidth=800&containerheight=618&content=http://content.screencast.com/users/CRFSolutions/folders/JobFilter/media/ca66618d-6a20-4faa-8a60-60e66ca52825/JobFilterFLVAudio.flv&blurover=false" />
                    <param name="allowFullScreen" value="true" />
                    <param name="scale" value="showall" />
                    <param name="allowScriptAccess" value="always" />
                    <param name="base" value="http://content.screencast.com/users/CRFSolutions/folders/JobFilter/media/ca66618d-6a20-4faa-8a60-60e66ca52825/" />
                    <video width="800" height="618" controls="controls">
                        <br />
                        <source src="http://content.screencast.com/users/CRFSolutions/folders/JobFilter/media/ca66618d-6a20-4faa-8a60-60e66ca52825/JobFilterFLVAudio.flv" type="video/mp4;" /><br />
                        <b>Your browser cannot play this video. <a href="http://www.screencast.com/handlers/redirect.ashx?target=viewingembededhelp">Learn how to fix this</a>.</b></video>
                </object>
            </div>
        </div>
    </div>
</div>

