<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

  
<style type="text/css">
    .style2 {
        font-family: Arial, Helvetica, sans-serif;
    }

    .style3 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }

    .style7 {
        font-weight: bold;
        font-style: italic;
        font-family: Arial, Helvetica, sans-serif;
    }
</style>

<script src="../../global/js/raphael.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.js"></script>
<script src="../../global/js/color.jquery.js"></script>	
<script src="../../global/js/us-map.js"></script>

	
<script type="text/javascript">

    function MaintainMenuOpen() {
        debugger;
        var PageName = window.location.search.substring(1);
        if (PageName == "help.lienresources1.JobView") {
            SetHeaderBreadCrumb('RentalView', 'Resources', 'State Statutes (Private)');
            MainMenuToggle('liRentalViewResources');
            SubMenuToggle('liLienStateJobView');
        }
        else {
            SetHeaderBreadCrumb('LienView', 'Resources', 'State Statutes (Private)');
            MainMenuToggle('liLienViewResources');
            SubMenuToggle('liLienState');
        }
        

        //$('#liLienViewResources').addClass('site-menu-item has-sub active open');
        //$('#liLienState').addClass('site-menu-item active');
        return false;

    }
    $(document).ready(function () {
        
    })
</script>


<%--<!-- start Fla-shop.com HTML5 Map -->
<div id='map-container'></div>
<link href="/US_Map/map.css" rel="stylesheet">
<script src="/US_Map/raphael.min.js"></script>
<script src="/US_Map/settings.js"></script>
<script src="/US_Map/paths.js"></script>
<script src="/US_Map/map.js"></script>
<script>
  var map = new FlaMap(map_cfg);
  map.drawOnDomReady('map-container');
</script>
<!-- end HTML5 Map -->--%>
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: center;">
                                                State Lien Statutes (Private) 
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <div class="col-md-12 style7" style="text-align: left;">
                                                To access the statutes, just click on any of the States on the map below,or you can 
									also use the state links listed below the map.
                                            </div>
                                        </div>
                                    <div class="form-group">
                                            <div class="col-md-12" style="text-align: center;">
                                                <!-- start Fla-shop.com HTML5 Map -->
                                                <div style="text-align: center"><div id='map-container' style="margin-left: 447px"></div></div>
                                                <link href="/US_Map/map.css" rel="stylesheet">
                                                <script src="/US_Map/raphael.min.js"></script>
                                                <script src="/US_Map/settingsLienStatutes(Private).js"></script>
                                                <script src="/US_Map/paths.js"></script>
                                                <script src="/US_Map/map.js"></script>
                                                <script>
                                                  var map = new FlaMap(map_cfg);
                                                  map.drawOnDomReady('map-container');
                                                </script>
                                               <%-- <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
                                                    codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
                                                    width="530" height="410" id="usa_locator" align="center">
                                                    <param name="movie" value="usa_locator.swf">
                                                    <param name="FlashVars" value="xmlfile1=usa_locator_sum.xml">
                                                    <param name="quality" value="high">
                                                    <param name="wmode" value="direct">
                                                    <param name="bgcolor" value="#F5F5F5">
                                                    <embed src="usa_locator.swf?xmlfile1=usa_locator_sum.xml" quality="high" wmode="transparent" bgcolor="#F5F5F5" width="530" height="410" name="usa_locator" align=""
                                                        type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                                                </object>--%>
                                            </div>
                                        </div>
                                      <%-- <div id="map" style="width: 930px; height: 630px; border: solid 3px red;">--%>

                                       </div>
                                     <div id="clicked-state"></div>

                                    <div class="form-group">
                                       <div class="col-md-12" style="text-align: center;">
                                       <table align="center" cellpadding="5" cellspacing="0" border="1" style="margin-left: 470px;">
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AL.pdf" target="_blank" title="Alabama Statute">Alabama Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MT.pdf" title="Montana Statute">Montana Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AK.pdf" target="_blank" title="Alaska Statute">Alaska Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NE.pdf" target="_blank" title="Nebraska Statute">Nebraska Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AZ.pdf" target="_blank" target="_blank" title="Arizona Statute">Arizona Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NV.pdf" target="_blank" title="Nevada Statute">Nevada Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/AR.pdf" target="_blank" title="Arkansas Statute">Arkansas Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NH.pdf" target="_blank" title="New Jersey Statute">New Hampshire Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/CA.pdf" target="_blank" title="California Statute">California Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NJ.pdf" target="_blank" title="New Jersey Statute">New Jersey Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/CO.pdf" target="_blank" title="Colorado Statute">Colorado Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NM.pdf" target="_blank" title="New Mexico Statute">New Mexico Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/CT.pdf" target="_blank" title="Connecticut Statute">Connecticut Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NY.pdf" target="_blank" title="New York Statute">New York Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/DE.pdf" target="_blank" title="Delaware Statute">Delaware Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/NC.pdf" target="_blank" title="North Carolina Statute">North Carolina Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/DC.pdf" target="_blank" title="District of Columbia Statute">District of Columbia Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/ND.pdf" target="_blank" title="North Dakota Statute">North Dakota Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/FL.pdf" target="_blank" title="Florida Statute">Florida Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/OH.pdf" target="_blank" title="Ohio Statute">Ohio Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/GA.pdf" target="_blank" title="Georgia Statute">Georgia Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/OK.pdf" target="_blank" title="Oklahoma Statute">Oklahoma Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/HI.pdf" target="_blank" title="Hawaii Statute">Hawaii Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/OR.pdf" target="_blank" title="Oregon Statute">Oregon Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/ID.pdf" target="_blank" title="Idaho Statute">Idaho Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/PA.pdf" target="_blank" title="Pennsylvania Statute">Pennsylvania Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/IL.pdf" target="_blank" title="Illinois Statute">Illinois Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/RI.pdf" target="_blank" title="Rhode Island Statute">Rhode Island Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/IN.pdf" target="_blank" title="Iowa Statute">Indiana Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/SC.pdf" target="_blank" title="South Dakota Statute">South Carolina Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/IA.pdf" target="_blank" title="Iowa Statute">Iowa Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/SD.pdf" target="_blank" title="South Dakota Statute">South Dakota Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/KS.pdf" target="_blank" title="Kansas Statute">Kansas Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/TN.pdf" target="_blank" title="Tennessee Statute">Tennessee Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/KY.pdf" target="_blank" title="Kentucky Statute">Kentucky Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/TX.pdf" target="_blank" title="Texas Statute">Texas Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/LA.pdf" target="_blank" title="Louisiana  Statute">Louisiana  Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/UT.pdf" target="_blank" title="Utah Statute">Utah Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/ME.pdf" target="_blank" title="Maine Statute">Maine Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/VT.pdf" target="_blank" title="Vermont Statute">Vermont Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MD.pdf" target="_blank" title="Maryland Statute">Maryland Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/VA.pdf" target="_blank" title="Virginia Statute">Virginia Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MA.pdf" target="_blank" title="Massachusetts Statute">Massachusetts Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WA.pdf" target="_blank" title="Washington Statute">Washington Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MI.pdf" target="_blank" title="Michigan Statute">Michigan Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WV.pdf" target="_blank" title="West Virginia Statute">West Virginia Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MN.pdf" target="_blank" title="Minnesota Statute">Minnesota Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WI.pdf" target="_blank" title="Wisconsin Statute">Wisconsin Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MS.pdf" target="_blank" title="Mississippi Statute">Mississippi Statute</a>
                </td>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/WY.pdf" target="_blank" title="Wyoming Statute">Wyoming Statute</a>
                </td>
            </tr>
            <tr>
                <td align="center" width="30%" class="style2"><a href="Images/ResourceImages/Statute/MO.pdf" target="_blank" title="Missouri Statute">Missouri Statute</a>
                </td>

            </tr>


        </table>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style7" style="text-align: left; background-color: #f1fbff;">
                                             Information and documents provided in ClientView™, to our clients free of charge, 
									are provided without any warranty, express or implied, as to their legal effect 
									and completeness. CRF Solutions accepts no liability in connection with reliance 
									thereon or any loss sustained by anyone using or relying on the information contained 
									herein. The information and documents offered by CRF Solutions are not a substitute 
									for the advice of an attorney. CRF Solutions strongly recommends you seek assistance 
									from an experienced construction attorney for any legal questions you might have.
                                            </div>
                                        </div>



                                    </div>
                              
