<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style2
    {
        font-family: Arial, Helvetica, sans-serif;
    }
    .style3
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }
    .style4
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: x-small;
    }
</style>
        <tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" 
    width="75%" align="center" class="style4">
							<tr>
								<td align="center" valign="top"><img src="ResourceImages/StateFlags10x5.jpg" width="500" height="150" alt="State-Specific Lien Laws"></td>

							</tr>
						</table>
					</td>
        </tr>
        <br />

        <tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" width="75%" align="center">
							<tr>
								<td align="center" class="style3">
									State Lien Laws 
								</td>

							</tr>
							<tr>
								<td align="left" bgcolor="#edfoff" class="style4">
									Information and documents provided in ClientView™, to our clients free of charge, are provided without any warranty, express or implied, as to their legal effect and completeness. CRF Solutions accepts no liability in connection with reliance thereon or any loss sustained by anyone using or relying on the information contained herein. The information and documents offered by CRF Solutions are not a substitute for the advice of an attorney. CRF Solutions strongly recommends you seek assistance from an experienced construction attorney for any legal questions you might have.
								</td>

							</tr>
						</table>
					</td>
        </tr>
        <br />
<tr>
			<td valign="top">
				<table align="center" cellpadding="5" cellspacing="0" border="1">
				<tr>
				    <td align="center"><img src="ResourceImages/States/Alabama.jpg" width="52" height="70" border="0">
					</td>
				    <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Alabama Lien Law">Alabama Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Montana.jpg" width="88" height="56" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Montana Lien Law">Montana Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Alaska.jpg" width="86" height="72" border="0">
					</td>    
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Alaska Lien Law">Alaska Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Nebraska.jpg" width="74" height="50" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Nebraska Lien Law">Nebraska Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Arizona.jpg" width="58" height="66" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Arizona Lien Law">Arizona Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Nevada.jpg" width="42" height="60" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Nevada Lien Law">Nevada Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Arkansas.jpg" width="58" height="60" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Arkansas Lien Law">Arkansas Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/NewHampshire.jpg" width="34" height="62" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New Jersey Lien Law">New Hampshire Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/California.jpg" width="72" height="84" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="California Lien Law">California Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/NewJersey.jpg" width="42" height="66" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New Jersey Lien Law">New Jersey Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Colorado.jpg" width="70" height="56" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Colorado Lien Law">Colorado Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/NewMexico.jpg" width="48" height="56" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New Mexico Lien Law">New Mexico Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Connecticut.jpg" width="56" height="46" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Connecticut Lien Law">Connecticut Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/NewYork.jpg" width="88" height="78" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="New York Lien Law">New York Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Delaware.jpg" width="30" height="64" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Delaware Lien Law">Delaware Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/NorthCarolina.jpg" width="90" height="40" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="North Carolina Lien Law">North Carolina Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/DC.jpg" width="88" height="40" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="District of Columbia Lien Law">District of Columbia Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/NorthDakota.jpg" width="78" height="48" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="North Dakota Lien Law">North Dakota Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Florida.jpg" width="96" height="82" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Florida Lien Law">Florida Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Ohio.jpg" width="54" height="58" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Ohio Lien Law">Ohio Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Georgia.jpg" width="62" height="80" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Georgia Lien Law">Georgia Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Oklahoma.jpg" width="78" height="44" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Oklahoma Lien Law">Oklahoma Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Hawaii.jpg" width="94" height="80" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Hawaii Lien Law">Hawaii Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Oregon.jpg" width="80" height="58" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Oregon Lien Law">Oregon Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Idaho.jpg" width="54" height="84" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Idaho Lien Law">Idaho Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Pennsylvania.jpg" width="68" height="42" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Pennsylvania Lien Law">Pennsylvania Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Illinois.jpg" width="46" height="72" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Illinois Lien Law">Illinois Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/RhodeIsland.jpg" width="42" height="56" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Rhode Island Lien Law">Rhode Island Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Indiana.jpg" width="42" height="66" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Iowa Lien Law">Indiana Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/SouthCarolina.jpg" width="70" height="54" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="South Dakota Lien Law">South Carolina Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Iowa.jpg" width="74" height="50" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Iowa Lien Law">Iowa Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/SouthDakota.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="South Dakota Lien Law">South Dakota Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Kansas.jpg" width="78" height="48" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Kansas Lien Law">Kansas Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Tennessee.jpg" width="98" height="28" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Tennessee Lien Law">Tennessee Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Kentucky.jpg" width="92" height="52" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Kentucky Lien Law">Kentucky Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Texas.jpg" width="82" height="78" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Texas Lien Law">Texas Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Louisianna.jpg" width="62" height="52" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Louisianna Lien Law">Louisianna Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Utah.jpg" width="48" height="60" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Utah Lien Law">Utah Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Maine.jpg" width="44" height="74" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Maine Lien Law">Maine Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Vermont.jpg" width="34" height="58" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Vermont Lien Law">Vermont Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Maryland.jpg" width="82" height="48" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Maryland Lien Law">Maryland Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Virginia.jpg" width="86" height="46" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Virginia Lien Law">Virginia Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Massachusetts.jpg" width="80" height="62" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Massachusetts Lien Law">Massachusetts Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Washington.jpg" width="70" height="46" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Washington Lien Law">Washington Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Michigan.jpg" width="74" height="76" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Michigan Lien Law">Michigan Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/WestVirginia.jpg" width="64" height="66" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="West Virginia Lien Law">West Virginia Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Minnesota.jpg" width="60" height="64" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Minnesota Lien Law">Minnesota Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Wisconsin.jpg" width="56" height="64" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Wisconsin Lien Law">Wisconsin Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Mississippi.jpg" width="44" height="72" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Mississippi Lien Law">Mississippi Lien Law</a>
			        </td>
			        <td align="center"><img src="ResourceImages/States/Wyoming.jpg" width="66" height="52" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alaska-construction-lien-law/" title="Wyoming Lien Law">Wyoming Lien Law</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="ResourceImages/States/Missouri.jpg" width="74" height="60" border="0">
					</td>
			        <td align="center" width=30% class="style2"><a href="http://constructionliens.uslegal.com/state-laws/alabama-construction-lien-law/" title="Missouri Lien Law">Missouri Lien Law</a>
			        </td>
			        
			    </tr>
			    
			        
			    </table>
			</td>
</tr>
				    
