<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style2
    {
        font-family: Arial, Helvetica, sans-serif;
        font-weight: bold
    }
    .style3
    {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }
    .style5
    {
        font-family: Arial, Helvetica, sans-serif;        
    }
    .style6
    {
        font-family: Arial, Helvetica, sans-serif;
        font-style: italic;
        font-size: small;
        font-weight: bold;
    }
</style>
        

        <tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" width="65%" align="center">
							<tr>
								<td align="center" class="style3">
									State Lien Summary (Private Jobs)</td>

							</tr>
							<tr>
								<td align="left" bgcolor="#f1fbff" class="style5">
									The State Lien Summaries in this section are provided to you by the law firm of Seyfarth 
									Shaw LLP, a full service law firm with several offices across the United States.  We 
									feel you will find these summaries extremely helpful and easy to understand.  We also 
									invite you to visit their website at <a href="http://www.seyfarth.com" target="_blank">www.seyfarth.com</a> to learn about the firm and see 
									what they might be able to do for you.
								</td>

							</tr>
						</table>
					</td>
        </tr>
        <br />
<tr>
			<td align="center" valign="top">
				<table align="center" cellpadding="5" cellspacing="0" border="1">
				<tr>
				    <td align="center"><img src="Images/ResourceImages/Flags/Alabama.jpg" width="80" height="50" border="0">
					</td>
				    <td align="center" width="32%" class="style2" bgcolor="#edf0ff"><a href="Images/ResourceImages/Summary/ALPrivateSum.pdf" target="_blank" title="Alabama Lien Summary">Alabama</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Montana.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MTPrivateSum.pdf" target="_blank" title="Montana Lien Summary">Montana</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Alaska.jpg" width="80" height="50" border="0">
					</td>    
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/AKPrivateSum.pdf" target="_blank" title="Alaska Lien Summary">Alaska</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Nebraska.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NEPrivateSum.pdf" target="_blank" title="Nebraska Lien Summary">Nebraska</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Arizona.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/AZPrivateSum.pdf" target="_blank" title="Arizona Lien Summary">Arizona</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Nevada.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NVPrivateSum.pdf" target="_blank" title="Nevada Lien Summary">Nevada</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Arkansas.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/ARPrivateSum.pdf" target="_blank" title="Arkansas Lien Summary">Arkansas</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/NewHampshire.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NHPrivateSum.pdf" target="_blank" title="New Jersey Lien Summary">New Hampshire</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/California.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/CAPrivateSum.pdf" target="_blank" title="California Lien Summary">California</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/NewJersey.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NJPrivateSum.pdf" target="_blank" title="New Jersey Lien Summary">New Jersey</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Colorado.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/COPrivateSum.pdf" target="_blank" title="Colorado Lien Summary">Colorado</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/NewMexico.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NMPrivateSum.pdf" target="_blank" title="New Mexico Lien Summary">New Mexico</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Connecticut.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/CTPrivateSum.pdf" target="_blank" title="Connecticut Lien Summary">Connecticut</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/NewYork.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NYPrivateSum.pdf" target="_blank" title="New York Lien Summary">New York</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Delaware.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href=Images/ResourceImages/Summary/DEPrivateSum.pdf" target="_blank" title="Delaware Lien Summary">Delaware</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/NorthCarolina.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NCPrivateSum.pdf" target="_blank" title="North Carolina Lien Summary">North Carolina</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/DCFlag.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/DCPrivateSum.pdf" target="_blank" title="District of Columbia Lien Summary">District of Columbia</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/NorthDakota.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/NDPrivateSum.pdf" target="_blank" title="North Dakota Lien Summary">North Dakota</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Florida.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/FLPrivateSum.pdf" target="_blank" title="Florida Lien Summary">Florida</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Ohio.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/OHPrivateSum.pdf" target="_blank" title="Ohio Lien Summary">Ohio</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Georgia.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/GAPrivateSum.pdf" target="_blank" title="Georgia Lien Summary">Georgia</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Oklahoma.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/OKPrivateSum.pdf" target="_blank" title="Oklahoma Lien Summary">Oklahoma</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Hawaii.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/HIPrivateSum.pdf" target="_blank" title="Hawaii Lien Summary">Hawaii</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Oregon.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/ORPrivateSum.pdf" target="_blank" title="Oregon Lien Summary">Oregon</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Idaho.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/IDPrivateSum.pdf" target="_blank" title="Idaho Lien Summary">Idaho</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Pennsylvania.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/PAPrivateSum.pdf" target="_blank" title="Pennsylvania Lien Summary">Pennsylvania</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Illinois.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/ILPrivateSum.pdf" target="_blank" title="Illinois Lien Summary">Illinois</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/RhodeIsland.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/RIPrivateSum.pdf" target="_blank" title="Rhode Island Lien Summary">Rhode Island</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Indiana.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/INPrivateSum.pdf" target="_blank" title="Indiana Lien Summary">Indiana</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/SouthCarolina.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/SCPrivateSum.pdf" target="_blank" title="South Carolina Lien Summary">South Carolina</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Iowa.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/IAPrivateSum.pdf" target="_blank" title="Iowa Lien Summary">Iowa</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/SouthDakota.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/SDPrivateSum.pdf" target="_blank" title="South Dakota Lien Summary">South Dakota</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Kansas.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/KSPrivateSum.pdf" target="_blank" title="Kansas Lien Summary">Kansas</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Tennessee.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/TNPrivateSum.pdf" target="_blank" title="Tennessee Lien Summary">Tennessee</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Kentucky.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/KYPrivateSum.pdf" target="_blank" title="Kentucky Lien Summary">Kentucky</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Texas.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/TXPrivateSum.pdf" target="_blank" title="Texas Lien Summary">Texas</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Louisiana.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/LAPrivateSum.pdf" target="_blank" title="Louisiana Lien Summary">Louisiana</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Utah.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/UTPrivateSum.pdf" target="_blank" title="Utah Lien Summary">Utah</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Maine.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MEPrivateSum.pdf" target="_blank" title="Maine Lien Summary">Maine</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Vermont.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/VTPrivateSum.pdf" target="_blank" title="Vermont Lien Summary">Vermont</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Maryland.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MDPrivateSum.pdf" target="_blank" title="Maryland Lien Summary">Maryland</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Virginia.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/VAPrivateSum.pdf" target="_blank" title="Virginia Lien Summary">Virginia</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Massachusetts.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MAPrivateSum.pdf" target="_blank" title="Massachusetts Lien Summary">Massachusetts</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Washington.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/WAPrivateSum.pdf" target="_blank" title="Washington Lien Summary">Washington</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Michigan.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MIPrivateSum.pdf" target="_blank" title="Michigan Lien Summary">Michigan</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/WestVirginia.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/WVPrivateSum.pdf" target="_blank" title="West Virginia Lien Summary">West Virginia</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Minnesota.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MNPrivateSum.pdf" target="_blank" title="Minnesota Lien Summary">Minnesota</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Wisconsin.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/WIPrivateSum.pdf" target="_blank" title="Wisconsin Lien Summary">Wisconsin</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Mississippi.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MSPrivateSum.pdf" target="_blank" title="Mississippi Lien Summary">Mississippi</a>
			        </td>
			        <td align="center"><img src="Images/ResourceImages/Flags/Wyoming.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/WYPrivateSum.pdf" target="_blank" title="Wyoming Lien Summary">Wyoming</a>
			        </td>
			    </tr>
			    <tr>
			        <td align="center"><img src="Images/ResourceImages/Flags/Missouri.jpg" width="80" height="50" border="0">
					</td>
			        <td align="center" bgcolor="#edf0ff" width="32%" class="style2"><a href="Images/ResourceImages/Summary/MOPrivateSum.pdf" title="Missouri Lien Summary">Missouri</a>
			        </td>
			        
			    </tr>
			    
			        
			    </table>
			</td>
</tr>

<tr>
                    <td>
						<table cellpadding="0" cellspacing="0" border="0" width="60%" align="center">							
							<tr>
								<td align="left" bgcolor="#f1fbff" class="style6">
									Information and documents provided in ClientView™, to our clients free of charge, 
									are provided without any warranty, express or implied, as to their legal effect 
									and completeness. CRF Solutions accepts no liability in connection with reliance 
									thereon or any loss sustained by anyone using or relying on the information contained 
									herein. The information and documents offered by CRF Solutions are not a substitute 
									for the advice of an attorney. CRF Solutions strongly recommends you seek assistance 
									from an experienced construction attorney for any legal questions you might have.
								</td>

							</tr>
						</table>
					</td>
</tr>				    
<br />