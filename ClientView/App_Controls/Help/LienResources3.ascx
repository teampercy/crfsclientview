<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style3 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }

    .style5 {
        font-family: Arial, Helvetica, sans-serif;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        debugger;
        //alert('hi');
        //alert(window.location.search.substring(1));
        var PageName = window.location.search.substring(1);
        if (PageName == "help.lienresources3.JobView")
        {
            SetHeaderBreadCrumb('RentalView', 'Resources', 'Lien Resources');
            MainMenuToggle('liRentalViewResources');
            SubMenuToggle('liLienResourcesJobView');
        }
        else
        {
            SetHeaderBreadCrumb('LienView', 'Resources', 'Lien Resources');
            MainMenuToggle('liLienViewResources');
            SubMenuToggle('liLienResources');
        }
       
        //$('#liLienViewResources').addClass('site-menu-item has-sub active open');
        //$('#liLienResources').addClass('site-menu-item active');
        return false;

    }
    
</script>


                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: center;">
                                                Lien Resources
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: left; background-color: #f1fbff;">
                                                Information and documents provided in ClientView™, to our clients free of charge, are provided without any warranty, express or implied, as to their legal effect and completeness. CRF Solutions accepts no liability in connection with reliance thereon or any loss sustained by anyone using or relying on the information contained herein. The information and documents offered by CRF Solutions are not a substitute for the advice of an attorney. CRF Solutions strongly recommends you seek assistance from an experienced construction attorney for any legal questions you might have.
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: left;">
                                                Sample Forms
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style5" style="text-align: left;">
                                                <a href="Images/ResourceImages/Liens/Notary(Jurat).pdf" target="_blank" title="Notary (Jurat)">California Notary (Jurat)</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style5" style="text-align: left;">
                                                <a href="Images/ResourceImages/Liens/Notary(Ack).pdf" target="_blank" title="Notary (Acknowledgment)">California Notary (Acknowledgment)</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style5" style="text-align: left;">
                                                <a href="Images/ResourceImages/Liens/MechanicLienRequest.doc" target="_blank" title="Mech Lien Request(WORD)">Post Preliminary Notice Request Form (WORD)</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style5" style="text-align: left;">
                                                <a href="Images/ResourceImages/Liens/MechanicLienRequest.pdf" target="_blank" title="Mech Lien Request(PDF)">Post Preliminary Notice Request Form (PDF)</a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: left;">
                                                Industry Links
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style5" style="text-align: left;">
                                                <a href="http://www.fiscal.treasury.gov/fsreports/ref/suretyBnd/c570_a-z.htm" target="_blank" title="Treasury Surety Address List">Treasury Surety Address List</a>
                                            </div>
                                        </div>


                                    </div>
                           


