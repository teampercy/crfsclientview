﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>


<style type="text/css">
    .style2 {
        font-family: Arial, Helvetica, sans-serif;
    }

    .style3 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
        font-weight: bold;
    }

    .style6 {
        font-family: Arial, Helvetica, sans-serif;
        font-style: italic;
        font-size: small;
        font-weight: bold;
    }

    .style7 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        debugger;
        var PageName = window.location.search.substring(1);
        if (PageName == "help.lienresourcesPublic.JobView") {
            SetHeaderBreadCrumb('RentalView', 'Resources', 'Lien Summary (Public)');
            MainMenuToggle('liRentalViewResources');
            SubMenuToggle('liLienPublic1JobView');
        }
        else {
            SetHeaderBreadCrumb('LienView', 'Resources', 'Lien Summary (Public)');
            MainMenuToggle('liLienViewResources');
            SubMenuToggle('liLienPublic1');
        }
       
        //$('#liLienViewResources').addClass('site-menu-item has-sub active open');
        //$('#liLienPrivate').addClass('site-menu-item active');
        return false;

    }
  
</script>

                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: center;">
                                                State Construction Law Information
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: left;">
                                                Deadline Highlights
                                            </div>
                                            <div class="col-md-12 style7" style="text-align: left;">
                                                CRF Solutions has highlighted key state deadlines in user friendly infographics.  Access these highlights by clicking the state in the map or table below. 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: left;">
                                                Construction Law Summaries
                                            </div>
                                            <div class="col-md-12 style7" style="text-align: left;">
                                                CRF Solutions brings you comprehensive construction law summaries written by experienced construction attorneys in each state, complements of the ALFA International Construction Practice Group. 
                                            </div>
                                        </div>

                                        <div class="form-group">                                            
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 style7" style="text-align: left;">
                                                ALFA construction law summaries can be accessed here: <a href="https://www.alfainternational.com/construction-law-compendium" target="_blank">www. alfainternational.com/construction-law-compendium</a>. 
                                                ALFA International is the premier global network of independent law firms. Like the field of construction law itself, the ALFA International Construction Practice 
                                                Group is broad and diverse. It includes the best and brightest construction lawyers from every region of the United States and internationally. 
                                            </div>
                                        </div>

                                        <div class="form-group">                                            
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 style7" style="text-align: left;">
                                               Should you have any questions, please contact CRF Solutions at 800-522-3858 or clientservice@crfsolutions.com. 
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: center;">
                                                   <!-- start Fla-shop.com HTML5 Map -->
                                                <div style="text-align: center"><div id='map-container' style="margin-left: 447px"></div></div>
                                                <link href="/US_Map/map.css" rel="stylesheet">
                                                <script src="/US_Map/raphael.min.js"></script>
                                                <script src="/US_Map/settingsLienSummariesPublic.js"></script>
                                                <script src="/US_Map/paths.js"></script>
                                                <script src="/US_Map/map.js"></script>
                                                <script>
                                                  var map = new FlaMap(map_cfg);
                                                  map.drawOnDomReady('map-container');
                                                </script>
                                                <%-- <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
                                                    codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
                                                    width="530" height="410" id="usa_locator" align="center">
                                                    <param name="movie" value="usa_locator.swf">
                                                    <param name="FlashVars" value="xmlfile1=usa_locator_sum.xml">
                                                    <param name="quality" value="high">
                                                    <param name="wmode" value="direct">
                                                    <param name="bgcolor" value="#F5F5F5">
                                                    <embed src="usa_locator.swf?xmlfile1=usa_locator_sum.xml" quality="high" wmode="transparent" bgcolor="#F5F5F5" width="530" height="410" name="usa_locator" align=""
                                                        type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                                                </object>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: center;">
                                                <table align="center" cellpadding="5" cellspacing="0" border="1" style="margin-left: 470px;">
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/ALPublicSum.pdf" target="_blank" title="Alabama Lien Summary">Alabama Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MTPublicSum.pdf" target="_blank" title="Montana Lien Summary">Montana Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/AKPublicSum.pdf" target="_blank" title="Alaska Lien Summary">Alaska Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NEPublicSum.pdf" target="_blank" title="Nebraska Lien Summary">Nebraska Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/AZPublicSum.pdf" target="_blank" target="_blank" title="Arizona Lien Summary">Arizona Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NVPublicSum.pdf" target="_blank" title="Nevada Lien Summary">Nevada Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/ARPublicSum.pdf" target="_blank" title="Arkansas Lien Summary">Arkansas Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NHPublicSum.pdf" target="_blank" title="New Jersey Lien Summary">New Hampshire Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/CAPublicSum.pdf" target="_blank" title="California Lien Summary">California Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NJPublicSum.pdf" target="_blank" title="New Jersey Lien Summary">New Jersey Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/COPublicSum.pdf" target="_blank" title="Colorado Lien Summary">Colorado Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NMPublicSum.pdf" target="_blank" title="New Mexico Lien Summary">New Mexico Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/CTPublicSum.pdf" target="_blank" title="Connecticut Lien Summary">Connecticut Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NYPublicSum.pdf" target="_blank" title="New York Lien Summary">New York Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/DEPublicSum.pdf" target="_blank" title="Delaware Lien Summary">Delaware Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NCPublicSum.pdf" target="_blank" title="North Carolina Lien Summary">North Carolina Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/DCPublicSum.pdf" target="_blank" title="District of Columbia Lien Summary">District of Columbia Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/NDPublicSum.pdf" target="_blank" title="North Dakota Lien Summary">North Dakota Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/FLPublicSum.pdf" target="_blank" title="Florida Lien Summary">Florida Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/OHPublicSum.pdf" target="_blank" title="Ohio Lien Summary">Ohio Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/GAPublicSum.pdf" target="_blank" title="Georgia Lien Summary">Georgia Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/OKPublicSum.pdf" target="_blank" title="Oklahoma Lien Summary">Oklahoma Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/HIPublicSum.pdf" target="_blank" title="Hawaii Lien Summary">Hawaii Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/ORPublicSum.pdf" target="_blank" title="Oregon Lien Summary">Oregon Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/IDPublicSum.pdf" target="_blank" title="Idaho Lien Summary">Idaho Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/PAPublicSum.pdf" target="_blank" title="Pennsylvania Lien Summary">Pennsylvania Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/ILPublicSum.pdf" target="_blank" title="Illinois Lien Summary">Illinois Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/RIPublicSum.pdf" target="_blank" title="Rhode Island Lien Summary">Rhode Island Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/INPublicSum.pdf" target="_blank" title="Iowa Lien Summary">Indiana Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/SCPublicSum.pdf" target="_blank" title="South Dakota Lien Summary">South Carolina Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/IAPublicSum.pdf" target="_blank" title="Iowa Lien Summary">Iowa Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/SDPublicSum.pdf" target="_blank" title="South Dakota Lien Summary">South Dakota Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/KSPublicSum.pdf" target="_blank" title="Kansas Lien Summary">Kansas Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/TNPublicSum.pdf" target="_blank" title="Tennessee Lien Summary">Tennessee Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/KYPublicSum.pdf" target="_blank" title="Kentucky Lien Summary">Kentucky Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/TXPublicSum.pdf" target="_blank" title="Texas Lien Summary">Texas Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/LAPublicSum.pdf" target="_blank" title="Louisiana Lien Summary">Louisiana  Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/UTPublicSum.pdf" target="_blank" title="Utah Lien Summary">Utah Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MEPublicSum.pdf" target="_blank" title="Maine Lien Summary">Maine Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/VTPublicSum.pdf" target="_blank" title="Vermont Lien Summary">Vermont Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MDPublicSum.pdf" target="_blank" title="Maryland Lien Summary">Maryland Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/VAPublicSum.pdf" target="_blank" title="Virginia Lien Summary">Virginia Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MAPublicSum.pdf" target="_blank" title="Massachusetts Lien Summary">Massachusetts Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/WAPublicSum.pdf" target="_blank" title="Washington Lien Summary">Washington Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MIPublicSum.pdf" target="_blank" title="Michigan Lien Summary">Michigan Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/WVPublicSum.pdf" target="_blank" title="West Virginia Lien Summary">West Virginia Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MNPublicSum.pdf" target="_blank" title="Minnesota Lien Summary">Minnesota Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/WIPublicSum.pdf" target="_blank" title="Wisconsin Lien Summary">Wisconsin Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MSPublicSum.pdf" target="_blank" title="Mississippi Lien Summary">Mississippi Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/WYPublicSum.pdf" target="_blank" title="Wyoming Lien Summary">Wyoming Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/SummaryPublic/MOPublicSum.pdf" target="_blank" title="Missouri Lien Summary">Missouri Lien Summary</a>
                                                        </td>

                                                    </tr>


                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style6" style="text-align: left;background-color:#f1fbff;">
                                                Information and documents provided in ClientView™, to our clients free of charge, 
									are provided without any warranty, express or implied, as to their legal effect 
									and completeness. CRF Solutions accepts no liability in connection with reliance 
									thereon or any loss sustained by anyone using or relying on the information contained 
									herein. The information and documents offered by CRF Solutions are not a substitute 
									for the advice of an attorney. CRF Solutions strongly recommends you seek assistance 
									from an experienced construction attorney for any legal questions you might have.
                                            </div>
                                        </div>
                                      


                                    </div>
                              



