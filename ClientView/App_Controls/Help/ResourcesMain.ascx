<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<head>
    <style type="text/css">
        .style5 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-large;
        }

        .style6 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
        }

            .style6 > i > b {
                font-weight: bold;
            }
    </style>
    <title>Resources</title>
    <script type="text/javascript">
        var index = location.href.lastIndexOf(".");
        var View = location.href.substring(index + 1, location.href.length);

        function MaintainMenuOpen() {
            SetHeaderBreadCrumb('', '', 'Resources');
            document.getElementById('olBreadCrumb').style.visibility = "hidden";
            if (View == "CollectView") {               
                MainMenuToggle('liCollectViewResources');
                //$('#liCollectViewResources').addClass('site-menu-item has-sub active open');
            }
            else {               
                MainMenuToggle('liLienViewResources');
               // $('#liLienViewResources').addClass('site-menu-item has-sub active open');
            }

            return false;

        }

    </script>


</head>

<br />
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p class="style6">
                                                This new section of ClientView� provides users with links to various items that can help you 
        with your day to day responsibilities. This is an area that will continue to grow as we move forward, so if you 
        have any ideas or suggestions of things you would like to see available here, just let us know.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p class="style6">
                                                For our collection Client�s, we have provided links to various forms, like a personal guaranty 
        or promissory note, as well as links to documents to help better educate you on things like bankruptcies and our 
        legal process. In addition to these types of items, you will also find links to the Federal Credit Reporting Act 
        and the Federal Fair Debt Collection Practices Act and also to the different trade associations that we are members 
        of.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p class="style6">
                                                For those of you that use CRF Solutions to manage your construction notices, you now have the 
        ability to get a quick summary of the Lien Laws in all 50 States, including Washington DC. In addition, you can 
        also view or download a copy of the actual lien statute for any of the States.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p class="style6">
                                                <i><b>NOTE: It is important to note that the information and documents provided in ClientView�, to our 
        clients free of charge, are provided without any warranty, express or implied, as to their legal effect and 
        completeness. CRF Solutions accepts no liability in connection with reliance thereon or any loss sustained by 
        anyone using or relying on the information contained herein. The information and documents offered by CRF Solutions 
        are not a substitute for the advice of an attorney. CRF Solutions strongly recommends you seek assistance from an 
        experienced construction attorney for any legal questions you might have.</b></i>
                                            </p>
                                            <p class="style6">&nbsp;</p>
                                        </div>

                                    </div>
                                </div>

                         