<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Public Property TabIndex() As String
        Get
            Return Me.txtYui.TabIndex
        End Get
        Set(ByVal value As String)
            Me.txtYui.TabIndex = value
        End Set
    End Property
    Public Property Width() As System.Web.UI.WebControls.Unit
        Get
            Return Me.txtYui.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Me.txtYui.Width = value
        End Set
    End Property
    Public Property CSSClass() As String
        Get
            Return Me.txtYui.CssClass
        End Get
        Set(ByVal value As String)
            Me.txtYui.CssClass = value
        End Set
    End Property
    Public Property ErrorMessage() As String
        Get
            If IsNothing(Me.ViewState("ErrorMessage")) Then
                Return Me.ID
            End If
            Return Me.ViewState("ErrorMessage")
        End Get
        Set(ByVal value As String)
            Me.ViewState("ErrorMessage") = value
        End Set
    End Property
    Public Property IsReadOnly() As Boolean
        Get
            Return Me.txtYui.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.txtYui.ReadOnly = value
        End Set
    End Property
    Public Property IsEnabled() As Boolean
        Get
            Return Me.txtYui.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.txtYui.Enabled = value
        End Set
    End Property
    Public Property IsRequired() As Boolean
        Get
            If IsNothing(Me.ViewState("IsRequired")) Then
                Return False
            End If
            Return Me.ViewState("IsRequired")
        End Get
        Set(ByVal value As Boolean)
            Me.ViewState("IsRequired") = value
        End Set
    End Property
    Public Property Value() As String
        Get
            If IsDate(Me.txtYui.Text) Then
                Return Me.txtYui.Text
            Else
                Return String.Empty
            End If
        End Get
        Set(ByVal value As String)
            If value = "TODAY" Then

                Me.txtYui.Text = Today
            Else
                If IsDate(value) Then
                    Me.txtYui.Text = FormatDate(value)

                End If
            End If
            Me.txtYui.Text = Me.txtYui.Text

        End Set
    End Property
    Public Shared Function FormatDate(ByVal adate As Object) As String

        Try
            If IsDate(adate) And adate <> Date.MinValue Then
                Dim s As String() = Strings.Split(Date.Parse(adate).ToShortDateString, "/")
                Dim y As String = Right("00" & s(0), 2) & "/" & Right("00" & s(1), 2) & "/" & s(2)
                Return y
            Else
                Return String.Empty
            End If
        Catch ex As Exception
            Return String.Empty
        End Try
    End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.txtYui.FriendlyName = Me.ErrorMessage
        Me.txtYui.IsRequired = Me.IsRequired
        Me.txtYui.EnableClientSideScript = False
        Me.txtYui.DataType = DataTypes.DateValue
        Me.txtYui.CssClass = "form-control datepicker"
        Me.txtYui.Enabled = Me.IsEnabled
        
    End Sub
</script>
<cc1:DataEntryBox ID="txtYui" runat="server" DataType="datevalue" CssClass="form-control datepicker"  Width="120px" Enabled="true" />&nbsp;
<%--<asp:Image runat="Server" ID="btnYUI"  ImageUrl="~/app_themes/vbjuice/img/Calendar.png" width="23px" Height="23"/>
<ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" CssClass="calendar" PopupButtonID="btnYui" TargetControlID="txtYui" />--%>
