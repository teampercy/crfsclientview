<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Public Event OkClicked()
    Dim IsPosted As Boolean = False
    Dim _tempfilename As String = ""
    Dim _filename As String = ""
    Public Property HeadLine() As String
        Get
            Return Me.Label1.Text
        End Get
        Set(ByVal value As String)
            Me.Label1.Text = value
        End Set
    End Property
    Public Property FileName() As String
        Get
            Return _filename
        End Get
        Set(ByVal value As String)
            _filename = value
        End Set
    End Property
    Public Property TempFileName() As String
        Get
            Return _tempfilename
        End Get
        Set(ByVal value As String)
            _tempfilename = value
        End Set
    End Property
    
    Public Property Caption() As String
        Get
            Return Me.btnFileUpLoad.Text
        End Get
        Set(ByVal value As String)
            Me.btnFileUpLoad.Text = value
        End Set
    End Property
    Public Property Message() As String
        Get
            Return Me.Label2.Text
        End Get
        Set(ByVal value As String)
            Me.Label2.Text = value
        End Set
    End Property
    Protected Sub btnFileUpLoadOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadOk.Click
        'If IsPosted = False Then
        '    IsPosted = True
        '    _tempfilename = "~/userdata/uploads/" & System.IO.Path.GetTempFileName
        '    Me.FileUpLoad1.PostedFile.SaveAs(TempFileName)
        '    _filename = Me.FileUpLoad1.PostedFile.FileName
        '    RaiseEvent OkClicked()
        'End If
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '   FileUpLoad1.Attributes.Add("size", "50")
       
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.FileUpLoad1.PostedFile.SaveAs(TempFileName)
        _filename = Me.FileUpLoad1.PostedFile.FileName
    End Sub
</script>
<asp:Button ID="btnFileUpLoad" runat="server" Text="FileUpLoad" CssClass="button" Height="25px"  /><br />&nbsp;&nbsp;&nbsp;
<ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1" 
                runat="server" BehaviorID="FileUpLoad" TargetControlID="btnFileUpLoad" 
                PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground"
                OkControlID="" />


                 
<asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none" Width="450px" >
<div id="modcontainer"  style="MARGIN:  0px  0px 0px 0px; width:450px; ">
<div class="panelheader">
                    <asp:Label ID="Label1" runat="server" CssClass="msg" Text="Upload" />
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="close" OnClientClick="$find('FileUpLoad').hide(); return false;" />
</div>
<div class="body">
                 
                    <div class="body">
                    <p>
                    <asp:Label ID="Label2" runat="server" CssClass="msg" Text="Select File to Upload" />
                   </p>
       <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>              
                <asp:FileUpload ID="fileUpload1" runat="server" />
                <asp:Button ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" />
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="btnUpload" />               
            </Triggers>
        </asp:UpdatePanel>
 
       
                    </div>
                    <div class="footer">
                        <asp:Button ID="btnFileUpLoadOk" runat="server" Text="Yes" Width="40px" OnClick="btnFileUpLoadOk_Click" />
                        <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="No" Width="40px" OnClientClick="$find('FileUpLoad').hide(); return false;" />
                    </div>                                                
</div>
</div>

</asp:Panel>
