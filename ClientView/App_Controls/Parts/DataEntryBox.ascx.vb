Imports System
Imports System.Drawing
Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Text.RegularExpressions
Imports HDS.WEBLIB.Controls
Partial Class App_Controls_Common_DataEntryBox
    Inherits System.Web.UI.UserControl
    Dim _minvalue As String = String.Empty
    Dim _maxvalue As String = String.Empty
    Dim _maxlength As Integer = -1
    Dim _ismasked As Boolean = False
    Dim _isrequired As Boolean = False

    Dim _isvalidempty As Boolean = True
    Dim _isvalidate As Boolean = True
    Dim _datatype As DataTypes = DataTypes.Any
    Dim _errormessage As String
    Public Property TextMode() As System.Web.UI.WebControls.TextBoxMode
        Get
            Return Me.DataEntryBox.TextMode
        End Get
        Set(ByVal value As TextBoxMode)
            Me.DataEntryBox.TextMode = value
        End Set
    End Property
    Public Property MaxLength() As Integer
        Get
            Return _maxlength
        End Get
        Set(ByVal value As Integer)
            _maxlength = value
        End Set
    End Property
    Public Property DataType() As DataTypes
        Get
            Return _datatype
        End Get
        Set(ByVal value As DataTypes)
            _datatype = value
        End Set
    End Property
    Public Property MinValue() As String
        Get
            Return _minvalue
        End Get
        Set(ByVal value As String)
            _minvalue = value
        End Set
    End Property
    Public Property MaxValue() As String
        Get
            Return _maxvalue
        End Get
        Set(ByVal value As String)
            _maxvalue = value
        End Set
    End Property
    Public Property TabIndex() As String
        Get
            Return Me.DataEntryBox.TabIndex
        End Get
        Set(ByVal value As String)
            Me.DataEntryBox.TabIndex = value
        End Set
    End Property
    Public Property Width() As System.Web.UI.WebControls.Unit
        Get
            Return Me.DataEntryBox.Width
        End Get
        Set(ByVal value As System.Web.UI.WebControls.Unit)
            Me.DataEntryBox.Width = value
        End Set
    End Property
    Public Property CSSClass() As String
        Get
            Return Me.DataEntryBox.CssClass
        End Get
        Set(ByVal value As String)
            Me.DataEntryBox.CssClass = value
        End Set
    End Property
    Public Property ErrorMessage() As String
        Get
            Return _errormessage
        End Get
        Set(ByVal value As String)
            _errormessage = value
            'Me.DataEntryBox.ErrorMessage = value
        End Set
    End Property
    Public Property IsValidate() As Boolean
        Get
            Return _isvalidate
        End Get
        Set(ByVal value As Boolean)
            _isvalidate = value
        End Set
    End Property
    Public Property IsValidEmpty() As Boolean
        Get
            Return _isvalidempty
        End Get
        Set(ByVal value As Boolean)
            _isvalidempty = value
        End Set
    End Property
    Public Property IsReadOnly() As Boolean
        Get
            Return Me.DataEntryBox.ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Me.DataEntryBox.ReadOnly = value
        End Set
    End Property
    Public Property IsEnabled() As Boolean
        Get
            Return Me.DataEntryBox.Enabled
        End Get
        Set(ByVal value As Boolean)
            Me.DataEntryBox.Enabled = value
        End Set
    End Property
    Public Property IsMasked() As Boolean
        Get
            Return _ismasked
        End Get
        Set(ByVal value As Boolean)
            _ismasked = value
        End Set
    End Property
    Public Property IsRequired() As Boolean
        Get
            Return _isrequired
        End Get
        Set(ByVal value As Boolean)
            _isrequired = value
            ' Me.DataEntryBox.IsRequired = value
        End Set
    End Property
    Public Property Value() As String
        Get
            Return Me.DataEntryBox.Text
        End Get
        Set(ByVal value As String)
            If value = "TODAY" Then
                Me.DataEntryBox.Text = Today
            Else
                Me.DataEntryBox.Text = value
            End If

        End Set
    End Property
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.MaskedEditExtender1.Enabled = False
            Me.MaskedEditExtender2.Enabled = False
            Me.MaskedEditExtender3.Enabled = False
            Me.MaskedEditExtender4.Enabled = False
            If Me.IsRequired = True Then
                Me.RequiredFieldValidator1.ErrorMessage = Me.ErrorMessage & " Is Required"
                Me.RequiredFieldValidator1.Display = ValidatorDisplay.Dynamic
                Me.RequiredFieldValidator1.Visible = True
            Else
                Me.RequiredFieldValidator1.Visible = False
            End If

            If Me.MaxLength > 0 Then
                Me.DataEntryBox.Attributes.Add("maxLength", Me.MaxLength)
            End If
            If Me.IsMasked = True Then
                Select Case Me.DataType
                    Case HDS.WEBLIB.Controls.DataTypes.DateValue
                        Me.MaskedEditExtender1.Enabled = True

                    Case HDS.WEBLIB.Controls.DataTypes.PhoneNo
                        Me.MaskedEditExtender2.Enabled = True

                    Case HDS.WEBLIB.Controls.DataTypes.Number
                        Me.MaskedEditExtender3.Enabled = True

                    Case HDS.WEBLIB.Controls.DataTypes.Money
                        Me.MaskedEditExtender4.Enabled = True

                End Select
            Else

            End If
        End If
        

    End Sub
End Class
