<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Public Event IsDownLoadedIt(ByVal DownLoad As Boolean)
    Dim mydocURL As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    End Sub
    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        DownloadFile(Me.Page.MapPath(Me.HiddenField1.Value))
    End Sub
    Public Property Caption() As String
        Get
            Return Me.btnDownload.Text
        End Get
        Set(ByVal value As String)
            Me.btnDownload.Text = value
        End Set
    End Property
    Public Property DocumentURL() As String
        Get
            Return Me.HiddenField1.Value
        End Get
        Set(ByVal value As String)
            Me.HiddenField1.Value = value
        End Set
    End Property
    Public Sub DownloadFile(ByVal FileLoc As String)
        Dim objFile As New System.IO.FileInfo(FileLoc)
        Dim objResponse As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
        If objFile.Exists Then
            objResponse.ClearContent()
            objResponse.ClearHeaders()
            objResponse.AppendHeader("content-disposition", "attachment; filename=" + objFile.Name.ToString)
            objResponse.AppendHeader("Content-Length", objFile.Length.ToString())

            Dim strContentType As String
            Select Case Strings.LCase(objFile.Extension)
                Case ".txt" : strContentType = "text/plain"
                Case ".htm", ".html" : strContentType = "text/html"
                Case ".rtf" : strContentType = "text/richtext"
                Case ".jpg", ".jpeg" : strContentType = "image/jpeg"
                Case ".gif" : strContentType = "image/gif"
                Case ".bmp" : strContentType = "image/bmp"
                Case ".mpg", ".mpeg" : strContentType = "video/mpeg"
                Case ".avi" : strContentType = "video/avi"
                Case ".pdf" : strContentType = "application/pdf"
                Case ".doc", ".dot" : strContentType = "application/msword"
                Case ".csv", ".xls", ".xlt" : strContentType = "application/vnd.msexcel"
                Case Else : strContentType = "application/octet-stream"
            End Select
            objResponse.ContentType = strContentType
            objResponse.WriteFile(objFile.FullName)
            objResponse.Flush()
            objResponse.Close()
            RaiseEvent IsDownLoadedIt(True)
        Else
            RaiseEvent IsDownLoadedIt(False)
        End If
    End Sub
</script>
<asp:Button ID="btnDownload" runat="server" Text="DownLoad Report" CssClass="button" Height="25px"  /><br />&nbsp;&nbsp;&nbsp;
<asp:HiddenField ID="HiddenField1" runat="server" />
