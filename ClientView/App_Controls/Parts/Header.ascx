<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
        Public Function GetLoginOrLogoffLIteral() As String
            If Me.MyPage.IsAuthenticated = True Then
            Me.litUserINfo.Text = Me.UserInfo.UserClientName
            Response.Redirect("MainDefault.aspx", False)
            Return "Logoff"
           
            Else
                Return "Login"
            End If
        End Function
</script>
<div id="header-links">
<p>
<a href="<%=ME.mypage.getabsoluteurl("Default.aspx") %>">Home</a>&nbsp;|&nbsp; 
<a href="<%=ME.mypage.getabsoluteurl("Default.aspx?myaccount.contactus")%>">Contact</a>&nbsp;|&nbsp; 
    <a href="Login.aspx"><%=Me.GetLoginorLogoffLiteral%></a>&nbsp;&nbsp;

<%--<a href="<%=ME.mypage.getabsoluteurl("?login")%>"><%=Me.GetLoginorLogoffLiteral%></a>&nbsp;&nbsp;--%>
<br />
<asp:Literal ID="litUserINfo" runat="server"></asp:Literal>
</p>
</div>
			
