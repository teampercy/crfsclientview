<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DataEntryBox.ascx.vb" Inherits="App_Controls_Common_DataEntryBox" %>
<%@ Register TagPrefix="cc1" Namespace="HDS.WEBLIB.Controls" Assembly="HDS.WEBLIB" %>
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-12">
            <cc1:DataEntryBox ID="DataEntryBox" CssClass="form-control" runat="server"></cc1:DataEntryBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                ControlToValidate="DataEntryBox" SetFocusOnError="true" 
                ErrorMessage="Is Required Field">*</asp:RequiredFieldValidator>


            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1"
                runat="server"
                TargetControlID="DataEntryBox"
                Mask="99/99/99"
                MaskType="Date"
                MessageValidatorTip="true"
                OnFocusCssClass="focus"
                OnInvalidCssClass="error" />
            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender2"
                runat="server"
                TargetControlID="DataEntryBox"
                Mask="999-999-9999"
                MaskType="NUMBER"
                ClearMaskOnLostFocus="False"
                MessageValidatorTip="true"
                OnFocusCssClass="focus"
                OnInvalidCssClass="error"
                InputDirection="LeftToRight"
                DisplayMoney="None"
                AcceptNegative="None" />

            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender3"
                runat="server"
                TargetControlID="DataEntryBox"
                Mask="999,999"
                MaskType="NUMBER"
                ClearMaskOnLostFocus="False"
                MessageValidatorTip="true"
                OnFocusCssClass="focus"
                OnInvalidCssClass="error"
                InputDirection="LeftToRight"
                DisplayMoney="None"
                AcceptNegative="Left" />

            <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4"
                runat="server"
                TargetControlID="DataEntryBox"
                Mask="9,999,999.99"
                MaskType="Number"
                MessageValidatorTip="true"
                OnFocusCssClass="MaskEditFocus"
                OnInvalidCssClass="MaskedEditError"
                InputDirection="RightToLeft"
                DisplayMoney="None"
                AcceptNegative="Left" />
        </div>
    </div>
</div>



