<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register TagPrefix="cc" Namespace="Winthusiasm.HtmlEditor" Assembly="Winthusiasm.HtmlEditor" %>
<script runat="server">
    Public Property Value() As String
        Get
            Return Me.Content.Text
        End Get
        Set(ByVal value As String)
            Me.Content.Text = IIf(IsNothing(value), "", value)
        End Set
    End Property
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Value = Me.Content.Text
        SaveContent_Clicked()
        
       
    End Sub
</script>
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-12">
            <asp:LinkButton ID="btnHtmlEdit" runat="server" Text="Edit" CssClass="btn btn-primary" />
        </div>
    </div>
</div>

<ajaxToolkit:ModalPopupExtender ID="ModalConfirmExtender1"
    runat="server" BehaviorID="confirm" TargetControlID="btnHtmlEdit"
    PopupControlID="pnlHtmlEdit" BackgroundCssClass="modalBackground"
    OkControlID="" />

<asp:Panel ID="pnlHtmlEdit" CssClass="modalpopup" Style="display: none;" runat="server" BackColor="#dcdcdc" BorderStyle="Groove">

    <div id="modcontainer" style="margin: 10px  10px 10px 10px; WIDTH: 800PX; background: #dcdcdc">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-12">
                    <cc:HtmlEditor ID="Content" runat="server" Height="500px" Width="800px" ButtonMouseOverBorderColor="DodgerBlue" OutputXHTML="False" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-8"></div>
                <div class="col-md-4" style="text-align :right">
                    <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" OnClick="btnCancel_Click"
                        Text="Cancel" />
               
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="btnSave_Click" />
                </div>
            </div>
        </div>

    </div>

</asp:Panel>
