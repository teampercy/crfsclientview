<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<script runat="server">
    'Dim myitem As New HDS.WEBLIB.PORTALDB.TABLES.Portal
    
    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    If Page.IsPostBack = False Then
    '        Me.LoginPassword.Text = ""
    '        If Me.MyPage.IsAuthenticated = True Then
    '            Me.MyPage.SignOffUser()
    '        End If
    '    End If
    'End Sub
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    If Page.IsValid = False Then Exit Sub
    '    myitem = Me.MyPage.WebSiteSettings
    '    If Me.LoginPassword.Text.Length < 4 Then
    '        customvalidator1.ErrorMessage = "Pasword must be greater than 4 charactest, try again"
    '        customvalidator1.IsValid = False
    '        Exit Sub
    '    End If
    '    Dim myuser As HDS.WEBLIB.PORTALDB.TABLES.Portal_Users = HDS.WEBLIB.Providers.WebSite.GetUser(Me.LoginId.Text, Me.LoginPassword.Text)
        
    ''If UCase(myuser.loginpassword) = UCase(Me.LoginPassword.Text) And UCase(myuser.logincode) = UCase(Me.LoginId.Text) Then
    ''    Me.MyPage.SignOnUser(myuser, Me.chkRemember.Checked)
    ''Else
    ''    customvalidator1.ErrorMessage = "UserId or Password don't match, try again"
    ''    customvalidator1.IsValid = False
    ''    Exit Sub
    ''End If
       
    ''Dim sb As StringBuilder = New StringBuilder()
    ''sb.AppendLine("Web Contact Request From: " & contactname.Text & "<br/>")
    ''sb.AppendLine("Phone: " & Me.ContactPhone.Text & "<br/>")
    ''sb.AppendLine("Type: " & Me.RadioButtonList1.SelectedValue & "<br/>")
    ''sb.AppendLine("Submitted On: " & Now.ToString & "<br/>")
    ''sb.AppendLine("<******** Description ********>" & "<br/>")
    ''sb.AppendLine(ContactContent.Text & "<br/>")

    ''HDS.WEBLIB.Providers.WebSite.SendEmailToUs(MyPage.WebSiteSettings, Me.email.Text, Me.contactname.Text, Me.Subject.Text, sb.ToString)

    'Me.MyPage.RedirectToHomePage()
    'End Sub
    'Private Function ValidateUser(ByVal username As String, ByVal password As String) As Boolean
    '    If IsNothing(HDS.WEBLIB.Providers.WebSite.GetUser(username, password)) Then
    '        Return False
    '    Else
    '        Return True
    '    End If
    'End Function
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            'If IsNothing(Session("download")) = False Then
                
            'End If
            ''  Me.Literal1.Text = HDS.WEBLIB.Common.Utils.GetIFrame(Me.ID & "_file", "download.axd?test.pdf", 100, 100, "False").Text
        
        End If
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        me.ValidationSummary1.Visible = True
        If Page.IsValid = False Then Exit Sub
        Dim myfile As String = "test.pdf"
        'CustomValidator1.ErrorMessage = HDS.WEBLIB.Providers.WebSite.ProcessDownLoad(Me.useremail.Value, Me.loginpassword.Value, myfile, False)
        ''If customvalidator1.errormessage.Length > 5 Then
        ''     customvalidator1.IsValid = False
        ''     exit Sub
        ''End If
        
    End Sub

   
</script>
<div id="modcontainer"  style="MARGIN:  10px  10px 10px 10px; WIDTH: 600PX;">
<h1>Download Page</h1>
<div class="body">
<p>
        This sample demonstrates the shopping cart control, here the user can edit/delete items from the cart.  By pressing Continue Shopping the user can return to the Product List to Add more items to the Cart.
        When the user presses the buy now button, the order is automatically created and the paypal url is returned
        with a reference to the orderid just created.&nbsp; The user is redirected to PAYPAL.
        &nbsp; The order details and &nbsp;customer information is updated upon receipt of PAYPAL IPN this is done in the NOTIFY.ASPX
        page.
</p>
<asp:ValidationSummary ID="ValidationSummary1"  runat="server" Width="300px" HeaderText="Please Correct the Following Field(s): " />
</div>
<div class="footer">
 	<asp:LinkButton ID="btnSave" runat="server" CssClass="button" Text="Submit" OnClick="btnSave_Click"  />  <BR/>
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>
</div>
</div>
<cc1:CustomValidator id="CustomValidator1" Display=None runat="server" > </cc1:CustomValidator>
		