<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ss As String() = Strings.Split(Me.Page.Request.QueryString(0), ".")
        If Me.Page.Request.QueryString.Count > 0 Then
            If ss.Length > 1 Then
                Dim s As String = "~/app_controls/admin/" & ss(1) & ".ascx"
                If System.IO.File.Exists(Request.MapPath(s)) = True Then
                    Me.PlaceHolder1.Controls.Clear()
                    Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                End If
            End If
        End If
    End Sub
</script>
<script type="text/javascript">
    
    var index = location.href.lastIndexOf(".");
    var View = location.href.substring(index + 1, location.href.length);

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('', '', '');
        document.getElementById('divBreadcrumb').innerHTML = "";
        if (View == "SiteSetting") {
            MainMenuToggle('liSiteSetting');
          
        }
        else {
            MainMenuToggle('liScheduler');
        }

        return false;
    }
</script>
 <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
<%--<div id="sidebar">
    <div class="sidemenu">
        <SiteControls:SideMenu ID="sidemenu1" runat="server" />
    </div>
</div>
<div id="main">
    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
</div>--%>
<%--<div class="no-js css-menubar">
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-2">
            <div id="sidemenu" class="site-menubar site-menubar-light site-menubar-light">
                <div class="site-menubar-body">
                    <SiteControls:SideMenu ID="sidemenu1" runat="server" />
                </div>
            </div>
        </div>
        <div class="col-md-10">
            <div id="main">
                <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            </div>
        </div>
    </div>
</div>
</div>--%>

<%--<link href="../../App_Themes/VBJUICE/style.css" rel="stylesheet" />--%>
<style>
    .panel-body, .form-control.input-sm {
        font-size: 11px;
    }

    #ctl06_ctl00_example tbody tr:hover {
        cursor: pointer;
    }

    #ctl06_ctl00_example thead tr th {
        font-weight: bold;
    }

    #ctl06_ctl00_example thead tr th, #ctl06_ctl00_example tbody tr td {
        border: 0;
    }

    #JobFilterBody .radio-custom label {
        width: 75px;
    }

    #JobFilterBody .radio-custom.radio-default.radio-inline {
        min-height: 0px;
        padding-top: 0px;
    }
</style>

<%--<link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png">
<link rel="shortcut icon" href="../../assets/images/favicon.ico">--%>
<!-- Stylesheets -->
<%--<link rel="stylesheet" href="../../global/css/bootstrap.min.css">
<link rel="stylesheet" href="../../global/css/bootstrap-extend.min.css">
<link rel="stylesheet" href="../../assets/css/site.css">--%>
<%--<link rel="stylesheet" href="../../assets/css/site.min.css">--%>
<!-- Plugins -->
<%--<link rel="stylesheet" href="../../global/vendor/animsition/animsition.css">
<link rel="stylesheet" href="../../global/vendor/asscrollable/asScrollable.css">
<link rel="stylesheet" href="../../global/vendor/switchery/switchery.css">
<link rel="stylesheet" href="../../global/vendor/intro-js/introjs.css">
<link rel="stylesheet" href="../../global/vendor/slidepanel/slidePanel.css">
<link rel="stylesheet" href="../../global/vendor/datatables-bootstrap/dataTables.bootstrap.css" />--%>
<%--<link rel="stylesheet" href="../../global/vendor/jquery-mmenu/jquery-mmenu.css">--%>
<%--<link rel="stylesheet" href="../../global/vendor/flag-icon-css/flag-icon.css">
<link rel="stylesheet" href="../../global/vendor/chartist-js/chartist.css">
<link rel="stylesheet" href="../../global/vendor/jvectormap/jquery-jvectormap.css">
<link rel="stylesheet" href="../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css">
<link rel="stylesheet" href="../../assets/examples/css/dashboard/v1.css">
<link rel="stylesheet" href="../../assets/examples/css/forms/masks.css">--%>
<!-- Fonts -->
<%--<link rel="stylesheet" href="../../global/fonts/weather-icons/weather-icons.css">
<link rel="stylesheet" href="../../global/fonts/web-icons/web-icons.min.css">
<link rel="stylesheet" href="../../global/fonts/brand-icons/brand-icons.min.css">
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
<script src="../../global/vendor/modernizr/modernizr.js"></script>
<script src="../../global/vendor/breakpoints/breakpoints.js"></script>--%>
<%--<script>
    Breakpoints();
</script>--%>

<%--<script src="../../global/vendor/jquery/jquery.js"></script>
<script src="../../global/vendor/bootstrap/bootstrap.js"></script>
<script src="../../global/vendor/animsition/animsition.js"></script>
<script src="../../global/vendor/asscroll/jquery-asScroll.js"></script>
<script src="../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
<script src="../../global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
<script src="../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>--%>
<!-- Plugins -->
<%--<script src="../../global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>--%>
<%--<script src="../../global/vendor/switchery/switchery.min.js"></script>
<script src="../../global/vendor/intro-js/intro.js"></script>
<script src="../../global/vendor/screenfull/screenfull.js"></script>
<script src="../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
<script src="../../global/vendor/datatables/jquery.dataTables.js"></script>
<script src="../../global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
<script src="../../global/vendor/matchheight/jquery.matchHeight-min.js"></script>
<script src="../../global/vendor/asrange/jquery-asRange.min.js"></script>
<script src="../../global/vendor/formatter-js/jquery.formatter.js"></script>--%>
<!-- Scripts -->
<%--<script src="../../global/js/core.js"></script>
<script src="../../assets/js/site.js"></script>

<script src="../../assets/js/sections/menubar.js"></script>
<script src="../../assets/js/sections/gridmenu.js"></script>
<script src="../../assets/js/sections/sidebar.js"></script>
<script src="../../global/js/configs/config-colors.js"></script>
<script src="../../assets/js/configs/config-tour.js"></script>
<script src="../../global/js/components/asscrollable.js"></script>
<script src="../../global/js/components/animsition.js"></script>
<script src="../../global/js/components/slidepanel.js"></script>
<script src="../../global/vendor/skycons/skycons.js"></script>
<script src="../../global/js/components/switchery.js"></script>
<script src="../../global/js/components/formatter-js.js"></script>
<script src="../../global/js/components/datatables.js"></script>
<script src="../../global/js/plugins/responsive-tabs.js"></script>
<script src="../../global/js/plugins/closeable-tabs.js"></script>
<script src="../../global/js/components/tabs.js"></script>
<script src="../../assets/examples/js/uikit/icon.js"></script>
<script src="../../global/js/components/panel.js"></script>
<script src="../../assets/examples/js/uikit/panel-actions.js"></script>--%>
<%--<link href="../../assets/skins/indigo.min.css" rel="stylesheet" />--%>
<%--<script src="../../global/js/components/matchheight.js"></script>
<script src="../../global/js/components/jvectormap.js"></script>--%>
<%--<script src="../assets/examples/js/dashboard/v1.js"></script>--%>
<%--<script src="../../assets/js/sections/menu.js"></script>--%>
<style type="text/css">
    #olBreadCrumb {
        padding-top: 0px !important;
    }
</style>
<script type="text/javascript">

    function SetTabName(id) {
        //   alert('hh');
        // alert(id);
        document.getElementById("ctl06_ctl00_hddbTabName").value = id;

    }
    function ActivateTab(TabName) {
        // alert(TabName);
        // debugger;
        //alert('hhh');
        // alert(TabName);
        switch (TabName) {
            case "SiteSetting":
                if (!$('#SiteSetting').hasClass('active')) {
                    $('#SiteSetting').addClass('active');
                }
                if (!$('#exampleTabsOne').hasClass('active'))
                    $('#exampleTabsOne').addClass('active');
                $('#EmailSetting').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                break;

            case "EmailSetting":
                if (!$('#EmailSetting').hasClass('active')) {
                    $('#EmailSetting').addClass('active');
                }
                if (!$('#exampleTabsTwo').hasClass('active'))
                    $('#exampleTabsTwo').addClass('active');
                $('#SiteSetting').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                break;
        }
    }
</script>
