<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<script runat="server">
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal
    Dim myuser As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users
    
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
           
            Try
                If IsNothing(Request.Cookies("MyCookie")) = False Then
                    Me.logincode.Text = Request.Cookies("MyCookie")("Data")
                End If
             
            Catch ex As Exception
                Me.logincode.Value = ""
            End Try
           
            Me.Page.SetFocus(Me.loginpassword)
            
            'If IsNothing(Page.Request.QueryString("v")) = False Then
            '    If HDS.WEBLIB.Providers.Membership.RegisterUser(Page.Request.QueryString("v")) = True Then
            '        Me.MultiView1.SetActiveView(Me.View2)
            '        Exit Sub
            '    End If
            'End If
          
            If Me.MyPage.IsAuthenticated = True Then
                me.MyPage.SignOffUser(True)
            End If
            
        End If
        End Sub
        Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            If Page.IsValid = False Then Exit Sub
        '  myitem = Me.MyPage.get
        If Me.loginpassword.Text.Length < 1 Then
            CustomValidator1.ErrorMessage = "Password must be greater than 4 characters, try again"
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        
        CustomValidator1.ErrorMessage = Data.MembershipProvider.ValidateUserByLoginCode(Me.logincode.Value, Me.loginpassword.Value, myuser)
       
        If CustomValidator1.ErrorMessage.Length > 1 Then
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        Dim MYINFO As New CRF.CLIENTVIEW.BLL.ClientUserInfo(myuser.ID)
        If MYINFO.Client.IsInactive = True Then
            CustomValidator1.ErrorMessage = "You Account Is Inactive, Please Contact Customer Support"
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        If MYINFO.UserInfo.isinactive = True Then
            CustomValidator1.ErrorMessage = "You Account Is Inactive, Please Contact Customer Support"
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        
        Me.Session("UserInfo") = MYINFO
        Response.Cookies("MyCookie")("Data") = Me.logincode.Text
        Response.Cookies("MyCookie")("Time") = DateTime.Now.ToString("G")
        Response.Cookies("MyCookie").Expires = DateTime.Now.AddMonths(1)
       
        Me.MyPage.SignOnUser(myuser.ID, Me.chkRemember.Checked)
        Response.Redirect("~/MainDefault.aspx", False)
        'Me.MyPage.RedirectToHomePage() 'Commented by jaywanti
   
    End Sub
 
</script>
<div id="modcontainer"  style="MARGIN-LEFT:  220px ; margin-top: 75px; margin-bottom: 50px;WIDTH: 500PX; ">
<asp:MultiView ID="MultiView1" runat="server">
<asp:View ID="View1" runat="server">
            <h1 class="panelheader" >Login Form</h1>
       <div class="body">
            <table>
			<tr>
				<td class="row-label" style="width: 135px; float:right" >
                    Login Code</td>
				<td class="row-data" style="width: 300px" >
					<cc1:DataEntryBox id="logincode" runat="server" CssClass="textbox" Width="200px" Height="18px" IsRequired="True" AutoCompleteType="FirstName"></cc1:DataEntryBox>
				</td>
			</tr>
			<tr>
				<td class="row-label" style="width: 135px; float:right" >
                    Password</td>
				<td class="row-data" >
					<cc1:DataEntryBox id="loginpassword"  runat="server" CssClass="textbox" Width="200px" Height="18px" TextMode="Password" IsRequired="True"></cc1:DataEntryBox>
				</td>
			</tr>
			<tr>
				<td class="row-label" style="width: 135px; float:right" >
                </td>
				<td class="row-data" >
                <br />				
                    <asp:CheckBox ID="chkRemember"  runat="server" text=" Remember Me" Checked="True" />&nbsp;&nbsp&nbsp;
    	        </td>                			        
			</tr>
            <tr>
            <td><br /></td>
            </tr>            
            <tr>
				<td class="row-label" style="width: 160px; float:right" valign="center">
                    Site Best Viewed With: (Click Link To Download)</td>
				<td class="row-data">				
                <a href="https://www.google.com/intl/en/chrome/browser/?hl=en&brand=CHMA&utm_campaign=en&utm_source=en-ha-na-us-bk&utm_medium=ha
                " target="_blank"><img src="../../Images/ResourceImages/Chrome.jpg" alt="Chrome Logo" class="float-center" style="width: 80px; height: 23px" /></a>
    &nbsp;&nbsp;<a href="http://mozilla-firefox.todownload.com/?lp=adwords&tg=us&kw=Firefox%20download&mt=e&ad=33229594518&pl=&ds=s&gclid=CMH25p3C4roCFU7ZQgodJTAA_w
                " target="_blank"><img src="../../Images/ResourceImages/Firefox.jpg" alt="Firefox Logo" class="float-center" style="width: 70px; height: 25px" /></a>
    &nbsp;&nbsp;<a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie
                " target="_blank"><img src="../../Images/ResourceImages/IE11.png" alt="IE11 Logo" class="float-center" style="width: 105px; height: 30px" /></a>
				</td>
			</tr>                      
		</table>
	
	
     <asp:ValidationSummary ID="ValidationSummary1"  runat="server" Width="300px" HeaderText="Please Correct the Following Field(s): " />
     </div>

     <div class="footer">
         <asp:Panel ID="Panel1" DefaultButton="btnSave" runat="server">
         <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Submit" OnClick="btnSave_Click" />&nbsp;  <BR/>
         </asp:Panel>
    	  </div>
<cc1:CustomValidator id="CustomValidator1" Display=None runat="server" > </cc1:CustomValidator></asp:View>
   
<asp:View ID="View2" runat="server">
<h1>Registration Completed</h1>
 <div class="body">
 <p>
        You must be regsample demonstrates the shopping cart control, here the user can edit/delete items from the cart.  By pressing Continue Shopping the user can return to the Product List to Add more items to the Cart.
        When the user presses the buy now button, the order is automatically created and the paypal url is returned
        with a reference to the orderid just created.&nbsp; The user is redirected to PAYPAL.
        &nbsp; The order details and &nbsp;customer information is updated upon receipt of PAYPAL IPN this is done in the NOTIFY.ASPX
        page.
 </p>
</div>
</asp:View> 
</asp:MultiView>
</div>
		