<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style>
    body {
        padding-top: 22px;
    }

        body.modal-open {
            padding-right: 0px !important;
        }

    /*#ctl25_ctl00_gvwDocs thead tr th, #ctl25_ctl00_gvwDocs tbody tr td {
        border: 0;
    }*/
</style>
<!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
<!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
<!-- Scripts -->

<%--<script src="global/vendor/modernizr/modernizr.js"></script>
<script src="global/vendor/breakpoints/breakpoints.js"></script>
<link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">
  <!-- Stylesheets -->
  <link rel="stylesheet" href="global/css/bootstrap.min.css">
  <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="assets/css/site.min.css">
  <!-- Plugins -->
  <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="global/vendor/datatables-bootstrap/dataTables.bootstrap.css"/>
  <link rel="stylesheet" href="global/vendor/jquery-mmenu/jquery-mmenu.css">
  <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="assets/examples/css/forms/masks.css">
  <!-- Fonts -->
  <link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>--%>

  <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
 
<script>
    Breakpoints();
</script>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ss As String() = Strings.Split(Me.Page.Request.QueryString(0), ".")
       
        If Me.MyPage.IsAuthenticated = False Then
            Me.MyPage.RedirectToHomePage()
        End If
       
        Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(Me.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
        If IsNothing(myuser) Then
            Me.MyPage.SignOffUser(True)
        End If
      
        If myuser.HasLienService = False Then
            Me.MyPage.SignOffUser(True)
        End If
        
        
        If Me.Page.Request.QueryString.Count > 0 Then
            If ss.Length > 1 Then
                Dim s As String = "~/app_controls/_custom/lienview/" & ss(1) & ".ascx"
                If System.IO.File.Exists(Request.MapPath(s)) = True Then
                    Me.PlaceHolder1.Controls.Clear()
                    Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                End If
            End If
        End If
    End Sub
</script>
      <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
<%--<div class="row">
    <div class="col-xlg-12 col-md-12">
          <div >
   
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
        </div>
    </div>

</div>--%>

<%--<div id="sidebar" >
<div class="sidemenu" style="font-size:12px;">
<SiteControls:SideMenu ID="sidemenu1" runat= "server" />
</div>
</div>--%>
<%--<div id="main" >
<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
</div>--%>


<script type="text/javascript">   

    function StartLoading(id) {
        var $this = $(id);
        var isLoading;
        var type = $this.data('loader-type');
        if (!type) {
            type = 'default';
        }
        $loading = $('<div class="panel-loading">' +
          '<div class="loader loader-' + type + '"></div>' +
          '</div>');

        $loading.appendTo($this);

        $this.addClass('is-loading');
        $this.trigger('loading.uikit.panel');
        isLoading = true;
    }

    function EndLoading(id) {
        var $this = $(id);
        var type = $this.data('loader-type');
        if (!type) {
            type = 'default';
        }
        $loading = $('<div class="panel-loading">' +
          '<div class="loader loader-' + type + '"></div>' +
          '</div>');

        $loading.remove();
        $this.removeClass('is-loading');
        $this.trigger('loading.done.uikit.panel');
        isLoading = false;
    }

    function modalHideShow(id, state) {
        $("#" + id).modal(state);
    }
</script>
  <%--<script   src="global/vendor/jquery/jquery.js"></script>
  <script src="global/vendor/bootstrap/bootstrap.js"></script>
  <script src="global/vendor/animsition/animsition.js"></script>
  <script src="global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <!-- Plugins -->
  <script src="global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
  <script src="global/vendor/switchery/switchery.min.js"></script>
  <script src="global/vendor/intro-js/intro.js"></script>
  <script src="global/vendor/screenfull/screenfull.js"></script>
  <script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="global/vendor/datatables/jquery.dataTables.js"></script>
  <script src="global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
  <script src="global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="global/vendor/formatter-js/jquery.formatter.js"></script>
  <!-- Scripts -->
  <script src="global/js/core.js"></script>
  <script src="assets/js/site.js"></script>
  <script src="assets/js/sections/menu.js"></script>
  <script src="assets/js/sections/menubar.js"></script>
  <script src="assets/js/sections/gridmenu.js"></script>
  <script src="assets/js/sections/sidebar.js"></script>
  <script src="global/js/configs/config-colors.js"></script>
  <script src="assets/js/configs/config-tour.js"></script>
  <script src="global/js/components/asscrollable.js"></script>
  <script src="global/js/components/animsition.js"></script>
  <script src="global/js/components/slidepanel.js"></script>
  <script src="global/js/components/switchery.js"></script>
  <script src="global/js/components/formatter-js.js"></script>
  <script src="global/js/components/datatables.js"></script>
  <script src="global/js/plugins/responsive-tabs.js"></script>
  <script src="global/js/plugins/closeable-tabs.js"></script>
  <script src="global/js/components/tabs.js"></script>
  <script src="assets/examples/js/uikit/icon.js"></script>
  <script src="global/js/components/panel.js"></script>
  <script src="assets/examples/js/uikit/panel-actions.js"></script>--%>