<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ss As String() = Strings.Split(Me.Page.Request.QueryString(0), ".")
        Dim s As String
        Me.contentbox1.Visible = False
        If ss.Length > 1 Then
            Me.contentbox1.Visible = False
            Select Case ss(1).ToLower
                Case "lventerjob"
                    s = "~/app_controls/training/" & "lventerjob.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                Case "lvviewjob"
                    s = "~/app_controls/training/" & "lvviewjob.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                Case "lvfilterjob"
                    s = "~/app_controls/training/" & "lvfilterjob.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                Case "lvwaiver"
                    s = "~/app_controls/training/" & "lvwaiver.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                Case "videosmain"
                    s = "~/app_controls/training/" & "videosmain.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                Case "cventeraccount"
                    s = "~/app_controls/training/" & "cventeraccount.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                Case "cvviewaccount"
                    s = "~/app_controls/training/" & "cvviewaccount.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                Case "cvreports"
                    s = "~/app_controls/training/" & "cvreports.ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        Me.PlaceHolder1.Controls.Clear()
                        Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                    End If
                
                Case Else
                    Me.contentbox1.Visible = True
                    Me.contentbox1.TemplateName = Me.Page.Request.QueryString(0)
                    
            End Select
            
        End If
        
    End Sub
</script>
<style>
    body {
        padding-top: 22px;
    }

        body.modal-open {
            padding-right: 0px !important;
        }

    /*#ctl25_ctl00_gvwDocs thead tr th, #ctl25_ctl00_gvwDocs tbody tr td {
        border: 0;
    }*/
</style>
   <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            <SiteControls:contentbox ID="contentbox1" TemplateName="home" runat="server" />
<%--<div class="row">
    <div class="col-xlg-12 col-md-12">
        <div>
            <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
            <SiteControls:contentbox ID="contentbox1" TemplateName="home" runat="server" />
        </div>
    </div>

</div>--%>
<%--<div id="sidebar" >
<div class="sidemenu">
 <SiteControls:SideMenu ID="SIDEMenu1" runat="server"></SiteControls:SideMenu>
</div>
</div>--%>
<%--<div id="main" >
<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
<sitecontrols:contentbox ID="contentbox1" TemplateName="home" runat="server" />
</div>--%>
