<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ss As String() = Strings.Split(Me.Page.Request.QueryString(0), ".")
        If Me.MyPage.IsAuthenticated = False Then
            Me.MyPage.RedirectToHomePage()
        End If
        
        Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(Me.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
        If IsNothing(myuser) Then
            Me.MyPage.SignOffUser(True)
        End If
      
        If myuser.HasCollectionService = False Then
            Me.MyPage.SignOffUser(True)
        End If
         
        If Me.Page.Request.QueryString.Count > 0 Then
            If ss.Length > 1 Then
                Dim s As String = "~/app_controls/_custom/collectview/" & ss(1) & ".ascx"
                If System.IO.File.Exists(Request.MapPath(s)) = True Then
                    Me.PlaceHolder1.Controls.Clear()
                    Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                End If
            End If
        End If
    End Sub
</script>
<%--<link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
  <link rel="shortcut icon" href="assets/images/favicon.ico">--%>
<!-- Stylesheets -->
<%--<link rel="stylesheet" href="global/css/bootstrap.min.css">
  <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
  <link rel="stylesheet" href="assets/css/site.min.css">--%>
<!-- Plugins -->
<%--<link rel="stylesheet" href="global/vendor/animsition/animsition.css">
  <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
  <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
  <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
  <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
  <link rel="stylesheet" href="global/vendor/datatables-bootstrap/dataTables.bootstrap.css"/>
  <link rel="stylesheet" href="global/vendor/jquery-mmenu/jquery-mmenu.css">
  <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
  <link rel="stylesheet" href="assets/examples/css/forms/masks.css">--%>
<!-- Fonts -->
<%--<link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css">
  <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
  <script src="global/vendor/modernizr/modernizr.js"></script>
  <script src="global/vendor/breakpoints/breakpoints.js"></script>--%>

<%--<script   src="global/vendor/jquery/jquery.js"></script>
  <script src="global/vendor/bootstrap/bootstrap.js"></script>
  <script src="global/vendor/animsition/animsition.js"></script>
  <script src="global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>--%>
<!-- Plugins -->
<%--<script src="global/vendor/jquery-mmenu/jquery.mmenu.min.all.js"></script>
  <script src="global/vendor/switchery/switchery.min.js"></script>
  <script src="global/vendor/intro-js/intro.js"></script>
  <script src="global/vendor/screenfull/screenfull.js"></script>
  <script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="global/vendor/datatables/jquery.dataTables.js"></script>
  <script src="global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
  <script src="global/vendor/matchheight/jquery.matchHeight-min.js"></script>
  <script src="global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="global/vendor/formatter-js/jquery.formatter.js"></script>--%>
<!-- Scripts -->
<%-- <script src="global/js/core.js"></script>
  <script src="assets/js/site.js"></script>
  <script src="assets/js/sections/menu.js"></script>
  <script src="assets/js/sections/menubar.js"></script>
  <script src="assets/js/sections/gridmenu.js"></script>
  <script src="assets/js/sections/sidebar.js"></script>
  <script src="global/js/configs/config-colors.js"></script>
  <script src="assets/js/configs/config-tour.js"></script>
  <script src="global/js/components/asscrollable.js"></script>
  <script src="global/js/components/animsition.js"></script>
  <script src="global/js/components/slidepanel.js"></script>
  <script src="global/js/components/switchery.js"></script>
  <script src="global/js/components/formatter-js.js"></script>
  <script src="global/js/components/datatables.js"></script>
  <script src="global/js/plugins/responsive-tabs.js"></script>
  <script src="global/js/plugins/closeable-tabs.js"></script>
  <script src="global/js/components/tabs.js"></script>
  <script src="assets/examples/js/uikit/icon.js"></script>
  <script src="global/js/components/panel.js"></script>
  <script src="assets/examples/js/uikit/panel-actions.js"></script>--%>
<script type="text/javascript">


    function modalHideShow(id, state) {
        $("#" + id).modal(state);
    }

</script>
<style>
    body {
        padding-top: 22px;
    }

        body.modal-open {
            padding-right: 0px !important;
        }

    /*#ctl25_ctl00_gvwDocs thead tr th, #ctl25_ctl00_gvwDocs tbody tr td {
        border: 0;
    }*/
</style>

<%--<div id="sidebar" >
<div class="sidemenu">
<SiteControls:SideMenu ID="sidemenu1" runat= "server" />
</div>
</div>--%>
  <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
<%--<div class="page-header" style="text-align: left;">
    <ol class="breadcrumb" style="padding-top: 30px;">
        <li><a href="../index.html">Home</a></li>
        <li class="active">Basic UI</li>
    </ol>
    <h1 class="page-title">Tabs &amp; Accordions</h1>
</div>
<div class="page-content padding-30 container-fluid" style="margin-top: -58px;">
    <div class="row" data-plugin="matchHeight" data-by-row="true">
        <div class="col-xlg-12 col-md-12">
            <div class="widget widget-shadow widget-responsive" id="widgetLineareaColor">
                <div class="widget-content">
                    <div class="padding-top-30 padding-30" style="height: calc(100% - 250px);">

                        <div class="row">
                            <div class="col-xlg-12 col-md-12">
                                <div>
                                 
                                    <asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>--%>

