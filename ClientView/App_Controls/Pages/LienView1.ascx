<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ss As String() = Strings.Split(Me.Page.Request.QueryString(0), ".")
       
        If Me.MyPage.IsAuthenticated = False Then
            Me.MyPage.RedirectToHomePage()
        End If
       
        Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(Me.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
        If IsNothing(myuser) Then
            Me.MyPage.SignOffUser(True)
        End If
      
        If myuser.HasLienService = False Then
            Me.MyPage.SignOffUser(True)
        End If
        
        
        If Me.Page.Request.QueryString.Count > 0 Then
            If ss.Length > 1 Then
                Dim s As String = "~/app_controls/_custom/lienview/" & ss(1) & ".ascx"
                If System.IO.File.Exists(Request.MapPath(s)) = True Then
                    Me.PlaceHolder1.Controls.Clear()
                    Me.PlaceHolder1.Controls.Add(Me.LoadControl(s))
                End If
            End If
        End If
    End Sub
</script>
<div id="sidebar" >
<div class="sidemenu">
<SiteControls:SideMenu ID="sidemenu1" runat= "server" />
</div>
</div>
<div id="main" >
<asp:PlaceHolder ID="PlaceHolder1" runat="server"></asp:PlaceHolder>
</div>
