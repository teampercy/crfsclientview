Partial Class App_Controls_crfLienView_NewJobList
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim mylieninfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientLienInfo
    Dim myclient As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Client
    Protected WithEvents btnDeleteItem As LinkButton
    Protected WithEvents btnEditItem As LinkButton
    Protected WithEvents btnPrintItem As LinkButton
    Protected WithEvents btnCopyItem As LinkButton

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Me.HiddenField1.Value = Me.CurrentUser.Id
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Session("filter") = ""
            Me.Session("sort") = "SubmittedOn desc"
            Me.gvItemList.DataBind()

            Me.ViewState("BatchJobId") = -1

            txtFindText.Enabled = gvItemList.Rows.Count > 0
            btnFindText.Enabled = gvItemList.Rows.Count > 0
            btnRefresh.Enabled = gvItemList.Rows.Count > 0
            btnPrint.Enabled = gvItemList.Rows.Count > 0

            If (gvItemList.Rows.Count > 0) Then
                btnFindText.CssClass = "btn btn-primary"
                btnRefresh.CssClass = "btn btn-primary"
                btnPrint.CssClass = "btn btn-primary"
            Else
                btnFindText.CssClass = "btn btn-primary"
                btnRefresh.CssClass = "btn btn-primary "
                btnPrint.CssClass = "btn btn-primary "
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFuncJobList();", True)

        End If
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If



    End Sub
#Region "List Related"
    Protected Sub btnFindText_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindText.Click
        Dim myfilter As New HDS.DAL.COMMON.FilterBuilder
        Dim s As String = ""
        myfilter.Clear()

        myfilter.SearchAnyWords("jobname", Me.txtFindText.Text, "OR")
        myfilter.SearchAnyWords("jobnum", Me.txtFindText.Text, "OR")
        myfilter.SearchAnyWords("jobadd1", Me.txtFindText.Text, "OR")
        myfilter.SearchAnyWords("custname", Me.txtFindText.Text, "")

        s = myfilter.GetFilter
        Me.Session("filter") = s
        Me.gvItemList.DataBind()
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFuncJobList();", True)

    End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.Session("filter") = ""
        Me.gvItemList.DataBind()
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFuncJobList();", True)
    End Sub
    Protected Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim s As String = "~/MainDefault.aspx?lienview.newjobnew&itemid=-1"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?lienview.newjobnew&itemid=" & btnEditItem.Attributes("rowno")

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnPrintItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintItem.Click
        btnPrintItem = TryCast(sender, LinkButton)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobAck(Me.CurrentUser.Id, btnPrintItem.Attributes("rowno"), True)
        ' sreport = Replace(sreport, "'", "\'")
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    Filepath = Replace(Filepath, "'", "~")

                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View1)
        End If
    End Sub
    Protected Sub btnDeleteItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteItem.Click
        btnDeleteItem = TryCast(sender, LinkButton)
        myitem.Id = btnDeleteItem.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        Me.gvItemList.DataBind()
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFuncJobList();", True)
        Me.MultiView1.SetActiveView(Me.View1)


        txtFindText.Enabled = gvItemList.Rows.Count > 0
        btnFindText.Enabled = gvItemList.Rows.Count > 0
        btnRefresh.Enabled = gvItemList.Rows.Count > 0
        btnPrint.Enabled = gvItemList.Rows.Count > 0

        If (gvItemList.Rows.Count > 0) Then
            btnFindText.CssClass = "btn btn-primary"
            btnRefresh.CssClass = "btn btn-primary"
            btnPrint.CssClass = "btn btn-primary"
        Else
            btnFindText.CssClass = "btn btn-primary "
            btnRefresh.CssClass = "btn btn-primary"
            btnPrint.CssClass = "btn btn-primary"
        End If
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobsSubmitted(Me.CurrentUser.Id, True)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    Filepath = Replace(Filepath, "'", "~")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View1)
        End If
    End Sub
    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= Me.gvItemList.PageCount Then
            Me.gvItemList.PageIndex = pageNumber - 1
        Else
            Me.gvItemList.PageIndex = 0
        End If
    End Sub
    Protected Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteItem"), LinkButton).Click, AddressOf btnDeleteItem_Click
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
            btnPrintItem = TryCast(e.Row.FindControl("btnPrintItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrintItem"), LinkButton).Click, AddressOf btnPrintItem_Click
            btnCopyItem = TryCast(e.Row.FindControl("btnCopyItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnCopyItem"), LinkButton).Click, AddressOf btnCopyItem_Click

        End If
    End Sub
    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteItem.Attributes("rowno") = dr("Id")
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = dr("Id")
            btnPrintItem = TryCast(e.Row.FindControl("btnPrintItem"), LinkButton)
            btnPrintItem.Attributes("rowno") = dr("Id")
            btnCopyItem = TryCast(e.Row.FindControl("btnCopyItem"), LinkButton)
            btnCopyItem.Attributes("rowno") = dr("Id")
        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
        End If

        If e.Row.RowType = DataControlRowType.Pager Then
            Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
            lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

            Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
            txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

            Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
            ddlPageSize.SelectedValue = gridView.PageSize.ToString()
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Display the company name in italics.
            e.Row.Cells(1).Text = "" & Strings.Left(e.Row.Cells(1).Text, 25) & ""
            e.Row.Cells(2).Text = "" & Strings.Left(e.Row.Cells(2).Text, 10) & ""
            e.Row.Cells(3).Text = "" & Strings.Left(e.Row.Cells(3).Text, 15) & ""
            e.Row.Cells(4).Text = "" & Strings.Left(e.Row.Cells(4).Text, 15) & ""
            e.Row.Cells(5).Text = "" & Strings.Left(e.Row.Cells(5).Text, 20) & ""
            e.Row.Cells(6).Text = "" & Strings.FormatCurrency(Decimal.Parse(e.Row.Cells(6).Text)) & ""


        End If

    End Sub
    Protected Sub gvItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvItemList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)

    End Sub
    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
#End Region
    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        Dim s As String = "~/MainDefault.aspx?lienview.JobSearch"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnCopyItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopyItem.Click
        btnCopyItem = TryCast(sender, LinkButton)
        myitem.Id = btnCopyItem.Attributes("rowno")

        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_LiensDataEntry_CloneBatchJob
        mysproc.BatchJobId = myitem.Id
        mysproc.submittedbyuserid = Me.CurrentUser.Id
        mysproc.TempKey = CRF.CLIENTVIEW.BLL.ProviderBase.GetUniqueId
        Dim myretid As Integer = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecScalar(mysproc)
        Dim MYSQL As String = "Select Id From BatchJob where batchid = - 1 "
        MYSQL += " And SubmittedByuserId = '" & Me.CurrentUser.Id & "' "
        MYSQL += " And CloneJobId = '" & myitem.Id & "' "
        MYSQL += " And TempKey = '" & mysproc.TempKey & "' "
        Dim myds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetDataSet(MYSQL)
        Dim mynewjobid = myds.Tables(0).Rows(0).Item("Id")
        Dim s As String = "~/MainDefault.aspx?lienview.newjobnew&itemid=" & mynewjobid
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub

   
    Protected Sub btnDeleteJobId_Click(sender As Object, e As EventArgs)
        myitem.Id = hdnJobId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)
        Me.gvItemList.DataBind()
        If gvItemList.Rows.Count > 0 Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFuncJobList();", True)
        Me.MultiView1.SetActiveView(Me.View1)


        txtFindText.Enabled = gvItemList.Rows.Count > 0
        btnFindText.Enabled = gvItemList.Rows.Count > 0
        btnRefresh.Enabled = gvItemList.Rows.Count > 0
        btnPrint.Enabled = gvItemList.Rows.Count > 0

        If (gvItemList.Rows.Count > 0) Then
            btnFindText.CssClass = "btn btn-primary"
            btnRefresh.CssClass = "btn btn-primary"
            btnPrint.CssClass = "btn btn-primary"
        Else
            btnFindText.CssClass = "btn btn-primary "
            btnRefresh.CssClass = "btn btn-primary"
            btnPrint.CssClass = "btn btn-primary"
        End If
    End Sub
End Class
