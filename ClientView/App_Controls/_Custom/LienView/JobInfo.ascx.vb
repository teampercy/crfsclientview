﻿Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Imports System.Net.Mail
Imports System.IO
Partial Class App_Controls__Custom_LienView_JobInfo
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myview As HDS.DAL.COMMON.TableView
    Dim dsjobinfo As System.Data.DataSet
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
    Dim mywaiver As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobWaiverLog
    Dim mycustomer As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomer
    Dim itemid As String
    Dim tabindex As String
    Dim sremail As String
    Dim JobAddSubjectLine As String
    Dim s1 As String


    Protected Property currentSortColumn() As String
        Get
            Return HttpContext.Current.Session("CurrentSortCol") ' "" And HttpContext.Current.Session("CurrentSortDir")
        End Get
        Set(ByVal Value As String)
            HttpContext.Current.Session("CurrentSortCol") = Value
        End Set
    End Property

    Protected Property currentSortDirection() As String
        Get
            Return HttpContext.Current.Session("CurrentSortDir")
        End Get
        Set(ByVal Value As String)
            HttpContext.Current.Session("CurrentSortDir") = Value
        End Set
    End Property

    Protected WithEvents btnDeleteItem As LinkButton
    Protected WithEvents btnPrntNotice As LinkButton
    Protected WithEvents btnPrntWaiver As LinkButton
    Protected WithEvents btnPrntSForm As LinkButton
    Protected WithEvents btnEmailWaiver As LinkButton

    Protected WithEvents btnEditWaiver As LinkButton
    Protected WithEvents btnDeleteWaiver As LinkButton

    Protected WithEvents btnEditSForm As LinkButton
    Protected WithEvents btnDeleteSForm As LinkButton

    Protected WithEvents btnEditTReq As LinkButton
    Protected WithEvents btnPrntTReq As LinkButton
    Protected WithEvents btnDeleteTReq As LinkButton

    Protected WithEvents btnEditVReq As LinkButton
    Protected WithEvents btnPrntVReq As LinkButton
    Protected WithEvents btnDeleteVReq As LinkButton

    Protected WithEvents btnEditINVC As LinkButton
    Protected WithEvents btnPrntINVC As LinkButton
    Protected WithEvents btnDeleteINVC As LinkButton

    Protected WithEvents btnPrntDocs As LinkButton
    Protected WithEvents btnlnk As LinkButton
    Protected WithEvents btnViewNote As LinkButton

    Protected WithEvents btnDeleteDocItem As LinkButton
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job

    Protected WithEvents btnRecension As LinkButton
    Dim myJobLegalParties As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobLegalParties
    Dim myStateForms As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateForms
    Dim StateCode As String
    Dim dsJobWaiverLogByInfo As System.Data.DataTable
    Dim dsJobWaiverLogByRecensionLetter As System.Data.DataTable
    Dim dsJobLegalParties As System.Data.DataTable
    Dim JobWaiverLogId As String
    Dim LegalPartyId As String
    Dim JobLegalPartiesTypeCode As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ' myview = DirectCast(Me.Session("joblist"), HDS.DAL.COMMON.TableView)
            Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
            Me.itemid = qs.GetParameter("ItemId")
            'markparimal-uploadfile:jobid value is taken in session so that it can be used in file upload page.
            Session("jobid") = Me.itemid

        Session("bitjobid") = 1
        Dim test As String = Request.Url.Segments.Last()
        hddbTabName.Value = Request.Form(hddbTabName.UniqueID)
        If (hddbTabName.Value = "") Then
            hddbTabName.Value = "MainInfo"
        End If

        If Me.CurrentUser.UserManager = 0 Then

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideDiveNotes", "hideDiveNotes();", True)

        End If
        If Me.Page.IsPostBack = False Then
            'markparimal-navigation issues 1/2/2011
            Me.Session("WaiverTab") = qs.GetParameter("tabstate")
            'Me.Page.ClientScript.RegisterStartupScript(Page.GetType, "Upload Completed", "test('" & Session("WaiverTab") & "');", True)
            If Me.Session("WaiverTab") = "WAIVER" Then
                WaiverTab()
            End If
            If Me.Session("WaiverTab") = "SFORM" Then
                SFORMTab()
            End If
            ' end navigation issues 


            Me.ViewState("VIEW") = "MAIN"
            'If qs.HasParameter("VIEW") Then
            '    Me.ViewState("VIEW") = qs.GetParameter("VIEW").ToUpper()
            'End If
            Me.Session("LIST") = Nothing
            If qs.HasParameter("LIST") Then
                Me.Session("LIST") = qs.GetParameter("LIST")
            End If

            Me.MultiView1.SetActiveView(Me.View1)
            LoadData(qs.GetParameter("ItemId"))

            If qs.HasParameter("VIEW") Then
                Me.ViewState("VIEW") = qs.GetParameter("VIEW").ToUpper()
                If qs.HasParameter("formid") = True And qs.GetParameter("VIEW") = "WAIVER" Then
                    'Me.Session("WaiverTab") = qs.GetParameter("VIEW")
                    PrintWaiver(qs.GetParameter("formid"))
                End If
                If qs.HasParameter("formid") = True And qs.GetParameter("VIEW") = "SFORM" Then
                    'Me.Session("WaiverTab") = qs.GetParameter("VIEW")
                    PrintSFORM(qs.GetParameter("formid"))
                End If

                If Me.ViewState("VIEW") = "TREQ" Then
                    'Me.TabContainer.ActiveTabIndex = 7
                    'Me.TabContainer.TabIndex = 7
                    hddbTabName.Value = "TXRequests"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActives", "ActivateTab('" & hddbTabName.Value & "');", True)
                End If
                If Me.ViewState("VIEW") = "VREQ" Then
                    'Me.TabContainer.ActiveTabIndex = 7
                    'Me.TabContainer.TabIndex = 7
                    hddbTabName.Value = "NoticeRequest"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActives", "ActivateTab('" & hddbTabName.Value & "');", True)
                End If
                If Me.ViewState("VIEW") = "INVC" Then
                    'Me.TabContainer.ActiveTabIndex = 7
                    'Me.TabContainer.TabIndex = 7
                    hddbTabName.Value = "Invoices"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActives", "ActivateTab('" & hddbTabName.Value & "');", True)
                End If
            End If
        Else
            If gvwLegalParties.Rows.Count > 0 Then
                Me.gvwLegalParties.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwNotes.Rows.Count > 0 Then
                Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwNotices.Rows.Count > 0 Then
                Me.gvwNotices.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwDocs.Rows.Count > 0 Then
                Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwWaivers.Rows.Count > 0 Then
                Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwStateForms.Rows.Count > 0 Then
                Me.gvwStateForms.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwVReq.Rows.Count > 0 Then
                Me.gvwVReq.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwTexasReq.Rows.Count > 0 Then
                Me.gvwTexasReq.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwINVC.Rows.Count > 0 Then
                Me.gvwINVC.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncLegalPartiesList", "CallSuccessFuncLegalPartiesList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncNotesList", "CallSuccessFuncNotesList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDocsList", "CallSuccessFuncDocsList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncWaiverList", "CallSuccessFuncWaiverList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncStateList", "CallSuccessFuncStateList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncTexasList", "CallSuccessFuncTexasList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncNoticesReqList", "CallSuccessFuncNoticesReqList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncInvoiceList", "CallSuccessFuncInvoiceList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncNoticesList", "CallSuccessFuncNoticesList();", True)

            LoadData(qs.GetParameter("ItemId"))

        End If
        Dim strpagename As String
        If (Not qs.GetParameter("PageName") Is Nothing) Then
            strpagename = qs.GetParameter("PageName")
        Else
            strpagename = String.Empty
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsHeader", "SetHeaderInformation('" & strpagename & "');", True)

            'markparimal-uploadfile:To redirect to Doc Tab.
            'tabindex = Request.QueryString("tabindex")
            'If tabindex = "4" Then
            '    TabContainer.ActiveTabIndex = 4
            'Else
            '    TabContainer.ActiveTabIndex = 0
            'End If
        Catch ex As Exception
            Dim mymsg As String = ex.Message
        End Try
        SetCurrentTab()
       
      
    End Sub
    Public Overloads Sub LoadData(ByVal ajobid As String)
        Try

            dsjobinfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobByInfo(ajobid)
            Dim myview As New VIEWS.vwJobInfo
            HDS.WEBLIB.Common.Mapper.FillEntity(dsjobinfo.Tables(0), myview)
            hdnIsCancelled.Value = myview.IsCancelled
            Me.JobId.Text = myview.JobId
            Me.JobNumber.Text = myview.JobNum
            Me.JobName.Text = myview.JobName
            Me.JobAdd1.Text = myview.JobAdd1
            Me.JobAdd2.Text = myview.JobAdd2
            Me.JobCity.Text = myview.JobCity & ", " & myview.JobState & " " & myview.JobZip
            Me.JobState.Text = myview.JobState
            StateCode = myview.JobState
            Me.JobZip.Text = myview.JobZip

        Me.StartDate.Value = myview.StartDate
        Me.EndDate.Value = myview.EndDate
        Me.DateAssigned.Value = myview.DateAssigned
        Me.NoticeSent.Value = myview.NoticeSent
        'Me.LienDate.Value = myview.LienDate
        Me.LienDate.Value = myview.FileDate
        Me.StatusCode.Value = myview.StatusCode
        Me.APNNum.Value = myview.APNNum
        Me.EstBalance.Value = myview.EstBalance
        Me.JobCounty.Value = myview.JobCounty
        Me.StatusCode.Value = myview.StatusCode & "-" & Strings.Left(myview.JobStatusDescr, 90)
        Me.PONum.Value = myview.PONum
        Me.CustRefNum.Value = myview.CustRefNum
        Me.EquipRate.Value = myview.EquipRate
        Me.FilterKey.Value = myview.FilterKey
        Me.EquipRental.Value = myview.EquipRental
        'Me.RANum.Value = myview.RANum
        Me.SpecialInstruction.Text = myview.SpecialInstruction
        Me.AZLotAllocate.Text = myview.AZLotAllocate
        Me.NOCDate.Value = myview.NOCDate
        If myview.Note.ToString <> "" Then
            Me.Note.Text = Replace(Server.HtmlEncode(myview.Note.ToString), "#", "")
        End If
        Me.BuildingPermitNum.Text = myview.BuildingPermitNum

        ' Added 5/16/2016
        Me.NOIDeadlineDate.Value = myview.NoticeDeadlineDate
        Me.NOIDeadline.Value = myview.NOIDeadlineDate
        Me.BondDeadlineDate.Value = myview.BondDeadlineDate
        Me.SNDeadlineDate.Value = myview.SNDeadlineDate
        Me.LienDeadlineDate.Value = myview.LienDeadlineDate
        Me.BondDate.Value = myview.BondDate
        Me.SNDate.Value = myview.SNDate
        Me.ForecloseDeadline.Value = myview.ForeclosureDeadlineDate
        Me.ForecloseDate.Value = myview.ForeclosureDate
        Me.BondSuitDeadline.Value = myview.BondSuitDeadlineDate
        Me.BondSuitDate.Value = myview.BondSuitDate

        If myview.PrivateJob = True Then
            Me.PropertyType.Text = "Private"
        ElseIf myview.PublicJob = True Then
            Me.PropertyType.Text = "Public"
        ElseIf myview.ResidentialBox = True Then
            Me.PropertyType.Text = "Residential"
        ElseIf myview.FederalJob = True Then
            Me.PropertyType.Text = "Federal"
        End If
        Me.CustomerName.Text = myview.CustName

        ViewState("JobAddSubjectLine") = "(" + Convert.ToString(myview.JobId) + ") " + myview.JobAdd1 + " " + myview.JobCity + " " + myview.JobState

        'Added by Jaywanti on 26-07-2016
        If myview.PrelimBox = True Then
            Me.ServiceType.Text = "Verify Job Data & Send Notice"
        ElseIf myview.SendAsIsBox = True Then 'PrelimASIS this is for batchjob and SendAsIsBox is for Job Table both are equivalent
            Me.ServiceType.Text = "Send Notice With Data Provided"
        ElseIf myview.VerifyJob = True Then
            Me.ServiceType.Text = "Verify Job Data Only - No Notice Sent"
        ElseIf myview.TitleVerifiedBox = True Then
            Me.ServiceType.Text = "Store Job Data As Provided - No Notice Sent"
        ElseIf myview.VerifyOWOnly = True Then
            Me.ServiceType.Text = "Verify Owner Only"
        ElseIf myview.VerifyOWOnlyTX = True Then
            Me.ServiceType.Text = "Verify Owner Only TX"
        ElseIf myview.VerifyOWOnlySend = True Then
            Me.ServiceType.Text = "Verify Owner and Send"
        End If
        'If (Me.hdnServiceTypeId.Value = "1") Then
        '    myitem.PrelimBox = True
        'ElseIf (Me.hdnServiceTypeId.Value = "2") Then
        '    myitem.PrelimASIS = True
        'ElseIf (Me.hdnServiceTypeId.Value = "3") Then
        '    myitem.VerifyJob = True
        'ElseIf (Me.hdnServiceTypeId.Value = "4") Then
        '    myitem.TitleVerifiedBox = True
        'ElseIf (Me.hdnServiceTypeId.Value = "5") Then
        '    myitem.VerifyOWOnly = True
        'ElseIf (Me.hdnServiceTypeId.Value = "6") Then
        '    myitem.VerifyOWOnlyTX = True
        'ElseIf (Me.hdnServiceTypeId.Value = "7") Then
        '    myitem.VerifyOWOnlySend = True
        'End If


        'Me.chkBondClaim.Checked = myview.BondReleaseBox
        'Me.chkFederal.Checked = myview.FederalJob
        'Me.chkMechanicLien.Checked = myview.MechanicLien
        Me.BondNum.Value = myview.LenderBondNum
        'Me.chkPrelim.Checked = myview.PrelimBox
        'Me.chkPublicJob.Checked = myview.PublicJob
        ' Me.chkVerify.Checked = myview.VerifyJob
        ' Me.chkSendAsIS.Checked = myview.SendAsIsBox
        ' Me.chkJobAlert.Checked = myview.JobAlertBox

        'Me.chkResindential.Checked = myview.ResidentialBox
        'Me.chkStopNotice.Checked = myview.StopNoticeBox

        Me.gvwNotes.DataSource = dsjobinfo.Tables(1)
        Me.gvwNotes.DataBind()

            If Not Me.gvwNotes.Rows.Count = 0 Then
                Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Me.gvwLegalParties.DataSource = dsjobinfo.Tables(3)
            Me.gvwLegalParties.DataBind()
            If Not Me.gvwLegalParties.Rows.Count = 0 Then
                Me.gvwLegalParties.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncLegalPartiesList", "CallSuccessFuncLegalPartiesList();", True)
            Me.gvwNotices.DataSource = dsjobinfo.Tables(2)
            Me.gvwNotices.DataBind()
            If Not Me.gvwNotices.Rows.Count = 0 Then
                Me.gvwNotices.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Me.gvwWaivers.DataSource = dsjobinfo.Tables(8)
            Me.gvwWaivers.DataBind()
            If Not Me.gvwWaivers.Rows.Count = 0 Then
                Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Me.gvwStateForms.DataSource = dsjobinfo.Tables(11)
            Me.gvwStateForms.DataBind()
            If Not Me.gvwStateForms.Rows.Count = 0 Then
                Me.gvwStateForms.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Me.gvwVReq.DataSource = dsjobinfo.Tables(15)
            Me.gvwVReq.DataBind()
            If Not Me.gvwVReq.Rows.Count = 0 Then
                Me.gvwVReq.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Me.gvwTexasReq.DataSource = dsjobinfo.Tables(10)
            Me.gvwTexasReq.DataBind()
            If Not Me.gvwTexasReq.Rows.Count = 0 Then
                Me.gvwTexasReq.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Me.gvwINVC.DataSource = dsjobinfo.Tables(14)
            Me.gvwINVC.DataBind()
            If Not Me.gvwINVC.Rows.Count = 0 Then
                Me.gvwINVC.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
            MYSQL += " Where JobId = " & myview.JobId
            MYSQL += "ORDER BY Id DESC"
            Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
            Me.gvwDocs.DataSource = MYDT
            Me.gvwDocs.DataBind()
            If Not Me.gvwDocs.Rows.Count = 0 Then
                Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
            End If



        Me.panJobInfo.Visible = True

        Me.litJobInfo.Text = " CRF# (" & myview.ClientCode & "-" & Me.JobId.Text.Trim & ") "
        Me.litJobInfo.Text += " Job# (" & Me.JobNumber.Text.Trim & ") "
        Me.litJobInfo.Text += " JobName (" & Me.JobName.Text.Trim & ") "


        'Me.TabPanel7.Visible = False
        'Me.TabPanel8.Visible = False
        'Me.TabPanel9.Visible = False
        NoticeRequest.Style.Add("display", "none")
        TXRequests.Style.Add("display", "none")
        Invoices.Style.Add("display", "none")

        Dim NONTO As Boolean = dsjobinfo.Tables(4).Rows(0).Item("NoNTO")
        If NONTO = False Then
            ' If (myview.VerifiedDate > "01/01/1980" And myview.IsNoticeRequestStatus = True And Me.JobState.Value <> "TX" And Me.JobState.Value <> "TN") Then
            'NoticeRequest.Style.Add("display", "block")
            'End If


            'If (Me.JobState.Value = "TN" And myview.IsNoticeRequestStatus = True And myview.VerifiedDate > "01/01/1980") Then
            '    NoticeRequest.Style.Add("display", "block")
            'End If

            '^^*** commented by pooja 10/09/2020*** ^^
            'If (Me.JobState.Value = "TN") Then
            '    NoticeRequest.Style.Add("display", "block")
            'End If
            '^^
            '^^*** Created by pooja 12/03/2020*** ^^***  *** ^^
            If (myview.VerifiedDate > "01/01/1980" And myview.IsNoticeRequestStatus = True And Me.JobState.Value <> "TX" And Me.JobState.Value <> "TN") Then
                NoticeRequest.Style.Add("display", "block")

            ElseIf (Me.JobState.Value = "TN" And (myview.VerifiedDate > "01/01/1980" Or myview.NoticeSent > "01/01/1980")) Then
                NoticeRequest.Style.Add("display", "block")

            ElseIf (Me.JobState.Value = "TX" And myview.IsNoticeRequestStatus = True) Then
                TXRequests.Style.Add("display", "block")
                NoticeRequest.Style.Add("display", "none")
            Else
                NoticeRequest.Style.Add("display", "block")
                btnAddVReqClient.Enabled = False
                btnAddVReq.Enabled = False
                btnAddVReqClient.OnClientClick = False

            End If
            '^^***  *** ^^
        End If

        Me.litData1.Text = ""
        Me.litData2.Text = ""
        Me.litLabel1.Text = ""
        Me.litlabel2.Text = ""

        '^^*** commented by pooja 11/18/2020*** ^^
        'If Me.JobState.Value <> "AZ" Then
        '    'Me.TabPanel9.Visible = False
        '    Invoices.Style.Add("display", "none")
        '    divlabel2.Style.Add("display", "none")
        'Else
        '    Invoices.Style.Add("display", "block")
        '    divlabel2.Style.Add("display", "block")
        '    'Me.TabPanel9.Visible = True
        '    Me.litLabel1.Text = "Invoice Totals:"
        '    Me.litData1.Text = FormatCurrency(GetInvoiceTotal())
        '    Me.litlabel2.Text = "Percent:"
        '    Me.litData2.Text = GetInvoicePercent()

        'End If

        '^^***  *** ^^

        If Me.JobState.Value = "AZ" Then
            Me.panAZLotAllocate.Visible = True
        Else
            Me.panAZLotAllocate.Visible = False
        End If

        'If Me.UserInfo.Client.IsRentalCo = True Then
        '    Me.panRentalINfo.Visible = True
        'Else
        '    Me.panRentalINfo.Visible = False
        'End If

        Me.JobAddNote1.ClearData(ajobid)
        Me.JobEdit1.ClearData(ajobid)


        SetCurrentTab()

        Session("exitviewer") = "0"

        Catch ex As Exception
            Dim mymsg As String = ex.Message
        End Try

    End Sub
    Private Function GetInvoiceTotal() As Decimal
        Dim myamt As Decimal = 0
        Dim dr As System.Data.DataRow
        If dsjobinfo.Tables(14).Rows.Count > 0 Then
            For Each dr In dsjobinfo.Tables(14).Rows
                myamt += dr("amountowed")
            Next
        End If

        Return myamt

    End Function
    Private Function GetInvoicePercent() As String
        Dim myamt As Decimal = GetInvoiceTotal()
        Dim mytot As Decimal = Me.EstBalance.Value
        Dim myrate As String = Math.Floor(myamt / mytot * 100).ToString
        Return myrate & "%"
    End Function
    'Private Sub SetCurrentTab()
    '    If Session("exitviewer") = "1" Then

    '        Me.TabContainer.ActiveTab = Me.tabPanel1
    '        If Me.ViewState("VIEW") = "WAIVER" Then
    '            Me.TabContainer.ActiveTab = Me.TabPanel5
    '            TabContainer.ActiveTabIndex = 5
    '        End If
    '        If Me.ViewState("VIEW") = "VREQ" Then
    '            Me.TabContainer.ActiveTabIndex = 6
    '            Me.TabContainer.TabIndex = 6
    '        End If

    '        If Me.ViewState("VIEW") = "TREQ" Then
    '            Me.TabContainer.ActiveTabIndex = 7
    '            Me.TabContainer.TabIndex = 7
    '        End If

    '        If Me.ViewState("VIEW") = "INVC" Then
    '            Me.TabContainer.ActiveTabIndex = 8
    '            Me.TabContainer.TabIndex = 8
    '        End If

    '        If Me.ViewState("VIEW") = "SFORM" Then
    '            Me.TabContainer.ActiveTab = Me.TabPanel6
    '            TabContainer.ActiveTabIndex = 6
    '        End If
    '        If Me.ViewState("VIEW") = "NOTICE" Then
    '            Me.TabContainer.ActiveTab = Me.TabPanel4
    '        End If
    '        If Me.ViewState("VIEW") = "NOTES" Then
    '            Me.TabContainer.ActiveTab = Me.TabPanel3
    '        End If
    '        If Me.ViewState("VIEW") = "Notices Sent" Then
    '            TabContainer.ActiveTabIndex = 3
    '        End If
    '        If Me.ViewState("VIEW") = "Docs" Then
    '            TabContainer.ActiveTabIndex = 4
    '        End If
    '    End If

    'End Sub
    '


    'navigation issues  1/2/2012
    Private Sub WaiverTab()

        If Me.Session("WaiverTab") = "WAIVER" Then
            'Me.TabContainer.ActiveTab = Me.TabPanel5
            'TabContainer.ActiveTabIndex = 5

            hddbTabName.Value = "Waivers"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaa", "ActivateTab('" & hddbTabName.Value & "');", True)
            Me.Session("WaiverTab") = Nothing

        End If
    End Sub

    Private Sub SFORMTab()
        If Me.Session("WaiverTab") = "SFORM" Then
            'Me.TabContainer.ActiveTab = Me.TabPanel6
            'TabContainer.ActiveTabIndex = 6
            hddbTabName.Value = "StateForms"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListable", "ActivateTab('" & hddbTabName.Value & "');", True)
            Me.Session("WaiverTab") = Nothing
        End If
    End Sub
    'end navigation issues 
    Private Sub SetCurrentTab()

        If Session("exitviewer") = "1" Then

            If Session("currenttab") = "WAIVER" Then
                'Me.TabContainer.ActiveTab = Me.TabPanel5
                'TabContainer.ActiveTabIndex = 5
                hddbTabName.Value = "Waivers"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaaa", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "SFORM" Then
                'Me.TabContainer.ActiveTab = Me.TabPanel6
                'TabContainer.ActiveTabIndex = 6
                hddbTabName.Value = "StateForms"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaLi", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "NOTES" Then
                'Me.TabContainer.ActiveTab = Me.TabPanel2
                'TabContainer.ActiveTabIndex = 2
                hddbTabName.Value = "Notes"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaLis", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "Notices Sent" Then
                'TabContainer.ActiveTabIndex = 3
                hddbTabName.Value = "NoticesSent"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaLists", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "Docs" Then
                'TabContainer.ActiveTabIndex = 4
                hddbTabName.Value = "Docs"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListData", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "TREQ" Then
                'TabContainer.ActiveTabIndex = 3
                hddbTabName.Value = "TXRequests"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListss", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "INVC" Then
                'TabContainer.ActiveTabIndex = 3
                hddbTabName.Value = "Invoices"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListss", "ActivateTab('" & hddbTabName.Value & "');", True)
            End If

        Else
            'hddbTabName.Value = "MainInfo"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListSet", "ActivateTab('" & hddbTabName.Value & "');", True)
        End If




        'Else
        ''TabContainer.ActiveTabIndex = 0
        ''hddbTabName.Value = "MainInfo"
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListSet", "ActivateTab('" & hddbTabName.Value & "');", True)
        'End If

    End Sub

#Region "Notices"
    Protected Sub gvwNotices_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotices.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntNotice = TryCast(e.Row.FindControl("btnPrntNotice"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntNotice"), LinkButton).Click, AddressOf btnPrntNotice_Click
        End If

        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    btnlnk = TryCast(e.Row.FindControl("lnk"), LinkButton)
        '    AddHandler CType(e.Row.FindControl("lnk"), LinkButton).Click, AddressOf btnlnk_Click
        'End If
    End Sub
    Protected Sub gvwNotices_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotices.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntNotice = TryCast(e.Row.FindControl("btnPrntNotice"), LinkButton)
            btnPrntNotice.Attributes("rowno") = DR("JobNoticeHistoryId")
            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DatePrinted")) & ""

        End If

        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    ' Get the database row bound to this Grid row...
        '    Dim dataRowView As System.Data.DataRowView = DirectCast(e.Row.DataItem, System.Data.DataRowView)

        '    Dim idValue As String = dataRowView("CertNum").ToString()

        '    'Dim textBoxId As String = TextBox1.ClientID

        '    Dim gridLinkButton As LinkButton = DirectCast(e.Row.FindControl("lnk"), LinkButton)
        '    If gridLinkButton IsNot Nothing Then
        '        Dim eventHandler As String = String.Format("return openWin('../../../App_Controls/_Custom/LienView/TrackAndConfirm.aspx?CertNum={0}');", idValue)
        '        gridLinkButton.Attributes.Add("onclick", eventHandler)
        '        gridLinkButton.Text = idValue
        '    End If
        'End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Get the database row bound to this Grid row...
            Dim dataRowView As System.Data.DataRowView = DirectCast(e.Row.DataItem, System.Data.DataRowView)

            Dim idValue As String = dataRowView("CertNum").ToString()

            'Dim textBoxId As String = TextBox1.ClientID

            Dim gridHyperLink As HyperLink = DirectCast(e.Row.FindControl("hyperlnk"), HyperLink)
            If gridHyperLink IsNot Nothing Then
                Dim eventHandler As String = String.Format("javascript:return openWin('{0}');", idValue)
                gridHyperLink.Attributes.Add("onclick", eventHandler)
                gridHyperLink.Text = idValue
            End If
        End If
    End Sub
    Protected Sub btnPrntNotice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntNotice.Click
        btnPrntNotice = TryCast(sender, LinkButton)
        Session("currenttab") = "Notices Sent"
        SetCurrentTab()

        Dim sreport As String = GetNoticeCopy(btnPrntNotice.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False AndAlso sreport = "NotExists" Then
            'If sreport = "NotExists" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "AlertMessage", "alert('This notice copy has been archived due to age, or was just created today, and is not yet available online. Please contact us at 805-823-8032 and we can send a copy of the notice to you.')", True)
            'TabContainer.ActiveTabIndex = 3
            hddbTabName.Value = "NoticesSent"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsGens", "DisplayTablelayout();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsGen", "ActivateTab('" & hddbTabName.Value & "');", True)
        ElseIf IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    Filepath = Replace(Filepath, "'", "~")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
    Public Function GetNoticeCopy(ByVal ANOTICEID As String) As String

        Dim myfilename As String = ""
        myfilename = ""
        myfilename = Me.JobId.Value
        myfilename += "-" & ANOTICEID & ".PDF"

        'Dim mydest As String = CRFVIEW.COMMON.GetKeyValue("JOBNOTICEIMAGEPATH") & myfilename
        Dim mydest As String = "E:\JOBNOTICEIMAGES\" & myfilename
        Dim mydest1 As String = Me.MyPage.MapPath("~/UserData/Output/" & myfilename)
        If System.IO.File.Exists(mydest1) = True Then
            System.IO.File.Delete(mydest1)
        End If

        If System.IO.File.Exists(mydest) = True Then
            System.IO.File.Copy(mydest, mydest1, True)
            System.Threading.Thread.Sleep(300)
            If System.IO.File.Exists(mydest1) = True Then
                Return mydest1
            End If
        Else
            Return "NotExists"

        End If

        Return LienView.Provider.PrintPrelimNotice(btnPrntNotice.Attributes("rowno"))

    End Function
#End Region
#Region "Waivers"
    Protected Sub btnAddWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWaiver.Click
        Dim s As String = "~/MainDefault.aspx?lienview.jobeditwaiver&itemid=" & Me.itemid & "&subid=" & -1
        Response.Redirect(Me.Page.ResolveUrl(s), True)


    End Sub
    Protected Sub gvwWaivers_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwWaivers.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntWaiver = TryCast(e.Row.FindControl("btnPrntWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntWaiver"), LinkButton).Click, AddressOf btnPrntWaiver_Click
            btnEditWaiver = TryCast(e.Row.FindControl("btnEditWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditWaiver"), LinkButton).Click, AddressOf btnEditWaiver_Click
            btnDeleteWaiver = TryCast(e.Row.FindControl("btnDeleteWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteWaiver"), LinkButton).Click, AddressOf btnDeleteWaiver_Click
            btnEmailWaiver = TryCast(e.Row.FindControl("btnEmailWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEmailWaiver"), LinkButton).Click, AddressOf btnEmailWaiver_Click



        End If
    End Sub
    Protected Sub gvwWaivers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwWaivers.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntWaiver = TryCast(e.Row.FindControl("btnPrntWaiver"), LinkButton)
            btnPrntWaiver.Attributes("rowno") = DR("JobWaiverLogId")
            btnEditWaiver = TryCast(e.Row.FindControl("btnEditWaiver"), LinkButton)
            btnEditWaiver.Attributes("rowno") = DR("JobWaiverLogId")
            btnDeleteWaiver = TryCast(e.Row.FindControl("btnDeleteWaiver"), LinkButton)
            btnDeleteWaiver.Attributes("rowno") = DR("JobWaiverLogId")
            btnEmailWaiver = TryCast(e.Row.FindControl("btnEmailWaiver"), LinkButton)
            btnEmailWaiver.Attributes("rowno") = DR("JobWaiverLogId")



            e.Row.Cells(2).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            e.Row.Cells(3).Text = "" & Utils.FormatDate(DR("DatePrinted")) & ""

            If Me.CurrentUser.UserManager = 0 Then
                btnPrntWaiver.Visible = False
                btnEditWaiver.Visible = False
                btnDeleteWaiver.Visible = False
                btnEmailWaiver.Visible = False
                'gvwWaivers.Columns(0).Visible = False
                'gvwWaivers.Columns(6).Visible = False
            End If
        End If

    End Sub
    Public Sub checkpayor()
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(btnPrntWaiver.Attributes("rowno"), mywaiver)
        If mywaiver.Payor = "" Then
            Dim myview As HDS.DAL.COMMON.TableView
            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
            MYSPROC.JobId = Me.itemid
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
            Dim strquery As String = ""
            If (mywaiver.CheckbyCU) Then
                strquery = "TypeCode='CU'"
            ElseIf (mywaiver.CheckByGC) Then
                strquery = "TypeCode='GC'"
            ElseIf (mywaiver.CheckbyLE) Then
                strquery = "TypeCode='LE'"
            ElseIf (mywaiver.CheckByOT) Then
                strquery = "TypeCode='OT'"
            ElseIf (mywaiver.CheckbyOW) Then
                strquery = "TypeCode='OW'"
            End If
            myview.RowFilter = strquery
                Dim GetName As String = myview.RowItem("AddressName")
                mywaiver.Payor = GetName
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(mywaiver)
            End If
    End Sub
    Protected Sub btnPrntWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntWaiver.Click
        btnPrntWaiver = TryCast(sender, LinkButton)
        Session("currenttab") = "WAIVER"
        SetCurrentTab()
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(btnPrntWaiver.Attributes("rowno"), mywaiver)
        If mywaiver.Payor = "" Then
            checkpayor()
        End If
        Dim sreport As String = LienView.Provider.PrintJobWaiver(btnPrntWaiver.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(btnPrntWaiver.Attributes("rowno"), mywaiver)
            With mywaiver
                If (.Notary = True) Then
                    PrintNotary(btnPrntWaiver.Attributes("rowno"))
                    MergeNotary(sreport)
                End If

            End With
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    Filepath = Replace(Filepath, "'", "~")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
    Public Sub PrintNotary(ByVal JobWaiverLogId As String)

        Dim sreport As String = LienView.Provider.PrintJobNotary(JobWaiverLogId)

        If IsNothing(sreport) = False Then

            s1 = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))

        End If

    End Sub
    Public Sub MergeNotary(ByVal sreport As String)
        Dim s2 As String = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
        Dim path As String = ConfigurationManager.AppSettings("path").ToString()
        File.Create(path & (System.IO.Path.GetFileName(sreport))).Close()
        Dim notary As String

        notary = s1

        Dim result As String = path & (System.IO.Path.GetFileName(sreport))
        Dim source_files() As String = {s2, notary}
        Dim document As iTextSharp.text.Document = New iTextSharp.text.Document
        Dim copy As iTextSharp.text.pdf.PdfCopy = New iTextSharp.text.pdf.PdfCopy(document, New FileStream(result, FileMode.Create))
        'open the document
        document.Open()
        Dim reader As iTextSharp.text.pdf.PdfReader
        Dim i As Integer = 0
        Do While (i < source_files.Length)
            'create PdfReader object
            reader = New iTextSharp.text.pdf.PdfReader(source_files(i))
            'merge combine pages
            Dim page As Integer = 1
            Do While (page <= reader.NumberOfPages)
                copy.AddPage(copy.GetImportedPage(reader, page))
                page = (page + 1)
            Loop

            i = (i + 1)
        Loop

        'close the document object
        document.Close()
        '-------------------------------------------------------------
        File.Copy(result, sreport, True)
        File.Delete(result)

    End Sub
    Private Sub PrintWaiver(ByVal itemid As String)

        Me.ViewState("VIEW") = "WAIVER"
        SetCurrentTab()
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(itemid, mywaiver)
        If mywaiver.Payor = "" Then
            checkpayor()
        End If
        Dim sreport As String = LienView.Provider.PrintJobWaiver(itemid)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub

    Protected Sub btnEmailWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailWaiver.Click
        btnEmailWaiver = TryCast(sender, LinkButton)
        Session("currenttab") = "WAIVER"
        SetCurrentTab()

        sremail = btnEmailWaiver.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(sremail, mywaiver)
        Me.txtToList.Text = mywaiver.EmailTo
        JobAddSubjectLine = ViewState("JobAddSubjectLine").ToString
        Dim EmailUserName As String = Me.UserInfo.UserInfo.UserName
        Dim EmailUserEmail As String = Me.UserInfo.UserInfo.Email

        ViewState("JobWaiverLogId") = sremail
        If (JobAddSubjectLine.Trim() <> "") Then
            txtSubject.Text = "Waiver Document for " + JobAddSubjectLine

        Else
            txtSubject.Text = "Waiver Document for "
        End If
        Me.txtBody.Text = "PLEASE DO NOT RESPOND TO THIS EMAIL." + vbCrLf + "If you have any questions, or need any assistance, please contact " + EmailUserName + " at " + EmailUserEmail + "."
        hdnEmailflag.Value = "W"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayEmailPopUp", "DisplayEmailPopUp();", True)


    End Sub

    Protected Sub btnEmailsend_Click(sender As Object, e As EventArgs)
        'sremail = ViewState("JobWaiverLogId").ToString
        'If Me.txtToList.Text.Trim() <> "" Then
        '    EmailWaiver(sremail)
        'End If

        If hdnEmailflag.Value = "W" Then
            sremail = ViewState("JobWaiverLogId").ToString
            EmailWaiver(sremail)
        ElseIf hdnEmailflag.Value = "RL" Then
            sremail = ViewState("JobWaiverLogId").ToString
            RecensionLetterEmail(sremail)
        End If


    End Sub
    Public Sub EmailWaiver(ByVal JobWaiverLogId As String)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobWaiverLogId, mywaiver)
        If mywaiver.Payor = "" Then
            checkpayor()
        End If
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(JobWaiverLogId)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobWaiverLogId, mywaiver)
            With mywaiver
                If (.Notary = True) Then
                    PrintNotary(JobWaiverLogId)
                    MergeNotary(sreport)
                End If

            End With
            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            Dim FlagError = EmailToUser(txtToList.Text, txtSubject.Text, txtBody.Text, Filename, fileArray) 'For a timing

            If FlagError = "0" Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EmailSendPopup", "EmailSendPopup();", True)

            End If
        End If

    End Sub
    Public Shared Function GetSettings() As TABLES.Report_Settings
        Dim myitem As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, myitem)

        Return myitem

    End Function
    Public Shared Function EmailToUser(ByVal email As String, ByVal subject As String, ByVal strBody As String, ByVal FileName As String, ByVal pdfcontent As Byte())

        Dim FlagError As String = "0"

        Dim ToArrayList As String()
        ToArrayList = email.Split(";")

        Dim MYSITE As TABLES.Report_Settings = GetSettings()

        Dim msg As New MailMessage
        '  msg.From = New MailAddress("shishir.peeru@rhealtech.com", "ClientView")
        msg.From = New MailAddress("CRFIT@crfsolutions.com", "Job Waiver Central")
        msg.Subject = subject
        For index = 0 To ToArrayList.Length - 1
            msg.To.Add(ToArrayList(index))
        Next
        msg.IsBodyHtml = True
        msg.Body = strBody
        msg.Attachments.Add(New Attachment(New MemoryStream(pdfcontent), FileName))

        'msg.Body = body

        Try
            With MYSITE
                Dim _smtp As New System.Net.Mail.SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
            End With
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            MsgBox("error on email:" & vbCrLf & mymsg)
            FlagError = "1"
        End Try
        Return FlagError
    End Function


    Protected Sub btnEditWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditWaiver.Click
        Session("currenttab") = "WAIVER"
        btnEditWaiver = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?lienview.jobeditwaiver&itemid=" & Me.itemid & "&subid=" & btnEditWaiver.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnDeleteWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteWaiver.Click
        'Me.ViewState("VIEW") = "WAIVER"
        Session("currenttab") = "WAIVER"
        Session("exitviewer") = "1"
        SetCurrentTab()

        btnDeleteWaiver = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobWaiverLog
        myitem.JobWaiverLogId = btnDeleteWaiver.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)

    End Sub
#End Region
#Region "Verified Requests"
    Protected Sub btnAddVREQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddVReq.Click
        Dim s As String
        If (Me.JobState.Value = "TN") Then
            s = "~/MainDefault.aspx?lienview.jobeditvreq&itemid=" & Me.itemid & "&subid=" & -1 & "&state=TN"
        Else
            s = "~/MainDefault.aspx?lienview.jobeditvreq&itemid=" & Me.itemid & "&subid=" & -1 & "&state=OT"
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwVReq_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwVReq.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            Me.btnEditVReq = TryCast(e.Row.FindControl("btnEditVReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditVReq"), LinkButton).Click, AddressOf btnEditVReq_Click
            btnDeleteVReq = TryCast(e.Row.FindControl("btnDeleteVReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteVReq"), LinkButton).Click, AddressOf btnDeleteVREQ_Click

        End If
    End Sub
    Protected Sub gvwVReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwVReq.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditVReq = TryCast(e.Row.FindControl("btnEditVReq"), LinkButton)
            btnEditVReq.Attributes("rowno") = DR("Id")

            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteVReq"), LinkButton)
            btnDeleteTReq.Attributes("rowno") = DR("Id")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
        End If

    End Sub
    Protected Sub btnEditVReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditVReq.Click
        btnEditVReq = TryCast(sender, LinkButton)
        Dim s As String
        If (Me.JobState.Value = "TN") Then
            s = "~/MainDefault.aspx?lienview.jobeditvreq&itemid=" & Me.itemid & "&subid=" & btnEditVReq.Attributes("rowno") & "&state=TN"
        Else
            s = "~/MainDefault.aspx?lienview.jobeditvreq&itemid=" & Me.itemid & "&subid=" & btnEditVReq.Attributes("rowno") & "&state=OT"
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnDeleteVREQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteINVC.Click
        Me.ViewState("VIEW") = "VREQ"
        SetCurrentTab()

        btnDeleteVReq = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobVerifiedSentRequest
        myitem.Id = btnDeleteVReq.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)

    End Sub
#End Region
#Region "Texas Requests"
    Protected Sub btnAddTXStmt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTXStmt.Click
        Dim s As String = "~/MainDefault.aspx?lienview.jobedittreq&itemid=" & Me.itemid & "&subid=" & -1
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwTexasReq_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwTexasReq.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntTReq"), LinkButton).Click, AddressOf btnPrntTReq_Click
            Me.btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditTReq"), LinkButton).Click, AddressOf btnEditTReq_Click
            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteTReq"), LinkButton).Click, AddressOf btnDeleteTReq_Click

        End If
    End Sub
    Protected Sub gvwTexasReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            btnEditTReq.Attributes("rowno") = DR("Id")

            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            btnPrntTReq.Attributes("rowno") = DR("Id")

            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            btnDeleteTReq.Attributes("rowno") = DR("Id")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
        End If

    End Sub
    Protected Sub btnEditTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditTReq.Click
        btnEditTReq = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?lienview.jobedittreq&itemid=" & Me.itemid & "&subid=" & btnEditTReq.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnPrntTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntTReq.Click
        btnPrntWaiver = TryCast(sender, LinkButton)
        Me.ViewState("VIEW") = "TREQ"
        Session("currenttab") = "TREQ"
        SetCurrentTab()

        Dim sreport As String = LienView.Provider.GetTexasRequestAck(btnPrntWaiver.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    Filepath = Replace(Filepath, "'", "~")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub btnDeleteTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteTReq.Click
        Me.ViewState("VIEW") = "TREQ"
        SetCurrentTab()

        btnDeleteTReq = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobTexasRequest
        myitem.Id = btnDeleteTReq.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)
        If Me.ViewState("VIEW") = "TREQ" Then
            'Me.TabContainer.ActiveTabIndex = 7
            'Me.TabContainer.TabIndex = 7
            hddbTabName.Value = "NoticeRequest"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsDta", "ActivateTab('" & hddbTabName.Value & "');", True)
        End If
    End Sub
#End Region
#Region "Invoice"
    Protected Sub btnAddNewINVCE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewINVC.Click
        Dim s As String = "~/MainDefault.aspx?lienview.jobeditInvoice&itemid=" & Me.itemid & "&subid=" & -1
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwINVC_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwINVC.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then

            'btnPrntINVC = TryCast(e.Row.FindControl("btnPrntINVC"), ImageButton)
            'AddHandler CType(e.Row.FindControl("btnPrntINVC"), ImageButton).Click, AddressOf btnPrntINVC_Click

            Me.btnEditINVC = TryCast(e.Row.FindControl("btnEditINVC"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditINVC"), LinkButton).Click, AddressOf btnEditINVC_Click

            btnDeleteINVC = TryCast(e.Row.FindControl("btnDeleteINVC"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteINVC"), LinkButton).Click, AddressOf btnDeleteINVC_Click

        End If
    End Sub
    Protected Sub gvwINVC_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditINVC = TryCast(e.Row.FindControl("btnEditINVC"), LinkButton)
            btnEditINVC.Attributes("rowno") = DR("Id")

            'btnPrntINVC = TryCast(e.Row.FindControl("btnPrntINVC"), LinkButton)
            'btnPrntINVC.Attributes("rowno") = DR("Id")

            btnDeleteINVC = TryCast(e.Row.FindControl("btnDeleteINVC"), LinkButton)
            btnDeleteINVC.Attributes("rowno") = DR("Id")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""

            e.Row.Cells(3).Text = Strings.FormatCurrency(e.Row.Cells(3).Text)
            e.Row.Cells(4).Text = Strings.FormatCurrency(e.Row.Cells(4).Text)
            e.Row.Cells(5).Text = Strings.FormatCurrency(e.Row.Cells(5).Text)
            e.Row.Cells(6).Text = Strings.FormatCurrency(e.Row.Cells(6).Text)

        End If

    End Sub
    Protected Sub btnEditINVC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditINVC.Click
        btnEditINVC = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?lienview.jobeditInvoice&itemid=" & Me.itemid & "&subid=" & btnEditINVC.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnDeleteINVC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteINVC.Click
        Me.ViewState("VIEW") = "INVC"
        SetCurrentTab()

        btnDeleteINVC = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobInvoices
        myitem.Id = btnDeleteINVC.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)

    End Sub
    Protected Sub btnPrintINVC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintINVC.Click
        btnPrntWaiver = TryCast(sender, LinkButton)
        Me.ViewState("VIEW") = "INVC"
        Session("currenttab") = "INVC"
        SetCurrentTab()

        Dim sreport As String = LienView.Provider.GetInvoiceReport(Me.JobId.Value)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
#End Region
#Region "Legal Parties"
    Protected Sub gvwLegalParties_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwLegalParties.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnRecension = TryCast(e.Row.FindControl("btnRecension"), LinkButton)
            btnRecension.Attributes("rowno") = DR("Id") & "-" & e.Row.Cells(0).Text.ToUpper

            Select Case e.Row.Cells(0).Text.ToUpper
                Case "CU"
                    e.Row.Cells(0).Text = "Customer"
                Case "GC"
                    e.Row.Cells(0).Text = "General Contractor"
                Case "OW"
                    e.Row.Cells(0).Text = "Owner"
                Case "LE"
                    e.Row.Cells(0).Text = "Lender/Surety"

            End Select
            e.Row.Cells(1).Text = "" & Strings.Left(e.Row.Cells(1).Text, 25) & ""
            e.Row.Cells(2).Text = "" & Strings.Left(e.Row.Cells(2).Text, 20) & ""
            e.Row.Cells(3).Text = "" & Strings.Left(e.Row.Cells(3).Text, 25) & ""
            e.Row.Cells(4).Text = "" & Strings.Left(e.Row.Cells(4).Text, 25) & ""
            e.Row.Cells(5).Text = "" & Strings.Left(e.Row.Cells(5).Text, 15) & ""
            e.Row.Cells(6).Text = "" & Utils.FormatPhoneNo(e.Row.Cells(6).Text) & ""

        End If

    End Sub




    Protected Sub gvwLegalParties_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwLegalParties.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnRecension = TryCast(e.Row.FindControl("btnRecension"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnRecension"), LinkButton).Click, AddressOf btnRecension_Click
        End If
    End Sub

    Protected Sub btnRecension_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecension.Click
        btnRecension = TryCast(sender, LinkButton)
        Dim btnRecensionDetails As String() = Strings.Split(btnRecension.Attributes("rowno"), "-")
        LegalPartyId = btnRecensionDetails(0)
        JobLegalPartiesTypeCode = btnRecensionDetails(1)

        hdnLegalPartyId.Value = LegalPartyId
        hdnJobLegalPartiesTypeCode.Value = JobLegalPartiesTypeCode
        myJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetails(LegalPartyId, JobLegalPartiesTypeCode)

        myStateForms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateForms(StateCode)

        dsJobWaiverLogByInfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByInfo(itemid, myStateForms.Id, myJobLegalParties.AddressName)

        dsJobWaiverLogByRecensionLetter = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByRecensionLetter(itemid, myStateForms.Id, myJobLegalParties.AddressName)

        If (dsJobWaiverLogByRecensionLetter.Rows.Count > 0) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CreateAnotherRecensionLetter", "CreateAnotherRecensionLetter('" + myJobLegalParties.AddressName + "');", True)
        Else
            CreateRecensionLetter()
        End If

    End Sub

    Protected Sub btnCreateAnotherRecensionLetter_Click(sender As Object, e As EventArgs)

        CreateRecensionLetter()

    End Sub


    Public Sub CreateRecensionLetter()
        Try
            LegalPartyId = hdnLegalPartyId.Value
            JobLegalPartiesTypeCode = hdnJobLegalPartiesTypeCode.Value

            myJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetails(LegalPartyId, JobLegalPartiesTypeCode)

            myStateForms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateForms(StateCode)

            dsJobWaiverLogByInfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByInfo(itemid, myStateForms.Id, myJobLegalParties.AddressName)

            dsJobWaiverLogByRecensionLetter = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByRecensionLetter(itemid, myStateForms.Id, myJobLegalParties.AddressName)

            If (myStateForms.Id > 0) Then
                If (Not (myJobLegalParties.AddressName Is Nothing Or myJobLegalParties.AddressName = "")) Then
                    With mywaiver
                        .JobId = itemid
                        .FormId = myStateForms.Id
                        .FormCode = Strings.Left((myStateForms.FormCode + "-" + myStateForms.Description), 50)
                        .DateCreated = Today
                        .DatePrinted = Today
                        .RequestedBy = Left(Me.UserInfo.UserInfo.LoginCode, 10)
                        .RequestedByUserId = Me.CurrentUser.Id
                        .MailToName = myJobLegalParties.AddressName
                        .MailToAddr1 = myJobLegalParties.AddressLine1
                        .MailToCity = myJobLegalParties.City
                        .MailToState = myJobLegalParties.State
                        .MailToZip = myJobLegalParties.PostalCode

                        If (myJobLegalParties.TypeCode = "CU") Then
                            .MailtoCU = True
                            .MailtoGC = False
                            .MailtoLE = False
                            .MailtoOW = False
                        ElseIf (myJobLegalParties.TypeCode = "GC") Then
                            .MailtoGC = True
                            .MailtoCU = False
                            .MailtoLE = False
                            .MailtoOW = False
                        ElseIf (myJobLegalParties.TypeCode = "LE") Then
                            .MailtoLE = True
                            .MailtoGC = False
                            .MailtoCU = False
                            .MailtoOW = False
                        ElseIf (myJobLegalParties.TypeCode = "OW") Then
                            .MailtoOW = True
                            .MailtoLE = False
                            .MailtoGC = False
                            .MailtoCU = False
                        Else
                            .MailtoOW = False
                            .MailtoLE = False
                            .MailtoGC = False
                            .MailtoCU = False
                        End If
                    End With
                    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(mywaiver)
                    'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RecensionLetterCreatedPopup", "RecensionLetterCreatedPopup('" + myJobLegalParties.AddressName + "');", True)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showRecensionLetterCreatedPopup", "showRecensionLetterCreatedPopup('" + myJobLegalParties.AddressName + "');", True)
                    ViewState("JobWaiverLogId") = mywaiver.JobWaiverLogId
                Else
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ExitLegalParties", "ExitLegalParties();", True)
                End If
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RecensionLetterNotAvailableForLegalParties", "RecensionLetterNotAvailableForLegalParties('" + myJobLegalParties.AddressName + "');", True)
            End If


        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnPrintRecensionLetter_Click(sender As Object, e As EventArgs)
        PrintRecensionLetter()
    End Sub
    Public Sub PrintRecensionLetter()
        Dim btnRecensionDetails As String() = Strings.Split(btnRecension.Attributes("rowno"), "-")
        Dim LegalPartyId As String = btnRecensionDetails(0)
        Dim JobLegalPartiesTypeCode As String = btnRecensionDetails(1)

        myJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetails(LegalPartyId, JobLegalPartiesTypeCode)

        JobWaiverLogId = ViewState("JobWaiverLogId").ToString
        Dim sreport As String = LienView.Provider.PrintJobWaiver(JobWaiverLogId)

        If IsNothing(sreport) = False Then

            Dim s1 As String = Me.ResolveUrl("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", s1, 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)

                End If
            End If
            'Dim URL As String = "MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + URL + "'", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showRecensionLetterCreatedPopup", "showRecensionLetterCreatedPopup('" + myJobLegalParties.AddressName + "');", True)
        End If

    End Sub

    Protected Sub btnEmailRecensionLetter_Click(sender As Object, e As EventArgs)

        LegalPartyId = hdnLegalPartyId.Value
        JobLegalPartiesTypeCode = hdnJobLegalPartiesTypeCode.Value

        dsJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetailsDataTable(LegalPartyId, JobLegalPartiesTypeCode)

        Dim EmailUserName As String = Me.UserInfo.UserInfo.UserName
        Dim EmailUserEmail As String = Me.UserInfo.UserInfo.Email

        If dsJobLegalParties.Rows.Count <> 0 Then
            Me.txtToListForRecensionLetter.Text = Convert.ToString(dsJobLegalParties.Rows(0)("Email"))

        Else
            Me.txtToListForRecensionLetter.Text = " "
        End If

        JobAddSubjectLine = ViewState("JobAddSubjectLine").ToString

        If (JobAddSubjectLine.Trim() <> "") Then
            txtRecensionLetterSubject.Text = "Recension Letter for " + JobAddSubjectLine
        Else
            txtRecensionLetterSubject.Text = "Recension Letter for "
        End If

        Me.txtRecensionLetterBody.Text = "PLEASE DO NOT RESPOND TO THIS EMAIL." + vbCrLf + "If you have any questions, or need any assistance, please contact " + EmailUserName + " at " + EmailUserEmail + "."
        hdnEmailflag.Value = "RL"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideRecensionLetterPopUp", "HideRecensionLetterPopUp();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayEmailRecensionLetterPopUp", "DisplayEmailRecensionLetterPopUp();", True)

    End Sub

    Public Sub RecensionLetterEmail(ByVal JobWaiverLogId As String)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(JobWaiverLogId)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobWaiverLogId, mywaiver)
            With mywaiver
                If (.Notary = True) Then
                    PrintNotary(JobWaiverLogId)
                    MergeNotary(sreport)
                End If

            End With
            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            'Dim FlagError = EmailToUser("Job Waiver Central", txtToListForRecensionLetter.Text, txtRecensionLetterSubject.Text, txtRecensionLetterBody.Text, Filename, fileArray) 'For a timing
            Dim FlagError = EmailToUser(txtToListForRecensionLetter.Text, txtRecensionLetterSubject.Text, txtRecensionLetterBody.Text, Filename, fileArray) 'For a timing

            If FlagError = "0" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RecensionLetterEmailSendPopup", "RecensionLetterEmailSendPopup('Your Recension Letter Has Been Emailed.');", True)
            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RemoveBackdrop", "RemoveBackdrop();", True)
    End Sub
#End Region
#Region "Notes"
    Protected Sub JobAddNote1_ItemSaved() Handles JobAddNote1.ItemSaved
        LoadData(Me.itemid)
        'Me.TabContainer.ActiveTab = Me.TabPanel3
        'Dim s As String = Me.Page.Request.Url.ToString.ToUpper
        '  s = Strings.Replace(s, "DEFAULT.ASPX", "")
        'Me.Page.Response.Redirect(s, True)
        hddbTabName.Value = "Notes"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsSet", "ActivateTab('" & hddbTabName.Value & "');", True)

    End Sub
    Protected Sub gvwNotes_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotes.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnViewNote = TryCast(e.Row.FindControl("btnViewNote"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnViewNote"), LinkButton).Click, AddressOf btnViewNote_Click

        End If
    End Sub

    Protected Sub gvwNotes_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotes.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnViewNote = TryCast(e.Row.FindControl("btnViewNote"), LinkButton)
            btnViewNote.Attributes("rowno") = DR("Id")

            ' e.Row.Cells(0).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            e.Row.Cells(1).Text = "" & Strings.Left(e.Row.Cells(1).Text, 10) & ""
            e.Row.Cells(1).Width = "80"

            e.Row.Cells(2).Wrap = True
            If Me.CurrentUser.UserManager = 0 Then
                btnViewNote.Visible = False
                'gvwNotes.Columns(0).Visible = False

            End If
        End If
    End Sub
    Protected Sub btnViewNote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewNote.Click
        btnViewNote = TryCast(sender, LinkButton)
        Dim myItem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        Dim test As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActionHistory
        Dim test1 As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActions
        If btnViewNote.Attributes("rowno") > 1 Then

            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(btnViewNote.Attributes("rowno"), myItem)
            Me.txtNotes.Text = myItem.Note.Replace("<", "[").Replace(">", "]")
            If Me.txtNotes.Text.Trim = "" Then
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(btnViewNote.Attributes("rowno"), test)
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(test.ActionId, test1)
                Me.txtNotes.Text = test1.HistoryNote.Replace("<", "[").Replace(">", "]")
            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsNotePopUp", "DisplayNotePopUp();", True)
        'Comment by Jaywanti on 31-03-2016
        'Dim s As String = "~/MainDefault.aspx?lienview.jobeditnote&itemid=" & Me.itemid & "&subid=" & btnViewNote.Attributes("rowno")
        'Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
#End Region

#Region "Job Info"
    Protected Sub JobEdit1_ItemSaved() Handles JobEdit1.ItemSaved
        Dim s As String = Me.Page.Request.Url.ToString.ToUpper
        '  s = Strings.Replace(s, "DEFAULT.ASPX", "")
        Me.Page.Response.Redirect(s, True)

        'LoadData(Me.itemid)
        'Me.TabContainer.ActiveTab = Me.TabPanel3

    End Sub
#End Region
#Region "Docs"
    Protected Sub btnPrintNotes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintNotes.Click
        Me.ViewState("VIEW") = "NOTES"
        Session("currenttab") = "NOTES"

        SetCurrentTab()
        hddbTabName.Value = "Notes"
        Dim sreport As String = LienView.Provider.GetNotesForJob(itemid, True)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    Filepath = Replace(Filepath, "'", "~")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub

    Protected Sub gvwDocs_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntDocs"), LinkButton).Click, AddressOf btnPrntDocs_Click

        End If
    End Sub
    Protected Sub gvwDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            btnPrntDocs.Attributes("rowno") = DR("Id")
            btnDeleteDocItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteDocItem.Attributes("rowno") = DR("Id")
            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            If Me.CurrentUser.UserManager = 0 Then
                btnPrntDocs.Visible = False
                btnDeleteDocItem.Visible = False
                'gvwDocs.Columns(0).Visible = False
                'gvwDocs.Columns(5).Visible = False
            End If
        End If

    End Sub
    Protected Sub btnPrntDocs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntDocs.Click
        'Session("currenttab") = "Docs"
        btnPrntDocs = TryCast(sender, LinkButton)
        'Me.ViewState("VIEW") = "Docs"
        Session("currenttab") = "Docs"
        SetCurrentTab()
        Dim myitem As New TABLES.JobAttachments
        ProviderBase.DAL.Read(btnPrntDocs.Attributes("rowno"), myitem)

        Dim sreport As String = "~/UserData/output/" & myitem.Id & "-" & myitem.FileName
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.DocumentImage, Me.MapPath(sreport))

        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)

                Dim firstpart As String = Filepath.Substring(0, Filepath.LastIndexOf("/"))
                Dim lastpart As String = Filepath.Substring(Filepath.LastIndexOf("/") + 1)
                'lastpart = HttpUtility.UrlEncode(lastpart)  'Remove # with %23 (Html Encoding UTF-8)
                ' lastpart = HttpUtility.UrlPathEncode(lastpart)  'Remove space with %20
                lastpart = Uri.EscapeDataString(lastpart) 'Remove # with %23 , space with %20

                Filepath = firstpart + "/" + lastpart

                If (Filepath <> "") Then
                        Filepath = Replace(Filepath, "'", "~")
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                    End If
                End If
                'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
                'Me.MultiView1.SetActiveView(Me.View2)
            Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub


    Protected Sub frm_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles frm.Unload
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where JobId = " & Me.itemid
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub


    Protected Sub btnFileUpLoadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadCancel.Click
        'markparimal-uploadfile:To refresh Grid values .

        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where JobId = " & Me.itemid
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsGens", "DisplayTablelayout();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModalFileUpload','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
    End Sub
#End Region
#Region "State Forms"
    Protected Sub btnAddStateFrom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddStateFrom.Click
        Dim s As String = "~/MainDefault.aspx?lienview.jobeditsform&itemid=" & Me.itemid & "&subid=" & -1
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwStateForms_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwStateForms.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntSForm = TryCast(e.Row.FindControl("btnPrntSForm"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntSForm"), LinkButton).Click, AddressOf btnPrntSForm_Click
            btnEditSForm = TryCast(e.Row.FindControl("btnEditSForm"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditSForm"), LinkButton).Click, AddressOf btnEditSForm_Click
            btnDeleteSForm = TryCast(e.Row.FindControl("btnDeleteSForm"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteSForm"), LinkButton).Click, AddressOf btnDeleteSForm_Click

        End If
    End Sub
    Protected Sub gvwStateForms_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwStateForms.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntSForm = TryCast(e.Row.FindControl("btnPrntSForm"), LinkButton)
            btnPrntSForm.Attributes("rowno") = DR("JobNoticeLogId")
            btnEditSForm = TryCast(e.Row.FindControl("btnEditSForm"), LinkButton)
            btnEditSForm.Attributes("rowno") = DR("JobNoticeLogId")
            btnDeleteSForm = TryCast(e.Row.FindControl("btnDeleteSForm"), LinkButton)
            btnDeleteSForm.Attributes("rowno") = DR("JobNoticeLogId")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            e.Row.Cells(2).Text = "" & Utils.FormatDate(DR("DatePrinted")) & ""
            If Me.CurrentUser.UserManager = 0 Then
                btnPrntSForm.Visible = False
                btnEditSForm.Visible = False
                btnDeleteSForm.Visible = False
                'gvwStateForms.Columns(0).Visible = False
                'gvwStateForms.Columns(4).Visible = False
            End If
        End If

    End Sub
    Protected Sub btnPrntSForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntSForm.Click
        btnPrntSForm = TryCast(sender, LinkButton)
        Session("currenttab") = "SFORM"
        SetCurrentTab()
        Dim sreport As String = LienView.Provider.PrintStateForm(btnPrntSForm.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    Filepath = Replace(Filepath, "'", "~")
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindowNew('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub
    Private Sub PrintSFORM(ByVal itemid As String)

        Me.ViewState("VIEW") = "SFORM"
        SetCurrentTab()

        Dim sreport As String = LienView.Provider.PrintStateForm(itemid)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub
    Protected Sub btnEditSForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditSForm.Click
        Session("currenttab") = "SFORM"
        btnEditSForm = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?lienview.jobeditsform&itemid=" & Me.itemid & "&subid=" & btnEditSForm.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnDeleteSForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteSForm.Click
        Session("currenttab") = "SFORM"
        Session("exitviewer") = "1"
        'Me.ViewState("VIEW") = "SFORM"
        SetCurrentTab()

        btnDeleteSForm = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobOtherNoticeLog
        myitem.JobNoticeLogId = btnDeleteSForm.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)

    End Sub
#End Region
#Region "Navigation"
    Protected Sub btnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetJobFromList("F")
    End Sub
    Protected Sub btnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetJobFromList("L")
    End Sub
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetJobFromList(-1)
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        GetJobFromList(+1)
    End Sub
    Private Sub GetJobFromList(ByVal increment As String)

        If (HttpContext.Current.Session("SortCol") <> "" And HttpContext.Current.Session("SortDir") <> "") And (HttpContext.Current.Session("SortCol") <> currentSortColumn Or HttpContext.Current.Session("SortDir") <> currentSortDirection) Then
            Dim dtviewjobinfo As System.Data.DataView
            dtviewjobinfo = myview
            Dim strsort As String = "JobId"

            Select Case HttpContext.Current.Session("SortCol")
                Case "1"
                    strsort = "JobId"
                    currentSortColumn = 1
                Case "2"
                    strsort = "JobName"
                    currentSortColumn = 2
                Case "3"
                    strsort = "JobNum"
                    currentSortColumn = 3
                Case "4"
                    strsort = "CityStateZip"
                    currentSortColumn = 4
                Case "5"
                    strsort = "BranchNum"
                    currentSortColumn = 5
                Case "6"
                    strsort = "StatusCode"
                    currentSortColumn = 6
                Case "7"
                    strsort = "ClientCustomer"
                    currentSortColumn = 7
                Case "8"
                    strsort = "DateAssigned"
                    currentSortColumn = 8
                Case "9"
                    strsort = "ClientCode"
                    currentSortColumn = 9
            End Select

            currentSortDirection = HttpContext.Current.Session("SortDir")

            dtviewjobinfo.Sort = strsort & " " & HttpContext.Current.Session("SortDir")
            myview = dtviewjobinfo
        End If

        Select Case increment
            Case "F"
                myview.RowIndex = 0
            Case "L"
                myview.RowIndex = myview.Count - 1
            Case Else
                myview.RowIndex = GetRowIndex() + increment
        End Select

        If myview.RowIndex < 0 Then
            myview.RowIndex = 0
        End If
        If myview.RowIndex >= myview.Count Then
            myview.RowIndex = myview.Count - 1
        End If

        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myview.RowItem("JobId")
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)

        'Added by KD just for testing
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NavigateThroughJobDetails", "getJobDetails(" + Me.Page.ResolveUrl(s) + ");", True)

    End Sub
    Private Function GetRowIndex() As Integer
        Dim myidx As Integer = 0
        myview.MoveFirst()
        Do Until myview.EOF
            If myview.RowItem("jobid") = Me.itemid Then
                Return myidx
            End If
            myidx += 1
            myview.MoveNext()
        Loop
        Return myidx = 0

    End Function
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?lienview.joblistnew&page=1"
        If IsNothing(Me.Session("LIST")) = False Then
            If Me.Session("LIST").ToString.ToUpper = "LDR" Then
                s = "~/MainDefault.aspx?lienview.liensdayremaininglist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "TPR" Then
                s = "~/MainDefault.aspx?lienview.texaspendinglist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "WDR" Then
                s = "~/MainDefault.aspx?lienview.waiversdayremaininglist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "INV" Then
                s = "~/MainDefault.aspx?lienview.invoicealertlist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "TJR" Then
                s = "~/MainDefault.aspx?lienview.TexasReview&page=1"
            End If
        End If
        Me.Session("LIST") = Nothing
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnExitViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitViewer.Click
        'Me.MultiView1.SetActiveView(Me.View1)
        'SetCurrentTab()
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Session("tabindex") = "main"
        'selected tab issue -- add 28/01/2015
        'If Not String.IsNullOrEmpty(qs.GetParameter("tabstate")) Then
        '    Session("currenttab") = qs.GetParameter("VIEW")
        'End If

        'If Not String.IsNullOrEmpty(qs.GetParameter("tabstate")) Then
        '    Session("currenttab") = qs.GetParameter("tabstate")
        'End If
        'navigation issues ---add TabState in queryString 1/2/2012
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & qs.GetParameter("itemid") & "&tabindex=" & 4 & "&tabstate=" & Session("currenttab")
        Session("exitviewer") = "1"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
#End Region
#Region "Print Forms"
    Public Sub PrintNotice()

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")
        'Dim sreportname As String = CRF.BLL.Providers.LiensClientView.PrintPrelimNotice(CRF.CLIENTVIEW.BLL.LienView.Provider.GetCurrentUser, s(1))
        'If System.IO.File.Exists(sreportname) = False Then Exit Sub
        'Dim sreport = "~/userdata/output/" & System.IO.Path.GetFileName(sreportname)
        'Me.panJobInfo.Visible = False
        'Me.panReportViewer.Visible = True
        'Me.plcReportViewer.Controls.Clear()
        'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 550, 650, "true"))

    End Sub
    Public Sub PrintWaivers()
        Session("currenttab") = "WAIVER"
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)

        Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")
        'Dim sreportname As String = CRF.BLL.Providers.LiensClientView.PrintJobWaiver(CRF.CLIENTVIEW.BLL.LienView.Provider.GetCurrentUser, s(1))
        'If System.IO.File.Exists(sreportname) = False Then Exit Sub
        'Dim sreport = "~/userdata/output/" & System.IO.Path.GetFileName(sreportname)
        'Me.panJobInfo.Visible = False
        'Me.panReportViewer.Visible = True
        'Me.plcReportViewer.Controls.Clear()
        'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 550, 650, "true"))

    End Sub
    Public Sub PrintStateForm()

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")

        'Dim sreportname As String = CRF.BLL.Providers.LiensClientView.PrintOtherNotice(CRF.CLIENTVIEW.BLL.LienView.Provider.GetCurrentUser, s(1))
        'If System.IO.File.Exists(sreportname) = False Then Exit Sub
        'Dim sreport = "~/userdata/output/" & System.IO.Path.GetFileName(sreportname)
        'Me.panJobInfo.Visible = False
        'Me.panReportViewer.Visible = True
        'Me.plcReportViewer.Controls.Clear()
        'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 550, 650, "true"))

    End Sub


#End Region

    Protected Sub btnTableLayout_Click(sender As Object, e As EventArgs)
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwNotes.Rows.Count = 0 Then
            Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        If Not Me.gvwLegalParties.Rows.Count = 0 Then
            Me.gvwLegalParties.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwNotices.Rows.Count = 0 Then
            Me.gvwNotices.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwWaivers.Rows.Count = 0 Then
            Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwStateForms.Rows.Count = 0 Then
            Me.gvwStateForms.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwVReq.Rows.Count = 0 Then
            Me.gvwVReq.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwTexasReq.Rows.Count = 0 Then
            Me.gvwTexasReq.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwINVC.Rows.Count = 0 Then
            Me.gvwINVC.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncLegalPartiesList", "CallSuccessFuncLegalPartiesList();", True)
    End Sub

    Protected Sub btnDeleteWaiverId_Click(sender As Object, e As EventArgs)
        Session("currenttab") = "WAIVER"
        Session("exitviewer") = "1"
        SetCurrentTab()

        btnDeleteWaiver = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobWaiverLog
        myitem.JobWaiverLogId = hdnWaiverId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
    End Sub

    Protected Sub btnStateFormId_Click(sender As Object, e As EventArgs)
        Session("currenttab") = "SFORM"
        Session("exitviewer") = "1"
        'Me.ViewState("VIEW") = "SFORM"
        SetCurrentTab()

        btnDeleteSForm = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobOtherNoticeLog
        myitem.JobNoticeLogId = hdnStateFormId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeletes", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
    End Sub

    Protected Sub btnTXReqId_Click(sender As Object, e As EventArgs)
        Me.ViewState("VIEW") = "VREQ"
        SetCurrentTab()

        btnDeleteVReq = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobVerifiedSentRequest
        myitem.Id = hdnTXReqId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeletesData", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
    End Sub

    Protected Sub btnTexasRequest_Click(sender As Object, e As EventArgs)
        Me.ViewState("VIEW") = "TREQ"
        SetCurrentTab()

        btnDeleteTReq = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobTexasRequest
        myitem.Id = hdnTexasRequest.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeleted", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
        If Me.ViewState("VIEW") = "TREQ" Then
            'Me.TabContainer.ActiveTabIndex = 7
            'Me.TabContainer.TabIndex = 7
            hddbTabName.Value = "NoticeRequest"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsDta", "ActivateTab('" & hddbTabName.Value & "');", True)
        End If
    End Sub

    Protected Sub btnInvoiceid_Click(sender As Object, e As EventArgs)
        Me.ViewState("VIEW") = "INVC"
        SetCurrentTab()
        btnDeleteINVC = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobInvoices
        myitem.Id = hdnInvoiceid.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeletedInvoices", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
    End Sub

    Protected Sub btnNoteDetail_Click(sender As Object, e As EventArgs)
        Dim myItem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        Dim test As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActionHistory
        Dim test1 As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActions
        If hdnBatchJobNoteId.Value > 1 Then

            Dim result As String = ""

            If hdnBatchJobNoteType.Value = "NH" Then
                Dim MYSQL As String = "SELECT Id,DateCreated,UserId,EnteredByUserId,Note as Note FROM JOBHISTORY (NOLOCK) WHERE ID =" & hdnBatchJobNoteId.Value
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                result = myview.RowItem("Note")
            ElseIf hdnBatchJobNoteType.Value = "AH" Then
                Dim MYSQL As String = "SELECT JobActionHistoryId as Id,DateCreated,RequestedBy,RequestedByUserId,HistoryNote as Note FROM dbo.JobActionHistory (NOLOCK) INNER JOIN dbo.JobActions (NOLOCK) ON dbo.JobActionHistory.ActionId = dbo.JobActions.Id WHERE JobActionHistoryId =" & hdnBatchJobNoteId.Value
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                result = myview.RowItem("Note")
            Else
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchJobNoteId.Value, myItem)
                result = myItem.Note
            End If

            Me.txtNotes.Text = result.Replace("<", "[").Replace(">", "]")

            'If hdnBatchJobNoteType.Value = "NH" Then
            '    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchJobNoteId.Value, myItem)

            '    Me.txtNotes.Text = myItem.Note.Replace("<", "[").Replace(">", "]")
            'ElseIf hdnBatchJobNoteType.Value = "AH" Then
            '    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchJobNoteId.Value, test)
            '    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(test.ActionId, test1)
            '    Me.txtNotes.Text = test1.HistoryNote.Replace("<", "[").Replace(">", "]")

            'End If


            'If Me.txtNotes.Text.Trim = "" Then
            '    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchJobNoteId.Value, test)
            '    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(test.ActionId, test1)
            '    Me.txtNotes.Text = test1.HistoryNote
            'End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsNotePopUp", "DisplayNotePopUp();", True)
    End Sub
    Protected Sub btnDeleteDoc_Click(sender As Object, e As EventArgs)
        Dim myitem As New TABLES.JobAttachments
        myitem.Id = hdnDocId.Value
        ProviderBase.DAL.Delete(myitem)
        hddbTabName.Value = "Docs"
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where JobId = " & Me.JobId.Text
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetTabName", "SetTabName('Docs');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)

    End Sub
    Protected Sub btnSaveAdditionalInfo_Click(sender As Object, e As EventArgs)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)
        With myjob
            .APNNum = Me.APNNum.Text.Trim()
            .PONum = Me.PONum.Text.Trim()
            ' .LenderBondNum = Me.BondNum.Text.Trim()
            .BuildingPermitNum = Me.BuildingPermitNum.Text.Trim()
            .SpecialInstruction = Me.SpecialInstruction.Text.Trim()
            .Note = Me.Note.Text.Trim()
            .EquipRate = EquipRate.Text.Trim()
            .EquipRental = EquipRental.Text.Trim()
            .FilterKey = FilterKey.Text.Trim()
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        End With
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivAdditionalInfo','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
    End Sub

    Protected Sub btnBackFilter_Click(sender As Object, e As EventArgs)
        Dim s As String = "~/MainDefault.aspx?lienview.joblistnew"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub

    <System.Web.Services.WebMethod(EnableSession:=True)>
    Public Shared Sub DeletSession()

        Try
            'If Not HttpContext.Current.Session("UploadedFileName") Is Nothing Then
            '    HttpContext.Current.Session("UploadedFileName") = Nothing
            'End If
            'If Not HttpContext.Current.Session("UploadedFileDataBytes") Is Nothing Then
            '    HttpContext.Current.Session("UploadedFileDataBytes") = Nothing
            'End If
            'If Not HttpContext.Current.Session("UploadedFileExtension") Is Nothing Then
            '    HttpContext.Current.Session("UploadedFileExtension") = Nothing
            'End If
            'HttpContext.Current.Session("UploadFiles") = Nothing
            If Not HttpContext.Current.Session("UploadFiles") Is Nothing Then
                HttpContext.Current.Session("UploadFiles") = Nothing
                'Dim uploadFiles As List(Of UploadFile) ' = New List(Of UploadFile)()
                'uploadFiles = HttpContext.Current.Session("UploadFiles")
                'For i = uploadFiles.Count - 1 To 0 Step -1
                '    If uploadFiles(i).UploadedFileName = filename Then
                '        uploadFiles.RemoveAt(i)
                '    End If
                'Next
                'If uploadFiles.Count > 0 Then
                '    HttpContext.Current.Session("UploadFiles") = uploadFiles
                'Else
                '    HttpContext.Current.Session("UploadFiles") = Nothing
                'End If

            End If






        Catch ex As Exception
        End Try
    End Sub
End Class
