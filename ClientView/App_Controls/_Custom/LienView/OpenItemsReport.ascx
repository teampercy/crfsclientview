<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            LoadClientList()
            If IsNothing(Me.Page.Request.QueryString("rpt")) = False Then
                Me.DownLoadReport(Me.Page.Request.QueryString("rpt"))
            End If
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_CLIENTVIEW_GetJobInventory
        With MYSPROC
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                End If
            End If
            If Me.BranchNum.Text.Trim.Length > 1 Then
                .BranchNum = Me.BranchNum.Text.Trim
            End If
            .FromDate = "01/01/1980"
            .ThruDate = Today
            .UserId = Me.CurrentUser.Id
            .SelectByOpenItems = True
            .JobType = 0
        End With

        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetOpentItemsReport(MYSPROC, Me.PDFReport.Checked))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If Me.SpreadSheet.Checked = False Then
                Me.plcReportViewer.Controls.Clear()
                Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
                Me.MultiView1.SetActiveView(Me.View2)
            Else
                Me.btnDownLoad.Visible = True
                Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
                Me.MultiView1.SetActiveView(Me.View2)
            End If
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        Me.btnDownLoad.Visible = False
               
    End Sub
    

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DownLoadReport(Me.Session("spreadsheet"))
    End Sub
    Private Sub LoadClientList()
       
        Dim vwclients As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop
            Me.panClients.Visible = True
        End If
    End Sub
</script>

<script>

    function MaintainMenuOpen() {
        MainMenuToggle('liLienViewReport');
        SubMenuToggle('liOpenItemsReport');
        SetHeaderBreadCrumb('LienView', 'Reports', 'Open Items Report');
    }

    $(function () {
     
    });

</script>


<div id="modcontainer" class="margin-bottom-0" style="width: 100%">
    <h1 class="panelheader">Open Items Report</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">
                <asp:Panel ID="panClients" Visible="false" runat="server">
                    <table width="550">
                        <tr>
                            <td class="row-label" style="width: 200px">Client Select:</td>
                            <td class="row-data" style="width: 490px">
                                <asp:DropDownList ID="DropDownList1" runat="server" Width="250px">
                                </asp:DropDownList>&nbsp;<asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                    RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1" Width="224px">
                                    <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL"> All Clients   </asp:ListItem>
                                    <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED"> Selected Client</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>

                </asp:Panel>

                <div class="form-horizontal">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Branch Filter:</label>
                        <div class="col-md-2" style="display: flex;">
                            <asp:TextBox ID="BranchNum" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">PDF Report:</label>
                        <div class="col-md-8">
                            <div class="radio-custom radio-default">
                                <asp:RadioButton ID="PDFReport" Text=" " runat="server" Checked="True" GroupName="ReportType" ></asp:RadioButton>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-4 control-label">Spreadsheet:</label>
                        <div class="col-md-8">
                            <div class="radio-custom radio-default">
                            <asp:RadioButton ID="SpreadSheet" runat="server" Text=" "  GroupName="ReportType"></asp:RadioButton>
                            </div>
                        </div>
                    </div>


                </div>                
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="The Following Field(s) are Required: " />
            </div>
            <div class="footer">
                <asp:UpdatePanel ID="updPan1" runat="server">
                    <ContentTemplate>
                        <div class="col-md-12" style="text-align: right;">
                            <asp:LinkButton ID="btnSubmit"
                                CssClass="btn btn-primary"
                                runat="server"
                                Text="Create Report" OnClick="btnSubmit_Click" />
                        </div>


                        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="TransparentGrayBackground"></div>
                                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                    <div class="PageUpdateProgress">
                                        <asp:Image ID="ajaxLoadNotificationImage"
                                            runat="server"
                                            ImageUrl="~/images/ajax-loader.gif"
                                            AlternateText="[image]" />
                                        &nbsp;Please Wait...
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:AlwaysVisibleControlExtender
                                    ID="AlwaysVisibleControlExtender1"
                                    runat="server"
                                    TargetControlID="alwaysVisibleAjaxPanel"
                                    HorizontalSide="Center"
                                    HorizontalOffset="150"
                                    VerticalSide="Middle"
                                    VerticalOffset="0">
                                </ajaxToolkit:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Selection" OnClick="Linkbutton2_Click"></asp:LinkButton>
                        </div>
                        <div class="col-md-2">
                            <asp:Button ID="btnDownLoad" class="btn btn-primary" Text="DownLoad SpreadSheet" runat="server" OnClick="btnDownLoad_Click" />
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</div>
