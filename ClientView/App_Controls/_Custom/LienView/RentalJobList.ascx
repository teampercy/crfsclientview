﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style>
    #JobFilterBody .radio-custom label {
        width: 75px;
    }

    #JobFilterBody .radio-custom.radio-default.radio-inline {
        min-height: 0px;
        padding-top: 0px;
    }

    .dataTables_length {
        margin-top: 10px;
    }

    .RemovePadding {
        padding: 0px !important;
    }
</style>
<script type="text/javascript">
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Tools', 'RentalView Job List');
        MainMenuToggle('liTools');
        SubMenuToggle('liRentalJobList');
        return false;
    }
    function CallDatePicker() {

        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            showButtonPanel: true,
        });
    }
    function CallSuccessFunc() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [5, 15, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": false,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-3'i><'col-sm-1'l><'col-sm-8'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },
        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwJobs.ClientID%>').dataTable(options);
    }
    function BindClientSelection() {
        $('#UlClientSelectionType li').remove();
        var x = document.getElementById('<%=DropDownList1.ClientID%>');
        var txt = "";
        var i;
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                $('#UlClientSelectionType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClientSel_' + x.options[i].value + '" onclick="setSelectedClientId(' + x.options[i].value + ',\'' + x.options[i].text + '\')">' + x.options[i].text + '</a></li>');
            }
        }
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val() != "") {
            $('#btnClientSelectionType').html($('#liClientSel_' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedClientId(id, TextData) {
        $('#' + '<%=hdnClientSelectionId.ClientID%>').val(id);
        $('#btnClientSelectionType').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    //Bind List to Job Status Dropdown 
    function BindJobStatus() {          //------------------
        $('#UlJobStatus').html();
        var x = document.getElementById('<%=DrpJobStatus.ClientID%>');
        var txt = "<select class=\"form-control\" id=\"editable-select\" style=\"overflow-y: auto; \">";
        var i;
        txt = txt + '<option role="presentation"><a href="javascript:void(0)" role="menuitem" id="liJobStatus_0" >--Select Job Status--</a></option>';
        if (x != null) {
            if ($('#' + '<%=hdnJobStatusSelectedId.ClientID%>').val() != "") {
                var oldVal = $('#' + '<%=hdnJobStatusSelectedId.ClientID%>').val();
                for (i = 0; i < x.length; i++) {
                    if (x.options[i].value == oldVal) {
                        txt = txt + '<option selected="selected" role="menuitem" id="liJobStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                    }
                    else {
                        txt = txt + '<option role="menuitem" id="liJobStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                    }
                }
            }
            else {
                for (i = 0; i < x.length; i++) {
                    txt = txt + '<option role="menuitem" id="liJobStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                }
            }
        }

        txt = txt + "</select>";
        $('#UlJobStatus').html(txt);
        $('#editable-select').editableSelect();
        $('#editable-select').on('select.editable-select', function (e) {
            //Working only for chrome only need to be handled for all browsers.
            var ul = $('#' + $(e.target).prop('id')).next('ul')[0];
            var li = $(ul).children('li.selected')[0];
            var id = $($($(li)[0]).prop('attributes'))[2].value;
            $('#' + '<%=hdnJobStatusSelectedId.ClientID%>').val(id);
        });

    }
    function setSelectedJobStatusId(id, TextData) {
        //$('#' + '<%=hdnJobStatusSelectedId.ClientID%>').val(id);
        //$('#btnJobStatus').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    //end here
    //Bind List to Desk Dropdown
    function BindDesk() {
        $('#UlDesk li').remove();
        var x = document.getElementById('<%=DrpDesk.ClientID%>');
        var txt = "";
        var i;
        $('#UlDesk').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liDesk_0" onclick="setSelectedDeskId(\'0\',\'--Select Desk--\')">--Select Desk--</a></li>');
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                $('#UlDesk').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liDesk_' + x.options[i].value + '" onclick="setSelectedDeskId(\'' + x.options[i].value + '\',\'' + x.options[i].text + '\')">' + x.options[i].text + '</a></li>');
            }
        }
        if ($('#' + '<%=hdnDeskSelectedId.ClientID%>').val() != "") {
            $('#btnDesk').html($('#liDesk_' + $('#' + '<%=hdnDeskSelectedId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedDeskId(id, TextData) {
        $('#' + '<%=hdnDeskSelectedId.ClientID%>').val(id);
        $('#btnDesk').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    //end here
    function myFunctionCustomerNameLike(id) {
        if (id == 1) {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionCustomerRefLike(id) {

        if (id == 1) {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = true;
        }
    }



    function myFunctionJobNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionJobCityLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobCityLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobCityLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobCityLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobCityLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobAddressLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNoLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionOwnerNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionContractorNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=ContractorNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=ContractorNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=ContractorNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=ContractorNameLike_2.ClientID%>').checked = true;
        }
    }
    $(document).ready(function () {
        BindJobStatus();
        BindDesk();
    });
</script>
<script runat="server">
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV
    Dim vwclients As HDS.DAL.COMMON.TableView
    Dim vwJobStatus As HDS.DAL.COMMON.TableView
    Dim vwDesk As HDS.DAL.COMMON.TableView
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Me.Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            LoadClientList()
            LoadDropDown()
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindJobStatus", "BindJobStatus();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindDesk", "BindDesk();", True)
            If gvwJobs.Rows.Count > 0 Then
                Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If
       
    End Sub
#Region "Bind List to Job Status Dropdown and Desk Dropdown"
    Private Sub LoadDropDown()
        vwJobStatus = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVStatusList()
        Dim strquery As String = ""
        strquery = "JOBVIEWSTATUS <> null or JOBVIEWSTATUS <>''"
        vwJobStatus.RowFilter = strquery
        If vwJobStatus.Count > 0 Then
            vwJobStatus.MoveFirst()
            vwJobStatus.Sort = "FriendlyName"
            Me.DrpJobStatus.Items.Clear()
            Dim li As ListItem
            Do Until vwJobStatus.EOF
                li = New ListItem()
                li.Value = vwJobStatus.RowItem("JobviewStatus")
                li.Text = vwJobStatus.RowItem("FriendlyName")
                Me.DrpJobStatus.Items.Add(li)
                vwJobStatus.MoveNext()
            Loop
        End If
        vwDesk = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVDeskList()
        strquery = "DESKNUM <> null or DESKNUM <>''"
        vwDesk.RowFilter = strquery
        If vwDesk.Count > 0 Then
            vwDesk.MoveFirst()
            vwDesk.Sort = "FriendlyName"
            Me.DrpDesk.Items.Clear()
            Dim li As ListItem
            Do Until vwDesk.EOF
                li = New ListItem()
                li.Value = vwDesk.RowItem("DeskNum")
                li.Text = vwDesk.RowItem("FriendlyName")
                Me.DrpDesk.Items.Add(li)
                vwDesk.MoveNext()
            Loop
        End If
    End Sub
#End Region
   
    Private Sub LoadClientList()
       
        vwclients = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop
           
            Me.panClients.Visible = True
            mysproc = GetFilter()
            If mysproc.ClientCode.Length > 4 Then
                Me.DropDownList1.SelectedValue = mysproc.ClientCode
                Me.hdnClientSelectionId.Value = mysproc.ClientCode
            End If
          
            
        End If
        
    End Sub
    Protected Function GetFilter() As Object
        If IsNothing(Session("JobRentalViewListP")) = False Then
            Return Session("JobRentalViewListP")
        Else
            Return New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV
        End If
    End Function
    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackFilter.Click
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        Try
            If Not Me.Session("JobRentalList") Is Nothing Then
                myviewSorting = Session("JobRentalList")
            Else
             
                Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV
                obj.Page = 1
                obj.PageRecords = 10000 '500 Changed by jaywanti
                obj.UserId = Me.CurrentUser.Id ' here hardoded value is there i.e 5
                myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
                Me.Session("JobRentalList") = myviewSorting
            End If
            
            
            Dim SortBy As String
            If drpSortBy.SelectedItem.Value = "-1" Then
                SortBy = "ListId ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "1" Then
                SortBy = "RefNum ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "2" Then
                SortBy = "JobState ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "3" Then
                SortBy = "JVDeskNum ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "4" Then
                SortBy = "JobBalance ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "5" Then
                SortBy = "NOIDeadlineDate ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "6" Then
                SortBy = "LienDeadlineDate ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "7" Then
                SortBy = "BondDeadlineDate ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "8" Then
                SortBy = "SNDeadlineDate ASC"
                myviewSorting.Sort = SortBy
                'ElseIf drpSortBy.SelectedItem.Value = "9" Then
                '    SortBy = "JobAdd1 ASC"
                '    myviewSorting.Sort = SortBy
                'ElseIf drpSortBy.SelectedItem.Value = "10" Then
                '    SortBy = "JobName ASC"
                '    myviewSorting.Sort = SortBy
            End If
            Me.gvwJobs.DataSource = myviewSorting
            Me.gvwJobs.DataBind()
            Me.MultiView1.SetActiveView(Me.View2)
            If gvwJobs.Rows.Count > 0 Then
                Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAll.Click
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV
        If Me.panClients.Visible = True Then
            If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                mysproc.ClientCode = Me.DropDownList1.SelectedValue
                mysproc.ClientCode = Me.hdnClientSelectionId.Value
            End If
        End If

        mysproc.Page = 1
        GetData(mysproc, 1, "F")
        Me.MultiView1.SetActiveView(Me.View2)
      
    End Sub
    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV, ByVal apage As Integer, ByVal adirection As String)
        myview = TryCast(Me.Session("JobRentalList"), HDS.DAL.COMMON.TableView)
        
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If
        
        asproc.PageRecords = 20000
        asproc.UserId = Me.CurrentUser.Id
        If adirection = "F" Then
            asproc.Page = 1
        End If
        If adirection = "N" Then
            asproc.Page += 1
        End If
        If adirection = "P" Then
            asproc.Page -= 1
        End If
        
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        If adirection = "L" Then
            myview = Session("JobRentalList")
            asproc.Page = myview.RowItem("pages")
        End If
        
        If adirection.Length < 1 And IsNothing(myview) = False Then
            myview = Session("JobRentalList")
        Else
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
        End If
        
        If myview.Count < 1 Then
            Me.gvwJobs.DataSource = Nothing
            Me.gvwJobs.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
            Exit Sub
        End If
        
        Session("JobRentalViewListP") = asproc
        Session("JobRentalList") = myview

        Dim totrecs As String = myview.RowItem("totalrecords")
        Dim currpage As String = myview.RowItem("page")
        Dim totpages As Integer = (totrecs / 14 + 1)
        totpages = myview.RowItem("pages")
         
        Dim i As Integer = 0

        Me.gvwJobs.DataSource = myview
        Me.gvwJobs.DataBind()
        If gvwJobs.Rows.Count > 0 Then
            Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        'Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        Me.MultiView1.SetActiveView(Me.View2)
        
        Me.Session("JobRentalList") = myview
        
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        With mysproc
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                    .ClientCode = Me.hdnClientSelectionId.Value
                End If
            End If

            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike_1.Checked Then
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If
            
                If Me.CustomerNameLike_2.Checked Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                End If
            End If
            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike_1.Checked Then
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If
            
                If Me.CustomerRefLike_2.Checked Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                End If
            End If
         
            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobName = Me.JobName.Text.Trim & "%"
                End If
            
                If Me.JobNameLike_2.Checked Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                End If
            End If
            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If
            
                If Me.JobNameLike_2.Checked Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                End If
            End If
            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike_1.Checked Then
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If
            
                If Me.JobAddressLike_2.Checked Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                End If
            End If
            If Me.JobCity.Text.Trim.Length > 0 Then
                If Me.JobCityLike_1.Checked Then
                    .JobCity = Me.JobCity.Text.Trim & "%"
                End If
            
                If Me.JobCityLike_2.Checked Then
                    .JobCity = "%" & Me.JobCity.Text.Trim & "%"
                End If
            End If
            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            End If
            If Me.OwnerName.Text.Trim.Length > 0 Then
                If Me.OwnerNameLike_1.Checked Then
                    .OwnerName = Me.OwnerName.Text.Trim & "%"
                End If
            
                If Me.OwnerNameLike_2.Checked Then
                    .OwnerName = "%" & Me.OwnerName.Text.Trim & "%"
                End If
            End If
            If Me.ContractorName.Text.Trim.Length > 0 Then
                If Me.ContractorNameLike_1.Checked Then
                    .GeneralContractor = Me.ContractorName.Text.Trim & "%"
                End If
            
                If Me.ContractorNameLike_2.Checked Then
                    .GeneralContractor = "%" & Me.ContractorName.Text.Trim & "%"
                End If
            End If
            If Me.hdnJobStatusSelectedId.Value.Trim.Length > 0 Then
                If Not Me.hdnJobStatusSelectedId.Value = "0" Then
                    .StatusCode = Me.hdnJobStatusSelectedId.Value.Trim & "%"
                End If
            End If
          
            '.FromReferalDate = Me.Calendar1.Value
            '.ThruReferalDate = Me.Calendar2.Value
            'If Me.chkByAssignDate.Checked Then
            '    .ReferalRange = 1
            'End If
            ' Added 5/16/2016
            .FromDeadlineDate = Me.Calendar3.Value
            .ThruDeadlineDate = Me.Calendar4.Value
            If Me.chkByDeadlineDate.Checked Then
                .DeadlineRange = 1
            End If
            ' Added 5/17/2016
            .FromBalance = Utils.GetDecimal(Me.FromBalance.Value)
            .ThruBalance = Utils.GetDecimal(Me.ThruBalance.Value)
            If Me.chkByBalance.Checked Then
                .BalanceRange = 1
            End If
            If Me.hdnDeskSelectedId.Value.Trim.Length > 0 Then
                If Not Me.hdnDeskSelectedId.Value = "0" Then
                    .DESKNUM = Me.hdnDeskSelectedId.Value.Trim & "%"
                End If
            End If
            If Me.txtRA.Text.Trim.Length > 0 Then
                .RANum = Me.txtRA.Text.Trim & "%"
            End If
            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            End If
            If Me.txtCreditRating.Text.Trim.Length > 0 Then
                .CreditRating = Me.txtCreditRating.Text.Trim & "%"
            End If
            If Me.txtCRFS.Text.Trim.Length > 0 Then
                .JobId = Me.txtCRFS.Text.Trim
            End If

           

            .UserId = Me.CurrentUser.Id

        End With
   
        GetData(mysproc, 1, "F")
        Me.MultiView1.SetActiveView(Me.View2)
    End Sub
    Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwJobs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            Dim lblAmount As Label = CType(e.Row.FindControl("lblAmount"), Label)
            Dim lblNoticeDate As Label = CType(e.Row.FindControl("lblNoticeDate"), Label)
            Dim lblLienDate As Label = CType(e.Row.FindControl("lblLienDate"), Label)
            Dim lblBondDeadline As Label = CType(e.Row.FindControl("lblBondDeadline"), Label)
            Dim lblSNDeadDate As Label = CType(e.Row.FindControl("lblSNDeadDate"), Label)
            Dim lblCity As Label = CType(e.Row.FindControl("lblCity"), Label)
            If Not (String.IsNullOrEmpty(lblAmount.Text)) Then
                lblAmount.Text = "$" + String.Format("{0:0.00}", lblAmount.Text)
            Else
                lblAmount.Text = "" '"$0.00"
            End If
            lblCity.Text = lblCity.Text & " " & DR("JobZip")
            '            If the NoticeDeadlineDate is less than 10 days away AND a notice has not been sent yet (NoticeSent is null), the font needs to be RED

            'If the LienDeadlineDate is less than 30 days away AND a Lien has not been filed yet (LienDate is null), the font needs to be RED

            'If the BondDeadlineDate is less than 30 days away AND a Bond has not been filed yet (BondDate is null), AND Public box is True, the font needs to be RED

            'If the SNDeadlineDate is less than 30 days away AND a SN has not been filed yet (SNDate is null), the font needs to be RED


            
            Dim currentdate As Date = Date.Now()
            currentdate = currentdate.Date '
            'Dim CurNextDate As Date = Date.Now()
            Dim TotalDays As Double
            Dim dtDate As DateTime
            If Not String.IsNullOrEmpty(lblNoticeDate.Text) Then
                dtDate = Convert.ToDateTime(lblNoticeDate.Text)
                TotalDays = (dtDate - currentdate).TotalDays
                If (TotalDays >= 0 And TotalDays <= 10) And (Convert.IsDBNull(DR("NoticeSent"))) Then
                    lblNoticeDate.Style.Add("color", "red")
                End If
            End If
            If Not String.IsNullOrEmpty(lblLienDate.Text) Then
                dtDate = Convert.ToDateTime(lblLienDate.Text)
                TotalDays = (dtDate - currentdate).TotalDays
                If (TotalDays >= 0 And TotalDays <= 30) And (Convert.IsDBNull(DR("LienDate"))) Then
                    lblLienDate.Style.Add("color", "red")
                End If
            End If
            If Not String.IsNullOrEmpty(lblBondDeadline.Text) Then
                dtDate = Convert.ToDateTime(lblBondDeadline.Text)
                TotalDays = (dtDate - currentdate).TotalDays
                If (TotalDays >= 0 And TotalDays <= 30) And (Convert.IsDBNull(DR("BondDate"))) Then
                    lblBondDeadline.Style.Add("color", "red")
                End If
            End If
            If Not String.IsNullOrEmpty(lblSNDeadDate.Text) Then
                dtDate = Convert.ToDateTime(lblSNDeadDate.Text)
                TotalDays = (dtDate - currentdate).TotalDays
                If (TotalDays >= 0 And TotalDays <= 30) And (Convert.IsDBNull(DR("SNDate"))) Then
                    lblSNDeadDate.Style.Add("color", "red")
                End If
            End If
            'If Not String.IsNullOrEmpty(lblLienDate.Text) Then
            '    CurNextDate = currentdate.Date.AddDays(30)
            '    dtDate = Convert.ToDateTime(lblLienDate.Text)
            '    If dtDate >= currentdate And dtDate <= CurNextDate Then
            '        lblLienDate.Style.Add("color", "red")
            '    End If
            'End If
            'If Not String.IsNullOrEmpty(lblBondDeadline.Text) Then
            '    CurNextDate = currentdate.Date.AddDays(30)
            '    dtDate = Convert.ToDateTime(lblBondDeadline.Text)
            '    If dtDate >= currentdate And dtDate <= CurNextDate Then
            '        lblBondDeadline.Style.Add("color", "red")
            '    End If
            'End If
            'If Not String.IsNullOrEmpty(lblSNDeadDate.Text) Then
            '    CurNextDate = currentdate.Date.AddDays(30)
            '    dtDate = Convert.ToDateTime(lblSNDeadDate.Text)
            '    If dtDate >= currentdate And dtDate <= CurNextDate Then
            '        lblSNDeadDate.Style.Add("color", "red")
            '    End If
            'End If
        End If
    End Sub
</script>
<div id="modcontainer" style="width: 100%;" class="margin-bottom-0">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1 class="panelheader">Job Filter</h1>
            <div class="body" id="JobFilterBody">
                <div class="form-horizontal">
                    <asp:Panel ID="panClients" runat="server">
                        <div class="form-group">
                            <label class="col-md-3 control-label">Client Selection</label>
                            <div class="col-md-9" style="text-align: left; padding-left: 7px;">
                                <asp:DropDownList ID="DropDownList1" runat="server" Style="display: none;">
                                </asp:DropDownList>&nbsp;
                                        <asp:HiddenField ID="hdnClientSelectionId" runat="server" />
                                <div class="btn-group" style="text-align: left;" align="left">
                                    <button type="button" class="btn btn-default dropdown-toggle" style="width: 300px; text-align: left;" align="left"
                                        data-toggle="dropdown" aria-expanded="false" id="btnClientSelectionType" tabindex="1">
                                        --Select Client Selection--
                     
                                                                                    <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlClientSelectionType" style="overflow-y: auto; height: 150px;">
                                    </ul>
                                </div>

                                <div class="radio-custom radio-default radio-inline">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                        <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL" style="white-space: nowrap;"> All Clients</asp:ListItem>
                                        <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED" style="white-space: nowrap; padding-left: 40px;">Selected Only</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                    </asp:Panel>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Customer Name:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="CustomerName" TabIndex="1" CssClass="form-control" MaxLength="45" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="CustomerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerNameLike(1)" TabIndex="2" />
                                <asp:RadioButton ID="CustomerNameLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerNameLike(2)" TabIndex="3" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Customer Ref:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="CustomerRef" TabIndex="4" CssClass="form-control" MaxLength="45" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="CustomerRefLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerRefLike(1)" TabIndex="5" />
                                <asp:RadioButton ID="CustomerRefLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerRefLike(2)" TabIndex="6" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">Job Name:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="JobName" CssClass="form-control" MaxLength="45" TabIndex="7" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="JobNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNameLike(1)" TabIndex="8" />
                                <asp:RadioButton ID="JobNameLike_2" runat="server" Text="Includes" onchange="myFunctionJobNameLike(2)" TabIndex="9" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Job #:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="JobNo" CssClass="form-control" MaxLength="45" TabIndex="10" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="JobNoLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNoLike(1)" TabIndex="11" />
                                <asp:RadioButton ID="JobNoLike_2" runat="server" Text="Includes" onchange="myFunctionJobNoLike(2)" TabIndex="12" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Job Address:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="JobAddress" CssClass="form-control" MaxLength="45" TabIndex="13" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="JobAddressLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobAddressLike(1)" TabIndex="14" />
                                <asp:RadioButton ID="JobAddressLike_2" runat="server" Text="Includes" onchange="myFunctionJobAddressLike(2)" TabIndex="15" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Job City:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="JobCity" CssClass="form-control" MaxLength="20" TabIndex="16" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="JobCityLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobCityLike(17)" TabIndex="17" />
                                <asp:RadioButton ID="JobCityLike_2" runat="server" Text="Includes" onchange="myFunctionJobCityLike(18)" TabIndex="18" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Job State:</label>
                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="JobState" CssClass="form-control" Width="70px" MaxLength="2" TabIndex="19" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Owner Name:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="OwnerName" CssClass="form-control" TabIndex="20" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="OwnerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionOwnerNameLike(1)" TabIndex="21" />
                                <asp:RadioButton ID="OwnerNameLike_2" runat="server" Text="Includes" onchange="myFunctionOwnerNameLike(2)" TabIndex="22" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Contractor Name:</label>
                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="ContractorName" CssClass="form-control" TabIndex="23" />
                        </div>
                        <div class="col-md-4" style="padding-left: 0px;">
                            <div class="radio-custom radio-default radio-inline">

                                <asp:RadioButton ID="ContractorNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionContractorNameLike(1)" TabIndex="24" />
                                <asp:RadioButton ID="ContractorNameLike_2" runat="server" Text="Includes" onchange="myFunctionContractorNameLike(2)" TabIndex="25" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Job Status:</label>
                        <div class="col-md-5" style="text-align: left;">

                            <div id="UlJobStatus">
                            </div>
                            <asp:DropDownList ID="DrpJobStatus" runat="server" Style="display: none;">
                            </asp:DropDownList>&nbsp;
                                        <asp:HiddenField ID="hdnJobStatusSelectedId" runat="server" />

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Deadline Date:</label>
                        <div class="col-md-5" style="display: flex;">
                            <SiteControls:Calendar ID="Calendar3" runat="server" ErrorMessage="From Date"
                                IsReadOnly="false" IsRequired="true" Value="TODAY" TabIndex="27" />
                            &nbsp;&nbsp;Thru&nbsp;
                            <SiteControls:Calendar ID="Calendar4" runat="server"
                                ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" Value="TODAY" TabIndex="28" />

                        </div>
                        <div class="col-md-4">
                            <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                <asp:CheckBox ID="chkByDeadlineDate" CssClass="redcaption"
                                    runat="server" Text=" Use Deadline Date Range"
                                    Width="224px" TabIndex="29" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Job Balance:</label>
                        <div class="col-md-5" style="display: flex;">

                            <cc1:DataEntryBox ID="FromBalance" runat="server" CssClass="form-control" DataType="Money"
                                EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                Value="0.00" Width="120px" TabIndex="30">$1.00</cc1:DataEntryBox>
                            &nbsp; &nbsp;&nbsp;Thru&nbsp;
                                       <cc1:DataEntryBox ID="ThruBalance" runat="server" CssClass="form-control" DataType="Money"
                                           EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                           FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                           Value="0.00" Width="120px" TabIndex="31">$1000.00</cc1:DataEntryBox>


                        </div>
                        <div class="col-md-4">
                            <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                <asp:CheckBox ID="chkByBalance" CssClass="redcaption"
                                    runat="server" Text=" Use Job Balance Range"
                                    Width="224px" TabIndex="32" />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Desk:</label>
                        <div class="col-md-9" style="text-align: left;">

                            <div class="btn-group" style="text-align: left;" align="left">
                                <button type="button" class="btn btn-default dropdown-toggle" style="text-align: left;" align="left"
                                    data-toggle="dropdown" aria-expanded="false" id="btnDesk" tabindex="33">
                                    --Select Desk--
                     
                                                                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlDesk" style="overflow-y: auto; height: 150px;">
                                </ul>
                            </div>
                            <asp:DropDownList ID="DrpDesk" runat="server" Style="display: none;">
                            </asp:DropDownList>&nbsp;
                                        <asp:HiddenField ID="hdnDeskSelectedId" runat="server" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">RA #:</label>
                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="txtRA" CssClass="form-control" TabIndex="34" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">Branch #:</label>
                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="BranchNo" CssClass="form-control" MaxLength="10" TabIndex="35" />
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">Credit Rating:</label>
                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="txtCreditRating" CssClass="form-control" TabIndex="36" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">CRFS #:</label>
                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="txtCRFS" CssClass="form-control" TabIndex="37" />
                        </div>
                    </div>
                </div>
                <div class="footer">
                    <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">
                        <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="btn btn-primary" CausesValidation="False" Style="margin-top: 5px;" />&nbsp;
                                <asp:Button ID="btnViewAll" runat="server" Text="View All Jobs" CssClass="btn btn-primary" CausesValidation="False" Style="margin-top: 5px;" />
                        <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>&nbsp;
                    </asp:Panel>
                </div>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <h1 class="panelheader">RentalView Job List</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-6" style="text-align: left;">
                            <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter" CausesValidation="False"></asp:LinkButton>
                            <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Cust#" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Job State" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Job Desk" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Amount Owed" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Notice Deadline Date" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Lien Deadline Date" Value="6"></asp:ListItem>
                                <asp:ListItem Text="Bond Deadline Date" Value="7"></asp:ListItem>
                                <asp:ListItem Text="SN Deadline Date" Value="8"></asp:ListItem>
                                <asp:ListItem Text="Lien Foreclosure Deadline Date" Value="9"></asp:ListItem>
                                <asp:ListItem Text="Bond Suit Deadline Date" Value="10"></asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                </div>
                <div style="width: auto; height: 550px; overflow: auto;">
                    <asp:GridView ID="gvwJobs" runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 12px !important;">
                        <Columns>

                            <asp:TemplateField HeaderText="">
                                <HeaderTemplate>
                                    <table style="width: 100%;" rules="cols">
                                        <tr>
                                            <td style="width: 6%; word-break: break-all;">CRFS #</td>
                                            <td style="width: 17%; max-width: 18%; word-break: break-all;">Jobsite Id</td>
                                            <td style="width: 17%; max-width: 16%; word-break: break-all;"></td>
                                            <td style="width: 9%; word-break: break-all;"></td>
                                            <td style="width: 9%; word-break: break-all;"></td>
                                            <td style="width: 12%;"></td>
                                            <td colspan="2" style="width: 30%;"></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 6%; word-break: break-all;">Cust #</td>
                                            <td style="width: 17%; max-width: 18%; word-break: break-all;">Jobsite Name</td>
                                            <td style="width: 17%; max-width: 16%; word-break: break-all;">Job Address</td>
                                            <td style="width: 9%; word-break: break-all;">Job Status</td>
                                            <td style="width: 9%; word-break: break-all;">Job Desk</td>
                                            <td style="width: 12%; word-break: break-all;">Amount Owed</td>
                                            <td colspan="2" style="width: 30%; word-break: break-all;">Deadline Dates</td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <HeaderStyle CssClass="RemovePadding" />
                                <ItemStyle CssClass="RemovePadding" />
                                <ItemTemplate>
                                    <table id="TableRentalJobList" style="width: 100%; font-size: 14px;" rules="cols">
                                        <tr>
                                            <td style="width: 6%; word-break: break-all;">
                                                <asp:Label ID="lblJobid" runat="server" Text='<%# Eval("JobId")%>'></asp:Label></td>
                                            <td style="width: 17%; max-width: 18%; word-break: break-all;">
                                                <asp:Label ID="lblJobNum" runat="server" Text='<%# Eval("JobNum")%>'></asp:Label></td>
                                            <td style="width: 17%; max-width: 16%; word-break: break-all;">
                                                <asp:Label ID="lblJobAdd1" runat="server" Text='<%# Eval("JobAdd1")%>'></asp:Label></td>
                                            <td style="width: 9%; word-break: break-all;">
                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("JVStatusCode")%>'></asp:Label></td>
                                           <td style="width: 9%;word-break: break-all;">
                                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("JVDeskNum")%>'></asp:Label></td>
                                            <td style="width: 12%; word-break: break-all;text-align:right;">
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("JobBalance")%>'></asp:Label></td>
                                            <%--'<%# Eval("JobBalance")%>'--%>
                                            <td style="width: 14%; word-break: break-all;"><b style="font-weight: bold;">Notice - </b>
                                                <asp:Label ID="lblNoticeDate" runat="server" Text='<%#Utils.FormatDate(Eval("NOIDeadlineDate"))%>'></asp:Label></td>
                                            <td style="width: 16%; word-break: break-all;"><b style="font-weight: bold;">Foreclose - </b>
                                                <asp:Label ID="lblForceCloseDate" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="word-break: break-all;">
                                                <asp:Label ID="lblCustRefNum" runat="server" Text='<%# Eval("RefNum")%>'></asp:Label></td>
                                            <td style="word-break: break-all;">
                                                <asp:Label ID="lblJobName" runat="server" Text='<%# Eval("JobName")%>'></asp:Label></td>
                                            <td style="word-break: break-all;">
                                                <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityState")%>'></asp:Label></td>
                                            <td style="word-break: break-all;">
                                              </td>
                                                 <td style="word-break: break-all;">
                                              </td>
                                            <td style="word-break: break-all;">
                                                <asp:Label ID="Label10" runat="server" Text=""></asp:Label></td>
                                            <td style="word-break: break-all;"><b style="font-weight: bold;">Lien - </b>
                                                <asp:Label ID="lblLienDate" runat="server" Text='<%#Utils.FormatDate(Eval("LienDeadlineDate"))%>'></asp:Label></td>
                                            <td style="word-break: break-all;"><b style="font-weight: bold;">Bond Suit - </b>
                                                <asp:Label ID="lblBondSuitDate" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                             <td></td>
                                            <td></td>
                                            <td></td>
                                            <td style="word-break: break-all;"><b style="font-weight: bold;">Bond - </b>
                                                <asp:Label ID="lblBondDeadline" runat="server" Text='<%# Utils.FormatDate(Eval("BondDeadlineDate"))%>'></asp:Label></td>
                                            <td style="word-break: break-all;"><b style="font-weight: bold;">SN - </b>
                                                <asp:Label ID="lblSNDeadDate" runat="server" Text='<%#Utils.FormatDate(Eval("SNDeadlineDate"))%>'></asp:Label></td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</div>
