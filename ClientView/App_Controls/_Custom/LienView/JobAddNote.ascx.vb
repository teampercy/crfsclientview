
Partial Class App_Controls_LienView_JobAddNote
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim MYITEM As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
    Public Sub ClearData(ByVal JobId As String)
        Me.ViewState("JobId") = JobId
        Me.Note.Text = ""

    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim s As String = ""

    End Sub
    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Me.ViewState("JobId"))
        'Dim hdnActicetab As New System.Web.UI.WebControls.HiddenField
        'hdnActicetab = Me.Parent.FindControl("hddbTabName")
        'Dim asd As String
        'asd = hdnActicetab.Value
        'hdnActicetab.Value = "Notes"
        'TextBox txtBuyer = (TextBox)this.Parent.FindControl("txtBuyer");
        'Dim hdnTabName As System.Web.UI.hdn  Parent.FindControl("hddbTabName")
        'Session("currenttab") = "NOTES"
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NewCompanyNotSaved", "ActivateTabSet()", True)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsSets", "ActivateTabSet();", True)
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        With mynote
            .JobId = Me.ViewState("JobId")
            .DateCreated = Now()
            .Note = Me.Note.Text
            .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .ClientView = True
            .EnteredByUserId = Me.UserInfo.UserInfo.ID
            .ActionTypeId = 0
            .NoteTypeId = 1
        End With

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
        ClearData(Me.ViewState("JobId"))
        ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsSetas", "ActivateTabSet();", True)
        RaiseEvent ItemSaved()



    End Sub

End Class
