
Partial Class App_Controls__Custom_LienView_JobSearch
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
    Protected WithEvents btnEditItem As LinkButton

    Dim vwclients As HDS.DAL.COMMON.TableView
    Dim myview As HDS.DAL.COMMON.TableView

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)

        If Me.Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            LoadClientList()

            If qs.HasParameter("page") Then
                GetData(GetFilter, 1, "")
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
        If hdnClientSelectionId.Value <> "" Then
            RadioButtonList1.Items.FindByValue("SELECTED").Selected = True
        End If

    End Sub

    Private Sub LoadClientList()

        vwclients = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop

            Me.panClients.Visible = True
            mysproc = GetFilter()
            If mysproc.ClientCode.Length > 4 Then
                Me.DropDownList1.SelectedValue = mysproc.ClientCode
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        With mysproc
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                    .ClientCode = Me.hdnClientSelectionId.Value
                End If
            End If

            .FromReferalDate = Me.Calendar1.Value
            .ThruReferalDate = Me.Calendar2.Value
            If Me.chkByAssignDate.Checked Then
                .ReferalRange = 1
            End If

            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike.SelectedValue.Trim = "Includes" Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                Else
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If
            End If
            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike.SelectedValue.Trim = "Includes" Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                Else
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If
            End If

            If Me.OwnerName.Text.Trim.Length > 0 Then
                If Me.OwnerNameLike.SelectedValue.Trim = "Includes" Then
                    .PropOwner = "%" & Me.OwnerName.Text.Trim & "%"
                Else
                    .PropOwner = Me.OwnerName.Text.Trim & "%"
                End If
            End If
            If Me.GCName.Text.Trim.Length > 0 Then
                If Me.GCNameLike.SelectedValue.Trim = "Includes" Then
                    .GeneralContractor = "%" & Me.GCName.Text.Trim & "%"
                Else
                    .GeneralContractor = Me.GCName.Text.Trim & "%"
                End If
            End If

            If Me.LenderName.Text.Trim.Length > 0 Then
                If Me.LenderNameLike.SelectedValue.Trim = "Includes" Then
                    .LenderName = "%" & Me.LenderName.Text.Trim & "%"
                Else
                    .LenderName = Me.LenderName.Text.Trim & "%"
                End If
            End If

            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike.SelectedValue.Trim = "Includes" Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                Else
                    .JobName = Me.JobName.Text.Trim & "%"
                End If
            End If

            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike.SelectedValue.Trim = "Includes" Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                Else
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If
            End If

            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNameLike.SelectedValue.Trim = "Includes" Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                Else
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If
            End If

            If Me.JobCity.Text.Trim.Length > 0 Then
                .JobCity = Me.JobCity.Text.Trim & "%"
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            End If

            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim
            End If

            If Me.JobStatus.Text.Trim.Length > 0 Then
                .StatusCode = Me.JobStatus.Text.Trim & "%"
            End If

            If Me.FileNumber.Text.Trim.Length > 0 Then
                .JobId = Me.FileNumber.Text.Trim
            End If

            If Me.chkPaidInFull.Checked = True Then
                .ExcludePIF = 1
            End If

            .UserId = Me.CurrentUser.id

        End With

        GetData(mysproc, 1, "F")
        Me.MultiView1.SetActiveView(Me.View2)

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAll.Click
        Dim s As String = "~/MainDefault.aspx?lienview.NewJobList"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
    Protected Function GetViewURL1(ByVal AITEMID As String) As String
        Dim QS As New HDS.WEBLIB.Common.QueryString(Me.Page)
        mysproc = GetFilter()
        QS.RemoveParameter("PrintId")
        Dim s As String = "212" & "I"
        QS.SetParameter("plid", s)
        QS.SetParameter("page", mysproc.Page)
        QS.SetParameter("ItemId", AITEMID)
        Return QS.All
    End Function

    Protected Sub btnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "F")

    End Sub
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "P")

    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "N")

    End Sub
    Protected Sub btnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "L")

    End Sub

    ''we are using jqeury datatable paging

    'Protected Sub ddPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddPage.SelectedIndexChanged
    '    GetData(GetFilter, ddPage.SelectedIndex, "I")

    'End Sub
    Protected Function GetFilter() As Object
        If IsNothing(Session("JobListP")) = False Then
            Return Session("JobListP")
        Else
            Return New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
        End If
    End Function
    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1, ByVal apage As Integer, ByVal adirection As String)
        myview = TryCast(Me.Session("JobList"), HDS.DAL.COMMON.TableView)

        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        asproc.PageRecords = 10000 '25 Changed by jaywanti
        asproc.UserId = Me.CurrentUser.Id
        If adirection = "F" Then
            asproc.Page = 1
        End If
        If adirection = "N" Then
            asproc.Page += 1
        End If
        If adirection = "P" Then
            asproc.Page -= 1
        End If

        ''jqeury datatable paging
        'If adirection = "I" Then
        '    asproc.Page = ddPage.SelectedIndex + 1
        'End If

        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        If adirection = "L" Then
            myview = Session("JobList")
            asproc.Page = myview.RowItem("pages")
        End If

        If adirection.Length < 1 And IsNothing(myview) = False Then
            myview = Session("JobList")
        Else
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
        End If

        If myview.Count < 1 Then
            Exit Sub
        End If

        Session("JobListP") = asproc
        Session("JobList") = myview

        'Dim totrecs As String = myview.RowItem("totalrecords")
        'Dim currpage As String = myview.RowItem("page")
        'Dim totpages As Integer = (totrecs / 14 + 1)
        'totpages = myview.RowItem("pages")

        'Me.lblCurrentPage.Text = currpage
        'Me.lblTotalPages.Text = totpages
        'Me.lblTotRecs.Text = totrecs
        'Dim i As Integer = 0

        'ddPage.Items.Clear()
        'Do Until i > totpages - 1
        '    i += 1
        '    ddPage.Items.Add(i.ToString)
        'Loop

        'ddPage.SelectedIndex = currpage - 1
        Me.gvwList.DataSource = myview
        Me.gvwList.DataBind()
        Me.MultiView1.SetActiveView(Me.View2)

        Me.Session("joblist") = myview

    End Sub
    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = DR("JobId")

            btnEditItem.Attributes("batchrowno") = DR("BatchJobId")

            e.Row.Cells(1).Text = "" & Strings.Left(e.Row.Cells(1).Text, 7) & ""

            e.Row.Cells(2).Text = "" & Strings.Left(e.Row.Cells(2).Text, 25) & ""
            e.Row.Cells(2).Wrap = False

            e.Row.Cells(3).Text = "" & Strings.Left(e.Row.Cells(3).Text, 7) & ""
            e.Row.Cells(3).Wrap = False

            e.Row.Cells(4).Text = "" & Strings.Left(e.Row.Cells(4).Text, 20) & ""
            e.Row.Cells(4).Wrap = False

            e.Row.Cells(5).Text = "" & Strings.Left(e.Row.Cells(5).Text, 5) & ""
            e.Row.Cells(5).Wrap = False

            e.Row.Cells(6).Text = "" & Strings.Left(e.Row.Cells(6).Text, 5) & ""
            e.Row.Cells(6).Wrap = False

            e.Row.Cells(7).Text = "" & Strings.Left(e.Row.Cells(7).Text, 15) & ""
            e.Row.Cells(7).Wrap = False

            e.Row.Cells(8).Text = "" & Utils.FormatDate(DR("DateAssigned")) & ""
            e.Row.Cells(8).Wrap = False


        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.TableSection = TableRowSection.TableHeader
        End If

    End Sub
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_LiensDataEntry_CloneJob
        mysproc.jobid = myitemid
        mysproc.submittedbyuserid = Me.CurrentUser.Id
        mysproc.TempKey = CRF.CLIENTVIEW.BLL.ProviderBase.GetUniqueId
        Dim myretid As Integer = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecScalar(mysproc)
        Dim MYSQL As String = "Select Id From BatchJob where batchid = - 1 "
        MYSQL += " And SubmittedByuserId = '" & Me.CurrentUser.Id & "' "
        MYSQL += " And CloneJobId = '" & myitemid & "' "
        MYSQL += " And TempKey = '" & mysproc.TempKey & "' "
        Dim myds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetDataSet(MYSQL)
        Dim mynewjobid = myds.Tables(0).Rows(0).Item("Id")
        Dim s As String = "~/MainDefault.aspx?lienview.newjobnew&itemid=" & mynewjobid
        ' Dim s As String = "~/MainDefault.aspx?lienview.newjobnew&itemid=-1&CopyJobId=" & myitemid

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
End Class

