
Partial Class App_Controls__Custom_LienView_JobVerifiedSentReq
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobVerifiedSentRequest
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Public Sub ClearData(ByVal JobId As String)
        Me.ViewState("JobId") = JobId
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobId, myjob)
        myreq = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobVerifiedSentRequest
        Dim MYSQL As String = "Select * from JobVerifiedSentRequest where isprocessed = 0 and JobId = '" & myjob.Id & "' "
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetItem(MYSQL, myreq)
        With myreq
            Me.AmountOwed.Value = .AmountOwed
            Me.MonthDebtIncurred.Value = .MonthDebtIncurred
            Me.StmtofAccts.Text = .StmtOfAccts
        End With
      
    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim s As String = ""

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.ViewState("JobId"), myjob)
        myreq = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobVerifiedSentRequest
        Dim MYSQL As String = "Select * from JobVerifiedSentRequest where isprocessed = 0 and JobId = '" & myjob.Id & "' "
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetItem(MYSQL, myreq)

        With myreq
            .JobId = myjob.Id
            .DateCreated = Now()
            .AmountOwed = Me.AmountOwed.Value
            .MonthDebtIncurred = Me.MonthDebtIncurred.Value
            .StmtOfAccts = Me.StmtofAccts.Text
            .SubmittedBy = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .SubmittedByUserId = Me.CurrentUser.Id
        End With

        If myreq.Id < 1 Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myreq)
        Else
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myreq)
        End If
        
        ClearData(Me.ViewState("JobId"))

        RaiseEvent ItemSaved()

    End Sub

    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Me.ViewState("JobId"))
        RaiseEvent ItemSaved()

    End Sub
End Class
