
Partial Class App_Controls__Custom_LienView_JobEditTReq
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim ItemId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Me.ItemId = qs.GetParameter("ItemId")
        myjob = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobById(ItemId)
        Me.litJobInfo.Text = " Texas Request for CRF# (" & myjob.Id & ") "
        Me.litJobInfo.Text += " Job# (" & myjob.JobNum.Trim & ") "
        Me.litJobInfo.Text += " JobName (" & myjob.JobName.Trim & ") "

        If Me.Page.IsPostBack = False Then
            If qs.HasParameter("SubId") Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
                Me.IsTX60Day.Checked = True
                If myreq.Id > 0 Then
                    With myreq
                        Me.AmountOwed.Value = .AmountOwed
                        Me.IsTX60Day.Checked = .IsTX60Day
                        Me.IsTX90Day.Checked = .IsTX90Day
                        Me.MonthDebtIncurred.Value = .MonthDebtIncurred
                        Me.StmtofAccts.Text = .StmtOfAccts
                    End With
                End If
            End If

        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.Page.Validate()
        If Page.IsValid = False Then
            Exit Sub
        End If

        Me.CustomValidator1.ErrorMessage = ""
        Me.CustomValidator1.IsValid = True
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If qs.HasParameter("SubId") Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
        End If
        If (Me.MonthDebtIncurred.Value = "") Then
            Me.CustomValidator1.ErrorMessage = "Month/Year Work Performed is required for this notice request"
            Me.CustomValidator1.IsValid = False
            Exit Sub
        End If
        With myreq
            .JobId = ItemId
            .DateCreated = Now()
            .AmountOwed = Me.AmountOwed.Value
            .IsTX60Day = Me.IsTX60Day.Checked
            .IsTX90Day = Me.IsTX90Day.Checked
            .MonthDebtIncurred = Me.MonthDebtIncurred.Value
            .StmtOfAccts = Me.StmtofAccts.Text
            .SubmittedBy = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .SubmittedByUserId = Me.CurrentUser.Id
        End With

        Dim MYSQL As String = " SELECT * FROM JOBTEXASREQUEST WHERE DATEPROCESSED Is NULL"
        MYSQL += " And JobId = " & myreq.JobId
        MYSQL += " And Id <> " & myreq.Id
        If Me.IsTX60Day.Checked = True Then
            MYSQL += " And IsTx60day = 1"
        Else
            MYSQL += " And IsTx90day = 1"
        End If

        Dim MYVIEW As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView(MYSQL)
        If myjob.FederalJob = True Then
            Me.CustomValidator1.ErrorMessage = "This is a Federal Job and requires a Miller Act Notice 90 days after last furnishing. Please call our Client Service Department at 800-522-3858 for assistance."
            Me.CustomValidator1.IsValid = False
            Exit Sub
        End If
        If MYVIEW.Count > 0 Then
            If Me.IsTX60Day.Checked = True Then
                Me.CustomValidator1.ErrorMessage = "You already have a 60 DAY request for this Job"
                Me.CustomValidator1.IsValid = False
                Exit Sub
            Else
                Me.CustomValidator1.ErrorMessage = "You already have a 90 DAY request for this Job"
                Me.CustomValidator1.IsValid = False
                Exit Sub
            End If
        End If


        If myjob.PublicJob = True And Me.IsTX90Day.Checked And Me.StmtofAccts.Text.Length < 4 Then
            Me.CustomValidator1.ErrorMessage = "A statement of account is required for this notice request"
            Me.CustomValidator1.IsValid = False
            Exit Sub
        End If

        ' 8/10/2015 - LEC   
        If myjob.PrivateJob = True And myjob.ResidentialBox = True And Me.IsTX60Day.Checked Then
            Me.CustomValidator1.ErrorMessage = "For a residential construction project in Texas, you must send the owner and the GC a notice of the unpaid balance by the 15th of the 2nd month.  Please select the 3rd month notice for this purpose as the 2nd month notice to the GC 'only' is not required or appropriate. TX Sec. 53.252"
            Me.CustomValidator1.IsValid = False
            Exit Sub
        End If
        If myreq.Id > 0 Then
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
        Else
            myreq.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myreq)
            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = ItemId
                .DateCreated = Now()
                .Note = "Notice request made. Request is now being processed."
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)

            End With
        End If

        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&view=" & "TREQ"
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
   
    Private Function setdate(ByVal adate As String) As String
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return ""
        End If
        If s = Date.MinValue Then
            Return ""
        End If
        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Private Function getdate(ByVal adate As String) As Date
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return Date.MinValue
        End If

        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&view=" & "TREQ"
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
End Class
