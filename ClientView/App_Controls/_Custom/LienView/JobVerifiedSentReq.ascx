<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobVerifiedSentReq.ascx.vb" Inherits="App_Controls__Custom_LienView_JobVerifiedSentReq" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<asp:LinkButton ID="btnAddNew" CssClass="button" runat="server" Text="Job Verified"  />
<asp:Panel ID="panCustNameSuggest" runat="server" Style="display: none"
    Width="400px" Font-Bold="True">
<div id="modcontainer"  style="MARGIN:  0px  0px 0px 0px; width:600px; ">
<h1 class="panelheader" >Job Verified Request</h1>
<div class="body">
<asp:LinkButton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " CausesValidation="False"  />
<br /><br />
<TABLE align="center" style="WIDTH: 600px">
			
			<TR>
				<TD class="row-label" style="WIDTH: 200px">Amount Owed:</TD>
				<TD class="row-data">
					<cc1:DataEntryBox id="AmountOwed" runat="server" Width="136px" DataType="Money"></cc1:DataEntryBox>
				</TD>
			</TR>
			<TR>
				<TD class="row-label" style="WIDTH: 200px">Month Worked Performed:</TD>
				<TD class="row-data">
					<cc1:DataEntryBox id="MonthDebtIncurred" runat="server" Width="136px" DataType="Any"></cc1:DataEntryBox>
				</TD>
			</TR>
</TABLE>
<TABLE align="center" style="WIDTH: 600px">
		<TR>
		<TD style="width: 587px;">
		    <H2>Enter Account Statement Info:</H2>
             <cc1:DataEntryBox ID="StmtofAccts" runat="server" DataType="Any" Height="200px" Tag="StmtofAccts"
                    TextMode="MultiLine" Width="575px"></cc1:DataEntryBox>
        </TD>
		</TR>
</TABLE>   
</div>
<div class="footer">
<asp:LinkButton ID="btnSave" CssClass="button" runat="server" Text="Save" />&nbsp;&nbsp;
<br />
<asp:HiddenField ID="BatchJobId" runat="server" /><asp:HiddenField ID="ItemId" runat="server" />
</div>
</div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID=""  OkControlID="" DropShadow="true"  
    PopupControlID="panCustNameSuggest" TargetControlID="btnAddNew">
</ajaxToolkit:ModalPopupExtender>
