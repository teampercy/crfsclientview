<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewJobList.ascx.vb" Inherits="App_Controls_crfLienView_NewJobList" %>
<%@ Register Src="AddLegalPty.ascx" TagName="AddLegalPty" TagPrefix="uc3" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Register Src="GCSearch.ascx" TagName="GCSearch" TagPrefix="uc2" %>

<script type="text/javascript">
    // $('#liJobs').addClass('site-menu-item has-sub open');
    //$(window).load(function () {
    //   // alert('hi');
    //    $('#liJobs').addClass('site-menu-item has-sub open');
    //});
    $(document).ready(function () {
        CallSuccessFuncJobList();
    });

    function DeleteAccount(id) {
        document.getElementById('<%=hdnJobId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteJobId.ClientID%>').click();
      });
        return false;
    }
    function CallSuccessFuncJobList() {
        //debugger;
        // alert('hiab');

        //StartLoading();
        //debugger;
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [15, 25, 35, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
           
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-4'il><'col-sm-7'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },
            //data: response,
            //columns: [
            //    { data: "CRFS" },//CRFS#</th>
            //    { data: "JobName" },//JOB NAME</th>
            //    { data: "Job" },//JOB#</th>
            //    { data: "JobAdd" },//JOB ADD</th>
            //    { data: "Branch" },//BRANCH</th>
            //    { data: "Stat" },//STAT</th>
            //    { data: "Customer" },//CUSTOMER</th>
            //    { data: "Assigned" },//ASSIGNED</th>
            //    { data: "Client" } //CLIENT#</th>
            //]
        };
        // debugger;
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvItemList.ClientID%>').dataTable(options);
        //$('#ctl25_ctl00_gvItemList').dataTable(options);
        //debugger;
        //  EndLoading();

    }
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Jobs', 'Place Jobs');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubPlaceJobs');
        //$('#liJobs').addClass('site-menu-item has-sub active open');
        //$('#lisubPlaceJobs').addClass('site-menu-item active');
        return false;

    }
  
</script>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server" 
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetNewJobList" 
    TypeName="CRF.CLIENTVIEW.BLL.LienView.Provider">
    <SelectParameters>
        <asp:ControlParameter ControlID="HiddenField1" DefaultValue="-1" Name="userid" PropertyName="Value"
            Type="String" />
        <asp:SessionParameter DefaultValue="" Name="filter" SessionField="filter" Type="String" />
        <asp:SessionParameter Name="sort" SessionField="sort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>


<div id="modcontainer" style="MARGIN: 10px  0px 0px 0px; width: 100%;">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1 class="panelheader">New Job List</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12" style="text-align:left;">
                            <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Enter New Job" />
                               <asp:Button ID="btnPrint" runat="server" Text="Print Submitted" CssClass="btn btn-primary" />
                                    <asp:Button ID="btnCopy" runat="server" Text="Copy Existing Job" CssClass="btn btn-primary" OnClick="btnCopy_Click" />
                               <cc1:DataEntryBox ID="txtFindText" runat="server" Text="" CssClass="form-control" style="display:none" />
                              <asp:Button ID="btnFindText" runat="server" Text="Search" CssClass="btn btn-primary"  style="display:none" />
                              <asp:Button ID="btnRefresh" runat="server" Text="Show All" CssClass="btn btn-primary"  style="display:none" />
                        </div>
                  <%--      <div class="col-md-2">                
                        </div>
                        <div class="col-md-2">
             
                        </div>
                           <div class="col-md-2">
                         
                        </div>
                        <div class="col-md-1">
                          
                        </div>
                        <div class="col-md-2">
                          
                        </div>--%>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:UpdatePanel ID="upItemList" runat="server">
                                <ContentTemplate>
                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:GridView ID="gvItemList" runat="server" DataSourceID="ItemsDataSource"
                                            CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                            BorderWidth="1px" Width="100%" Style="min-width:1200px">
                                            <%--           <RowStyle CssClass="rowstyle" />
                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                        <HeaderStyle CssClass="headerstyle" />
                                        <PagerStyle CssClass="pagerstyle" />
                                        <PagerTemplate>
                                            <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Value="5" />
                                                <asp:ListItem Value="10" />
                                                <asp:ListItem Value="15" />
                                                <asp:ListItem Value="20" />
                                            </asp:DropDownList>
                                            &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                            of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                            &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                            <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                            <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                            <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                                        </PagerTemplate>--%>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Edit/Print/Copy">
                                                    <ItemTemplate>


                                                        <asp:LinkButton ID="btnEditItem" CssClass="icon ti-write" runat="server" />
                                                        <%--	<asp:ImageButton ID="btnEditItem1" CssClass="icon ti-printer"  runat="server" />--%>
                                                                                            &nbsp;
                                                                                                <asp:LinkButton ID="btnPrintItem" CssClass="icon ti-printer" runat="server" />
                                                        <%--	<asp:ImageButton ID="btnPrintItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server"  />--%>
                                                                                            &nbsp;
                                                                                                <asp:LinkButton ID="btnCopyItem" CssClass="icon ti-layers" runat="server" />
                                                        <%--	    <asp:ImageButton ID="btnCopyItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/page.gif" runat="server"  />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="85px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>


                                                <asp:BoundField DataField="JobName" SortExpression="JobName" HeaderText="Job Name">
                                                    <ItemStyle Width="110px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="JobNum" SortExpression="JobNum" HeaderText="Job#">
                                                    <ItemStyle Width="60px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="JobAdd1" SortExpression="JobAdd1" HeaderText="Address">
                                                    <ItemStyle Width="110px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="JobState" SortExpression="JobState" HeaderText="State">
                                                    <ItemStyle HorizontalAlign="Center" Width="45px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CustName" SortExpression="CustName" HeaderText="Customer">
                                                    <ItemStyle Width="110px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EstBalance" SortExpression="EstBalance" HeaderText="Est Bal$">
                                                    <ItemStyle HorizontalAlign="Right" Width="60px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                           
                                                <asp:TemplateField HeaderText="Del">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                        <%-- <asp:ImageButton ID="btnDeleteItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')" />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15px" />
                                                </asp:TemplateField>

                                            </Columns>
                                            <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="btnFindText" EventName="Click" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>




                <ajaxToolkit:UpdatePanelAnimationExtender ID="UpdatePanelAnimationExtender1" runat="server" TargetControlID="upItemList">
                    <Animations>
                    <OnUpdating>
                        <Parallel Duration="0">
                            <EnableAction AnimationTarget="btnFindText" Enabled="false" />
                            
                            <FadeOut MinimumOpacity=".5" />
                        </Parallel>
                    </OnUpdating>
                    <OnUpdated>
                        <Parallel Duration="0">
                            <FadeIn MinimumOpacity=".5" />
                            <EnableAction AnimationTarget="btnFindText" Enabled="true" />
                        </Parallel>                    
                    </OnUpdated>
                    </Animations>
                </ajaxToolkit:UpdatePanelAnimationExtender>

            </div>
            <div class="footer">
                <asp:HiddenField ID="hdnJobId" runat="server" Value="0" />
                <asp:Button ID="btnDeleteJobId" runat="server" Style="display: none;" OnClick="btnDeleteJobId_Click" />
                <br />
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12">

                            <asp:Button ID="btnCancel1" runat="server" CssClass="btn btn-primary" Text="Back to List" CausesValidation="False" OnClick="btnCancel1_Click"></asp:Button>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">

                            <div style="width: auto; height: auto; overflow: auto;">
                                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="HiddenField2" runat="server" />
<asp:HiddenField ID="custid" runat="server" />
<asp:HiddenField ID="gcid" runat="server" />
<asp:HiddenField ID="batchjobid" runat="server" />


<asp:UpdateProgress ID="siteUpdateProgress" runat="server">
    <ProgressTemplate>
        <div class="TransparentGrayBackground"></div>
        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
            <div class="PageUpdateProgress">
                <asp:Image ID="ajaxLoadNotificationImage"
                    runat="server"
                    ImageUrl="~/images/ajax-loader.gif"
                    AlternateText="[image]" />
                &nbsp;Please Wait...
            </div>
        </asp:Panel>
        <ajaxToolkit:AlwaysVisibleControlExtender
            ID="AlwaysVisibleControlExtender1"
            runat="server"
            TargetControlID="alwaysVisibleAjaxPanel"
            HorizontalSide="Center"
            HorizontalOffset="150"
            VerticalSide="Middle"
            VerticalOffset="0">
        </ajaxToolkit:AlwaysVisibleControlExtender>
    </ProgressTemplate>
</asp:UpdateProgress>


