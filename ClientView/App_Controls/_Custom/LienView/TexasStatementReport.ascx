<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) handles MyBase.Load
         If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Calendar1.Value = Today
            Me.Calendar2.Value = Today
    
            LoadClientList()
            If IsNothing(Me.Page.Request.QueryString("rpt")) = False Then
                Me.DownLoadReport(Me.Page.Request.QueryString("rpt"))
            End If
        Else
             ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
       
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasStatements
        With MYSPROC
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                End If
            End If
            If Me.BranchNum.Text.Trim.Length > 1 Then
                .BranchNum = Me.BranchNum.Text.Trim
            End If
            .FromDate = ME.Calendar1.Value
            .ThruDate = ME.Calendar2.Value
            .UserId = Me.CurrentUser.id
        End With
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetTexasStatementReport(MYSPROC, True)
        ' Dim sreport As String = Nothing
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        
    End Sub

    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
                
    End Sub
    
    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DownLoadReport(Me.Session("spreadsheet"))
    End Sub
 
    Private Sub LoadClientList()
       
        Dim vwclients As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop
            Me.panClients.Visible = True
        End If
    End Sub
</script>
<div id="modcontainer" style="MARGIN: 10px; WIDTH: 650px">
<h1 class="panelheader" >Texas Statement Report</h1>
<asp:MultiView ID="MultiView1" runat="server">
<asp:View ID="View1" runat="server">
<div class="body" >
<asp:Panel ID="panClients" Visible="false" runat="server" >
        <table width="590">
 			<tr>
				<td class="row-label" style="WIDTH: 200px">
                    Client Select:</td>
				<td class="row-data" style="width: 490px">
                    <asp:DropDownList ID="DropDownList1" runat="server" Width="250px">
                    </asp:DropDownList>&nbsp;<asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                        RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1" Width="224px">
                        <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL">All Clients</asp:ListItem>
                        <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED">Selected Only</asp:ListItem>
                    </asp:RadioButtonList>
                 </td>
			</tr>
    </table>

</asp:Panel>
<TABLE align="center" width="490">
<tr>
	<td class="row-label" style="WIDTH: 200px">
        Branch Filter:</td>
	<td class="row-data" style="width: 490px">
        <asp:TextBox ID="BranchNum" runat="server"></asp:TextBox>
    </td>
</tr>
<TR>
	<TD class="row-label" style="width: 30% " align="right">From Date:</TD>
	<TD class="row-data" style="width: 200px">
        <SiteControls:Calendar  ID="Calendar1" Value="TODAY" IsReadOnly=false IsRequired=true ErrorMessage="From Date" runat="server" />
  	</TD>
</TR>
<TR>
	<TD class="row-label" style="width: 30% " align="right">Through Date:</TD>
	<TD class="row-data" style="width: 200px">
 	  <SiteControls:Calendar  ID="Calendar2" Value="TODAY"  IsReadOnly=false IsRequired=true ErrorMessage="Thru Date" runat="server" />
   	</TD>
</TR>
</TABLE>
<asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="The Following Field(s) are Required: " />
</div>
<div class="footer" >
<asp:UpdatePanel ID="updPan1" runat="server">
<ContentTemplate>
<asp:LinkButton ID="btnSubmit" 
                                cssclass="button"
                                runat="server" 
                                 Text="Create Report" OnClick="btnSubmit_Click" />&nbsp;
<br /><br />

<asp:UpdateProgress ID="siteUpdateProgress" runat="server">
<ProgressTemplate>
<div class="TransparentGrayBackground"></div>
<asp:Panel  ID="alwaysVisibleAjaxPanel" runat="server" >
<div class="PageUpdateProgress">
<asp:Image  ID="ajaxLoadNotificationImage" 
runat="server" 
ImageUrl="~/images/ajax-loader.gif" 
AlternateText="[image]" />
&nbsp;Please Wait...
</div>
</asp:Panel>
<ajaxToolKit:AlwaysVisibleControlExtender 
ID="AlwaysVisibleControlExtender1" 
runat="server"
TargetControlID="alwaysVisibleAjaxPanel"
HorizontalSide="Center"
HorizontalOffset="150"
VerticalSide="Middle"
VerticalOffset="0"
>
</ajaxToolKit:AlwaysVisibleControlExtender>
</ProgressTemplate>
</asp:UpdateProgress>
</ContentTemplate>
</asp:UpdatePanel>
</div>
</asp:View>
<asp:View ID="View2" runat="server">
<div class="body" >

<asp:LinkButton ID="Linkbutton2" runat="server" CssClass="button" Text="Back to Selection" OnClick="Linkbutton2_Click"></asp:LinkButton>
<br /><br />
 <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder> 
<br />
<center>
    &nbsp;</center>
</div>
</asp:View>
</asp:MultiView>
</div>
