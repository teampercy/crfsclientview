﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<style>
    .panel-body, .form-control.input-sm {
        font-size: 11px;
    }

    #ctl32_ctl00_gvJobViewList tbody tr:hover {
        cursor: pointer;
    }

    #ctl32_ctl00_gvJobViewList thead tr th {
        font-weight: bold;
    }

    #ctl32_ctl00_gvJobViewList thead tr th, #ctl32_ctl00_gvJobViewList tbody tr td {
        /*border: 0;*/
    }

    #JobFilterBody .radio-custom label {
        width: 75px;
    }

    #JobFilterBody .radio-custom.radio-default.radio-inline {
        min-height: 0px;
        padding-top: 0px;
    }

    .RemovePadding {
        padding: 0px !important;
    }
    .nowordwraptd{
        padding-left: 5px;
white-space: nowrap;
text-overflow: ellipsis;
overflow: hidden;
    }
</style>
<script type="text/javascript">
   function modalshow() {
        debugger;
        // alert("hii");
        $("#modelDivExport").modal('show');
        return false;
    }
    function modalhide() {
        //alert("hide");

        //alert($('input[type=radio]:checked').val());
        if ($('input[type=radio]:checked').val() == "PDFReport")
        {
            //$("#LodingPanel").show();
        }
        else
        {
            HideLoadingPanel();
            
        }
       
        $("#modelDivExport").modal('hide');
        document.getElementById("<%= btnExportReport.ClientID %>").click();
        return false;
    }
    function MaintainMenuOpen() {
        // SetHeaderBreadCrumb('RentalView', 'View All Jobs', 'View Jobs');
        SetHeaderBreadCrumb('LienView', 'Jobs', 'Texas Job Review');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubTexasJobReviewLienView')
        return false;

    }
  
    function rebindDataTable() {
        $(document).ready(function () {
            CallTexasReviewSuccessFunc();
        });
    }



    function HideLoadingPanel() {
        //alert("hii");
        $(".TransparentGrayBackground").hide();

        $(".PageUpdateProgress").hide();
    }


   function CallTexasReviewSuccessFunc() {

        var defaults = {

            "stateSave": true,
            "stateDuration": 60 * 10,
            "searching": false,
            "lengthMenu": [5, 15, 20, 25, 35, 50, 100],
            "pageLength": 20,
            bAutoWidth: false,
            responsive: true,
            "bSort": false,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true }

            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                     "<'row'<'col-sm-4'il><'col-sm-7'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                },

            }
        };
        var options = $.extend(true, {}, defaults, $(this).data());
        var gvJobViewListdiv = $('#' + '<%=gvJobViewList.ClientID%>');

       
        gvJobViewListdiv.dataTable(options);


    }
</script>
<script runat="server">
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTXReviewJobList

    Protected WithEvents btnEditItem As LinkButton
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)

        If Me.Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)

            mysproc.JobState = "TX" 'Priya
            mysproc.UserId = Me.CurrentUser.Id
            mysproc.Page = 1
            Session("mysprocTX") = mysproc
            GetData(mysproc, 1, "F")
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js3", "rebindDataTable();", True)
            If gvJobViewList.Rows.Count > 0 Then
                Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
            End If

        End If

    End Sub

    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTXReviewJobList, ByVal apage As Integer, ByVal adirection As String)
        myview = TryCast(Me.Session("JobList"), HDS.DAL.COMMON.TableView)

        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        asproc.PageRecords = 2500
        asproc.UserId = Me.CurrentUser.Id
        asproc.JobState = "TX"
        If adirection = "F" Then
            asproc.Page = 1
        End If
        If adirection = "N" Then
            asproc.Page += 1
        End If
        If adirection = "P" Then
            asproc.Page -= 1
        End If

        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        If adirection = "L" Then
            myview = Session("JobList")
            asproc.Page = myview.RowItem("pages")
        End If

        If adirection.Length < 1 And IsNothing(myview) = False Then
            myview = Session("JobList")
        Else
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
        End If

        If myview.Count < 1 Then
            Me.gvJobViewList.DataSource = Nothing
            Me.gvJobViewList.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasJob", "CallTexasReviewSuccessFunc();", True)
            Exit Sub
        End If

        Session("JobViewListP") = asproc
        Session("JobList") = myview

        Dim i As Integer = 0

        Me.gvJobViewList.DataSource = myview
        Me.gvJobViewList.DataBind()
        If gvJobViewList.Rows.Count > 0 Then
            Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        Me.Session("joblist") = myview

    End Sub

    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myitemid & "&list=tjr" & "&PageName=TexasJobReview"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = DR("JobId")
        End If
    End Sub

    Protected Sub SortJobList()
        Try
            If Not Me.Session("joblist") Is Nothing Then
                myviewSorting = Session("joblist")
            Else

                Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTXReviewJobList
                obj.Page = 1
                obj.PageRecords = 2500 '500 Changed by jaywanti
                obj.UserId = Me.CurrentUser.Id
                obj.JobState = "TX"        'Harcoded added by priyanka
                myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
                Me.Session("joblist") = myviewSorting
            End If

            Dim SortType As String = "ASC"
            If drpSortType.SelectedItem.Value = "-1" Then
                SortType = "ASC"
            ElseIf drpSortType.SelectedItem.Value = "1" Then
                SortType = "DESC"
            End If

            Dim SortBy As String = ""
            If drpSortBy.SelectedItem.Value = "-1" Then
                SortBy = "ListId " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "1" Then
                SortBy = "JobId " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "2" Then
                SortBy = "JobNum " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "3" Then
                SortBy = "JobName " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "4" Then
                SortBy = "JobAdd1 " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "5" Then
                SortBy = "JobCity " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "6" Then
                SortBy = "JobState " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "7" Then
                SortBy = "CustId " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "8" Then
                SortBy = "ClientCustomer " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "9" Then
                SortBy = "BranchNum " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "10" Then
                SortBy = "FilterKey " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "11" Then
                SortBy = "NoticeSent " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "12" Then
                SortBy = "NoticeDeadlineDate " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "13" Then
                SortBy = "LienDeadlineDate " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "14" Then
                SortBy = "BondDeadlineDate " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "15" Then
                SortBy = "SNDeadlineDate " + SortType
            ElseIf drpSortBy.SelectedItem.Value = "16" Then
                SortBy = "NOIDeadlineDate " + SortType
            End If

            myviewSorting.Sort = SortBy

            If (SortType = "ASC") Then
                If (drpSortBy.SelectedItem.Value = "15" Or drpSortBy.SelectedItem.Value = "16" Or drpSortBy.SelectedItem.Value = "11" Or drpSortBy.SelectedItem.Value = "12" Or drpSortBy.SelectedItem.Value = "13" Or drpSortBy.SelectedItem.Value = "14") Then
                    If (drpSortBy.SelectedItem.Value = "11") Then
                        myviewSorting = SetNullDate("NoticeSent", myviewSorting)
                    ElseIf (drpSortBy.SelectedItem.Value = "12") Then
                        myviewSorting = SetNullDate("NoticeDeadlineDate", myviewSorting)
                    ElseIf (drpSortBy.SelectedItem.Value = "13") Then
                        myviewSorting = SetNullDate("LienDeadlineDate", myviewSorting)
                    ElseIf (drpSortBy.SelectedItem.Value = "14") Then
                        myviewSorting = SetNullDate("BondDeadlineDate", myviewSorting)
                    ElseIf (drpSortBy.SelectedItem.Value = "15") Then
                        myviewSorting = SetNullDate("SNDeadlineDate", myviewSorting)
                    ElseIf (drpSortBy.SelectedItem.Value = "16") Then
                        myviewSorting = SetNullDate("NOIDeadlineDate", myviewSorting)
                    End If
                End If
            End If
            Me.gvJobViewList.DataSource = myviewSorting
            Me.gvJobViewList.DataBind()

            If gvJobViewList.Rows.Count > 0 Then
                Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasJob", "CallTexasReviewSuccessFunc();", True)

        Catch ex As Exception

        End Try
    End Sub


    Protected Function SetNullDate(ByVal DateField As String, ByVal viewcheckdate As HDS.DAL.COMMON.TableView)
        For Each dr As System.Data.DataRow In viewcheckdate.ToTable.Rows
            If (dr(DateField).ToString = "2079-01-01 00:00:00.000") Then
                dr(DateField) = DBNull.Value

            End If

        Next
        Return viewcheckdate
    End Function



    'Protected Sub gvJobViewList_RowDataBound(sender As Object, e As GridViewRowEventArgs)
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        'Dim lbl As Label = TryCast(e.Row.FindControl("lblJobid"), Label)
    '        'If lbl.Text <> "" Then
    '        '    lbl.ForeColor=Drawing.Color.Blue

    '        'End If
    '        Dim lblNDLDate As Label = TryCast(e.Row.FindControl("lblNoticeDate"), Label)
    '        Dim lblLienDate As Label = TryCast(e.Row.FindControl("lblLienDate"), Label)
    '        Dim lblBondDate As Label = TryCast(e.Row.FindControl("lblBondDate"), Label)
    '        Dim lblSNDate As Label = TryCast(e.Row.FindControl("lblSNDate"), Label)
    '        Dim lblForeClose As Label = TryCast(e.Row.FindControl("lblForeClose"), Label)
    '        Dim lblSuit As Label = TryCast(e.Row.FindControl("lblSuit"), Label)


    '        Dim hdnNoticeSent As HiddenField = TryCast(e.Row.FindControl("hdnNoticeSent"), HiddenField)
    '        Dim hdnLienDate As HiddenField = TryCast(e.Row.FindControl("hdnLienDate"), HiddenField)
    '        Dim hdnBondDate As HiddenField = TryCast(e.Row.FindControl("hdnBondDate"), HiddenField)
    '        Dim hdnSNDate As HiddenField = TryCast(e.Row.FindControl("hdnSNDate"), HiddenField)
    '        Dim hdnForeclosureDate As HiddenField = TryCast(e.Row.FindControl("hdnForeclosureDate"), HiddenField)
    '        Dim hdnBondSuitDate As HiddenField = TryCast(e.Row.FindControl("hdnBondSuitDate"), HiddenField)
    '        Dim hdnPublicJob As HiddenField = TryCast(e.Row.FindControl("hdnPublicJob"), HiddenField)

    '        If lblNDLDate.Text IsNot "" Then
    '            Dim NDLDate As Date = CType(lblNDLDate.Text, Date)
    '            If DateDiff(DateInterval.Day, Now, NDLDate) < 10 And NDLDate > Date.Now And hdnNoticeSent.Value Is "" Then
    '                lblNDLDate.ForeColor = Drawing.Color.Red
    '            End If
    '        End If
    '        If lblLienDate.Text IsNot "" Then
    '            Dim LDLDate As Date = CType(lblLienDate.Text, Date)
    '            If DateDiff(DateInterval.Day, Now, LDLDate) < 30 And LDLDate > Date.Now And hdnLienDate.Value Is "" Then
    '                lblLienDate.ForeColor = Drawing.Color.Red
    '            End If
    '        End If
    '        If lblBondDate.Text IsNot "" Then
    '            Dim BDLDate As Date = CType(lblBondDate.Text, Date)
    '            If DateDiff(DateInterval.Day, Now, BDLDate) < 30 And BDLDate > Date.Now And hdnBondDate.Value Is "" Then
    '                lblBondDate.ForeColor = Drawing.Color.Red
    '            End If
    '        End If
    '        If lblSNDate.Text IsNot "" Then
    '            Dim SNDate As Date = CType(lblSNDate.Text, Date)
    '            If DateDiff(DateInterval.Day, Now, SNDate) < 30 And SNDate > Date.Now And hdnSNDate.Value Is "" Then
    '                lblSNDate.ForeColor = Drawing.Color.Red
    '            End If
    '        End If

    '        If lblForeClose.Text IsNot "" Then
    '            Dim ForeClose As Date = CType(lblForeClose.Text, Date)
    '            If DateDiff(DateInterval.Day, Now, ForeClose) < 30 And ForeClose > Date.Now And hdnForeclosureDate.Value Is "" Then
    '                lblForeClose.ForeColor = Drawing.Color.Red
    '            End If
    '        End If

    '        If lblSuit.Text IsNot "" Then
    '            Dim Suit As Date = CType(lblSuit.Text, Date)
    '            If DateDiff(DateInterval.Day, Now, Suit) < 30 And Suit > Date.Now And hdnBondSuitDate.Value Is "" And hdnPublicJob.Value = True Then
    '                lblSuit.ForeColor = Drawing.Color.Red
    '            End If
    '        End If


    '    End If
    'End Sub

    Public Function GetPropertyName(ByVal prPublic As String, ByVal prPrivate As String, ByVal prResidential As String, ByVal prFederal As String)
        Dim type As String = ""
        If prPublic Then
            type = "Public"
        ElseIf prPrivate = True And prResidential = False Then
            type = "Private"
        ElseIf prPrivate = True And prResidential = True Then
            type = "Residential"
        ElseIf prFederal Then
            type = "Federal"

        End If


        Return type
    End Function

    Protected Sub drpSortType_SelectedIndexChanged(sender As Object, e As EventArgs)
        SortJobList()
    End Sub

    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        SortJobList()
    End Sub
    Protected Sub btnExportReport_Click(sender As Object, e As EventArgs)

        siteUpdateProgress.Visible = False

        mysproc = Session("mysprocTX")

        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.JobListExportTXJobReview(mysproc, Me.PDFReport.Checked))
        'Dim sreport As String = JobListExportJV(False)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If Me.SpreadSheet.Checked = False Then
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If

            Else

                Dim reportexcel As String = System.IO.Path.GetFileName(sreport)
                Session("reportexcel") = reportexcel
                Me.DownLoadReport(Session("reportexcel"))


            End If
        End If

        Dim pageURL1 As String = "MainDefault.aspx?lienview.TexasReview"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + pageURL1 + "'", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasJob", "CallTexasReviewSuccessFunc();", True)
    End Sub
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="WIDTH: 100%;" class="margin-bottom-0">
            <asp:MultiView ID="MultiView1" runat="server">

           
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Texas Review Job List</h1>
                    <div class="body">
                        <div class="form-horizontal">
                              <div class="form-group">

                                <div class="col-md-12">
                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="text-align: left;">
                                <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" CausesValidation="False" OnClientClick="return modalshow();"></asp:LinkButton>
                                    <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                       <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="CRFS #" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Job #" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Job Name" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Job Address" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Job City" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Job State" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="CustId" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Cust Name" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Branch#" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="FilterKey" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="NoticeSent" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Notice Deadline Date" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Lien Deadline Date" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="Bond Deadline Date" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="SN Deadline Date" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="NOI Deadline Date" Value="16"></asp:ListItem>
                                        <%--'##101--%>
                                        <asp:ListItem Text="Job Balance" Value="13"></asp:ListItem>
                                    </asp:DropDownList>
                                    <%--##101--%>
                                    <asp:DropDownList ID="drpSortType" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortType_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="Ascending" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Descending" Value="1"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            
                            </div>
                        </div>
                        <div style="width: auto; height: auto; overflow: auto;">
                          

                            <asp:GridView ID="gvJobViewList"  runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 12px !important;">
                                   <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <HeaderTemplate>
                                            <table style="width: 100%; table-layout: fixed; border-color: black;" rules="cols">
                                                <tr>
                                                    <td style="width: 35px;"></td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">CRFS #</td>
                                                    <td style="width: 15%;" class="nowordwraptd">Job #</td>
                                                    <td style="width: 20%;" class="nowordwraptd">Job Information</td>
                                                    <td style="width: 20%; word-break: break-all;" class="nowordwraptd">Customer</td>
                                                    <td style="width: 95px;" class="nowordwraptd">Branch#</td>
                                                    <td style="width: 110px;" class="nowordwraptd">Assigned Date</td>
                                                    <td colspan="3" style="width: 393px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">View</td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">Client #</td>
                                                    <td style="width: 15%; word-break: break-all;" class="nowordwraptd">Job Status</td>
                                                    <td style="width: 20%; word-break: break-all;" class="nowordwraptd">Property Type</td>
                                                    <td style="width: 20%;" class="nowordwraptd">Cust#</td>
                                                    <td style="width: 95px;" class="nowordwraptd">Filter Key</td>
                                                    <td style="width: 110px;" class="nowordwraptd">Notice Sent</td>
                                                    <td colspan="3" style="width: 393px" class="nowordwraptd">Deadline Dates</td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle CssClass="RemovePadding" />
                                        <ItemStyle CssClass="RemovePadding" />
                                        <ItemTemplate>
                                            <table id="TableJobViewList" style="width: 100%; border-color: black; font-size: 14px; table-layout: fixed;" rules="cols">
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">
                                                        <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server" /></td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobid" runat="server" Text='<%# Eval("JobId")%>'></asp:Label></td>
                                                    <td style="width: 15%; max-width: 18%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobNum" runat="server" Text='<%# Eval("JobNum")%>'></asp:Label>

                                                    </td>
                                                    <td style="width: 20%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobName" runat="server" Text='<%# Eval("JobName")%>'></asp:Label></td>
                                                    <td style="width: 20%;" class="nowordwraptd">

                                                        <asp:Label ID="lblCustName" runat="server" Text='<%# Eval("ClientCustomer")%>'></asp:Label></td>
                                                    <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblBranchNum" runat="server" Text='<%# Eval("BranchNum")%>'></asp:Label></td>
                                                    <td style="width: 110px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblDateAssigned" runat="server" Text='<%#Utils.FormatDate(Eval("DateAssigned"))%>'></asp:Label></td>
                                                    <td style="width: 135px;" class="nowordwraptd"><b style="font-weight: bold;">Notice - </b>
                                                        <asp:Label ID="lblNoticeDate" runat="server" Text='<%#Utils.FormatDate(Eval("NoticeDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnNoticeSent" runat="Server" Value='<%#Eval("NoticeSent")%>' />
                                                    </td>

                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">Bond - </b>
                                                        <asp:Label ID="lblBondDate" runat="server" Text='<%#Utils.FormatDate(Eval("BondDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnBondDate" runat="Server" Value='<%#Eval("BondDate")%>' />
                                                    </td>
                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">SN - </b>
                                                        <asp:Label ID="lblSNDate" runat="server" Text='<%#Utils.FormatDate(Eval("SNDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnSNDate" runat="Server" Value='<%#Eval("SNDate")%>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 35px"></td>
                                                    <td style="text-align: center; width: 71px; word-break: break-all;" class="nowordwraptd">
                                                        <asp:Label ID="lblClientCode" runat="server" Text='<%# Eval("ClientCode")%>'></asp:Label></td>
                                                    <td class="nowordwraptd">
                                                        <asp:Label ID="lblStatusCode" runat="server" Text='<%# String.Format("{0} - {1}", Eval("StatusCode"), Eval("JobStatusDescr")) %>'></asp:Label>
                                                    </td>
                                                    <td style="width: 18%; max-width: 16%; word-break: break-all;" class="nowordwraptd">
                                                        <asp:Label ID="lblCityStateZip" runat="server" Text='<%# Eval("CityState") %>'></asp:Label>

                                                        <td style="word-break: break-all;" class="nowordwraptd">

                                                            <asp:Label ID="lblCustId" runat="server" Text='<%# Eval("CustId")%>'></asp:Label>
                                                        </td>
                                                        <td style="word-break: break-all; text-align: center;" class="nowordwraptd">
                                                            <asp:Label ID="lblFilterKey" runat="server" Text='<%# Eval("FilterKey")%>'></asp:Label>
                                                        </td>
                                                        <td style="word-break: break-all; text-align: center;" class="nowordwraptd">
                                                            <asp:Label ID="lblNoticeSent" runat="server" Text='<%# Utils.FormatDate(Eval("NoticeSent"))%>'></asp:Label>
                                                        </td>
                                                        <td class="nowordwraptd"><b style="font-weight: bold;">Lien - </b>
                                                            <asp:Label ID="lblLienDate" runat="server" Text='<%#Utils.FormatDate(Eval("LienDeadlineDate"))%>'></asp:Label>
                                                            <asp:HiddenField ID="hdnLienDate" runat="Server" Value='<%#Eval("LienDate")%>' />
                                                        </td>
                                                        <td class="nowordwraptd"><b style="font-weight: bold;">Suit - </b>
                                                            <asp:Label ID="lblSuit" runat="server" Text='<%#Utils.FormatDate(Eval("BondSuitDeadlineDate")) %>'></asp:Label></td>
                                                        <asp:HiddenField ID="hdnBondSuitDate" runat="server" Value='<%#Utils.FormatDate(Eval("BondSuitDate")) %>' />
                                                        <asp:HiddenField ID="hdnPublicJob" runat="server" Value='<%# Eval("PublicJob")%>' />

                                                        <td class="nowordwraptd"><b style="font-weight: bold;">NOI - </b>
                                                            <asp:Label ID="lblNOIDeadline" runat="server" Text='<%# Utils.FormatDate(Eval("NOIDeadlineDate"))%>'></asp:Label></td>

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 35px"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="width: 18%; max-width: 16%; word-break: break-all;" class="nowordwraptd">
                                                        <asp:Label ID="lblPropertyType" runat="server" Text='<%# GetPropertyName(DataBinder.GetPropertyValue(Container.DataItem, "PublicJob"), DataBinder.GetPropertyValue(Container.DataItem, "PrivateJob"), DataBinder.GetPropertyValue(Container.DataItem, "ResidentialBox"), DataBinder.GetPropertyValue(Container.DataItem, "FederalJob")) %>'></asp:Label>

                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="nowordwraptd"><b style="font-weight: bold;">Foreclose - </b>
                                                        <asp:Label ID="lblForeClose" runat="server" Text='<%#Utils.FormatDate(Eval("ForeclosureDeadlineDate"))%>'></asp:Label>

                                                        <asp:HiddenField ID="hdnForeclosureDate" runat="Server" Value='<%#Eval("ForeclosureDate")%>' />
                                                        <asp:HiddenField ID="hdndateassigned" runat="server" Value='<%#Utils.FormatDate(Eval("DateAssigned")) %>' />
                                                    </td>
                                                    <td class="nowordwraptd"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="modal fade modal-primary example-modal-lg" id="modelDivExport" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
                        tabindex="-1" style="display: none;" data-backdrop="static">
                        <div class="modal-dialog modal-center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>

                                    </button>

                                    <h3 class="modal-title">Export Job List !!</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">

                                        <h4 class="modal-title">What Type Of Export Would You Like ? </h4>
                                        <div class="form-group" style="padding-top: 20px; padding-left: 150px">

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PDFReport" runat="server" Checked="True" GroupName="ReportType" Text="PDF" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="SpreadSheet" runat="server" GroupName="ReportType" Text="Spreadsheet" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer" style="text-align: center">
                                    <asp:Button ID="btnExportReport" runat="server" CssClass="btn btn-primary" OnClick="btnExportReport_Click" Style="display: none" />
                                    <button type="button" id="btnSubmitReport" aria-label="Submit" class="btn btn-primary" onclick="modalhide();" />
                                    <span>Submit</span>
                                   

                                    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Cancel">
                                        <span>Cancel</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif" AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center" HorizontalOffset="150" VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
