
Partial Class App_Controls__Custom_LienView_JobEditVreq
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobVerifiedSentRequest
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim ItemId As String
    Dim state As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Me.ItemId = qs.GetParameter("ItemId")
        Me.state = qs.GetParameter("state")
        myjob = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobById(ItemId)
        Me.litJobInfo.Text = "Verified Request for CRF# (" & myjob.Id & ") "
        Me.litJobInfo.Text += " Job# (" & myjob.JobNum.Trim & ") "
        Me.litJobInfo.Text += " JobName (" & myjob.JobName.Trim & ") "
        If Me.state = "TN" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "hideDivNotTN();", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "hideDivTN();", True)
        End If


        If Me.Page.IsPostBack = False Then
            If qs.HasParameter("SubId") Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
                If myreq.Id > 0 Then
                    With myreq
                        Me.AmountOwed.Value = .AmountOwed
                        Me.StmtofAccts.Text = .StmtOfAccts
                        Me.TNLastDateWorkPerformed.Value = .MonthDebtIncurred
                    End With
                Else
                    Dim MYSQL As String = "Select * from JobVerifiedSentRequest where isprocessed = 0 and JobId = '" & myjob.Id & "' "
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetItem(MYSQL, myreq)
                    With myreq
                        Me.AmountOwed.Value = .AmountOwed
                        Me.StmtofAccts.Text = .StmtOfAccts
                        Me.AmountOwed.Value = myjob.EstBalance
                        Me.TNLastDateWorkPerformed.Value = .MonthDebtIncurred
                    End With
                End If
            End If

        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSAVE.Click
        Me.Page.Validate()
        If Page.IsValid = False Then
            Exit Sub
        End If

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If qs.HasParameter("SubId") Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
        End If
        Dim MYSQL As String = "Select * from JobVerifiedSentRequest where isprocessed = 0 and JobId = '" & ItemId & "' "
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetItem(MYSQL, myreq)

        With myreq
            .JobId = ItemId
            .DateCreated = Now()
            .AmountOwed = Me.AmountOwed.Value
            .StmtOfAccts = Me.StmtofAccts.Text
            .MonthDebtIncurred = Me.TNLastDateWorkPerformed.Value
            .SubmittedBy = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .SubmittedByUserId = Me.CurrentUser.Id
        End With

        Me.CustomValidator1.IsValid = True
        Me.CustomValidator1.ErrorMessage = ""

        If myreq.Id > 0 Then
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
        Else
            myreq.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myreq)
            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = ItemId
                .DateCreated = Now()
                .Note = "Notice request made. Request is now being processed."
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)

            End With
        End If

        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&view=" & "VREQ"
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub

    Private Function setdate(ByVal adate As String) As String
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return ""
        End If
        If s = Date.MinValue Then
            Return ""
        End If
        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Private Function getdate(ByVal adate As String) As Date
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return Date.MinValue
        End If

        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&view=" & "VREQ"
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
End Class
