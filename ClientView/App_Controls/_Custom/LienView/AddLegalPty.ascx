<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddLegalPty.ascx.vb" Inherits="App_Controls_crfLienView_AddLegalPty" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script type="text/javascript">
    $('#' + '<%=btnAddParty.ClientID%>').click(function () {
        var type;
        if (document.getElementById("chkPartyGC").checked) { type = 'GC' }
        else if (document.getElementById("chkPartyOwner").checked) { type = 'Owner' }
        else if (document.getElementById("chkPartySuretyLender").checked) { type = 'Surety/Lender' }
        else if (document.getElementById("chkPartyDesignee").checked) { type = 'Designee' }
        if (rowIndex == -1) {
            tbl.row.add([
                "<a href='#' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row' " +
            "data-toggle='tooltip' data-original-title='Edit'><i class='icon wb-edit' aria-hidden='true'></i></a>",
            type,
            $("#partyName").val(),
            $("#partyAddLine1").val(),
            $("#partyAddLine2").val(),
            $("#partyCity").val(),
            $("#partyState").val(),
            $("#partyZip").val(),
            $("#partyPhone").val(),
            $("#partyFax").val(),
            $("#partyBondNum").val(),
            "<a href='#' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row'" +
                "data-toggle='tooltip' data-original-title='Remove'><i class='icon wb-trash' aria-hidden='true'></i></a>"
            ]).draw(false);
            $("#ModalLegalParty").modal('hide');
        }
        else {
            rowData = tbl.row(rowIndex).data();
            rowData[1] = type;
            rowData[2] = $("#partyName").val(),
            rowData[3] = $("#partyAddLine1").val(),
            rowData[4] = $("#partyAddLine2").val(),
            rowData[5] = $("#partyCity").val(),
            rowData[6] = $("#partyState").val(),
            rowData[7] = $("#partyZip").val(),
            rowData[8] = $("#partyPhone").val(),
            rowData[9] = $("#partyFax").val(),
            rowData[10] = $("#partyBondNum").val(),
            tbl.row(rowIndex).data(rowData);
            $("#ModalLegalParty").modal('hide');
        }
        rowIndex = -1;
    });
    function OpenLegalPartyModal() {
        //document.getElementById("chkPartyGC").checked = true;
        //document.getElementById("chkPartyOwner").checked = false;
        //document.getElementById("chkPartySuretyLender").checked = false;
        $("#<%=partyName.ClientID%>").val('');
        $("#<%=partyAddLine1.ClientID%>").val('');
        $("#<%=partyAddLine2.ClientID%>").val('');
        $("#<%=partyCity.ClientID%>").val('');
        $("#<%=partyState.ClientID%>").val('');
        $("#<%=partyZip.ClientID%>").val('');
        $("#<%=partyPhone.ClientID%>").val('');
        $("#<%=partyFax.ClientID%>").val('');
        $("#<%=partyBondNum.ClientID%>").val('');
        $("#ModalLegalParty").modal('show');
    }
</script>
<div style="margin: 2px 0px 5px 0px;">
    <asp:Button ID="btnAddNew" CssClass="btn btn-primary" runat="server" Text="Add Legal Party" Style="display: none;" />
</div>
<div class="modal fade  modal-primary" id="ModalLegalParty" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-center">
        <div class="modal-content" style="min-width:700px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <asp:HiddenField runat="server" ID="hddnRowIndex" />
                <h4 class="modal-title">ADDITIONAL LEGAL PARTY</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label">Type</label>
                        <div class="col-md-10 col-sm-10 col-xs-10">
                            <div class="radio-custom radio-default" style="padding-top: 3px;">
                                <style>
                                    #ctl06_ctl00_AddLegalPty1_RadioButtonList2 label {
                                        width: 55px;
                                    }
                                </style>
                      <%--            <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" Width="256px" AutoPostBack="False">--%>
                                <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatDirection="Horizontal" AutoPostBack="False">
                                    <asp:ListItem Value="GC"> GC/Contractor</asp:ListItem>
                                    <asp:ListItem Value="OW" style="padding-left: 30px;"> Owner/Tenant</asp:ListItem>
                                    <asp:ListItem Value="LE" style="padding-left: 30px;"> Surety/Lender</asp:ListItem>
                                    <asp:ListItem Value="DE" style="padding-left: 30px;">Designee</asp:ListItem>
                                </asp:RadioButtonList>
                                <%--<input type="radio" id="chkPartyGC" name="inputRadios" checked="checked">
                          <label for="inputRadiosUnchecked" style="width:55px;">GC</label>
                          <input type="radio" id="chkPartyOwner" name="inputRadios">
                          <label for="inputRadiosUnchecked" style="width:75px;">Owner</label>
                          <input type="radio" id="chkPartySuretyLender" name="inputRadios">
                          <label for="inputRadiosUnchecked">Surety/Lender</label>--%>
                            </div>
                        </div>
                    </div>
                    <%-- ##1--%>
                    <%-- <div class="form-group" >
                    <label class="col-md-2  col-sm-2 col-xs-2 control-label">Name</label>
                    <div class="col-md-5  col-sm-5 col-xs-5" >--%>
                    <%--<input type="text" id="partyName" class="form-control" />--%>
                    <%--<asp:TextBox runat="server" ID="partyName" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-5  col-sm-5 col-xs-5" style="display:flex;padding-left:0px;padding-right:0px;">
                        <div class="checkbox-custom checkbox-default" style="min-height: 20px;font-size:12px;">
                            <asp:CheckBox ID="chkMLAgent" runat="server" Text="Show as ML Agent (If Owner checked)" />
                        </div>
                    </div>
                </div>--%>
                   <%-- ##1--%>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-8 col-sm-8 col-xs-8">
                            <div class="checkbox-custom checkbox-default" style="min-height: 20px; font-size: 12px;">
                                <asp:CheckBox ID="chkMLAgent" runat="server" Text="Show as ML Agent (If Owner checked)" />
                            </div>
                        </div>
                    </div>
                   <%-- ##1--%>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label">Name</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <%--<input type="text" id="partyName" class="form-control" />--%>
                            <asp:TextBox runat="server" ID="partyName" CssClass="form-control" TabIndex="0"></asp:TextBox>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label">Address</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <%--<input type="text" id="partyAddLine1" class="form-control" />--%>
                            <asp:TextBox runat="server" ID="partyAddLine1" CssClass="form-control" TabIndex="1"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label"></label>
                        <div class="col-md-8 col-sm-8 col-xs-8 col-sm-8 col-xs-8">
                            <%--<input type="text" id="partyAddLine2" class="form-control" />--%>
                            <asp:TextBox runat="server" ID="partyAddLine2" CssClass="form-control" TabIndex="2"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label">City/State/Zip</label>

                        <div class="col-md-10 col-sm-10 col-xs-10" style="display: flex;">
                            <asp:TextBox runat="server" ID="partyCity" CssClass="form-control" Width="160px" TabIndex="3"></asp:TextBox>&nbsp;
                        <asp:TextBox runat="server" ID="partyState" CssClass="form-control" Width="65px" TabIndex="4"></asp:TextBox>&nbsp;
                            <asp:TextBox runat="server" ID="partyZip" CssClass="form-control" Width="130px" TabIndex="5"></asp:TextBox>
                        </div>
                        <%-- <div class="col-md-4 col-sm-4 col-xs-4" style="display:flex;pa">
                   
                    
                    </div>--%>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label">Phone#</label>
                        <div class="col-md-5  col-sm-5 col-xs-5">
                            <%--<input type="text" id="partyPhone" class="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]" />--%>
                            <asp:TextBox runat="server" ID="partyPhone" Width="135px" TabIndex="6" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label">Fax#</label>
                        <div class="col-md-5  col-sm-5 col-xs-5">
                            <%--<input type="text" id="partyFax" class="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]" />--%>
                            <asp:TextBox runat="server" ID="partyFax" Width="135px" TabIndex="7" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2  col-sm-2 col-xs-2 control-label">BondNum</label>
                        <div class="col-md-5  col-sm-5 col-xs-5  col-sm-5 col-xs-5">
                            <%--<input type="text" id="partyBondNum" class="form-control" />--%>
                            <asp:TextBox runat="server" ID="partyBondNum" Width="135px" TabIndex="8" CssClass="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--<button type="button" class="btn btn-primary margin-0" id="btnAddParty" >Add</button>--%>
                <asp:Button runat="server" ID="btnAddParty" CssClass="btn btn-primary margin-0" Text="Save" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:Panel ID="panDialog" runat="server" Style="display: none"
    Width="575px" Font-Bold="True">
    <div id="modcontainer" style="margin: 0px  0px 0px 0px; width: 575px;">
        <h1 class="panelheader">Additional Legal Party</h1>
        <div class="body">
            <asp:LinkButton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " />
            <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="row-label" style="width: 75px;">Type</td>
                    <td class="row-data" width="475">
                          <%-- <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" Width="256px" AutoPostBack="False">--%>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal"  AutoPostBack="False">
                            <asp:ListItem Value="GC"> GC</asp:ListItem>
                            <asp:ListItem Value="OW"> Owner</asp:ListItem>
                            <asp:ListItem Value="LE"> Surety/Lender</asp:ListItem>
                             <asp:ListItem Value="DE"> Designee </asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px; height: 24px;">Name:</td>
                    <td class="row-data" width="475" style="height: 24px">
                        <cc1:DataEntryBox ID="LPName" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="11" tag="Name" Width="224px"></cc1:DataEntryBox>&nbsp;
                    <asp:CheckBox ID="MLAGENT" runat="server" Text="  Show as ML Agent (If Owner checked)" />
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">Address:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPAdd1" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="13" tag="Address" Width="224px"></cc1:DataEntryBox>

                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;"></td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPAdd2" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="14" tag="Address2" Width="224px"></cc1:DataEntryBox>

                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px; height: 24px;">City/State/Zip:</td>
                    <td class="row-data" width="475" style="height: 24px">
                        <cc1:DataEntryBox ID="LPCity" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="15" Width="104px" tag="City"></cc1:DataEntryBox>
                        <cc1:DataEntryBox ID="LPState" runat="server" CssClass="textbox" IsRequired="False" TabIndex="16" tag="State" Width="32px"></cc1:DataEntryBox>
                        <cc1:DataEntryBox ID="LPZip" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="17" Width="78px" tag="Zip"></cc1:DataEntryBox>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">Phone#:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPPhone" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="18" Tag="Legal Party Phone" Width="80px"></cc1:DataEntryBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server" AcceptNegative="None"
                            ClearMaskOnLostFocus="False" DisplayMoney="None" InputDirection="LeftToRight"
                            Mask="999-999-9999" MaskType="NUMBER" MessageValidatorTip="true" OnFocusCssClass="focus"
                            OnInvalidCssClass="error" TargetControlID="LPPhone"></ajaxToolkit:MaskedEditExtender>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">Fax#:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPFax" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="19" Tag="Legal Party Fax" Width="80px"></cc1:DataEntryBox><ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" AcceptNegative="None"
                                ClearMaskOnLostFocus="False" DisplayMoney="None" InputDirection="LeftToRight"
                                Mask="999-999-9999" MaskType="NUMBER" MessageValidatorTip="true" OnFocusCssClass="focus"
                                OnInvalidCssClass="error" TargetControlID="LPFax"></ajaxToolkit:MaskedEditExtender>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">BondNum:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="RefNum" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="20" Tag="Address" Width="250px"></cc1:DataEntryBox></td>
                </tr>
            </table>
        </div>
        <div class="footer">
            <asp:LinkButton ID="btnSave" CssClass="button" runat="server" Text="Save" />&nbsp;&nbsp;
            <br />
            <asp:HiddenField ID="BatchJobId" runat="server" />
            <asp:HiddenField ID="ItemId" runat="server" />
        </div>
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="" OkControlID="" DropShadow="true"
    PopupControlID="panDialog" TargetControlID="btnAddNew">
</ajaxToolkit:ModalPopupExtender>
