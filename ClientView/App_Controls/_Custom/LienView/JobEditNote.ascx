<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<script language="javascript" type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Tools', 'Texas Pending list');
        MainMenuToggle('liTools');
        SubMenuToggle('liTexaspendingList');
        //$('#liTools').addClass('site-menu-item has-sub active open');
        //$('#liTexaspendingList').addClass('site-menu-item active');
        return false;

    }

    </script>
<script runat="server">
    Dim ItemId As String
    Dim ChildId As String = -1
    Dim myforms As HDS.DAL.COMMON.TableView
    Dim myItem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
    Dim test As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActionHistory
    Dim test1 As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActions
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Me.ItemId = qs.GetParameter("ItemId")
        Me.ChildId = qs.GetParameter("SubId")
        If qs.HasParameter("SubId") = False Then
            Me.ChildId = -1
        End If
        'myItem = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNoteById(ChildId)
        If Page.IsPostBack = False Then
            If ChildId > 1 Then
                
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(ChildId, myItem)
                Me.Note.Text = myItem.Note
                If Me.Note.Text.Trim = "" Then
                    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(ChildId, test)
                    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(test.ActionId, test1)
                    Me.Note.Text = test1.HistoryNote
                End If
                
               

            End If
        End If
        
    End Sub
   

    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click

    '    Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
    '    With mynote
    '        ' .JobId = me.ItemId
    '        .DateCreated = Now()
    '        .Note = Me.Note.Text
    '        '  .UserId = Strings.Left(Me.CurrentUser.username, 10)
    '        .ClientView = True
    '        .EnteredByUserId = Me.CurrentUser.id
    '        .ActionTypeId = 0
    '    End With
    '    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
    '    Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
       
    '    qs.SetParameter("action", "vnote")
    '    Response.Redirect(qs.All, True)
 
    'End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
       
        Session("exitviewer") = "1"
        Session("currenttab") = "NOTES"
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & ItemId & "&view=" & "NOTES"
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)
     
    End Sub
</script>


                                <h2 style="text-align:left;"> Note Detail</h2>
                                <div class="body" style="background-color: #edf5ff; height: 444px;">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: left;">
                                                <asp:TextBox ID="Note" runat="server" Height="402px" Width="100%" TextMode="MultiLine"
                                                    CssClass="textbox" ReadOnly="True"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: left;">
                                                <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                                <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                                <div class="footer">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-2" style="text-align: left;">
                                                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Back"
                                                    OnClick="btnCancel_Click"></asp:Button>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                                <div></div>
                          


