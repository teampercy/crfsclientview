﻿
Partial Class App_Controls__Custom_LienView_FUDefault
    Inherits HDS.WEBSITE.UI.DefaultPage

    Protected Sub btnFileUpLoadOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFu.Click
        Try

            Dim str As String
            Dim jobid As String
            Dim batchjobid As String
            Dim usercode As String
            Dim fileName As String
            Dim userid As Integer
            Dim Todaydate As Date

            'markparimal-uploadfile:check whether document type is selected or not
            If DocType.SelectedIndex = 0 Then
                lblFuMsg.Text = " Select Document Type"
                Return
            Else
                lblFuMsg.Text = ""
            End If

            'markparimal-uploadfile:Convertsfile into byte stream.
            Dim flv As FileUpload = CType(Page.FindControl("FileUpd"), FileUpload)
            str = FileUpd.PostedFile.FileName

            Dim fileData As Byte() = New Byte(CInt(FileUpd.PostedFile.InputStream.Length - 1)) {}
            FileUpd.PostedFile.InputStream.Read(fileData, 0, fileData.Length)

            'markparimal-uploadfile:sets username 
            usercode = CurrentUser.UserName.ToString

            'markparimal-uploadfile:sets current user
            userid = CurrentUser.Id

            'markparimal-uploadfile:sets Filename
            fileName = FileUpd.FileName

            'markparimal-uploadfile:sets todaydate
            Todaydate = txtDocDate.Text

            'markparimal-uploadfile:checks call is from view or new lien view job
            If Session("bitjobid") = 0 Then
                jobid = 0
                batchjobid = Session("BatchJobId")
            Else
                jobid = Session("jobid")
                batchjobid = 0
            End If


            'CRFS.ClientView.CrfsDll.InsertJobAttachments(jobid, userid, usercode, Calendar2.Value, fileName, ddldoctype.SelectedValue, Txtmiscinfo.Text, fileData, batchjobid)
            CRFS.ClientView.CrfsBll.InsertJobAttachments(jobid, userid, usercode, Todaydate, fileName, DocType.SelectedValue, micsinfo.Text, fileData, batchjobid)
            If Session("bitjobid") = 0 Then

                lblFuMsg.Text = "File Uploaded For Temp Job#" + " " + batchjobid
            Else
                lblFuMsg.Text = "File Uploaded For Job#" + " " + jobid
            End If

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.Page.IsPostBack = False Then
            txtDocDate.Text = DateTime.Today.ToString("MM/dd/yyyy")
        End If

    End Sub

End Class
