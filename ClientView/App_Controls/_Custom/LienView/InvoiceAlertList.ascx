<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script runat="server">
    Protected WithEvents btnEditItem As LinkButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
      
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.UserId.Value = Me.CurrentUser.Id
            Me.fromAsgDate.Value = DateAdd(DateInterval.Day, -180, Today)
            Me.thruAsgDate.Value = DateAdd(DateInterval.Day, 1, Today)
            Dim mydt As System.Data.DataTable
            mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverDaysRemaining(Me.CurrentUser.Id).Tables(0)
            Me.gvwList.DataSourceID = Me.objWithEndDates.ID
            Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
            
            Me.gvwList.DataBind()
            
        End If
    End Sub
   
    Protected Sub gvwItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvwList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvwList.PageSize = Integer.Parse(dropDown.SelectedValue)

    End Sub
    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub gvwList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = DR("JobId")
      
            e.Row.Cells(2).Text = Strings.Left(e.Row.Cells(2).Text, 7)
            e.Row.Cells(3).Text = Strings.Left(e.Row.Cells(3).Text, 20)

            e.Row.Cells(4).Text = Strings.FormatCurrency(e.Row.Cells(4).Text)
            e.Row.Cells(5).Text = Strings.FormatCurrency(e.Row.Cells(5).Text)
            e.Row.Cells(6).Text = Math.Round(Double.Parse(e.Row.Cells(6).Text)) & "%"

        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.TableSection = TableRowSection.TableHeader
            
        End If
    End Sub
    Private Function FormatDate(ByVal adate As String) As String
        If IsDate(adate) = False Then
            Return ""
        End If
        Return Strings.FormatDateTime(adate)
    End Function
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myitemid & "&list=inv"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mydt As System.Data.DataTable
        mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetInvoiceList(Me.CurrentUser.Id).Tables(0)
        Me.gvwList.DataSourceID = Me.objWithEndDates.ID
        Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
          
        Me.gvwList.DataBind()
        
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)

    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sreport As String = ""
        sreport = CRF.CLIENTVIEW.BLL.LienView.Provider.GetInvoiceListReport(Me.CurrentUser.Id)
        If IsNothing(sreport) = False Then
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View3)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View3)
        End If
    End Sub

    Protected Sub Linkbutton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub

Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
End Sub
</script>

<script>
    function MaintainMenuOpen() {
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubInvoiceAlertList');
        SetHeaderBreadCrumb('LienView', 'Jobs', 'Amended Alert List');


    }


    $(function () {

        CallSuccessFunc();
    });


    function CallSuccessFunc() {
       // alert('call');
        var Columns = [
             { "bSortable": false, "searchable": false },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true }           
        ]
        var sorting= [[0, "asc"]];
        MakeDataTable('#<%=gvwList.ClientID%>', Columns, null, null);

    }

</script>

<asp:ObjectDataSource ID="objWithEndDates" runat="server"
		OldValuesParameterFormatString="original_{0}" SelectMethod="GetInvoiceList"
		TypeName="CRF.CLIENTVIEW.BLL.LienView.Provider">
        <SelectParameters>
            <asp:ControlParameter ControlID="UserId" Name="userid" PropertyName="Value" Type="String" />
        </SelectParameters>
</asp:ObjectDataSource>




<div id="modcontainer" class="margin-bottom-0" style="width: 100%">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1 class="panelheader">Amended Alert List</h1>
            <div class="body">

                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-2">

                            <asp:LinkButton ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Print Report" OnClick="btnReport_Click" />
                        </div>
                        <div class="col-md-2">
                            <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh List" CssClass="btn btn-primary" OnClick="btnRefresh_Click" />
                        </div>
                        <div class="col-md-8">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-12">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                <ContentTemplate>
                                    <asp:GridView ID="gvwList" runat="server" DataSourceID="ItemsDataSource"
                                         CssClass="table dataTable table-striped"  AutoGenerateColumns="False"
                                        BorderWidth="1px" Width="100%">
                                        <%--<RowStyle CssClass="rowstyle" />
                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                        <HeaderStyle CssClass="headerstyle" />
                                        <PagerStyle CssClass="pagerstyle" />
                                        <PagerTemplate>
                                            <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Value="5" />
                                                <asp:ListItem Value="10" />
                                                <asp:ListItem Value="15" />
                                                <asp:ListItem Value="20" />
                                            </asp:DropDownList>
                                            &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                            of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                            &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                            <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                            <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                            <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                                        </PagerTemplate>--%>
                                        <Columns>
                                            <asp:TemplateField HeaderText="View">
                                                <ItemTemplate>
                                                    <%--<asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server"  />--%>
                                                    <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="20px" Wrap="False" />

                                            </asp:TemplateField>
                                            <asp:BoundField DataField="JobId" HeaderText="File#" SortExpression="JobId">
                                                <ItemStyle HorizontalAlign="Center" Width="50px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JobName" HeaderText="Job Name" SortExpression="JobName">
                                                <ItemStyle Width="130px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CustName" HeaderText="Customer Name" SortExpression="CustName" />
                                            <asp:BoundField DataField="EstBalance" HeaderText="Est Balance" SortExpression="EstBalance">
                                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AmountOwed" HeaderText="Invoice Total" SortExpression="AmountOwed">
                                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PercentOwed" HeaderText="Percent" SortExpression="PercentOwed">
                                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                                            </asp:BoundField>

                                        </Columns>
                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>


                </div>





            </div>
            <%--  <div class="footer">
                <br />
            </div>--%>
        </asp:View>
        <asp:View ID="View3" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-2">
                            <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Amended Alerts" OnClick="Linkbutton2_Click"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                    </div>
                </div>
            </div>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="UserId" runat="server" />
<asp:HiddenField ID="fromAsgDate" runat="server" />
<asp:HiddenField ID="thruAsgDate" runat="server" />
<asp:HiddenField ID="CurrentRowIndex" runat="server" />
<asp:UpdateProgress ID="siteUpdateProgress" runat="server">
    <ProgressTemplate>
        <div class="TransparentGrayBackground"></div>
        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
            <div class="PageUpdateProgress">
                <asp:Image ID="ajaxLoadNotificationImage"
                    runat="server"
                    ImageUrl="~/images/ajax-loader.gif"
                    AlternateText="[image]" />
                &nbsp;Please Wait...
            </div>
        </asp:Panel>
        <ajaxToolkit:AlwaysVisibleControlExtender
            ID="AlwaysVisibleControlExtender1"
            runat="server"
            TargetControlID="alwaysVisibleAjaxPanel"
            HorizontalSide="Center"
            HorizontalOffset="150"
            VerticalSide="Middle"
            VerticalOffset="0">
        </ajaxToolkit:AlwaysVisibleControlExtender>
    </ProgressTemplate>
</asp:UpdateProgress>
