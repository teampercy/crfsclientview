﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="FUDefault.aspx.vb" Inherits="App_Controls__Custom_LienView_FUDefault" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta name="generator" content="hds vbjuice" />
    <meta id="metaKeywords" name="Keywords" content="" />
    <meta id="metaDescr" name="Description" content="" />
    <meta id="metaCopyRight" name="copyright" content="" />
    <title></title>
    <!-- Stylesheets -->

    <link rel="stylesheet" href="../../../global/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="../../../assets/css/site.min.css">
    <style>
        .modal-body table tr td {
            padding: 3px;
        }
    </style>
</head>
<body style="padding-top: 0px !important">
    <form id="myform" runat="server" autocomplete="on">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-12">
                    <asp:Label runat="server" CssClass="col-md-5" ID="lblFuMsg" Style="color: red;"></asp:Label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    <label class="col-md-3 control-label">Document Date </label>
                </div>
                <div class="col-md-8">
                    <asp:TextBox runat="server" ID="txtDocDate" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    <label class="col-md-3 control-label">Document Type </label>
                </div>
                <div class="col-md-8">
                    <asp:DropDownList ID="DocType" class="form-control" runat="server">
                        <asp:ListItem Text="--SELECT--" Selected="true"></asp:ListItem>
                        <asp:ListItem Text="AZ Owner Ack"></asp:ListItem>
                        <asp:ListItem Text="Bond Copy"></asp:ListItem>
                        <asp:ListItem Text="Building Permit"></asp:ListItem>
                        <asp:ListItem Text="Job Info Sheet"></asp:ListItem>
                        <asp:ListItem Text="Job Info Fax"></asp:ListItem>
                        <asp:ListItem Text="Miscellaneous"></asp:ListItem>
                        <asp:ListItem Text="Notice of Commencement"></asp:ListItem>
                        <asp:ListItem Text="Notice of Completion"></asp:ListItem>
                        <asp:ListItem Text="Surety Letter"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    <label class="col-md-3 control-label">Misc Info:</label>
                </div>
                <div class="col-md-8">
                    <asp:TextBox ID="micsinfo" runat="server" TextMode="MultiLine" Width="150px" Font-Names="Arial"
                        class="form-control" Font-Size="small"></asp:TextBox>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-4">
                    <label class="col-md-3 control-label">Select File to Upload:</label>
                </div>
                <div class="col-md-8">
                    <asp:FileUpload runat="server" ID="FileUpd" ClientIDMode="Static" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="col-md-10" style="color: red; text-align: center;">
                        ** Attached file cannot be larger than 4 mbs. If your file is larger than 
                            this, please break it up into multiple files and attach them separately.
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <asp:Button runat="server" CssClass="btn btn-primary margin-0" Text="Upload" ID="btnFu" />
                </div>
            </div>
        </div>      
    </form>
    <script src="../../../global/vendor/jquery/jquery.js"></script>  
    <script src="../../../global/vendor/bootstrap/bootstrap.js"></script>
</body>
</html>
