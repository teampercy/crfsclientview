
Partial Class App_Controls_LienView_AddSigner
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim MYITEM As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
    Public Sub ClearData()
        Me.SignerName.Text = ""
        Me.SignerTitle.Text = ""
    End Sub
     Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData()
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
        With myitem
            .ClientId = Me.UserInfo.UserInfo.ClientId
            .Signer = Me.SignerName.Text
            .SignerTitle = Me.SignerTitle.Text
        End With
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myitem)
        ClearData()
        RaiseEvent ItemSaved()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsHideModalPopup", "HideModalPopup();", True)
    End Sub

End Class
