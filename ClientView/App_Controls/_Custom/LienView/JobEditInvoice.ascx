<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobEditInvoice.ascx.vb" Inherits="App_Controls__Custom_LienView_JobEditInvoice" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>

<script type="text/javascript">
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Jobs', 'View Jobs');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubJobList');
        //$('#liJobs').addClass('site-menu-item has-sub active open');
        //$('#lisubJobList').addClass('site-menu-item active');
        return false;

    }
  
</script>

                                    <div id="modcontainer" style="MARGIN: 10px; WIDTH: 100%;">
                                        <h1>
                                            <asp:Literal ID="litJobInfo" runat="server"></asp:Literal></h1>
                                        <div class="body">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-md-2">
                                                        <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to Job Info" CausesValidation="False" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Invoice Date:</label>
                                                    <div class="col-md-7" style="text-align: left;"> <span style="display: flex;">
                                                        <SiteControls:Calendar ID="InvoiceDate" runat="server" ErrorMessage="From Date"
                                                            IsReadOnly="false" IsRequired="FALSE" Value="" /></span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Invoice Amount:</label>
                                                    <div class="col-md-3" style="text-align: left;">
                                                        <cc1:DataEntryBox ID="InvoiceAmount" runat="server" DataType="Money" CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Amount Paid:</label>
                                                    <div class="col-md-3" style="text-align: left;">
                                                        <cc1:DataEntryBox ID="PmtAmount" runat="server"  DataType="Money" CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Amount Adjusted:</label>
                                                    <div class="col-md-3" style="text-align: left;">
                                                        <cc1:DataEntryBox ID="AdjAmount" runat="server"  DataType="Money" CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Invoice Ref#:</label>
                                                    <div class="col-md-3" style="text-align: left;">
                                                        <cc1:DataEntryBox ID="InvoiceRefNum" runat="server"  DataType="Any" CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label" style="text-align: left;">
                                                        <h3>Description:</h3>
                                                    </label>

                                                </div>
                                                <div class="form-group">

                                                    <div class="col-md-9" style="text-align: left;">
                                                        <cc1:DataEntryBox ID="Description" runat="server" DataType="Any" Height="96px" Tag="StmtofAccts"
                                                            TextMode="MultiLine"  CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-12 control-label" style="text-align: left;">
                                                        <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                                        <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
                                                    </label>

                                                </div>
                                            </div>


                                        </div>
                                        <div class="footer">
                                            <asp:UpdatePanel ID="updPan1" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="btnSAVE"
                                                        CssClass="btn btn-primary"
                                                        runat="server"
                                                        Text="Submit" CausesValidation="False" />&nbsp;
                                                    <br />
                                                    <br />

                                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                        <ProgressTemplate>
                                                            <div class="TransparentGrayBackground"></div>
                                                            <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                                                <div class="PageUpdateProgress">
                                                                    <asp:Image ID="ajaxLoadNotificationImage"
                                                                        runat="server"
                                                                        ImageUrl="~/images/ajax-loader.gif"
                                                                        AlternateText="[image]" />
                                                                    &nbsp;Please Wait...
                                                                </div>
                                                            </asp:Panel>
                                                            <ajaxToolkit:AlwaysVisibleControlExtender
                                                                ID="AlwaysVisibleControlExtender1"
                                                                runat="server"
                                                                TargetControlID="alwaysVisibleAjaxPanel"
                                                                HorizontalSide="Center"
                                                                HorizontalOffset="150"
                                                                VerticalSide="Middle"
                                                                VerticalOffset="0">
                                                            </ajaxToolkit:AlwaysVisibleControlExtender>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

                         
