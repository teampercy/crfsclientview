Imports CRF.CLIENTVIEW.BLL
Imports System.Net.Mail
Imports System.IO
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports HDS.DAL.Providers
Imports HDS.Reporting.C1Reports.Common

'Imports iTextSharp.text
'Imports iTextSharp.text.pdf

Partial Class App_Controls__Custom_LienView_JobEditWaiver
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim childid As String = "-1"
    Dim jobid As String = ""
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobWaiverLog
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateForms
    Dim mysigner As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
    Dim mycustomer As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomer
    Dim mysigners As HDS.DAL.COMMON.TableView
    Dim myforms As HDS.DAL.COMMON.TableView
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim myreport As HDS.Reporting.C1Reports.Common.StandardReport
    Dim hdreport As Liens.Reports.JobWaiverNotary

    Dim ItemId As String
    Dim mysignerid As Integer = 0
    Dim mylastform As String
    Dim PageName As String
    Dim CustId As String
    Dim JobWaiverLogId As String
    Dim custEmail As String
    Dim subjectwaivertype As String
    Dim JobAddSubjectLine As String
    Dim QueryJobNum As String
    Dim s1 As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        PaymentAmt.Attributes.Add("onblur", "PaymentAmtlostfocus();")
        DisputedAmt.Attributes.Add("onblur", "DisputedAmtlostfocus();")
        OtherChkIssuerName.Attributes.Add("onblur", "fnOtherCheckIssuer();")


        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        mylastform = Me.Session("LastWaiverForm")

        Me.ItemId = qs.GetParameter("ItemId")
        Me.PageName = qs.GetParameter("PageName")
        myjob = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobById(ItemId)
        Me.hdnCustId.Value = myjob.ClientId
        ViewState("JobAddSubjectLine") = "(" + Convert.ToString(myjob.Id) + ") " + myjob.JobAdd1 + " " + myjob.JobCity + " " + myjob.JobState
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.hdnCustId.Value, mycustomer)

        If Me.Page.IsPostBack = False Then
            Dim li As New ListItem
            myforms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverStateForms(myjob.JobState)
            myforms.MoveFirst()
            Me.FormId.Items.Clear()
            Do Until myforms.EOF
                li = New ListItem

                myform = myforms.FillEntity(myform)

                li.Value = myform.Id
                li.Text = myform.FormCode & "-" & myform.Description

                Me.FormId.Items.Add(li)
                If IsNothing(mylastform) = False And li.Value = mylastform Then
                    Me.FormId.SelectedValue = li.Value

                End If

                myforms.MoveNext()
            Loop
            'If Me.FormId.Items.Count > 0 Then
            '    hdnWaiverTypeSelectedId.Value = Me.FormId.Items(0).Value
            'End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & myjob.JobState & "');", True)
            Dim VWSIGNERS As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientSigners
            VWSIGNERS.MoveFirst()
            Me.SignerId.Items.Clear()
            GetLastSignerId()
            Dim signerid = 0
            Do Until VWSIGNERS.EOF
                li = New ListItem
                Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
                csg = VWSIGNERS.FillEntity(csg)
                li.Value = csg.Id
                li.Text = csg.Signer & "," & csg.SignerTitle
                Me.SignerId.Items.Add(li)
                If csg.Id = mysignerid Then
                    signerid = mysignerid
                End If
                VWSIGNERS.MoveNext()
            Loop
            If Me.SignerId.Items.Count > 0 Then
                Me.SignerId.SelectedValue = Me.SignerId.Items(0).Value
                If signerid > 0 Then
                    Me.SignerId.SelectedValue = signerid
                End If
            End If

            If (Request.Cookies("SignerSelectedValue") IsNot Nothing) Then

                If (Request.Cookies("SignerSelectedValue")("Data") IsNot Nothing) Then
                    hdnSignerSelectedValue.Value = Request.Cookies("SignerSelectedValue")("Data")
                End If
            End If





            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & signerid & ");", True)
            Me.litJobInfo.Text = " Waiver for CRF# (" & myjob.Id & ") "
            Me.litJobInfo.Text += " Job# (" & myjob.JobNum.Trim & ") "
            Me.litJobInfo.Text += " JobName (" & myjob.JobName.Trim & ") "
            ViewState("QueryJobNum") = myjob.Id
            LoadData()
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsMailToDropDown", "BindMailTo(" & myjob.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsMailCheckDropDown", "BindMailCheck('" & myjob.Id & "');", True)


        'FormId.DataBind()
        'SignerId.DataBind()

    End Sub



    Public Sub LoadData()

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)



        'Commented by Jaywanti
        'If Me.SignerId.SelectedValue.Length > 0 Then
        '    myreq.SignerId = Me.SignerId.SelectedValue
        'End If

        If hdnSignerSelectedValue.Value <> "" And hdnSignerSelectedValue.Value <> " Then0" Then
            myreq.SignerId = hdnSignerSelectedValue.Value
        End If

        If qs.HasParameter("SubId") Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
        End If

        With myreq
            Me.PaymentAmt.Value = .PaymentAmt.ToString
            Me.DisputedAmt.Value = .DisputedAmt.ToString

            Dim getMailid As String
            If qs.GetParameter("SubId") = "-1" Then
                Dim myview As HDS.DAL.COMMON.TableView
                Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
                MYSPROC.JobId = ViewState("QueryJobNum").ToString
                myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
                Dim strquery As String = "TypeCode='CU'"
                myview.RowFilter = strquery
                If (myview.Count > 0) Then
                    hdnDropMailCheckId.Value = myview.RowItem("Id") & "-CU"
                    hdnDropMailToSelectedId.Value = myview.RowItem("Id") & "-CU"
                End If

            Else
                If .MailToName <> "" Then
                    If (.MailtoCU = True AndAlso .MailtoGC = False AndAlso .MailtoLE = False AndAlso .MailtoOW = False) Then

                        getMailid = getMailToId("CU", .MailToName)
                        hdnDropMailToSelectedId.Value = Trim(getMailid) + "-CU"

                    ElseIf (.MailtoGC = True AndAlso .MailtoCU = False AndAlso .MailtoLE = False AndAlso .MailtoOW = False) Then

                        getMailid = getMailToId("GC", .MailToName)
                        hdnDropMailToSelectedId.Value = Trim(getMailid) + "-GC"

                    ElseIf (.MailtoLE = True AndAlso .MailtoGC = False AndAlso .MailtoCU = False AndAlso .MailtoOW = False) Then

                        getMailid = getMailToId("LE", .MailToName)
                        hdnDropMailToSelectedId.Value = Trim(getMailid) + "-LE"

                    ElseIf (.MailtoOW = True AndAlso .MailtoLE = False AndAlso .MailtoGC = False AndAlso .MailtoCU = False) Then

                        getMailid = getMailToId("OW", .MailToName)
                        hdnDropMailToSelectedId.Value = Trim(getMailid) + "-OW"

                    Else
                    End If

                End If
                If .Payor <> "" Then

                    Dim checktypecode As String = Left(.Payor, 4)
                    Dim MailCheckName As String

                    If checktypecode.Contains("CU-") Then
                        MailCheckName = Replace(.Payor, "CU-", "", 1, 3)
                    ElseIf checktypecode.Contains("LE-") Then
                        MailCheckName = Replace(.Payor, "LE-", "", 1, 3)
                    ElseIf checktypecode.Contains("GC-") Then
                        MailCheckName = Replace(.Payor, "GC-", "", 1, 3)
                    ElseIf checktypecode.Contains("OW-") Then
                        MailCheckName = Replace(.Payor, "OW-", "", 1, 3)
                    ElseIf checktypecode.Contains("OT-") Then
                        MailCheckName = Replace(.Payor, "OT-", "", 1, 3)
                        'MailCheckName = .Payor.Split("-"c)(1)
                    Else
                        MailCheckName = .Payor
                    End If



                    If (.CheckbyCU = True AndAlso .CheckByGC = False AndAlso .CheckbyLE = False AndAlso .CheckByOT = False AndAlso .CheckbyOW = False) Then

                        getMailid = getMailToId("CU", MailCheckName)
                        hdnDropMailCheckId.Value = Trim(getMailid) + "-CU"

                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = True AndAlso .CheckbyLE = False AndAlso .CheckByOT = False AndAlso .CheckbyOW = False) Then

                        getMailid = getMailToId("GC", MailCheckName)
                        hdnDropMailCheckId.Value = Trim(getMailid) + "-GC"

                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = False AndAlso .CheckbyLE = True AndAlso .CheckByOT = False AndAlso .CheckbyOW = False) Then

                        getMailid = getMailToId("LE", MailCheckName)
                        hdnDropMailCheckId.Value = Trim(getMailid) + "-LE"

                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = False AndAlso .CheckbyLE = False AndAlso .CheckByOT = False AndAlso .CheckbyOW = True) Then

                        getMailid = getMailToId("OW", MailCheckName)
                        hdnDropMailCheckId.Value = Trim(getMailid) + "-OW"


                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = False AndAlso .CheckbyLE = False AndAlso .CheckByOT = True AndAlso .CheckbyOW = False) Then

                        getMailid = getMailToId("OT", MailCheckName)
                        hdnDropMailCheckId.Value = Trim(getMailid) + "-OT"

                    Else


                    End If
                Else
                    If (.CheckbyCU = True AndAlso .CheckByGC = False AndAlso .CheckbyLE = False AndAlso .CheckByOT = False AndAlso .CheckbyOW = False) Then

                        getDefaultCustomerId("CU")

                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = True AndAlso .CheckbyLE = False AndAlso .CheckByOT = False AndAlso .CheckbyOW = False) Then

                        getDefaultCustomerId("GC")
                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = False AndAlso .CheckbyLE = True AndAlso .CheckByOT = False AndAlso .CheckbyOW = False) Then

                        getDefaultCustomerId("LE")

                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = False AndAlso .CheckbyLE = False AndAlso .CheckByOT = False AndAlso .CheckbyOW = True) Then
                        getDefaultCustomerId("OW")

                    ElseIf (.CheckbyCU = False AndAlso .CheckByGC = False AndAlso .CheckbyLE = False AndAlso .CheckByOT = True AndAlso .CheckbyOW = False) Then

                        getDefaultCustomerId("OT")
                    End If
                End If
            End If
                If .NotaryType = 1 Then
                Me.NotarywithoutCA.Checked = True
            ElseIf .NotaryType = 2 Then
                Me.NotarywithCA.Checked = True
            End If
            Me.chkOtherIssuer.Checked = .CheckByOT

            Me.JointCheckAgreement.Checked = .JointCheckAgreement
            Me.OtherChkIssuerName.Text = .OtherChkIssuerName()
            'Me.Notary.Checked = .Notary
            Me.StartDate.Value = getdate(.StartDate)
            Me.ThroughDate.Value = getdate(.ThroughDate)

            Me.SignerId.SelectedValue = .SignerId
            hdnSignerSelectedValue.Value = .SignerId
            Me.FormId.SelectedValue = .FormId
            hdnWaiverTypeSelectedId.Value = .FormId
            Me.WaiverNote.Text = .WaiverNote
            Me.InvoicePaymentNo.Text = .InvoicePaymentNo
            Me.IsPaidInFull.Checked = .IsPaidInFull
            Me.WaiverDates.Text = .WaiverDates
            Me.WaiverPayments.Text = .WaiverPayments

        End With
        If hdnWaiverTypeSelectedId.Value = 0 Then
            hdnWaiverTypeSelectedId.Value = Me.FormId.SelectedValue
        End If
        Dim SignersId As Integer
        SignersId = Convert.ToInt32(hdnSignerSelectedValue.Value)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDowns", "SetSigner(" & SignersId & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDowns", "SetWaiverTypeId(" & Convert.ToInt32(hdnWaiverTypeSelectedId.Value) & ");", True)
        'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.FormId.SelectedValue, myform) 'Commented by Jaywanti
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(hdnWaiverTypeSelectedId.Value, myform)
        If myform.IsPIFPrompt = True And myjob.StatusCodeId <> "60" Then
            Me.IsMarkPIF.Visible = True

        Else
            Me.IsMarkPIF.Visible = False

        End If

        If myjob.JobState <> "CA" Then
            Me.WaiverDates.Visible = False
            Me.WaiverPayments.Visible = False
        End If

        If myjob.JobState <> "NV" Then
            Me.InvoicePaymentNo.Visible = False
        End If

        'If myjob.JobState <> "TX" Then
        '    Me.Notary.Checked = False
        'Else
        '    Me.Notary.Checked = True
        'End If
        txtCustEmail.Text = mycustomer.Email
        ViewState("custEmail") = mycustomer.Email
    End Sub

    Private Sub getDefaultCustomerId(ByVal type As String)
        Dim myview As HDS.DAL.COMMON.TableView
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
        MYSPROC.JobId = ViewState("QueryJobNum").ToString
        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
        Dim strquery As String = "TypeCode='" + type + "'"
        myview.RowFilter = strquery
        If (myview.Count > 0) Then
            hdnDropMailCheckId.Value = myview.RowItem("Id") & "-" & type
            hdnDropMailCheckIdText.Value = myview.RowItem("AddressName")
        End If

    End Sub

    Private Function getMailToId(ByVal TypeCode As String, ByVal Name As String) As String

        Dim myview As HDS.DAL.COMMON.TableView
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
        MYSPROC.JobId = ViewState("QueryJobNum").ToString
        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
        Dim strquery As String = ""
        If InStr(Name, "'") Then
            strquery = "AddressName like '%" + Replace(Name, "'", "''") + "%' And TypeCode='" + TypeCode + "'"
        Else
            strquery = "AddressName like '%" + Name + "%' And TypeCode='" + TypeCode + "'"
        End If
        Dim GetId As String = ""
        myview.RowFilter = strquery
        If (myview.Count > 0) Then
            GetId = myview.RowItem("Id")
        End If

        Return GetId
    End Function
    Private Function GetLastSignerId() As Integer
        Try
            Dim mysql As String = "select top 1 jobwaiverlogid,datecreated,signerid,requestedbyuserid from jobwaiverlog"
            mysql += " where SignerId Is Not null and requestedbyuserid='" & Me.CurrentUser.Id & "' "
            mysql += " order by jobwaiverlogid desc"
            Dim myds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetDataSet(mysql)
            If myds.Tables(0).Rows.Count < 1 Then
                mysignerid = 0
            End If
            mysignerid = myds.Tables(0).Rows(0).Item("SignerId")

        Catch ex As Exception
            mysignerid = 0
        End Try

    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSAVE.Click

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If qs.HasParameter("SubId") Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
        End If

        With myreq
            .JobId = ItemId
            '.FormId = Me.FormId.SelectedValue 'Commented By Jaywanti
            .FormId = hdnWaiverTypeSelectedId.Value
            '.FormCode = Strings.Left(Me.FormId.SelectedItem.Text, 20) 'Commented By Jaywanti
            .FormCode = Strings.Left(hdnWaiverTypeSelectedText.Value, 50)
            'ViewState("subjectwaivertype") = hdnWaiverTypeSelectedText.Value
            .PaymentAmt = Me.PaymentAmt.Value
            .DisputedAmt = Me.DisputedAmt.Value
            Dim selectedMailTo As String = hdnDropMailToSelectedId.Value
            Dim drpMailToTypeCode As String = selectedMailTo.Substring(selectedMailTo.IndexOf("-"c) + 1)

            If (drpMailToTypeCode = "CU") Then
                .MailtoCU = True
                .MailtoGC = False
                .MailtoLE = False
                .MailtoOW = False
            ElseIf (drpMailToTypeCode = "GC") Then
                .MailtoGC = True
                .MailtoCU = False
                .MailtoLE = False
                .MailtoOW = False
            ElseIf (drpMailToTypeCode = "LE") Then
                .MailtoLE = True
                .MailtoGC = False
                .MailtoCU = False
                .MailtoOW = False
            ElseIf (drpMailToTypeCode = "OW") Then
                .MailtoOW = True
                .MailtoLE = False
                .MailtoGC = False
                .MailtoCU = False

            Else
                .MailtoOW = False
                .MailtoLE = False
                .MailtoGC = False
                .MailtoCU = False

            End If
            Dim selectedMailCheck As String = hdnDropMailCheckId.Value
            Dim drpMailCheckTypeCode As String = selectedMailCheck.Substring(selectedMailCheck.IndexOf("-"c) + 1)

            If (drpMailCheckTypeCode = "CU") Then
                .CheckbyCU = True
                .CheckByGC = False
                .CheckbyLE = False
                .CheckByOT = False
                .CheckbyOW = False
            ElseIf (drpMailCheckTypeCode = "GC") Then
                .CheckbyCU = False
                .CheckByGC = True
                .CheckbyLE = False
                .CheckByOT = False
                .CheckbyOW = False
            ElseIf (drpMailCheckTypeCode = "LE") Then
                .CheckbyCU = False
                .CheckByGC = False
                .CheckbyLE = True
                .CheckByOT = False
                .CheckbyOW = False
            ElseIf (drpMailCheckTypeCode = "OW") Then
                .CheckbyCU = False
                .CheckByGC = False
                .CheckbyLE = False
                .CheckByOT = False
                .CheckbyOW = True
            ElseIf (drpMailCheckTypeCode = "OT") Then
                .CheckbyCU = False
                .CheckByGC = False
                .CheckbyLE = False
                .CheckByOT = True
                .CheckbyOW = False

            Else

                .CheckbyCU = False
                .CheckByGC = False
                .CheckbyLE = False
                .CheckByOT = False
                .CheckbyOW = False

            End If
            If (Me.chkOtherIssuer.Checked = True) Then
                .CheckByOT = True
            End If
            If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                .Notary = True
                If (NotarywithoutCA.Checked = True And NotarywithCA.Checked = False) Then
                    .NotaryType = 1
                ElseIf (NotarywithCA.Checked = True And NotarywithoutCA.Checked = False) Then
                    .NotaryType = 2
                End If
            Else
                .Notary = False
            End If
            ' .Payor = Me.hdnDropMailCheckIdText.Value.Split("-"c)(1)
            .JointCheckAgreement = Me.JointCheckAgreement.Checked
            .OtherChkIssuerName = Me.OtherChkIssuerName.Text
            '.Notary = Me.Notary.Checked
            .StartDate = Nothing
            .ThroughDate = Nothing
            If Me.StartDate.Value.Length > 1 Then
                If IsDate(Me.StartDate.Value) Then
                    .StartDate = Utils.GetDate(Me.StartDate.Value)
                End If
            End If
            If Me.ThroughDate.Value.Length > 1 Then
                If IsDate(Me.ThroughDate.Value) Then
                    .ThroughDate = Utils.GetDate(Me.ThroughDate.Value)
                End If
            End If

            .RequestedBy = Left(Me.UserInfo.UserInfo.LoginCode, 10)
            .RequestedByUserId = Me.CurrentUser.Id
            If Me.SignerId.SelectedIndex >= 0 Then
                .SignerId = Me.SignerId.SelectedValue
            End If
            If hdnSignerSelectedValue.Value <> "" And hdnSignerSelectedValue.Value <> "0" Then
                .SignerId = hdnSignerSelectedValue.Value
                Response.Cookies("SignerSelectedValue")("Data") = hdnSignerSelectedValue.Value
                Response.Cookies("SignerSelectedValue")("Time") = DateTime.Now.ToString("G")
                Response.Cookies("SignerSelectedValue").Expires = DateTime.Now.AddYears(1)
            End If

            .WaiverNote = Me.WaiverNote.Text
            .IsPaidInFull = Me.IsPaidInFull.Checked
            .IsMarkPIF = Me.IsMarkPIF.Checked

            If myjob.JobState = "CA" Then
                .WaiverDates = Me.WaiverDates.Text
                .WaiverPayments = Me.WaiverPayments.Text
            End If

            If myjob.JobState = "NV" Then
                .InvoicePaymentNo = Me.InvoicePaymentNo.Text
            End If

        End With
        If Not ViewState("QueryJobNum").ToString Is Nothing Then
            Dim dtlegalpartyinfo As System.Data.DataTable
            If hdnDropMailToSelectedId.Value <> "" Then
                dtlegalpartyinfo = GetLegalPartyName(hdnDropMailToSelectedId.Value)
                If (dtlegalpartyinfo.Rows.Count > 0) Then
                    myreq.MailToName = dtlegalpartyinfo.Rows(0)("AddressName").ToString()
                    myreq.MailToAddr1 = dtlegalpartyinfo.Rows(0)("AddressLine1").ToString()
                    myreq.MailToCity = dtlegalpartyinfo.Rows(0)("City").ToString()
                    myreq.MailToState = dtlegalpartyinfo.Rows(0)("State").ToString()
                    myreq.MailToZip = dtlegalpartyinfo.Rows(0)("PostalCode").ToString()
                End If
            End If
            If hdnDropMailCheckId.Value <> "" Then
                dtlegalpartyinfo = GetLegalPartyName(hdnDropMailCheckId.Value)
                If (dtlegalpartyinfo.Rows.Count > 0) Then
                    myreq.Payor = dtlegalpartyinfo.Rows(0)("AddressName").ToString()

                End If
            End If
        End If

        If myreq.JobWaiverLogId > 0 Then
            myreq.DatePrinted = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
        Else
            myreq.DatePrinted = Today
            myreq.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myreq)
        End If

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myreq.JobId, myjob)
        'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.FormId.SelectedValue, myform) 'Commented by Jaywanti
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(hdnWaiverTypeSelectedId.Value, myform)
        If myform.IsPIFPrompt = True And Me.IsMarkPIF.Checked = True And myjob.StatusCodeId <> "60" Then
            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = myjob.Id
                .DateCreated = Now()
                .Note += "Job Changed from Active to Paid In Full"
                .Note += vbCrLf
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
            End With
            With myjob
                .StatusCodeId = 60
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
            End With

        End If
        ViewState("JobWaiverLogId") = myreq.JobWaiverLogId
        Me.Session("LastWaiverForm") = myreq.FormId
        Dim s As String
        If (Me.PageName = "JobView") Then
            s = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&tabstate=" & "WAIVER" & "&view=" & "WAIVER" & "&formid=" & myreq.JobWaiverLogId
        Else
            s = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&tabstate=" & "WAIVER" & "&view=" & "WAIVER" & "&formid=" & myreq.JobWaiverLogId
        End If

        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If
        Session("currenttab") = "WAIVER"

        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsAlertSuccessFunc", "AlertSuccessFunc();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayWaiverPopUp", "DisplayWaiverPopUp();", True)

    End Sub
    Protected Function GetLegalPartyName(ByVal selectedlegalpartyId As String) As System.Data.DataTable
        Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
        obj.JobId = ViewState("QueryJobNum").ToString
        Dim view As HDS.DAL.COMMON.TableView
        view = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
        Dim dtjobinfo As System.Data.DataTable
        dtjobinfo = view.ToTable()
        If Not view Is Nothing Then
            'Dim view As New DataView

            view.Table = dtjobinfo
            Dim strquery As String = ""
            If dtjobinfo.Rows.Count > 0 Then
                Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
                If selectedlegalpartyId <> "" Then
                    Dim str As String()
                    str = selectedlegalpartyId.ToString().Split("-")
                    For count = 0 To str.Length - 1
                        If (count = 0) Then
                            strquery = "Id=" + str(count).Trim()
                        ElseIf (count = 1) Then
                            strquery = strquery + " AND TypeCode='" + str(count).Trim() + "'"
                        End If
                    Next
                    view.RowFilter = strquery
                    dtjobinfo = view.ToTable()
                End If
            End If
        End If
        Return dtjobinfo

    End Function


    Private Function setdate(ByVal adate As String) As String
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return ""
        End If
        If s = Date.MinValue Then
            Return ""
        End If
        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Private Function getdate(ByVal adate As String) As Date
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return Date.MinValue
        End If

        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function

    Protected Sub AddSigner1_ItemSaved() Handles AddSigner1.ItemSaved
        Dim li As New ListItem
        Dim VWSIGNERS As HDS.DAL.COMMON.TableView = LienView.Provider.GetClientSigners
        VWSIGNERS.MoveFirst()
        Me.SignerId.Items.Clear()
        Do Until VWSIGNERS.EOF
            li = New ListItem
            Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
            csg = VWSIGNERS.FillEntity(csg)
            li.Value = csg.Id
            li.Text = csg.Signer & "," & csg.SignerTitle
            Me.SignerId.Items.Add(li)
            VWSIGNERS.MoveNext()
        Loop
        HttpContext.Current.Session("SignerList") = Nothing
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & myjob.JobState & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0);", True)
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click


        Session("exitviewer") = "1"
        Session("currenttab") = "WAIVER"
        If Me.PageName = "JobView" Then
            Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&view=" & "WAIVER"
            Response.Redirect(Me.Page.ResolveUrl(s), True)
        Else
            Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&view=" & "WAIVER"
            Response.Redirect(Me.Page.ResolveUrl(s), True)
        End If

        'If Me.Session("LIST") <> Nothing Then
        '    s += "&list=" & Me.Session("LIST")
        'End If




    End Sub

    Protected Sub FormId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormId.SelectedIndexChanged
        'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.FormId.SelectedValue, myform) 'Commented by Jaywanti
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(hdnWaiverTypeSelectedId.Value, myform)
        If myform.IsPIFPrompt = True And myjob.StatusCodeId <> "60" Then
            Me.IsMarkPIF.Visible = True

        Else
            Me.IsMarkPIF.Visible = False

        End If

    End Sub
    Protected Sub btnForm_Click(sender As Object, e As EventArgs)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(hdnWaiverTypeSelectedId.Value, myform)
        If myform.IsPIFPrompt = True And myjob.StatusCodeId <> "60" Then
            Me.IsMarkPIF.Visible = True

        Else
            Me.IsMarkPIF.Visible = False

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & myjob.JobState & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & hdnSignerSelectedValue.Value & ");", True)
    End Sub
    Protected Sub btnNew_Click(sender As Object, e As EventArgs)

        Dim URL As String = "MainDefault.aspx?lienview.jobeditwaiver&itemid=" & myjob.Id & "&subid=" & myreq.JobWaiverLogId
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + URL + "'", True)

    End Sub
    Protected Sub btnEmail_Click(sender As Object, e As EventArgs)
        QueryJobNum = ViewState("QueryJobNum").ToString
        'Dim s As String = "~/MainDefault.aspx?lienview.BulkWaiver&itemid=" & Me.ItemId & "&subid=" & -1 & "&PageName=JobView"
        Dim s As String = "~/MainDefault.aspx?lienview.BulkWaiver&itemid=" & QueryJobNum

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub



    Public Sub EmailWaiver(ByVal JobWaiverLogId As String)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(JobWaiverLogId)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                PrintNotary()
                MergeNotary(sreport)
            End If
            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            Dim Flagerror = EmailToUser(txtToList.Text, txtSubject.Text, txtBody.Text, Filename, fileArray) 'For a timing
            If Flagerror = "0" Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EmailSendPopup", "EmailSendPopup();", True)
            End If
        End If


    End Sub


    Public Sub PrintWaiver()


        JobWaiverLogId = ViewState("JobWaiverLogId").ToString
        Dim sreport As String = LienView.Provider.PrintJobWaiver(JobWaiverLogId)

        If IsNothing(sreport) = False Then

            ' Me.btnDownLoad.Visible = True
            Dim s1 As String = Me.ResolveUrl("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
            If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                PrintNotary()
                MergeNotary(sreport)

            End If
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            '  Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", s1, 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)

                End If
            End If
            Dim URL As String = "MainDefault.aspx?lienview.jobeditwaiver&itemid=" & myjob.Id & "&subid=" & JobWaiverLogId
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + URL + "'", True)
        End If

    End Sub

    Public Sub PrintNotary()
        JobWaiverLogId = ViewState("JobWaiverLogId").ToString
        Dim sreport As String = LienView.Provider.PrintJobNotary(JobWaiverLogId)

        If IsNothing(sreport) = False Then

            s1 = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))

        End If

    End Sub
    Public Sub MergeNotary(ByVal sreport As String)
        Dim s2 As String = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
        Dim path As String = ConfigurationManager.AppSettings("path").ToString()
        File.Create(path & (System.IO.Path.GetFileName(sreport))).Close()
        Dim notary As String

        notary = s1

        Dim result As String = path & (System.IO.Path.GetFileName(sreport))
        Dim source_files() As String = {s2, notary}
        Dim document As iTextSharp.text.Document = New iTextSharp.text.Document
        Dim copy As iTextSharp.text.pdf.PdfCopy = New iTextSharp.text.pdf.PdfCopy(document, New FileStream(result, FileMode.Create))
        'open the document
        document.Open()
        Dim reader As iTextSharp.text.pdf.PdfReader
        Dim i As Integer = 0
        Do While (i <source_files.Length)
            'create PdfReader object
                reader = New iTextSharp.text.pdf.PdfReader(source_files(i))
            'merge combine pages
            Dim page As Integer = 1
            Do While (page <= reader.NumberOfPages)
                copy.AddPage(copy.GetImportedPage(reader, page))
                page = (page + 1)
            Loop

            i = (i + 1)
        Loop

        'close the document object
        document.Close()
        '-------------------------------------------------------------
        File.Copy(result, sreport, True)
        File.Delete(result)

    End Sub

    Protected Sub btnPrintWaiver_Click(sender As Object, e As EventArgs)
        PrintWaiver()

    End Sub

    Protected Sub btnEmailWaiver_Click(sender As Object, e As EventArgs)


        custEmail = ViewState("custEmail").ToString
        Dim EmailUserName As String = Me.UserInfo.UserInfo.UserName
        Dim EmailUserEmail As String = Me.UserInfo.UserInfo.Email



        JobAddSubjectLine = ViewState("JobAddSubjectLine").ToString
        Me.txtToList.Text = txtCustEmail.Text
        'Me.txtToList.Text = ""
        If (JobAddSubjectLine.Trim() <> "") Then
            txtSubject.Text = "New Waiver Document for " + JobAddSubjectLine

        Else
            txtSubject.Text = "New Waiver Document for "
        End If

        Me.txtBody.Text = "PLEASE DO NOT RESPOND TO THIS EMAIL." + vbCrLf + "If you have any questions, or need any assistance, please contact " + EmailUserName + " at " + EmailUserEmail + "."


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideWaiverPopUp", "HideWaiverPopUp();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayEmailPopUp", "DisplayEmailPopUp();", True)



    End Sub

    Public Shared Function GetSettings() As TABLES.Report_Settings
        Dim myitem As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, myitem)

        Return myitem

    End Function
    Public Shared Function EmailToUser(ByVal email As String, ByVal subject As String, ByVal strBody As String, ByVal FileName As String, ByVal pdfcontent As Byte())

        Dim Flagerror As String = "0"


        Dim ToArrayList As String()
        ToArrayList = email.Split(";")

        Dim MYSITE As TABLES.Report_Settings = GetSettings()

        Dim msg As New MailMessage
        'msg.From = New MailAddress("priyanka", "ClientView")
        msg.From = New MailAddress("CRFIT@crfsolutions.com", "Job Waiver Central")
        msg.Subject = subject
        For index = 0 To ToArrayList.Length - 1
            msg.To.Add(ToArrayList(index))
        Next
        msg.IsBodyHtml = True
        msg.Body = strBody
        msg.Attachments.Add(New Attachment(New MemoryStream(pdfcontent), FileName))

        'msg.Body = body

        Try
            With MYSITE
                Dim _smtp As New System.Net.Mail.SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
            End With
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            MsgBox("Error On email:" & vbCrLf & mymsg)
            Flagerror = "1"
        End Try
        Return Flagerror
    End Function
    Protected Sub btnEmailsend_Click(sender As Object, e As EventArgs)

        JobWaiverLogId = ViewState("JobWaiverLogId").ToString
        If Me.txtToList.Text.Trim() <> "" Then
            EmailWaiver(JobWaiverLogId)
        End If
    End Sub


    'Protected Sub BindGridMailTo()

    'End Sub
    Protected Sub hdnbtnclose_Click(sender As Object, e As EventArgs)
        JobWaiverLogId = ViewState("JobWaiverLogId").ToString
        Dim URL As String = "MainDefault.aspx?lienview.jobeditwaiver&itemid=" & myjob.Id & "&subid=" & JobWaiverLogId
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + URL + "'", True)
    End Sub



End Class