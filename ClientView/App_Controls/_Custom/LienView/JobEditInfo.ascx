<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<script runat="server">
  dim childid as String = "-1"
    Dim jobid As String = ""
    Dim itemid As String
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobClientViewUpdates
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If qs.HasParameter("action") Then
            Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")
            If s.Length > 1 Then
                childid = s(1)
            End If
            If Page.IsPostBack = False And s(0) = "vjobinfo" Then
                LoadData(jobid, childid)
            End If
        End If
    End Sub
    Public Sub LoadData(ByVal ajobid As String, Optional ByVal childid As String = "-1")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.ItemId, myjob)
        With myjob
            Me.EndDate.SelectedValue = myjob.EndDate
            Me.StartDate.SelectedValue = myjob.StartDate
            Me.EstBalance.Value = myjob.EstBalance
        End With
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
       
        qs.SetParameter("action", "vjobinfo")
        Response.Redirect(qs.All, True)
 
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        CustomValidator1.Visible = True
        With myreq
            .JobId = Me.ItemId
            .DateCreated = Now
            .UserId = Me.CurrentUser.id
            '   .UserCode = (Left(Me.CurrentUser.username, 10))
            .EndDate = Me.EndDate.PostedDate
            .StartDate = Me.StartDate.PostedDate
            .LienAlert = Me.chkLienAlert.Checked
            .FileMechanicLien = Me.chkFileMechanic.Checked
            .JobPaidInFull = Me.chkJobPaidInFull.Checked
            .SendAmendedNotice = Me.chkSendAmended.Checked
            .WaiverAlert = Me.chkWaiverAlert.Checked
            .EstBalance = Me.EstBalance.Value
            .LienAmount = Me.LienAmount.Value
        End With
        
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myreq)
        
        ' 1/17/2013
        CRFS.ClientView.CrfsBll.UpdateDeadlineDates(0, Me.itemid)

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        qs.SetParameter("action", "vjobinfo")
        Response.Redirect(qs.All, True)
    End Sub
 
</script>
<h2>Edit Job Form</h2>
<div class="body" style="background-color:#edf5ff; border:1px solid, #a3a3a3;" >
<table class="body" style="background-color:#edf5ff; border:1px solid, #a3a3a3;"  >
    <tr>
        <td class="row-label" style="width: 168px">
            Start Date:</td>
        <td class="row-data">
            <ew:calendarpopup id="StartDate" runat="server" width="72px"></ew:calendarpopup>
        </td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            End Date:</td>
        <td class="row-data">
            <ew:calendarpopup id="EndDate" runat="server" width="72px"></ew:calendarpopup>
        </td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            Est Balance:</td>
        <td class="row-data">
            <cc1:dataentrybox id="EstBalance" runat="server"  
                text="" width="152px" DataType="Money" ></cc1:dataentrybox>
        </td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            Send Amended Notice:</td>
        <td class="row-data">
            <asp:CheckBox ID="chkSendAmended" runat="server" /></td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            File Mechanic Lien:</td>
        <td class="row-data">
            <asp:CheckBox ID="chkFileMechanic" runat="server" /></td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            Lien Amount:</td>
        <td class="row-data">
            <cc1:dataentrybox id="LienAmount" runat="server"  
                text="" width="152px" DataType="Money"></cc1:dataentrybox>
        </td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            Job Paid In Full:</td>
        <td class="row-data">
            <asp:CheckBox ID="chkJobPaidInFull" runat="server" /></td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            Remove Lien Alert:</td>
        <td class="row-data">
            <asp:CheckBox ID="chkLienAlert" runat="server" /></td>
    </tr>
    <tr>
        <td class="row-label" style="width: 168px; height: 22px">
            Remove Waiver Alert:</td>
        <td class="row-data">
            <asp:CheckBox ID="chkWaiverAlert" runat="server" /></td>
    </tr>
</table>
<cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
<asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
</div>
<div class="footer">
    <asp:LinkButton ID="btnCancel" runat="server" CssClass="button" Text="Cancel"></asp:LinkButton>&nbsp;&nbsp;
    <asp:LinkButton ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:LinkButton>&nbsp;<br />
    <br />
    <br />
</div>
