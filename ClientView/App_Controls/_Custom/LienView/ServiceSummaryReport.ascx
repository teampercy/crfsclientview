﻿<%--<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServiceSummaryReport.ascx.cs" Inherits="App_Controls_JobView_ServiceSummaryReport" %>--%>
<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>


<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Reports', 'Service Summary');
        MainMenuToggle('liLienViewReport');
        SubMenuToggle('liServiceSummaryLienView');
      
        return false;
    }

    
    function BindClientSelection() {
        $('#UlClientSelectionType li').remove();
        var x = document.getElementById('<%=DropDownList1.ClientID%>');
        var txt = "";
        var i;
        for (i = 0; i < x.length; i++) {
            var ResultReplace = ReplaceSpecChar(x.options[i].text);
            $('#UlClientSelectionType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClientSel_' + x.options[i].value + '" onclick="setSelectedClientId(' + x.options[i].value + ',\'' + ResultReplace + '\')">' + x.options[i].text + '</a></li>');
        }
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val() != "") {
            $('#btnClientSelectionType').html($('#liClientSel_' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedClientId(id, TextData) {
        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnClientSelectionId.ClientID%>').val(id);
        $('#btnClientSelectionType').html(ResultReplace + "<span class='caret' ></span>");
         if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val().length > 0)
        {
            <%-- alert('hii ' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val() );--%>
            var value = "SELECTED";
            var radio = $("[id*=ctl33_ctl00_RadioButtonList1] input[value=" + value + "]");
            radio.attr("checked", "checked");

        }
        return false;
    }


</script>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Calendar1.Value = Today
            Me.Calendar2.Value = Today
            'LoadClientList()
            If IsNothing(Me.Page.Request.QueryString("rpt")) = False Then
                Me.DownLoadReport(Me.Page.Request.QueryString("rpt"))
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If


        'If hdnClientSelectionId.Value <> "" Then
        '    RadioButtonList1.Items.FindByValue("SELECTED").Selected = True
        'End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetServiceSummaryLV
        With MYSPROC
            'If Me.panClients.Visible = True Then
            '    If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
            '        .ClientCode = Me.DropDownList1.SelectedValue
            '        .ClientCode = Me.hdnClientSelectionId.Value
            '    End If
            'End If

            .USERID = Me.CurrentUser.Id
            .JOBSTATE = Me.txtJobState.Text
            .DOCUMENTFROM = Me.Calendar1.Value
            .DOCUMENTTO = Me.Calendar2.Value
        End With
        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetServiceSummaryReportLV(MYSPROC, False))
        Me.btnDownLoad.Visible = False
        Me.plcReportViewer.Visible = True
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.btnDownLoad.Visible = True
            Me.plcReportViewer.Visible = False
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

        Me.plcReportViewer.Controls.Clear()
        Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
        Me.MultiView1.SetActiveView(Me.View2)
    End Sub



    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        Me.MultiView1.SetActiveView(Me.View1)
        Me.btnDownLoad.Visible = False

    End Sub

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DownLoadReport(Me.Session("spreadsheet"))
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
    End Sub

    Private Sub LoadClientList()

        Dim vwclients As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop
            Me.panClients.Visible = True
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        End If
    End Sub
</script>


<div id="modcontainer" style="MARGIN: 10px; WIDTH: 100%;">
    <h1 class="panelheader">Service Summary Report</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">
          
                <asp:Panel ID="panClients" Visible="false" runat="server">
                    <div class="form-horizontal" style="display: none;">
                        <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label  align-lable">Client Select:</label>
                        <div class="col-md-4 col-sm-4 col-xs-4  " style="text-align: left;">
                                  <asp:DropDownList ID="DropDownList1" runat="server" Style="display: none;">
                                </asp:DropDownList>
                            <asp:HiddenField ID="hdnClientSelectionId" runat="server" />
                            <div class="btn-group" style="text-align: left; width: 100%">
                                <button type="button" class="btn btn-default dropdown-toggle" style="width: 100%; text-align: left;" align="left"
                                    data-toggle="dropdown" aria-expanded="false" id="btnClientSelectionType" tabindex="1">
                                    --Select Client Selection--
                     
                                                                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlClientSelectionType" style="overflow-y: auto; height: 150px;">
                                </ul>
                            </div>
                        </div>
                        <div class="radio-custom radio-default radio-inline col-md-4 col-sm-4 col-xs-4">
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL" style="white-space: nowrap;"> All Clients  </asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED" style="white-space: nowrap; padding-left: 40px;"> Selected Client</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                    </div>
                        </div>
                     </asp:Panel>

                <div class="form-horizontal">

                   <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable"> Job State:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5 ">
                            <asp:TextBox runat="server" ID="txtJobState" CssClass="form-control" Width="11%" MaxLength="2" />
                              </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Service Date:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5 ">
                            <div style="float: left;">
                           <span style="display: flex;" >
                                <SiteControls:Calendar ID="Calendar1" Value="TODAY" IsReadOnly="false"  runat="server" />
                            </span>
                                </div>
                       
                            <div>
                             <label class="control-label align-lable" style="float: left;padding-left: 20px;padding-right: 20px;">Thru:</label>
                           <span style="display: flex;">
                                <SiteControls:Calendar ID="Calendar2" Value="TODAY" IsReadOnly="false" runat="server" />
                           </span>
                        </div>
                     </div>
                   </div>
                </div>

            </div>
            <div class="footer">
                <div class="form-group">

                    <div class="col-md-12" style="text-align: right;">
                        <asp:UpdatePanel ID="updPan1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSubmit"
                                    CssClass="btn btn-primary"
                                    runat="server"
                                    Text="Create Report" OnClick="btnSubmit_Click"/>&nbsp;
                                                            <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                                                                <ProgressTemplate>
                                                                    <div class="TransparentGrayBackground"></div>
                                                                    <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                                                        <div class="PageUpdateProgress">
                                                                            <asp:Image ID="ajaxLoadNotificationImage"
                                                                                runat="server"
                                                                                ImageUrl="~/images/ajax-loader.gif"
                                                                                AlternateText="[image]" />
                                                                            &nbsp;Please Wait...
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <ajaxToolkit:AlwaysVisibleControlExtender
                                                                        ID="AlwaysVisibleControlExtender1"
                                                                        runat="server"
                                                                        TargetControlID="alwaysVisibleAjaxPanel"
                                                                        HorizontalSide="Center"
                                                                        HorizontalOffset="150"
                                                                        VerticalSide="Middle"
                                                                        VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12" style="text-align: left;">
                            <asp:Button ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Selection" OnClick="Linkbutton2_Click"></asp:Button>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <div style="width: auto; height: auto; overflow: auto;">
                                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:Button ID="btnDownLoad" Text="DownLoad SpreadSheet" runat="server" CssClass="btn btn-primary" OnClick="btnDownLoad_Click" />
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
    </asp:MultiView>
</div>