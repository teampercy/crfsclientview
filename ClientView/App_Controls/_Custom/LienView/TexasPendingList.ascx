<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script type="text/javascript">
    $(document).ready(function () {
        CallSuccessFuncTexasList();
    });
    function DeleteAccount(id) {
        document.getElementById('<%=hdnId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteId.ClientID%>').click();
      });
        return false;
    }
    function CallSuccessFuncTexasList() {

        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [15, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                 "<'row'<'col-sm-4'il><'col-sm-7'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwList.ClientID%>').dataTable(options);

         //debugger;
         //  EndLoading();

     }
    function MaintainMenuOpen() {
        debugger;
        SetHeaderBreadCrumb('LienView', 'Tools', 'Texas Pending List');
        MainMenuToggle('liTools');
        SubMenuToggle('liTexaspendingList');
        //var PageName = window.location.search.substring(1);
        ////alert(PageName);
        //if (PageName == "lienview.texaspendinglist.JobView") {
        //    SetHeaderBreadCrumb('RentalView', 'Tools', 'Texas Pending List');
        //    MainMenuToggle('liRentalToolsJobView');
        //    SubMenuToggle('liTexaspendingListJobView');
        //}
        //else {
        //    SetHeaderBreadCrumb('LienView', 'Tools', 'Texas Pending List');
        //    MainMenuToggle('liTools');
        //    SubMenuToggle('liTexaspendingList');
        //}
         
         //$('#liTools').addClass('site-menu-item has-sub active open');
         //$('#liTexaspendingList').addClass('site-menu-item active');
         return false;

     }

</script>
<script runat="server">
    Protected WithEvents btnEditItem As LinkButton
    Protected WithEvents btnDeleteTReq As LinkButton
    Protected WithEvents btnEditTReq As LinkButton
    Protected WithEvents btnPrntTReq As LinkButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.UserId.Value = Me.CurrentUser.Id
            Me.fromAsgDate.Value = DateAdd(DateInterval.Day, -180, Today)
            Me.thruAsgDate.Value = DateAdd(DateInterval.Day, 1, Today)
            Dim mydt As System.Data.DataTable
            mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverDaysRemaining(Me.CurrentUser.Id).Tables(0)
            Me.gvwList.DataSourceID = Me.objWithEndDates.ID
            Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)

            Me.gvwList.DataBind()
            If gvwList.Rows.Count > 0 Then
                Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasPending", "CallSuccessFuncTexasList();", True)
        End If
    End Sub

    Protected Sub gvwItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvwList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvwList.PageSize = Integer.Parse(dropDown.SelectedValue)

    End Sub
    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntTReq"), LinkButton).Click, AddressOf btnPrntTReq_Click
            Me.btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditTReq"), LinkButton).Click, AddressOf btnEditTreq_Click
            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteTReq"), LinkButton).Click, AddressOf btnDeleteTReq_Click

        End If

    End Sub
    Protected Sub gvwList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            btnEditTReq.Attributes("rowno") = DR("JobId")

            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            btnPrntTReq.Attributes("rowno") = DR("JobId")

            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            btnDeleteTReq.Attributes("rowno") = DR("RequestId")
            e.Row.Cells(2).Text = "" & Left(DR("JobName").ToString.Trim, 25)
            e.Row.Cells(3).Text = "" & Left(DR("CustName").ToString.Trim, 25)
            e.Row.Cells(4).Text = "" & Left(DR("JobAdd1").ToString.Trim, 25)
            e.Row.Cells(6).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            e.Row.Cells(9).Text = "$" & Left(DR("AmountOwed").ToString.Trim, 35)
            e.Row.Cells(10).Text = "" & Left(DR("MonthDebtIncurred").ToString.Trim, 35)
        End If

    End Sub
    Private Function FormatDate(ByVal adate As String) As String
        If IsDate(adate) = False Then
            Return ""
        End If
        Return Strings.FormatDateTime(adate)
    End Function


    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mydt As System.Data.DataTable
        mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverDaysRemaining(Me.CurrentUser.Id).Tables(0)
        Me.gvwList.DataSourceID = Me.objWithEndDates.ID
        Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)

        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexas", "CallSuccessFuncTexasList();", True)

    End Sub

    Protected Sub btnEditTreq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        'Dim s As String = "?lienview.jobinfo&itemid=" & myitemid & "&list=tpr"//Commented by Jaywanti
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myitemid & "&list=tpr" & "&PageName=TexaspendList" 'added by jaywanti

        Dim fullpath As String
        fullpath = Request.Url.AbsoluteUri
        If fullpath.Contains("JobView") Then
            s =  "~/MainDefault.aspx?jobview.jobviewinfo.texaspendingList&itemid=" & myitemid 'added by jaywanti
        End If
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub btnDeleteTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteTReq.Click
        btnDeleteTReq = TryCast(sender, LinkButton)
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
        myitem.Id = btnDeleteTReq.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList", "CallSuccessFuncTexasList();", True)
    End Sub

    Protected Sub Linkbutton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub
    Protected Sub btnPrntTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntTReq.Click
        btnEditItem = TryCast(sender, LinkButton)

        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetTexasRequestAck(btnEditItem.Attributes("rowno"))
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "true")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList1", "CallSuccessFuncTexasList();", True)
    End Sub



    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests
        With MYSPROC
            .FromDate = "01/01/1980"
            .ThruDate = Today
            .UserId = Me.CurrentUser.Id
            .SelectPending = True
        End With
        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetTexasPendingReport(MYSPROC, True))
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList1", "CallSuccessFuncTexasList();", True)
    End Sub

    Protected Sub btnSpreadsheet_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests
        With MYSPROC
            .FromDate = "01/01/1980"
            .ThruDate = Today
            .UserId = Me.CurrentUser.Id
            .SelectPending = True
        End With
        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetTexasPendingReport(MYSPROC, False))
        If IsNothing(sreport) = False Then
            'Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            'Me.btnDownLoad.Visible = True
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.MultiView1.SetActiveView(Me.View2)
            Me.DownLoadReport(Me.Session("spreadsheet"))

        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasPendingList", "CallSuccessFuncTexasList();", True)
        'Me.btnDownLoad.Visible = False
    End Sub

    'Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Me.DownLoadReport(Me.Session("spreadsheet"))
    'End Sub

    Protected Sub btnDeleteId_Click(sender As Object, e As EventArgs)
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
        myitem.Id = hdnId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)
        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList", "CallSuccessFuncTexasList();", True)
    End Sub
</script>
<asp:ObjectDataSource ID="objWithEndDates" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetRequests"
    TypeName="Data.TexasRequestProvider">
    <SelectParameters>
        <asp:Parameter Name="maxrows" Type="Int32" />
        <asp:Parameter Name="afilter" Type="String" />
        <asp:Parameter Name="asort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="uspbo_ClientView_GetWaiverDaysRemaining"
    TypeName="Data.TexasRequestProvider">
    <SelectParameters>
        <asp:Parameter Name="UserId" Type="String" />
        <asp:Parameter Name="ClientCode" Type="String" />
        <asp:Parameter Name="RETURNVALUE" Type="Int16" Direction="Output" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="MARGIN: 10px  0px 0px 0px; width: 100%;">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Texas Pending List</h1>

                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12" style="text-align: left;">
                                    <asp:LinkButton ID="btnReport" runat="server" CssClass="btn btn-primary"
                                        Text="Print PDF Report" OnClick="btnReport_Click" />
                                    <asp:LinkButton ID="btnSpreadsheet" runat="server" CssClass="btn btn-primary"
                                        OnClick="btnSpreadsheet_Click" Text="Print Spreadsheet" />
                                    <asp:LinkButton ID="btnRefresh" runat="server" CssClass="btn btn-primary"
                                        OnClick="btnRefresh_Click" Text="Refresh List" />
                                </div>

                            </div>
                            <div class="form-group">

                                <div class="col-md-12">

                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:GridView ID="gvwList" runat="server" DataSourceID="ItemsDataSource"
                                            CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                            BorderWidth="1px" Width="100%">
                                            <%--    <RowStyle CssClass="rowstyle" />
                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                            <HeaderStyle CssClass="headerstyle" />
                                                                            <PagerStyle CssClass="pagerstyle" />
                                                                            <PagerTemplate>
                                                                                <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                                                                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="5" />
                                                                                    <asp:ListItem Value="10" />
                                                                                    <asp:ListItem Value="15" />
                                                                                    <asp:ListItem Value="20" />
                                                                                </asp:DropDownList>
                                                                                &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                                                                of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                                                                &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                                                                <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                                                                <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                                                                <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                                                                            </PagerTemplate>--%>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Edit/Print">
                                                    <ItemTemplate>
                                                        &nbsp;&nbsp;
                                                                                          <asp:LinkButton ID="btnEditTReq" CssClass="icon ti-write" runat="server" />&nbsp;&nbsp;
                                                                                        <%--<asp:ImageButton ID="btnEditTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server"  />--%>
                                                        <asp:LinkButton ID="btnPrntTReq" CssClass="icon ti-printer" runat="server" />
                                                        <%-- <asp:ImageButton ID="btnPrntTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                                                                        &nbsp; &nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle Width="65px" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="JobId" HeaderText="CRF#" SortExpression="JobId">
                                                    <ItemStyle HorizontalAlign="Center" Width="50px"  />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="JobName" SortExpression="JobName" HeaderText="Job Name">
                                                    <ItemStyle Width="100px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CustName" HeaderText="Cust Name" SortExpression="CustName">
                                                    <ItemStyle Width="90px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="" SortExpression="" HeaderText="Job Add">
                                                    <ItemStyle Width="90px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="BranchNumber" SortExpression="BranchNumber" HeaderText="Branch #" HeaderStyle-Wrap="false">
                                                    <ItemStyle Width="50px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date">
                                                    <ItemStyle HorizontalAlign="Center" Width="60px" Height="15px" Wrap="False" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="IsTX60Day" SortExpression="IsTX60Day" HeaderText="2nd Mth" HeaderStyle-Wrap="false" >
                                                    <ItemStyle HorizontalAlign="Center" Width="40px" Height="15px"   />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IsTX90Day" SortExpression="IsTX90Day" HeaderText="3rd Mth" HeaderStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Center" Width="40px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AmountOwed"  SortExpression="AmountOwed" HeaderText="Amount Owed"  DataFormatString="{0:C}"  HeaderStyle-Wrap="false">
                                                    <ItemStyle HorizontalAlign="Right" Width="30px" Height="15px" Wrap="False"  />
                                                </asp:BoundField>
                                                <%--<asp:TemplateField SortExpression="AmountOwed" HeaderText="Amount Owed" >
                                                      <ItemStyle HorizontalAlign="Right" Width="30px" Height="15px" Wrap="False"  />
                                                    <ItemTemplate>
                                                        $<asp:Label ID="lblAmountOwed" runat="server" Text='<%#Eval("AmountOwed") %>'></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>--%>
                                                <asp:BoundField DataField="MonthDebtIncurred" SortExpression="MonthDebtIncurred" HeaderText="Month" >
                                                    <ItemStyle HorizontalAlign="Center" Width="40px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText=" Del ">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="btnDeleteTReq" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                        <%--    <asp:ImageButton ID="btnDeleteTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server"  />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="20px" Wrap="False" />

                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                        <asp:Button ID="btnDeleteId" runat="server" Style="display: none;" OnClick="btnDeleteId_Click" />
                        <br />
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12">

                                    <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to List" OnClick="Linkbutton2_Click"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-12">

                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="UserId" runat="server" />
        <asp:HiddenField ID="fromAsgDate" runat="server" />
        <asp:HiddenField ID="thruAsgDate" runat="server" />
        <asp:HiddenField ID="CurrentRowIndex" runat="server" />
        <asp:UpdatePanel ID="updatepanel2" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                    <ProgressTemplate>
                        <div class="TransparentGrayBackground"></div>
                        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                            <div class="PageUpdateProgress">
                                <asp:Image ID="ajaxLoadNotificationImage"
                                    runat="server"
                                    ImageUrl="~/images/ajax-loader.gif"
                                    AlternateText="[image]" />
                                &nbsp;Please Wait...
                            </div>
                        </asp:Panel>
                        <ajaxToolkit:AlwaysVisibleControlExtender
                            ID="AlwaysVisibleControlExtender1"
                            runat="server"
                            TargetControlID="alwaysVisibleAjaxPanel"
                            HorizontalSide="Center"
                            HorizontalOffset="150"
                            VerticalSide="Middle"
                            VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>

    </ContentTemplate>
</asp:UpdatePanel>

