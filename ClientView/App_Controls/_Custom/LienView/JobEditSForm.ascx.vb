Imports CRF.CLIENTVIEW.BLL
Partial Class App_Controls__Custom_LienView_JobEditSForm
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobOtherNoticeLog
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim mylender As HDS.DAL.COMMON.TableView
    Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateFormNotices
    Dim myfields As HDS.DAL.COMMON.TableView

    Dim mysigner As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
    Dim mysigners As HDS.DAL.COMMON.TableView
    Dim myforms As HDS.DAL.COMMON.TableView
    Dim myitem As HDS.DAL.COMMON.TableView
    Dim mytb As New System.Web.UI.WebControls.Table
    Dim mytr As New System.Web.UI.WebControls.TableRow
    Dim ItemId As String
    Dim ChildId As String = -1
    Dim PageName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Me.ItemId = qs.GetParameter("ItemId")
        Me.ChildId = qs.GetParameter("SubId")
        Me.PageName = qs.GetParameter("PageName")
        If qs.HasParameter("SubId") = False Then
            Me.ChildId = -1
        End If
        hdnFormTypeSelectedId.Value = Request.Form(hdnFormTypeSelectedId.UniqueID)
        hdnSignerSelectedValue.Value = Request.Form(hdnSignerSelectedValue.UniqueID)
        myjob = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobById(ItemId)
        mylender = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView("Select * from JobLegalParties Where Id = " & myjob.LenderId)
        myforms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetOtherStateForms(myjob.JobState)
        myforms.Sort = "FriendlyName"
        myfields = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView("Select * from StateFormFields")

        Me.litJobInfo.Text = "SForm for CRF# (" & myjob.Id & ") "
        Me.litJobInfo.Text += " Job# (" & myjob.JobNum.Trim & ") "
        Me.litJobInfo.Text += " JobName (" & myjob.JobName.Trim & ") "

        If Me.Page.IsPostBack = False Then
            Dim li As New ListItem
            myforms.MoveFirst()
            Me.FormId.Items.Clear()
            Do Until myforms.EOF
                li = New ListItem
                myform = myforms.FillEntity(myform)
                li.Value = myform.StateFormNoticeId
                li.Text = myform.FriendlyName
                Me.FormId.Items.Add(li)
                myforms.MoveNext()
            Loop


            Dim VWSIGNERS As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientSigners
            VWSIGNERS.MoveFirst()
            Me.SignerId.Items.Clear()
            Do Until VWSIGNERS.EOF
                li = New ListItem
                Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
                csg = VWSIGNERS.FillEntity(csg)
                li.Value = csg.Id
                li.Text = csg.Signer & "," & csg.SignerTitle
                Me.SignerId.Items.Add(li)
                VWSIGNERS.MoveNext()
            Loop

            If ChildId > 0 Then
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(ChildId, myreq)
                Me.FormId.SelectedValue = myreq.FormId
                Me.SignerId.SelectedValue = myreq.SignerId
                hdnFormTypeSelectedId.Value = myreq.FormId
                hdnSignerSelectedValue.Value = myreq.SignerId

            End If

            Me.FormId.DataBind()
            If (Request.Cookies("SignerSelectedValue") IsNot Nothing) Then

                If (Request.Cookies("SignerSelectedValue")("Data") IsNot Nothing) Then
                    hdnSignerSelectedValue.Value = Request.Cookies("SignerSelectedValue")("Data")
                End If
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsFormTypeDropDown", "BindFormTypeDropdown('" & myjob.JobState & "');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown();", True)
            'Me.SignerId.DataBind()
            BuildEntryTable()

        End If

        If Page.IsPostBack = True Then
            Me.FormId.DataBind()
            Me.SignerId.DataBind()
            BuildEntryTable()
        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSAVE.Click
        Me.Page.Validate()
        If Page.IsValid = False Then
            Exit Sub
        End If

        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobOtherNoticeLog
        ' 11/21/2013
        If ChildId <> -1 Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(ChildId, myitem)
        End If

        HDS.WEBLIB.Common.Mapper.SetEntityValues(Me.panFields, myitem)
        Dim balanceDueText As TextBox = Me.panFields.FindControl("BalanceDueAmt")
        If Not IsNothing(balanceDueText) Then
            myitem.BalanceDueAmt = balanceDueText.Text.Replace("$", String.Empty)
        End If
        
        With myitem
            .JobNoticeLogId = ChildId
            .JobId = ItemId
            '.FormId = Me.FormId.SelectedValue 'Commented by Jaywanti
            .FormId = hdnFormTypeSelectedId.Value
            '.FormCode = Strings.Left(Me.FormId.SelectedItem.Text, 20) 'Commented by Jaywanti
            .FormCode = Strings.Left(hdnFormTypeSelectedText.Value, 20)
            'Commented by Jaywanti
            'If Me.SignerId.SelectedIndex >= 0 Then
            '    .SignerId = Me.SignerId.SelectedValue
            'End If
            If hdnSignerSelectedValue.Value <> "" Then
                .SignerId = hdnSignerSelectedValue.Value
                Response.Cookies("SignerSelectedValue")("Data") = hdnSignerSelectedValue.Value
                Response.Cookies("SignerSelectedValue")("Time") = DateTime.Now.ToString("G")
                Response.Cookies("SignerSelectedValue").Expires = DateTime.Now.AddYears(1)
            End If
            .RequestedBy = Left(Me.UserInfo.UserInfo.LoginCode, 10)
            .RequestedByUserId = Me.CurrentUser.Id
        End With

        If myitem.JobNoticeLogId > 0 Then
            myitem.DatePrinted = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myitem)
        Else
            myitem.DatePrinted = Today
            myitem.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myitem)
        End If
        Dim s As String
        If (Me.PageName = "JobView") Then
            s = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&view=" & "SFORM" & "&formid=" & myitem.JobNoticeLogId
        Else
            s = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&view=" & "SFORM" & "&formid=" & myitem.JobNoticeLogId
        End If
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        'Response.Redirect(Me.Page.ResolveUrl(s), True)   'Commented by Sudhakar

        'Print PDF On New Tab instead Own Tab

        'btnPrntSForm = TryCast(sender, LinkButton)
        Session("currenttab") = "SFORM"
        'SetCurrentTab()
        Dim sreport As String = LienView.Provider.PrintStateForm(myitem.JobNoticeLogId)
        'Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            'Me.btnDownLoad.Visible = True
            Dim s1 As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            'Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s1), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    Dim URL As String = "MainDefault.aspx?lienview.jobeditsform&itemid=" & myjob.Id & "&subid=" & myitem.JobNoticeLogId
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + URL + "'", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
            'Else
            'Me.plcReportViewer.Controls.Clear()
            'Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            'Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub
    Private Sub BuildEntryTable()
        ''If Me.FormId.SelectedValue.ToString.Length < 1 Then Exit Sub

        If hdnFormTypeSelectedId.Value = "" Then Exit Sub

        Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateFormNotices
        'Commented by Jaywanti
        'myforms.FillByKeyValue(CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateFormNotices.ColumnNames.StateFormNoticeId, Me.FormId.SelectedValue, myform)
        'myforms.RowFilter = "StateFormNoticeId=" & Me.FormId.SelectedValue

        myforms.FillByKeyValue(CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateFormNotices.ColumnNames.StateFormNoticeId, hdnFormTypeSelectedId.Value, myform)
        myforms.RowFilter = "StateFormNoticeId=" & hdnFormTypeSelectedId.Value

        mytb = New System.Web.UI.WebControls.Table
        mytb.Style.Add("width", "90%")
        Me.panFields.Controls.Clear()
        If myforms.Count = 1 Then
            myforms.MoveFirst()
            Dim s As String
            Dim ss As String() = Strings.Split(myform.TableColumnNames, ",")
            For Each s In ss
                Dim avalue As Object = myforms.RowItem(s)
                AddRow(s, avalue)
            Next
        End If

        Me.panFields.Controls.Add(mytb)

        If myreq.JobNoticeLogId < 1 Then
            myreq.EndDate = IIf(myjob.EndDate > "01/01/1980", Strings.FormatDateTime(myjob.EndDate, DateFormat.ShortDate), Nothing)
            myreq.JobCounty = myjob.JobCounty
            myreq.FileDate = IIf(myjob.BondDate > "01/01/1980", Strings.FormatDateTime(myjob.BondDate, DateFormat.ShortDate), Nothing)
            myreq.NOISendDate = IIf(myjob.NOIDate > "01/01/1980", Strings.FormatDateTime(myjob.NOIDate, DateFormat.ShortDate), Nothing)
            myreq.DateNoticeSent = IIf(myjob.NoticeSent > "01/01/1980", Strings.FormatDateTime(myjob.NoticeSent, DateFormat.ShortDate), Nothing)
            myreq.RecorderNum = myjob.RecorderNum
            ' 9/25/2014
            myreq.APNNum = myjob.APNNum
            myreq.ThroughDate = IIf(myreq.ThroughDate > "01/01/1980", myreq.ThroughDate, Nothing)
            If mylender.Count > 0 Then
                myreq.BondNumber = mylender.RowItem("RefNum")
            End If
        End If

        HDS.WEBLIB.Common.Mapper.GetEntityValues(Me.panFields, myreq)

    End Sub
    Private Sub GetRow(ByVal acolumnname As String, ByVal avalue As Object)

    End Sub
    Protected Sub FormId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormId.SelectedIndexChanged
        BuildEntryTable()

    End Sub
    Private Sub AddRow(ByVal acolumnname As String, ByVal avalue As Object)


        If UCase(acolumnname) = "STATEFORMNOTICEID" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "STATECODE" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "REPORTNAME" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "FRIENDLYNAME" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "FORMTYPE" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "CLIENTVIEW" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "CRFVIEW" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "ALTCLAIMANTCONTACT" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "ALTCLAIMANTADD1" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "ALTCLAIMANTADD2" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "ALTCLAIMANTCITY" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "ALTCLAIMANTSTATE" Then
            Exit Sub
        End If
        If UCase(acolumnname) = "ALTCLAIMANTZIP" Then
            Exit Sub
        End If
        If avalue = False Then
            Exit Sub
        End If

        'myfields.RowFilter = "FieldName Like '" & Strings.Left(acolumnname, 6) & "%'"
        myfields.RowFilter = "FieldName Like '" & acolumnname & "'"

        Dim mytr As New TableRow
        Dim mytd1 As New TableCell
        mytb.CssClass = "form-horizontal"
        mytr.CssClass = "form-group"
        mytd1.CssClass = "col-md-3 control-label"
        mytd1.Style.Add("text-align", "right")
        'mytd1.Width = New System.Web.UI.WebControls.Unit(160)
        mytd1.Text = acolumnname + ":"
        If myfields.Count = 1 Then
            mytd1.Text = myfields.RowItem("FriendlyName")
        End If

        Dim mytd2 As New TableCell
        mytd2.CssClass = "row-data"

        Dim tb As New HDS.WEBLIB.Controls.DataEntryBox
        tb.ID = acolumnname
        tb.DataType = HDS.WEBLIB.Controls.DataTypes.Any
        tb.CssClass = "form-control col-md-2"
        'mytd2.Width = New System.Web.UI.WebControls.Unit(250)
        mytd2.CssClass = "col-md-4"
        mytd2.Controls.Add(tb)

        mytr.Controls.Add(mytd1)
        mytr.Controls.Add(mytd2)
        mytb.Controls.Add(mytr)

    End Sub

    Private Function setdate(ByVal adate As String) As String
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return ""
        End If
        If s = Date.MinValue Then
            Return ""
        End If
        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Private Function getdate(ByVal adate As String) As Date
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return Date.MinValue
        End If

        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Session("exitviewer") = "1"
        Session("currenttab") = "SFORM"
        Dim s As String
        If Me.PageName = "JobView" Then
            s = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&view=" & "SFORM"
            Response.Redirect(Me.Page.ResolveUrl(s), True)
        Else
            s = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myjob.Id & "&view=" & "SFORM"
        End If
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub AddSigner1_ItemSaved() Handles AddSigner1.ItemSaved
        ' Me.MyPage.RedirectCurrentPage()
        Dim li As New ListItem

        Dim VWSIGNERS As HDS.DAL.COMMON.TableView = LienView.Provider.GetClientSigners
        VWSIGNERS.MoveFirst()
        Me.SignerId.Items.Clear()
        Do Until VWSIGNERS.EOF
            li = New ListItem
            Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
            csg = VWSIGNERS.FillEntity(csg)
            li.Value = csg.Id
            li.Text = csg.Signer & "," & csg.SignerTitle
            Me.SignerId.Items.Add(li)
            VWSIGNERS.MoveNext()
        Loop
        HttpContext.Current.Session("SignerList") = Nothing
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsFormsTypeDropDown", "BindFormTypeDropdown('" & myjob.JobState & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignersDropDown", "BindSignerDropdown();", True)
        BuildEntryTable()
    End Sub

    Protected Sub btnForm_Click(sender As Object, e As EventArgs)
        BuildEntryTable()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsFormTypeDropDown", "BindFormTypeDropdown('" & myjob.JobState & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown();", True)
    End Sub
End Class
