<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobEditTReq.ascx.vb" Inherits="App_Controls__Custom_LienView_JobEditTReq" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>

<script type="text/javascript">
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Jobs', 'View Jobs');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubJobList');
        //$('#liJobs').addClass('site-menu-item has-sub active open');
        //$('#lisubJobList').addClass('site-menu-item active');
        return false;

    }
   
</script>

                                    <div id="modcontainer" style="MARGIN: 10px; WIDTH: 100%;">
                                        <h1 >
                                            <asp:Literal ID="litJobInfo" runat="server"></asp:Literal></h1>
                                        <div class="body">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-md-2  col-sm-2 col-xs-2">
                                                        <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to Job Info" CausesValidation="False" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Notice Type:</label>
                                                    <div class="col-md-7 col-sm-7 col-xs-7" style="text-align:left;">
                                                         <div class="example-wrap" style="margin-bottom: 0;">
                                                                                <div class="radio-custom radio-default" style="display:flex;">
                                                        <asp:RadioButton ID="IsTX60Day" runat="server" Text=" 2nd Month" GroupName="NoticeType" 
                                                            CssClass="row-label col-md-3 col-sm-3 col-xs-3" Checked="True" style="text-align:left;"></asp:RadioButton>
                                                        <asp:RadioButton ID="IsTX90Day" runat="server" Text=" 3rd Month" GroupName="NoticeType" 
                                                            CssClass="row-label col-md-3 col-sm-3 col-xs-3" style="text-align:left;"></asp:RadioButton></div></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Amount Owed:</label>
                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                        <cc1:DataEntryBox ID="AmountOwed" runat="server" DataType="Money" CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Month/Year Work Performed:</label>
                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                        <cc1:DataEntryBox ID="MonthDebtIncurred" runat="server"  DataType="Any" MaxLength="50" CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-12 col-sm-12 col-xs-12  control-label align-lable" style="text-align:left;">
                                                        <h3>Enter Account Statement Info:</h3>
                                                    </label>
                                                    <div class="col-md-11">
                                                        <cc1:DataEntryBox ID="StmtofAccts" runat="server" DataType="Any" Height="150px" Tag="StmtofAccts"
                                                            TextMode="MultiLine"  CssClass="form-control"></cc1:DataEntryBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">

                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                        <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                                        <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="footer">
                                            <asp:UpdatePanel ID="updPan1" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="btnSAVE"
                                                        CssClass="btn btn-primary"
                                                        runat="server"
                                                        Text="Submit" CausesValidation="False" />&nbsp;
                                                    <br />
                                                    <br />

                                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                        <ProgressTemplate>
                                                            <div class="TransparentGrayBackground"></div>
                                                            <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                                                <div class="PageUpdateProgress">
                                                                    <asp:Image ID="ajaxLoadNotificationImage"
                                                                        runat="server"
                                                                        ImageUrl="~/images/ajax-loader.gif"
                                                                        AlternateText="[image]" />
                                                                    &nbsp;Please Wait...
                                                                </div>
                                                            </asp:Panel>
                                                            <ajaxToolkit:AlwaysVisibleControlExtender
                                                                ID="AlwaysVisibleControlExtender1"
                                                                runat="server"
                                                                TargetControlID="alwaysVisibleAjaxPanel"
                                                                HorizontalSide="Center"
                                                                HorizontalOffset="150"
                                                                VerticalSide="Middle"
                                                                VerticalOffset="0">
                                                            </ajaxToolkit:AlwaysVisibleControlExtender>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>

