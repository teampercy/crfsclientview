<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TrackAndConfirm.aspx.vb" Inherits="App_Controls__Custom_LienView_TrackAndConfirm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>http://trkcnfrm1.smi.usps.com/-USPS-Track & Confirm</title>
</head>
<script language="JavaScript" type="text/javascript">
		function nospc(x, y)
		{
		x = replc(x, ' ', ' ');
		if (0 <= x.indexOf('\t'))
		{
		x = replc(x, '\t', ' ');
		}
		y.value = x;
		}
		function replc(x, toreplc, wreplc)
		{
		while (x.charAt(0) == ' ')
		{
		x = x.substring(1, x.length);
		}
		while (x.charAt(x.length-1) == ' ')
		{
		x = x.substring(0, x.length-1);
		}
		return x;
		}
 
		function getTrackNum_validator()
		{
		nospc(document.getTrackNum.strOrigTrackNum.value, document.getTrackNum.strOrigTrackNum);
		if (document.getTrackNum.strOrigTrackNum.value.length == 0)
		{
		alert("You must enter a tracking number. Please try again.");
		document.getTrackNum.strOrigTrackNum.focus();
		return false;
		}
		if (document.getTrackNum.strOrigTrackNum.value.search("'") != -1)
		{
		alert("Invalid tracking number entered. Please try again.");
		document.getTrackNum.strOrigTrackNum.focus();
		return false;
		}
		return true;
		}
 
 
function openBrWindow(theURL,winName,features) { //v2.0
  
  window.open(theURL,winName,features);
}

function formunload()
{

document.forms["form1"].submit();
return false;
}

</script>


<body  onload="javascript:return formunload();" >
    <div style="display:none" >
    <form id="form1" runat="server" action="http://trkcnfrm1.smi.usps.com/PTSInternetWeb/InterLabelInquiry.do" method="POST" name="getTrackNum" onsubmit="return getTrackNum_validator(this)" style="width:100px;height:50px"  >
    
     <asp:TextBox ID="strOrigTrackNum" MaxLength="42"   runat="server" Width="240px" TabIndex="2" ></asp:TextBox>  
     <img height="1" width="4" border="0" alt="" src="../../../App_Themes/VBJUICE/img/button_go.gif" />
     <input type="image" tabindex="2" src="../../../App_Themes/VBJUICE/img/button_go.gif"   alt="Go to Track & Confirm" id="Go" name="Go to Track & Confirm" value="Go" />

    </form>
    </div>
</body>
</html>
