Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Imports System.Configuration
Imports System.Net.Mail

Partial Class App_Controls__Custom_LienView_FileUpload
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Protected Sub btnFileUpLoadOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadOk.Click
        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showLodingImage", "showLodingImage();", True)
            Dim str As String
            Dim jobid As String
            Dim batchjobid As String
            Dim usercode As String
            Dim fileName As String
            Dim userid As Integer
            Dim Todaydate As Date
            'markparimal-uploadfile:check whether document type is selected or not
            'If ddldoctype.SelectedIndex = 0 Then
            '    lblmsg.Text = " Select Document Type"
            '    Return
            'Else
            '    lblmsg.Text = ""
            'End If

            fileName = ""

            If hdnDocType.Value = "--Select--" Then
                lblmsg.Text = " Select Document Type"
                Return
            Else
                lblmsg.Text = ""
            End If
            'Comment by jaywanti on 10-09-2016
            'Dim UploadedFile As HttpPostedFile = Request.Files("fileDragnDrop")
            'str = UploadedFile.FileName
            'Dim fileData As Byte() = New Byte(UploadedFile.InputStream.Length - 1) {}
            'UploadedFile.InputStream.Read(fileData, 0, fileData.Length)
            'fileName = System.IO.Path.GetFileName(UploadedFile.FileName)

            Dim uploadFiles As List(Of UploadFile) ' = New List(Of UploadFile)()
            uploadFiles = Context.Session("UploadFiles")
            For Each files In uploadFiles
                fileName = ""
                Dim fileData As Byte()

                'If Not Session("UploadedFileName") Is Nothing Then
                If Not files.UploadedFileName Is Nothing Then
                    fileName = files.UploadedFileName
                End If
                'If Not Session("UploadedFileDataBytes") Is Nothing Then
                '    fileData = Session("UploadedFileDataBytes")
                If Not files.UploadedFileDataBytes Is Nothing Then
                    fileData = files.UploadedFileDataBytes
                End If
                usercode = CurrentUser.UserName.ToString
                userid = CurrentUser.Id

                Todaydate = txtdate.Text
                If Session("bitjobid") = 0 Then
                    jobid = 0
                    batchjobid = Session("BatchJobId")
                Else
                    jobid = Session("jobid")
                    batchjobid = 0
                End If

                'CRFS.ClientView.CrfsDll.InsertJobAttachments(jobid, userid, usercode, Calendar2.Value, fileName, ddldoctype.SelectedValue, Txtmiscinfo.Text, fileData, batchjobid)
                'Commented by Jaywanti 
                'CRFS.ClientView.CrfsBll.InsertJobAttachments(jobid, userid, usercode, Todaydate, fileName, ddldoctype.SelectedValue, Txtmiscinfo.Text, fileData, batchjobid)
                Dim DescriptionWithFileName = "[Filename: " + fileName + "]  " + Txtmiscinfo.Text
                'CRFS.ClientView.CrfsBll.InsertJobAttachments(jobid, userid, usercode, Todaydate, fileName, hdnDocType.Value, Txtmiscinfo.Text, fileData, batchjobid)
                CRFS.ClientView.CrfsBll.InsertJobAttachments(jobid, userid, usercode, Todaydate, fileName, hdnDocType.Value, DescriptionWithFileName, fileData, batchjobid)
            Next
            HttpContext.Current.Session("UploadFiles") = Nothing
            If Session("bitjobid") = 0 Then

                lblmsg.Text = "File Uploaded For Temp Job#" + " " + batchjobid
            Else
                lblmsg.Text = "File Uploaded For Job#" + " " + jobid
            End If

            If Session("bitjobid") = "1" Then
                Dim myitem As New TABLES.Job
                ProviderBase.DAL.Read(jobid, myitem)
                Dim ssubject = "Document Sent on Lien# " & jobid
                Dim MYSQL As String = "Select Email from JobDesks "
                MYSQL += " Where DeskId = " & myitem.DeskNumId
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                If MYDT.Tables.Count > 0 Then
                    'myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
                    Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                    EmailToUser(myview.RowItem("Email"), ssubject)
                End If
            End If
        Catch ex As Exception
            lblmsg.Text = "Error: - " + ex.Message
        End Try
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindDocumentUploadedFile", "BindFileUoload();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideLodingImage", "hideLodingImage();", True)
    End Sub
    Public Shared Function GetSettings() As TABLES.Report_Settings
        Dim myitem As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, myitem)

        Return myitem

    End Function
    Public Shared Sub EmailToUser(ByVal email As String, ByVal subject As String)

        Dim MYSITE As TABLES.Report_Settings = GetSettings()

        Dim msg As New MailMessage

        msg.From = New MailAddress("CRFIT@crfsolutions.com", "ClientView")
        msg.Subject = subject
        msg.To.Add(email)
        msg.Subject = subject
        msg.IsBodyHtml = True
        'msg.Body = body

        Try
            With MYSITE
                Dim _smtp As New SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
            End With
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            MsgBox("error on email:" & vbCrLf & mymsg)

        End Try

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.Page.IsPostBack = False Then
            txtdate.Text = DateTime.Today.ToString("MM/dd/yyyy")
        Else
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If

    End Sub
End Class
