<%@ Control Language="VB" AutoEventWireup="false" CodeFile="GCSearch.ascx.vb" Inherits="ClientViewLiens_GCSearch" %>
<asp:linkbutton ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Find"   style="display:none;"/>

<%--<button type="button" class="btn btn-primary" onclick="OpenGCModal();" value="Find" style="font-size:small">Find</button>--%>
<a onclick="OpenGCModal()" id="btnGCFindSearch" runat="server" class="btn_OverWrite btn-primary" style="font-size:small;">Find </a>
<asp:Panel ID="panCustNameSuggest" runat="server" Style="display: none"
    Width="460px" Font-Bold="True">
<div id="modcontainer" style="MARGIN: 0px; WIDTH: 460px">
<h1 class="panelheader" >General Contractor Search</h1>
<div class="body">
<asp:linkbutton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Exit" CausesValidation="False" />
<br />
<br />
<hr />
<TABLE align="center" cellpadding="0" cellspacing="0" border="0" width="460">
<TR>
<TD style="vertical-align :top">
<br />
    Enter GC Name:
<asp:TextBox ID="TextBox1" CssClass="textbox" runat="server" Width="250px"></asp:TextBox>&nbsp;&nbsp;
<asp:linkbutton ID="btnSearchIt" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="btnSearchIt_Click" />&nbsp;
</TD>
</TABLE>
<div align="center">
<asp:ListBox ID="ListBox1" Visible ="false" runat="server" Width="448px" AutoPostBack="True"></asp:ListBox>
</div>
<asp:HiddenField ID="myValue" runat="server" />
<asp:HiddenField ID="myClientId" runat="server" /><asp:HiddenField ID="mylist" runat="server" />
</div>
</div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="CancelButton"  OkControlID="" DropShadow="true"  
    PopupControlID="panCustNameSuggest" TargetControlID="btnSearch">
</ajaxToolkit:ModalPopupExtender>
<script type="text/javascript">
    function callsearchGC() {
        document.getElementById('<%=btnSearchIt.ClientID%>').click();
    }
</script>