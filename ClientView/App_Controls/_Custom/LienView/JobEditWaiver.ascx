<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobEditWaiver.ascx.vb" Inherits="App_Controls__Custom_LienView_JobEditWaiver" %>
<%@ Register Src="AddSigner.ascx" TagName="AddSigner" TagPrefix="uc3" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>

<%@ Import Namespace="System.Net.Mail" %>
<style type="text/css">
    .scrollable-menu {
        height: auto;
        max-height: 180px;
        overflow-x: hidden;
    }

        .scrollable-menu::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 4px;
        }

        .scrollable-menu::-webkit-scrollbar-thumb {
            border-radius: 3px;
            background-color: lightgray;
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);
        }

    #divAnimation {
        min-width: 1000px;
    }
</style>
<script type="text/javascript">


    var array_store;
    window.onload = function () {
        HiddenMailTo = document.getElementById("HiddenMailTo");
    }
    var currURL;

    function DisplayWaiverPopUp() {
        // alert('hi');

        currURL = window.location.href;
        $("#ModalWaiverSubmit").modal('show');
    }
    function HideWaiverPopUp() {
        // alert('hi');
        $("#ModalWaiverSubmit").modal('hide');
    }
    function DisplayEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('show');
    }
    function HideEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('hide');
        $('div').removeClass('modal-backdrop fade in');
        //fadeout();
    }
    function fadeout() {
        debugger;
        //alert(currURL);
        location.href = currURL;
        $('div').removeClass('modal-backdrop fade in');
        return false();
    }

    //function AlertSuccessFunc() {
    //    swal({
    //        title: "Successfully Submitted!",
    //        text: "Waiver data has been saved successfully! \n What would you like to do now?",
    //        confirmButtonClass: "btn btn-success",
    //        cancelButtonText: "Print",
    //        confirmButtonText: 'Email',
    //        type: "success",
    //        showDoneButton: true,
    //        closeOnDone: true

    //    });
    //}

    function EmailSendPopup() {
        debugger;
        swal({
            title: "Successfully Sent!",
            text: "Your Waiver Has Been Emailed.",
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false

        });

    }

    function Cancelpopup() {
        location.href = currURL;
        return false;
    }
    function CloseEmailLoadPage() {
        document.getElementById('<%=hdnbtnclose.ClientID%>').click();
    }

    //var JobAddress = document.getElementById('<%=txtCustEmail.ClientID%>').value;
   <%-- function email() {
       
        if (JobAddress) {
            document.getElementById('<%=txtSubject.ClientID%>').value = "New Waiver Document for " + JobAddress;
        }
        else {
            document.getElementById('<%=txtSubject.ClientID%>').value = "New Waiver Document";
        }
        document.getElementById('<%=txtBody.ClientID%>').value = "";
        $("#divModalEmail").modal('show');
        //document.getElementById('<%=btnEmailWaiver.ClientID%>').click();
        return false;
      }

      function print() {
        document.getElementById('<%=btnPrintWaiver.ClientID%>').click();
        return false;
    }--%>

    function SendMail() {
        if (document.getElementById('<%=txtToList.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the 'To' EmailId.";
            return false;
        }
        if (document.getElementById('<%=txtSubject.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the Subject.";
            return false;
        }
        document.getElementById('<%=btnEmailsend.ClientID%>').click();

        HideEmailPopUp();

        return false;
    }

    function getQueryStringValue(key) {
        return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    }


    <%--    function checkIsFinalWaiver() {
        debugger;
        if (getQueryStringValue("subid") == "-1") {
            var WaiverType = $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val();
            // var WaiverType = e.options[e.selectedIndex].text;
          
            debugger;
        
            if (WaiverType.indexOf("Final Payment") != -1) {
                var retVal = confirm("You are about to create a Final Waiver for this job.Please Confirm this is what you intended.");
                if (retVal == true) {

                    return true;
                }
                else {

                    return false;
                }
            }
        }
        else
        { return true; }
    }--%>
    function checkIsFinalWaiver(evt) {
        debugger;

        //if (getQueryStringValue("subid") == "-1") {
        var WaiverType = $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val();
        // var WaiverType = e.options[e.selectedIndex].text;

        debugger;

        if (WaiverType.indexOf("Final Payment") != -1) {
            evt.preventDefault();
            swal({
                title: "",
                text: "You are about to create a Final Waiver for this job.Please Confirm this is what you intended.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: true
            },
        function (isConfirm) {
            if (isConfirm) {
                // $.showprogress("Account Suspending.Please Wait...");
                window.__doPostBack("<%= btnSAVE.UniqueID%>", "");
            } else {
                evt.preventDefault();
            }
        });
    }

        //}
        //else { return true; }
}


</script>
<script type="text/javascript">
    function MaintainMenuOpen() {
        //debugger;
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            if (pair[0] == "PageName") {
                SetHeaderBreadCrumb('JobView', 'View All Jobs', 'View Jobs');
                MainMenuToggle('liViewAllJobs');
                $('#liJobs').removeClass('active open');
                $('#lisubJobList').removeClass('active');
                // SubMenuToggle('lisubJobList');
            }
            else {
                SetHeaderBreadCrumb('LienView', 'Jobs', 'View Jobs');
                MainMenuToggle('liJobs');
                SubMenuToggle('lisubJobList');
                $('#liViewAllJobs').removeClass('active open');
                //SubMenuToggle('lisubJobList');
            }
        }
        return false;

    }

    function BindWaiverTypeDropdown(JobState) {
        PageMethods.GetWaiverTypeList(JobState, LoadWaiverTypeList);
    }
    function LoadWaiverTypeList(result) {
        // alert('hii12');

        $('#ulDropDownWaiverType li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {

                if ($('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val() == "" || $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val(result[i].Value);
                    $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val(result[i].Text);
                }
                $('#ulDropDownWaiverType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liWaiverType_' + result[i].Value + '" onclick="setSelectedWaiverTypeId(' + result[i].Value + ',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');

                //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
            }
        }
        var TextData = $('#liWaiverType_' + $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val()).text();


        $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val(TextData);   
       <%-- alert($('#liWaiverType_' + $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val()).text());--%>
        $('#btnDropWaiverType').html(TextData + "<span class='caret' ></span>");
    }
    function setSelectedWaiverTypeId(id, TextData) {
        $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val(id);
        $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val(TextData);
        document.getElementById('<%=btnForm.ClientID%>').click();
        //alert(TextData);
        $('#btnDropWaiverType').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    function BindSignerDropdown(Signerid) {
        //  alert('hi');
        //  alert(Signerid);
        $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(Signerid)

        PageMethods.GetSignerList(LoadSignerList);
    }
    function LoadSignerList(result) {
        // alert('hii12');

        $('#ulDropDownSigner li').remove();

        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnSignerSelectedValue.ClientID%>').val() == "" || $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(result[i].Value);
                }
                var ResultReplace = ReplaceSpecChar(result[i].Text);
                $('#ulDropDownSigner').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liSign_' + result[i].Value + '" onclick="setSelectedSignerId(' + result[i].Value + ',\'' + ResultReplace + '\')">' + result[i].Text + '</a></li>');

                //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
            }
        }
        $('#btnDropSigner').html($('#liSign_' + $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedSignerId(id, TextData) {
        var ResultReplace = TextData.replace("`", "'");

        $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(id);
        //alert(TextData);
        $('#btnDropSigner').html(ResultReplace + "<span class='caret' ></span>");
        return false;
    }
    function SetWaiverTypeId(Id) {
        $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val(Id);
        var TextData = $('#liWaiverType_' + $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val()).text();
        $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val(TextData);               
      <%--  $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val($('#liWaiverType_' + $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val()).text());     --%>

        $('#btnDropWaiverType').html(TextData + "<span class='caret' ></span>");
        return false;
    }



    //----------------------------Bind drpMailCheck---------------------------------------------


    function BindMailCheck(jobid) {


        PageMethods.GetMailCheckList(jobid, LoadMailCheck);
    }

    function LoadMailCheck(result) {
        // alert('hii12');
        debugger;
        $('#ulMailCheck li').remove();

        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnDropMailCheckId.ClientID%>').val() == "" || $('#' + '<%=hdnDropMailCheckId.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnDropMailCheckId.ClientID%>').val(result[i].Value);

                    $('#' + '<%=hdnDropMailCheckIdText.ClientID%>').val(result[i].Text);
                }
                var ResultReplace = ReplaceSpecChar(result[i].Text);
                $('#ulMailCheck').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liMailCheck_' + result[i].Value + '" onclick="setSelectedMailCheck(\'' + result[i].Value + '\',\'' + ResultReplace + '\')">' + result[i].Text + '</a></li>');

                // alert('hii' + result[i].Value);
            }
        }
        $('#btnDropMailCheck').html($('#liMailCheck_' + $('#' + '<%=hdnDropMailCheckId.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedMailCheck(id, TextData) {
        debugger;

        // alert('hii ' + id+ " text" + TextData);


        var ResultReplace = TextData.replace("`", "'");

        $('#' + '<%=hdnDropMailCheckId.ClientID%>').val(id);
         $('#' + '<%= hdnDropMailCheckIdText.ClientID%>').val(ResultReplace);

         $('#btnDropMailCheck').html(ResultReplace + "<span class='caret' ></span>");
         return false;



     }





 <%--   function BindMailCheck() {
        debugger;
        //Added by Priyanka on 27-4-17
        $('#ulMailCheck li').remove();

        $('#ulMailCheck').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liCustomer" onclick="setSelectedMailCheck(1,\'Customer\')">Customer</a></li>');
        $('#ulMailCheck').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liGenCont" onclick="setSelectedMailCheck(2,\'Gen Cont\')">Gen Cont</a></li>');
        $('#ulMailCheck').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liOwner" onclick="setSelectedMailCheck(3,\'Owner\')">Owner</a></li>');
        $('#ulMailCheck').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liLender" onclick="setSelectedMailCheck(4,\'Lender\')">Lender</a></li>');
        $('#ulMailCheck').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liOther" onclick="setSelectedMailCheck(5,\'Other\')">Other</a></li>');

        if ($('#' + '<%=hdnDropMailCheckId.ClientID%>').val() == "1") {
            $('#btnDropMailCheck').html($('#liCustomer').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnDropMailCheckId.ClientID%>').val() == "2") {
            $('#btnDropMailCheck').html($('#liGenCont').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnDropMailCheckId.ClientID%>').val() == "3") {
            $('#btnDropMailCheck').html($('#liOwner').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnDropMailCheckId.ClientID%>').val() == "4") {
            $('#btnDropMailCheck').html($('#liLender').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnDropMailCheckId.ClientID%>').val() == "5") {
            $('#btnDropMailCheck').html($('#liOther').text() + "<span class='caret' ></span>");
        }


    return false;
}
    function setSelectedMailCheck(id, TextData) {
        if (id = "") {
            id = 1
        }
    $('#' + '<%=hdnDropMailCheckId.ClientID%>').val(id);
    $('#btnDropMailCheck').html(TextData + "<span class='caret' ></span>");
    return false;
}--%>
    //------------------------------End Bind MailCheck-----------------------------------------


    //----------------------------Bind drpMailTo---------------------------------------------


    function BindMailTo(jobid) {
        debugger;

        PageMethods.GetMailToList(jobid, LoadMailTo);
    }

    function LoadMailTo(result) {
        // alert('hii12');
         debugger;
         $('#ulMailTo li').remove();

        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnDropMailToSelectedId.ClientID%>').val() == "" || $('#' + '<%=hdnDropMailToSelectedId.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnDropMailToSelectedId.ClientID%>').val(result[i].Value);
                }
                
                var ResultReplace = ReplaceSpecChar(result[i].Text);
                //alert("final" + ResultReplace);
                $('#ulMailTo').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liMailTo_' + result[i].Value + '" onclick="setSelectedMailTo(\'' + result[i].Value + '\',\'' + ResultReplace + '\')">' + result[i].Text + '</a></li>');

               // alert('hii' + result[i].Value);
            }
        }
         $('#btnDropMailTo').html($('#liMailTo_' + $('#' + '<%=hdnDropMailToSelectedId.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedMailTo(id, TextData) {
        debugger;

        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnDropMailToSelectedId.ClientID%>').val(id);
        var myHidden = document.getElementById('<%= hdnDropMailToSelectedId.ClientID %>').value
      
        $('#btnDropMailTo').html(ResultReplace + "<span class='caret' ></span>");
        return false;
    }

    function ReplaceSpecChar(Textdata)
    {
        //alert(Textdata);
        var result = Textdata;
        var ResultReplace = result.replace("'", "`");
        //alert("ResultReplace" + ResultReplace);
        return ResultReplace;
    }



//------------------------------End Bind MailCheck-----------------------------------------
    function PaymentAmtlostfocus() {
        debugger;
        var MoneyCntrl = $('#<%=PaymentAmt.ClientID%>');
        if (MoneyCntrl.val().length > 0 && MoneyCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(MoneyCntrl.val().replace(",", ""))
            MoneyCntrl.val('$' + value.toFixed(2));
        }
    }
    function DisputedAmtlostfocus() {
        debugger;
        var MoneyCntrl = $('#<%=DisputedAmt.ClientID%>');
        if (MoneyCntrl.val().length > 0 && MoneyCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(MoneyCntrl.val().replace(",", ""))
            MoneyCntrl.val('$' + value.toFixed(2));
        }

    }
    function SetSigner(Id) {
        //alert('hi');
        /// alert(Id);
        //alert("signerid"+ID);
        // alert($('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(Id));
        $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(Id);
        $('#btnDropSigner').html($('#liSign_' + $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
        return false;
    }

    function fnOtherCheckIssuer()
    {
        debugger;
        //alert($('#'+'<%=OtherChkIssuerName.ClientID %>').val().length);
         if ($('#' + '<%=OtherChkIssuerName.ClientID %>').val().length > 1) {
             document.getElementById("<%=chkOtherIssuer.ClientID %>").checked = true;

        }
        else {
            document.getElementById("<%=chkOtherIssuer.ClientID %>").checked = false;
        }
        return lostfocus(<%=OtherChkIssuerName.ClientID %>);
     }

    function myNotary(id) {
        debugger;


        if (id == 1) {

            //alert("withoutca= " + document.getElementById('<%=NotarywithoutCA.ClientID%>').checked + " withca=" + document.getElementById('<%=NotarywithCA.ClientID%>').checked);
             if (document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithCA.ClientID%>').checked == false) {

                 document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
                 document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
             }
             else if (document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithCA.ClientID%>').checked == true) {

                 document.getElementById('<%=NotarywithCA.ClientID%>').checked = true;
               document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
              }
             else {

               document.getElementById('<%=NotarywithCA.ClientID%>').checked = true;
               document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
           }
   }
   else {
       if (document.getElementById('<%=NotarywithCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false) {

                 document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
            document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
        }
        else if (document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithCA.ClientID%>').checked == true) {

            document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
             document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = true;
         }
         else {

             document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
             document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = true;
         }

 }
}

</script>

<div id="modcontainer" style="margin: 10px; width: 100%; min-width:900px">
    <h1>
        <asp:Literal ID="litJobInfo" runat="server"></asp:Literal></h1>
    <div class="body">
        <div class="form-horizontal">
            <div class="form-group">
                <div  style="padding-left:20px;text-align: left;">
                    <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to Waivers" CausesValidation="False" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Waiver Type:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <div class="btn-group" style="text-align: left;" align="left">
                        <button type="button" class="btn btn-default dropdown-toggle col-md-12" style="min-width: 200px;" align="left"
                            data-toggle="dropdown" aria-expanded="false" id="btnDropWaiverType">
                            --Select Waiver Type--
                     
                                                                                    <span class="caret"></span>
                        </button>
                                                                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownWaiverType" style="overflow-y:auto;height:180px;">
                            <%--  <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>
                                                                                    <li class="divider" role="presentation"></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Separated link</a></li>--%>
                        </ul>
                    </div>
                    <asp:HiddenField ID="hdnWaiverTypeSelectedId" runat="server" Value="" />
                    <asp:HiddenField ID="hdnWaiverTypeSelectedText" runat="server" Value="" />
                                                        <cc1:ExtendedDropDown ID="FormId" CssClass="dropdown" runat="server" Width="300px" AutoPostBack="True" style="display:none;"></cc1:ExtendedDropDown>
                                                          <asp:Button ID="btnForm" runat ="server" style="display:none;" OnClick="btnForm_Click"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Payment Amt:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <cc1:DataEntryBox ID="PaymentAmt"  runat="server"   CssClass="form-control col-md-2" DataType="Money"
                        Width="140px" BlankOnZeroes="False">$0.00</cc1:DataEntryBox>
                                                        <div class="checkbox-custom checkbox-default col-md-4 col-xs-4 col-sm-4" style="margin-left:10px;">
                        <asp:CheckBox ID="IsPaidInFull"
                            runat="server" Text=" Is Paid to Date" Width="144px" />
                    </div>

                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Disputed Amt:</label>
                <div class="col-md-7 col-sm-7 col-xs-7 col-xs-7" style="text-align: left;">
                    <cc1:DataEntryBox ID="DisputedAmt" runat="server" CssClass="form-control col-md-2" DataType="Money"
                        Width="140px" BlankOnZeroes="False">$0.00</cc1:DataEntryBox>
                                                        <div class="checkbox-custom checkbox-default col-md-4 col-sm-4 col-xs-4" style="margin-left:10px;">
                        <asp:CheckBox ID="IsMarkPIF"
                            runat="server" Text=" Mark Job Paid In Full" Width="184px" />
                    </div>

                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Mail Waiver To:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <div class="btn-group" style="text-align: left;" align="left">
                        <button type="button" class="btn btn-default dropdown-toggle  col-md-12" style="min-width: 150px;" align="left"
                            data-toggle="dropdown" aria-expanded="false" id="btnDropMailTo">
                            --Select--
                     
                       <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulMailTo" style="overflow-y: auto; height: 160px;">
                        </ul>
                    </div>
                 
                    <asp:HiddenField  ID="hdnDropMailToSelectedId" runat="server"  Value="" EnableViewState="true" />
                    <asp:HiddenField ID="hdnDropMailToSelectedText" runat="server" Value="" />
                    <cc1:ExtendedDropDown ID="ExtendedDropDown1" CssClass="dropdown" runat="server" Width="300px" AutoPostBack="True" Style="display: none;"></cc1:ExtendedDropDown>




                    <%-- <div class="example-wrap" style="margin-bottom: 0;">
                       <div class="radio-custom radio-default">
                       <asp:RadioButton ID="MailtoCU" CssClass="redcaption col-md-2"  style="text-align:left;padding-left:0px;" runat="server" Text=" Customer"
                                 GroupName="MailTo" Checked="True"></asp:RadioButton>
                   <asp:RadioButton ID="MailtoGC" CssClass="redcaption col-md-3" style="margin-left:10px;" runat="server" Text=" Gen Cont"
                                 GroupName="MailTo"></asp:RadioButton>
                              <asp:RadioButton ID="MailtoOW" CssClass="redcaption col-md-2"  runat="server" Text=" Owner"
                      GroupName="MailTo"></asp:RadioButton>
                   <asp:RadioButton ID="MailtoLE" CssClass="redcaption col-md-2"  runat="server" Text=" Lender"
                         GroupName="MailTo"></asp:RadioButton>
                          </div>
                  </div>--%>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Who Will Issue Check:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left; float :left;">

                    <div class="btn-group" style="text-align: left; float :left " align="left">
                        <button type="button" class="btn btn-default dropdown-toggle  col-md-12" style="min-width: 150px;" align="left"
                            data-toggle="dropdown" aria-expanded="false" id="btnDropMailCheck">
                            --Select--
                     
                       <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulMailCheck" style="overflow-y: auto; height: 160px;">
                        </ul>
                    </div>
                    <asp:HiddenField ID="hdnDropMailCheckId" runat="server" Value="" />
                    <asp:HiddenField ID="hdnDropMailCheckIdText" runat="server" Value="" />
                    <cc1:ExtendedDropDown ID="ExtendedDropDown2" CssClass="dropdown" runat="server" Width="300px" AutoPostBack="True" Style="display: none;"></cc1:ExtendedDropDown>

                  <div class="checkbox-custom checkbox-default col-md-4 col-sm-4 col-xs-4" style="margin-left:10px;">
                       <asp:CheckBox ID="chkOtherIssuer" runat="server" Text=" Other" Width="184px" />
</div>

                    <%--<div class="example-wrap" style="margin-bottom: 0;">
                        <div class="radio-custom radio-default">
                            <asp:RadioButton ID="CheckbyCU" CssClass="redcaption  col-md-2" Style="text-align: left; padding-left: 0px;" runat="server" Text=" Customer"
                                GroupName="Issuer" Checked="True"></asp:RadioButton>
                            <asp:RadioButton ID="CheckByGC" CssClass="redcaption col-md-3" Style="margin-left: 10px;" runat="server" Text=" Gen Cont"
                                GroupName="Issuer"></asp:RadioButton>
                            <asp:RadioButton ID="CheckByOW" CssClass="redcaption col-md-2" runat="server" Text=" Owner"
                                GroupName="Issuer"></asp:RadioButton>
                            <asp:RadioButton ID="CheckByLE" CssClass="redcaption col-md-2" runat="server" Text=" Lender"
                                GroupName="Issuer"></asp:RadioButton>
                            <asp:RadioButton ID="CheckbyOT" runat="server" Width="100px" GroupName="Issuer" Text=" Other" CssClass="redcaption col-md-2"></asp:RadioButton>
                        </div>
                    </div>--%>
                </div>

            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Other Check Issuer:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <cc1:DataEntryBox CssClass="form-control" ID="OtherChkIssuerName" runat="server" Width="424px" DataType="Any"></cc1:DataEntryBox>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Joint Check Agreement:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                        <div class="checkbox-custom checkbox-default" style="padding-top:0px;">
                        <asp:CheckBox CssClass="checkbox" ID="JointCheckAgreement" runat="server" Text=" "></asp:CheckBox>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Notary:</label>
                <div class="checkbox-custom col-md-7 col-sm-7 col-xs-7">
                    <asp:CheckBox ID="NotarywithCA" CssClass="checkbox" Style="margin-left: 15px;" runat="server" Text=" Print CA Notary  (Use only if waiver is notarized in CA)"
                         OnClick="JavaScript:myNotary(1);" ></asp:CheckBox>

                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable"></label>
                <div class="checkbox-custom col-md-7 col-sm-7 col-xs-7">
                    <asp:CheckBox ID="NotarywithoutCA" CssClass="checkbox" Style="margin-left: 15px;" runat="server" Text=" Print Notary  (Do not use if waiver is notarized in CA)"
                       OnClick="JavaScript:myNotary(2);"></asp:CheckBox>
                </div>
            </div>
            <%-- <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Print CA Notary:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <a href="Images/ResourceImages/Liens/Notary(Jurat).pdf" target="_blank" title="CA Notary (Jurat)">Click Here</a>&nbsp;&nbsp;&nbsp;
                    <asp:Label>(Use only if waiver is signed in CA)</asp:Label>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Print Notary:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <a href="Images/ResourceImages/Liens/Notary(Jurat)1.pdf" target="_blank" title="Notary (Jurat)">Click Here</a>&nbsp;&nbsp;&nbsp;
                    <asp:Label>(Do not use if waiver is signed in CA)</asp:Label>
                </div>
            </div>--%>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">From Date:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <span style="display: flex;">
                        <SiteControls:Calendar ID="StartDate" runat="server" ErrorMessage="From Date"
                                                            IsReadOnly="false" IsRequired="FALSE" Value="" /></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Through Date:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <span style="display: flex;">
                        <SiteControls:Calendar ID="ThroughDate" runat="server" ErrorMessage="From Date"
                                                            IsReadOnly="false" IsRequired="FALSE" Value="" /></span>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Signer:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                           <div class="btn-group" style="text-align:left;" align="left">
                                                                                <button type="button" class="btn btn-default dropdown-toggle  col-md-12" style="min-width:200px;" align="left"
                            data-toggle="dropdown" aria-expanded="false" id="btnDropSigner">
                            --Select Signer--
                     
                                                                                    <span class="caret"></span>
                        </button>
                                                                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownSigner" style="overflow-y:auto;height:250px;">
                            <%--  <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>
                                                                                    <li class="divider" role="presentation"></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Separated link</a></li>--%>
                        </ul>
                    </div>
                    <asp:HiddenField ID="hdnSignerSelectedValue" runat="server" Value="" />
                    <cc1:ExtendedDropDown ID="SignerId" CssClass="dropdown" runat="server" Width="335px" Style="display: none;"></cc1:ExtendedDropDown>
                    <uc3:AddSigner ID="AddSigner1" runat="server"></uc3:AddSigner>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Email:</label>
                <div class="col-md-7 col-sm-7 col-xs-7">
                    <cc1:DataEntryBox ID="txtCustEmail" runat="server" CssClass="form-control" DataType="Any" ReadOnly="false"
                        ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                        FriendlyName="" IsRequired="False" IsValid="True" Width="424px" ValidationExpression="" Value=""></cc1:DataEntryBox>
                    <asp:HiddenField ID="hdnCustId" runat="server" Value="" />
                </div>
            </div>


            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Additional Note:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <cc1:DataEntryBox CssClass="form-control" ID="WaiverNote" runat="server" Width="424px" DataType="Any"></cc1:DataEntryBox>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">CA Previous Waiver Dates:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <cc1:DataEntryBox CssClass="form-control" ID="WaiverDates" runat="server" Width="424px" DataType="Any"></cc1:DataEntryBox>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">CA Unpaid Progress Pmts:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <cc1:DataEntryBox CssClass="form-control" ID="WaiverPayments" runat="server" Width="424px" DataType="Any"></cc1:DataEntryBox>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">NV Invoice/Payment App#:</label>
                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                    <cc1:DataEntryBox CssClass="form-control" ID="InvoicePaymentNo" runat="server" Width="424px" DataType="Any"></cc1:DataEntryBox>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-12 control-label">
                    <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                    <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
                </label>

            </div>
        </div>



        <br />

    </div>
    <div class="footer">
        <asp:UpdatePanel ID="updPan1" runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="btnSAVE"
                    CssClass="btn btn-primary"
                    runat="server" OnClientClick="return checkIsFinalWaiver(event)"
                    Text="Submit" CausesValidation="False" />&nbsp;
                                                    <br />
                <br />

                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <div class="TransparentGrayBackground"></div>
                        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                            <div class="PageUpdateProgress">
                                <asp:Image ID="ajaxLoadNotificationImage"
                                    runat="server"
                                    ImageUrl="~/images/ajax-loader.gif"
                                    AlternateText="[image]" />
                                &nbsp;Please Wait...
                            </div>
                        </asp:Panel>
                        <ajaxToolkit:AlwaysVisibleControlExtender
                            ID="AlwaysVisibleControlExtender1"
                            runat="server"
                            TargetControlID="alwaysVisibleAjaxPanel"
                            HorizontalSide="Center"
                            HorizontalOffset="150"
                            VerticalSide="Middle"
                                                                VerticalOffset="0">
                                                            </ajaxToolkit:AlwaysVisibleControlExtender>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<%--   <asp:HiddenField ID="hdnJobWaiverLogId" runat="server" Value="" />
        <asp:HiddenField ID="hdnCustEmail" runat="server" Value="" />
        <asp:Button ID="btnPrintWaiver" runat="server" OnClick="btnPrintWaiver_Click" Style="display: none;" />--%>
<asp:Button ID="btnEmailsend" runat="server" OnClick="btnEmailsend_Click" Style="display: none;" />


<div class="modal fade modal-primary" id="divModalEmail" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose" onclick="return CloseEmailLoadPage();">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title">Send Email</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4">To:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtToList" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <label style="color: red;text-align:left;">Note:- Multiple email ids can be added separated by semicolon</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Subject:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" Text="New Waiver Document"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Message:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtBody" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <asp:Label ID="lblErrorMsg" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--<asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Send" Style="display: none;" />--%>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaves" onclick="SendMail();">
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="return fadeout();">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-primary example-modal-lg" id="ModalWaiverSubmit" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseEmailLoadPage();">
                    <span aria-hidden="true">�</span>

                </button>


                <asp:Button ID="hdnbtnclose" CssClass="btn btn-primary" runat="server" Text="" Style="display: none;" OnClick="hdnbtnclose_Click" />
                <h3 class="modal-title">Waiver Created !!</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">

                    <h3 class="modal-title">Your Waiver Has Been Successfully Created </h3>
                </div>
            </div>
            <div class="modal-footer" style ="text-align:center">
                <asp:Button ID="btnNew" runat="server"   Text="New Waiver"  CssClass="btn btn-primary" OnClick="btnNew_Click" />
              <asp:Button ID="btnEmail" runat="server"   Text="Email Waiver" CssClass="btn btn-primary" OnClick="btnEmailWaiver_Click" />
               <asp:Button ID="btnPrint" runat="server"    Text="Print Waiver" CssClass="btn btn-primary" OnClick="btnPrintWaiver_Click"/>
               
            </div>
        </div>
    </div>
</div>


