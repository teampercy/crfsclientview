﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobInfo.ascx.vb" Inherits="App_Controls__Custom_LienView_JobInfo" %>
<%@ Register Src="JobAddNote.ascx" TagName="JobAddNote" TagPrefix="uc7" %>
<%@ Register Src="JobEdit.ascx" TagName="JobEdit" TagPrefix="uc6" %>
<%@ Register Src="FileUpload.ascx" TagName="uploadFile" TagPrefix="uc7" %>


<%--<%@ Register Src="~/App_Controls/_Custom/LienView/Default.aspx" TagName="AddDocumentsNew" TagPrefix="uc8" %>--%>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc2" %>

<style>
    .model-size {
        min-width: 700px;
        overflow-x: scroll;
    }

    .modal-open .modal {
        overflow-x: auto !important;
    }

    .TabBorder {
        border-color: #3a6dae;
    }

    .PaddingleftRemove {
        padding-left: 0px;
    }

    .tdhidden {
        display: none;
    }
</style>
<script language="javascript" type="text/javascript">

    function DisplayEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('show');
    }
    function HideEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('hide');
    }
    //
    //^^*** Created by pooja 10/15/2020*** ^^
    function Clear() {
        debugger;

        $("#frmUpload").attr("src", "App_Controls/_Custom/LienView/Default.aspx?id=1 &jobid=1");

        $("#ModalFileUpload").modal('hide');

    }
    //'^^***  *** ^^
    function SendMail() {
        if (document.getElementById('<%=txtToList.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the 'To' EmailId.";
            return false;
        }
        if (document.getElementById('<%=txtSubject.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the Subject.";
            return false;
        }
        document.getElementById('<%=btnEmailsend.ClientID%>').click();


        HideEmailPopUp();

        return false;
    }

    function EmailSendPopup() {
        debugger;
        swal({
            title: "Successfully Sent!",
            text: "Your Waiver Has Been Emailed.",
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false

        });

    }

    function Cancelpopup() {
        location.href = currURL;
        return false;
    }


    function DisplayNotePopUp() {
        // alert('hi');
        $("#ModalJobNoteDetail").modal('show');
    }
    function BindFileUoloadList() {

        document.getElementById('<%=btnFileUpLoadCancel.ClientID%>').click();
    }
    function SetNoteId(id, type) {
        //alert("id" + id);
        //alert("type" + type);
        document.getElementById('<%=hdnBatchJobNoteId.ClientID%>').value = $(id).attr("rowno");
         document.getElementById('<%=hdnBatchJobNoteType.ClientID%>').value = type;
         document.getElementById('<%=btnNoteDetail.ClientID%>').click();
         return false;
     }
     function DeleteAccountWaiver(id) {
         document.getElementById('<%=hdnWaiverId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteWaiverId.ClientID%>').click();
      });
        return false;
    }
    function DeleteAccountStateForm(id) {
        document.getElementById('<%=hdnStateFormId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnStateFormId.ClientID%>').click();
      });
        return false;
    }
    function DeleteAccountTexas(id) {
        document.getElementById('<%=hdnTXReqId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnTXReqId.ClientID%>').click();
      });
        return false;
    }
    function DeleteAccountTexasRequest(id) {
        document.getElementById('<%=hdnTexasRequest.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnTexasRequest.ClientID%>').click();
      });
        return false;
    }
    function DeleteAccountInvoice(id) {
        document.getElementById('<%=hdnInvoiceid.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnInvoiceid.ClientID%>').click();
      });
        return false;
    }
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Jobs', 'View Jobs');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubJobList');
        //$('#liJobs').addClass('site-menu-item has-sub active open');
        //$('#lisubJobList').addClass('site-menu-item active');
        return false;

    }
    function hideDiveNotes() {
        // alert("hi");
        $('#divnotes').hide();
        $('#divAddDoc').hide();
        $('#divWaiver').hide();
        $('#divStateForm').hide();
    }
    function ActivateTabSet() {
        // alert('hello:Notes');
        ActivateTab('Notes')
    }
    function DisplayTablelayout() {
        document.getElementById('<%=btnTableLayout.ClientID%>').click();
    }
    function SetHeaderInformation(PageName) {
        //   alert(PageName);
        if (PageName == "") {
            //alert('hi');
            $('#liTools').removeClass('active open');
            $('#liTexaspendingList').removeClass('active');
            $('#liJobsTitle').text("Jobs");
            $('#liJobSSubTitle').text("View Jobs");
            $('#liJobs').addClass('site-menu-item has-sub active open');
            $('#lisubJobList').addClass('site-menu-item active');

        }
        else {
            $('#liJobs').removeClass('active open');
            $('#lisubJobList').removeClass('active');
            $('#liJobsTitle').text("Tools");
            $('#liJobSSubTitle').text("Texas Pending List");
            $('#liTools').addClass('site-menu-item has-sub active open');
            $('#liTexaspendingList').addClass('site-menu-item active');

        }
    }
    function ActivateTab(TabName) {
        switch (TabName) {
            case "MainInfo":
                if (!$('#' + '<%=MainInfo.ClientID%>').hasClass('active')) {
                    $('#' + '<%=MainInfo.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsOne').hasClass('active'))
                    $('#exampleTabsOne').addClass('active');
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "LegalParties":
                //  alert('2');
                if (!$('#' + '<%=LegalParties.ClientID%>').hasClass('active')) {
                    $('#' + '<%=LegalParties.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsTwo').hasClass('active'))
                    $('#exampleTabsTwo').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                 $('#exampleTabsFive').removeClass("active");
                 $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                  $('#exampleTabsSeven').removeClass("active");
                  $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                 $('#exampleTabsNine').removeClass("active");
                 $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                 $('#exampleTabsTen').removeClass("active");
                 break;
             case "Notes":
                 debugger;
                 if (!$('#' + '<%=Notes.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Notes.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsThree').hasClass('active'))
                    $('#exampleTabsThree').addClass('active');

                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "NoticesSent":
                // alert('4');
                if (!$('#' + '<%=NoticesSent.ClientID%>').hasClass('active')) {
                    $('#' + '<%=NoticesSent.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsFour').hasClass('active'))
                    $('#exampleTabsFour').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "Docs":
                if (!$('#' + '<%=Docs.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Docs.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsFive').hasClass('active'))
                    $('#exampleTabsFive').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "Waivers":
                // alert('hi');

                if (!$('#' + '<%=Waivers.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Waivers.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsSix').hasClass('active'))
                    $('#exampleTabsSix').addClass('active');
                // alert('callend');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");

                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");


                break;
            case "StateForms":
                if (!$('#' + '<%=StateForms.ClientID%>').hasClass('active')) {
                    $('#' + '<%=StateForms.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsSeven').hasClass('active'))
                    $('#exampleTabsSeven').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");

                break;
            case "NoticeRequest":
                if (!$('#' + '<%=NoticeRequest.ClientID%>').hasClass('active')) {
                    $('#' + '<%=NoticeRequest.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsEight').hasClass('active'))
                    $('#exampleTabsEight').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "TXRequests":
                if (!$('#' + '<%=TXRequests.ClientID%>').hasClass('active')) {
                    $('#' + '<%=TXRequests.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsNine').hasClass('active'))
                    $('#exampleTabsNine').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "Invoices":
                if (!$('#' + '<%=Invoices.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Invoices.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsTen').hasClass('active'))
                    $('#exampleTabsTen').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=NoticeRequest.ClientID%>').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=TXRequests.ClientID%>').removeClass("active");
                $('#exampleTabsNine').removeClass("active");
                break;
        }
    }
    function SetTabName(id) {
        // alert(id);
        document.getElementById('<%=hddbTabName.ClientID%>').value = id;
    }
    function opengooglemap() {
        //     var add=document.getElementById('JobAdd1');
        //     var city=document.getElementById('JobCity');
        //     var state=document.getElementById('JobState');
        //     var zip=document.getElementById('JobZip')
        //       var GMapReqAddress = document.getElementById("< %= JobAdd1.ClientID %>").value + "," + document.getElementById("< %= JobCity.ClientID %>").value + "," + document.getElementById("< %= JobState.ClientID %>").value+ "," + document.getElementById("< %= JobZip.ClientID %>").value; 
        //var strpath = "http://maps.google.com/maps?q=" + $get('< %=JobAdd1.ClientID %>').value + "," + $get('< %=JobCity.ClientID %>').value + "," + $get('< %=JobState.ClientID %>').value + "," + $get('< %=JobZip.ClientID %>').value;
        var strpath = "http://maps.google.com/maps?q=" + $get('<%=JobAdd1.ClientID %>').value + "," + $get('<%=JobCity.ClientID %>').value;
        //    var varurl='ShowGoogleMap.aspx?add=' +GMapReqAddress;
        // var varurl='http://maps.google.com/maps?add=' +GMapReqAddress;
        PopupCenter(strpath, "mywindow", '900', '500');
        // window.open(strpath, "mywindow", "menubar=1,resizable=1,width=700,height=450");
    }
    function PopupCenter(url, title, w, h) {
        // Fixes dual-screen position                         Most browsers      Firefox
        var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
        var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

        var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
        var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

        var left = ((width / 2) - (w / 2)) + dualScreenLeft;
        var top = ((height / 2) - (h / 2)) + dualScreenTop;
        var newWindow = window.open(url, title, 'menubar=1,resizable=1,scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

        // Puts focus on the newWindow
        if (window.focus) {
            newWindow.focus();
        }
    }
    function openWin(cert) {

        var str = "./App_Controls/_Custom/LienView/TrackAndConfirm.aspx?CertNum=" + cert
        window.open(str, "mywindow", "menubar=1,resizable=1,width=800,height=550");
        return false;
    }
    function DisplayAdditionalInfo() {
        $("#ModaldivAdditionalInfo").modal('show');
        return false;
    }

    function RemoveBackdrop() {
        $('div.modal-backdrop').remove()
    }

   
</script>
<script type="text/javascript">

    //Added just for testing by KD
    //function getJobDetails(result) {
    //    console.log(result);
    //    alert(result);
    //}

    $(document).ready(function () {
        debugger;
        if ($('#' + '<%=gvwLegalParties.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncLegalPartiesList();
        }
        if ($('#' + '<%=gvwNotes.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncNotesList();
        }
        if ($('#' + '<%=gvwDocs.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncDocsList();
        }
        if ($('#' + '<%=gvwWaivers.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncWaiverList();
        }
        if ($('#' + '<%=gvwStateForms.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncStateList();
        }
        if ($('#' + '<%=gvwTexasReq.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncTexasList();
        }
        if ($('#' + '<%=gvwVReq.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncNoticesReqList();
        }
        if ($('#' + '<%=gvwNotices.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncNoticesList();
        }
        if ($('#' + '<%=gvwINVC.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncInvoiceList();
        }
        $("#btnAddDocumentsNew").click(function (e){
            DeleteSessionNew()
        });
    });
    function CallSuccessFuncNoticesReqList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwVReq.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncInvoiceList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwINVC.ClientID%>').dataTable(options);

    }
<%--    function CallSuccessFuncLegalPartiesList() {
        debugger;
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwLegalParties.ClientID%>').dataTable(options);
    }--%>

   function CallSuccessFuncLegalPartiesList() {
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            // "lengthMenu": [10, 20, 30, 50, 100],
            "bFilter": false,      //Remove Dropdown         
            "bLengthChange": false, //restrict to chnage Dropdown
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
            ],
            "bPaginate": false, //Remove Pagination
            "bInfo": false //Remove showing entries
        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwLegalParties.ClientID%>').dataTable(options);

    }

    function CallSuccessFuncNotesList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };


        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwNotes.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncNoticesList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwNotices.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncDocsList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                 { "bSortable": false }


            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwDocs.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncWaiverList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwWaivers.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncStateList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }


            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwStateForms.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncTexasList() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwTexasReq.ClientID%>').dataTable(options);

    }
    function Successpopup() {
        //empty function required
    }
    function DeleteAccount(id) {
        debugger;
        document.getElementById('<%=hdnDocId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=hddbTabName.ClientID%>').value = "Docs";
          document.getElementById('<%=btnDeleteDoc.ClientID%>').click();
      });
        return false;
    }

    function DisplayRequestNotice() {
        //alert("DisplayRequestNotice" + document.getElementById('<%=hdnIsCancelled.ClientID%>').value);
        debugger;
        if (document.getElementById('<%=hdnIsCancelled.ClientID%>').value == "True") {
            VerifyCancelledRequest();
        }
        else {
               document.getElementById('<%=btnAddVReq.ClientID%>').click();
        }
        return false;
    }
    function VerifyCancelledRequest() {
        //alert("VerifyCancelledRequest");
        debugger;
        swal({
            title: "Are you sure?",
            text: "A Notice Request was previously cancelled on this job. Do you still wish to proceed with this current request?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            closeOnConfirm: true
        },
function () {
    document.getElementById('<%=btnAddVReq.ClientID%>').click();
});
        return false;
    }


    //Recension Letter Popup
    function RecensionLetterCreatedPopup(AddressName) {
        debugger;
        swal({
            title: "Recension Letter Created",
            text: "Your Recension Letter has been created for \"" + AddressName + "\" Legal Party.",
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
        });
        ActivateTab('LegalParties');
        return false;
    }

    var currURL;
    currURL = window.location.href;
    function Cancelpopup() {
        debugger;
        location.href = currURL;
        //$("ctl33_ctl00_MainInfo").removeClass("active");
        //$("ctl33_ctl00_LegalParties").addClass("active");
        ActivateTab('LegalParties');
        return false;
    }

    function ExitLegalParties() {
        debugger;
        alert('Legal Party Details are not present.');
        ActivateTab('LegalParties');
        return false;
    }

    function RecensionLetterIsAlreadyCreatedToday(AddressName) {
        debugger;
        alert("Today Recension Letter is already created for \"" + AddressName + "\" Legal Party. Please check in Waivers Tab.");
        ActivateTab('LegalParties');
        return false;
    }

    function showRecensionLetterCreatedPopup(AddressName) {
        debugger;
        swal.close(); // closes sweet alert popup.
        var mymodal = $('#ModalRecensionLetterSubmit');
        document.getElementById("RecensionLetterModaldiv").innerHTML = "<h3>Your Recension Letter has been created for \"" + AddressName + "\" Legal Party.</h3>";
        //mymodal.find('.modal-body').text("<h3>Your Recension Letter has been created for \"" + AddressName + "\" Legal Party.</h3>");
        mymodal.modal('show');
        return false;
    }

    function SendRecensionLetterMail() {
        debugger;
        if (document.getElementById('<%=txtToListForRecensionLetter.ClientID%>').value == "") {
            document.getElementById('<%=lblError.ClientID%>').innerText = "Please enter the 'To' Email Id.";
            return false;
        }
         if (document.getElementById('<%=txtRecensionLetterSubject.ClientID%>').value == "") {
            document.getElementById('<%=lblError.ClientID%>').innerText = "Please enter the Subject.";
            return false;
        }
        
            document.getElementById('<%=btnEmailsend.ClientID%>').click();
            HideEmailRecensionLetterPopUp();
            return false;
  
    }
    function HideRecensionLetterPopUp() {
        // alert('hi');
        $("#ModalRecensionLetterSubmit").modal('hide');
    }

    function ShowRecensionLetterPopUp() {
        // alert('hi');
        $("#ModalRecensionLetterSubmit").modal('show');
    }

    function DisplayEmailRecensionLetterPopUp() {
        // alert('hi');
        $("#divModalEmailRecensionLetter").modal('show');
    }
    function HideEmailRecensionLetterPopUp() {
        // alert('hi');
        $("#divModalEmailRecensionLetter").modal('hide');
        $('div').removeClass('modal-backdrop fade in');
        //fadeout();
    }

    function fadeout() {
        debugger;
        //alert(currURL);
        location.href = currURL;
        $('div').removeClass('modal-backdrop fade in');
        return false();
    }

    function RecensionLetterEmailSendPopup(textdata) {
        debugger;
        swal({
            title: "Successfully Sent!",
            text: textdata,
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }

    function RecensionLetterNotAvailableForLegalParties(AddressName) {
        debugger;
        alert("Recension Letter is Not Available For \""+AddressName+"\" Legal Party.");
        ActivateTab('LegalParties');
        return false;
    }

    function CreateAnotherRecensionLetter(AddressName) {
        swal({
            title: "Are you sure?",
            text: "You want to create another Recension Letter for \"" + AddressName + "\" Legal Party.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            closeOnConfirm: false
        },
      function (isConfirm) {
          if (isConfirm) {
              document.getElementById('<%=btnCreateAnotherRecensionLetter.ClientID%>').click(); 
            }
      });
        return false;
    }
    function AddDocumentsNew() {
        //debugger;
        //alert("The button clicked.");
        //$.ajax({
        //    //type: "POST",
        //    //url: "Default.aspx/DeletSession",
        //    ////data: '{RowIndx: "' + Index + '" }',
        //    //// data: '{filename: "' + filename + '" }',
        //    //contentType: "application/json; charset=utf-8",
        //    //dataType: "json",
        //    type: "POST",
        //    url: "FileUpload.aspx/DeletSession",
        //    contentType: "application/json; charset=utf-8",
        //    async: false,
        //    dataType: "json",
        //    success: function (response) {
                $('#ModalFileUpload').modal('show');
               // alert("success");
                //return false;
        //        //window.open("../Pages/NewCharge.aspx", "_self");
        //    },
        //    failure: function (response) {
        //        //alert(response.d);
        //        alert("fail");
        //    }
        //});

        return false;
    }
    function AddDocumentsNew2() {
        debugger;
        alert("The button clicked.");
        $.ajax({
            //type: "POST",
            //url: "Default.aspx/DeletSession",
            ////data: '{RowIndx: "' + Index + '" }',
            //// data: '{filename: "' + filename + '" }',
            //contentType: "application/json; charset=utf-8",
            //dataType: "json",
            type: "POST",
            url: "FileUpload.aspx/DeletSession",

            contentType: "application/json; charset=utf-8",
            async: false,
            dataType: "json",
            success: function (response) {
                $('#ModalFileUpload').modal('show');
                alert("success");
                return false;
                //window.open("../Pages/NewCharge.aspx", "_self");
            },
            failure: function (response) {
                //alert(response.d);
                alert("fail");
            }
        });

        return false;
    }
//$(function () {
    //    $("#btnAddDocumentsNew").click(function (e) {
   //function DeleteSessionNew(){

   //     alert('hi');
   //       $.ajax({
   //              type: "POST",
   //              url: "Default.aspx/DeleteSession",
   //             contentType: "application/json; charset=utf-8",
   //             async: false,
   //             dataType: "json",
   //             success: function (response) {
   //                 $('#ModalFileUpload').modal('show');
   //                 alert("success");
   //                 return false;
   //                 //window.open("../Pages/NewCharge.aspx", "_self");
   //             },
   //             failure: function (response) {
   //                 //alert(response.d);
   //                 alert("fail");
   //             }
   //         });
          
   //     }

    //})

</script>

<div id="modcontainer" style="margin: 10px; width: 100%;">
    <h1>
        <asp:Literal ID="litJobInfo" runat="server"></asp:Literal></h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <asp:Panel ID="panJobInfo" runat="server">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                <asp:Button ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to List" CausesValidation="False" />
                                <%-- <asp:HiddenField runat="server" ID="hddbTabName" Value="MainInfo" />
                                <uc6:JobEdit ID="JobEdit1" runat="server" />--%>
                                <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" OnClick="btnBackFilter_Click" Text="Back to Filter" CausesValidation="False"></asp:LinkButton>
                                <asp:Button ID="btnTableLayout" runat="server" Style="display: none;" OnClick="btnTableLayout_Click" />
                                <asp:Button ID="btngooglejob" runat="server" CssClass="btn btn-primary" Text="Google Job" OnClientClick="opengooglemap();" />
                            </div>
                            <%--   <div class="col-md-2 col-sm-2 col-xs-2 col-sm-2 col-xs-2" style="text-align: left;">
                             
                            </div>
                            <div class="col-md-2 col-sm-2 col-xs-2 col-sm-2 col-xs-2" style="text-align: left;">
                             
                            </div>--%>
                        </div>
                    </div>
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li id="MainInfo" class="active" role="presentation" runat="server"><a data-toggle="tab" onclick="SetTabName('MainInfo');" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Main Info</a></li>
                            <li id="LegalParties" role="presentation" runat="server"><a data-toggle="tab" onclick="SetTabName('LegalParties');" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Legal Parties</a></li>
                            <li id="Notes" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Notes');" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab" aria-expanded="false">Notes</a></li>
                            <li id="NoticesSent" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('NoticesSent');" href="#exampleTabsFour" aria-controls="exampleTabsFour" role="tab" aria-expanded="false">Notices Sent</a></li>
                            <li id="Docs" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Docs');" href="#exampleTabsFive" aria-controls="exampleTabsFive" role="tab" aria-expanded="false">Docs</a></li>
                            <li id="Waivers" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Waivers');" href="#exampleTabsSix" aria-controls="exampleTabsSix" role="tab" aria-expanded="false">Waivers</a></li>
                            <li id="StateForms" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('StateForms');" href="#exampleTabsSeven" aria-controls="exampleTabsSeven" role="tab" aria-expanded="false">State Forms</a></li>
                            <li id="NoticeRequest" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('NoticeRequest');" href="#exampleTabsEight" aria-controls="exampleTabsEight" role="tab" aria-expanded="false">Notice Request</a></li>
                            <li id="TXRequests" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('TXRequests');" href="#exampleTabsNine" aria-controls="exampleTabsNine" role="tab" aria-expanded="false">TX Requests</a></li>
                            <li id="Invoices" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Invoices');" href="#exampleTabsTen" aria-controls="exampleTabsTen" role="tab" aria-expanded="false">Invoices</a></li>
                        </ul>
                        <div class="tab-content padding-top-20" style="border: 1px solid lightgray;">
                            <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Job Number:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobNumber" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3  control-label align-lable PaddingleftRemove">CRF:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="JobId" runat="server" CssClass="form-control" ReadOnly="True" DataType="Any"
                                                    Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobId" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression="">12:00:00 AM</cc2:DataEntryBox>
                                            </div>

                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Notice Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NOIDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Tag="LienSent" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Job Name:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobName" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobName" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Assigned:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="DateAssigned" runat="server" CssClass="form-control" ReadOnly="True" DataType="DateValue"
                                                    Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="StartDate" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">NOI Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NOIDeadline" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Tag="LienSent" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                        </div>
                                        <%-- <label class="col-md-2 col-sm-2 col-xs-2 col-sm-2 col-xs-2 control-label align-lable">Send As Is:</label>
                                        <div class="col-md-1">
                                            <div class="checkbox-custom checkbox-default">
                                                <asp:CheckBox ID="chkSendAsIS" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Address:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobAdd1" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobAdd1" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">First Furnished:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="StartDate" runat="server" CssClass="form-control" ReadOnly="True" DataType="DateValue"
                                                    Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EndDate" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Lien Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="LienDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Tag="LienDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                        </div>
                                        <%--  <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Verify As Is:</label>
                                        <div class="col-md-1">
                                            <div class="checkbox-custom checkbox-default">
                                                <asp:CheckBox ID="chkVerify" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove"></label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobAdd2" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobAdd2" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Last Furnished:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="EndDate" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="DateAssigned" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Bond Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Tag="BondDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                        </div>
                                        <%-- <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Public:</label>
                                        <div class="col-md-1">
                                            <div class="checkbox-custom checkbox-default">
                                                <asp:CheckBox ID="chkPublicJob" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove"></label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobCity" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Tag="JobCity" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Notice of Comp:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NOCDate" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="NoticeSent" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">SN Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="SNDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Tag="SNDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <cc2:DataEntryBox ID="JobState" runat="server" Visible="false"></cc2:DataEntryBox>
                                        <cc2:DataEntryBox ID="JobZip" runat="server" Visible="false"></cc2:DataEntryBox>
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Job County</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobCounty" runat="server" CssClass="form-control" ReadOnly="True" Tag="JobSate" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Prelim Sent:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NoticeSent" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Tag="LienSent" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Foreclose Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="ForecloseDeadline" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="NoticeSent" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            
                                        </div>

                                        <%--  <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Residential:</label>
                                        <div class="col-md-1">
                                            <div class="checkbox-custom checkbox-default">
                                                <asp:CheckBox ID="chkResindential" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="form-group">
                                        <%--<label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Job County</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobCounty" runat="server" CssClass="form-control" ReadOnly="True" Tag="JobSate" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>--%>
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Property Type:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="PropertyType" runat="server" CssClass="form-control" ReadOnly="True" Tag="PropertyType" BackColor="White" DataType="Any"
                                                ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Lien Filed:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="LienDate" runat="server" CssClass="form-control" ReadOnly="True" Style="text-align: center;"
                                                    DataType="DateValue" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EstBalance" Value="0.00" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Foreclose Filed:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="ForecloseDate" runat="server" CssClass="form-control" ReadOnly="True"
                                                    DataType="DateValue" Tag="LienSent" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            
                                        </div>


                                        <%--  <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Lien Alert:</label>
                                        <div class="col-md-1">
                                            <div class="checkbox-custom checkbox-default">
                                                <asp:CheckBox ID="chkJobAlert" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                                            </div>
                                        </div>--%>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Est Balance:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="EstBalance" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Money" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EstBalance" Value="0.00" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$">$0.00</cc2:DataEntryBox>
                                        </div>


                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Bond Sent:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondDate" runat="server" CssClass="form-control" ReadOnly="True" Style="text-align: center;"
                                                    DataType="DateValue" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EstBalance" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">BondSuit Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondSuitDeadline" runat="server" CssClass="form-control" ReadOnly="True" Style="text-align: center;"
                                                    DataType="DateValue" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EstBalance" Value="0.00" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"></cc2:DataEntryBox>
                                            </div>
                                            
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Customer #:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="CustRefNum" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="CustJobNum" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">SN Sent:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="SNDate" runat="server" CssClass="form-control" ReadOnly="True" Style="text-align: center;"
                                                    DataType="DateValue" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EstBalance" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Bond Suit Filed:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondSuitDate" runat="server" CssClass="form-control" ReadOnly="True" Style="text-align: center;"
                                                    DataType="DateValue" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EstBalance" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"></cc2:DataEntryBox>
                                            </div>


                                        </div>
                                    </div>
                                   <%-- <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-3 col-sm- col-xs-3 control-label align-lable">
                                                <asp:Literal ID="litLabel1" runat="server"></asp:Literal></label>
                                            <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable" style="font-family: Arial, Helvetica, sans-serif; text-align: left">
                                                <asp:Literal ID="litData1" runat="server"></asp:Literal>
                                            </label>
                                    </div>--%>
                                       <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 col-sm- col-xs-2 control-label align-lable PaddingleftRemove"">
                                                <asp:Literal ID="litLabel1" runat="server"></asp:Literal></label>
                                            <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable" style="font-family: Arial, Helvetica, sans-serif; text-align: left">
                                                <asp:Literal ID="litData1" runat="server"></asp:Literal>
                                            </label>
                                    </div>
                                    <div class="form-group" id="divlabel2" runat="server">
                                        <div>
                                            <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">
                                                <asp:Literal ID="litlabel2" runat="server"></asp:Literal></label>
                                            <label class="col-md-5  col-sm-5 col-xs-5 control-label align-lable" style="font-family: Arial, Helvetica, sans-serif; text-align: left">
                                                <asp:Literal ID="litData2" runat="server"></asp:Literal>
                                            </label>

                                        </div>

                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Customer Name:</label>
                                        <div class="col-md-4 col-sm-4 col-xs-4 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="CustomerName" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="StatusCode" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Service Type:</label>
                                        <div class="col-md-4 col-sm-4 col-xs-4 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="ServiceType" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="StatusCode" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Job Status:</label>
                                        <div class="col-md-4 col-sm-4 col-xs-4 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="StatusCode" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="StatusCode" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                    </div>

                                    <%--             <asp:Panel ID="panRentalINfo" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">RA #:</label>
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <cc2:DataEntryBox ID="RANum" runat="server" CssClass="form-control" IsRequired="False"
                                                    tag="RANum" DataType="Any" BackColor="White" BlankOnZeroes="True" ErrorMessage="" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask="" FriendlyName="" FormatString="" ValidationExpression=""></cc2:DataEntryBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Equip Rate:</label>
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <cc2:DataEntryBox ID="EquipRate" runat="server" CssClass="form-control" IsRequired="False"
                                                    tag="Equipment Rate" DataType="Money" BackColor="White" BlankOnZeroes="True" ErrorMessage="" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="0.00" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$">$0.00</cc2:DataEntryBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Equip Descr:</label>
                                            <div class="col-md-5  col-sm-5 col-xs-5">
                                                <cc2:DataEntryBox ID="EquipRental" runat="server" CssClass="form-control" IsRequired="False"
                                                    tag="EquipRental" BackColor="White" BlankOnZeroes="True" DataType="Any" ErrorMessage="" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                            </div>
                                        </div>
                                    </asp:Panel>--%>
                                    <asp:Panel ID="panAZLotAllocate" runat="server">
                                        <div class="form-group">
                                            <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Lot Allocation:</label>
                                            <div class="col-md-8  col-sm-8 col-xs-8 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="AZLotAllocate" runat="server" CssClass="form-control" Tag="Lot Allocation" IsRequired="False"
                                                    Height="30px" TextMode="MultiLine" DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" ValidationExpression="" Value=""></cc2:DataEntryBox>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <%--            <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Special Instr:</label>
                                        <div class="col-md-8  col-sm-8 col-xs-8">
                                            <cc2:DataEntryBox ID="SpecialInstruction" runat="server" CssClass="form-control" Tag="Special Instruction" IsRequired="False"
                                                Height="32px" TextMode="MultiLine" NormalCSS="form-control" DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable">Legal Descr:</label>
                                        <div class="col-md-8  col-sm-8 col-xs-8">
                                            <cc2:DataEntryBox ID="Note" runat="server" CssClass="form-control" Tag="Legal Description" IsRequired="False"
                                                Height="32px" TextMode="MultiLine" NormalCSS="form-control" DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="" FriendlyName="" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                                        </div>

                                    </div>--%>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                            <asp:HiddenField runat="server" ID="hddbTabName" Value="MainInfo" />
                                             <asp:HiddenField runat="server" ID="hdnIsCancelled" Value=False />
                                            <uc6:JobEdit ID="JobEdit1" runat="server" />
                                            <input type="button" id="btnAdditionalInfo" class="btn btn-primary" value="Additional Info" onclick="DisplayAdditionalInfo()" />

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwLegalParties" Width="100%" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="table dataTable table-striped">
                                                        <%--    <RowStyle CssClass="rowstyle" />
                                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:BoundField DataField="TypeCode" SortExpression="TypeCode" HeaderText="Legal Party">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AddressName" SortExpression="AddressName" HeaderText="Name">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AddressLine1" SortExpression="AddressLine1" HeaderText="Address1">
                                                                <ItemStyle Height="15px" Wrap="True" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AddressLine2" SortExpression="AddressLine2" HeaderText="Address2">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="City" SortExpression="City" HeaderText="City">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="State" SortExpression="State" HeaderText="State">
                                                                <ItemStyle HorizontalAlign="Center" Width="20px" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                              <%--              <asp:BoundField DataField="PostalCode" SortExpression="PostalCode" HeaderText="Zip">
                                                                <ItemStyle Width="40px" Height="15px" Wrap="False" />
                                                            </asp:BoundField>--%>
                                                            <asp:BoundField DataField="TelePhone1" SortExpression="TelePhone1" HeaderText="Phone">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>

                                                              <asp:TemplateField HeaderText="Recension">
                                                                <ItemTemplate>
                                                                       <asp:LinkButton ID="btnRecension" CssClass="btn btn-primary" Text="Recension" runat="server" style="color: white !important;" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="70px" Wrap="False" />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <%--##1--%>
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;" id="divnotes">
                                                <uc7:JobAddNote ID="JobAddNote1" runat="server" />
                                                <asp:Button ID="btnPrintNotes" runat="server" Text="Print Notes" CssClass="btn btn-primary" />
                                            </div>
                                            <%--##1--%>
                                            <%-- <div class="col-md-2 col-sm-2 col-xs-2">
                                                <uc7:JobAddNote ID="JobAddNote1" runat="server" />
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:Button ID="btnPrintNotes" runat="server" Text="Print Notes" CssClass="btn btn-primary" />
                                            </div>--%>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwNotes" Width="100%" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="table dataTable table-striped">
                                                        <%--   <RowStyle CssClass="rowstyle" />
                                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <%--&nbsp;&nbsp;<asp:ImageButton ID="btnViewNote" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />&nbsp;&nbsp;--%>
                                                                    <asp:LinkButton ID="btnViewNote" CssClass="icon ti-eye" runat="server" OnClientClick=<%# "return SetNoteId(this,'" + Eval("type") + "')"%>></asp:LinkButton>

                                                                </ItemTemplate>
                                                                <ItemStyle Width="25px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <%--##1--%>
                                                            <%--<asp:BoundField DataField="DateCreated" HeaderText="Date" SortExpression="DateCreated">--%>
                                                            <asp:BoundField DataField="DateCreated" DataFormatString="{0:d}" HeaderText="Date" SortExpression="DateCreated">
                                                                <ItemStyle Height="15px" HorizontalAlign="Center" Width="50px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="UserId" HeaderText="By" SortExpression="UserId">
                                                                <ItemStyle Height="15px" Width="80px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note">
                                                                <ItemStyle Wrap="True" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                </div>
                            </div>
                            <div class="tab-pane active" id="exampleTabsFour" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwNotices" Width="100%" runat="server" AutoGenerateColumns="False" DataKeyNames="JobNoticeHistoryId" CssClass="table dataTable table-striped">
                                                        <%-- <RowStyle CssClass="rowstyle" />
                                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <%--&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnPrntNotice" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />--%>
                                                                    <asp:LinkButton ID="btnPrntNotice" CssClass="icon ti-eye" runat="server"></asp:LinkButton>

                                                                </ItemTemplate>
                                                                <ItemStyle Width="40px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="DatePrinted" SortExpression="DatePrinted" HeaderText="Printed On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FormCode" SortExpression="FormCode" HeaderText="Form">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="LegalPartyType" SortExpression="LegalPartyType" HeaderText="Legal Party">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Cert #">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hyperlnk" runat="server" Style="cursor: pointer" Text='<%# Eval("CertNum", "({0})")%>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="MailStatus" SortExpression="MailStatus" HeaderText="Mail Status">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <b>No Items found for the specified criteria</b><td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                        </EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsFive" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div style="padding-left: 20px; text-align: left;" id="divAddDoc">
                                                <asp:Button ID="btnAddDocuments" runat="server" CssClass="btn btn-primary" CausesValidation="False" Text="Add Document" Style="display: none;" />
                                                <asp:Button ID="btnAddDocumentsNew" runat="server" OnClientClick="$('#ModalFileUpload').modal('show');document.getElementById('frmUpload').contentWindow.DeletFileUpload();return false;" CssClass="btn btn-primary" CausesValidation="False"
                                                    Text="Add Document" />
                                                <%-- <asp:Button ID="btnAddDocumentsNew" runat="server"  CssClass="btn btn-primary" CausesValidation="False"
                                                    Text="Add Document" />--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwDocs" runat="server" CssClass="table dataTable table-striped" Width="100%" PageSize="20" AutoGenerateColumns="False">
                                                        <%-- <AlternatingRowStyle CssClass="altrowstyle"></AlternatingRowStyle>--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnPrntDocs" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />--%>
                                                                    <asp:LinkButton ID="btnPrntDocs" CssClass="icon ti-eye" runat="server"></asp:LinkButton>

                                                                </ItemTemplate>
                                                                <ItemStyle Wrap="False" Width="40px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateCreated" HeaderText="Date Added" SortExpression="DateCreated">
                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="UserCode" HeaderText="User" SortExpression="UserCode">
                                                                <ItemStyle Wrap="False" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DocumentType" HeaderText="Type" SortExpression="DocumentType">
                                                                <ItemStyle Wrap="False" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DocumentDescription" HeaderText="Description" SortExpression="DocumentDescription">
                                                                <ItemStyle Wrap="True" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <b>No Items found for the specified criteria</b>
                                                        </EmptyDataTemplate>

                                                        <%--  <HeaderStyle CssClass="headerstyle"></HeaderStyle>

                                                                                            <RowStyle CssClass="rowstyle"></RowStyle>--%>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>





                                    <%--   <div style="float: left; margin-top: 30px; height: 100%;">--%>


                                    <ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1"
                                        runat="server" BehaviorID="FileUpLoad" TargetControlID="btnAddDocuments"
                                        PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" />

                                    <asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none" Width="90%" Height="100px">
                                        <h1 class="panelheader">
                                            <div style="width: 100%;">
                                                <div style="text-align: center; width: 90%; float: left;">Upload Documents</div>
                                                <div style="float: right; width: 10%;">
                                                    <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="Return" Width="40px" CssClass="btn btn-primary" />
                                                </div>
                                            </div>


                                        </h1>

                                        <iframe runat="server" id="frm" src="App_Controls/_Custom/LienView/Default.aspx?id=1 &jobid=1" style="width: 90%; height: 270px; background-color: White"></iframe>

                                    </asp:Panel>



                                    <%--   </div>--%>
                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsSix" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div style="padding-left: 20px; text-align: left;" id="divWaiver">
                                                <asp:LinkButton ID="btnAddWaiver" runat="server" Text="Create New Waiver" CssClass="btn btn-primary" OnClick="btnAddWaiver_Click" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwWaivers" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyNames="JobWaiverLogId" CssClass="table dataTable table-striped">
                                                        <%--  <RowStyle CssClass="rowstyle" />
                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit/Print/Email ">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                          <asp:LinkButton ID="btnEditWaiver" CssClass="icon ti-write" runat="server" />
                                                                    <%--     <asp:ImageButton ID="btnEditWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                                                                        &nbsp;&nbsp;
                                                                                                          <asp:LinkButton ID="btnPrntWaiver" CssClass="icon ti-printer" runat="server" />
                                                                    <%--   <asp:ImageButton ID="btnPrntWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                                                                                        &nbsp; &nbsp;
                                                                     <asp:LinkButton ID="btnEmailWaiver" CssClass="icon ti-email" runat="server" />
                                                                    &nbsp; &nbsp;
                                                                </ItemTemplate>
                                                                <ItemStyle Width="70px" Wrap="False" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="RequestedBy" SortExpression="RequestedBY" HeaderText="User">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Created On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DatePrinted" SortExpression="DatePrinted" HeaderText="Printed On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FORMDESCRIPTION" SortExpression="FORMDESCRIPTION" HeaderText="Waiver Type">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ThroughDate" SortExpression="ThroughDate" HeaderText="Through Date">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;
                                                                                                        <asp:LinkButton ID="btnDeleteWaiver" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountWaiver(this)" />
                                                                    <%--                                                                                                        <asp:ImageButton ID="btnDeleteWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane active" id="exampleTabsSeven" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div style="padding-left: 20px; text-align: left;" id="divStateForm">
                                                <asp:LinkButton ID="btnAddStateFrom" runat="server" Text="Create New State Form" CssClass="btn btn-primary" OnClick="btnAddStateFrom_Click" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwStateForms" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyNames="JobNoticeLogId" CssClass="table dataTable table-striped">
                                                        <RowStyle CssClass="rowstyle" />
                                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                                        <HeaderStyle CssClass="headerstyle" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit/Print">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                         <asp:LinkButton ID="btnEditSFORM" CssClass="icon ti-write" runat="server" />
                                                                    <%--<asp:ImageButton ID="btnEditSFORM" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                                                                        &nbsp;
                                                                                                         <asp:LinkButton ID="btnPrntSFORM" CssClass="icon ti-printer" runat="server" />
                                                                    <%--	<asp:ImageButton ID="btnPrntSFORM" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                                                                                        &nbsp; &nbsp;
                                                                </ItemTemplate>
                                                                <ItemStyle Width="68px" Wrap="False" />
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Created On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DatePrinted" SortExpression="DatePrinted" HeaderText="Printed On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FormCode" SortExpression="FormCode" HeaderText="Form Type">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;
                                                                                                         <asp:LinkButton ID="btnDeleteSForm" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountStateForm(this)" />
                                                                    <%--<asp:ImageButton ID="btnDeleteSForm" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />


                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsEight" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:LinkButton ID="btnAddVReqClient" runat="server" Text="Add New Request" CssClass="btn btn-primary" OnClientClick="return DisplayRequestNotice();"/>
                                                  <asp:LinkButton ID="btnAddVReq" runat="server" Text="Add New Request" CssClass="btn btn-primary" OnClick="btnAddVREQ_Click" Style="display:none"  />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwVReq" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table dataTable table-striped">
                                                        <%--  <RowStyle CssClass="rowstyle" />
                                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <ItemStyle Width="30px" Height="15px" />
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                            <asp:LinkButton ID="btnEditVReq" CssClass="icon ti-write" runat="server" />
                                                                    <%--<asp:ImageButton ID="btnEditVReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SubmittedBy" SortExpression="SubmittedBy" HeaderText="User">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AmountOwed" SortExpression="AmountOwed" HeaderText="Amount Owed">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                          <asp:LinkButton ID="btnDeleteVReq" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountTexas(this)" />
                                                                    <%-- <asp:ImageButton ID="btnDeleteVReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsNine" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:LinkButton ID="btnAddTXStmt" runat="server" Text="Add New Request" CssClass="btn btn-primary" OnClick="btnAddTXStmt_Click" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwTexasReq" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table dataTable table-striped" OnRowDataBound="gvwTexasReq_RowDataBound">
                                                        <%--   <RowStyle CssClass="rowstyle" />
                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit/Print">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;
                                                                   <asp:LinkButton ID="btnEditTReq" CssClass="icon ti-write" runat="server" />
                                                                    <%-- <asp:ImageButton ID="btnEditTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                                    <asp:LinkButton ID="btnPrntTReq" CssClass="icon ti-printer" runat="server" />
                                                                    <%--  <asp:ImageButton ID="btnPrntTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                                                                                        &nbsp; &nbsp;
                                                                </ItemTemplate>
                                                                <ItemStyle Width="68px" Wrap="False" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SubmittedBy" SortExpression="SubmittedBy" HeaderText="User">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IsTX60Day" SortExpression="IsTX60Day" HeaderText="2nd Month">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IsTX90Day" SortExpression="IsTX90Day" HeaderText="3rd Month">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AmountOwed" SortExpression="AmountOwed" HeaderText="Amount Owed">
                                                                <ItemStyle HorizontalAlign="Right" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MonthDebtIncurred" SortExpression="MonthDebtIncurred" HeaderText="Month(s) Debt Incurred">
                                                                <ItemStyle Height="15px" Wrap="True" />
                                                            </asp:BoundField>


                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDeleteTReq" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountTexasRequest(this)" />
                                                                    <%--<asp:ImageButton ID="btnDeleteTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />


                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsTen" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-2">
                                                <asp:LinkButton ID="btnAddNewINVC" runat="server" Text="Add New Invoice" CssClass="btn btn-primary" />
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-2 col-sm-2 col-xs-2">
                                                <asp:LinkButton ID="btnPrintINVC" runat="server" Text="Print Detail" CssClass="btn btn-primary" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwINVC" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table dataTable table-striped" OnRowDataBound="gvwINVC_RowDataBound">
                                                        <RowStyle CssClass="rowstyle" />
                                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                                        <HeaderStyle CssClass="headerstyle" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnEditINVC" CssClass="icon ti-write" runat="server" />
                                                                    <%--  <asp:ImageButton ID="btnEditINVC" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="35px" Wrap="False" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="InvoiceDate" SortExpression="InvoiceDate" HeaderText="Invoice Date">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="InvoiceRefNum" SortExpression="InvoiceRefNum" HeaderText="Invoice #">
                                                                <ItemStyle HorizontalAlign="Right" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="InvoiceAmount" SortExpression="InvoiceAmount" HeaderText="Invoice Amt">
                                                                <ItemStyle HorizontalAlign="Right" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="PmtAmount" SortExpression="PmtAmount" HeaderText="Paid">
                                                                <ItemStyle HorizontalAlign="Right" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AdjAmount" SortExpression="AdjAmount" HeaderText="Adjusted">
                                                                <ItemStyle HorizontalAlign="Right" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AmountOwed" SortExpression="AmountOwed" HeaderText="Owed">
                                                                <ItemStyle HorizontalAlign="Right" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDeleteINVC" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountInvoice(this)" />
                                                                    <%--<asp:ImageButton ID="btnDeleteINVC" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <b>No Items found for the specified criteria</b><td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                            <td class="tdhidden"></td>
                                                        </EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <%--  <ajaxToolkit:TabContainer ID="TabContainer" Width="710px" runat="server"
                                                            CssClass="tabs" ActiveTabIndex="1">
                                                         
                                                            <ajaxToolkit:TabPanel ID="tabPanel1" runat="server" HeaderText="Main Info">
                                                                <ContentTemplate>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Legal Parties">
                                                                <ContentTemplate>
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="Notes">
                                                                <ContentTemplate>
                                                                </ContentTemplate>

                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" HeaderText="Notices Sent">
                                                                <ContentTemplate>
                                                                  
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel10" runat="server" HeaderText="Docs">

                                                                <ContentTemplate>


                                                             

                                                                </ContentTemplate>

                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel5" runat="server" HeaderText="Waivers">
                                                                <ContentTemplate>
                                                                  
                                                                </ContentTemplate>

                                                            </ajaxToolkit:TabPanel>

                                                            <ajaxToolkit:TabPanel ID="TabPanel6" runat="server" HeaderText="State Forms">
                                                                <ContentTemplate>
                                                                
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel7" runat="server" HeaderText="Notice Request">
                                                                <ContentTemplate>
                                                               
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel8" runat="server" HeaderText="TX Requests">
                                                                <ContentTemplate>
                                                                   
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                            <ajaxToolkit:TabPanel ID="TabPanel9" runat="server" HeaderText="Invoices">
                                                                <ContentTemplate>
                                                              
                                                                </ContentTemplate>
                                                            </ajaxToolkit:TabPanel>
                                                        </ajaxToolkit:TabContainer>--%>
                </div>
                <div class="footer">
                    <table width="100%" style="display: none">
                        <tr>

                            <td align="right">

                                <asp:ImageButton ID="btnFirst" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_First.png" OnClick="btnFirst_Click" />

                                <asp:ImageButton ID="btnPrevious" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_Previous.png" OnClick="btnPrevious_Click" />

                                <asp:ImageButton ID="btnNext" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_Next.png" OnClick="btnNext_Click" />

                                <asp:ImageButton ID="btnLast" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_Last.png" OnClick="btnLast_Click" />

                            </td>

                        </tr>
                    </table>
                </div>

            </asp:Panel>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <asp:Button ID="btnExitViewer" runat="server" CssClass="btn btn-primary" Text="Exit Viewer" /><%--<asp:LinkButton ID="btnExitViewer" runat="server" CssClass="button" Text="Exit Viewer" ></asp:LinkButton>--%>

                <asp:Button ID="btnDownLoad" Text="DownLoad" CssClass="btn btn-primary" runat="server" /><%--<asp:LinkButton ID="btnDownLoad" Text="DownLoad" CssClass="button" runat="server"  />--%>

                <br />
                <br />

                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
            </div>
        </asp:View>
    </asp:MultiView>
    <asp:HiddenField ID="hdnWaiverId" runat="server" Value="0" />
    <asp:Button ID="btnDeleteWaiverId" runat="server" Style="display: none;" OnClick="btnDeleteWaiverId_Click" />
    <asp:HiddenField ID="hdnStateFormId" runat="server" Value="0" />
    <asp:Button ID="btnStateFormId" runat="server" Style="display: none;" OnClick="btnStateFormId_Click" />
    <asp:HiddenField ID="hdnTXReqId" runat="server" Value="0" />
    <asp:Button ID="btnTXReqId" runat="server" Style="display: none;" OnClick="btnTXReqId_Click" />
    <asp:HiddenField ID="hdnTexasRequest" runat="server" Value="0" />
    <asp:Button ID="btnTexasRequest" runat="server" Style="display: none;" OnClick="btnTexasRequest_Click" />
    <asp:HiddenField ID="hdnInvoiceid" runat="server" Value="0" />
    <asp:Button ID="btnInvoiceid" runat="server" Style="display: none;" OnClick="btnInvoiceid_Click" />
    <div class="modal fade modal-primary" id="ModalFileUpload" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
        tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-center">
            <div class="modal-content model-size">
                <div class="modal-header">
                      <%--^^*** commented by pooja 10/09/2020*** ^^--%>
                    <%--<button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick ="document.getElementById('frmUpload').contentWindow.DeletFileUpload();">
                        <span aria-hidden="true">×</span>
                    </button>--%>
                     <%--'^^***  *** ^^ --%>
                      
                      <%--^^*** Created by pooja 10/15/2020*** ^^--%>
                    <button type="button" class="close" OnClick ="javascript:Clear()" aria-label="Close">
                    <span aria-hidden="true">×</span>
                    </button>
                    
                       <%--'^^***  *** ^^  --%>
                    <h4 class="modal-title">UPLOAD DOCUMENT</h4>
                </div>
                <iframe id="frmUpload" src="App_Controls/_Custom/LienView/Default.aspx?id=1 &jobid=1" style="height: 540px; width: 550px; border: 0px;"></iframe>
                <div class="modal-footer">
                    <%--^^*** commented by pooja 10/15/2020*** ^^--%>
                   <%-- <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close" onclick ="document.getElementById('frmUpload').contentWindow.DeletFileUpload();">
                        <span aria-hidden="true">CLOSE</span>
                    </button>--%>

                       <%--'^^***  *** ^^ --%>


                            <%--^^*** Created by pooja 10/09/2020*** ^^--%>
                  <button type="button" class="btn btn-default margin-0" OnClick ="javascript:Clear()" aria-label="Close">
                                                <span aria-hidden="true">CLOSE</span> </button>

                       <%--'^^***  *** ^^  --%>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-primary" id="ModalJobNoteDetail" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose11">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Note Detail</h4>

                </div>
                <div class="modal-body">
                    <%--  <div id="modcontainer" style="MARGIN: 0px  0px 0px 0px; width: 100%;">--%>

                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <asp:TextBox ID="txtNotes" runat="server" Height="402px" Width="100%" TextMode="MultiLine"
                                        CssClass="textbox" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>

                    <%-- </div>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-primary example-modal-lg" id="ModaldivAdditionalInfo" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
        tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">ADDITIONAL INFO</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">APN:</label>
                            <div class="col-md-5  col-sm-5 col-xs-5">
                                <cc2:DataEntryBox ID="APNNum" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">PO#:</label>
                            <div class="col-md-5  col-sm-5 col-xs-5">
                                <cc2:DataEntryBox ID="PONum" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Permit#:</label>
                            <div class="col-md-5  col-sm-5 col-xs-5">
                                <cc2:DataEntryBox ID="BuildingPermitNum" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Bond#:</label>
                            <div class="col-md-5  col-sm-5 col-xs-5">
                                <cc2:DataEntryBox ID="BondNum" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Filter Key</label>
                            <div class="col-md-5  col-sm-5 col-xs-5">
                                <cc2:DataEntryBox ID="FilterKey" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Equip Rate:</label>
                            <div class="col-md-5  col-sm-5 col-xs-5  col-sm-5 col-xs-5">
                                <cc2:DataEntryBox ID="EquipRate" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Equip Descr:</label>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <cc2:DataEntryBox ID="EquipRental" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TextMode="MultiLine"></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Special Inst:</label>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <cc2:DataEntryBox ID="SpecialInstruction" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TextMode="MultiLine"></cc2:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable align-lable">Legal Descr:</label>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <cc2:DataEntryBox ID="Note" runat="server" CssClass="form-control" DataType="Any" ReadOnly="true"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""
                                    TextMode="MultiLine"></cc2:DataEntryBox>

                                <%--     <cc1:DataEntryBox ID="SpecialInstruction" runat="server" CssClass="form-control"
                                                Tag="Special Instruction" IsRequired="False" Height="60px" TextMode="MultiLine"
                                                TabIndex="53" BackColor="White" BlankOnZeroes="True" DataType="Any" ErrorMessage=""
                                                IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False"
                                                FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc1:DataEntryBox>--%>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <%-- <asp:Button ID="btnSaveAdditionalInfo" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSaveAdditionalInfo_Click" />--%>
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">CLOSE</span>
                    </button>

                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBatchJobNoteId" runat="server" />
    <asp:HiddenField ID="hdnBatchJobNoteType" runat="server" />
    <asp:Button ID="btnNoteDetail" runat="server" OnClick="btnNoteDetail_Click" Style="display: none;" />
    <asp:HiddenField ID="hdnDocId" runat="server" Value="0" />
    <asp:Button ID="btnDeleteDoc" runat="server" Style="display: none;" OnClick="btnDeleteDoc_Click" />
</div>
<div class="modal fade modal-primary" id="divModalEmail" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose21" onclick="RemoveBackdrop();">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Send Email</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4">To:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtToList" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4"></div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <label style="color: red; text-align: left;">Note:- Multiple email ids can be added separated by semicolon</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4">Subject:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" Text="New Waiver Document"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4 col-sm-4 col-xs-4">Message:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtBody" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblErrorMsg" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--<asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Send" Style="display: none;" />--%>
                <button type="button" class="btn btn-primary" id="btnSaves1" onclick="SendMail();">
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnEmailsend" runat="server" OnClick="btnEmailsend_Click" Style="display: none;" />



<%--Recension Letter Modal--%>
<div class="modal fade modal-primary example-modal-lg" id="ModalRecensionLetterSubmit" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="RemoveBackdrop();">
                    <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title">Recension Letter Created</h3>
            </div>
            <div class="modal-body">
                <div id ="RecensionLetterModaldiv"></div>
            </div>
            <div class="modal-footer" style="text-align: center">
            <asp:Button ID="btnEmail" runat="server" Text="Email Letter" CssClass="btn btn-primary" OnClick="btnEmailRecensionLetter_Click" />
                <asp:Button ID="btnPrint" runat="server" Text="Print Letter" CssClass="btn btn-primary" OnClick="btnPrintRecensionLetter_Click" />
               <button type="button" class="btn btn-primary"  data-dismiss="modal" onclick="RemoveBackdrop();">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="divModalEmailRecensionLetter" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="RemoveBackdrop();">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Send Email</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                          <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-sm-12 col-xs-12" style="text-align: left;">
                            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-md-4">To:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtToListForRecensionLetter" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <label style="color: red; text-align: left;">Note:- Multiple email ids can be added separated by semicolon</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Subject:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtRecensionLetterSubject" runat="server" CssClass="form-control" Text="New Waiver Document"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Message:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtRecensionLetterBody" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <asp:Label ID="Label2" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="SendRecensionLetterMail();">
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="return fadeout();">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hdnEmailflag" runat="server" />
<asp:HiddenField ID="hdnLegalPartyId" runat="server" />
<asp:HiddenField ID="hdnJobLegalPartiesTypeCode" runat="server" />
<asp:Button ID="btnCreateAnotherRecensionLetter" runat="server" OnClick="btnCreateAnotherRecensionLetter_Click" Style="display: none;" />
