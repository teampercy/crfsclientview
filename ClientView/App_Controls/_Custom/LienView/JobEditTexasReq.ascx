<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script runat="server">
   dim childid as String = "-1"
    Dim jobid As String = ""
   Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
   Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
   Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
       
        If qs.HasParameter("action") Then
            Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")
            If s.Length > 1 Then
                childid = s(1)
            End If
            If Page.IsPostBack = False And s(0) = "vtxreq" Then
                LoadData(jobid, childid)
            End If
        End If
        
    End Sub
   
    Public Sub LoadData(ByVal ajobid As String, Optional ByVal childid As String = "-1")
        Me.btnDelete.Visible = False
        If childid > 1 Then
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(childid, myreq)
            Me.btnDelete.Visible = True
        Else
            myreq.IsTX60Day = True
        End If
        With myreq
            Me.AmountOwed.Value = .AmountOwed
            Me.IsTX60Day.Checked = .IsTX60Day
            Me.IsTX90Day.Checked = .IsTX90Day
            Me.MonthDebtIncurred.Value = .MonthDebtIncurred
            Me.StmtofAccts.Text = .StmtOfAccts
        End With
    
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        if page.IsValid = False then exit Sub
        Me.CustomValidator1.ErrorMessage = Validate()
        If Me.Message.Text.Length > 1 Then
            Me.CustomValidator1.IsValid = False
            Exit Sub
        End If
        
        With myreq
            .Id = childid
            .JobId = jobid
            .DateCreated = Now()
            .AmountOwed = Me.AmountOwed.Value
            .IsTX60Day = Me.IsTX60Day.Checked
            .IsTX90Day = Me.IsTX90Day.Checked
            .MonthDebtIncurred = Me.MonthDebtIncurred.Value
            .StmtOfAccts = Me.StmtofAccts.Text
            '    .SubmittedBy = Strings.Left(Me.CurrentUser.username, 10)
            .SubmittedByUserId = Me.CurrentUser.id
        End With
        
        If myreq.Id > 0 Then
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
        Else
            myreq.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myreq)
        End If
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
       
        qs.SetParameter("action", "vtxreq")
        Response.Redirect(qs.All, True)
 
    End Sub
   
    Private Function Validate() as string
    
        Dim MYSQL As String = " SELECT * FROM JOBTEXASREQUEST WHERE DATEPROCESSED Is NULL"
        MYSQL += " And JobId = " & myreq.JobId
        MYSQL += " And Id <> " & myreq.Id
        If Me.IsTX60Day.Checked = True Then
            MYSQL += " And IsTx60day = 1"
        Else
            MYSQL += " And IsTx90day = 1"
        End If
        
        Dim MYVIEW As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSQL)
        If MYVIEW.Count > 0 Then
            If Me.IsTX60Day.Checked = True Then
                return "You already have a 60 DAY request for this Job"
            Else
                return "You already have a 90 DAY request for this Job"
            End If
        End If

        If myjob.PublicJob = True And Me.IsTX90Day.Checked And Me.StmtofAccts.Text.Length < 4 Then
            return "A Statement of account is required for this request"
        End If
        
        ' 8/10/2015 - LEC   
        If myjob.PrivateJob = True And myjob.ResidentialBox = True And Me.IsTX60Day.Checked Then
            Return "The 2nd Month letter should not be used for a Residential job in Texas. Please request the 3rd Month letter in order to maintain your lien rights."
        End If
        return string.Empty
        
    end function
</script>
<h2>Texas Request</h2>
<asp:Panel ID="panForm" runat="server">
<div class="body"  >
<TABLE align="center" style="WIDTH: 600px">
			<TR>
				<TD class="row-label" style="WIDTH: 200px">Notice Type:</TD>
				<TD class="row-data">
					<asp:RadioButton id="IsTX60Day" runat="server" Text="60 day" GroupName="NoticeType" Width="60px"
						Cssclass="row-label" Checked="True"></asp:RadioButton>
					<asp:RadioButton id="IsTX90Day" runat="server" Text="90 day" GroupName="NoticeType" Width="60px"
						Cssclass="row-label"></asp:RadioButton>
				</TD>
			</TR>
			<TR>
				<TD class="row-label" style="WIDTH: 200px">Amount Owed:</TD>
				<TD class="row-data">
					<cc1:DataEntryBox id="AmountOwed" runat="server" Width="136px" DataType="Money"></cc1:DataEntryBox>
				</TD>
			</TR>
			<TR>
				<TD class="row-label" style="WIDTH: 200px">Month Worked Performed:</TD>
				<TD class="row-data">
					<cc1:DataEntryBox id="MonthDebtIncurred" runat="server" Width="136px" DataType="Any" MaxLength="50"></cc1:DataEntryBox>
				</TD>
			</TR>
		</TABLE>
		<TABLE align="center" style="WIDTH: 600px">
		<TR>
		<TD style="width: 587px;">
		    <H2>Enter Account Statement Info:</H2>
             <cc1:DataEntryBox ID="StmtofAccts" runat="server" DataType="Any" Height="200px" Tag="StmtofAccts"
                    TextMode="MultiLine" Width="592px"></cc1:DataEntryBox>
        </TD>
		</TR>
		</TABLE>
<br/>
<cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
<asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
</div>
<div class="footer">
    <div class="savebtns" style="float :left" >
        &nbsp;<asp:LinkButton ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:LinkButton>
    </div>
    <div class="deletebtns" style="float :right" >
    <asp:LinkButton ID="btnDelete" runat="server" CssClass="button" Text="Delete"></asp:LinkButton>
    </div>
    <br />
    <br />
</div>
</asp:Panel>
