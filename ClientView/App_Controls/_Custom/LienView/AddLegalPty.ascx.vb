
Partial Class App_Controls_crfLienView_AddLegalPty
    Inherits System.Web.UI.UserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim MYJOB As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
    Dim MYITEM As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJobLegalParties
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo

    Public Sub ClearData(ByVal abatchjobid As String)
        Me.ViewState("BatchJobId") = abatchjobid
        Me.ViewState("ItemId") = -1
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(abatchjobid, MYJOB)
        mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(MYJOB.JobState)
        MYITEM = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJobLegalParties
        MYITEM.TypeCode = "GC"

        '4/10/13
        If mystate.MLAgent = True Then
            Me.MLAGENT.Visible = True
        Else
            Me.MLAGENT.Visible = False
        End If
        'Me.MLAGENT.Checked = False

        'Me.RadioButtonList1.Items.Clear()
        'Dim myli As New System.Web.UI.WebControls.ListItem
        'myli = New System.Web.UI.WebControls.ListItem
        'myli.Value = "CU"
        'myli.Text = "Customer"
        'Me.RadioButtonList1.Items.Add(myli)

        'myli = New System.Web.UI.WebControls.ListItem
        'myli.Value = "GC"
        'myli.Text = "Gen Contractor"
        'Me.RadioButtonList1.Items.Add(myli)

        ' Me.MLAgent.Visible = True
        'If MYJOB.JobState = "VA" And MYITEM.TypeCode = "OW" And MYJOB.MLAgent = False Then
        '    'myli = New System.Web.UI.WebControls.ListItem
        '    'myli.Value = "MLAGENT"
        '    'myli.Text = "ML Agent"
        '    'Me.RadioButtonList1.Items.Add(myli)
        '    Me.MLAgent.Visible = True
        'End If

        'myli = New System.Web.UI.WebControls.ListItem
        'myli.Value = "LE"
        'myli.Text = "Surety Lender"
        'Me.RadioButtonList1.Items.Add(myli)

        LoadItem()

    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim s As String = "1"

    End Sub
    Public Sub EditItem(ByVal abatchjobid As String, ByVal aitemid As String)

        Me.ViewState("BatchJobId") = abatchjobid
        Me.ViewState("ItemId") = aitemid

        ItemId.Value = aitemid
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(abatchjobid, MYJOB)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(aitemid, MYITEM)
        LoadItemInModal()
        'LoadItem()
        'Me.ModalPopupExtender1.Show()

    End Sub
    Private Sub LoadItem()
        

        If mystate.MLAgent = True Then
            Me.MLAGENT.Visible = True
        Else
            Me.MLAGENT.Visible = False
        End If

        With MYITEM
            Me.LPAdd1.Text = .AddressLine1
            Me.LPAdd2.Text = .AddressLine2
            Me.RefNum.Text = .RefNum
            Me.LPCity.Text = .City
            Me.LPName.Text = .AddressName
            Me.LPState.Text = .State
            Me.LPZip.Text = .PostalCode
            Me.LPPhone.Text = Utils.FormatPhoneNo(.Telephone1)
            Me.LPFax.Text = Utils.FormatPhoneNo(.Fax)
            Me.RadioButtonList1.SelectedValue = .TypeCode
            Me.MLAGENT.Checked = False
            If .MLAgent = True Then
                Me.RadioButtonList1.SelectedValue = "OW"
                Me.MLAGENT.Checked = True
            End If
        End With

      

    End Sub

    Private Sub LoadItemInModal()
        If mystate.MLAgent = True Then
            Me.chkMLAgent.Visible = True
        Else
            Me.chkMLAgent.Visible = False
        End If

        With MYITEM
            Me.partyAddLine1.Text = .AddressLine1
            Me.partyAddLine2.Text = .AddressLine2
            Me.partyBondNum.Text = .RefNum
            Me.partyCity.Text = .City
            Me.partyName.Text = .AddressName
            Me.partyState.Text = .State
            Me.partyZip.Text = .PostalCode
            Me.partyPhone.Text = Utils.FormatPhoneNo(.Telephone1)
            Me.partyFax.Text = Utils.FormatPhoneNo(.Fax)
            Me.RadioButtonList2.SelectedValue = .TypeCode
            Me.MLAGENT.Checked = False
            If .MLAgent = True Then
                Me.RadioButtonList2.SelectedValue = "OW"
                Me.chkMLAgent.Checked = True
            End If
        End With
    End Sub

    Private Sub EnforceRules()
        Select Case Me.RadioButtonList1.SelectedValue
            Case "CU", "LE"
                '     Me.lblRefNUm.Visible = True
                Me.RefNum.Visible = True
                If Me.RadioButtonList1.SelectedValue = "CU" Then
                    '        Me.lblRefNUm.Text = "Reference#"
                Else
                    '       Me.lblRefNUm.Text = "Bond#"
                End If
            Case Else
                '  Me.lblRefNUm.Visible = False
                Me.RefNum.Visible = False
                Me.RefNum.Text = ""
        End Select
    End Sub
    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Me.ViewState("BatchJobId"))

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        With MYITEM
            .BatchJobId = Me.ViewState("BatchJobId")
          
            .Id = Me.ViewState("ItemId")
            .TypeCode = Me.RadioButtonList1.SelectedValue
            .AddressLine1 = Me.LPAdd1.Text
            .AddressLine2 = Me.LPAdd2.Text
            .City = Me.LPCity.Text
            .AddressName = Me.LPName.Text
            .State = Me.LPState.Text
            .PostalCode = Me.LPZip.Text
            .Telephone1 = Utils.GetPhoneNo(Me.LPPhone.Text)
            .Fax = Utils.GetPhoneNo(Me.LPFax.Text)
            .RefNum = Me.RefNum.Value
            .IsPrimary = 0
            .MLAgent = False
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(.BatchJobId, MYJOB)
            mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(MYJOB.JobState)
            'If MYJOB.JobState = "VA" And MYITEM.TypeCode = "OW" And MYJOB.MLAgent = False Then
            .MLAgent = Me.MLAGENT.Checked
            '.TypeCode = "OW"

        End With

        If MYITEM.Id > 0 Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYITEM)
        Else
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(MYITEM)
        End If

        RaiseEvent ItemSaved()

    End Sub

    Protected Sub btnAddPArty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddParty.Click
        With MYITEM
            .BatchJobId = Me.ViewState("BatchJobId")

            .Id = Me.ViewState("ItemId")
            .TypeCode = Me.RadioButtonList2.SelectedValue
            .AddressLine1 = Me.partyAddLine1.Text
            .AddressLine2 = Me.partyAddLine2.Text
            .City = Me.partyCity.Text
            .AddressName = Me.partyName.Text
            .State = Me.partyState.Text
            .PostalCode = Me.partyZip.Text
            .Telephone1 = Utils.GetPhoneNo(Me.partyPhone.Text)
            .Fax = Utils.GetPhoneNo(Me.partyFax.Text)
            .RefNum = Me.partyBondNum.Text
            .IsPrimary = 0
            .MLAgent = False
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(.BatchJobId, MYJOB)
            mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(MYJOB.JobState)
            'If MYJOB.JobState = "VA" And MYITEM.TypeCode = "OW" And MYJOB.MLAgent = False Then
            .MLAgent = Me.chkMLAgent.Checked
            '.TypeCode = "OW"

        End With

        If MYITEM.Id > 0 Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYITEM)
        Else
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(MYITEM)
        End If

        RaiseEvent ItemSaved()
    End Sub

   
   
End Class
