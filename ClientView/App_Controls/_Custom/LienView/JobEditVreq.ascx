<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobEditVreq.ascx.vb" Inherits="App_Controls__Custom_LienView_JobEditVreq" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script type="text/javascript">
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('LienView', 'Jobs', 'View Jobs');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubJobList');
        //$('#liJobs').addClass('site-menu-item has-sub active open');
        //$('#lisubJobList').addClass('site-menu-item active');
        return false;

    }
    function hideDivTN()
    {
        $('#DivTN').hide();
    }
    function hideDivNotTN() {
        $('#DivNotTN').hide();
    }

</script>


<div id="modcontainer" style="margin: 10px; width: 100%;">
    <h1>
        <asp:Literal ID="litJobInfo" runat="server"></asp:Literal></h1>
    <div class="body">
        <div class="form-horizontal">
            <div class="form-group">
                <div class="col-md-2  col-sm-2 col-xs-2">
                    <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to Job Info" CausesValidation="False" />
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label">Amount Owed:</label>
                <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                    <cc1:DataEntryBox ID="AmountOwed" runat="server" DataType="Money" CssClass="form-control"></cc1:DataEntryBox>
                </div>
            </div>
            <div id="DivTN">
                <div class="form-group">
                    <label class="col-md-4 col-sm-4 col-xs-4 control-label">Last Date Work Performed :</label>
                    <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                        <cc1:dataentrybox id="TNLastDateWorkPerformed" runat="server" cssclass="form-control datepicker" datatype="DateValue"
                            backcolor="White" errormessage="" normalcss="form-control"
                            requiredcss="requiredtextbox" tag="EndDate" value="12:00:00 AM" enableclientsidescript="False" formatmask=""
                            formatstring="{0:dd/MM/yyyy}" friendlyname=""
                            validationexpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc1:dataentrybox>


                    </div>
                </div>
            </div>
            <div id="DivNotTN">
                <div class="form-group">
                    <label class="col-md-12 col-sm-12 col-xs-12 control-label" style="text-align: left;">
                        <h3>Enter Account Statement Info:</h3>
                    </label>

                </div>
                <div class="form-group">

                    <div class="col-md-9 col-sm-9 col-xs-9" style="text-align: left;">
                        <cc1:DataEntryBox ID="StmtofAccts" runat="server" DataType="Any" Height="150px" Tag="StmtofAccts"
                            TextMode="MultiLine" CssClass="form-control"></cc1:DataEntryBox>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-4 col-sm-4 col-xs-4 control-label">
                    <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                    <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label></label>

            </div>
        </div>



    </div>
    <div class="footer">
        <asp:UpdatePanel ID="updPan1" runat="server">
            <ContentTemplate>
                <asp:LinkButton ID="btnSAVE"
                    CssClass="btn btn-primary"
                    runat="server"
                    Text="Submit" CausesValidation="False" />&nbsp;
                                                    <br />
                <br />

                <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                    <ProgressTemplate>
                        <div class="TransparentGrayBackground"></div>
                        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                            <div class="PageUpdateProgress">
                                <asp:Image ID="ajaxLoadNotificationImage"
                                    runat="server"
                                    ImageUrl="~/images/ajax-loader.gif"
                                    AlternateText="[image]" />
                                &nbsp;Please Wait...
                            </div>
                        </asp:Panel>
                        <ajaxToolkit:AlwaysVisibleControlExtender
                            ID="AlwaysVisibleControlExtender1"
                            runat="server"
                            TargetControlID="alwaysVisibleAjaxPanel"
                            HorizontalSide="Center"
                            HorizontalOffset="150"
                            VerticalSide="Middle"
                            VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
