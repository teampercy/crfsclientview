﻿Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Partial Class App_Controls__Custom_LienView_NewJob
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim mylieninfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientLienInfo
    Dim myclient As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Client

    Protected WithEvents btnDeleteLP As ImageButton
    Protected WithEvents btnEditLP As LinkButton
    Protected WithEvents btnPrntDocs As ImageButton
    Private Shared _IsExit As Boolean

    Public Shared Property IsexitViewer() As Boolean
        Get
            Return _IsExit
        End Get
        Set(ByVal value As Boolean)
            _IsExit = value
        End Set
    End Property

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)

        If Me.Page.IsPostBack = False Then

            Me.MultiView1.SetActiveView(Me.View1)

            Dim li As New ListItem
            myitem.JobState = "CA"

            Dim VWSTATES As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateTable
            VWSTATES.MoveFirst()
            Do Until VWSTATES.EOF
                li = New ListItem
                mystate = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
                mystate = VWSTATES.FillEntity(mystate)
                li.Value = mystate.StateInitials
                li.Text = mystate.StateName
                Me.StateTableId.Items.Add(li)
                VWSTATES.MoveNext()
            Loop

            Dim VWCLIENTS As HDS.DAL.COMMON.TableView = LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
            VWCLIENTS.MoveFirst()
            VWCLIENTS.Sort = "ClientName"
            Do Until VWCLIENTS.EOF
                li = New ListItem
                li.Value = VWCLIENTS.RowItem("ClientId")
                li.Text = VWCLIENTS.RowItem("ClientName")
                Me.ClientTableId.Items.Add(li)
                VWCLIENTS.MoveNext()
            Loop

            If qs.GetParameter("ItemId") > 0 Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("ItemId"), myitem)
                Me.ViewState("BatchJobId") = myitem.Id
            Else
                myitem = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
                myitem.BatchId = -2
                myitem.ClientId = Me.UserInfo.UserInfo.ClientId
                myitem.JobState = "CA"
                myitem.SubmittedOn = Now()
                myitem.LastUpdateOn = Now()
                myitem.SubmittedByUserId = Me.CurrentUser.Id
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myitem)
                Me.ViewState("BatchJobId") = myitem.Id

                Session("BatchJobId") = myitem.Id

                Dim myuser As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users
                If IsNothing(Me.Session("JobState")) = True Then
                    CRFVIEW.Globals.DBO.Read(Me.CurrentUser.Id, myuser)
                    If myuser.LastJobState.Length > 1 Then
                        Me.StateTableId.SelectedValue = myuser.LastJobState
                        Me.ClientTableId.SelectedValue = myuser.LastClientId
                        myitem.JobState = Me.StateTableId.SelectedValue
                        myitem.BranchNum = Me.Session("BranchNum")
                    End If
                Else
                    Me.StateTableId.SelectedValue = Me.Session("JobState")
                    Me.ClientTableId.SelectedValue = Me.Session("ClientTableId")
                    myitem.ClientId = Me.ClientTableId.SelectedValue
                    myitem.JobState = Me.StateTableId.SelectedValue
                    myitem.BranchNum = Me.Session("BranchNum")
                End If
            End If

            LoadData()

        End If

    End Sub

    Private Sub LoadData()

        Me.MLAgent.Visible = False
        Me.MLAgent.Checked = False

        Me.StateTableId.SelectedValue = myitem.JobState
        ' Me.StateTableId.DataBind()

        Me.ClientTableId.SelectedValue = myitem.ClientId
        ' Me.ClientTableId.DataBind()

        Me.JobAddr1.Text = myitem.JobAdd1
        Me.JobAddr2.Text = myitem.JobAdd2
        Me.JobCity.Text = myitem.JobCity
        Me.JobName.Text = myitem.JobName
        Me.JobNum.Text = myitem.JobNum
        Me.StateTableId.SelectedValue = myitem.JobState
        Me.JobZip.Text = myitem.JobZip
        Me.JointCk.Checked = myitem.JointCk
        Me.EstBalance.Value = myitem.EstBalance
        Me.APNNUM.Text = myitem.APNNum
        Me.PONUM.Text = myitem.PONum
        Me.FolioNUm.Text = myitem.FolioNum
        Me.BranchNum.Text = myitem.BranchNum
        Me.PrelimBox.Checked = myitem.PrelimBox
        Me.PrelimAsIS.Checked = myitem.PrelimASIS
        Me.RushOrder.Checked = myitem.RushOrder
        ' 07/22/2015
        Me.ResidentialBox.Checked = myitem.ResidentialBox
        ' Me.OwnrSameAs.Checked = myitem.ResidentialBox

        Me.GreenCard.Checked = myitem.GreenCard
        Me.PublicJob.Checked = myitem.PublicJob
        Me.FederalJob.Checked = myitem.FederalJob
        Me.VerifyJob.Checked = myitem.VerifyJob
        Me.VerifyJobASIS.Checked = myitem.TitleVerifiedBox
        Me.SuretyBox.Checked = myitem.SuretyBox
        Me.NOCBox.Checked = myitem.NOCBox

        'If Me.StateTableId.SelectedValue = "TX" Then
        '    Me.PrelimAsIS.Checked = myitem.TitleVerifiedBox
        '    '         Me.PrelimBox.Checked = myitem.VerifyJob
        'End If


        Me.CustAdd1.Text = myitem.CustAdd1
        Me.CustAdd2.Text = myitem.CustAdd2

        Me.CustCity.Text = myitem.CustCity
        Me.CustName.Text = Left(myitem.CustName, 50)
        Me.CustRef.Text = myitem.CustRefNum
        Me.CustState.Text = myitem.CustState
        Me.CustZip.Text = myitem.CustZip

        Me.CustPhone.Text = Utils.FormatPhoneNo(myitem.CustPhone1)
        Me.CustFax.Text = Utils.FormatPhoneNo(myitem.CustFax)
        Me.CustJobNum.Text = myitem.CustJobNum
        Me.CustEmail.Text = myitem.CustEmail
        Me.ViewState("custid") = myitem.CustId

        Me.GCAdd1.Text = myitem.GCAdd1
        Me.GCAdd2.Text = myitem.GCAdd2

        Me.GCCity.Text = myitem.GCCity
        Me.GCName.Text = Left(myitem.GCName, 50)
        Me.GCRef.Text = myitem.GCRefNum
        Me.GCState.Text = myitem.GCState
        Me.GCZip.Text = myitem.GCZip
        Me.GCCity.Text = myitem.GCCity
        Me.GCEmail.Text = myitem.GCEmail
        Me.GCPhone.Text = Utils.FormatPhoneNo(myitem.GCPhone1)
        Me.GCFax.Text = Utils.FormatPhoneNo(myitem.GCFax)
        Me.ViewState("gcid") = myitem.GenId

        Me.OwnrAdd1.Text = myitem.OwnrAdd1
        Me.OwnrAdd2.Text = myitem.OwnrAdd2
        Me.OwnrCity.Text = myitem.OwnrCity
        Me.OwnrName.Text = Left(myitem.OwnrName, 50)
        Me.OwnrState.Text = myitem.OwnrState
        Me.OwnrZip.Text = myitem.OwnrZip
        Me.OwnrPhone.Text = Utils.FormatPhoneNo(myitem.OwnrPhone1)
        Me.OwnrFax.Text = Utils.FormatPhoneNo(myitem.OwnrFax)
        Me.MLAgent.Checked = myitem.MLAgent

        Me.LenderAdd1.Text = myitem.LenderAdd1
        Me.LenderAdd2.Text = myitem.LenderAdd2
        Me.LenderCity.Text = myitem.LenderCity
        Me.LenderName.Text = Left(myitem.LenderName, 50)
        Me.LenderState.Text = myitem.LenderState
        Me.LenderZip.Text = myitem.LenderZip
        Me.LenderPhone.Text = Utils.FormatPhoneNo(myitem.LenderPhone1)
        Me.LenderFax.Text = Utils.FormatPhoneNo(myitem.LenderFax)

        Me.BondNum.Text = myitem.BondNum
        Me.BuildingPermitNum.Text = myitem.BuildingPermitNum

        Me.EquipRate.Value = myitem.EquipRate
        Me.EquipRental.Value = myitem.EquipRental
        Me.RANum.Value = myitem.RANum
        Me.Note.Text = myitem.Note
        Me.SpecialInstruction.Text = myitem.SpecialInstruction
        Me.AZLotAllocate.Text = myitem.AZLotAllocate

        Me.GCSameAsCust.Checked = myitem.SameAsCust
        Me.OwnrSameAs.Checked = myitem.SameAsGC
        Me.StartDate.Value = Utils.FormatDate(myitem.StartDate)
        Me.PrelimBox.Checked = True
        Me.CustNameSearch1.ClearData()
        Me.GCSearch1.ClearData()
        Me.TabContainer1.ActiveTab = Me.TabPanel1

        LoadLegalParties(myitem.Id)

        EnforceRules()

        Me.AddLegalPty1.ClearData(myitem.Id)
        Me.CustNameSearch1.ClearData()
        Me.GCSearch1.ClearData()
        Me.CustNameSearch1.SelectClientId = Me.ClientTableId.SelectedValue
        Me.GCSearch1.SelectClientId = Me.ClientTableId.SelectedValue
        'markparimal-uploadfile:BatchJobId value is taken in session so that it can be used in file upload page.
        Session("BatchJobId") = myitem.Id

        'markparimal-uploadfile:To fill Grid on page load .
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where BatchJobId = " & myitem.Id
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()

        ' 9/17/2014
        StateValidation()
        GetStateAlerts()
    End Sub

    Public Sub StateValidation()
        If Me.StateTableId.SelectedValue = "TX" Then
            Me.StartDate.IsRequired = False
            Me.PrelimBox.Enabled = False
            Me.PrelimASIS.Enabled = False

            Me.PrelimBox.Checked = False
            Me.PrelimASIS.Checked = False
            Me.VerifyJobASIS.Checked = False

            If Me.PrelimASIS.Checked = False And Me.VerifyJobASIS.Checked = False Then
                Me.VerifyJob.Checked = True
            End If
        Else
            Me.StartDate.IsRequired = True
            Me.PrelimBox.Enabled = True
            Me.PrelimASIS.Enabled = True

            '    Me.PrelimASIS.Checked = False
            '    Me.VerifyJobASIS.Checked = False
            '    Me.VerifyJob.Checked = False

            '    If Me.PrelimASIS.Checked = False And Me.VerifyJob.Checked = False And Me.VerifyJobASIS.Checked = False Then
            '        Me.PrelimBox.Checked = True
            '    End If
        End If
    End Sub
    Public Sub LoadLegalParties(ByVal abatchjobId As String)
        Me.gvwAddLegal.DataSource = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobLegalParties(abatchjobId)
        Me.gvwAddLegal.DataBind()
        Me.AddLegalPty1.ClearData(Me.ViewState("BatchJobId"))

    End Sub
    Public Sub GetStateAlerts()

        'markparimal-State Alert:To Get State Alert For Selected State.
        'Dim crfs As New CRFS.ClientView.CrfsBll

        'CRFS.GetStateAlertByStateInitials(StateTableId.SelectedValue)
        'txtstatealert.Text = crfs.StateAlert
        mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(Me.StateTableId.SelectedValue)
        txtstatealert.Text = mystate.StateAlert
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Me.MultiView1.ActiveViewIndex > 0 Then Exit Sub

        EnforceRules()

        Me.Page.Validate()
        If Me.Page.IsValid = False Then
            Me.ValidationSummary1.Visible = True
            Exit Sub
        End If

        Me.customvalidator1.ErrorMessage = ""
        Me.customvalidator1.IsValid = True

        If Me.GCName.Value.Length > 1 Then
            Dim s As String = "A Valid GC Name, Address are required"
            If (Me.GCAdd1.Text.Length < 1 Or Me.GCCity.Text.Length < 1 Or Me.GCState.Text.Length < 1) Then
                Me.customvalidator1.ErrorMessage = s
                Me.customvalidator1.IsValid = False
                Exit Sub
            End If
        End If

        If Me.PrelimASIS.Checked = True Or Me.VerifyJobASIS.Checked = True Then
            If Me.LenderName.Value.Length > 0 Then
                Dim s As String = "A valid Lender/Surety Name is required"
                If Me.LenderName.Text = "N/A" Or Me.LenderName.Text = "n/a" Or Me.LenderName.Text = "Unknown" Or Me.LenderName.Text = "unknown" Or InStr(Me.LenderName.Text, "?") > 0 Then
                    Me.customvalidator1.ErrorMessage = s
                    Me.customvalidator1.IsValid = False
                    Exit Sub
                End If
            End If
            If Me.LenderName.Value.Length > 0 Then
                Dim s As String = "A valid Lender/Surety Address is required"
                If Me.LenderAdd1.Text = "N/A" Or Me.LenderAdd1.Text = "n/a" Or Me.LenderAdd1.Text = "Unknown" Or Me.LenderAdd1.Text = "unknown" Or InStr(Me.LenderAdd1.Text, "?") > 0 Then
                    Me.customvalidator1.ErrorMessage = s
                    Me.customvalidator1.IsValid = False
                    Exit Sub
                End If
            End If
            If Me.LenderName.Value.Length > 0 And Me.LenderAdd1.Value.Length = 0 Then
                Dim s As String = "A Lender/Surety Address is required"
                Me.customvalidator1.ErrorMessage = s
                Me.customvalidator1.IsValid = False
                Exit Sub
            End If
            If Me.LenderName.Value.Length > 0 Then
                Dim s As String = "A valid Lender/Surety City is required"
                If Me.LenderCity.Text = "N/A" Or Me.LenderCity.Text = "n/a" Or Me.LenderCity.Text = "Unknown" Or Me.LenderCity.Text = "unknown" Or InStr(Me.LenderCity.Text, "?") > 0 Then
                    Me.customvalidator1.ErrorMessage = s
                    Me.customvalidator1.IsValid = False
                    Exit Sub
                End If
            End If
            If Me.LenderName.Value.Length > 0 And Me.LenderCity.Value.Length = 0 Then
                Dim s As String = "A Lender/Surety City is required"
                Me.customvalidator1.ErrorMessage = s
                Me.customvalidator1.IsValid = False
                Exit Sub
            End If
            If Me.LenderName.Value.Length > 0 Then
                Dim s As String = "A valid Lender/Surety State is required"
                If Me.LenderState.Text = "N/A" Or Me.LenderState.Text = "n/a" Or Me.LenderState.Text = "Unknown" Or Me.LenderState.Text = "unknown" Or InStr(Me.LenderState.Text, "?") > 0 Then
                    Me.customvalidator1.ErrorMessage = s
                    Me.customvalidator1.IsValid = False
                    Exit Sub
                End If
            End If
            If Me.LenderName.Value.Length > 0 And Me.LenderState.Value.Length = 0 Then
                Dim s As String = "A Lender/Surety State is required"
                Me.customvalidator1.ErrorMessage = s
                Me.customvalidator1.IsValid = False
                Exit Sub
            End If
            If Me.LenderName.Value.Length > 0 Then
                Dim s As String = "A valid Lender/Surety Zip is required"
                If Me.LenderZip.Text = "N/A" Or Me.LenderZip.Text = "n/a" Or Me.LenderZip.Text = "Unknown" Or Me.LenderZip.Text = "unknown" Or InStr(Me.LenderZip.Text, "?") > 0 Then
                    Me.customvalidator1.ErrorMessage = s
                    Me.customvalidator1.IsValid = False
                    Exit Sub
                End If
            End If
            If Me.LenderName.Value.Length > 0 And Me.LenderZip.Value.Length = 0 Then
                Dim s As String = "A Lender/Surety Zip is required"
                Me.customvalidator1.ErrorMessage = s
                Me.customvalidator1.IsValid = False
                Exit Sub
            End If
        End If

        If mylieninfo.NOCustCall = False Then
            Dim s As String = "A Valid Customer Phone Number is Required"
            If (Me.CustPhone.Text.Length < 1) Then
                Me.customvalidator1.ErrorMessage = s
                Me.customvalidator1.IsValid = False
                Exit Sub
            End If
        End If

        If Me.EstBalance.Value = 0 And Me.EstBalance.IsRequired = True Then
            Dim s As String = "Estimated Balance is Required"
            Me.customvalidator1.ErrorMessage = s
            Me.customvalidator1.IsValid = False
            Exit Sub
        End If

        mylieninfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientLienInfo(Me.ClientTableId.SelectedValue)
        myclient = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientInfo(Me.ClientTableId.SelectedValue)

        If mylieninfo.TXApproved = False And Strings.Left(Me.StateTableId.SelectedValue, 2) = "TX" Then
            Dim s As String = "You Must be Texas Approved to Enter a Texas Job, Contact Customer Service"
            Me.customvalidator1.ErrorMessage = s
            Me.customvalidator1.IsValid = False
            Exit Sub

        End If


        myitem = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
        myitem.Id = Me.ViewState("BatchJobId")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myitem.Id, myitem)

        myitem.JobAdd1 = Strings.Left(Me.JobAddr1.Text, 50)
        myitem.JobAdd2 = Strings.Left(Me.JobAddr2.Text, 50)
        myitem.JobCity = Strings.Left(Me.JobCity.Text, 20)
        myitem.JobName = Strings.Left(Me.JobName.Text, 100)
        myitem.JobNum = Strings.Left(Me.JobNum.Text, 20)
        myitem.JobState = Strings.Left(Me.StateTableId.SelectedValue, 2)
        myitem.JobZip = Strings.Left(Me.JobZip.Text, 10)

        myitem.JointCk = Me.JointCk.Checked
        myitem.EstBalance = Utils.GetDecimal(Me.EstBalance.Value)
        '07/22/2015
        'myitem.ResidentialBox = Me.OwnrSameAs.Checked
        myitem.ResidentialBox = Me.ResidentialBox.Checked

        myitem.GreenCard = Me.GreenCard.Checked
        myitem.RushOrder = Me.RushOrder.Checked
        myitem.PublicJob = Me.PublicJob.Checked
        myitem.FederalJob = Me.FederalJob.Checked

        myitem.PrelimBox = Me.PrelimBox.Checked
        myitem.PrelimASIS = Me.PrelimASIS.Checked
        myitem.VerifyJob = Me.VerifyJob.Checked
        myitem.TitleVerifiedBox = Me.VerifyJobASIS.Checked
        myitem.SuretyBox = Me.SuretyBox.Checked
        myitem.NOCBox = Me.NOCBox.Checked

        'If myitem.JobState = "TX" Then
        '    myitem.TitleVerifiedBox = Me.PrelimAsIS.Checked
        '    myitem.VerifyJob = Me.PrelimBox.Checked
        '    myitem.PrelimBox = False
        '    myitem.PrelimASIS = False
        'End If

        myitem.APNNum = Strings.Left(Me.APNNUM.Text, 50)
        myitem.PONum = Strings.Left(Me.PONUM.Text, 30)
        myitem.BuildingPermitNum = Strings.Left(Me.BuildingPermitNum.Text, 50)
        myitem.FolioNum = Strings.Left(Me.FolioNUm.Text, 20)
        myitem.BranchNum = Strings.Left(Me.BranchNum.Text, 25)
        myitem.CustJobNum = Me.CustJobNum.Value
        myitem.CustAdd1 = Strings.Left(Me.CustAdd1.Text, 50)
        myitem.CustAdd2 = Strings.Left(Me.CustAdd2.Text, 50)

        myitem.CustCity = Strings.Left(Me.CustCity.Text, 25)
        myitem.CustName = Strings.Left(Me.CustName.Text, 50)
        myitem.CustRefNum = Strings.Left(Me.CustRef.Text, 25)
        myitem.CustState = Strings.Left(Me.CustState.Text, 2)
        myitem.CustZip = Strings.Left(Me.CustZip.Text, 10)
        myitem.CustPhone1 = Utils.GetPhoneNo(Me.CustPhone.Text)
        myitem.CustFax = Utils.GetPhoneNo(Me.CustFax.Text)
        myitem.CustId = Me.ViewState("custid")

        myitem.GCAdd1 = Strings.Left(Me.GCAdd1.Text, 50)
        myitem.GCAdd2 = Strings.Left(Me.GCAdd2.Text, 50)
        myitem.GCCity = Strings.Left(Me.GCCity.Text, 25)
        myitem.GCName = Strings.Left(Me.GCName.Text, 50)
        myitem.GCRefNum = Strings.Left(Me.GCRef.Text, 50)
        myitem.GCState = Strings.Left(Me.GCState.Text, 2)
        myitem.GCZip = Strings.Left(Me.GCZip.Text, 10)
        myitem.GCPhone1 = Utils.GetPhoneNo(Me.GCPhone.Text)
        myitem.GCFax = Utils.GetPhoneNo(Me.GCFax.Text)
        myitem.GenId = Me.ViewState("gcid")
        If Me.GCSameAsCust.Checked = True Then
            myitem.GCAdd1 = Strings.Left(Me.CustAdd1.Text, 50)
            myitem.GCAdd2 = Strings.Left(Me.CustAdd2.Text, 50)
            myitem.GCCity = Strings.Left(Me.CustCity.Text, 25)
            myitem.GCName = Strings.Left(Me.CustName.Text, 50)
            myitem.GCRefNum = Strings.Left(Me.CustRef.Text, 25)
            myitem.GCState = Strings.Left(Me.CustState.Text, 2)
            myitem.GCZip = Strings.Left(Me.CustZip.Text, 10)
            myitem.GCPhone1 = Utils.GetPhoneNo(Me.CustPhone.Text)
            myitem.GCFax = Utils.GetPhoneNo(Me.CustFax.Text)
            myitem.GenId = -1
        End If

        myitem.OwnrAdd1 = Strings.Left(Me.OwnrAdd1.Text, 50)
        myitem.OwnrAdd2 = Strings.Left(Me.OwnrAdd2.Text, 50)

        myitem.OwnrCity = Strings.Left(Me.OwnrCity.Text, 25)
        myitem.OwnrName = Strings.Left(Me.OwnrName.Text, 50)
        myitem.OwnrState = Strings.Left(Me.OwnrState.Text, 2)
        myitem.OwnrZip = Strings.Left(Me.OwnrZip.Text, 10)
        myitem.OwnrPhone1 = Utils.GetPhoneNo(Me.OwnrPhone.Text)
        myitem.OwnrFax = Utils.GetPhoneNo(Me.OwnrFax.Text)
        myitem.MLAgent = Me.MLAgent.Checked

        myitem.LenderCity = Strings.Left(Me.LenderCity.Text, 25)
        myitem.LenderName = Strings.Left(Me.LenderName.Text, 50)
        myitem.LenderAdd1 = Strings.Left(Me.LenderAdd1.Text, 50)
        myitem.LenderAdd2 = Strings.Left(Me.LenderAdd2.Text, 50)
        myitem.LenderState = Strings.Left(Me.LenderState.Text, 2)
        myitem.LenderZip = Strings.Left(Me.LenderZip.Text, 10)
        myitem.LenderPhone1 = Utils.GetPhoneNo(Me.LenderPhone.Text)
        myitem.LenderFax = Utils.GetPhoneNo(Me.LenderFax.Text)
        myitem.BondNum = Me.BondNum.Text

        myitem.AZLotAllocate = Strings.Left(Me.AZLotAllocate.Text, 150)

        myitem.Note = Me.Note.Text
        myitem.SpecialInstruction = Strings.Left(Me.SpecialInstruction.Text, 255)
        myitem.EquipRate = Me.EquipRate.Value
        myitem.EquipRental = Me.EquipRental.Value
        myitem.RANum = Me.RANum.Value

        myitem.SameAsCust = Me.GCSameAsCust.Checked
        myitem.SameAsGC = Me.OwnrSameAs.Checked
        myitem.StartDate = Utils.GetDate(Me.StartDate.Value)

        myitem.SubmittedByUserId = Me.CurrentUser.Id
        myitem.BatchId = -1
        myitem.ClientId = Me.ClientTableId.SelectedValue
        myitem.JobState = Me.StateTableId.SelectedValue
        myitem.PlacementSource = "WEB"
        myitem.IsError = 0
        myitem.ErrorDescription = DBNull.Value.ToString
        myitem.LastUpdateOn = Now()

        If myitem.Id > 0 Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myitem)
        Else
            myitem.SubmittedOn = Now()
            myitem.LastUpdateOn = Now()
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myitem)
        End If

        Dim myu As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users
        CRFVIEW.Globals.DBO.Read(Me.CurrentUser.Id, myu)
        myu.LastClientId = myitem.ClientId
        myu.LastJobState = myitem.JobState
        myu.LastBranchNum = myitem.BranchNum

        CRFVIEW.Globals.DBO.Update(myu)

        Me.Session("JobState") = Me.StateTableId.SelectedValue
        Me.Session("ClientTableId") = Me.ClientTableId.SelectedValue
        Me.Session("BranchNum") = Me.BranchNum.Value

        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobAck(Me.CurrentUser.Id, myitem.Id, True)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

        IsexitViewer = True

    End Sub

    Public Sub EnforceRules()
        Me.ValidationSummary1.Visible = False

        mylieninfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientLienInfo(Me.ClientTableId.SelectedValue)
        myclient = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientInfo(Me.ClientTableId.SelectedValue)
        mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(Me.StateTableId.SelectedValue)
        Me.CustName.IsRequired = True
        If mylieninfo.BranchBox = False Then
            Me.BranchNum.Visible = False
            Me.BranchNum.IsRequired = False
        Else
            Me.BranchNum.Visible = True
            Me.BranchNum.IsRequired = True
        End If

        Me.FolioNUm.Visible = True
        If mystate.NOCBox = True Then
            Me.FolioNUm.IsRequired = False
        Else
            Me.FolioNUm.IsRequired = False
        End If

        If mystate.EstBalanceBox = False Then
            Me.EstBalance.IsRequired = False
        Else
            Me.EstBalance.IsRequired = True
        End If

        ' GC Validation
        Me.GCName.IsRequired = False
        Me.GCAdd1.IsRequired = False
        Me.GCAdd2.IsRequired = False
        Me.GCCity.IsRequired = False
        Me.GCState.IsRequired = False
        Me.GCZip.IsRequired = False
        If mystate.GCServBox = True And (Me.PrelimAsIS.Checked Or Me.VerifyJobASIS.Checked) Then
            Me.GCName.IsRequired = True
            Me.GCAdd1.IsRequired = True
            Me.GCCity.IsRequired = True
            Me.GCState.IsRequired = True
            Me.GCZip.IsRequired = True
        End If

        If Me.GCSameAsCust.Checked = True Then
            Me.GCRef.Visible = False
            Me.GCName.Text = Me.CustName.Text
            Me.GCAdd1.Text = Me.CustAdd1.Text
            Me.GCAdd2.Text = Me.CustAdd2.Text
            Me.GCCity.Text = Me.CustCity.Text
            Me.GCState.Text = Me.CustState.Text
            Me.GCZip.Text = Me.CustZip.Text
            Me.GCPhone.Text = Me.CustPhone.Text
            Me.GCFax.Text = Me.CustFax.Text
            Me.GCRef.Visible = False
            Me.GCAdd1.Enabled = False
            Me.GCAdd2.Enabled = False
            Me.GCCity.Enabled = False
            Me.GCFax.Enabled = False
            Me.GCName.Enabled = False
            Me.GCPhone.Enabled = False
            Me.GCState.Enabled = False
            Me.GCZip.Enabled = False
        Else
            Me.GCAdd1.Enabled = True
            Me.GCAdd2.Enabled = True
            Me.GCCity.Enabled = True
            Me.GCFax.Enabled = True
            Me.GCName.Enabled = True
            Me.GCPhone.Enabled = True
            Me.GCState.Enabled = True
            Me.GCZip.Enabled = True
        End If

        ' Onwer validation
        Me.OwnrName.IsRequired = False
        Me.OwnrAdd1.IsRequired = False
        Me.OwnrCity.IsRequired = False
        Me.OwnrState.IsRequired = False
        Me.OwnrZip.IsRequired = False
        If mystate.OwnerServBox = True And (Me.PrelimAsIS.Checked Or Me.VerifyJobASIS.Checked) Then
            Me.OwnrName.IsRequired = True
            Me.OwnrAdd1.IsRequired = True
            Me.OwnrCity.IsRequired = True
            Me.OwnrState.IsRequired = True
            Me.OwnrZip.IsRequired = True
        End If

        If Me.OwnrSameAs.Checked = True Then
            Me.OwnrName.Text = Me.GCName.Text
            Me.OwnrAdd1.Text = Me.GCAdd1.Text
            Me.OwnrAdd2.Text = Me.GCAdd2.Text
            Me.OwnrCity.Text = Me.GCCity.Text
            Me.OwnrState.Text = Me.GCState.Text
            Me.OwnrZip.Text = Me.GCZip.Text
            Me.OwnrPhone.Text = Me.GCPhone.Text
            Me.OwnrFax.Text = Me.GCFax.Text
            Me.OwnrAdd1.Enabled = False
            Me.OwnrCity.Enabled = False
            Me.OwnrFax.Enabled = False
            Me.OwnrName.Enabled = False
            Me.OwnrPhone.Enabled = False
            Me.OwnrState.Enabled = False
            Me.OwnrZip.Enabled = False
        Else
            Me.OwnrAdd1.Enabled = True
            Me.OwnrCity.Enabled = True
            Me.OwnrFax.Enabled = True
            Me.OwnrName.Enabled = True
            Me.OwnrPhone.Enabled = True
            Me.OwnrState.Enabled = True
            Me.OwnrZip.Enabled = True
        End If

        ' Lender validation
        Me.LenderName.IsRequired = False
        Me.LenderAdd1.IsRequired = False
        Me.LenderCity.IsRequired = False
        Me.LenderState.IsRequired = False
        Me.LenderZip.IsRequired = False

        If mystate.LenderServBox = True And (Me.PrelimASIS.Checked Or Me.VerifyJobASIS.Checked) And Me.LenderName.Value.Length > 1 Then
            Me.LenderName.IsRequired = True
            Me.LenderAdd1.IsRequired = True
            Me.LenderCity.IsRequired = True
            Me.LenderState.IsRequired = True
            Me.LenderZip.IsRequired = True
        End If


        'If Me.StateTableId.SelectedValue = "TX" Then   comment on 23-12-2011
        '    Me.StartDate.IsRequired = False
        '    Me.PrelimBox.Enabled = False
        '    Me.PrelimAsIS.Enabled = False

        '    Me.PrelimBox.Checked = False
        '    Me.PrelimAsIS.Checked = False
        '    Me.VerifyJobASIS.Checked = False

        '    If Me.PrelimAsIS.Checked = False And Me.VerifyJobASIS.Checked = False Then
        '        Me.VerifyJob.Checked = True
        '    End If
        'Else
        '    Me.StartDate.IsRequired = True
        '    Me.PrelimBox.Enabled = True
        '    Me.PrelimAsIS.Enabled = True

        '    Me.PrelimAsIS.Checked = False
        '    Me.VerifyJobASIS.Checked = False
        '    Me.VerifyJob.Checked = False

        '    If Me.PrelimAsIS.Checked = False And Me.VerifyJob.Checked = False And Me.VerifyJobASIS.Checked = False Then
        '        Me.PrelimBox.Checked = True
        '    End If
        'End If

        If Me.StateTableId.SelectedValue = "AZ" Then
            Me.panAZLotAllocate.Visible = True
        Else
            Me.panRentalINfo.Visible = False
        End If

        Me.EquipRental.IsRequired = False
        If myclient.IsRentalCo = True Then
            Me.panRentalINfo.Visible = True
            If Me.StateTableId.SelectedValue = "MO" Then
                Me.EquipRate.IsRequired = True
                Me.EquipRental.IsRequired = True
            End If
        Else
            Me.panRentalINfo.Visible = False
        End If

        If mylieninfo.GreenCard = True Then
            Me.GreenCard.Checked = True
        End If

        If mylieninfo.JointCK = True Then
            Me.JointCk.Checked = True
        End If

        If mylieninfo.IsNOCApproved = True And mystate.NoticeOfCompBox = True Then
            Me.NOCBox.Visible = True
        Else
            Me.NOCBox.Visible = False
        End If
        CustNameSearch1.SelectClientId = mylieninfo.ClientId
        GCSearch1.SelectClientId = mylieninfo.ClientId

    End Sub

    Protected Sub ClientTableId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClientTableId.SelectedIndexChanged
        EnforceRules()


    End Sub
    Protected Sub VerifyJobASIS_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles VerifyJobASIS.CheckedChanged
        EnforceRules()

    End Sub
    Protected Sub StateTableId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateTableId.SelectedIndexChanged ' Handles StateTableId.SelectedIndexChanged
        mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(Me.StateTableId.SelectedValue)
        If mystate.MLAgent = True Then
            'If Me.StateTableId.SelectedValue = "VA" Then
            Me.TabPanel4.HeaderText = "Owner"
            Me.MLAgent.Visible = True
        Else
            Me.TabPanel4.HeaderText = "Owner"
            Me.MLAgent.Visible = False
            Me.MLAgent.Checked = False
        End If

        If mylieninfo.IsNOCApproved = True And mystate.NoticeOfCompBox = True Then
            Me.NOCBox.Visible = True
        Else
            Me.NOCBox.Visible = False
        End If

        ' 9/17/2014
        StateValidation()

        ' 10/9/2013 Undid commenting of EnforceRules()
        EnforceRules()
        GetStateAlerts()
    End Sub

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?lienview.newjoblist"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub

    Protected Sub GCSearch1_ItemSelected() Handles GCSearch1.ItemSelected
        Dim myid As String = Strings.Replace(GCSearch1.SelectedValue, "C", "")
        myid = Strings.Replace(myid, "B", "")
        If IsNothing(myid) Then Exit Sub
        If Left(Me.GCSearch1.SelectedValue, 1) = "B" Then
            Dim mynewJob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mynewJob)
            Me.GCName.Text = Left(mynewJob.GCName, 50)
            Me.GCAdd1.Text = mynewJob.GCAdd1
            Me.GCAdd2.Text = mynewJob.GCAdd2

            Me.GCCity.Value = mynewJob.GCCity
            Me.GCState.Value = mynewJob.GCState
            Me.GCZip.Value = mynewJob.GCZip
            Me.GCPhone.Text = Utils.FormatPhoneNo(mynewJob.GCPhone1)
            Me.GCFax.Text = Utils.FormatPhoneNo(mynewJob.GCFax)
            Me.GCRef.Text = mynewJob.GCRefNum
            Me.ViewState("gcid") = -1
        Else
            Dim mygc As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientGeneralContractor
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mygc)
            Me.GCName.Text = mygc.GeneralContractor
            Me.GCAdd1.Text = mygc.AddressLine1
            Me.GCAdd2.Text = mygc.AddressLine2

            Me.GCCity.Value = mygc.City
            Me.GCState.Value = mygc.State
            Me.GCZip.Value = mygc.PostalCode
            Me.GCRef.Text = mygc.RefNum
            Me.GCPhone.Text = Utils.FormatPhoneNo(mygc.Telephone1)
            Me.GCFax.Text = Utils.FormatPhoneNo(mygc.Telephone2)
            Me.ViewState("gcid") = mygc.GenId

            Me.GCName.Enabled = False
            Me.GCAdd1.Enabled = False
            Me.GCAdd2.Enabled = False

            Me.GCCity.Enabled = False
            Me.GCState.Enabled = False
            Me.GCZip.Enabled = False
            Me.GCRef.Enabled = False
            Me.GCPhone.Enabled = False
            Me.GCFax.Enabled = False
            Me.GCEmail.Enabled = False
            btnEditGeneralCont.Visible = True

        End If
    End Sub
    Protected Sub btnEditGeneralCont_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditGeneralCont.Click
        Me.ViewState("gcid") = -1
        Me.GCName.Enabled = True
        Me.GCAdd1.Enabled = True
        Me.GCAdd2.Enabled = True

        Me.GCCity.Enabled = True
        Me.GCState.Enabled = True
        Me.GCZip.Enabled = True
        Me.GCRef.Enabled = True
        Me.GCPhone.Enabled = True
        Me.GCFax.Enabled = True
        Me.GCEmail.Enabled = True

    End Sub
    Protected Sub GCSearch1_SearchSelected(ByVal searchkey As String) Handles GCSearch1.SearchSelected
        GCSearch1.ShowList(searchkey)
    End Sub

    Protected Sub CustNameSearch1_ItemSelected() Handles CustNameSearch1.ItemSelected
        Dim myid As String = Strings.Replace(CustNameSearch1.SelectedValue, "C", "")
        myid = Strings.Replace(myid, "B", "")
        If IsNothing(myid) Then Exit Sub
        If Left(Me.CustNameSearch1.SelectedValue, 1) = "B" Then
            Dim mynewcust As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mynewcust)
            Me.CustName.Text = Left(mynewcust.CustName, 50)
            Me.CustAdd1.Text = mynewcust.CustAdd1
            Me.CustAdd2.Text = mynewcust.CustAdd2

            Me.CustCity.Value = mynewcust.CustCity
            Me.CustState.Value = mynewcust.CustState
            Me.CustZip.Value = mynewcust.CustZip
            Me.CustRef.Text = mynewcust.CustRefNum
            Me.CustPhone.Text = Utils.FormatPhoneNo(mynewcust.CustPhone1)
            Me.CustFax.Text = Utils.FormatPhoneNo(mynewcust.GCPhone2)
            Me.ViewState("custid") = -1
        Else
            Dim mycust As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomer
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mycust)
            If mycust.CustId <> myid Then Exit Sub
            Me.CustName.Text = Left(mycust.ClientCustomer, 50)
            Me.CustAdd1.Text = mycust.AddressLine1
            Me.CustAdd2.Text = mycust.AddressLine2

            Me.CustCity.Value = mycust.City
            Me.CustState.Value = mycust.State
            Me.CustZip.Value = mycust.PostalCode
            Me.CustRef.Text = mycust.RefNum
            Me.CustPhone.Text = Utils.FormatPhoneNo(mycust.Telephone1)
            Me.CustFax.Text = Utils.FormatPhoneNo(mycust.Fax)
            Me.ViewState("custid") = mycust.CustId

            'set disable controlles
            Me.CustName.Enabled = False
            Me.CustAdd1.Enabled = False
            Me.CustAdd2.Enabled = False

            Me.CustCity.Enabled = False
            Me.CustState.Enabled = False
            Me.CustZip.Enabled = False
            Me.CustRef.Enabled = False
            Me.CustPhone.Enabled = False
            Me.CustFax.Enabled = False

            Me.CustEmail.Enabled = False
            Me.CustJobNum.Enabled = False
            Me.btnEditCustDet.Visible = True

        End If

    End Sub
    Protected Sub btnEditCustDet_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCustDet.Click
        Me.ViewState("custid") = -1
        Me.CustName.Enabled = True
        Me.CustAdd1.Enabled = True
        Me.CustAdd2.Enabled = True

        Me.CustCity.Enabled = True
        Me.CustState.Enabled = True
        Me.CustZip.Enabled = True
        Me.CustRef.Enabled = True
        Me.CustPhone.Enabled = True
        Me.CustFax.Enabled = True

        Me.CustEmail.Enabled = True
        Me.CustJobNum.Enabled = True
        Me.btnEditCustDet.Visible = True


    End Sub
    Protected Sub CustNameSearch1_SearchSelected(ByVal searchkey As String) Handles CustNameSearch1.SearchSelected
        CustNameSearch1.ShowList(searchkey)
    End Sub

    Protected Sub GCSameAsCust_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles GCSameAsCust.CheckedChanged
        EnforceRules()

    End Sub

    Protected Sub OwnrSameAs_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles OwnrSameAs.CheckedChanged
        EnforceRules()

    End Sub


    Protected Sub PrelimAsIS_CheckedChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrelimASIS.CheckedChanged
        EnforceRules()

    End Sub

    Protected Sub PrelimBox_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles PrelimBox.CheckedChanged
        EnforceRules()

    End Sub

    Protected Sub AddLegalPty1_ItemSaved() Handles AddLegalPty1.ItemSaved
        Me.LoadLegalParties(Me.ViewState("BatchJobId"))

    End Sub

    Protected Sub gvwAddLegal_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwAddLegal.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnDeleteLP = TryCast(e.Row.FindControl("btnDeleteLP"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnDeleteLP"), ImageButton).Click, AddressOf btnDeleteLP_Click
            btnEditLP = TryCast(e.Row.FindControl("btnEditLP"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditLP"), LinkButton).Click, AddressOf btnEditLP_Click
        End If
    End Sub
    Protected Sub gvwAddLegal_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvwAddLegal.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnDeleteLP = TryCast(e.Row.FindControl("btnDeleteLP"), ImageButton)
            btnDeleteLP.Attributes("rowno") = dr("Id")
            btnEditLP = TryCast(e.Row.FindControl("btnEditLP"), LinkButton)
            btnEditLP.Attributes("rowno") = dr("Id")

        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

    End Sub
    Protected Sub btnEditLP_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditLP.Click
        btnEditLP = TryCast(sender, LinkButton)
        Dim myitem As String = btnEditLP.Attributes("rowno")
        Me.AddLegalPty1.EditItem(Me.ViewState("BatchJobId"), myitem)

    End Sub

    Protected Sub btnDeleteLP_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteLP.Click
        btnDeleteLP = TryCast(sender, ImageButton)
        Dim MYITEM As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJobLegalParties
        MYITEM.Id = btnDeleteLP.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(MYITEM)
        LoadLegalParties(Me.ViewState("BatchJobId"))

    End Sub



    Protected Sub btnFileUpLoadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadCancel.Click
        'markparimal-uploadfile:To refresh Grid values .
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where BatchJobId = " & Session("BatchJobId")
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()

    End Sub

    Protected Sub gvwDocs_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnPrntDocs"), ImageButton).Click, AddressOf btnPrntDocs_Click

        End If
    End Sub

    Protected Sub gvwDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), ImageButton)
            btnPrntDocs.Attributes("rowno") = DR("Id")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
        End If
    End Sub

    Protected Sub btnPrntDocs_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrntDocs.Click
        'markparimal-uploadfile:To display selected upload file .converts file from byte 

        btnPrntDocs = TryCast(sender, ImageButton)
        Me.ViewState("VIEW") = "Docs"

        Dim myitem As New TABLES.JobAttachments
        ProviderBase.DAL.Read(btnPrntDocs.Attributes("rowno"), myitem)

        Dim sreport As String = "~/UserData/output/" & myitem.Id & "-" & myitem.FileName
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.DocumentImage, Me.MapPath(sreport))

        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

        IsexitViewer = False
    End Sub
    Protected Sub btnExitViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitViewer.Click

        If (IsexitViewer = True) Then
            Dim s As String = "~/MainDefault.aspx?lienview.newjoblist"
            Response.Redirect(Me.Page.ResolveUrl(s), True)
        Else
            Me.MultiView1.SetActiveView(Me.View1)
            TabContainer1.ActiveTabIndex = 7
        End If
       
    End Sub
    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub

    Protected Sub btngetstate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btngetstate.Click

        EnforceRules()
        GetStateAlerts()
    End Sub

    Protected Sub VerifyJob_CheckedChanged(sender As Object, e As System.EventArgs) Handles VerifyJob.CheckedChanged
        EnforceRules()
    End Sub

    Protected Sub TreasurySuretyAddressList_Click(sender As Object, e As System.EventArgs) Handles TreasurySuretyAddressList.Click

    End Sub

    Protected Sub CopyJob_CheckedChanged(sender As Object, e As System.EventArgs) Handles CopyJob.CheckedChanged
        ' 9/23/2014
        If Me.CopyJob.Checked = True Then
            Me.OwnrAdd1.Text = Me.JobAddr1.Text
            Me.OwnrAdd2.Text = Me.JobAddr2.Text
            Me.OwnrCity.Text = Me.JobCity.Text
            Me.OwnrState.Text = Strings.Left(Me.StateTableId.SelectedValue, 2)
            Me.OwnrZip.Text = Me.JobZip.Text
        End If

    End Sub
End Class
