<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style>
    .panel-body, .form-control.input-sm {
        font-size: 12px;
    }

    #ctl06_ctl00_gvJobViewList tbody tr:hover {
        cursor: pointer;
    }

    #ctl06_ctl00_gvJobViewList thead tr th {
        font-weight: bold;
    }

    #ctl06_ctl00_gvJobViewList thead tr th, #ctl06_ctl00_gvJobViewList tbody tr td {
        /*border: 0;*/
    }

    #JobFilterBody .radio-custom label {
        width: 95px;
    }

    #JobFilterBody .radio-custom.radio-default.radio-inline {
        min-height: 0px;
        padding-top: 0px;
    }

    #clsdropdownClient .btn-group {
        width: 97%;
    }

        #clsdropdownClient .btn-group .btn {
            width: 85%;
        }

            #clsdropdownClient .btn-group .btn.dropdown-toggle {
                width: 100%;
            }

        #clsdropdownClient .btn-group .dropdown-menu {
            width: 100%;
        }

    .RemovePadding {
        padding: 0px !important;
        border: 1px solid black;
    }

    .nowordwraptd {
        padding-left: 5px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .wordwraptd {
        padding-left: 5px;
        text-overflow: ellipsis;
    }

    .es-list{
        text-align:left;
    }
     .activePage{
            z-index: 3;
    color: white !important;
    cursor: default;
    background-color: #3a6dae!important;
    border-color: #3a6dae!important;
    }

    /*table, th, td {
    border: 1px solid !important;*/
</style>
<script type="text/javascript">
    function validateJobFilter() {
        debugger;
        //alert("validate")
        var ISValid = true;
        //  $("#modcontainer input[type=text]").each(function() {
        $("#modcontainer").find("input").each(function (idx) {

            var $element = $(this);
            var eType = $element.attr('type');
            // console.log($element);
            if ($element.attr('id') == "ctl33_ctl00_Calendar1" || $element.attr('id') == "ctl33_ctl00_Calendar2" || $element.attr('id') == "ctl33_ctl00_Calendar3" ||
                $element.attr('id') == "ctl33_ctl00_Calendar4") {
                return true;
            }

            else if (eType == "text") {
                var textBox = $.trim($element.val());
                if (textBox == "" || textBox == "$0.00") {

                    ISValid = false;

                }
                else {
                    ISValid = true;

                }
            }
            else if (eType == "checkbox") {
                if ($element.prop("checked") == false) {
                    ISValid = false;

                } else {
                    ISValid = true;

                }
            }

            // console.log(ISValid);
            if (ISValid == true) {
                return false;
            }
        });
        //console.log(ISValid);
        //alert(ISValid);
        if (ISValid == false) {
            $("#ctl33_ctl00_btnSubmit").prop("disabled", true);


        }
        else {
            $("#ctl33_ctl00_btnSubmit").prop("disabled", false);

        }
    }
    function modalshow() {
        debugger;
        // alert("hii");
        $("#modelDivExport").modal('show');
        return false;
    }
    function HideLoadingPanel() {
        //alert("hii");
        $(".TransparentGrayBackground").hide();

        $(".PageUpdateProgress").hide();
    }
    function modalhide() {
        //alert("hide");

        //alert($('input[type=radio]:checked').val());
        if ($('input[type=radio]:checked').val() == "PDFReport") {
            //$("#LodingPanel").show();
        }
        else {
            HideLoadingPanel();

        }

        $("#modelDivExport").modal('hide');
        document.getElementById("<%= btnExportReport.ClientID %>").click();
        return false;
    }

    var todaycheck;
    function getFormatDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = mm + '/' + dd + '/' + yyyy;

        return today
    }

    function CheckAssignedDateRange() {
        debugger;

        todaycheck = getFormatDate();
        if (((document.getElementById('<%=Calendar1.ClientID%>').value == todaycheck) && (document.getElementById('<%=Calendar2.ClientID%>').value == todaycheck)) || ((document.getElementById('<%=Calendar1.ClientID%>').value == "") && (document.getElementById('<%=Calendar2.ClientID%>').value == ""))) {
            //alert("start");


            $('#ctl33_ctl00_chkByAssignDate').prop('checked', false);
            // alert("end");
        }
        else {
            $('#ctl33_ctl00_chkByAssignDate').prop('checked', true);
            validateJobFilter();
        }
    }


    function CheckDeadlineDateRange() {
        todaycheck = getFormatDate();
        debugger;

        if (((document.getElementById('<%=Calendar3.ClientID%>').value == todaycheck) && (document.getElementById('<%=Calendar4.ClientID%>').value == todaycheck)) || ((document.getElementById('<%=Calendar3.ClientID%>').value == "") && (document.getElementById('<%=Calendar4.ClientID%>').value == ""))) {
            //alert("start");

            $('#ctl33_ctl00_chkByDeadlineDate').prop('checked', false);
            // alert("end");
        }
        else {
            $('#ctl33_ctl00_chkByDeadlineDate').prop('checked', true);
            validateJobFilter();
        }
    }


    function FromBalance_LostFocus() {
        var FromBalanceCntrl = $('#<%=FromBalance.ClientID%>');
        withoutcurrfrombal = FromBalanceCntrl.val();
        if (FromBalanceCntrl.val().length > 0 && FromBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(FromBalanceCntrl.val().replace(",", ""))
            FromBalanceCntrl.val('$' + value.toFixed(2));
            CheckJobBalance();
        }
    }

    function ThruBalance_LostFocus() {
        var ThruBalanceCntrl = $('#<%=ThruBalance.ClientID%>');
        withoutcurrthrubal = ThruBalanceCntrl.val();
        if (ThruBalanceCntrl.val().length > 0 && ThruBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(ThruBalanceCntrl.val().replace(",", ""))
            ThruBalanceCntrl.val('$' + value.toFixed(2));
            CheckJobBalance();
        }
    }
    function CheckJobBalance() {
        debugger;


        if ((document.getElementById('<%=FromBalance.ClientID%>').value != "") && (document.getElementById('<%=ThruBalance.ClientID%>').value != "")) {
            //alert("start");


            $('#ctl33_ctl00_chkByBalance').prop('checked', true);
            validateJobFilter();
            // alert("end");
        }
        else {
            $('#ctl33_ctl00_chkByBalance').prop('checked', false);
        }
    }

    function myFunctionCustomerNameLike(id) {
        // alert ("hii" +id)
        if (id == 1) {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = true;
             document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = false;
         }
         else {
             document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = false;
             document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = true;
         }
     }

     function myFunctionCustomerRefLike(id) {

         if (id == 1) {
             document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionOwnerNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionGCNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=GCNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=GCNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=GCNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=GCNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionLenderNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=LenderNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=LenderNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=LenderNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=LenderNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobAddressLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNoLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobCityLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobCity_1.ClientID%>').checked = true;
            document.getElementById('<%=JobCity_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobCity_1.ClientID%>').checked = false;
            document.getElementById('<%=JobCity_2.ClientID%>').checked = true;
        }
    }


    <%--document.getElementById('<%=CustomerNameLikelientID%>').tabIndex = "2";--%>

    $(document).ready(function () {
        //rebindDataTable();
        <%--        todaycheck = getFormatDate();
       document.getElementById('<%=Calendar1.ClientID%>').value = todaycheck
        document.getElementById('<%=Calendar2.ClientID%>').value = todaycheck
        document.getElementById('<%=Calendar3.ClientID%>').value = todaycheck
        document.getElementById('<%=Calendar4.ClientID%>').value = todaycheck--%>
        BindPropertyType();
        validateJobFilter();
        //MakeJobList();
        $("div").on('blur change keyup', "input", function () {
            var $element = $(this);
            var eType = $element.attr('type');

            if (eType == "text" || eType == "checkbox") {
                validateJobFilter();
            }

        });

        //if (/[?&]page=/.test(location.search)) {
            //alert("saved");
            //CallSuccessFunc(0);
      //  }
       // else {
            //alert("not saved");
        //    CallSuccessFunc(1);
      //  }

        $(document).click(function () {
            if (event.target.id != "editable-selectPropertyType") {
                $(".es-list").css("display", "none");
                //  console.log(event.target.id);
            }

        });
    });

    function CallSuccessFunc(flag) {
        // $.noConflict();
        //  console.log('2');
        //var test = $("#ctl33_ctl00_gvJobViewList").html();
        //alert("hii" + test);
        var defaults = {

            "stateSave": true,
            "stateDuration": 60 * 10,
            "searching": false,
            "lengthMenu": [5, 15, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": false,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }


            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-4'il><'col-sm-7'p>>",

            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                },

            }
        };
        var defaults1 = {

            "stateSave": false,
            "stateDuration": 60 * 10,
            "searching": false,
            "lengthMenu": [5, 15, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": false,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-4'il><'col-sm-7'p>>",
            //"<'row'<'col-sm-2'i><'col-sm-1'l><'col-sm-7'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                },

            }
        };

        if (flag == 1) {
            var options = $.extend(true, {}, defaults1, $(this).data());
        }
        else {
            var options = $.extend(true, {}, defaults, $(this).data());
        }

        var gvJobViewListdiv = $('#' + '<%=gvJobViewList.ClientID%>');
        gvJobViewListdiv.DataTable(options);


    }

    function MaintainMenuOpen() {
        //  $('#liJobs').addClass('site-menu-item has-sub active open');
        //  $('#lisubJobList').addClass('site-menu-item active');
        SetHeaderBreadCrumb('LienView', 'Jobs', 'View Jobs');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubJobList');
        return false;

    }
    function BindClientSelection() {
        $('#UlClientSelectionType li').remove();
        var x = document.getElementById('<%=DropDownList1.ClientID%>');
        var txt = "";
        var i;
        for (i = 0; i < x.length; i++) {
            var ResultReplace = ReplaceSpecChar(x.options[i].text);
            $('#UlClientSelectionType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClientSel_' + x.options[i].value + '" onclick="setSelectedClientId(' + x.options[i].value + ',\'' + ResultReplace + '\')">' + x.options[i].text + '</a></li>');
        }
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val() != "") {
            $('#btnClientSelectionType').html($('#liClientSel_' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedClientId(id, TextData) {
        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnClientSelectionId.ClientID%>').val(id);
        $('#btnClientSelectionType').html(ResultReplace + "<span class='caret' ></span>");
       
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val().length > 0)
        {
            <%-- alert('hii ' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val() );--%>
            var value = "SELECTED";
            var radio = $("[id*=ctl33_ctl00_RadioButtonList1] input[value=" + value + "]");
            radio.attr("checked", "checked");

        }
        return false;
    }
    function ReplaceSpecChar(Textdata) {
        //alert(Textdata);
        var result = Textdata;
        var ResultReplace = result.replace("'", "`");
        //alert("ResultReplace" + ResultReplace);
        return ResultReplace;
    }
    function ClearDataTable() {
        debugger;
        alert(clear);
        var dt = $('#' + '<%=gvJobViewList.ClientID%>').DataTable();
        dt.table().state.clear();

    }
    function BindPropertyType() {
        // alert("BindPropertyType");
        debugger;
        $('#UlPropertyType').html();
        $('#UlPropertyType').empty();

        var x = document.getElementById('<%=DrpPropertyType.ClientID%>');
        var txt = "<select class=\"form-control\" id=\"editable-selectPropertyType\" style=\"overflow-y: auto; \">";
      
        var i;
        //txt = txt + '<option role="presentation" ><a href="javascript:void(0)" id="liPropertyType_0">--Select Property--</a></option>';
        txt = txt + '<option role="menuitem" value="0" id="liPropertyType_0">--Select Property--</option>';
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                txt = txt + '<option role="menuitem"  id="liPropertyType_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
            }
        }

        txt = txt + "</select>";
        $('#UlPropertyType').empty();
        $('#UlPropertyType').html(txt);

        $('#editable-selectPropertyType').editableSelect().on('select.editable-select', function (e, li) {

            if (li) {

                $('#' + '<%= hdnDrpPropertyType.ClientID%>').val(li.attr('value'));
            }
        });

    }

</script>
<script runat="server">
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
    Protected WithEvents btnEditItem As LinkButton

    Dim vwclients As HDS.DAL.COMMON.TableView
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Dim dtBeforeSort As New System.Data.DataTable
    Dim dtAfterSort As New System.Data.DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        FromBalance.Attributes.Add("onblur", "FromBalance_LostFocus();")
        ThruBalance.Attributes.Add("onblur", "ThruBalance_LostFocus();")
        If Me.Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            LoadClientList()
            Calendar1.Text = DateTime.Today.ToString("MM/dd/yyyy")
            Calendar2.Text = DateTime.Today.ToString("MM/dd/yyyy")
            Calendar3.Text = DateTime.Today.ToString("MM/dd/yyyy")
            Calendar4.Text = DateTime.Today.ToString("MM/dd/yyyy")
            If hdnClientSelectionId.Value <> "" Then
                RadioButtonList1.Items.FindByValue("SELECTED").Selected = True
            End If
            If qs.HasParameter("page") Then

                If (IsNothing(Session("filterOrder")) = False And IsNothing(Session("filterColumn")) = False) Then
                    drpSortType.Items.FindByValue(Session("filterOrder")).Selected = True
                    drpSortBy.Items.FindByValue(Session("filterColumn")).Selected = True
                End If

                If IsNothing(Session("pageIndex")) Then
                    GetJobsPageWise(GetFilter, 1)
                Else
                    GetJobsPageWise(GetFilter, Convert.ToInt32(Session("pageIndex")))
                End If
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
        If hdnClientSelectionId.Value <> "" Then
            RadioButtonList1.Items.FindByValue("SELECTED").Selected = True
        End If
    End Sub

    Private Sub LoadClientList()

        vwclients = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop

            Me.panClients.Visible = True
            mysproc = GetFilter()
            If mysproc.ClientCode.Length > 4 Then
                Me.DropDownList1.SelectedValue = mysproc.ClientCode
                Me.hdnClientSelectionId.Value = mysproc.ClientCode
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)

        End If

    End Sub

    Protected Function GetFilter() As Object
        If IsNothing(Session("mysproc")) = False Then
            Return Session("mysproc")
        Else
            Return New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
        End If
    End Function

    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackFilter.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        Dim s As String = "~/MainDefault.aspx?lienview.joblistnew"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
        Me.MultiView1.SetActiveView(Me.View1)
        CustomerName.Text = ""
        CustomerRef.Text = ""
        OwnerName.Text = ""
        GCName.Text = ""
        LenderName.Text = ""
        JobName.Text = ""
        JobAddress.Text = ""
        JobNo.Text = ""
        'Calendar1.Text = ""
        'Calendar2.Text = ""
        'Calendar3.Text = ""
        'Calendar4.Text = ""


        Calendar1.Text = DateTime.Today.ToString("MM/dd/yyyy")
        Calendar2.Text = DateTime.Today.ToString("MM/dd/yyyy")
        Calendar3.Text = DateTime.Today.ToString("MM/dd/yyyy")
        Calendar4.Text = DateTime.Today.ToString("MM/dd/yyyy")
        FromBalance.Text = ""
        ThruBalance.Text = ""
        JobCity.Text = ""
        FilterKey.Text = ""
        JobState.Text = ""
        BranchNo.Text = ""
        PONum.Text = ""
        JobStatus.Text = ""
        TempId.Text = ""
        FileNumber.Text = ""
        CustomerNameLike_1.Checked = True
        CustomerNameLike_2.Checked = False
        CustomerRefLike_1.Checked = True
        CustomerRefLike_2.Checked = False
        OwnerNameLike_1.Checked = True
        OwnerNameLike_2.Checked = False
        GCNameLike_1.Checked = True
        GCNameLike_2.Checked = False
        LenderNameLike_1.Checked = True
        LenderNameLike_2.Checked = False
        JobNameLike_1.Checked = True
        JobNameLike_2.Checked = False
        JobAddressLike_1.Checked = True
        JobAddressLike_2.Checked = False
        JobNoLike_1.Checked = True
        JobNoLike_2.Checked = False
        JobCity_1.Checked = True
        JobCity_2.Checked = False
        chkByAssignDate.Checked = False
        chkByDeadlineDate.Checked = False
        chkByBalance.Checked = False
        chkPaidInFull.Checked = False
        rdoAll.Checked = False
        rdoBond.Checked = False
        rdoLien.Checked = False
        rdoStopNotice.Checked = False
        rdoBondSuit.Checked = False
        rdoForeclosure.Checked = False
        rdoNOI.Checked = False
    End Sub
    Protected Function GetViewURL1(ByVal AITEMID As String) As String
        Dim QS As New HDS.WEBLIB.Common.QueryString(Me.Page)
        mysproc = GetFilter()
        QS.RemoveParameter("PrintId")
        Dim s As String = "212" & "I"
        QS.SetParameter("plid", s)
        QS.SetParameter("page", mysproc.Page)
        QS.SetParameter("ItemId", AITEMID)
        Return QS.All
    End Function


    'End Sub

    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim s As String = "~/MainDefault.aspx?lienview.jobinfo&itemid=" & myitemid
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = DR("JobId")

            '-------------New---------------------------------
            Dim lblNDLDate As Label = TryCast(e.Row.FindControl("lblNoticeDate"), Label)
            Dim lblLienDate As Label = TryCast(e.Row.FindControl("lblLienDate"), Label)
            Dim lblBondDate As Label = TryCast(e.Row.FindControl("lblBondDate"), Label)
            Dim lblSNDate As Label = TryCast(e.Row.FindControl("lblSNDate"), Label)
            Dim lblForeClose As Label = TryCast(e.Row.FindControl("lblForeClose"), Label)
            Dim lblSuit As Label = TryCast(e.Row.FindControl("lblSuit"), Label)
            Dim lblJobid As Label = TryCast(e.Row.FindControl("lblJobid"), Label)
            Dim lblNOI As Label = TryCast(e.Row.FindControl("lblNOIDeadline"), Label)


            Dim hdnNoticeSent As HiddenField = TryCast(e.Row.FindControl("hdnNoticeSent"), HiddenField)
            Dim hdnLienDate As HiddenField = TryCast(e.Row.FindControl("hdnLienDate"), HiddenField)
            Dim hdnBondDate As HiddenField = TryCast(e.Row.FindControl("hdnBondDate"), HiddenField)
            Dim hdnSNDate As HiddenField = TryCast(e.Row.FindControl("hdnSNDate"), HiddenField)
            Dim hdnForeclosureDate As HiddenField = TryCast(e.Row.FindControl("hdnForeclosureDate"), HiddenField)
            Dim hdnBondSuitDate As HiddenField = TryCast(e.Row.FindControl("hdnBondSuitDate"), HiddenField)
            Dim hdnPublicJob As HiddenField = TryCast(e.Row.FindControl("hdnPublicJob"), HiddenField)
            Dim hdndateassigned As HiddenField = TryCast(e.Row.FindControl("hdndateassigned"), HiddenField)


            If lblNDLDate.Text IsNot "" Then
                Dim NDLDate As Date = CType(lblNDLDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, NDLDate) < 10 And hdnNoticeSent.Value Is "" Then
                    lblNDLDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblLienDate.Text IsNot "" Then
                Dim LDLDate As Date = CType(lblLienDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, LDLDate) < 30 And hdnLienDate.Value Is "" Then
                    lblLienDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblBondDate.Text IsNot "" Then
                Dim BDLDate As Date = CType(lblBondDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, BDLDate) < 30 And hdnBondDate.Value Is "" Then
                    lblBondDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblSNDate.Text IsNot "" Then
                Dim SNDate As Date = CType(lblSNDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, SNDate) < 30 And hdnSNDate.Value Is "" Then
                    lblSNDate.ForeColor = Drawing.Color.Red
                End If
            End If

            If lblForeClose.Text IsNot "" Then
                Dim ForeClose As Date = CType(lblForeClose.Text, Date)
                If DateDiff(DateInterval.Day, Now, ForeClose) < 30 And hdnForeclosureDate.Value Is "" Then
                    lblForeClose.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblNOI.Text IsNot "" Then
                Dim NOI As Date = CType(lblNOI.Text, Date)
                If DateDiff(DateInterval.Day, Now, NOI) < 10 And hdnLienDate.Value Is "" Then
                    lblNOI.ForeColor = Drawing.Color.Red
                End If
            End If

            If lblSuit.Text IsNot "" Then
                Dim Suit As Date = CType(lblSuit.Text, Date)
                If DateDiff(DateInterval.Day, Now, Suit) < 30 And hdnBondSuitDate.Value Is "" And hdnPublicJob.Value = True Then
                    lblSuit.ForeColor = Drawing.Color.Red
                End If
            End If
            If hdndateassigned.Value IsNot "" Then
                lblJobid.ForeColor = Drawing.Color.Blue
            End If

        End If


    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim today As String = Format(DateTime.Now.Date, "MM/dd/yyyy")
        If (chkByDeadlineDate.Checked = True) Then

            If (rdoLien.Checked = False And rdoBond.Checked = False And rdoStopNotice.Checked = False And rdoAll.Checked = False And rdoForeclosure.Checked = False And rdoBondSuit.Checked = False And rdoNOI.Checked = False) Then
                CustomValidator1.ErrorMessage = "Select DeadLine Type"
                CustomValidator1.IsValid = False
                Exit Sub
            Else
                CustomValidator1.ErrorMessage = ""
                CustomValidator1.IsValid = True

            End If
        End If
        With mysproc
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                    .ClientCode = Me.hdnClientSelectionId.Value
                End If
            End If

            .FromReferalDate = Me.Calendar1.Text
            .ThruReferalDate = Me.Calendar2.Text
            If Me.chkByAssignDate.Checked Then
                .ReferalRange = 1
            End If
            ' Added 5/16/2016
            .FromDeadlineDate = Me.Calendar3.Text
            .ThruDeadlineDate = Me.Calendar4.Text
            If Me.chkByDeadlineDate.Checked Then
                .DeadlineRange = 1
            End If


            If rdoLien.Checked = True Then
                .DeadlineType = 1
            ElseIf rdoBond.Checked = True Then
                .DeadlineType = 2
            ElseIf rdoStopNotice.Checked = True Then
                .DeadlineType = 3
            ElseIf rdoForeclosure.Checked = True Then
                .DeadlineType = 4
            ElseIf rdoBondSuit.Checked = True Then
                .DeadlineType = 5
            ElseIf rdoNOI.Checked = True Then
                .DeadlineType = 6
            ElseIf rdoAll.Checked = True Then
                .DeadlineType = 0
            Else

            End If

            ' Added 5/17/2016
            .FromBalance = Me.FromBalance.Value
            .ThruBalance = Me.ThruBalance.Value
            If Me.chkByBalance.Checked Then
                .BalanceRange = 1
            End If


            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike_1.Checked Then
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If

                If Me.CustomerNameLike_2.Checked Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                End If
            End If


            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike_1.Checked Then
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If

                If Me.CustomerRefLike_2.Checked Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                End If
            End If

            If Me.OwnerName.Text.Trim.Length > 0 Then
                If Me.OwnerNameLike_1.Checked Then
                    .PropOwner = Me.OwnerName.Text.Trim & "%"
                End If

                If Me.OwnerNameLike_2.Checked Then
                    .PropOwner = "%" & Me.OwnerName.Text.Trim & "%"
                End If
            End If

            If Me.GCName.Text.Trim.Length > 0 Then
                If Me.GCNameLike_1.Checked Then
                    .GeneralContractor = Me.GCName.Text.Trim & "%"
                End If

                If Me.GCNameLike_2.Checked Then
                    .GeneralContractor = "%" & Me.GCName.Text.Trim & "%"
                End If
            End If



            If Me.LenderName.Text.Trim.Length > 0 Then
                If Me.LenderNameLike_1.Checked Then
                    .LenderName = Me.LenderName.Text.Trim & "%"
                End If

                If Me.LenderNameLike_2.Checked Then
                    .LenderName = "%" & Me.LenderName.Text.Trim & "%"
                End If
            End If

            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobName = Me.JobName.Text.Trim & "%"
                End If

                If Me.JobNameLike_2.Checked Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                End If
            End If


            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike_1.Checked Then
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If

                If Me.JobAddressLike_2.Checked Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                End If
            End If


            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If

                If Me.JobNameLike_2.Checked Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                End If
            End If


            If Me.JobCity.Text.Trim.Length > 0 Then
                If Me.JobCity_1.Checked Then
                    .JobCity = Me.JobCity.Text.Trim & "%"
                End If

                If Me.JobCity_2.Checked Then
                    .JobCity = "%" & Me.JobCity.Text.Trim & "%"
                End If

            End If


            If Me.FilterKey.Text.Trim.Length > 0 Then
                .FilterKey = Me.FilterKey.Text.Trim & "%"
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            End If

            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            End If

            If Me.PONum.Text.Trim.Length > 0 Then
                .PONum = Me.PONum.Text.Trim
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim
            End If

            If Me.JobStatus.Text.Trim.Length > 0 Then
                .StatusCode = Me.JobStatus.Text.Trim & "%"
            End If

            If Me.FileNumber.Text.Trim.Length > 0 Then
                .JobId = Me.FileNumber.Text.Trim
            End If

            If Me.TempId.Text.Trim.Length > 0 Then
                .TempId = Me.TempId.Text.Trim
            End If

            If Me.chkPaidInFull.Checked = True Then
                .ExcludePIF = 1
            End If



            If Me.hdnDrpPropertyType.Value = "Private" Then
                .JobType = 1
            ElseIf Me.hdnDrpPropertyType.Value = "Public" Then
                .JobType = 2
            ElseIf Me.hdnDrpPropertyType.Value = "Residential" Then
                .JobType = 3
            ElseIf Me.hdnDrpPropertyType.Value = "Federal" Then
                .JobType = 4
            ElseIf Me.hdnDrpPropertyType.Value = "All" Then
                .JobType = 0
            Else
                .JobType = 0
            End If
            .UserId = Me.CurrentUser.Id

        End With

        Session("mysproc") = mysproc
        Session("pageIndex") = 1
        GetJobsPageWise(mysproc, 1)
        Me.MultiView1.SetActiveView(Me.View2)

    End Sub


    Protected Sub btnExportReport_Click(sender As Object, e As EventArgs)
        Dim mysproc1 As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
        mysproc1 = Session("mysproc")
        mysproc1.Page = 1
        If IsNothing(Session("TotalRecords")) = False Then
            mysproc1.PageRecords = Session("TotalRecords")
        Else
            mysproc1.PageRecords = 2500
        End If


        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(mysproc1)
        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.JobListExport(mysproc1, Me.PDFReport.Checked))
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If Me.SpreadSheet.Checked = False Then
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If

            Else

                Dim reportexcel As String = System.IO.Path.GetFileName(sreport)
                Session("reportexcel") = reportexcel
                'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideupdate", "hideupdate()", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideLoadingPanel", "HideLoadingPanel()", True)

                Me.DownLoadReport(Session("reportexcel"))



            End If
        End If

        Dim pageURL1 As String = "MainDefault.aspx?lienview.joblistnew&page=1"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + pageURL1 + "'", True)
    End Sub

    Protected Sub drpSortType_SelectedIndexChanged(sender As Object, e As EventArgs)
        mysproc = Session("mysproc")
        GetJobsPageWise(mysproc, 1)
    End Sub
    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        mysproc = Session("mysproc")
        GetJobsPageWise(mysproc, 1)
    End Sub


    Private Function GetSortedData(ByVal SortTypeValue As String, ByVal SortByValue As String, mysprocSort As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1)
        Dim SortType As String = "ASC"
        If SortTypeValue = "-1" Then
            SortType = "ASC"
        ElseIf SortTypeValue = "1" Then
            SortType = "DESC"
        End If

        Dim SortBy As String
        If SortByValue = "-1" Then
            SortBy = "ListId " + SortType
        ElseIf SortByValue = "1" Then
            SortBy = "JobId " + SortType
        ElseIf SortByValue = "2" Then
            SortBy = "JobNum " + SortType
        ElseIf SortByValue = "3" Then
            SortBy = "JobName " + SortType
        ElseIf SortByValue = "4" Then
            SortBy = "JobAdd1 " + SortType
        ElseIf SortByValue = "5" Then
            SortBy = "JobCity " + SortType
        ElseIf SortByValue = "6" Then
            SortBy = "JobState " + SortType
        ElseIf SortByValue = "7" Then
            SortBy = "CustId " + SortType
        ElseIf SortByValue = "8" Then
            SortBy = "ClientCustomer " + SortType
        ElseIf SortByValue = "9" Then
            SortBy = "BranchNum " + SortType
        ElseIf SortByValue = "10" Then
            SortBy = "FilterKey " + SortType
        ElseIf SortByValue = "11" Then
            SortBy = "NoticeSent " + SortType
        ElseIf SortByValue = "12" Then
            SortBy = "NoticeDeadlineDate " + SortType
        ElseIf SortByValue = "13" Then
            SortBy = "LienDeadlineDate " + SortType
        ElseIf SortByValue = "14" Then
            SortBy = "BondDeadlineDate " + SortType
        ElseIf SortByValue = "15" Then
            SortBy = "SNDeadlineDate " + SortType
        ElseIf SortByValue = "16" Then
            SortBy = "NOIDeadlineDate " + SortType
        End If

        mysprocSort.SortColType = SortBy
        myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(mysprocSort)

        If (SortType = "ASC") Then
            If (SortByValue = "15" Or SortByValue = "16" Or SortByValue = "11" Or SortByValue = "12" Or SortByValue = "13" Or SortByValue = "14") Then
                If (SortByValue = "11") Then
                    myviewSorting = SetNullDate("NoticeSent", myviewSorting)
                ElseIf (SortByValue = "12") Then
                    myviewSorting = SetNullDate("NoticeDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "13") Then
                    myviewSorting = SetNullDate("LienDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "14") Then
                    myviewSorting = SetNullDate("BondDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "15") Then
                    myviewSorting = SetNullDate("SNDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "16") Then
                    myviewSorting = SetNullDate("NOIDeadlineDate", myviewSorting)
                End If
            End If
        End If

        If myviewSorting.Count < 1 Then
            Me.gvJobViewList.DataSource = Nothing
            Me.gvJobViewList.DataBind()


        End If
        Session("filterOrder") = SortTypeValue
        Session("filterColumn") = SortByValue
        Session("JobList") = mysprocSort
        Return myviewSorting
    End Function


    Protected Function SetNullDate(ByVal DateField As String, ByVal viewcheckdate As HDS.DAL.COMMON.TableView)
        For Each dr As System.Data.DataRow In viewcheckdate.ToTable.Rows
            If (dr(DateField).ToString = "2079-01-01 00:00:00.000") Then
                dr(DateField) = DBNull.Value

            End If

        Next
        Return viewcheckdate
    End Function
    Public Function GetPropertyName(ByVal prPublic As String, ByVal prPrivate As String, ByVal prResidential As String, ByVal prFederal As String)
        Dim type As String = ""
        If prPublic Then
            type = "Public"
        ElseIf prPrivate = True And prResidential = False Then
            type = "Private"
        ElseIf prPrivate = True And prResidential = True Then
            type = "Residential"
        ElseIf prFederal Then
            type = "Federal"

        End If


        Return type
    End Function

    'Custom Pagging for job list
    Protected Sub PageSize_Changed(ByVal sender As Object, ByVal e As EventArgs)
        mysproc = Session("mysproc")
        Session("pageIndex") = 1
        Me.GetJobsPageWise(mysproc, 1)
    End Sub

    Protected Sub Page_Changed(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageIndex As Integer = Integer.Parse(CType(sender, LinkButton).CommandArgument)
        Session("pageIndex") = pageIndex
        mysproc = Session("mysproc")
        Me.GetJobsPageWise(mysproc, pageIndex)
        'End If
    End Sub

    Private Sub PopulatePager(ByVal recordCount As Integer, ByVal currentPage As Integer)
        Dim dblPageCount As Double = CType((CType(recordCount, Decimal) / Decimal.Parse(ddlPageSize.SelectedValue)), Double)
        Dim pageCount As Integer = CType(Math.Ceiling(dblPageCount), Integer)
        Dim pages As New List(Of ListItem)

        If (pageCount > 0) Then

            Dim showMax As Integer = 5
            Dim startPage As Integer
            Dim endPage As Integer
            If (pageCount <= showMax) Then

                startPage = 1
                endPage = pageCount

            Else

                startPage = currentPage
                endPage = currentPage + showMax - 1

            End If

            pages.Add(New ListItem("<<", "1", (currentPage > 1)))

            For i As Integer = startPage To endPage

                pages.Add(New ListItem(i.ToString, i.ToString, (i <> currentPage)))
            Next

            pages.Add(New ListItem(">>", pageCount.ToString, (currentPage < pageCount)))

        End If
        rptPager.DataSource = pages
        rptPager.DataBind()
        Dim recordsperpage As Integer = CType(ddlPageSize.SelectedValue, Integer)
        Dim FromRecord As Integer = ((recordsperpage * currentPage) - recordsperpage) + 1
        Dim ToRecord As Integer = recordsperpage * currentPage
        Dim info As String = "Showing " & FromRecord & " to " & ToRecord & " of " & recordCount & " entries"
        lblTableInfo.Text = info
    End Sub
    Private Sub GetJobsPageWise(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1, ByVal pageIndex As Integer)
        asproc.Page = pageIndex
        asproc.PageRecords = Integer.Parse(ddlPageSize.SelectedValue)
        'If (IsSort) Then

        myview = GetSortedData(drpSortType.SelectedItem.Value, drpSortBy.SelectedItem.Value, asproc)


        If myview.Count < 1 Then
            Me.gvJobViewList.DataSource = Nothing
            Me.gvJobViewList.DataBind()

            Exit Sub
        End If

        Dim totrecs As String = myview.RowItem("totalrecords")
        Session("TotalRecords") = totrecs
        Me.gvJobViewList.DataSource = myview
        Me.gvJobViewList.DataBind()
        If gvJobViewList.Rows.Count > 0 Then
            Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        Me.MultiView1.SetActiveView(Me.View2)

        Me.PopulatePager(totrecs, pageIndex)
    End Sub


</script>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="width: 100%; min-width :800px" class="margin-bottom-0">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Job Filter</h1>
                    <div class="body" id="JobFilterBody">
                        <div class="form-horizontal">
                            <asp:Panel ID="panClients" runat="server">
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Client Selection</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="text-align: left; padding-left: 7px;" id="clsdropdownClient">
                                        <asp:DropDownList ID="DropDownList1" runat="server" Style="display: none;">
                                        </asp:DropDownList>&nbsp;
                                     
                                        <div class="btn-group" style="text-align: left;" align="left">
                                            <button type="button" class="btn btn-default dropdown-toggle" style="text-align: left;" align="left"
                                                data-toggle="dropdown" aria-expanded="false" id="btnClientSelectionType" tabindex="1">
                                                --Select Client Selection--
                     
                                                                                    <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlClientSelectionType" style="min-width: 450px; overflow-y: auto; height: 150px;">
                                            </ul>
                                        </div>

                                        <asp:HiddenField ID="hdnClientSelectionId" runat="server" />
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                        <div class="radio-custom radio-default radio-inline">
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL" style="white-space: nowrap;"> All Clients</asp:ListItem>
                                                <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED" style="white-space: nowrap; padding-left: 40px;">Selected Only</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                            </asp:Panel>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Customer Name:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="CustomerName" TabIndex="1" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <%--<asp:RadioButtonList ID="CustomerNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1" >
                                                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                                                                <asp:ListItem meta:resourcekey="ListItemResource2"  style="padding-left: 30px;">Includes</asp:ListItem>
                                                                            </asp:RadioButtonList>--%>
                                        <%--<input type="radio" id="CustomerNameLike_1" onclick="myalert"/>--%>
                                        <asp:RadioButton ID="CustomerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerNameLike(1)" TabIndex="2" />
                                        <asp:RadioButton ID="CustomerNameLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerNameLike(2)" TabIndex="3" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Customer Ref:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="CustomerRef" TabIndex="4" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <%--<asp:RadioButtonList ID="CustomerRefLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1" >
                                                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                                                                <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px;">&nbsp;Includes</asp:ListItem>
                                                                            </asp:RadioButtonList>--%>
                                        <asp:RadioButton ID="CustomerRefLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerRefLike(1)" TabIndex="5" />
                                        <asp:RadioButton ID="CustomerRefLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerRefLike(2)" TabIndex="6" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Owner Name:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="OwnerName" CssClass="form-control" meta:resourcekey="OwnerNameResource1" TabIndex="7" MaxLength="45" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <%--<asp:RadioButtonList ID="OwnerNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" meta:resourcekey="OwnerNameLikeResource1">
                                                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource3" >&nbsp;Starts&nbsp;</asp:ListItem>
                                                                                <asp:ListItem meta:resourcekey="ListItemResource4" style="padding-left: 30px;">&nbsp;Includes</asp:ListItem>
                                                                            </asp:RadioButtonList>--%>
                                        <asp:RadioButton ID="OwnerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionOwnerNameLike(1)" TabIndex="8" />
                                        <asp:RadioButton ID="OwnerNameLike_2" runat="server" Text="Includes" onchange="myFunctionOwnerNameLike(2)" TabIndex="9" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Contractor Name:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="GCName" CssClass="form-control" meta:resourcekey="GCNameResource1" MaxLength="45" TabIndex="10" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <%--<asp:RadioButtonList ID="GCNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" meta:resourcekey="GCNameLikeResource1">
                                                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource5">&nbsp;Starts&nbsp;</asp:ListItem>
                                                                                <asp:ListItem meta:resourcekey="ListItemResource6" style="padding-left: 30px;">&nbsp;Includes</asp:ListItem>
                                                                            </asp:RadioButtonList>--%>
                                        <asp:RadioButton ID="GCNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionGCNameLike(1)" TabIndex="11" />
                                        <asp:RadioButton ID="GCNameLike_2" runat="server" Text="Includes" onchange="myFunctionGCNameLike(2)" TabIndex="12" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Lender Name:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="LenderName" CssClass="form-control" meta:resourcekey="LenderNameResource1" MaxLength="45" TabIndex="13" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <%--<asp:RadioButtonList ID="LenderNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" meta:resourcekey="LenderNameLikeResource1">
                                                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource7">&nbsp;Starts&nbsp;</asp:ListItem>
                                                                                <asp:ListItem meta:resourcekey="ListItemResource8" style="padding-left: 30px;">&nbsp;Includes</asp:ListItem>
                                                                            </asp:RadioButtonList>--%>
                                        <asp:RadioButton ID="LenderNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionLenderNameLike(1)" TabIndex="14" />
                                        <asp:RadioButton ID="LenderNameLike_2" runat="server" Text="Includes" onchange="myFunctionLenderNameLike(2)" TabIndex="15" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job #:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobNo" CssClass="form-control" meta:resourcekey="JobNoResource1" MaxLength="45" TabIndex="16" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="JobNoLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNoLike(1)" TabIndex="17" />
                                        <asp:RadioButton ID="JobNoLike_2" runat="server" Text="Includes" onchange="myFunctionJobNoLike(2)" TabIndex="18" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Name:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobName" CssClass="form-control" meta:resourcekey="JobNameResource1" MaxLength="45" TabIndex="19" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <%--<asp:RadioButtonList ID="JobNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" meta:resourcekey="JobNameLikeResource1">
                                                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource9">&nbsp;Starts&nbsp;</asp:ListItem>
                                                                                <asp:ListItem meta:resourcekey="ListItemResource10" style="padding-left: 30px;">&nbsp;Includes</asp:ListItem>
                                                                            </asp:RadioButtonList>--%>
                                        <asp:RadioButton ID="JobNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNameLike(1)" TabIndex="20" />
                                        <asp:RadioButton ID="JobNameLike_2" runat="server" Text="Includes" onchange="myFunctionJobNameLike(2)" TabIndex="21" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Address:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobAddress" CssClass="form-control" meta:resourcekey="JobAddressResource1" MaxLength="45" TabIndex="22" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <%--<asp:RadioButtonList ID="JobAddressLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                                RepeatLayout="Flow" meta:resourcekey="JobAddressLikeResource1">
                                                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource11">&nbsp;Starts&nbsp;</asp:ListItem>
                                                                                <asp:ListItem meta:resourcekey="ListItemResource12" style="padding-left: 30px;">&nbsp;Includes</asp:ListItem>
                                                                            </asp:RadioButtonList>--%>
                                        <asp:RadioButton ID="JobAddressLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobAddressLike(1)" TabIndex="23" />
                                        <asp:RadioButton ID="JobAddressLike_2" runat="server" Text="Includes" onchange="myFunctionJobAddressLike(2)" TabIndex="24" />
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job City:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobCity" CssClass="form-control" MaxLength="20" TabIndex="25" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="JobCity_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobCityLike(1)" TabIndex="26" />
                                        <asp:RadioButton ID="JobCity_2" runat="server" Text="Includes" onchange="myFunctionJobCityLike(2)" TabIndex="27" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job State:</label>
                                <div class="col-md-2  col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="JobState" CssClass="form-control" Width="70px" Style="text-align: center" meta:resourcekey="JobStateResource1" MaxLength="2" TabIndex="28" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Property Type:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <div id="UlPropertyType" style="width:200px;">
                                    </div>
                                    <asp:DropDownList ID="DrpPropertyType" runat="server" Style="display: none;">

                                        <asp:ListItem Text="Public" Value="Public"> </asp:ListItem>
                                        <asp:ListItem Text="Federal" Value="Federal"></asp:ListItem>
                                        <asp:ListItem Text="Residential" Value="Residential"></asp:ListItem>
                                        <asp:ListItem Text="Private" Value="Private"></asp:ListItem>
                                        <asp:ListItem Text="All" Value="All"></asp:ListItem>

                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnDrpPropertyType" runat="server" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Balance:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <%-- <asp:TextBox runat="server" ID="FromBalance" DataType="Money" onblur="CheckJobBalance()"
                                        CssClass="form-control" Width="120px" meta:resourcekey="BranchNoResource1"
                                        MaxLength="15" TabIndex="27" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;Thru&nbsp;
                                   <asp:TextBox runat="server" ID="ThruBalance" CssClass="form-control" onblur="CheckJobBalance()"
                                       Width="120px" meta:resourcekey="BranchNoResource1" MaxLength="15" TabIndex="28" />--%>
                                    <cc1:DataEntryBox ID="FromBalance" runat="server" CssClass="form-control" DataType="Money"
                                        EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                        FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                        Value="0.00" Width="120px" TabIndex="29" AutoPostBack="false"></cc1:DataEntryBox>
                                    &nbsp;&nbsp;&nbsp;Thru&nbsp;
                                       <cc1:DataEntryBox ID="ThruBalance" runat="server" CssClass="form-control" DataType="Money"
                                           EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                           FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                           Value="0.00" Width="120px" TabIndex="30" AutoPostBack="false"></cc1:DataEntryBox>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByBalance" CssClass="redcaption"
                                            runat="server" Text=" Use Job Balance Range"
                                            Width="224px" TabIndex="31" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Assigned Date:</label>
                                <%--   <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <SiteControls:Calendar ID="Calendar1" runat="server" ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" Value="TODAY" TabIndex="25" />
                                    &nbsp;&nbsp;Thru&nbsp;
                                    <SiteControls:Calendar ID="Calendar2" runat="server" ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" Value="TODAY" TabIndex="26"  />
                                </div>--%>


                                <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <asp:TextBox ID="Calendar1" runat="server" Width="120px" onblur="CheckAssignedDateRange()" CssClass="form-control datepicker" IsReadOnly="false" IsRequired="true" TabIndex="32"></asp:TextBox>
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Calendar1" ErrorMessage="From Date is Required">*</asp:RequiredFieldValidator>--%>&nbsp;&nbsp;&nbsp;Thru&nbsp;

                                      <asp:TextBox ID="Calendar2" runat="server" Width="120px" onblur="CheckAssignedDateRange()" CssClass="form-control datepicker" IsReadOnly="false" IsRequired="true" TabIndex="33"></asp:TextBox>&nbsp;
                                     <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Calendar2" ErrorMessage="Thru Date is Required">*</asp:RequiredFieldValidator>--%>
                                </div>


                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByAssignDate" CssClass="redcaption" runat="server" Text=" Use Assigned Date Range" TabIndex="34" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Deadline Date:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <asp:TextBox ID="Calendar3" runat="server" Width="120px" onblur="CheckDeadlineDateRange()" CssClass="form-control datepicker" IsReadOnly="false" IsRequired="true" TabIndex="35"></asp:TextBox>
                                    <%-- <asp:RequiredFieldValidator ID="reqq1" runat="server" ControlToValidate="Calendar3" ErrorMessage="From Date is Required">*</asp:RequiredFieldValidator>--%>&nbsp;&nbsp;&nbsp;Thru&nbsp;

                                      <asp:TextBox ID="Calendar4" runat="server" Width="120px" onblur="CheckDeadlineDateRange()" CssClass="form-control datepicker" IsReadOnly="false" IsRequired="true" TabIndex="36"></asp:TextBox>&nbsp;
                                   <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Calendar4" ErrorMessage="Thru Date is Required">*</asp:RequiredFieldValidator>--%>
                                </div>


                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByDeadlineDate" CssClass="redcaption"
                                            runat="server" Text=" Use Deadline Date Range"
                                            Width="224px" TabIndex="37" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Deadline Type:</label>
                                <div class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="example-wrap" style="margin-bottom: 0;">
                                        <div class="radio-custom radio-default" style="float: left; width: 100px;">
                                            <asp:RadioButton ID="rdoLien" runat="server"
                                                Text="Lien" GroupName="DeadlineFilter" TabIndex="38" />
                                        </div>
                                        <div class="radio-custom radio-default" style="float: left; width: 150px;">
                                            <asp:RadioButton ID="rdoForeclosure" runat="server"
                                                Text="Foreclosure" GroupName="DeadlineFilter" TabIndex="39" />
                                        </div>

                                        <div class="radio-custom radio-default" style="float: left; width: 150px;">
                                            <asp:RadioButton ID="rdoStopNotice" runat="server" Text="Stop Notice" 
                                                GroupName="DeadlineFilter" TabIndex="40" />
                                        </div>
                                        <div class="radio-custom radio-default">
                                            <asp:RadioButton ID="rdoAll" runat="server" Text="All" TabIndex="41"
                                                GroupName="DeadlineFilter" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3  col-sm-3 col-xs-3"></div>
                                <div class="col-md-7 col-sm-7 col-xs-7" style="padding-bottom: 10px">
                                    <div class="example-wrap" style="margin-bottom: 0;">
                                        <div class="radio-custom radio-default" style="float: left; width: 100px;">
                                            <asp:RadioButton ID="rdoBond" runat="server" Text="Bond"
                                                GroupName="DeadlineFilter" TabIndex="42" />
                                        </div>
                                        <div class="radio-custom radio-default" style="float: left; width: 150px;">
                                            <asp:RadioButton ID="rdoBondSuit" runat="server"
                                                Text="Bond Suit" GroupName="DeadlineFilter" TabIndex="43" />
                                        </div>
                                        <div class="radio-custom radio-default" style="float: left; width: 150px;">
                                            <asp:RadioButton ID="rdoNOI" runat="server" Text="NOI" TabIndex="44"
                                                GroupName="DeadlineFilter" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Branch #:</label>
                                <div class="col-md-2  col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="BranchNo" CssClass="form-control" meta:resourcekey="BranchNoResource1" MaxLength="10" TabIndex="44" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">PO #:</label>
                                <div class="col-md-2  col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="PONum" CssClass="form-control" meta:resourcekey="PONoResource1" MaxLength="15" TabIndex="45" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Status:</label>
                                <div class="col-md-2  col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="JobStatus" CssClass="form-control" meta:resourcekey="BranchNoResource1" Width="70px" TabIndex="46" />
                                </div>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkPaidInFull" CssClass="redcaption" runat="server" Text="  Exclude Paid In Full Jobs" TabIndex="47" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Filter Key:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="FilterKey" CssClass="form-control" MaxLength="25" TabIndex="48" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">CRF File#:</label>
                                <div class="col-md-2  col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="FileNumber" CssClass="form-control" meta:resourcekey="FileNumberResource1" TabIndex="50" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">CRF Temp Id #:</label>
                                <div class="col-md-2  col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="TempId" CssClass="form-control" meta:resourcekey="FileNumberResource1" TabIndex="51" />
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-12">
                                    <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                    <asp:Label ID="Message1" runat="server" CssClass="normalred"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">
                                <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="btn btn-primary" CausesValidation="False" Style="margin-top: 5px;" TabIndex="52" />&nbsp;
                                
                                <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>&nbsp;
                            </asp:Panel>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <h1 class="panelheader">Job List</h1>
                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12">
                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">

                                <div class="col-md-12" style="text-align: left;">
                                    <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter" CausesValidation="False"></asp:LinkButton>
                                    <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" CausesValidation="False" OnClientClick="return modalshow();"></asp:LinkButton>
                                    <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="CRFS #" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Job #" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Job Name" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Job Address" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Job City" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Job State" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="CustId" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Cust Name" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Branch#" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="FilterKey" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="NoticeSent" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Notice Deadline Date" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="Lien Deadline Date" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="Bond Deadline Date" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="SN Deadline Date" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="NOI Deadline Date" Value="16"></asp:ListItem>
                                        <%--'##101--%>
                                    </asp:DropDownList>
                                    <%--##101--%>
                                    <asp:DropDownList ID="drpSortType" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortType_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="Ascending" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Descending" Value="1"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>

                        <div style="width: auto; height: auto; overflow: auto;">
                            <%--<asp:GridView ID="example" runat="server" CssClass="table dataTable table-striped" AutoGenerateColumns="False" DataKeyNames="JobId" Width="100%" BorderWidth="1px">
                            
                                <Columns>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
                                           
                                            <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="JobId" SortExpression="JobId" HeaderText="CRFS#">
                                        <ItemStyle HorizontalAlign="Center" Width="40px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="JobName" SortExpression="JobName" HeaderText="Job Name">
                                        <ItemStyle Width="100px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="JobNum" SortExpression="JobNum" HeaderText="Job#">
                                        <ItemStyle Width="60px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CityStateZip" SortExpression="CityStateZip" HeaderText="Job Add">
                                        <ItemStyle Width="120px" Height="15px" Wrap="true" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BranchNum" SortExpression="BranchNum" HeaderText="Branch">
                                        <ItemStyle HorizontalAlign="Center" Width="45px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="StatusCode" SortExpression="StatusCode" HeaderText="Status">
                                        <ItemStyle HorizontalAlign="Center" Width="30px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ClientCustomer" SortExpression="ClientCustomer" HeaderText="Customer">
                                        <ItemStyle Height="15px" Wrap="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DateAssigned" SortExpression="DateAssigned" HeaderText="Assigned">
                                        <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client#">
                                        <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                    </asp:BoundField>

                                </Columns>
                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                            </asp:GridView>--%>

                            <asp:GridView ID="gvJobViewList" runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 12px !important; min-width: 1501px">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <HeaderTemplate>
                                            <table style="width: 100%; table-layout: fixed; border-color: black;" rules="cols">
                                                <tr>
                                                    <td style="width: 35px;"></td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">CRFS #</td>
                                                    <td style="width: 15%;" class="nowordwraptd">Job #</td>
                                                    <td style="width: 20%;" class="nowordwraptd">Job Information</td>
                                                    <td style="width: 20%; word-break: break-all;" class="nowordwraptd">Customer</td>
                                                    <td style="width: 95px;" class="nowordwraptd">Branch#</td>
                                                    <td style="width: 110px;" class="nowordwraptd">Assigned Date</td>
                                                    <td colspan="3" style="width: 393px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">View</td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">Client #</td>
                                                    <td style="width: 15%; word-break: break-all;" class="nowordwraptd">Job Status</td>
                                                    <td style="width: 20%; word-break: break-all;" class="nowordwraptd">Property Type</td>
                                                    <td style="width: 20%;" class="nowordwraptd">Cust#</td>
                                                    <td style="width: 95px;" class="nowordwraptd">Filter Key</td>
                                                    <td style="width: 110px;" class="nowordwraptd">Notice Sent</td>
                                                    <td colspan="3" style="width: 393px" class="nowordwraptd">Deadline Dates</td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle CssClass="RemovePadding" />
                                        <ItemStyle CssClass="RemovePadding" />
                                        <ItemTemplate>
                                            <table id="TableJobViewList" style="width: 100%; border-color: black; font-size: 14px; table-layout: fixed;" rules="cols">
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">
                                                        <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server" /></td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobid" runat="server" Text='<%# Eval("JobId")%>'></asp:Label></td>
                                                    <td style="width: 15%; max-width: 18%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobNum" runat="server" Text='<%# Eval("JobNum")%>'></asp:Label>

                                                    </td>
                                                    <td style="width: 20%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobName" runat="server" Text='<%# Eval("JobName")%>'></asp:Label></td>
                                                    <td style="width: 20%;" class="nowordwraptd">

                                                        <asp:Label ID="lblCustName" runat="server" Text='<%# Eval("ClientCustomer")%>'></asp:Label></td>
                                                    <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblBranchNum" runat="server" Text='<%# Eval("BranchNum")%>'></asp:Label></td>
                                                    <td style="width: 110px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblDateAssigned" runat="server" Text='<%#Utils.FormatDate(Eval("DateAssigned"))%>'></asp:Label></td>
                                                    <td style="width: 135px;" class="nowordwraptd"><b style="font-weight: bold;">Notice - </b>
                                                        <asp:Label ID="lblNoticeDate" runat="server" Text='<%#Utils.FormatDate(Eval("NoticeDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnNoticeSent" runat="Server" Value='<%#Eval("NoticeSent")%>' />
                                                    </td>

                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">Bond - </b>
                                                        <asp:Label ID="lblBondDate" runat="server" Text='<%#Utils.FormatDate(Eval("BondDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnBondDate" runat="Server" Value='<%#Eval("BondDate")%>' />
                                                    </td>
                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">SN - </b>
                                                        <asp:Label ID="lblSNDate" runat="server" Text='<%#Utils.FormatDate(Eval("SNDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnSNDate" runat="Server" Value='<%#Eval("SNDate")%>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 35px"></td>
                                                    <td style="text-align: center; width: 71px; word-break: break-all;" class="nowordwraptd">
                                                        <asp:Label ID="lblClientCode" runat="server" Text='<%# Eval("ClientCode")%>'></asp:Label></td>
                                                    <td class="nowordwraptd">
                                                        <asp:Label ID="lblStatusCode" runat="server" Text='<%# String.Format("{0} - {1}", Eval("StatusCode"), Eval("JobStatusDescr")) %>'></asp:Label>
                                                    </td>
                                                    <td style="width: 18%; max-width: 16%; word-break: break-all;" class="nowordwraptd">
                                                        <asp:Label ID="lblCityStateZip" runat="server" Text='<%# Eval("CityStateZip") %>'></asp:Label>

                                                        <td style="word-break: break-all;" class="nowordwraptd">

                                                            <%--<asp:Label ID="lblCustId" runat="server" Text='<%# Eval("CustId")%>'></asp:Label>--%>
                                                            <asp:Label ID="lblCustId" runat="server" Text='<%# Eval("CustRefNum")%>'></asp:Label>
                                                        </td>
                                                        <td style="word-break: break-all; text-align: center;" class="nowordwraptd">
                                                            <asp:Label ID="lblFilterKey" runat="server" Text='<%# Eval("FilterKey")%>'></asp:Label>
                                                        </td>
                                                        <td style="word-break: break-all; text-align: center;" class="nowordwraptd">
                                                            <asp:Label ID="lblNoticeSent" runat="server" Text='<%# Utils.FormatDate(Eval("NoticeSent"))%>'></asp:Label>
                                                        </td>
                                                        <td class="nowordwraptd"><b style="font-weight: bold;">Lien - </b>
                                                            <asp:Label ID="lblLienDate" runat="server" Text='<%#Utils.FormatDate(Eval("LienDeadlineDate"))%>'></asp:Label>
                                                            <asp:HiddenField ID="hdnLienDate" runat="Server" Value='<%#Eval("LienDate")%>' />
                                                        </td>
                                                        <td class="nowordwraptd"><b style="font-weight: bold;">Suit - </b>
                                                            <asp:Label ID="lblSuit" runat="server" Text='<%#Utils.FormatDate(Eval("BondSuitDeadlineDate")) %>'></asp:Label></td>
                                                        <asp:HiddenField ID="hdnBondSuitDate" runat="server" Value='<%#Utils.FormatDate(Eval("BondSuitDate")) %>' />
                                                        <asp:HiddenField ID="hdnPublicJob" runat="server" Value='<%# Eval("PublicJob")%>' />

                                                        <td class="nowordwraptd"><b style="font-weight: bold;">NOI - </b>
                                                            <asp:Label ID="lblNOIDeadline" runat="server" Text='<%# Utils.FormatDate(Eval("NOIDeadlineDate"))%>'></asp:Label></td>

                                                    </td>

                                                </tr>
                                                <tr>
                                                    <td style="width: 35px"></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="width: 18%; max-width: 16%; word-break: break-all;" class="nowordwraptd">
                                                        <asp:Label ID="lblPropertyType" runat="server" Text='<%# GetPropertyName(DataBinder.GetPropertyValue(Container.DataItem, "PublicJob"), DataBinder.GetPropertyValue(Container.DataItem, "PrivateJob"), DataBinder.GetPropertyValue(Container.DataItem, "ResidentialBox"), DataBinder.GetPropertyValue(Container.DataItem, "FederalJob")) %>'></asp:Label>

                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="nowordwraptd"><b style="font-weight: bold;">Foreclose - </b>
                                                        <asp:Label ID="lblForeClose" runat="server" Text='<%#Utils.FormatDate(Eval("ForeclosureDeadlineDate"))%>'></asp:Label>

                                                        <asp:HiddenField ID="hdnForeclosureDate" runat="Server" Value='<%#Eval("ForeclosureDate")%>' />
                                                        <asp:HiddenField ID="hdndateassigned" runat="server" Value='<%#Utils.FormatDate(Eval("DateAssigned")) %>' />
                                                    </td>
                                                    <td class="nowordwraptd"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                            </asp:GridView>
                            <br />
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-right:0px">
                            <div class="col-md-8 col-sm-8 col-xs-8" style="text-align:left">
                               <asp:Label ID="lblTableInfo" runat="server" Style="font-size:14px"></asp:Label>
                                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageSize_Changed">
                                    <asp:ListItem Text="10" Value="10" />
                                    <asp:ListItem Text="25" Value="25" />
                                    <asp:ListItem Text="50" Value="50" />
                                </asp:DropDownList>
                            </div>
                            <div style="padding-right:0px" class="col-md-4 col-sm-4 col-xs-4">
                                <ul class="pagination pull-right">
                                    <asp:Repeater ID="rptPager" runat="server">
                                        <ItemTemplate>
                                            <li class="page-item">
                                                <asp:LinkButton ID="lnkPage" CssClass='<%# If(Eval("Enabled") Or (Eval("Enabled") = False And Eval("Text") = "<<") Or (Eval("Enabled") = False And Eval("Text") = ">>"), "page-link ", "page-link activePage")%>'  runat="server"  CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' Text='<%#Eval("Text") %>' OnClick="Page_Changed">
                                                </asp:LinkButton>
                                            
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </ul>
                            </div>
</div>
                        </div>
                    </div>
                    <div class="modal fade modal-primary example-modal-lg" id="modelDivExport" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
                        tabindex="-1" style="display: none;" data-backdrop="static">
                        <div class="modal-dialog modal-center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">�</span>

                                    </button>

                                    <h3 class="modal-title">Export Job List !!</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">

                                        <h4 class="modal-title">What Type Of Export Would You Like ? </h4>
                                        <div class="form-group" style="padding-top: 20px; padding-left: 150px">

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PDFReport" runat="server" Checked="True" GroupName="ReportType" Text="PDF" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="SpreadSheet" runat="server" GroupName="ReportType" Text="Spreadsheet" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer" style="text-align: center">
                                    <asp:Button ID="btnExportReport" runat="server" CssClass="btn btn-primary" OnClick="btnExportReport_Click" Style="display: none" />
                                    <button type="button" id="btnSubmitReport" aria-label="Submit" class="btn btn-primary" onclick="modalhide();" />
                                    <span>Submit</span>


                                    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Cancel">
                                        <span>Cancel</span>
                                </div>
                            </div>
                        </div>
                    </div>

                </asp:View>
            </asp:MultiView>
        </div>
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif" AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center" HorizontalOffset="150" VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>

