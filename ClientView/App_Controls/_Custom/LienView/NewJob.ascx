﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewJob.ascx.vb"
    Inherits="App_Controls__Custom_LienView_NewJob" %>
<%@ Register Src="GCSearch.ascx" TagName="GCSearch" TagPrefix="uc2" %>
<%@ Register Src="CustNameSearch.ascx" TagName="CustNameSearch" TagPrefix="uc1" %>
<%@ Register Src="AddLegalPty.ascx" TagName="AddLegalPty" TagPrefix="uc3" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<script language="javascript" type="text/javascript">
    function callbtnevnt() {

        document.getElementById('<%=btngetstate.ClientID%>').click();
    }
   <%-- function OwnrSameChanged() {

        document.getElementById('=btnOwnrSame.ClientID').click();
    }--%>
    function OpenTreasurySuretyAddressList() {
        var strpath = "http://www.fiscal.treasury.gov/fsreports/ref/suretyBnd/c570_a-z.htm";
        window.open(strpath, "mywindow", "menubar=1,resizable=1,width=1050,height=800,scrollbars=yes");
        return false;
    }



</script>
<script language="javascript" type="text/javascript">
    function noenter(e) {
       // debugger;
       // alert(e.target.id);
        //Added by jay for the search button click
       
            if ((e.target || e.srcElement).id == "ctl06_ctl00_TabContainer1_TabPanel2_CustNameSearch1_TextBox1" || (e.target || e.srcElement).id == "ctl06_ctl00_TabContainer1_TabPanel3_GCSearch1_TextBox1") {
            e = e || window.event;
            var key = e.keyCode || e.charCode;
            if (key == 13) {
                if ((e.target || e.srcElement).id == "ctl06_ctl00_TabContainer1_TabPanel2_CustNameSearch1_TextBox1")
                    callsearch();
                else if ((e.target || e.srcElement).id == "ctl06_ctl00_TabContainer1_TabPanel3_GCSearch1_TextBox1")
                    callsearchGC();
               // event.returnValue = false;
                (event.preventDefault) ? event.preventDefault() : event.returnValue = false;
                //  e.preventDefault();
            }
        }
        else {
            e = e || window.event;
            var key = e.keyCode || e.charCode;
            return key !== 13;
        }
    }
    function IsCheckBoxChecked()
    {
        var chkStatus = document.getElementById('<%=GreenCard.ClientID%>');
        if (!chkStatus.checked) {
            document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "inline";
        } else { document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "none"; }
    }
    window.onload=function() {
   
        var chkStatus = document.getElementById('<%=GreenCard.ClientID%>');
        if (!chkStatus.checked) {
            document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "inline";
        } else { document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "none"; }

   
    };
</script>
<div id="modcontainer" style="width: 700px">
    <h1 class="panelheader">New Job</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body" onkeypress="return noenter(event)">
                <asp:Button ID="btnBack" CssClass="button" runat="server" Text="Back to List"
                    CausesValidation="False" />
                <br />
                <br />
                <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="tabs"
                    ActiveTabIndex="3">
                    <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Job Info">
                        <ContentTemplate>
                            <table width="500">
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Service Type:
                                    </td>
                                    <td class="row-data" valign="top" style="width: 150px">

                                        <asp:RadioButton ID="PrelimBox" runat="server" CssClass="checkbox" Text=" Verify Job Data & Send Notice"
                                            GroupName="JobType" Checked="True"></asp:RadioButton><br />
                                        <asp:RadioButton ID="PrelimASIS" runat="server" CssClass="checkbox" Text=" Send Notice With Data Provided"
                                            GroupName="JobType"></asp:RadioButton><br />
                                        <asp:RadioButton ID="VerifyJob" runat="server" CssClass="checkbox" Text=" Verify Job Data Only - No Notice Sent"
                                            GroupName="JobType"></asp:RadioButton><br />
                                        <asp:RadioButton ID="VerifyJobASIS" runat="server" CssClass="checkbox" Text=" Store Job Data As Provided - No Notice Sent"
                                            GroupName="JobType"></asp:RadioButton><br />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" style="width: 125px">Client#:</td>
                                    <td class="row-data">
                                        <cc1:ExtendedDropDown ID="ClientTableId" CssClass="dropdown" runat="server" Width="296px"
                                            AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                            IsValid="True" Tag="" Value="">
                                        </cc1:ExtendedDropDown>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Branch#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="BranchNum" runat="server" CssClass="textbox" Width="175px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Job#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="JobNum" runat="server" CssClass="textbox" Width="175px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Job Name:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="JobName" runat="server" CssClass="textbox" Width="296px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="JobAddr1" runat="server" CssClass="textbox" Width="296px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px"></td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="JobAddr2" runat="server" CssClass="textbox" Width="296px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">City:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="JobCity" runat="server" CssClass="textbox" Width="296px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                            
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">State/Zip:</td>
                                    <td class="row-data">
                                        <cc1:ExtendedDropDown ID="StateTableId" OnSelectedIndexChanged="StateTableId_SelectedIndexChanged" CssClass="dropdown" runat="server" Width="125px"
                                            AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                            IsValid="True" Tag="" Value="">
                                        </cc1:ExtendedDropDown>&nbsp;
                                        <cc1:DataEntryBox ID="JobZip" runat="server" CssClass="textbox" Width="75px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">State Alerts:
                                    </td>
                                    <td class="row-data">
                                        <asp:TextBox ID="txtstatealert" runat="server" TextMode="MultiLine" ReadOnly="True"
                                            Height="45px" Width="296px" ForeColor="Red" CssClass="textbox" Font-Names="Arial"
                                            Font-Size="Small"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">First Furnished</td>
                                    <td class="row-data">
                                        <SiteControls:Calendar ID="StartDate" runat="server" ErrorMessage="First Furnished Date" IsReadOnly="false"
                                            IsRequired="true" Value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Last Furnished</td>
                                    <td class="row-data">
                                        <SiteControls:Calendar ID="EndDate" runat="server" ErrorMessage="Last Furnished Date" IsReadOnly="false"
                                            IsRequired="false" Value="" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Est Balance:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="EstBalance" runat="server" CssClass="textbox" DataType="Money"
                                            EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                            Value="0.00" Width="88px">$0.00</cc1:DataEntryBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">APN:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="APNNUM" runat="server" CssClass="textbox" Width="175px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">PO#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="PONUM" runat="server" CssClass="textbox" Width="175px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">NOC/Folio#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="FolioNUm" runat="server" CssClass="textbox" Width="175px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Building Permit#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="BuildingPermitNum" runat="server" CssClass="textbox" Width="175px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table width="500">
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Check Boxes:
                                    </td>
                                    <td class="row-data" valign="top" style="width: 110px">
                                        <asp:CheckBox ID="RushOrder" runat="server" CssClass="checkbox" Text=" Prelim Rush"></asp:CheckBox><br />
                                        <asp:CheckBox ID="GreenCard" runat="server" CssClass="checkbox" onclick="IsCheckBoxChecked();" Text=" Return Receipt"></asp:CheckBox><br />
                                        <asp:CheckBox ID="JointCk" runat="server" CssClass="checkbox" Text=" Joint Check"></asp:CheckBox><br />
                                        <asp:CheckBox ID="NOCBox" runat="server" CssClass="checkbox" Text=" Notice of Comp Search"></asp:CheckBox><br />
                                    </td>
                                    <td class="row-data" valign="top" style="width: 180px;">
                                        <asp:CheckBox ID="PublicJob" runat="server" CssClass="checkbox" Text=" Public Job"></asp:CheckBox><br />
                                        <asp:CheckBox ID="FederalJob" runat="server" CssClass="checkbox" Text=" Federal Job"></asp:CheckBox><br />
                                        <asp:CheckBox ID="ResidentialBox" runat="server" CssClass="checkbox" Text=" Residential"></asp:CheckBox><br />
                                    </td>
                                </tr>
                            </table>
                            <div align="left">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Label ID="lblmsg" runat="server" Text="To request a Mechanics Lien on job, please call us at 805-823-8032."
                                    Font-Names="Trebuchet MS" ForeColor="Red" Font-Size="Small"></asp:Label>
                                  <asp:Label ID="lblReturnRcptMsg"  runat="server" Text=" Return Receipts after mailing are no longer available from the US Post Office. Return Receipts must be requested prior to mailing in order to obtain a copy of the recipient’s signature."
                                    Font-Names="Trebuchet MS" ForeColor="Red"  Font-Size="Small"></asp:Label>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Customer">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Name:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustName" runat="server" CssClass="textbox" Width="256px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        <uc1:CustNameSearch ID="CustNameSearch1" runat="server" />
                                        <asp:LinkButton ID="btnEditCustDet" CssClass="button" Visible="false" runat="server" Text="Edit" />

                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Ref#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustRef" runat="server" CssClass="textbox" Width="256px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address1:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustAdd1" runat="server" CssClass="textbox" Width="256px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address2:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustAdd2" runat="server" CssClass="textbox" Width="256px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">City:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustCity" runat="server" CssClass="textbox" Width="256px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">State/Zip:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustState" runat="server" CssClass="textbox" Style="text-align: center"
                                            Width="25px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                        <cc1:DataEntryBox ID="CustZip" runat="server" CssClass="textbox" Width="75px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Phone:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustPhone" runat="server" CssClass="textbox" Style="text-align: center"
                                            Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="true" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Fax:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustFax" runat="server" CssClass="textbox" Style="text-align: center"
                                            Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Email:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustEmail" runat="server" CssClass="textbox" Width="256px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Cust Job#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="CustJobNum" runat="server" CssClass="textbox" Width="113px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="General Contractor">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Name:</td>
                                    <td class="row-data" valign="top">
                                        <cc1:DataEntryBox ID="GCName" TabIndex="31" runat="server" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCName" BehaviorID="FTBExtGCName" runat="server" TargetControlID="GCName"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`">
                                        </ajaxToolkit:FilteredTextBoxExtender>

                                        &nbsp;<uc2:GCSearch ID="GCSearch1" TabIndex="9999" runat="server" />
                                        <asp:LinkButton ID="btnEditGeneralCont" CssClass="button" Visible="false" runat="server" Text="Edit" />

                                        <asp:CheckBox ID="GCSameAsCust" runat="server" CssClass="checkbox" Text="  Same As Customer"
                                            Width="160px" AutoPostBack="True" TabIndex="9999" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Ref#:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="GCRef" TabIndex="32" runat="server" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCRef" BehaviorID="FTBExtGCRef" runat="server" TargetControlID="GCRef"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address1:</td>
                                    <td class="row-data" valign="top">
                                        <cc1:DataEntryBox ID="GCAdd1" runat="server" TabIndex="33" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCAdd1" BehaviorID="FTBExtGCAdd1" runat="server" TargetControlID="GCAdd1"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`">
                                        </ajaxToolkit:FilteredTextBoxExtender>

                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address2:</td>
                                    <td class="row-data" valign="top">
                                        <cc1:DataEntryBox ID="GCAdd2" runat="server" TabIndex="34" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCAdd2" BehaviorID="FTBExtGCAdd2" runat="server" TargetControlID="GCAdd2"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">City:</td>
                                    <td class="row-data" valign="top">
                                        <cc1:DataEntryBox ID="GCCity" runat="server" TabIndex="35" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;

                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCCity" BehaviorID="FTBExtGCCity" runat="server" TargetControlID="GCCity"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`">
                                        </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">State/Zip:</td>
                                    <td class="row-data" valign="top">
                                        <cc1:DataEntryBox ID="GCState" runat="server" TabIndex="36" CssClass="textbox" Style="text-align: center"
                                            Width="25px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCState" BehaviorID="FTBExtGCState" runat="server" TargetControlID="GCState"
                                            FilterInterval="20" FilterMode="ValidChars" FilterType="UppercaseLetters">
                                        </ajaxToolkit:FilteredTextBoxExtender>

                                        <cc1:DataEntryBox ID="GCZip" runat="server" TabIndex="37" CssClass="textbox" Width="75px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;

                                         <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCZip" BehaviorID="FTBExtGCZip" runat="server" TargetControlID="GCZip"
                                             FilterInterval="20" FilterMode="ValidChars" ValidChars="1234567890-">
                                         </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Phone:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="GCPhone" runat="server" TabIndex="38" CssClass="textbox" Style="text-align: center"
                                            Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;

                                         <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCPhone" BehaviorID="FTBExtGCPhone" runat="server" TargetControlID="GCPhone"
                                             FilterInterval="20" FilterMode="ValidChars" ValidChars="1234567890-">
                                         </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Fax:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="GCFax" runat="server" TabIndex="39" CssClass="textbox" Style="text-align: center"
                                            Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;

                                          <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCFax" BehaviorID="FTBExtGCFax" runat="server" TargetControlID="GCFax"
                                              FilterInterval="20" FilterMode="ValidChars" ValidChars="1234567890-">
                                          </ajaxToolkit:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Email:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="GCEmail" runat="server" CssClass="textbox" Width="256px" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" HeaderText="Owner">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Name:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="OwnrName" TabIndex="41" runat="server" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <asp:CheckBox ID="OwnrSameAs" runat="server" CssClass="checkbox" Text="  Same As GC"
                                            Width="150px" AutoPostBack="True" CausesValidation="false" />

                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address1:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="OwnrAdd1" TabIndex="42" runat="server" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <asp:CheckBox ID="CopyJob" runat="server" CssClass="checkbox" Text="  Copy Job Info"
                                            Width="150px" AutoPostBack="True" CausesValidation="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address2:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="OwnrAdd2" TabIndex="42" runat="server" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <asp:CheckBox ID="MLAgent" runat="server" CssClass="checkbox" Text="   Show As Lien Agent"
                                            Width="150px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">City:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="OwnrCity" runat="server" TabIndex="43" CssClass="textbox" Width="256px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">State/Zip:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="OwnrState" runat="server" TabIndex="44" CssClass="textbox"
                                            Style="text-align: center" Width="25px" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <cc1:DataEntryBox ID="OwnrZip" runat="server" TabIndex="45" CssClass="textbox" Width="75px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Phone:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="OwnrPhone" runat="server" TabIndex="46" CssClass="textbox"
                                            Style="text-align: center" Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Fax:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="OwnrFax" runat="server" TabIndex="47" CssClass="textbox" Style="text-align: center"
                                            Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel5" runat="server" HeaderText="Lender/Surety">
                        <ContentTemplate>
                            <table>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Name:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="LenderName" runat="server" TabIndex="51" CssClass="textbox"
                                            Width="256px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                        <asp:CheckBox ID="SuretyBox" runat="server" CssClass="checkbox" Text="  Is Surety"
                                            Width="150px" AutoPostBack="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address1:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="LenderAdd1" TabIndex="52" runat="server" CssClass="textbox"
                                            Width="256px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                            <asp:LinkButton ID="TreasurySuretyAddressList" runat="server" Text="Treasury Surety Address List" Style="cursor: pointer" OnClientClick="OpenTreasurySuretyAddressList();"></asp:LinkButton>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Address2:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="LenderAdd2" TabIndex="52" runat="server" CssClass="textbox"
                                            Width="256px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">City:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="LenderCity" TabIndex="53" runat="server" CssClass="textbox"
                                            Width="256px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">State/Zip:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="LenderState" runat="server" TabIndex="54" CssClass="textbox"
                                            Style="text-align: center" Width="25px" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <cc1:DataEntryBox ID="LenderZip" runat="server" TabIndex="55" CssClass="textbox"
                                            Width="75px" DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Phone:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="LenderPhone" runat="server" TabIndex="56" CssClass="textbox"
                                            Style="text-align: center" Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Fax:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="LenderFax" runat="server" TabIndex="57" CssClass="textbox"
                                            Style="text-align: center" Width="90px" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td class="row-label" valign="top" style="width: 125px">Bond #:</td>
                                    <td class="row-data">
                                        <cc1:DataEntryBox ID="BondNum" runat="server" TabIndex="58" CssClass="textbox" Width="120px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel6" runat="server" HeaderText="Add Legal Parties">
                        <ContentTemplate>
                            <uc3:AddLegalPty ID="AddLegalPty1" runat="server" />

                            <asp:GridView ID="gvwAddLegal" Width="650px" CssClass="gridview" runat="server" AutoGenerateColumns="False"
                                DataKeyNames="Id" CellPadding="4" GridLines="None" PageSize="20">
                                <RowStyle CssClass="rowstyle" />
                                <AlternatingRowStyle CssClass="altrowstyle" />
                                <HeaderStyle CssClass="headerstyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <%--<asp:ImageButton ID="btnEditLP" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png"
                                                runat="server" Width="15px" />--%>
                                            <asp:LinkButton ID="btnEditLP" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" Wrap="False" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TypeCode" SortExpression="TypeCode" HeaderText="TypeCode"></asp:BoundField>
                                    <asp:BoundField DataField="AddressName" SortExpression="AddressName" HeaderText="AddressName"></asp:BoundField>
                                    <asp:BoundField DataField="AddressLine1" SortExpression="AddressLine1" HeaderText="AddressLine1"></asp:BoundField>
                                    <asp:BoundField DataField="City" SortExpression="City" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="State" SortExpression="State" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="PostalCode" SortExpression="PostalCode" HeaderText="PostalCode"></asp:BoundField>
                                    <asp:BoundField DataField="TelePhone1" SortExpression="TelePhone1" HeaderText="TelePhone1"></asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="btnDeleteLP" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif"
                                                runat="server" Width="15px" />
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" Wrap="False" />
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <b>No Records found for the specified criteria</b>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="TabPanel7" runat="server" HeaderText="Misc">
                        <ContentTemplate>
                            <asp:Panel ID="panInst" runat="server">
                                <table id="Table6" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="row-label" style="width: 110px">Special Instruction:
                                        </td>
                                        <td class="row-data" style="width: 490px">
                                            <cc1:DataEntryBox ID="SpecialInstruction" runat="server" Width="490px" CssClass="textbox"
                                                Tag="Special Instruction" IsRequired="False" Height="60px" TextMode="MultiLine"
                                                TabIndex="53" BackColor="White" BlankOnZeroes="True" DataType="Any" ErrorMessage=""
                                                IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False"
                                                FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc1:DataEntryBox>
                                    </tr>
                                    <tr>
                                        <td class="row-label" style="width: 110px"></td>
                                        <td class="row-label1" style="width: 490px">(If Special Instruction is time sensitive, please contact Stacy at 805-823-8032)</td>
                                    </tr>
                                    <tr>
                                        <td class="row-label" style="width: 110px">Legal Description:</td>
                                        <td class="row-data" style="width: 500px">
                                            <cc1:DataEntryBox ID="Note" runat="server" Width="490px" CssClass="textbox" Tag="Special Instruction"
                                                IsRequired="False" Height="52px" TextMode="MultiLine" TabIndex="54" BackColor="White"
                                                BlankOnZeroes="True" DataType="Any" ErrorMessage="" IsValid="True" NormalCSS="textbox"
                                                RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask=""
                                                FormatString="" FriendlyName="" ValidationExpression=""></cc1:DataEntryBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="panRentalINfo" runat="server">
                                <table id="Table5" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="row-label" style="width: 110px">RA #:</td>
                                        <td class="row-data" width="500">
                                            <cc1:DataEntryBox ID="RANum" runat="server" CssClass="textbox" IsRequired="False"
                                                TabIndex="56" tag="RANum" Width="210px" DataType="Any" BackColor="White" BlankOnZeroes="True"
                                                ErrorMessage="" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox"
                                                Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName=""
                                                ValidationExpression=""></cc1:DataEntryBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="row-label" style="width: 110px">Equip Rate:</td>
                                        <td class="row-data" width="500">
                                            <cc1:DataEntryBox ID="EquipRate" runat="server" CssClass="textbox" IsRequired="False"
                                                TabIndex="57" tag="EquipRate" Width="125px" DataType="Any" BackColor="White"
                                                BlankOnZeroes="True" ErrorMessage="" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox"
                                                Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName=""
                                                ValidationExpression=""></cc1:DataEntryBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="row-label" style="width: 110px">Equip Description:</td>
                                        <td class="row-data" width="500">
                                            <cc1:DataEntryBox ID="EquipRental" runat="server" CssClass="textbox" IsRequired="False"
                                                TabIndex="58" tag="EquipRental" Width="490px" BackColor="White" BlankOnZeroes="True"
                                                DataType="Any" ErrorMessage="" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox"
                                                Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName=""
                                                ValidationExpression=""></cc1:DataEntryBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="panAZLotAllocate" runat="server">
                                <table id="Table7" cellspacing="0" cellpadding="0" width="100%" border="0">
                                    <tr>
                                        <td class="row-label" style="width: 110px">Lot Allocation:</td>
                                        <td class="row-data" width="500">
                                            <cc1:DataEntryBox ID="AZLotAllocate" runat="server" Width="490px" CssClass="textbox"
                                                Tag="Special Instruction" IsRequired="False" Height="52px" MaxLength="150" TextMode="MultiLine"
                                                TabIndex="59" DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask=""
                                                FormatString="" FriendlyName="" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                    <ajaxToolkit:TabPanel ID="tabdocs" runat="server" HeaderText="Docs">
                        <ContentTemplate>
                            <div style="display: block">
                                <div style="margin: 2px 0px 4px 0px;">
                                    <asp:Button ID="btnAddDocuments" runat="server" CssClass="button" CausesValidation="False"
                                        Text="Add Document" />
                                </div>

                                <asp:GridView ID="gvwDocs" runat="server" CssClass="gridview" Width="650px" PageSize="20"
                                    AutoGenerateColumns="False">
                                    <AlternatingRowStyle CssClass="altrowstyle"></AlternatingRowStyle>
                                    <Columns>
                                        <asp:TemplateField HeaderText="Print">
                                            <ItemTemplate>
                                                &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnPrntDocs" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png"
                                                    runat="server" Width="15px" />
                                            </ItemTemplate>
                                            <ItemStyle Wrap="False" Width="40px"></ItemStyle>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DateCreated" HeaderText="Date Added" SortExpression="DateCreated">
                                            <ItemStyle HorizontalAlign="Center" Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UserCode" HeaderText="User" SortExpression="UserCode">
                                            <ItemStyle Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DocumentType" HeaderText="Type" SortExpression="DocumentType">
                                            <ItemStyle Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DocumentDescription" HeaderText="Description" SortExpression="DocumentDescription">
                                            <ItemStyle Wrap="True" Height="15px" Width="200px"></ItemStyle>
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <b>No Items found for the specified criteria</b>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="headerstyle"></HeaderStyle>
                                    <RowStyle CssClass="rowstyle"></RowStyle>
                                </asp:GridView>

                                <div style="float: left; margin-top: 30px; height: 100%;">
                                    <ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1" runat="server" BehaviorID="FileUpLoad"
                                        TargetControlID="btnAddDocuments" PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground"
                                        OkControlID="" />
                                    <asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none"
                                        Width="452px" Height="100px">
                                        <h1 class="panelheader">
                                            <div style="width: 100%;">
                                                <div style="text-align: center; width: 90%; float: left;">
                                                    Upload Documents
                                                </div>
                                                <div style="float: right; width: 10%;">
                                                    <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="Return" Width="40px" CssClass="button" />
                                                </div>
                                            </div>
                                        </h1>
                                        <iframe runat="server" id="frm" src="App_Controls/_Custom/LienView/Default.aspx?id=1&jobid=0"
                                            style="width: 450px; height: 270px; background-color: White"></iframe>
                                    </asp:Panel>
                                </div>
                            </div>
                        </ContentTemplate>
                    </ajaxToolkit:TabPanel>
                </ajaxToolkit:TabContainer>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
                <cc1:CustomValidator ID="customvalidator1" runat="server">
                </cc1:CustomValidator>
            </div>
            <div class="footer">
                <asp:UpdatePanel ID="updPan1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" CssClass="button" runat="server" CausesValidation="false"
                            Text="Submit Job" />&nbsp;
                        <br />
                        <br />

                        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="TransparentGrayBackground">
                                </div>
                                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                    <div class="PageUpdateProgress">
                                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                                            AlternateText="[image]" />
                                        &nbsp;Please Wait...
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                    TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
                                    VerticalSide="Middle" VerticalOffset="0">
                                </ajaxToolkit:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>

        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <asp:Button ID="btnExitViewer" runat="server" CssClass="button" Text="Exit Viewer"></asp:Button>
                <asp:Button ID="btnDownLoad" Text="DownLoad" CssClass="button" runat="server" />
                <br />
                <br />
                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                <br />
                <center>
                </center>
            </div>
        </asp:View>
    </asp:MultiView>
    <asp:Button ID="btngetstate" runat="server" Style="display: none;" />
    <%--    <asp:Button ID="btnOwnrSame" runat="server" Style="display: none;" CausesValidation="false" />--%>
</div>
