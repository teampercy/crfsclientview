Imports System.Collections.Generic
Partial Class ClientViewLiens_CustNameSearch
    Inherits System.Web.UI.UserControl
    Public Event ItemSelected()
    Public Event SearchSelected(ByVal searchkey As String)
    Public Sub ClearData()
        Me.ListBox1.Items.Clear()
        Me.TextBox1.Text = ""
        Me.ListBox1.Visible = False

    End Sub
    Public Sub ShowList(ByVal searchkey As String)
        Me.ListBox1.Items.Clear()
        Me.TextBox1.Text = searchkey
        LoadList()

        Me.ModalPopupExtender1.Show()

    End Sub

    Public ReadOnly Property SelectedValue() As String
        Get
            Return Me.myValue.Value
        End Get
    End Property
    Public Property SelectClientId() As String
        Get
            Return Me.myClientId.Value
        End Get
        Set(ByVal value As String)
            Me.myClientId.Value = value
            LoadList()
        End Set
    End Property
    Private Sub LoadList()

        Dim S As String = Me.RadioButtonList1.SelectedValue

        If Me.TextBox1.Text.Length < 1 Then Exit Sub
        Me.ListBox1.Items.Clear()

        Dim aOut As ArrayList = New ArrayList

        Dim myview As HDS.DAL.COMMON.TableView
        If Me.RadioButtonList1.SelectedValue = "NAME" Then
            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetCustomerByNameforClient
            MYSPROC.ClientId = SelectClientId
            MYSPROC.Reference = Me.TextBox1.Text & "%"
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
            myview.Sort = "Name"
        Else
            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetCustomerByReferenceforClient
            MYSPROC.ClientId = SelectClientId
            MYSPROC.Reference = Me.TextBox1.Text & "%"
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
            myview.Sort = "Name"
        End If
        If myview.Count = 0 Then
            Exit Sub
        End If

        myview.MoveFirst()
        Do Until myview.EOF
            Dim LI As New System.Web.UI.WebControls.ListItem
            LI.Text = Trim(myview.RowItem("Name")) & ", "
            LI.Text += Trim(myview.RowItem("Address"))
            LI.Text += ",( " & Trim(myview.RowItem("Ref")) & " ) "
            LI.Value = myview.RowItem("ID")
            Me.ListBox1.Items.Add(LI)
            myview.MoveNext()
        Loop

        Me.ListBox1.DataBind()
        Me.Session("CustSearchView") = myview
        Me.ListBox1.Visible = True

    End Sub

    'Private Function LoadCustomersByRef(ByVal sKeyword As String, ByVal aarg As String) As ArrayList
    '    Dim aOut As ArrayList = New ArrayList
    '    Dim ss As String() = Strings.Split(msDataType, ",")
    '    Dim slabel As String
    '    Dim oMenuItem As ASBMenuItem
    '    Dim myview As HDS.DAL.COMMON.TableView
    '    Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetCustomerByReferenceforClient
    '    MYSPROC.ClientId = ss(1)
    '    MYSPROC.Reference = sKeyword & "%"
    '    myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
    '    myview.Sort = "Ref"

    '    myview.MoveFirst()
    '    Do Until myview.EOF
    '        slabel = Trim(myview.RowItem("Ref")) & "," & Trim(myview.RowItem("Name"))
    '        slabel += Trim(myview.RowItem("Address"))
    '        oMenuItem = New ASBMenuItem
    '        oMenuItem.Label = slabel
    '        oMenuItem.Value = myview.RowItem("Id")
    '        aOut.Add(oMenuItem)
    '        myview.MoveNext()
    '    Loop
    '    Return aOut
    'End Function
    'Private Function LoadGCByName(ByVal sKeyword As String, ByVal aarg As String) As ArrayList
    '    Dim aOut As ArrayList = New ArrayList
    '    Dim ss As String() = Strings.Split(msDataType, ",")
    '    Dim slabel As String
    '    Dim oMenuItem As ASBMenuItem
    '    Dim myobj As New CRF.BLL.ClientView.Lien.NewJob(CRFVIEW.COMMON.GetCurrentUser)
    '    Dim myview As HDS.DAL.COMMON.TableView
    '    Dim sfilter As String = CStr(sKeyword & "%")

    '    Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetGeneralContractorList
    '    MYSPROC.ClientId = ss(1)
    '    MYSPROC.Name = sKeyword & "%"
    '    myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
    '    myview.Sort = "Name"
    '    myview.MoveFirst()
    '    Do Until myview.EOF
    '        slabel = Trim(myview.RowItem("Name")) + ","
    '        slabel += Trim(myview.RowItem("Address"))
    '        oMenuItem = New ASBMenuItem
    '        oMenuItem.Label = slabel
    '        oMenuItem.Value = myview.RowItem("Id")
    '        aOut.Add(oMenuItem)
    '        myview.MoveNext()
    '    Loop
    '    Return aOut
    'End Function

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            LoadList()
        End If
        Me.ListBox1.DataBind()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim S As String = Me.ListBox1.SelectedIndex

    End Sub

    Protected Sub btnSearchIt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearchIt.Click
        RaiseEvent SearchSelected(Me.TextBox1.Text)

    End Sub

    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        Me.ClearData()

    End Sub

    Protected Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        Me.ListBox1.DataBind()
        Me.myValue.Value = Me.ListBox1.SelectedValue 'listCustName
        'Me.myValue.Value = Me.listCustName.SelectedValue
        Me.ClearData()
        RaiseEvent ItemSelected()

    End Sub

End Class
