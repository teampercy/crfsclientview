<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobEdit.ascx.vb" Inherits="App_Controls_LienView_JobEdit" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script type="text/javascript">
    function OpenJobNewModal() {
        $("#ModalJobNew").modal('show');
    }
    function ModalHide2() {
       // alert('hello');

        // $("#btnclose").click();
        document.getElementById('<%=CancelButton.ClientID%>').click();
        DisplayTablelayout();
        $("#ModalJobNew").modal('hide');
      
    }
    function hideeditbtn(){
        //alert("hii");
        if ($('#' + '<%= IsClientViewManager.ClientID %>').val() == 'True') {
            //alert("e");
            $('#btnJobNew').prop('disabled', false);
        }
        else {
           // alert("d");
            $('#btnJobNew').prop('disabled', true);
        }
    }
</script>
<style>
     .PaddingleftRemove {
        padding-left: 0px;
    }
</style>
<asp:Button ID="btnAddNew" CssClass="btn btn-primary" runat="server" Text="Job Edit" Style="display: none;" />
<input type="button" id="btnJobNew" onclick="OpenJobNewModal();" value="Job Edit" class="btn btn-primary" />

<div class="modal fade modal-primary" id="ModalJobNew" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content" style="width:800px">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose1" onclick="ModalHide2();">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title" style="text-align:left !important;">Edit Job Info</h4>
                <asp:HiddenField ID="BatchJobId" runat="server" />
                <asp:HiddenField ID="ItemId" runat="server" />
                  <asp:HiddenField ID="IsClientViewManager" runat="server" />
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                              <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Lien Filed:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                 <span style="display: flex;">
                                <SiteControls:Calendar ID="LienDate" runat="server" ErrorMessage="Lien Date" IsReadOnly="false"
                                    IsRequired="false" Value="" /></span>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">First Furnished:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                 <span style="display: flex;">
                                <SiteControls:Calendar ID="StartDate" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                    IsRequired="false" Value="TODAY" /></span>
                            </div>
                        </div>
                        <div class="form-group">
                              <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" >Foreclose Filed:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                 <span style="display: flex;">
                                <SiteControls:Calendar ID="ForeclosureDate" runat="server"  IsReadOnly="false"
                                    IsRequired="false" Value="" /></span>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Last Furnished/End Date:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                 <span style="display: flex;">
                                <SiteControls:Calendar ID="EndDate" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                    IsRequired="false" Value="TODAY" /></span>
                            </div>
                        </div>
                        <div class="form-group">
                              <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Bond Date:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                 <span style="display: flex;">
                                <SiteControls:Calendar ID="BondDate" runat="server"  IsReadOnly="false"
                                    IsRequired="false" Value="" /></span>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Notice of Completion:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                 <span style="display: flex;">
                                <SiteControls:Calendar ID="NOCDate" runat="server" IsReadOnly="false"
                                    IsRequired="false" Value="TODAY" /></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Bond Suit Filed:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                 <span style="display: flex;">
                                <SiteControls:Calendar ID="BondSuitDate" runat="server"  IsReadOnly="false"
                                    IsRequired="false" Value="" /></span>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Estimated Balance:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                <cc1:DataEntryBox ID="EstBalance" runat="server" CssClass="form-control"
                                    Text="" Width="180px" DataType="Money"></cc1:DataEntryBox>
                            </div>
                        </div>
                         <div class="form-group">
                             <div class="col-md-3 col-sm-3 col-xs-3"></div>
                             <div class="col-md-3 col-sm-3 col-xs-3"></div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Filter Key : </label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                <cc1:DataEntryBox ID="FilterKey" runat="server" CssClass="form-control"
                                    Text="" Width="180px" DataType="Any"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3"></div>
                            <div class="col-md-3 col-sm-3 col-xs-3"></div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Send Amended Notice:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                   <div class="checkbox-custom checkbox-default">
                                <asp:CheckBox ID="chkSendAmended" runat="server" Text=" "/></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3"></div>
                            <div class="col-md-3 col-sm-3 col-xs-3"></div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">
                                <asp:Label ID="lblPaidStatus" runat="server" Text="Job Paid In Full:"></asp:Label></label>
                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                   <div class="checkbox-custom checkbox-default">
                                <asp:CheckBox ID="chkJobPaidInFull" runat="server" Text=" " /></div>
                            </div>
                        </div>
                       <%-- <div class="form-group">
                            <label class="col-md-5 control-label align-lable">
                                <asp:Label ID="lblJobAlert" runat="server" Text="Lien Alert:"></asp:Label></label>
                            <div class="col-md-7">
                                   <div class="checkbox-custom checkbox-default">
                                <asp:CheckBox ID="chkJobAlert" runat="server" Text=" " /></div>
                            </div>
                        </div>--%>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-3"></div>
                            <div class="col-md-3 col-sm-3 col-xs-3"></div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable align-lable PaddingleftRemove">
                                <asp:Label ID="lblNOCompBox" runat="server" Text="Notice of Comp Search:"></asp:Label></label>
                            <div class="col-md-3 col-sm-3 col-xs-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                   <div class="checkbox-custom checkbox-default">
                                <asp:CheckBox ID="chkNOCompBox" runat="server" text=" "/></div>
                            </div>
                        </div>
                    </div>
                    <asp:LinkButton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " CausesValidation="False" Style="display: none;" />


                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="ModalHide2();">Close</button>
            </div>
        </div>
    </div>
</div>
<%--<asp:Panel ID="panCustNameSuggest" runat="server" Style="display: none"
    Width="400px" Font-Bold="True">
    <div id="modcontainer" style="MARGIN: 0px  0px 0px 0px; width: 400px;">
        <h1 class="panelheader">Edit Job Info</h1>
        <div class="body">
            <asp:LinkButton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " CausesValidation="False" />
            <br />
            <br />

            <table class="body">
                <tr>
                    <td class="row-label" style="width: 168px">First Furnished:</td>
                    <td class="row-data">
                        <SiteControls:Calendar ID="StartDate" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                            IsRequired="true" Value="TODAY" />
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 168px; height: 22px">Last Furnished:</td>
                    <td class="row-data">
                        <SiteControls:Calendar ID="EndDate" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                            IsRequired="true" Value="TODAY" />
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 168px; height: 22px">Job Completion:</td>
                    <td class="row-data">
                        <SiteControls:Calendar ID="NOCDate" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                            IsRequired="true" Value="TODAY" />
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 168px; height: 22px">Estimated Balance:</td>
                    <td class="row-data">
                        <cc1:DataEntryBox ID="EstBalance" runat="server"
                            Text="" Width="100px" DataType="Money"></cc1:DataEntryBox>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 168px; height: 22px">Send Amended Notice:</td>
                    <td class="row-data">
                        <asp:CheckBox ID="chkSendAmended" runat="server" /></td>
                </tr>

                <tr>
                    <td class="row-label" style="width: 168px; height: 22px">
                        <asp:Label ID="lblPaidStatus" runat="server" Text="Job Paid In Full:"></asp:Label></td>
                    <td class="row-data" style="height: 22px">
                        <asp:CheckBox ID="chkJobPaidInFull" runat="server" /></td>
                </tr>

                <tr>
                    <td class="row-label" style="width: 168px; height: 22px">
                        <asp:Label ID="lblJobAlert" runat="server" Text="Lien Alert:"></asp:Label></td>
                    <td class="row-data" style="height: 22px">
                        <asp:CheckBox ID="chkJobAlert" runat="server" /></td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 168px; height: 22px">
                        <asp:Label ID="lblNOCompBox" runat="server" Text="Notice of Comp Search:"></asp:Label></td>
                    <td class="row-data" style="height: 22px">
                        <asp:CheckBox ID="chkNOCompBox" runat="server" /></td>
                </tr>
            </table>
        </div>
        <div class="footer">
            <asp:Button ID="btnSave" CssClass="button" runat="server" Text="Save" />&nbsp;&nbsp;
            <br />
            <asp:HiddenField ID="BatchJobId" runat="server" />
            <asp:HiddenField ID="ItemId" runat="server" />
        </div>
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="" OkControlID="" DropShadow="true"
    PopupControlID="panCustNameSuggest" TargetControlID="btnAddNew">
</ajaxToolkit:ModalPopupExtender>--%>
