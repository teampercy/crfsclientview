﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FileUploadTest.ascx.vb" Inherits="App_Controls__Custom_LienView_FileUploadTest" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<meta http-equiv='cache-control' content='no-cache, no-store, must-revalidate' /> 
<meta http-equiv='expires' content='0'> 
<meta http-equiv='pragma' content='no-cache'> 
<style type="text/css">

</style>
<script type="text/javascript">
    function setSelectedDocTypeId(id) {
        $('#' + '<%=hdnDocType.ClientID%>').val(id);
        $('#btnDropDocType').html(id + "<span class='caret' ></span>");
        return false;
    }
    function EnableFileUploadButton()
    {
        debugger;
        document.getElementById('<%=btnFileUpLoadOk.ClientID%>').disabled = false;
    }
    function showLodingImage() {
                
        $('#loadingImageDiv').show();
    } // 5 seconds }

    function hideLodingImage() {
        // alert('hideLodingImage');
      
        // debugger;             
        var tmr = setInterval(function () { zzzhideLodingImageActul(tmr) }, 1000);
    }
    function zzzhideLodingImageActul(tmr) { $('#loadingImageDiv').hide(); clearInterval(tmr); }   
    function BindFileUoload() {
        parent.BindFileUoloadList();
    }
    function SetDatePicker() {
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            showButtonPanel: true,
        });
    }

    function DeleteSessionNew() {

        alert('hi');
        $.ajax({
            type: "POST",
            url: "Default.aspx/DeleteSession",
            contentType: "application/json; charset=utf-8",
            async: false,
            dataType: "json",
            success: function (response) {
                $find('FileUpLoad').hide(); return false;
                //window.open("../Pages/NewCharge.aspx", "_self");
            },
            failure: function (response) {
                //alert(response.d);
                alert("fail");
            }
        });

    }

    function showLoadingImage() {
        $("#loadingImageDiv").show();
    }

    function uploadFileValidation() {
        debugger;
       // alert('call function');
        var fileUpload = document.getElementById('<%= FileUpLoad1.ClientID %>');
        var fileName = fileUpload.value;
        //alert(fileName);[a-zA-Z0-9\s_\\.\-:\(\)\“(“ “)”]
        if (fileName.search(/[<>'\+\"\/;`%,#$&@@]/) > 0) {
            alert('Please upload the file without special characters');
            //$('#btnFileUpLoadOk').prop('disabled', true);
            document.getElementById('<%=btnFileUpLoadOk.ClientID%>').disabled = true;
           // $(':input[type="submit"]').prop('disabled', true);
            return false;
        }
        else {
            //alert('The file "' + fileName + '" has been selected.');
           // $(':input[type="submit"]').prop('disabled', false);
            document.getElementById('<%=btnFileUpLoadOk.ClientID%>').disabled = false;
           // $('#btnFileUpLoadOk').prop('disabled', false);
            //$("#uploadedFileName").empty();
           // $("#uploadedFileName").append(fileName);
        }
    }
  <%--  $(document).ready(
       
    function(){

        debugger;
               $("{#UploadFile7_FileUpLoad1").change(function(){
                   if ($("#UploadFile7_RegularExpressionValidator1") == "Please select a valid  File")
                   {
                       $("#btnFileUpLoadOk").attr('disabled', true);
                  // $('#<%= btnFileUpLoadOk.ClientID %>').attr('disabled',true);
               }
               });
             });
    --%>

</script>
<style type="text/css">
    .scrollable-menu {
        height: auto;
        max-height: 180px;
        overflow-x: hidden;
    }

        .scrollable-menu::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 4px;
        }

        .scrollable-menu::-webkit-scrollbar-thumb {
            border-radius: 3px;
            background-color: lightgray;
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);
        }

    .dropify-wrapper {
        height: 160px;
    }
        .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
    }
</style>
<%--<form id="frm" runat="server">--%>
<asp:Panel ID="FileUpLoad" class="myFileUpLoad" runat ="server" CssClass="modcontainer" Width="100%">
<div style="float: left;">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Document Date:</label>
            <div class="col-xs-4" style="display: flex;">
                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Document Type:</label>
            <div class="col-xs-7" style="text-align: left;">
                <div class="btn-group" style="text-align: left;" align="left">
                    <button type="button" class="btn btn-default dropdown-toggle" style="min-width: 200px;" align="left"
                        data-toggle="dropdown" aria-expanded="false" id="btnDropDocType">
                        --Select--
                        <span class="caret" style="float: right; margin-top: 5px;"></span>
                    </button>
                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState" style="overflow-y: auto; height: 250px;">
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('AZ Owner Ack')">AZ Owner Ack</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Bond Copy')">Bond Copy</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Building Permit')">Building Permit</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Job Info Sheet')">Job Info Sheet</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Job Info Fax')">Job Info Fax</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Miscellaneous')">Miscellaneous</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Notice of Commencement')">Notice of Commencement</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Notice of Completion')">Notice of Completion</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Surety Letter')">Surety Letter</a></li>
                      <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Waivers')">Waivers</a></li>
                    </ul>
                </div>
                <asp:HiddenField ID="hdnDocType" runat="server" Value="--Select--" />
                <asp:DropDownList ID="ddldoctype" runat="server" Style="display: none;">
                    <asp:ListItem Text="--SELECT--" Selected="true"></asp:ListItem>
                    <asp:ListItem Text="AZ Owner Ack"></asp:ListItem>
                    <asp:ListItem Text="Bond Copy"></asp:ListItem>
                    <asp:ListItem Text="Building Permit"></asp:ListItem>
                    <asp:ListItem Text="Job Info Sheet"></asp:ListItem>
                    <asp:ListItem Text="Job Info Fax"></asp:ListItem>
                    <asp:ListItem Text="Miscellaneous"></asp:ListItem>
                    <asp:ListItem Text="Notice of Commencement"></asp:ListItem>
                    <asp:ListItem Text="Notice of Completion"></asp:ListItem>
                    <asp:ListItem Text="Surety Letter"></asp:ListItem>
                     <asp:ListItem Text="Waivers"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Misc Info:</label>
            <div class="col-xs-8">
                <asp:TextBox ID="Txtmiscinfo" runat="server" TextMode="MultiLine" Font-Names="Arial" Font-Size="small" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
         
        <div class="form-group">
          
            <div class="col-xs-4 control-label text-right">
                File To Upload :               
            </div>
          
             <div class="col-xs-8">
                 <%--<asp:FileUpload ID="FileUpLoad1" AlternateText="You cannot upload files" runat="server"
                        size="40" Height="25px" Visible="false" />--%>
                <asp:FileUpload id="FileUpLoad1" runat="server" AllowMultiple="true" onChange="uploadFileValidation()"/>
                  
                <asp:RequiredFieldValidator ID="rfvFileupload" ValidationGroup="validate" runat="server"

                                                ControlToValidate="FileUpload1"></asp:RequiredFieldValidator>




               
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:\(\)\“(“ “)”])+(.jpg|.jpeg|.gif|.png|.bmp|.doc|.docx|.txt|.csv|.xls|.zip|.pdf|.xlsx|.PDF|.JPG|.JPEG|.GIF|.PNG|.BMP|.DOC|.DOCX|.TXT|.CSV|.XLS|.ZIP|.XLSX)$"
                 ControlToValidate="FileUpload1" runat="server" ForeColor="Red" ErrorMessage="Please select a valid  File"
                  Display="Dynamic" />
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" style="margin-left: -86px;" ValidationExpression ="^([1-9][0-9]{0,5}|[12][0-9]{6}|4(0[0-9]{5}|1([0-4][0-9]{4}|4([0-4][0-9]{4}|5([0-6][0-9]{2}|7([01][0-9]|2[0-8]))))))$" ErrorMessage="File is too large, must select file under 4 Mb." ControlToValidate="FileUpload1" runat="server"></asp:RegularExpressionValidator>
                   --%> 
                <div id="fileList"></div>
          
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-8" style="text-align: left; text-wrap: normal; font-weight: normal">
                <%--<asp:Label ID="Label3" runat="server"
                    Text="Attached file cannot be larger than 4mbs. If your attachment is larger,please break it up into multiple files and attach them separately."
                    ForeColor="Red" />--%>
                <asp:Label ID="Label1" runat="server"
                    Text="Attached file cannot be larger than 4mbps.If your attachment is larger,please break it up into multiple files and attach them separately."
                    ForeColor="Red" />
                <br />
                <label class="msg row-label" style="text-align: left; padding: 0px">Select File to Upload: (PDF,DOC,TXT,CSV,XLS,JPG,GIF,BMP,ZIP Only)</label>


            </div>
        </div>
       <%-- <div class="form-group">
            <div class="col-xs-12">
                <div class="file-wrap container-fluid">
                    <div class="file-list row filelistempty" ></div>
                    <br /><%-- <b>Files uploaded above</b>--%>

           <%--     </div>
            </div>
        </div>--%>
       <%--   <div class="form-group">
            <div class="col-xs-12">
                <div class="file-wrap container-fluid">
                    <div class="file-list row"></div>
                </div>
            </div>
        </div>--%>
        <div class="form-group">
            <div class="col-xs-4">
            </div>

   
       <%--  <div class="col-xs-8">
                <asp:Button ID="btnFileUpLoadOk" ClientIDMode="Static" runat="server" Style="float: none" Enabled="true" CausesValidation="true"
                    OnClientClick="showLoadingImage()" Text="Save Document" CssClass="btn btn-primary" />

            </div>--%>

            
         <div class="col-xs-8">
                <asp:Button ID="btnFileUpLoadOk" ClientIDMode="Static" runat="server" Style="float: none" Enabled="true" CausesValidation="true"
                    OnClientClick="showLoadingImage()" Text="Save Document" CssClass="btn btn-primary" />

            </div>

        </div>
           <div class="form-group">
           <%-- <div class="col-xs-4">OnClick="btnFileUpLoadOk_Click" 
            </div>--%>
            <div class="col-xs-8">
                <div>
                    <%--<asp:LinkButton ID="LinkButton1" runat="server" CssClass="close" OnClientClick="$find('FileUpLoad').hide(); return false;" />--%>
                      <asp:LinkButton ID = "LinkButton1" runat="server" CssClass="close" OnClientClick="$find('FileUpLoad1').hide();return false;" /> 
                    <%--<asp:LinkButton ID="LinkButton1" runat="server" CssClass="close" OnClientClick="document.getElementById('frmUpload').contentWindow.DeletFileUpload('1');return false;" />--%>
                   
                </div>
                <div style="padding-left: 10px; text-align: left">
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDate"
                        ErrorMessage="Enter Date in MM/DD/YYYY Format" Operator="DataTypeCheck" Type="Date" />

                    <br />
                    <asp:Label ID="lblmsg" runat="server" ForeColor="red"></asp:Label>
                </div>

            </div>
        </div>
    </div>
</div>
<%--    <div style="text-align: left">
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUpLoad1"
            Text="Select File To Upload"></asp:RequiredFieldValidator>
    </div>--%>



 <script src="https://code.jquery.com/jquery-1.12.0.min.js">

    <%-- function clear(){
     var file = document.querySelector('input');
var emptyFile = document.createElement('input');
emptyFile.type = 'file';

document.querySelector('button').onclick = function(){
  file.files = emptyFile.files;
}--%>

 </script>
<%--    <script>

        $(function () {
            //$('#UploadedFile').change(function () {
            //    alert('UploadedFile manulay');

            //});

            $("#btnFileUpLoadOk").click(function (e) {
                debugger;
                var Flag = true;
                var ValidationMessage = "";
                if ($("#<%=hdnDocType.ClientID%>").val() == '--Select--') {
                    Flag = false;
                    ValidationMessage = " Select Document Type<br>";
                }
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/GetFileUploadExtension",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    dataType: "json",
                    success: function (response) {
                        if (response.d == "") {
                            Flag = false;
                            ValidationMessage = ValidationMessage + " Select File to upload";
                        }
                        else {
                            var AllowedExt = new Array("jpg", "jpeg", "gif", "png", "bmp", "doc", "docx", "txt", "csv", "xls", "zip", "pdf", "xlsx");
                            var ext = response.d;
                            ext = ext.substring(1);
                            if (AllowedExt.indexOf(ext.toLowerCase()) < 0) {
                                Flag = false;
                                ValidationMessage = ValidationMessage + " Select Valid File<br>";
                            }
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                        Flag = false;
                    }
                });
                $("#<%= lblmsg.ClientID%>").html(ValidationMessage);
                debugger;
                if (Flag) {
                    showLodingImage();
                }
                else {
                    hideLodingImage();
                }
                return Flag;
            });
        })

    </script>--%>
    </asp:Panel>