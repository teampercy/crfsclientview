
Partial Class App_Controls_LienView_JobEdit
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobClientViewUpdates
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim dsjobinfo As System.Data.DataSet
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim mylieninfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientLienInfo

    Public Sub ClearData(ByVal JobId As String)
        Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(Me.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
        myuser = Session("UserInfo")
        If myuser.UserInfo.IsClientViewManager = 0 Then
            IsClientViewManager.Value = False
        Else
            IsClientViewManager.Value = True
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hide", "hideeditbtn();", True)

        Me.ViewState("JobId") = JobId
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobId, myjob)

        With Me
            .StartDate.Value = myjob.StartDate
            .EndDate.Value = myjob.EndDate
            .EstBalance.Value = myjob.EstBalance
            .FilterKey.Value = myjob.FilterKey
            .NOCDate.Value = myjob.NOCDate
            '.LienDate.Value = myjob.LienDate
            .LienDate.Value = myjob.FileDate
            .ForeclosureDate.Value = myjob.ForeclosureDate
            .BondDate.Value = myjob.BondDate
            .BondSuitDate.Value = myjob.BondSuitDate

            .chkSendAmended.Checked = False
            .chkSendAmended.Checked = False
            .chkJobPaidInFull.Checked = False
            '.chkJobAlert.Checked = myjob.JobAlertBox
            '.chkNOCompBox.Checked = myjob.NOCompBox
        End With
        mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(myjob.JobState)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myjob.ClientId, mylieninfo)
        If mystate.NoticeOfCompBox = True And mylieninfo.IsNOCApproved = True Then
            Me.lblNOCompBox.Visible = True
            Me.chkNOCompBox.Visible = True
        Else
            Me.lblNOCompBox.Visible = False
            Me.chkNOCompBox.Visible = False
        End If

        If myjob.StatusCode = "CJP" Then
            Me.lblPaidStatus.Text = "Mark Paid Job Active"
        Else
            Me.lblPaidStatus.Text = "Job Paid In Full"
        End If

        'If myjob.JobAlertBox = False Then
        '    Me.lblJobAlert.Text = "Add to Lien Alert"
        'Else
        '    Me.lblJobAlert.Text = "Remove from Lien Alert"
        'End If

    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim s As String = ""

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.ViewState("JobId"), myjob)

        With myreq
            .JobId = myjob.Id
            .DateCreated = Now
            .UserId = Me.CurrentUser.Id
            .UserCode = (Left(Me.UserInfo.UserInfo.UserName, 10))
            .EndDate = Utils.GetDate(Me.EndDate.Value)
            .StartDate = Utils.GetDate(Me.StartDate.Value)
            .JobPaidInFull = Me.chkJobPaidInFull.Checked
            .SendAmendedNotice = Me.chkSendAmended.Checked
            .EstBalance = Utils.GetDecimal(Me.EstBalance.Value)
            .FilterKey = Me.FilterKey.Value
            .NOCDate = Utils.GetDate(Me.NOCDate.Value)
            '.JobAlert = Me.chkJobAlert.Checked
            .NOCompBox = Me.chkNOCompBox.Checked
            '.LienDate = Utils.GetDate(LienDate.Value)
            .FileDate = Utils.GetDate(LienDate.Value)
            .ForeclosureDate = Utils.GetDate(ForeclosureDate.Value)
            .BondDate = Utils.GetDate(BondDate.Value)
            .BondSuitDate = Utils.GetDate(BondSuitDate.Value)

        End With

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myreq)

        Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        With mynote
            .JobId = myjob.Id
            .DateCreated = Now()
            .Note = ""

            If Me.chkSendAmended.Checked = True Then
                .Note += "Amended Notice Requested"
                .Note += vbCrLf
            End If
            If myreq.EndDate <> myjob.EndDate Then
                .Note += "End Date Changed From:" & myjob.EndDate & " to " & myreq.EndDate
                .Note += vbCrLf
            End If
            If myreq.FilterKey <> myjob.FilterKey Then
                .Note += "Filter Key Changed From:" & myjob.FilterKey & " to " & myreq.FilterKey
                .Note += vbCrLf
            End If
            If myreq.StartDate <> myjob.StartDate Then
                .Note += "Start Date Changed From:" & myjob.StartDate & " to " & myreq.StartDate
                .Note += vbCrLf
            End If
            'If myreq.EstBalance <> myjob.EstBalance Then
            '    .Note += "Estimated Balance Changed From:" & myjob.EstBalance & " to " & myreq.EstBalance
            '    .Note += vbCrLf
            'End If
            If myreq.NOCDate <> myjob.NOCDate Then
                .Note += "NOC Date Changed From:" & myjob.NOCDate & " to " & myreq.NOCDate
                .Note += vbCrLf
            End If

            'If myreq.LienDate <> myjob.LienDate Then
            '    .Note += "Lien Date Changed From:" & myjob.LienDate & " to " & myreq.LienDate
            '    .Note += vbCrLf
            'End If
            If myreq.FileDate <> myjob.FileDate Then
                .Note += "Lien Date Changed From:" & myjob.FileDate & " to " & myreq.FileDate
                .Note += vbCrLf
            End If
            If myreq.ForeclosureDate <> myjob.ForeclosureDate Then
                .Note += "Foreclosure Date Changed From:" & myjob.ForeclosureDate & " to " & myreq.ForeclosureDate
                .Note += vbCrLf
            End If
            If myreq.BondDate <> myjob.BondDate Then
                .Note += "Bond Date Changed From:" & myjob.BondDate & " to " & myreq.BondDate
                .Note += vbCrLf
            End If
            If myreq.BondSuitDate <> myjob.BondSuitDate Then
                .Note += "BondSuit Date Changed From:" & myjob.BondSuitDate & " to " & myreq.BondSuitDate
                .Note += vbCrLf
            End If
            'If myreq.JobAlert <> myjob.JobAlertBox Then
            '    If Me.chkJobAlert.Checked = True Then
            '        .Note += "Added to Lien Alert"
            '        .Note += vbCrLf
            '    ElseIf Me.chkJobAlert.Checked = False Then
            '        .Note += "Removed from Lien Alert"
            '        .Note += vbCrLf
            '    End If
            'End If
            If Me.chkJobPaidInFull.Checked = True Then
                If myjob.StatusCode = "CJP" Then
                    .Note += "Job Changed to Active from Paid In Full"
                    .Note += vbCrLf
                Else
                    .Note += "Job Changed to Paid In Full"
                    .Note += vbCrLf
                End If
            End If
            'If Me.chkSendAmended.Checked = True Then
            '    .Note += "Amended Notice Requested"
            '    .Note += vbCrLf
            'End If
            'If Me.chkNOCompBox.Checked = True Then
            '    .Note += "Notice of Completion Requested"
            '    .Note += vbCrLf
            'End If
            If myreq.EstBalance <> myjob.EstBalance Then
                .Note += "Estimated Balance Changed From:" & myjob.EstBalance & " to " & myreq.EstBalance
                .Note += vbCrLf
            End If

            .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .ClientView = True
            .EnteredByUserId = Me.UserInfo.UserInfo.ID
            .ActionTypeId = 0

            If Strings.Len(.Note.ToString) > 1 Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
            End If
        End With

        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_UpdateJob
        MYSPROC.RequestId = myreq.PKID
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.ExecNonQuery(MYSPROC)

        With myjob
            .EndDate = myreq.EndDate
            .StartDate = myreq.StartDate
            .EstBalance = myreq.EstBalance
            .LienAmt = myreq.EstBalance
            .FilterKey = myreq.FilterKey
            .NOCDate = myreq.NOCDate
            .JobAlertBox = myreq.JobAlert
            .NOCompBox = myreq.NOCompBox
            '.LienDate = myreq.LienDate
            .FileDate = myreq.FileDate
            .ForeclosureDate = myreq.ForeclosureDate
            .BondDate = myreq.BondDate
            .BondSuitDate = myreq.BondSuitDate

            If Me.chkJobPaidInFull.Checked = True Then
                If myjob.StatusCodeId <> "60" Then
                    .StatusCodeId = 60    'CJP
                Else
                    '.StatusCodeId = 33     'CNS
                    .StatusCodeId = 65       'CUP Changed on 08/19/19
                End If
            End If

        End With
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)

        Dim MYSPROC2 As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_LiensDayEnd_UpdateDeadlineDates
        MYSPROC2.ExpireDays = 1200
        MYSPROC2.JobId = myjob.Id
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.ExecNonQuery(MYSPROC2)

        ClearData(Me.ViewState("JobId"))

        RaiseEvent ItemSaved()

    End Sub

    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Me.ViewState("JobId"))
        ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsModalHide", "ModalHide();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActive", "ActivateTab('MainInfo');", True)

    End Sub

End Class
