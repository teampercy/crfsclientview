<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="App_Controls__Custom_LienView_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Src="FileUpload.ascx" TagName="uploadFile" TagPrefix="uc7" %>
<%@ Register Src="~/App_Controls/_Custom/CollectView/debtaccountfileupload.ascx" TagName ="debtaccountfileupload" TagPrefix="uc8"   %>
 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
<meta name="generator" content="hds vbjuice" />
    <meta id="metaKeywords" name="Keywords" content="" />
    <meta id="metaDescr" name="Description" content="" />
    <meta id="metaCopyRight" name="copyright" content="" />
    <link id="Link1" rel="stylesheet" href="../../../App_Themes/VBJUICE/StyleSheet.css" type="text/css" />
    <link rel="shortcut icon" href="/../../images/favicon.ico" />
     
     <link rel="apple-touch-icon" href="../../../assets/images/apple-touch-icon.png" />
    <link rel="shortcut icon" href="../../../assets/images/favicon.ico" />
    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../../global/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/css/bootstrap-extend.min.css" type="text/css" />
    <link rel="stylesheet" href="../../../assets/css/site.min.css" type="text/css" />
    <!-- Plugins -->
    <link rel="stylesheet" href="../../../global/vendor/animsition/animsition.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/asscrollable/asScrollable.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/switchery/switchery.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/intro-js/introjs.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/slidepanel/slidePanel.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/flag-icon-css/flag-icon.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/chartist-js/chartist.css" type="text/css" />
    <%-- <link rel="stylesheet" href="global/vendor/jvectormap/jquery-jvectormap.css" type="text/css">--%>
    <link rel="stylesheet" href="../../../global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css" type="text/css" />
    <link rel="stylesheet" href="../../../assets/examples/css/dashboard/v1.css" type="text/css" />
     <link rel="stylesheet" href="../../../global/vendor/jquery-mmenu/jquery-mmenu.css" type="text/css">
    <!-- Fonts -->
    <link rel="stylesheet" href="../../../global/fonts/weather-icons/weather-icons.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/fonts/web-icons/web-icons.min.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/fonts/brand-icons/brand-icons.min.css" type="text/css" />
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic' />
     <link rel="stylesheet" href="../../../global/vendor/datatables-bootstrap/dataTables.bootstrap.css" type="text/css"/>
      <script src="../../../global/vendor/modernizr/modernizr.js" type="text/javascript"></script>
    <script src="../../../global/vendor/breakpoints/breakpoints.js" type="text/javascript"></script>
    <link rel="alternate" type="application/rss+xml" title="gohds.com RSS Feed Description" href="HTTP://www.gohds.net/feeds.axd?BLOG" />
    <title>Untitled Page</title>
   <%-- <script type="text/javascript" src="../../../jquery-1.8.3.min.js"></script>--%>


    <script language="javascript" type="text/javascript">    
       

        $(document).ready(function () {
          
            $('body').on('keydown', 'input, select, textarea', function (e) {
               // alert('hisds');
                var self = $(this)
                  , form = self.parents('form:eq(0)')
                  , focusable
                  , next
                ;
                if (e.keyCode == 13) {
                    //alert('sdsdsd');
                    focusable = form.find('input,a,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                        next.focus();
                    } else {
                        form.submit();
                    }
                    return false;
                }
            });
        });
</script>
</head>
<body style="padding-top:10px;">
    <form id="myform" runat="server"  autocomplete="on" style="width:97%;height:100%;" >
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <div  >
    <uc7:uploadFile ID="UploadFile7" runat="server" />
    </div>
    <div>
    <uc8:debtaccountfileupload ID="uploadfile8" runat="server" />  
    </div>
    
    </form> 
     <!-- Core  -->
    <script src="../../../global/vendor/jquery/jquery.js" type="text/javascript"></script>
    <script src="../../../global/vendor/bootstrap/bootstrap.js" type="text/javascript"></script>
    <script src="../../../global/vendor/animsition/animsition.js" type="text/javascript"></script>
    <script src="../../../global/vendor/asscroll/jquery-asScroll.js" type="text/javascript"></script>
    <script src="../../../global/vendor/mousewheel/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="../../../global/vendor/asscrollable/jquery.asScrollable.all.js" type="text/javascript"></script>
    <script src="../../../global/vendor/ashoverscroll/jquery-asHoverScroll.js" type="text/javascript"></script>
    <!-- Plugins -->
    <script src="../../../global/vendor/switchery/switchery.min.js" type="text/javascript"></script>
    <script src="../../../global/vendor/intro-js/intro.js" type="text/javascript"></script>
    <script src="../../../global/vendor/screenfull/screenfull.js" type="text/javascript"></script>
    <script src="../../../global/vendor/slidepanel/jquery-slidePanel.js" type="text/javascript"></script>
    <script src="../../../global/vendor/skycons/skycons.js" type="text/javascript"></script>
    <script src="../../../global/vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
    <script src="../../../global/vendor/datatables-bootstrap/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="../../../global/vendor/asrange/jquery-asRange.min.js" type="text/javascript"></script>
    <script src="../../../global/vendor/formatter-js/jquery.formatter.js" type="text/javascript"></script>
    <%--  <script src="global/vendor/chartist-js/chartist.min.js"></script>
    <script src="global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.js"></script>--%>
    <script src="../../../global/vendor/aspieprogress/jquery-asPieProgress.min.js" type="text/javascript"></script>
    <%--  <script src="global/vendor/jvectormap/jquery.jvectormap.min.js"></script>
    <script src="global/vendor/jvectormap/maps/jquery-jvectormap-au-mill-en.js"></script>--%>
    <script src="../../../global/vendor/matchheight/jquery.matchHeight-min.js" type="text/javascript"></script>
    <!-- Scripts -->
    <script src="../../../global/js/core.js" type="text/javascript"></script>
    <script src="../../../assets/js/site.js" type="text/javascript"></script>
    <script src="../../../assets/js/sections/menu.js" type="text/javascript"></script>
    <script src="../../../assets/js/sections/menubar.js" type="text/javascript"></script>
    <script src="../../../assets/js/sections/gridmenu.js" type="text/javascript"></script>
    <script src="../../../assets/js/sections/sidebar.js" type="text/javascript"></script>
    <script src="../../../global/js/configs/config-colors.js" type="text/javascript"></script>
    <script src="../../../assets/js/configs/config-tour.js" type="text/javascript"></script>
    <script src="../../../global/js/components/asscrollable.js" type="text/javascript"></script>
    <script src="../../../global/js/components/animsition.js" type="text/javascript"></script>
    <script src="../../../global/js/components/slidepanel.js" type="text/javascript"></script>
    <script src="../../../global/js/components/switchery.js" type="text/javascript"></script>
    <script src="../../../global/js/components/matchheight.js" type="text/javascript"></script>
    <%--    <script src="global/js/components/jvectormap.js"></script>--%>
    <script src="../../../assets/examples/js/dashboard/v1.js" type="text/javascript"></script>
    <script src="../../../global/js/components/formatter-js.js" type="text/javascript"></script>
    <script src="../../../global/js/components/datatables.js" type="text/javascript"></script>
    <script src="../../../global/js/plugins/responsive-tabs.js" type="text/javascript"></script>
    <script src="../../../global/js/plugins/closeable-tabs.js" type="text/javascript"></script>
    <script src="../../../global/js/components/tabs.js" type="text/javascript"></script>
    <script src="../../../assets/examples/js/uikit/icon.js" type="text/javascript"></script>
    <script src="../../../global/js/components/panel.js" type="text/javascript"></script>
    <script src="../../../assets/examples/js/uikit/panel-actions.js" type="text/javascript"></script>
</body>
</html>
