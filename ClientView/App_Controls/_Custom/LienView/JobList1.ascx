<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
  <link rel="apple-touch-icon" href="../../../assets/images/apple-touch-icon.png"/>
  <link rel="shortcut icon" href="../../../assets/images/favicon.ico"/>
  <!-- Stylesheets -->
  <link rel="stylesheet" href="../../../global/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="../../../global/css/bootstrap-extend.min.css"/>
  <link rel="stylesheet" href="../../../assets/css/site.min.css"/>
  <!-- Plugins -->
  <link rel="stylesheet" href="../../../global/vendor/animsition/animsition.css"/>
  <link rel="stylesheet" href="../../../global/vendor/asscrollable/asScrollable.css"/>
  <link rel="stylesheet" href="../../../global/vendor/switchery/switchery.css"/>
  <link rel="stylesheet" href="../../../global/vendor/intro-js/introjs.css"/>
  <link rel="stylesheet" href="../../../global/vendor/slidepanel/slidePanel.css"/>
  <link rel="stylesheet" href="../../../global/vendor/flag-icon-css/flag-icon.css"/>
  <link rel="stylesheet" href="../../../global/vendor/datatables-bootstrap/dataTables.bootstrap.css"/>
  <link rel="stylesheet" href="../../../global/vendor/datatables-fixedheader/dataTables.fixedHeader.css"/>
  <link rel="stylesheet" href="../../../global/vendor/datatables-responsive/dataTables.responsive.css"/>
  <link rel="stylesheet" href="../../../assets/examples/css/tables/datatable.css"/>
  <link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css"/>
  <!-- Fonts 
  <link rel="stylesheet" href="global/fonts/font-awesome/font-awesome.css"/>
  <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css"/>
  <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'/>
  <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
  <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
  <!-- Scripts -->
  <script src="../../../global/vendor/modernizr/modernizr.js"></script>
  <script src="../../../global/vendor/breakpoints/breakpoints.js"></script>
  <script>
      Breakpoints();
  </script>
<script runat="server">
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
    Protected WithEvents btnEditItem As LinkButton
    
    Dim vwclients As HDS.DAL.COMMON.TableView
    Dim myview As HDS.DAL.COMMON.TableView
      
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
             
        If Me.Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
              LoadClientList
      
            If qs.HasParameter("page") Then
                GetData(GetFilter, 1, "")
            End If
        End If
        
    End Sub
    
    Private Sub LoadClientList()
       
        vwclients = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop
           
            Me.panClients.Visible = True
            mysproc = GetFilter()
            If mysproc.ClientCode.Length > 4 Then
                Me.DropDownList1.SelectedValue = mysproc.ClientCode
            End If
            
        End If
        
    End Sub
    
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) HANDLES btnSubmit.Click
        With mysproc
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                End If
            End If

            .FromReferalDate = Me.Calendar1.Value
            .ThruReferalDate = Me.Calendar2.Value
            If Me.chkByAssignDate.Checked Then
                .ReferalRange = 1
            End If

            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike.SelectedValue.Trim = "Includes" Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                Else
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If
            End If
            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike.SelectedValue.Trim = "Includes" Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                Else
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If
            End If

            If Me.OwnerName.Text.Trim.Length > 0 Then
                If Me.OwnerNameLike.SelectedValue.Trim = "Includes" Then
                    .PropOwner = "%" & Me.OwnerName.Text.Trim & "%"
                Else
                    .PropOwner = Me.OwnerName.Text.Trim & "%"
                End If
            End If
            If Me.GCName.Text.Trim.Length > 0 Then
                If Me.GCNameLike.SelectedValue.Trim = "Includes" Then
                    .GeneralContractor = "%" & Me.GCName.Text.Trim & "%"
                Else
                    .GeneralContractor = Me.GCName.Text.Trim & "%"
                End If
            End If

            If Me.LenderName.Text.Trim.Length > 0 Then
                If Me.LenderNameLike.SelectedValue.Trim = "Includes" Then
                    .LenderName = "%" & Me.LenderName.Text.Trim & "%"
                Else
                    .LenderName = Me.LenderName.Text.Trim & "%"
                End If
            End If

            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike.SelectedValue.Trim = "Includes" Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                Else
                    .JobName = Me.JobName.Text.Trim & "%"
                End If
            End If

            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike.SelectedValue.Trim = "Includes" Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                Else
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If
            End If

            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNameLike.SelectedValue.Trim = "Includes" Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                Else
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If
            End If

            If Me.JobCity.Text.Trim.Length > 0 Then
                .JobCity = Me.JobCity.Text.Trim & "%"
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            End If
           
            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            End If
           
            If Me.PONum.Text.Trim.Length > 0 Then
                .PONum = Me.PONum.Text.Trim
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim
            End If

            If Me.JobStatus.Text.Trim.Length > 0 Then
                .StatusCode = Me.JobStatus.Text.Trim & "%"
            End If

            If Me.FileNumber.Text.Trim.Length > 0 Then
                .JobId = Me.FileNumber.Text.Trim
            End If

            If Me.TempId.Text.Trim.Length > 0 Then
                .TempId = Me.TempId.Text.Trim
            End If

            If Me.chkPaidInFull.Checked = True Then
                .ExcludePIF = 1
            End If
            
            .UserId = Me.CurrentUser.id

        End With
   
        GetData(mysproc, 1, "F")
      Me.MultiView1.SetActiveView(Me.View2)
      
    End Sub
    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) HANDLES btnViewAll.Click
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
        If Me.panClients.Visible = True Then
            If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                mysproc.ClientCode = Me.DropDownList1.SelectedValue
            End If
        End If

        mysproc.Page = 1
        GetData(mysproc, 1, "F")
        Me.MultiView1.SetActiveView(Me.View2)
       
    End Sub
    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
    Protected Function GetViewURL1(ByVal AITEMID As String) As String
        Dim QS As New HDS.WEBLIB.Common.QueryString(Me.Page)
        mysproc = GetFilter()
        QS.RemoveParameter("PrintId")
        Dim s As String = "212" & "I"
        QS.SetParameter("plid", s)
        QS.SetParameter("page", mysproc.Page)
        QS.SetParameter("ItemId", AITEMID)
        Return QS.All
    End Function
  
    Protected Sub btnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "F")
    
    End Sub
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "P")
       
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "N")
       
    End Sub
    Protected Sub btnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "L")
   
    End Sub
    Protected Sub ddPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddPage.SelectedIndexChanged
        GetData(GetFilter, ddPage.SelectedIndex, "I")

    End Sub
    Protected Function GetFilter() As Object
        If IsNothing(Session("JobListP")) = False Then
            Return Session("JobListP")
        Else
            Return New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
        End If
    End Function
    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1, ByVal apage As Integer, ByVal adirection As String)
        myview = TryCast(Me.Session("JobList"), HDS.DAL.COMMON.TableView)
        
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        asproc.PageRecords = 20000 '25 Changed by jaywanti
        asproc.UserId = Me.CurrentUser.Id
        If adirection = "F" Then
            asproc.Page = 1
        End If
        If adirection = "N" Then
            asproc.Page += 1
        End If
        If adirection = "P" Then
            asproc.Page -= 1
        End If
        If adirection = "I" Then
            asproc.Page = ddPage.SelectedIndex + 1
        End If
        
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        If adirection = "L" Then
            myview = Session("JobList")
            asproc.Page = myview.RowItem("pages")
        End If
        
        If adirection.Length < 1 And IsNothing(myview) = False Then
            myview = Session("JobList")
        Else
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
        End If
        
        If myview.Count < 1 Then
            Exit Sub
        End If
        
        Session("JobListP") = asproc
        Session("JobList") = myview

        Dim totrecs As String = myview.RowItem("totalrecords")
        Dim currpage As String = myview.RowItem("page")
        Dim totpages As Integer = (totrecs / 14 + 1)
        totpages = myview.RowItem("pages")
         
        Me.lblCurrentPage.Text = currpage
        Me.lblTotalPages.Text = totpages
        Me.lblTotRecs.Text = totrecs
        Dim i As Integer = 0

        ddPage.Items.Clear()
        Do Until i > totpages - 1
            i += 1
            ddPage.Items.Add(i.ToString)
        Loop

        ddPage.SelectedIndex = currpage - 1
        Me.gvwList.DataSource = myview
        Me.gvwList.DataBind()
        Me.MultiView1.SetActiveView(Me.View2)
        
        Me.Session("joblist") = myview
        
    End Sub
     Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = dr("JobId")
      
            e.Row.Cells(1).Text = "" & Strings.Left(e.Row.Cells(1).Text, 7) & ""
          
            e.Row.Cells(2).Text = "" & Strings.Left(e.Row.Cells(2).Text, 20) & ""
            e.Row.Cells(2).Wrap = False

            e.Row.Cells(3).Text = "" & Strings.Left(e.Row.Cells(3).Text, 14) & ""
            e.Row.Cells(3).Wrap = False

            e.Row.Cells(4).Text = "" & Strings.Left(e.Row.Cells(4).Text, 20) & ""
            e.Row.Cells(4).Wrap = False

            e.Row.Cells(5).Text = "" & Strings.Left(e.Row.Cells(5).Text, 5) & ""
            e.Row.Cells(5).Wrap = False

            e.Row.Cells(6).Text = "" & Strings.Left(e.Row.Cells(6).Text, 5) & ""
            e.Row.Cells(6).Wrap = False

            e.Row.Cells(7).Text = "" & Strings.Left(e.Row.Cells(7).Text, 15) & ""
            e.Row.Cells(7).Wrap = False

            e.Row.Cells(8).Text = "" & Utils.FormatDate(DR("DateAssigned")) & ""
            e.Row.Cells(8).Wrap = False


        End If

    End Sub
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim s As String = "?lienview.jobinfo&itemid=" & myitemid
        Response.Redirect(Me.Page.ResolveUrl(s), True)
        
    End Sub
    
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
<ContentTemplate>
<div id="modcontainer" style="MARGIN: 10px; WIDTH: 725px">
<asp:MultiView ID="MultiView1" runat="server">
    <asp:View ID="View1" runat="server">
        <h1 class="panelheader" >Job Filter</h1>
        <div class= "body">

        <asp:Panel ID="panClients" runat="server" >
         <table width="700">
 
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Client Selection</td>
				        <td class="row-data" style="width: 600px">
                            <asp:DropDownList ID="DropDownList1" runat="server" Width="250px">
                            </asp:DropDownList>&nbsp;<asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1" Width="224px">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL">All Clients</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED">Selected Only</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr>
        </table>

        </asp:Panel>

        <table width="700">
 
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Customer Name:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="CustomerName"  CssClass="textbox" Width="250px" MaxLength="45"  />
                            <asp:RadioButtonList ID="CustomerNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource2">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Customer Ref:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="CustomerRef"  CssClass="textbox" Width="250px" MaxLength="45"  />
                            <asp:RadioButtonList ID="CustomerRefLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource2">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Owner Name:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="OwnerName"  CssClass="textbox" Width="250px" meta:resourcekey="OwnerNameResource1" MaxLength="45" />
                            <asp:RadioButtonList ID="OwnerNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="OwnerNameLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource3">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource4">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr><tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Contractor Name:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="GCName"  CssClass="textbox" Width="250px" meta:resourcekey="GCNameResource1" MaxLength="45" />
                            <asp:RadioButtonList ID="GCNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="GCNameLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource5">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource6">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr><tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Lender Name:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="LenderName"  CssClass="textbox" Width="250px" meta:resourcekey="LenderNameResource1" MaxLength="45" />
                            <asp:RadioButtonList ID="LenderNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="LenderNameLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource7">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource8">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr><tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Job Name:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="JobName"  CssClass="textbox" Width="250px" meta:resourcekey="JobNameResource1" MaxLength="45" />
                            <asp:RadioButtonList ID="JobNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="JobNameLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource9">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource10">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr><tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Job Address:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="JobAddress"  CssClass="textbox" Width="250px" meta:resourcekey="JobAddressResource1" MaxLength="45" />
                            <asp:RadioButtonList ID="JobAddressLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="JobAddressLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource11">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource12">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Job #:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="JobNo"  CssClass="textbox" Width="250px" meta:resourcekey="JobNoResource1" MaxLength="45" />
                            <asp:RadioButtonList ID="JobNoLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                RepeatLayout="Flow" meta:resourcekey="JobNoLikeResource1">
                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource13">&nbsp;Starts&nbsp;</asp:ListItem>
                                <asp:ListItem meta:resourcekey="ListItemResource14">&nbsp;Includes</asp:ListItem>
                            </asp:RadioButtonList></td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Assigned Date: </td>
				        <td class="row-data" style="width: 600px">
                            <SiteControls:Calendar ID="Calendar1" runat="server" ErrorMessage="From Date"
                                IsReadOnly="false" IsRequired="true" Value="TODAY" />
                            Thru&nbsp;<SiteControls:Calendar ID="Calendar2" runat="server"
                                ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" Value="TODAY" />
                            &nbsp;&nbsp;<asp:CheckBox ID="chkByAssignDate" CssClass="redcaption" runat="server" Text=" Use Assigned Date Range"
                                Width="224px" /></td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Job City:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="JobCity"  CssClass="textbox" Width="144px" MaxLength="20"  />
                            </td>
			        </tr>
			
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Job State:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="JobState"  CssClass="textbox" style="text-align: center" Width="32px" meta:resourcekey="JobStateResource1" MaxLength="2" />
                            </td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Branch #:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="BranchNo"  CssClass="textbox" Width="67px" meta:resourcekey="BranchNoResource1" MaxLength="10" />
                            </td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            PO #:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="PONum"  CssClass="textbox" Width="144px" meta:resourcekey="PONoResource1" MaxLength="15" />
                            </td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            Status:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="JobStatus"  CssClass="textbox" Width="32px" meta:resourcekey="BranchNoResource1" />&nbsp;&nbsp;&nbsp;
                            &nbsp;<asp:CheckBox ID="chkPaidInFull" CssClass="redcaption" runat="server" Text="  Exclude Paid In Full Jobs"
                                Width="224px" /></td>
			        </tr>
			        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            CRF Temp Id #:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="TempId"  CssClass="textbox" Width="66px" meta:resourcekey="FileNumberResource1" />
                            </td>
			        </tr>
				        <tr>
				        <td class="row-label" style="WIDTH: 104px">
                            CRF File#:</td>
				        <td class="row-data" style="width: 600px">
			            <asp:TextBox runat="server" ID="FileNumber"  CssClass="textbox" Width="66px" meta:resourcekey="FileNumberResource1" />
                            </td>
			        </tr>
	        </table>
	
	        </div>
        <div class= "footer">
            <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">
            <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="button" CausesValidation="False"   />&nbsp;
            <asp:Button ID="btnViewAll" runat="server" Text="View All Jobs" CssClass="button" CausesValidation="False"   />
            <asp:label id="Message" CssClass="normalred" runat="server" ></asp:label>&nbsp;
            </asp:Panel>
            </div>
    </asp:View>
    <asp:View ID="View2" runat="server">
        <h1 class="panelheader" >Job List</h1>
        <div class= "body">
        <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="button" Text="Back to Filter" OnClick="btnBackFilter_Click" CausesValidation="False"></asp:LinkButton>
        <br />
        <br />
         <asp:GridView ID="gvwList" runat="server" CssClass="gridview" AutoGenerateColumns="False" DataKeyNames="JobId"  PageSize="30"  Width="650px">
 	        <RowStyle CssClass="rowstyle"/>
            <AlternatingRowStyle CssClass="altrowstyle" />
              <HeaderStyle CssClass="headerstyle" />
              <Columns>
                <asp:TemplateField HeaderText="View">
            	        <ItemTemplate>
            	
            	        <%--&nbsp;<asp:imagebutton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" Width="15px" />--%>
				        <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
				        </ItemTemplate>
				        <ItemStyle Width="30px" Wrap = "False" />		                     
    	        </asp:TemplateField>
    	
                <asp:BoundField DataField="JobId" SortExpression="JobId"  HeaderText="CRFS#" >
                  <ItemStyle HorizontalAlign="Center" Width="40px" Height="15px" Wrap = "False" />
		            </asp:BoundField>
                <asp:BoundField DataField="JobName"  SortExpression="JobName" HeaderText="Job Name" >
                  <ItemStyle Width="100px" Height="15px" Wrap = "False" />
		            </asp:BoundField>
      	        <asp:BoundField DataField="JobNum" SortExpression="JobNum" HeaderText="Job#"  >
      	          <ItemStyle Width="60px" Height="15px" Wrap = "False" />
		            </asp:BoundField>
      	        <asp:BoundField DataField="CityStateZip" SortExpression="CityStateZip" HeaderText="Job Add"  >
      	          <ItemStyle Width="120px" Height="15px" Wrap = "true" />
		            </asp:BoundField>
                <asp:BoundField DataField="BranchNum" SortExpression="BranchNum" HeaderText="Branch"  >
                  <ItemStyle HorizontalAlign="Center" Width="45px" Height="15px" Wrap = "False" />
		            </asp:BoundField>
      	        <asp:BoundField DataField="StatusCode" SortExpression="StatusCode" HeaderText="Stat"  >
      	          <ItemStyle HorizontalAlign="Center" Width="30px" Height="15px" Wrap = "False" />
		            </asp:BoundField>
      	        <asp:BoundField DataField="ClientCustomer" SortExpression="ClientCustomer" HeaderText="Customer" >
      	          <ItemStyle  Height="15px" Wrap = "True" />
		            </asp:BoundField>
      	        <asp:BoundField DataField="DateAssigned" SortExpression="DateAssigned" HeaderText="Assigned"  >
      	          <ItemStyle Width="30px" Height="15px" Wrap = "False" />
		            </asp:BoundField>
      	        <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client#"  >
      	          <ItemStyle Width="30px" Height="15px" Wrap = "False" />
		            </asp:BoundField>
      
              </Columns>
    
               <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
     
              </asp:GridView>
        </div>
        <br />
        <div class= "footer">
         <table width="100%">
            <tr>
                <td>
                (Total Jobs on this List

                    <asp:label id="lblTotRecs" Runat="server"></asp:label>        
                 ) (Page

                    <asp:label id="lblCurrentPage" Runat="server"></asp:label> of 

                    <asp:label id="lblTotalPages" Runat="server"></asp:label>)


                </td>

                <td valign="top" align="right">

                    Page

                    <asp:DropDownList id="ddPage" runat="server"

                        AutoPostBack="true"></asp:DropDownList>

                </td> 

                <td align="right"> 

                    <asp:imagebutton id="btnFirst" Runat="server" Enabled="true" 

                        ImageUrl="~/App_Themes/VbJuice/Img/resultset_First.png" OnClick="btnFirst_Click" /> 

                    <asp:imagebutton id="btnPrevious" Runat="server" Enabled="true"

                       ImageUrl="~/App_Themes/VbJuice/Img/resultset_Previous.png" OnClick="btnPrevious_Click" /> 

                    <asp:imagebutton id="btnNext" Runat="server" Enabled="true"

                       ImageUrl="~/App_Themes/VbJuice/Img/resultset_Next.png" OnClick="btnNext_Click" /> 

                    <asp:imagebutton id="btnLast" Runat="server" Enabled="true" 

                       ImageUrl="~/App_Themes/VbJuice/Img/resultset_Last.png" OnClick="btnLast_Click" /> 

                </td> 

            </tr>
        </table>
        </div>
    </asp:View>
</asp:MultiView>
        <div class="panel" style="margin-top:30px;">
            <header class="panel-heading">
              <div class="panel-actions"></div>
              <h3 class="panel-title">Basic</h3>
            </header>
            <div class="panel-body">
                <table style="font-size:12px;width:100%;" class="table  dataTable table-striped width-full" data-plugin="dataTable">
                    <thead>
                        <tr>
                            <th style="width:25%">Name</th>
                            <th style="width:25%">Position</th>
                            <th style="width:25%">Office</th>
                            <th style="width:5%">Age</th>
                            <th style="width:10%">Date</th>
                            <th style="width:10%">Salary</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                    <td>Damon</td>
                    <td>5516 Adolfo Green</td>
                    <td>Littelhaven</td>
                    <td>85</td>
                    <td>2014/06/13</td>
                    <td>$553,536</td>
                    </tr>
                    <tr>
                    <td>Torrey</td>
                    <td>1995 Richie Neck</td>
                    <td>West Sedrickstad</td>
                    <td>77</td>
                    <td>2014/09/12</td>
                    <td>$243,577</td>
                    </tr>
                    <tr>
                    <td>Miracle</td>
                    <td>176 Hirthe Squares</td>
                    <td>Ryleetown</td>
                    <td>82</td>
                    <td>2013/09/27</td>
                    <td>$784,802</td>
                    </tr>
                    <tr>
                    <td>Wilhelmine</td>
                    <td>44727 O&#x27;Hara Union</td>
                    <td>Dibbertfurt</td>
                    <td>68</td>
                    <td>2013/06/28</td>
                    <td>$207,291</td>
                    </tr>
                    <tr>
                    <td>Hubert</td>
                    <td>8884 Jamel Pines</td>
                    <td>Howemouth</td>
                    <td>63</td>
                    <td>2013/05/28</td>
                    <td>$584,032</td>
                    </tr>
                    <tr>
                    <td>Myrtie.Gerhold</td>
                    <td>098 Noel Way</td>
                    <td>Santinoland</td>
                    <td>13</td>
                    <td>2014/12/12</td>
                    <td>$550,087</td>
                    </tr>
                    <tr>
                    <td>Chester</td>
                    <td>14095 Kling Gateway</td>
                    <td>Andresmouth</td>
                    <td>26</td>
                    <td>2014/09/27</td>
                    <td>$177,404</td>
                    </tr>
                    <tr>
                    <td>Melany_Gerhold</td>
                    <td>1100 Steve Pines</td>
                    <td>Immanuelfort</td>
                    <td>12</td>
                    <td>2014/06/28</td>
                    <td>$162,453</td>
                    </tr>
                    <tr>
                    <td>Thea</td>
                    <td>26114 Narciso Lodge</td>
                    <td>East Opal</td>
                    <td>64</td>
                    <td>2014/11/12</td>
                    <td>$581,736</td>
                    </tr>
                    <tr>
                    <td>Emerson</td>
                    <td>784 Adriel Radial</td>
                    <td>New Jerroldland</td>
                    <td>4</td>
                    <td>2014/02/26</td>
                    <td>$805,823</td>
                    </tr>
                    <tr>
                    <td>Herta</td>
                    <td>7491 Bednar Gardens</td>
                    <td>Port Lucianoton</td>
                    <td>23</td>
                    <td>2013/10/12</td>
                    <td>$352,269</td>
                    </tr>
                    <tr>
                    <td>Stone_Deckow</td>
                    <td>6440 Moen Loop</td>
                    <td>Jenningsbury</td>
                    <td>23</td>
                    <td>2014/07/28</td>
                    <td>$219,573</td>
                    </tr>
                    <tr>
                    <td>Davin</td>
                    <td>50922 Kiara Square</td>
                    <td>Port Edmund</td>
                    <td>37</td>
                    <td>2014/11/27</td>
                    <td>$241,432</td>
                    </tr>
                    <tr>
                    <td>Johnathan_Mraz</td>
                    <td>1998 Webster Fords</td>
                    <td>East Vern</td>
                    <td>50</td>
                    <td>2014/09/12</td>
                    <td>$290,875</td>
                    </tr>
                    <tr>
                    <td>Gunnar</td>
                    <td>92873 Barney Club</td>
                    <td>Beierview</td>
                    <td>82</td>
                    <td>2014/03/29</td>
                    <td>$569,778</td>
                    </tr>
                    <tr>
                    <td>Raina</td>
                    <td>828 Cathy Streets</td>
                    <td>West Uriahville</td>
                    <td>26</td>
                    <td>2013/09/27</td>
                    <td>$186,229</td>
                    </tr>
                    <tr>
                    <td>Marjorie.Orn</td>
                    <td>314 Aurore Canyon</td>
                    <td>Port Jaquelineburgh</td>
                    <td>3</td>
                    <td>2014/06/28</td>
                    <td>$547,752</td>
                    </tr>
                    <tr>
                    <td>Citlalli_Wehner</td>
                    <td>139 Ebert Freeway</td>
                    <td>Lake Esperanzamouth</td>
                    <td>78</td>
                    <td>2015/01/27</td>
                    <td>$892,012</td>
                    </tr>
                    <tr>
                    <td>Ruben.Reilly</td>
                    <td>02868 Cronin Tunnel</td>
                    <td>Rossieville</td>
                    <td>87</td>
                    <td>2013/09/12</td>
                    <td>$520,483</td>
                    </tr>
                    <tr>
                    <td>Gunner_Jakubowski</td>
                    <td>80391 Maxwell Parks</td>
                    <td>South Travon</td>
                    <td>26</td>
                    <td>2014/03/29</td>
                    <td>$272,005</td>
                    </tr>
                    <tr>
                    <td>Josephine</td>
                    <td>36238 Satterfield Avenue</td>
                    <td>New Alexanderhaven</td>
                    <td>51</td>
                    <td>2015/01/27</td>
                    <td>$189,18</td>
                    </tr>
                    <tr>
                    <td>Ceasar_Orn</td>
                    <td>2795 Clement Ridges</td>
                    <td>Beckerhaven</td>
                    <td>78</td>
                    <td>2013/11/27</td>
                    <td>$958,117</td>
                    </tr>
                    <tr>
                    <td>Coby</td>
                    <td>53700 Pagac Lodge</td>
                    <td>South Hershel</td>
                    <td>86</td>
                    <td>2013/08/28</td>
                    <td>$481,619</td>
                    </tr>
                    <tr>
                    <td>Colin</td>
                    <td>637 Paucek Mountain</td>
                    <td>West Luraberg</td>
                    <td>77</td>
                    <td>2015/02/26</td>
                    <td>$298,110</td>
                    </tr>
                    <tr>
                    <td>Monique_White</td>
                    <td>415 Corkery Walks</td>
                    <td>West Lauryn</td>
                    <td>97</td>
                    <td>2014/02/11</td>
                    <td>$222,343</td>
                    </tr>
                    <tr>
                    <td>Jarvis.Simonis</td>
                    <td>0778 Elvis Spurs</td>
                    <td>Harrisfurt</td>
                    <td>62</td>
                    <td>2013/05/28</td>
                    <td>$336,046</td>
                    </tr>
                </tbody>
                </table>
            </div>
        </div>
        
</div>
<asp:UpdateProgress ID="siteUpdateProgress" runat="server">
<ProgressTemplate>
<div class="TransparentGrayBackground"></div>
<asp:Panel  ID="alwaysVisibleAjaxPanel" runat="server" >
<div class="PageUpdateProgress">
<asp:Image  ID="ajaxLoadNotificationImage" 
runat="server" 
ImageUrl="~/images/ajax-loader.gif" 
AlternateText="[image]" />
&nbsp;Please Wait...
</div>
</asp:Panel>
<ajaxToolKit:AlwaysVisibleControlExtender 
ID="AlwaysVisibleControlExtender1" 
runat="server"
TargetControlID="alwaysVisibleAjaxPanel"
HorizontalSide="Center"
HorizontalOffset="150"
VerticalSide="Middle"
VerticalOffset="0"
>
</ajaxToolKit:AlwaysVisibleControlExtender>
</ProgressTemplate>
</asp:UpdateProgress>
</ContentTemplate>
</asp:UpdatePanel>

<script src="../../../global/vendor/jquery/jquery.js"></script>
  <script src="../../../global/vendor/bootstrap/bootstrap.js"></script>
  <script src="../../../global/vendor/animsition/animsition.js"></script>
  <script src="../../../global/vendor/asscroll/jquery-asScroll.js"></script>
  <script src="../../../global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="../../../global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
  <script src="../../../global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
  <!-- Plugins -->
  <script src="../../../global/vendor/switchery/switchery.min.js"></script>
  <script src="../../../global/vendor/intro-js/intro.js"></script>
  <script src="../../../global/vendor/screenfull/screenfull.js"></script>
  <script src="../../../global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="../../../global/vendor/datatables/jquery.dataTables.js"></script>
  <script src="../../../global/vendor/datatables-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="../../../global/vendor/datatables-bootstrap/dataTables.bootstrap.js"></script>
  <script src="../../../global/vendor/datatables-responsive/dataTables.responsive.js"></script>
  <script src="../../../global/vendor/datatables-tabletools/dataTables.tableTools.js"></script>
  <script src="../../../global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="../../../global/vendor/bootbox/bootbox.js"></script>
  <!-- Scripts -->
  <script src="../../../global/js/core.js"></script>
  <script src="../../../assets/js/site.js"></script>
  <script src="../../../assets/js/sections/menu.js"></script>
  <script src="../../../assets/js/sections/menubar.js"></script>
  <script src="../../../assets/js/sections/gridmenu.js"></script>
  <script src="../../../assets/js/sections/sidebar.js"></script>
  <script src="../../../global/js/configs/config-colors.js"></script>
  <script src="../../../assets/js/configs/config-tour.js"></script>
  <script src="../../../global/js/components/asscrollable.js"></script>
  <script src="../../../global/js/components/animsition.js"></script>
  <script src="../../../global/js/components/slidepanel.js"></script>
  <script src="../../../global/js/components/switchery.js"></script>
  <script src="../../../global/js/components/datatables.js"></script>
  <script src="../../../assets/examples/js/tables/datatable.js"></script>
  <script src="../../../assets/examples/js/uikit/icon.js"></script>
<script>
    
</script>


  