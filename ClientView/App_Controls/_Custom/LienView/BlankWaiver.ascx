<%@ Control Language="VB" AutoEventWireup="false" CodeFile="BlankWaiver.ascx.vb"
    Inherits="App_Controls__Custom_LienView_BlankWaiver" %>
<%@ Register Src="AddSigner.ascx" TagName="AddSigner" TagPrefix="uc3" %>
<%@ Register Src="GCSearch.ascx" TagName="GCSearch" TagPrefix="uc2" %>
<%@ Register Src="CustNameSearch.ascx" TagName="CustNameSearch" TagPrefix="uc1" %>
<style>
        #clsdropdownClient .btn-group {width:100%;}
#clsdropdownClient .btn-group .btn {width:90%;}
#clsdropdownClient .btn-group .btn.dropdown-toggle {width:100%;}
#clsdropdownClient .btn-group .dropdown-menu {width:100%;}
</style>
<script type="text/javascript">
    function fadeout() {
        debugger;
        //alert(currURL);
      //  location.href = currURL;
        $('div').removeClass('modal-backdrop fade in');
        return false();
    }
    function DisplayWaiverPopUp() {
        // alert('hi');

        currURL = window.location.href;
        $("#ModalWaiverSubmit").modal('show');
    }
    function HideWaiverPopUp() {
        // alert('hi');
        $("#ModalWaiverSubmit").modal('hide');
    }
    function DisplayEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('show');
    }
    function HideEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('hide');
        $('div').removeClass('modal-backdrop fade in');
        //fadeout();

    }
    function EmailSendPopup() {
        debugger;
        swal({
            title: "Successfully Sent!",
            text: "Your Waiver Has Been Emailed.",
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false

        });

    }

    function erroemsg(msg) {
        debugger;
        alert(msg);
    }

    function Cancelpopup() {
          CloseEmailLoadPage();
        return false;
    }
    function CloseEmailLoadPage() {               
        if ($('#'+'<%=hdnflag.ClientID %>').val()!="")
        {            
            if($('#'+'<%=hdnflag.ClientID %>').val()=="edit")
            {
                //alert('edit');
                document.getElementById('<%=hdnbtnclose.ClientID%>').click();
            }
            else if ($('#'+'<%=hdnflag.ClientID %>').val()=="grid") {
                //alert('grid');
                
                CallSuccessFunc();
                fadeout();
            }
            else {
                //alert('else');
                fadeout();
            }
        }
       
    }



    function SendMail() {
        if (document.getElementById('<%=txtToList.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the 'To' EmailId.";
            return false;
        }
        if (document.getElementById('<%=txtSubject.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the Subject.";
            return false;
        }
        document.getElementById('<%=btnEmailsend.ClientID%>').click();

        HideEmailPopUp();

        return false;
    }
    function OpenCustSearchModal() {
        document.getElementById("rbtnByName").checked = true;
        document.getElementById("rbtnByRef").checked = false;
        $('#txtSearchStr').val('');
        $('#listCustName option').remove();
        $("#ModalCustSearch").modal('show');
        document.getElementById("listCustName").style.display = 'none';
        return false;
    }
    function OpenGCModal() {
        document.getElementById("listGCNames").style.display = 'none';
        $('#txtGCSearchText').val('');
        $("#ModalGCSearch").modal('show');
    }
    function DeleteAccount(id) {
        document.getElementById('<%=hdnId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteId.ClientID%>').click();
       });
         return false;
     }
     function noenter(e) {

         //Added by jay for the search button click
         //debugger;
         if ((e.target || e.srcElement).id == "txtSearchStr" || (e.target || e.srcElement).id == "txtGCSearchText") {
             e = e || window.event;
             var key = e.keyCode || e.charCode;
             if (key == 13) {
                 if ((e.target || e.srcElement).id == "txtSearchStr") {
                     SearchCustName();
                 }
                 else if ((e.target || e.srcElement).id == "txtGCSearchText") {
                     GetGCNameList();
                 }
                 // event.returnValue = false;
                 (event.preventDefault) ? event.preventDefault() : event.returnValue = false;
                 //  e.preventDefault();
             }
         }
         else {
             e = e || window.event;
             var key = e.keyCode || e.charCode;
             return key !== 13;
         }
         //return false;
     }
     function SearchCustName() {
         //debugger;
         //alert('1');
         var SearchStr = document.getElementById("txtSearchStr").value;
         if (SearchStr != "") {
             var type;
             if (document.getElementById("rbtnByName").checked)
                 type = "Name";
             else
                 type = "Ref";
             var ClientId = document.getElementById('<%=hdnClientSelectedId.ClientID%>').value;// document.getElementById("ctl25_ctl00_CustSearchClientID").value;

            PageMethods.CustSearchList(SearchStr, type, ClientId, BindCustomer);
        }
        else {
            $('#listCustName option').remove();
        }
    }
    function BindCustomer(result) {

        document.getElementById("listCustName").style.display = 'block';
        $('#listCustName option').remove();
        if (result.length > 0) {
            var ele = document.getElementById("listCustName");
            $('#listCustName option').remove();

            for (var i = 0; i < result.length; i++) {
                var opt = document.createElement('option');
                opt.text = result[i].Text;
                opt.value = result[i].Value;
                ele.appendChild(opt);
            }
        }
    }
    function GetCustomer() {
        var custId = document.getElementById("listCustName").value;
       // alert(custId);
        PageMethods.GetCustomer(custId, LoadCustomer);
    }
    function LoadCustomer(result) {
        // alert(result.CustName);
        //console.log(result);
        var custId = document.getElementById("listCustName").value;
        if (custId.substring(0, 1) == 'B') {
            // alert('1');
            document.getElementById('<%=CustName.ClientID%>').value = result.CustName;
            document.getElementById('<%=CustRef.ClientID%>').value = result.CustRefNum;
            document.getElementById('<%=CustAdd1.ClientID%>').value = result.CustAdd1;
            document.getElementById('<%=CustCity.ClientID%>').value = result.CustCity;
            document.getElementById('<%=CustState.ClientID%>').value = result.CustState;
            document.getElementById('<%=CustZip.ClientID%>').value = result.CustZip;
          //  document.getElementById('<%=hdncustEmail.ClientID%>').value = result.CustEmail;
        }
        else {
            document.getElementById('<%=CustName.ClientID%>').value = result.ClientCustomer;
            document.getElementById('<%=CustRef.ClientID%>').value = result.RefNum;
            document.getElementById('<%=CustAdd1.ClientID%>').value = result.AddressLine1;
            document.getElementById('<%=CustCity.ClientID%>').value = result.City;
            document.getElementById('<%=CustState.ClientID%>').value = result.State;
            document.getElementById('<%=CustZip.ClientID%>').value = result.PostalCode;
            //document.getElementById('<%=hdncustEmail.ClientID%>').value = result.Email;
        }

        $("#ModalCustSearch").modal('hide');
    }
    function GetGCNameList() {
        var txtGC = document.getElementById("txtGCSearchText").value;
        if (txtGC != "") {
            var ClientId = document.getElementById('<%=hdnClientSelectedId.ClientID%>').value;
            PageMethods.GetGCNames(txtGC, ClientId, LoadGCNames);
        }
        else {
            $('#listGCNames option').remove();
        }
        return false;
    }

    function LoadGCNames(result) {
        document.getElementById("listGCNames").style.display = 'block';
        $('#listGCNames option').remove();
        if (result.length > 0) {
            var ele = document.getElementById("listGCNames");
            $('#listGCNames option').remove();

            for (var i = 0; i < result.length; i++) {
                var opt = document.createElement('option');
                opt.text = result[i].Text;
                opt.value = result[i].Value;
                ele.appendChild(opt);
            }
        }
    }

    function GetGCDetails(cntrl) {
        //var GCString = cntrl.value;
        var GCString = document.getElementById("listGCNames").value;
        // alert('hi');
        // alert(GCString);
        if (GCString != "") {

            var clientId = document.getElementById('<%=hdnGCSearchClientID.ClientID%>').value;
            //alert(clientId);
            PageMethods.GetGCDetails(GCString, clientId, LoadGCDetails);
        }
    }

    function LoadGCDetails(result) {
        //  alert('hey');
        var cntrl = document.getElementById("listGCNames").value;
        //  alert(cntrl);
        //debugger;
        if (cntrl.substring(0, 1) == "B") {
            document.getElementById('<%=GCName.ClientID%>').value = result.GCName;
            document.getElementById('<%=GCRef.ClientID%>').value = result.GCRefNum;
            document.getElementById('<%=GCAdd1.ClientID%>').value = result.GCAdd1;
            document.getElementById('<%=GCCity.ClientID%>').value = result.GCCity;
            document.getElementById('<%=GCZip.ClientID%>').value = result.GCZip;
            document.getElementById('<%=GCState.ClientID%>').value = result.GCState;
        }
        else {
            document.getElementById('<%=GCName.ClientID%>').value = result.GeneralContractor;
            document.getElementById('<%=GCRef.ClientID%>').value = result.RefNum;
            document.getElementById('<%=GCAdd1.ClientID%>').value = result.AddressLine1;
            document.getElementById('<%=GCCity.ClientID%>').value = result.City;
            document.getElementById('<%=GCZip.ClientID%>').value = result.PostalCode;
            document.getElementById('<%=GCState.ClientID%>').value = result.State;
        }
        $("#ModalGCSearch").modal('hide');
    }
</script>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        CallSuccessFunc();
    });
    function confirmSubmit() {
        alert("confirmSubmit");
        var agree = confirm("Are you sure you wish to continue?");
        if (agree)
            document.getElementById("hid1").value = 1;

        else
            document.getElementById("hid1").value = 0;
    }
    function SetTabName(id) {
        document.getElementById('<%=hddbTabName.ClientID%>').value = id;
    }
    function ActivateTab(TabName) {
       // alert('hi');
       // alert(TabName);
        switch (TabName) {
            case "WaiverInfo":
                if (!$('#WaiverInfo').hasClass('active')) {
                    $('#WaiverInfo').addClass('active');
                }
                if (!$('#exampleTabsOne').hasClass('active'))
                    $('#exampleTabsOne').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CustomerInfo').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#GeneralContractor').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                break;
            case "JobInfo":
                if (!$('#JobInfo').hasClass('active')) {
                    $('#JobInfo').addClass('active');
                }
                if (!$('#exampleTabsTwo').hasClass('active'))
                    $('#exampleTabsTwo').addClass('active');
                $('#WaiverInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#CustomerInfo').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#GeneralContractor').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                break;
            case "CustomerInfo":
                if (!$('#CustomerInfo').hasClass('active')) {
                    $('#CustomerInfo').addClass('active');
                }
                if (!$('#exampleTabsThree').hasClass('active'))
                    $('#exampleTabsThree').addClass('active');
                $('#WaiverInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#JobInfo').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#GeneralContractor').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                break;
            case "GeneralContractor":
                if (!$('#GeneralContractor').hasClass('active')) {
                    $('#GeneralContractor').addClass('active');
                }
                if (!$('#exampleTabsFour').hasClass('active'))
                    $('#exampleTabsFour').addClass('active');
                $('#WaiverInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#JobInfo').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CustomerInfo').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                break;
            case "Owner":
                if (!$('#Owner').hasClass('active')) {
                    $('#Owner').addClass('active');
                }
                if (!$('#exampleTabsFive').hasClass('active'))
                    $('#exampleTabsFive').addClass('active');
                $('#WaiverInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#JobInfo').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CustomerInfo').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#GeneralContractor').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                break;
            case "Lender":
                if (!$('#Lender').hasClass('active')) {
                    $('#Lender').addClass('active');
                }
                if (!$('#exampleTabsSix').hasClass('active'))
                    $('#exampleTabsSix').addClass('active');
                $('#WaiverInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#JobInfo').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CustomerInfo').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#GeneralContractor').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                break;

        }
    }
    function OpenAddNewSignModal() {
        $("#ModalAddSign").modal('show');
    }

    function CallSuccessFunc() {
        debugger;
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [15, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "bDestroy": true,
            "aaSorting": [[2]],
            //aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());

        $('#' + '<%=gvwWaivers.ClientID%>').dataTable(options);
    }
</script>
<script type="text/javascript">

    function BindSignerDropdown(Signerid) {
        // alert(Signerid);
        $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(Signerid)
        PageMethods.GetSignerList(LoadSignerList);
    }
    function LoadSignerList(result) {

        // alert($('#' + '<%=hdnSignerSelectedValue.ClientID%>').val());
        $('#ulDropDownSigner li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnSignerSelectedValue.ClientID%>').val() == "" || $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(result[i].Value);
                }
                var ResultReplace = ReplaceSpecChar(result[i].Text);
                $('#ulDropDownSigner').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liSign_' + result[i].Value + '" onclick="setSelectedSignerId(' + result[i].Value + ',\'' + ResultReplace + '\')">' + result[i].Text + '</a></li>');

            }
        }
        // alert($('#' + '<%=hdnSignerSelectedValue.ClientID%>').val());
        $('#btnDropSigner').html($('#liSign_' + $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedSignerId(id, TextData) {
        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(id);
        $('#btnDropSigner').html(ResultReplace + "<span class='caret' ></span>");
        return false;
    }

    function ReplaceSpecChar(Textdata) {
        //alert(Textdata);
        var result = Textdata;
        var ResultReplace = result.replace("'", "`");
        //alert("ResultReplace" + ResultReplace);
        return ResultReplace;
    }
    function BindWaiverTypeDropdown(JobState) {
        // alert(JobState);
        PageMethods.GetWaiverTypeList(JobState, LoadWaiverTypeList);
    }
    function LoadWaiverTypeList(result) {
        // alert($('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val());
        $('#ulDropDownWaiverType li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val() == "" || $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val() == "0") {
                     $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val(result[i].Value);
                    $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val(result[i].Text);
                }
                $('#ulDropDownWaiverType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liWaiverType_' + result[i].Value + '" onclick="setSelectedWaiverTypeId(' + result[i].Value + ',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');


            }
        }
         //alert($('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val());
        $('#btnDropWaiverType').html($('#liWaiverType_' + $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }

    function setSelectedWaiverTypeId(id, TextData) {
        $('#' + '<%=hdnWaiverTypeSelectedId.ClientID%>').val(id);
         $('#' + '<%=hdnWaiverTypeSelectedText.ClientID%>').val(TextData);
         document.getElementById('<%=FormId.ClientID%>').click();
         $('#btnDropWaiverType').html(TextData + "<span class='caret' ></span>");
         return false;
     }
    function BindStateDropdown() {
        PageMethods.GetStateList(LoadStateList);
    }
    function LoadStateList(result) {

        $('#ulDropDownState li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnStateSelectedValue.ClientID%>').val() == "" || $('#' + '<%=hdnStateSelectedValue.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnStateSelectedValue.ClientID%>').val(result[i].Value);
                }
                $('#ulDropDownState').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liState_' + result[i].Value + '" onclick="setSelectedStateId(\'' + result[i].Value + '\',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');
            }
        }

        if ($('#' + '<%=hdnStateSelectedValue.ClientID%>').val() != "") {
            $('#btnDropState').html($('#liState_' + $('#' + '<%=hdnStateSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedStateId(id, TextData) {
        $('#' + '<%=hdnStateSelectedValue.ClientID%>').val(id);
        document.getElementById('<%=btnDropStateClick.ClientID%>').click();
        $('#btnDropState').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    function BindClientDropdown(Id) {

        PageMethods.GetClientList(Id, LoadClientList);
    }
    function LoadClientList(result) {

        $('#uldrpClient li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnClientSelectedId.ClientID%>').val() == "" || $('#' + '<%=hdnClientSelectedId.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnClientSelectedId.ClientID%>').val(result[i].Value);
                }
                var ResultReplace = ReplaceSpecChar(result[i].Text);
                $('#uldrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClient_' + result[i].Value + '" onclick="setSelectedClientId(' + result[i].Value + ',\'' + ResultReplace + '\')">' + result[i].Text + '</a></li>');
            }
        }
        $('#btnClientDropDownSelected').html($('#liClient_' + $('#' + '<%=hdnClientSelectedId.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedClientId(id, TextData) {
        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnClientSelectedId.ClientID%>').val(id);
        document.getElementById('<%=btnDrpClient.ClientID%>').click();
        $('#btnClientDropDownSelected').html(ResultReplace + "<span class='caret' ></span>");
        return false;
    }
</script>

<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script type="text/javascript">
    function MaintainMenuOpen() {
        // debugger;
        // alert('gh');
        SetHeaderBreadCrumb('LienView', 'Tools', 'Blank Waivers');
        MainMenuToggle('liTools');
        SubMenuToggle('liBlankWaivers');
        //  var PageName = window.location.search.substring(1);
        ////  alert(PageName);
        //  if (PageName == "lienview.blankwaiver.JobView") {

        //  }
        //  else {
        //      SetHeaderBreadCrumb('LienView', 'Tools', 'Blank Waivers');
        //      MainMenuToggle('liTools');
        //      SubMenuToggle('liBlankWaivers');
        //  }

        //$('#liTools').addClass('site-menu-item has-sub active open');
        //$('#liBlankWaivers').addClass('site-menu-item active');
        return false;

    }

    function myNotary(id) {
        if (id == 1) {
            if (document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithCA.ClientID%>').checked == false) {

                document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
                document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
            }
            else if (document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithCA.ClientID%>').checked == true) {

                document.getElementById('<%=NotarywithCA.ClientID%>').checked = true;
                 document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
             }
             else {

                 document.getElementById('<%=NotarywithCA.ClientID%>').checked = true;
                 document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
             }
     }
     else {
         if (document.getElementById('<%=NotarywithCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false) {

                document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
             document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = false;
         }
         else if (document.getElementById('<%=NotarywithoutCA.ClientID%>').checked == false && document.getElementById('<%=NotarywithCA.ClientID%>').checked == true) {

             document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
           document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = true;
       }
       else {

           document.getElementById('<%=NotarywithCA.ClientID%>').checked = false;
           document.getElementById('<%=NotarywithoutCA.ClientID%>').checked = true;
       }
}
}

</script>
<style>
    .align-lable {
        text-align: right;
    }
</style>

<div id="modcontainer" style="width: 100%;">
    <h1 class="panelheader">Blank Waiver</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12" style="text-align: left;">
                            <asp:Button ID="btnback" runat="server" Text="Back To Waivers" CssClass="btn btn-primary" />
                            <asp:HiddenField runat="server" ID="hddbTabName" Value="WaiverInfo" />
                        </div>
                    </div>
                </div>
                <div class="nav-tabs-horizontal">
                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                        <li id="WaiverInfo" class="active" role="presentation"><a data-toggle="tab" onclick="SetTabName('WaiverInfo');" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Waiver Info</a></li>
                        <li id="JobInfo" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('JobInfo');" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Job Info</a></li>
                        <li id="CustomerInfo" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('CustomerInfo');" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab" aria-expanded="false">Customer Info</a></li>
                        <li id="GeneralContractor" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('GeneralContractor');" href="#exampleTabsFour" aria-controls="exampleTabsFour" role="tab" aria-expanded="false">General Contractor</a></li>
                        <li id="Owner" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Owner');" href="#exampleTabsFive" aria-controls="exampleTabsFive" role="tab" aria-expanded="false">Owner</a></li>
                        <li id="Lender" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Lender');" href="#exampleTabsSix" aria-controls="exampleTabsSix" role="tab" aria-expanded="false">Lender</a></li>

                    </ul>
                    <div class="tab-content padding-top-20" style="border: 1px solid lightgray;">
                        <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                            <div class="form-horizontal">

                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Client#:</label>
                                    <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;" id="clsdropdownClient">
                                        <div class="btn-group" style="text-align: left;" align="left">
                                            <button type="button" class="btn btn-default dropdown-toggle" style="text-align: left;" align="left"
                                                data-toggle="dropdown" aria-expanded="false" id="btnClientDropDownSelected" TabIndex="1">
                                                --Select Client--
                     
                                                                                    <span class="caret"></span>
                                                                                </button>
                                                                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="uldrpClient" style="min-width:450px;overflow-y: auto; height: 150px;">
                                                                                </ul>
                                                                            </div>
                                                                            <asp:HiddenField ID="hdnClientSelectedId" runat="server" Value="" />
                                                                            <asp:Button ID="btnDrpClient" runat="server" Style="display: none" OnClick="btnDrpClient_Click" />
                                                                            <cc1:ExtendedDropDown ID="ClientTableId" CssClass="dropdown" runat="server" Width="296px" Style="display: none"
                                                                                AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                                                                IsValid="True" Tag="" Value="">
                                                                            </cc1:ExtendedDropDown>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable align-lable">State:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <div class="btn-group" style="text-align: left;" align="left">
                                                                                <button type="button" class="btn btn-default dropdown-toggle" style="width: 130px; " align="left"
                                                                                    data-toggle="dropdown" aria-expanded="false" id="btnDropState" TabIndex="2">
                                                                                    --Select State--
                     
                                                                                    <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState" style="overflow-y: auto; height: 250px;">
                                            </ul>
                                        </div>

                                        <asp:HiddenField ID="hdnStateSelectedValue" runat="server" Value="" />
                                        <asp:Button ID="btnDropStateClick" runat="server" Style="display: none;" OnClick="btnDropStateClick_Click" />
                                        <cc1:ExtendedDropDown ID="StateTableId" CssClass="dropdown" runat="server" Width="184px"
                                            AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                            IsValid="True" Tag="" Value="" Style="display: none;">
                                        </cc1:ExtendedDropDown>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4  col-sm-4 col-xs-4 control-label align-lable">Waiver Type:</label>
                                    <div class="col-md-7  col-sm-7 col-xs-7" style="text-align: left;">
                                        <div class="btn-group" style="text-align: left;" align="left">
                                            <button type="button" class="btn btn-default dropdown-toggle col-md-12" style="min-width: 200px;" align="left"
                                                data-toggle="dropdown" aria-expanded="false" id="btnDropWaiverType" TabIndex="3">
                                                --Select Waiver Type--
                     
                                                                                    <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownWaiverType" style="overflow-y: auto; height: 180px;">
                                            </ul>
                                        </div>
                                        <asp:HiddenField ID="hdnWaiverTypeSelectedId" runat="server" Value="" />
                                          <asp:HiddenField ID="hdnWaiverTypeSelectedText" runat="server" Value="" />
                                        <cc1:ExtendedDropDown ID="FormId" CssClass="dropdown" runat="server" Width="240px"
                                            BackColor="White" CaseSensitiveKeySort="False" ErrorMessage="" IsValid="True"
                                            Tag="" Value="" Style="display: none;">
                                        </cc1:ExtendedDropDown>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4  col-sm-4 col-xs-4 control-label align-lable">Payment Amt:</label>
                                    <div class="col-md-2  col-sm-2 col-xs-2" style="text-align: left;">
                                        <cc1:DataEntryBox ID="PaymentAmt" runat="server" CssClass="form-control" DataType="Money"
                                            IsRequired="False" TabIndex="4" Tag="Balance" EnableClientSideScript="False"
                                            ErrorMessage="" FormatMask="" FormatString="{0:d3}" FriendlyName="" IsValid="True"
                                            ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                            Value="0.00">$0.00</cc1:DataEntryBox>



                                                                        </div>
                                                                        <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                                                                            <div class="checkbox-custom checkbox-default">
                                                                                <asp:CheckBox ID="IsPaidInFull" runat="server" Text="Is Paid to Date" TabIndex="5" />
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Disputed Amt:</label>
                                                                        <div class="col-md-2  col-sm-2 col-xs-2" style="text-align: left;">
                                                                            <cc1:DataEntryBox ID="DisputedAmt" runat="server" CssClass="form-control" DataType="Money"
                                                                                IsRequired="False" TabIndex="6" Tag="Balance" EnableClientSideScript="False"
                                                                                ErrorMessage="" FormatMask="" FormatString="{0:d3}" FriendlyName="" IsValid="True"
                                                                                ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                                                                Value="0.00">$0.00</cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Mail Waiver To:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <div class="example-wrap" style="margin-bottom: 0;">
                                                                                <div class="radio-custom radio-default">
                                                                                    <asp:RadioButton ID="MailtoCU" Width="100px" CssClass="redcaption" Style="text-align: left; padding-left: 0px;" runat="server" Text=" Customer"
                                                                                        GroupName="MailTo" Checked="True" TabIndex="7"></asp:RadioButton>
                                                                                    <asp:RadioButton ID="MailtoGC" Width="100px" CssClass="redcaption" runat="server" Text=" Gen Cont"
                                                                                        GroupName="MailTo" TabIndex="8"></asp:RadioButton>
                                                                                    <asp:RadioButton ID="MailtoOW" Width="80px" CssClass="redcaption" runat="server" Text=" Owner"
                                                                                        GroupName="MailTo" TabIndex="9"></asp:RadioButton>
                                                                                    <asp:RadioButton ID="MailtoLE" Width="100px" CssClass="redcaption" runat="server" Text=" Lender"
                                                                                        GroupName="MailTo" TabIndex="10"></asp:RadioButton>
                                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Who Issues The Check:</label>
                                                                        <div class="col-md-7 col-sm-8 col-xs-8" style="text-align: left;">
                                                                            <div class="example-wrap" style="margin-bottom: 0;">
                                                                                <div class="radio-custom radio-default">
                                                                                    <asp:RadioButton ID="CheckbyCU" Width="100px" CssClass="redcaption  " Style="text-align: left; padding-left: 0px;" runat="server" Text=" Customer"
                                                                                        GroupName="Issuer" Checked="True" TabIndex="11"></asp:RadioButton>
                                                                                     <asp:RadioButton ID="CheckByGC" Width="100px" CssClass="redcaption " runat="server" Text=" Gen Cont"
                                                                                        GroupName="Issuer" TabIndex="12"></asp:RadioButton>                                                                                  
                                                                                    <asp:RadioButton ID="CheckByOW" Width="80px" CssClass="redcaption " runat="server" Text=" Owner"
                                                                                        GroupName="Issuer" TabIndex="13"></asp:RadioButton>
                                                                                    <asp:RadioButton ID="CheckByLE" Width="90px" CssClass="redcaption " runat="server" Text=" Lender"
                                                                                        GroupName="Issuer" TabIndex="14"></asp:RadioButton>
                                                                                    <asp:RadioButton ID="CheckbyOT" runat="server" Width="100px" GroupName="Issuer" Text=" Other" 
                                                                                        CssClass="redcaption" TabIndex="15"></asp:RadioButton>                                                                                     
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Other Check Receiver:</label>
                                                                        <div class="col-md-7  col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <cc1:DataEntryBox CssClass="form-control" ID="OtherChkIssuerName" runat="server" TabIndex="16"
                                                                                DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Joint Check Agreement:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <div class="checkbox-custom checkbox-default">
                                                                                <asp:CheckBox CssClass="checkbox" ID="JointCheckAgreement" runat="server" TabIndex="17" Text=" "></asp:CheckBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4  col-sm-4 col-xs-4 control-label align-lable">Print Notary:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;padding-left:0">
                                                                          <%--  <a href="Images/ResourceImages/Liens/Notary(Jurat).pdf" target="_blank" title="Notary (Jurat)">Click Here</a>&nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="lblPrintCANote" runat="server">(Use only if waiver is signed in CA)</asp:Label>--%>
                                                                             <div class="checkbox-custom checkbox-default">
                                                                               <asp:CheckBox ID="NotarywithCA" CssClass="checkbox" Style="margin-left: 15px;" runat="server" Text=" Print CA Notary  (Use only if waiver is notarized in CA)"
                                                                                   OnClick="JavaScript:myNotary(1);" TabIndex="18"></asp:CheckBox>
                                                                       </div>
                                                                                  </div>
                                                                    </div>
                                                                    <div>
                                                                        <label class="col-md-4  col-sm-4 col-xs-4 control-label align-lable"></label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;padding-left:0">                                            
                                                                            <div class="checkbox-custom checkbox-default">
                                                                               <asp:CheckBox ID="NotarywithoutCA" CssClass="checkbox" Style="margin-left: 10px;" runat="server" Text="Print Notary  (Do not use if waiver is notarized in CA)"
                                                                                   OnClick="JavaScript:myNotary(2);" TabIndex="19"></asp:CheckBox>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">From Date:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <span style="display: flex;">
                                                                                <SiteControls:Calendar ID="StartDate" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                                                                    IsRequired="false" Value="" TabIndex="20" />
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Through Date:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <span style="display: flex;">
                                                                                <SiteControls:Calendar ID="ThroughDate" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                                                                    IsRequired="false" Value="" TabIndex="21"/>
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Signer:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <div class="btn-group" style="text-align: left;" align="left">
                                                                                <button type="button" class="btn btn-default dropdown-toggle  col-md-12" style="min-width: 200px;" align="left"
                                                                                    data-toggle="dropdown" aria-expanded="false" id="btnDropSigner" TabIndex="22">
                                                                                    --Select Signer--
                     
                                                                                    <span class="caret"></span>
                                                                                </button>
                                                                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownSigner" style="overflow-y: auto; height: 250px;">
                                                                                </ul>
                                                                            </div>
                                                                            <asp:HiddenField ID="hdnSignerSelectedValue" runat="server" Value="" />
                                                                            <cc1:ExtendedDropDown ID="SignerId" runat="server" Width="344px" CssClass="dropdown"
                                                                                BackColor="White" CaseSensitiveKeySort="False" ErrorMessage="" IsValid="True"
                                                                                Tag="" Value="" Style="display: none;">
                                                                            </cc1:ExtendedDropDown>
                                                                            <uc3:AddSigner ID="AddSigner1" runat="server"></uc3:AddSigner>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Additional Note:</label>
                                                                        <div class="col-md-7  col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <cc1:DataEntryBox CssClass="form-control" ID="WaiverNote" runat="server" TabIndex="23"
                                                                                DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">CA Previous Waiver Dates:</label>
                                                                        <div class="col-md-7  col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <cc1:DataEntryBox CssClass="form-control" ID="WaiverDates" TabIndex="24" runat="server" DataType="Any"></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">CA Unpaid Progress Pmts:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <cc1:DataEntryBox CssClass="form-control" ID="WaiverPayments" TabIndex="25" runat="server" DataType="Any"></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">NV Invoice Payment App#:</label>
                                                                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                                            <cc1:DataEntryBox CssClass="form-control" ID="InvoicePaymentNo" runat="server" TabIndex="26"
                                                                                DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                                                                <div class="form-horizontal">

                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Job#:</label>
                                                                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                                                            <cc1:DataEntryBox ID="JobNum" runat="server" CssClass="form-control" DataType="Any" TabIndex="27"
                                                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Name:</label>
                                                                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                                                            <cc1:DataEntryBox ID="JobName" runat="server" CssClass="form-control" DataType="Any" TabIndex="28"
                                                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Address:</label>
                                                                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                                                            <cc1:DataEntryBox ID="JobAddr1" runat="server" CssClass="form-control" DataType="Any" TabIndex="29"
                                                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable"></label>
                                                                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                                                            <cc1:DataEntryBox ID="JobAddr2" runat="server" CssClass="form-control" DataType="Any" TabIndex="30"
                                                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">City:</label>
                                                                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                                                            <cc1:DataEntryBox ID="JobCity" runat="server" CssClass="form-control" DataType="Any" TabIndex="31"
                                                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">State/Zip:</label>
                                                                        <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left; display: flex;">
                                                                            <cc1:DataEntryBox ID="JobState" runat="server" CssClass="form-control" DataType="Any" Width="50px"
                                                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString="" TabIndex="32"
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                                                             <cc1:DataEntryBox ID="JobZip" runat="server" CssClass="form-control" DataType="Any"
                                                                                 ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString="" TabIndex="33"
                                                                                 FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" Width="125px"></cc1:DataEntryBox>

                                                                        </div>
                                                                        <%--   <div class="col-md-2" style="text-align: left;">
                                                                           
                                                                        </div>--%>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                            <div class="tab-pane active" id="exampleTabsThree" role="tabpanel">
                                                                <div class="form-horizontal">
                                                                    <div class="form-group">
                                                                        <asp:HiddenField ID="hdncustEmail" runat="server"  />
                                                                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Name:</label>
                                                                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                                                            <cc1:DataEntryBox ID="CustName" runat="server" CssClass="form-control" DataType="Any" TabIndex="34"
                                                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>

                                    </div>
                                    <div class="col-md-2" style="text-align: left;">
                                        <uc1:CustNameSearch ID="CustNameSearch1" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Ref#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="CustRef" runat="server" CssClass="form-control" DataType="Any" TabIndex="35"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Address:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="CustAdd1" runat="server" CssClass="form-control" DataType="Any" TabIndex="36"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">City:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="CustCity" runat="server" CssClass="form-control" DataType="Any" TabIndex="37"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left; display: flex;">
                                        <cc1:DataEntryBox ID="CustState" runat="server" CssClass="form-control" DataType="Any" TabIndex="38"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString="" Width="50px"
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                                                             <cc1:DataEntryBox ID="CustZip" runat="server" CssClass="form-control" DataType="Any" TabIndex="39"
                                                                                 ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                 FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" Width="125px"></cc1:DataEntryBox>

                                    </div>
                                    <%-- <div class="col-md-3" style="text-align: left;">
                                                                           
                                                                        </div>--%>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane active" id="exampleTabsFour" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Name:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="GCName" runat="server" CssClass="form-control" DataType="Any" TabIndex="40"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                    <div class="col-md-2" style="text-align: left;">
                                        <uc2:GCSearch
                                            ID="GCSearch1" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Ref#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="GCRef" runat="server" CssClass="form-control" DataType="Any" TabIndex="41"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Address:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="GCAdd1" runat="server" CssClass="form-control" DataType="Any" TabIndex="42"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">City:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="GCCity" runat="server" CssClass="form-control" DataType="Any" TabIndex="43"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left; display: flex;">
                                        <cc1:DataEntryBox ID="GCState" runat="server" CssClass="form-control" DataType="Any" Width="50px" TabIndex="44"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                                                             <cc1:DataEntryBox ID="GCZip" runat="server" CssClass="form-control" DataType="Any" TabIndex="45"
                                                                                 ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                 FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" Width="125px"></cc1:DataEntryBox>

                                    </div>
                                    <%-- <div class="col-md-3" style="text-align: left;">
                                                                           
                                                                        </div>--%>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane active" id="exampleTabsFive" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Name:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="OwnrName" runat="server" CssClass="form-control" DataType="Any" TabIndex="46"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Address:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="OwnrAdd1" runat="server" CssClass="form-control" DataType="Any" TabIndex="47"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">City:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="OwnrCity" runat="server" CssClass="form-control" DataType="Any" TabIndex="48"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left; display: flex;">
                                        <cc1:DataEntryBox ID="OwnrState" runat="server" CssClass="form-control" DataType="Any" Width="50px" TabIndex="49"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                                                             <cc1:DataEntryBox ID="OwnrZip" runat="server" CssClass="form-control" DataType="Any" TabIndex="50"
                                                                                 ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                 FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" Width="125px"></cc1:DataEntryBox>

                                    </div>
                                    <%-- <div class="col-md-3" style="text-align: left;">
                                                                           
                                                                        </div>--%>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane active" id="exampleTabsSix" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Name:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="LenderName" runat="server" CssClass="form-control" TabIndex="51"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Address:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="LenderAdd1" runat="server" CssClass="form-control" TabIndex="52"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">City:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="text-align: left;">
                                        <cc1:DataEntryBox ID="LenderCity" runat="server" CssClass="form-control" TabIndex="53"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left; display: flex;">
                                        <cc1:DataEntryBox ID="LenderState" runat="server" CssClass="form-control" Width="50px" TabIndex="54"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                                                             <cc1:DataEntryBox ID="LenderZip" runat="server" CssClass="form-control" DataType="Any" TabIndex="55"
                                                                                 ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                 FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" Width="125px"></cc1:DataEntryBox>

                                    </div>
                                    <%--<div class="col-md-3" style="text-align: left;">
                                                                           
                                                                        </div>--%>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <%--  <ajaxToolkit:TabContainer ID="TabContainer1" runat="server" CssClass="tabs" ActiveTabIndex="0">
                                                        <ajaxToolkit:TabPanel ID="TabPanel6" runat="server" HeaderText="Waiver Info">
                                                            <ContentTemplate>
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Job Info">
                                                            <ContentTemplate>
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" HeaderText="Customer Info">
                                                            <ContentTemplate>
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel3" runat="server" HeaderText="General Contractor">
                                                            <ContentTemplate>
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" HeaderText="Owner">
                                                            <ContentTemplate>
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel5" runat="server" HeaderText="Lender">
                                                            <ContentTemplate>
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                    </ajaxToolkit:TabContainer>--%>
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
                                                </div>
                                                <div class="footer">
                                                    <asp:UpdatePanel ID="updPan1" runat="server">
                                                        <ContentTemplate>
                                                            <asp:LinkButton ID="btnSubmit" CssClass="btn btn-primary" runat="server" Text="Submit Waiver"
                                                                CausesValidation="False" />&nbsp;
                        <br />
                                                            <br />
                                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                                <ProgressTemplate>
                                                                    <div class="TransparentGrayBackground">
                                                                    </div>
                                                                    <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                                                        <div class="PageUpdateProgress">
                                                                            <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                                                                                AlternateText="[image]" />
                                                                            &nbsp;Please Wait...
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                                                        TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
                                                                        VerticalSide="Middle" VerticalOffset="0">
                                                                    </ajaxToolkit:AlwaysVisibleControlExtender>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="View2" runat="server">
                                                <div class="body">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <div class="col-md-3" style="text-align: left;">
                                                                <asp:Button ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Waiver Form"></asp:Button>
                                                            </div>
                                                            <div class="col-md-2" style="text-align: left;">
                                                                <asp:Button ID="btnDownLoad" Text="DownLoad" runat="server" CssClass="btn btn-primary" OnClick="btnDownLoad_Click" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12">
                                                                <div style="width: auto; height: auto; overflow: auto;">
                                                                    <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>
                                            </asp:View>
                                            <asp:View ID="View3" runat="server">
                                                <div class="body">
                                                    <div class="form-horizontal">
                                                        <br />
                                                        <div class="form-group">
                                                            <div class="col-md-12" style="text-align:left;">
                                                                <asp:Button ID="btnAddWaiver" runat="server" Text="Add New Waiver" CssClass="btn btn-primary"></asp:Button>
                                                        
                                                                 </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="col-md-12" style="text-align: center;" align="center">
                                                                <asp:UpdatePanel ID="updgvwWaivers" runat="server" UpdateMode="Conditional">
                                                                    <ContentTemplate>
                                                                        <div style="width: auto; height: auto; overflow: auto;">
                                                                            <asp:GridView ID="gvwWaivers" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                                CssClass="table dataTable table-striped" style="white-space:nowrap">

                                                                                <%-- <RowStyle CssClass="rowstyle" /> AllowSorting="true" OnSorted="gvwWaivers_Sorted" OnSorting="gvwWaivers_Sorting"
                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                                                <Columns>
                                                                                    <asp:TemplateField HeaderText="Edit / Print / Email / Copy">
                                                                                        <ItemTemplate>
                                                                                            &nbsp;
                                                                                              <asp:LinkButton ID="btnEditItem" CssClass="icon ti-write" runat="server" 
                                                                                                  CommandArgument='<%# Eval("BatchNoJobWaiverId") %>' CommandName="editwaiver"/>
                                       <%-- <asp:ImageButton ID="btnEditWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif"
                                            CommandArgument='<%# Eval("BatchNoJobWaiverId") %>' CommandName="editwaiver"
                                            runat="server" />--%>
                                                                                            &nbsp; &nbsp;
                                                                                            <asp:LinkButton ID="btnPrntWaiver" CssClass="icon ti-printer" runat="server" 
                                                                                                 CommandArgument='<%# Eval("BatchNoJobWaiverId")%>' CommandName="printwaiver"/>
                                       <%-- <asp:ImageButton ID="btnPrntWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png"
                                            CommandArgument='<%# Eval("BatchNoJobWaiverId")%>' CommandName="printwaiver"
                                            runat="server" />--%>
                                                                                            &nbsp;&nbsp; 
                                                                                             <asp:LinkButton ID="btnEmailWaiver" CssClass="icon ti-email" runat="server"
                                                                                                  CommandArgument='<%# Eval("BatchNoJobWaiverId")%>' CommandName="emailwaiver" />
                                                                                            &nbsp;&nbsp;
                                                                                            <asp:LinkButton ID="btnCopyItem" CssClass="icon ti-layers" runat="server"
                                                                                                CommandArgument='<%# Eval("BatchNoJobWaiverId") %>' CommandName="copywaiver" />
                                                                                        </ItemTemplate>
                                                                                        <ItemStyle Width="12%" Wrap="False" />
                                                                                    </asp:TemplateField>
                                                                                     <asp:TemplateField HeaderText="Job# / Job Name">
                                                                                     <ItemTemplate>
                                                                                        
                                                                                            <asp:Label ID="edtName" runat="server" Text='<%# Eval("JobNum") %>'  /><span>/</span>
                                                                                        
                                                                                          <asp:Label ID="edtName1" runat="server" Text='<%# Eval("JobName") %>' />
                                                                                        </ItemTemplate>
                                                                                         <ItemStyle  Height="15px" Wrap="False" />
                                                                                  

                                                                                          </asp:TemplateField>
                                                                                    <asp:BoundField DataField="BatchNoJobWaiverId"
                                                                                        HeaderText="BatchNoJobWaiverId" Visible="false">
                                                                                        <ItemStyle  Height="15px" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                   
                                                                                      <asp:BoundField DataField="CustName" HeaderText="Customer">
                                                                                        <ItemStyle Width="20%"  Height="15px" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="DateCreated" HeaderText="Created On">
                                                                                        <ItemStyle HorizontalAlign="Center"  Height="15px" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="DatePrinted" HeaderText="Printed On" ControlStyle-Font-Bold="false">
                                                                                        <ItemStyle HorizontalAlign="Center"  Height="15px" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="FormCode" HeaderText="Waiver Type">
                                                                                        <ItemStyle Width="20%" Height="15px" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                    <asp:BoundField DataField="PaymentAmt" HeaderText="Waiver Balance">
                                                                                        <ItemStyle HorizontalAlign="left"  Height="15px" Wrap="False" />
                                                                                    </asp:BoundField>
                                                                                    <asp:TemplateField HeaderText=" Delete ">
                                                                                        <ItemTemplate>
                                                                                             <asp:LinkButton ID="btnDeleteWaiver" CssClass="icon ti-trash" runat="server"
                                                                                                   
                                                                                                  OnClientClick="return DeleteAccount(this)"/>
                                                                                         <%--   CommandName="deletewaiver"  CommandArgument='<%# Eval("BatchNoJobWaiverId") %>'--%>
                                                                                        <%--    <asp:ImageButton ID="btnDeleteWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif"
                                                                                                CommandName="deletewaiver" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')"
                                                                                                CommandArgument='<%# Eval("BatchNoJobWaiverId") %>' runat="server" />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle Wrap="False" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <b>No Items found for the specified criteria</b>
                                            </EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

        </asp:View>
    </asp:MultiView>
    <div class="form-horizontal">
        <div class="form-group">
            <div class="col-md-12" style="text-align: left;">
                <asp:Label ID="lblmsg" runat="server"></asp:Label>
                <asp:HiddenField ID="hid1" runat="server" />
                <asp:HiddenField ID="hdnId" runat="server" Value="0" />
              <asp:HiddenField runat="server" ID="hdnflag" />
                <asp:Button ID="btnDeleteId" runat="server" Style="display: none;" OnClick="btnDeleteId_Click" />
            </div>
        </div>
    </div>
    <div class="modal fade modal-primary" id="ModalCustSearch" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">�</span>
                    </button>
                    <h4 class="modal-title">CUSTOMER SEARCH</h4>
                    <asp:HiddenField runat="server" ID="hdnCustSearchClientID" Value="0" />
                </div>
                <div class="modal-body">
                    <div style="display: flex;">
                        <div class="radio-custom radio-default radio-inline">
                            <input id="rbtnByName" type="radio" name="SearchName" checked="checked">
                            <label for="inputHorizontalMale">By Name</label>
                        </div>
                        <div class="radio-custom radio-default radio-inline">
                            <input id="rbtnByRef" type="radio" name="SearchName">
                            <label for="inputHorizontalMale">By Ref</label>
                        </div>
                        &nbsp;&nbsp;<input type="text" id="txtSearchStr" class="form-control" style="width: 40%;" onkeyup="SearchCustName()" />
                        &nbsp;&nbsp;<input type="button" value="Search" onclick="SearchCustName(); return false;" class="btn btn-primary" />
                    </div>
                    <div style="margin-top: 10px;">
                        <select size="4" id="listCustName" onchange="GetCustomer();" class="form-control"></select>
                        <%--<asp:ListBox runat="server" id="listCustName" Rows="4" style="width:100%;" ></asp:ListBox>--%>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade  modal-primary" id="ModalGCSearch" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">�</span>
                    </button>
                    <asp:HiddenField runat="server" ID="hdnGCSearchClientID" />
                    <h4 class="modal-title">GENERAL CONTRACTOR SEARCH</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-3 control-label align-lable">Enter GC Name: </label>
                            <div class="col-md-5">

                                <input type="text" id="txtGCSearchText" class="form-control" onkeyup="GetGCNameList()" />

                            </div>
                            <div class="col-md-2">
                                <button id="btnGCSearch" onclick="GetGCNameList();return false;" class="btn btn-primary" value="Search">Search</button>
                            </div>
                        </div>
                        <div class="form-group">
                            <select size="4" id="listGCNames" style="display: none;" onchange="GetGCDetails();" class="form-control"></select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                    <%-- <a class="btn btn-default margin-0" data-dismiss="modal">&times;</a>--%>
                </div>
            </div>
        </div>
    </div>
</div>

<%--//'^^^***CreatedBy Pooja   02/05/21 *** ^^^--%>
<div class="modal fade modal-primary" id="divModalEmail" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose" onclick="CloseEmailLoadPage();">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title">Send Email</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4">To:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtToList" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <label style="color: red; text-align: left;">Note:- Multiple email ids can be added separated by semicolon</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Subject:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" Text="New Waiver Document"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Message:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtBody" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <asp:Label ID="lblErrorMsg" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--<asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Send" Style="display: none;" />--%>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaves" onclick="SendMail();">
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="return CloseEmailLoadPage();">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnEmailsend" runat="server" OnClick="btnEmailsend_Click" Style="display: none;" />
<div class="modal fade modal-primary example-modal-lg" id="ModalWaiverSubmit" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseEmailLoadPage();">
                    <span aria-hidden="true">�</span>

                </button>


                <asp:Button ID="hdnbtnclose" CssClass="btn btn-primary" runat="server" Text="" Style="display: none;" OnClick="hdnbtnclose_Click" />
                <h3 class="modal-title">Waiver Created !!</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">

                    <h3 class="modal-title">Your Waiver Has Been Successfully Created </h3>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">
                <asp:Button ID="btnNew" runat="server" Text="New Waiver" CssClass="btn btn-primary" OnClick="btnNew_Click" />
                <asp:Button ID="btnEmail" runat="server" Text="Email Waiver" CssClass="btn btn-primary" OnClick="btnEmailWaiver_Click" />
                <asp:Button ID="btnPrint" runat="server" Text="Print Waiver" CssClass="btn btn-primary" OnClick="btnPrintWaiver_Click" />

            </div>
        </div>
    </div>
</div>
                               
