<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Default.aspx.vb" Inherits="App_Controls__Custom_LienView_Default" %>

<%--<%@ Register Src="FileUpload.ascx" TagName="uploadFile" TagPrefix="uc7" %>--%>
<%@ Register Src="FileUploadTest.ascx" TagName="uploadFile" TagPrefix="uc7" %>
<%@ Register Src="~/App_Controls/_Custom/CollectView/debtaccountfileupload.ascx" TagName="debtaccountfileupload" TagPrefix="uc8" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <meta name="generator" content="hds vbjuice" />
    <meta id="metaKeywords" name="Keywords" content="" />
    <meta id="metaDescr" name="Description" content="" />
    <meta id="metaCopyRight" name="copyright" content="" />
    <link rel="apple-touch-icon" href="../../../assets/images/apple-touch-icon.png" />
    <link rel="shortcut icon" href="../../../assets/images/favicon.ico" />

    <!-- Stylesheets -->
    <link rel="stylesheet" href="../../../global/css/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/css/bootstrap-extend.min.css" type="text/css" />
    <link rel="stylesheet" href="../../../assets/css/site.min.css" type="text/css" />
    <!-- Plugins -->
    <link rel="stylesheet" href="../../../global/vendor/animsition/animsition.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/asscrollable/asScrollable.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/switchery/switchery.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/intro-js/introjs.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/slidepanel/slidePanel.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/flag-icon-css/flag-icon.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/vendor/blueimp-file-upload/jquery.fileupload.css" />
    <link rel="stylesheet" href="../../../global/vendor/dropify/dropify.css" />
    <!-- Fonts -->
    <link rel="stylesheet" href="../../../global/fonts/web-icons/web-icons.min.css" type="text/css" />
    <link rel="stylesheet" href="../../../global/fonts/brand-icons/brand-icons.min.css" type="text/css" />
    <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic' />
       <link href="../../../upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="../../../upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" />
   <%-- <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js">--%>
    <!--[if lt IE 9]>
    <script src="../../../global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="../../../global/vendor/media-match/media.match.min.js"></script>
    <script src="../../../global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->


    <script src="../../../global/vendor/modernizr/modernizr.js" type="text/javascript"></script>
    <script src="../../../global/vendor/breakpoints/breakpoints.js" type="text/javascript"></script>
   
    <style type="text/css">
       .has-file .file-wrap {
    padding: 0px;
    background: #fff;
    border-radius: 3px;
}
        .file-wrap .file-action {
    position: absolute;
    top: 12px;
    left: 0;
    width: 100%;
    text-align: center;
}
        .file-wrap .preview {
    position: relative;
    width: 100%;
    height: 50px;
    margin-bottom: 10px;
    overflow: hidden;
    background-color: black;
    border-radius: 4px;
    opacity: .6;
    -webkit-transition: all .3s;
    -o-transition: all .3s;
    transition: all .3s;
}
        .dropify-wrapper {
            position: relative;
            display: block;
            width: 100%;
            max-width: 100%;
            height: 40px !important;
            font-family: Roboto, sans-serif;
            font-size: 14px;
            line-height: 22px;
            color: rgb(118, 131, 143);
            text-align: center;
            cursor: pointer;
            background-color: rgb(255, 255, 255);
            background-image: none;
            border-image-source: initial;
            border-image-slice: initial;
            border-image-width: initial;
            border-image-outset: initial;
            border-image-repeat: initial;
            padding: 5px 10px;
            overflow: hidden;
            border-width: 2px;
            border-style: solid;
            border-color: rgb(228, 234, 236);
        }
    </style>
   
</head>
<body style="padding-top: 10px;">

    <form id="myform" enctype="multipart/form-data" runat="server" method="post" autocomplete="on" style="width: 97%; height: 100%;">
       
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <div>
            <uc7:uploadFile ID="UploadFile7" runat="server" />
        </div>
        <div>
            <uc8:debtaccountfileupload ID="uploadfile8" runat="server" />
        </div>
         <div id="loadingImageDiv"   >
        </div>
    </form>
    <!-- Core  -->
    <script src="../../../global/vendor/jquery/jquery.js" type="text/javascript"></script>
    <script src="../../../global/vendor/bootstrap/bootstrap.js" type="text/javascript"></script>
    <script src="../../../global/vendor/animsition/animsition.js" type="text/javascript"></script>
    <script src="../../../global/vendor/asscroll/jquery-asScroll.js" type="text/javascript"></script>
    <script src="../../../global/vendor/mousewheel/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="../../../global/vendor/asscrollable/jquery.asScrollable.all.js" type="text/javascript"></script>
    <script src="../../../global/vendor/ashoverscroll/jquery-asHoverScroll.js" type="text/javascript"></script>
    <!-- Plugins -->
    <script src="../../../global/vendor/switchery/switchery.min.js" type="text/javascript"></script>
    <script src="../../../global/vendor/intro-js/intro.js" type="text/javascript"></script>
    <script src="../../../global/vendor/screenfull/screenfull.js" type="text/javascript"></script>
    <script src="../../../global/vendor/slidepanel/jquery-slidePanel.js" type="text/javascript"></script>
    <script src="../../../global/vendor/jquery-ui/jquery-ui.js"></script>
    <script src="../../../global/vendor/blueimp-tmpl/tmpl.js"></script>
    <script src="../../../global/vendor/blueimp-canvas-to-blob/canvas-to-blob.js"></script>
    <script src="../../../global/vendor/blueimp-load-image/load-image.all.min.js"></script>
    <script src="../../../global/vendor/blueimp-file-upload/jquery.fileupload.js"></script>
    <script src="../../../global/vendor/blueimp-file-upload/jquery.fileupload-process.js"></script>
    <script src="../../../global/vendor/blueimp-file-upload/jquery.fileupload-image.js"></script>
    <script src="../../../global/vendor/blueimp-file-upload/jquery.fileupload-audio.js"></script>
    <script src="../../../global/vendor/blueimp-file-upload/jquery.fileupload-video.js"></script>
    <script src="../../../global/vendor/blueimp-file-upload/jquery.fileupload-validate.js"></script>
    <script src="../../../global/vendor/blueimp-file-upload/jquery.fileupload-ui.js"></script>
    <script src="../../../global/vendor/dropify/dropify.min.js"></script>
    <!-- Scripts -->
    <script src="../../../global/js/core.js"></script>
    <script src="../../../assets/js/site.js"></script>
    <script src="../../../assets/js/sections/menu.js"></script>
    <script src="../../../assets/js/sections/menubar.js"></script>
    <script src="../../../assets/js/sections/sidebar.js"></script>
    <script src="../../../global/js/configs/config-colors.js"></script>
    <script src="../../../assets/js/configs/config-tour.js"></script>
    <script src="../../../global/js/components/asscrollable.js"></script>
    <script src="../../../global/js/components/animsition.js"></script>
    <script src="../../../global/js/components/slidepanel.js"></script>
    <script src="../../../global/js/components/switchery.js"></script>
    <script src="../../../global/js/components/dropify.js"></script>
    <script src="../../../assets/examples/js/forms/uploads.js"></script>


</body>
      <script language="javascript" type="text/javascript">
          function BindFileUoload() {             
              parent.BindFileUoloadList();
          }
          Breakpoints();
          function MaintainMenuOpen()
          { }
          function EmptyFileList() {
              //These below to two line added for preventing the multiple file upload, if you want to upload multiple file.then modify the method            
              //$(".file-list").empty();
              //DeletFileUpload();
              //end here
          }
          function DeletFileUpload() {
              debugger;
              //alert("The button clicked.");
              //$('#UploadFile7_FileUpLoad1').val('');
              //$('#UploadFile7_Txtmiscinfo').val('');
              //$("#loadingImageDiv").hide();
              //setSelectedDocTypeId('--SELECT--');
              //$('#UploadFile7_lblmsg').html('');
            
             
              $.ajax({
                  type: "POST",
                  url: "Default.aspx/DeletFileUploads",
                  contentType: "application/json; charset=utf-8",
                  async: false,
                  dataType: "json",
                  //data: '{RowIndx: "' + Index + '" }',
                //  data: '{filename: "' + filename + '" }',
                  success: function (response) {
                      //window.open("../Pages/NewCharge.aspx", "_self");
                  },
                  failure: function (response) {
                      alert(response.d);
                  }
              });

              return false;
          }
          function setLoadingImage() {
              //debugger;
              //alert('setLoadingImage');
              var ht = screen.height + 50;
              ht = 540;
              var wt = screen.width + 50;
              wt = 550;
              var imgmarTop = (ht / 2) - 100;

              var imgmarLeft = (wt / 2) - 100;

              var lyrStl = "width: " + wt + "px; height:" + ht + "px; background-color: rgba(255, 255, 255, 0); position: fixed; z-index: 10051 !important; margin: 0px 0px 0px 0px;top:0px;  ";

              var img = '<img src="../../../images/LoadingImage.gif" style=" margin-left:' + imgmarLeft + 'px; margin-top:' + imgmarTop + 'px;  z-index: 10052 !important;">';
              //var img = '<img src="../../../images/LoadingImage.gif" style="z-index: 10052 !important;">';

              var div = '<div style="' + lyrStl + '">';

              div = div + img;

              div = div + '<div>';

              $('#loadingImageDiv').html(div);

              $('#loadingImageDiv').hide();

          }
          function showLodingImage() {
             // alert('showLodingImage');
             // debugger;            
              $('#loadingImageDiv').show();
          } // 5 seconds }

          function hideLodingImage() {
             // alert('hideLodingImage');
             // debugger;             
              var tmr = setInterval(function () { zzzhideLodingImageActul(tmr) }, 1000);              
              //window.opener.testjava();
             
          }
        
          function zzzhideLodingImageActul(tmr) { $('#loadingImageDiv').hide(); clearInterval(tmr); }
          //$('.dropify').dropify(function () {
          //    alert('hi');
          //});

  </script>
     <script language="javascript" type="text/javascript">
         function SetDatePicker() {
             $('.datepicker').datepicker({
                 format: "mm/dd/yyyy",
                 changeMonth: true,
                 changeYear: true,
                 autoclose: true,
                 showButtonPanel: true,
             });
         }

         $(document).ready(function () {
             SetDatePicker();
             setLoadingImage();
             $('body').on('keydown', 'input, select, textarea', function (e) {
                 // alert('hisds');
                 var self = $(this)
                   , form = self.parents('form:eq(0)')
                   , focusable
                   , next
                 ;
                 if (e.keyCode == 13) {
                     //alert('sdsdsd');
                     focusable = form.find('input,a,select,button,textarea').filter(':visible');
                     next = focusable.eq(focusable.index(this) + 1);
                     if (next.length) {
                         next.focus();
                     } else {
                         form.submit();
                     }
                     return false;
                 }
             });
         });

    </script>
  
    <script src="../../../upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
    <script src="../../../upload/upload/global/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
</html>
