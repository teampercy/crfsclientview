<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobSearch.ascx.vb" Inherits="App_Controls__Custom_LienView_JobSearch" %>
<script>
    function MaintainMenuOpen() {

        SetHeaderBreadCrumb('LienView', 'Jobs', 'Job Search');
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubPlaceJobs');


    }


    function CallSuccessFunc() {
        
        var Columns = [
                 { "bSortable": false, "searchable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
        ];

        MakeDataTable('#<%=gvwList.ClientID%>', Columns, null, [[1, "asc"]]);
        
    }
    function BindClientSelection() {
        $('#UlClientSelectionType li').remove();
        var x = document.getElementById('<%=DropDownList1.ClientID%>');
        var txt = "";
        var i;
        for (i = 0; i < x.length; i++) {
            var ResultReplace = ReplaceSpecChar(x.options[i].text);
            $('#UlClientSelectionType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClientSel_' + x.options[i].value + '" onclick="setSelectedClientId(' + x.options[i].value + ',\'' + ResultReplace + '\')">' + x.options[i].text + '</a></li>');
        }
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val() != "") {
            $('#btnClientSelectionType').html($('#liClientSel_' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedClientId(id, TextData) {
        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnClientSelectionId.ClientID%>').val(id);
        $('#btnClientSelectionType').html(ResultReplace + "<span class='caret' ></span>");
           if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val().length > 0)
        {
            <%-- alert('hii ' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val() );--%>
            var value = "SELECTED";
            var radio = $("[id*=ctl33_ctl00_RadioButtonList1] input[value=" + value + "]");
            radio.attr("checked", "checked");

        }
        return false;
    }
    function ReplaceSpecChar(Textdata) {
        //alert(Textdata);
        var result = Textdata;
        var ResultReplace = result.replace("'", "`");
        //alert("ResultReplace" + ResultReplace);
        return ResultReplace;
    }
</script>


<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="width: 100%;" class="margin-bottom-0">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Copy Job Filter</h1>
                    <div class="body">
                        <div class="form-horizontal">
                            <asp:Panel ID="panClients" runat="server">

                                <div class="form-group">
                                    <label class="col-md-3 control-label">Client Selection:    </label>
                                    <div class="col-md-9" style="text-align: left;;padding-left:7px;">
                                        <asp:DropDownList ID="DropDownList1" runat="server" style="display:none;">
                                        </asp:DropDownList>&nbsp;
                                    <%--    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1" Width="224px">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL">All Clients</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED">Selected Only</asp:ListItem>
                                        </asp:RadioButtonList>--%>

                                         <asp:HiddenField ID="hdnClientSelectionId" runat="server" />
                                <div class="btn-group" style="text-align: left;" align="left">
                                    <button type="button" class="btn btn-default dropdown-toggle" style="width: 300px; text-align: left;" align="left"
                                        data-toggle="dropdown" aria-expanded="false" id="btnClientSelectionType" tabindex="1">
                                        --Select Client Selection--
                     
                                                                                    <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlClientSelectionType" style="overflow-y: auto; height: 150px;">
                                    </ul>
                                </div>                        

                                  <div class="radio-custom radio-default radio-inline">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                        <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL" style="white-space:nowrap;">All Clients</asp:ListItem>
                                        <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED" style="white-space:nowrap;padding-left:40px;">Selected Only</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>
                                    </div>
                                </div>

                            </asp:Panel>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CustomerName" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="CustomerNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer Ref:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CustomerRef" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="CustomerRefLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>



                            <div class="form-group">
                                <label class="col-md-3 control-label">Owner Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="OwnerName" CssClass="form-control" meta:resourcekey="OwnerNameResource1" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="OwnerNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Contractor Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="GCName" CssClass="form-control" meta:resourcekey="GCNameResource1" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="GCNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Lender Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="LenderName" CssClass="form-control" meta:resourcekey="LenderNameResource1" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="LenderNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Job Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobName" CssClass="form-control" meta:resourcekey="JobNameResource1" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="JobNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Job Address:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobAddress" CssClass="form-control" meta:resourcekey="JobAddressResource1" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="JobAddressLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Job #:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobNo" CssClass="form-control" meta:resourcekey="JobNoResource1" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButtonList ID="JobNoLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerRefLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1">&nbsp;Starts&nbsp;</asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" style="padding-left: 30px">&nbsp;Includes</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Assigned Date: </label>
                                <div class="col-md-5" style="display: flex;">
                                    <SiteControls:Calendar ID="Calendar1" runat="server" ErrorMessage="From Date"
                                        IsReadOnly="false" IsRequired="true" Value="TODAY" />
                                    &nbsp;
                                        Thru&nbsp;<SiteControls:Calendar ID="Calendar2" runat="server"
                                            ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" Value="TODAY" />
                                    &nbsp;&nbsp;
                                    
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByAssignDate" CssClass="redcaption" runat="server" Text=" Filter On Assign Date Range" />
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Job City:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobCity" CssClass="form-control" MaxLength="20" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;"></div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 control-label">Job State:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="JobState" Width="70px" CssClass="form-control" Style="text-align: center" meta:resourcekey="JobStateResource1" MaxLength="2" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Branch #:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="BranchNo" CssClass="form-control" meta:resourcekey="BranchNoResource1" MaxLength="10" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Status:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" Width="70px" ID="JobStatus" CssClass="form-control" meta:resourcekey="BranchNoResource1" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkPaidInFull" CssClass="redcaption" runat="server" Text="  Exclude Paid In Full Jobs" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">CRF File #:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="FileNumber" CssClass="form-control" meta:resourcekey="FileNumberResource1" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;"></div>
                            </div>

                        </div>
                    </div>
                    <div class="footer">
                        <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">
                            <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="btn btn-primary" CausesValidation="False" />&nbsp;
    <asp:Button ID="btnViewAll" runat="server" Text="Back to List" CssClass="btn btn-primary" CausesValidation="False" />
                            <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>&nbsp;
                        </asp:Panel>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <h1 class="panelheader">Job List</h1>
                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-12" style="text-align: left;">
                                    <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter" OnClick="btnBackFilter_Click" CausesValidation="False"></asp:LinkButton>
                                </div>
                            </div>
                        </div>


                        <div id="expPanel" style="color: black !important; padding: 15px 5px;" data-loader-type="circle">
                            <div style="width: auto; height: auto; overflow: auto;">
                                <asp:GridView ID="gvwList" runat="server" CssClass="table dataTable table-striped" AutoGenerateColumns="False" DataKeyNames="JobId"
                                    Width="100%">
                                    <%--<RowStyle CssClass="rowstyle" />
                                    <AlternatingRowStyle CssClass="altrowstyle" />
                                    <HeaderStyle CssClass="headerstyle" />--%>
                                    <Columns>
                                        <asp:TemplateField HeaderText="View">
                                            <ItemTemplate>
                                                <%--<asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />--%>
                                                <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="30px" Wrap="False" />
                                        </asp:TemplateField>

                                        <asp:BoundField DataField="JobId" SortExpression="JobId" HeaderText="CRFS#">
                                            <ItemStyle Width="50px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="JobName" SortExpression="JobName" HeaderText="Job Name">
                                            <ItemStyle Width="145px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="JobNum" SortExpression="JobNum" HeaderText="Job#">
                                            <ItemStyle Width="50px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CityStateZip" SortExpression="CityStateZip" HeaderText="Job Add">
                                            <ItemStyle Width="145px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BranchNum" SortExpression="BranchNum" HeaderText="Branch#">
                                            <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="StatusCode" SortExpression="StatusCode" HeaderText="Stat">
                                            <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ClientCustomer" SortExpression="ClientCustomer" HeaderText="Customer">
                                            <ItemStyle Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DateAssigned" SortExpression="DateAssigned" HeaderText="Assigned">
                                            <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client#">
                                            <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                        </asp:BoundField>

                                    </Columns>

                                    <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                </asp:GridView>
                            </div>
                        </div>



                    </div>
                   
                    

                </asp:View>
            </asp:MultiView>
        </div>
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage"
                            runat="server"
                            ImageUrl="~/images/ajax-loader.gif"
                            AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender
                    ID="AlwaysVisibleControlExtender1"
                    runat="server"
                    TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center"
                    HorizontalOffset="150"
                    VerticalSide="Middle"
                    VerticalOffset="0">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
