﻿Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Imports System.Configuration


Partial Class App_Controls__Custom_LienView_FileUploadTest
    'Inherits System.Web.UI.UserControl
    Inherits HDS.WEBSITE.UI.BaseUserControl

    'Protected Sub UploadBtn_Click(sender As Object, e As EventArgs) Handles UploadBtn.Click
    '    'Protected void UploadBtn_Click(Object sender, EventArgs e)  



    '    If FileUpLoad1.HasFile Then


    '        'FileUpLoad1.SaveAs(@"C:\temp\" 
    'Protected Sub UploadBtn_Click(sender As Object, e As EventArgs) Handles UploadBtn.Click
    '    'Protected void UploadBtn_Click(Object sender, EventArgs e)  



    '    If FileUpLoad1.HasFile Then


    '        'FileUpLoad1.SaveAs(@"C:\temp\" + FileUpLoad1.FileName)
    '        Label1.Text = "File Uploaded: " + FileUpLoad1.FileName

    '    Else

    '        Label1.Text = "No File Uploaded."

    '    End If
    'End Sub

    Protected Sub btnFileUpLoadOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadOk.Click

        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showLodingImage", "showLodingImage();", True)
            Dim str As String
            Dim jobid As String
            Dim batchjobid As String
            Dim usercode As String
            Dim fileName As String
            Dim userid As Integer
            Dim Todaydate As Date
            'markparimal-uploadfile:check whether document type is selected or not
            'If ddldoctype.SelectedIndex = 0 Then
            '    lblmsg.Text = " Select Document Type"
            '    Return
            'Else
            '    lblmsg.Text = ""
            'End If

            fileName = ""

            If hdnDocType.Value = "--Select--" Or hdnDocType.Value = "--SELECT--" Then
                lblmsg.Text = " Select Document Type"
                Return
            Else
                lblmsg.Text = ""
            End If
            'Comment by jaywanti on 10-09-2016
            'Dim UploadedFile As HttpPostedFile = Request.Files("fileDragnDrop")
            'str = UploadedFile.FileName
            'Dim fileData As Byte() = New Byte(UploadedFile.InputStream.Length - 1) {}
            'UploadedFile.InputStream.Read(fileData, 0, fileData.Length)
            'fileName = System.IO.Path.GetFileName(UploadedFile.FileName)


            If Context.Request.Files.Count > 0 Then
                Dim files As HttpFileCollection = Context.Request.Files
                For i As Integer = 0 To files.Count - 1
                    Dim file As HttpPostedFile = files(i)
                    Dim fname As String
                    If HttpContext.Current.Request.Browser.Browser.ToUpper() = "IE" OrElse HttpContext.Current.Request.Browser.Browser.ToUpper() = "INTERNETEXPLORER" Then
                        Dim testfiles As String() = file.FileName.Split(New Char() {"\"c})
                        fname = testfiles(testfiles.Length - 1)
                    Else
                        fname = file.FileName
                    End If
                    Dim fileData As Byte() = New Byte(file.InputStream.Length - 1) {}
                    file.InputStream.Read(fileData, 0, fileData.Length)

                    Dim objuploadFile As New UploadFile
                    Dim uploadFiles As List(Of UploadFile) ' = New List(Of UploadFile)()
                    uploadFiles = Context.Session("UploadFiles")
                    If uploadFiles Is Nothing Then
                        uploadFiles = New List(Of UploadFile)()
                    End If
                    objuploadFile.UploadedFileName = fname
                    objuploadFile.UploadedFileDataBytes = fileData
                    objuploadFile.UploadedFileExtension = System.IO.Path.GetExtension(file.FileName)

                    If Not objuploadFile.UploadedFileDataBytes Is Nothing Then
                        fileData = objuploadFile.UploadedFileDataBytes
                    End If
                    'If Not objuploadFile.UploadedFileName Is Nothing Then
                    '    fileName = objuploadFile.UploadedFileName
                    'End If

                    If Not objuploadFile.UploadedFileName Is Nothing And Not String.IsNullOrEmpty(objuploadFile.UploadedFileName) Then
                        fileName = objuploadFile.UploadedFileName
                    Else
                        lblmsg.Text = "Please Select a File to Upload"
                        Return
                    End If

                    usercode = CurrentUser.UserName.ToString
                    userid = CurrentUser.Id

                    Todaydate = txtdate.Text
                    If Session("bitjobid") = 0 Then
                        jobid = 0
                        batchjobid = Session("BatchJobId")
                    Else
                        jobid = Session("jobid")
                        batchjobid = 0
                    End If

                    'CRFS.ClientView.CrfsDll.InsertJobAttachments(jobid, userid, usercode, Calendar2.Value, fileName, ddldoctype.SelectedValue, Txtmiscinfo.Text, fileData, batchjobid)
                    'Commented by Jaywanti 
                    'CRFS.ClientView.CrfsBll.InsertJobAttachments(jobid, userid, usercode, Todaydate, fileName, ddldoctype.SelectedValue, Txtmiscinfo.Text, fileData, batchjobid)
                    Dim DescriptionWithFileName = "[Filename: " + fileName + "]  " + Txtmiscinfo.Text
                    'CRFS.ClientView.CrfsBll.InsertJobAttachments(jobid, userid, usercode, Todaydate, fileName, hdnDocType.Value, Txtmiscinfo.Text, fileData, batchjobid)
                    CRFS.ClientView.CrfsBll.InsertJobAttachments(jobid, userid, usercode, Todaydate, fileName, hdnDocType.Value, DescriptionWithFileName, fileData, batchjobid)
                Next
            End If
            'HttpContext.Current.Session("UploadFiles") = Nothing
            If Session("bitjobid") = 0 Then

                lblmsg.Text = "File Uploaded For Temp Job#" + " " + batchjobid
            Else
                lblmsg.Text = "File Uploaded For Job#" + " " + jobid
            End If

            If Session("bitjobid") = "1" Then
                Dim myitem As New TABLES.Job
                ProviderBase.DAL.Read(jobid, myitem)
                Dim ssubject = "Document Sent on Lien# " & jobid
                Dim MYSQL As String = "Select Email from JobDesks "
                MYSQL += " Where DeskId = " & myitem.DeskNumId
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                If MYDT.Tables.Count > 0 Then
                    'myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
                    Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                    'EmailToUser(myview.RowItem("Email"), ssubject)
                End If
            End If
        Catch ex As Exception
            lblmsg.Text = "Error: - " + ex.Message
        End Try
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindDocumentUploadedFile", "BindFileUoload();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideLodingImage", "hideLodingImage();", True)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Me.Page.IsPostBack = False Then
            txtdate.Text = DateTime.Today.ToString("MM/dd/yyyy")
        Else
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If

    End Sub
End Class
