﻿
  <%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewJobNew.ascx.vb"
    Inherits="App_Controls__Custom_LienView_NewJob" %>
<%@ Register Src="GCSearch.ascx" TagName="GCSearch" TagPrefix="uc2" %>
<%@ Register Src="CustNameSearch.ascx" TagName="CustNameSearch" TagPrefix="uc1" %>
<%@ Register Src="AddLegalPty.ascx" TagName="AddLegalPty" TagPrefix="uc3" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
 
#clsdropdownClient .btn-group {width:100%;}
#clsdropdownClient .btn-group .btn {width:90%;}
#clsdropdownClient .btn-group .btn.dropdown-toggle {width:100%;}
#clsdropdownClient .btn-group .dropdown-menu {width:100%;}
</style>
<style type="text/css">
     .model-size{
         min-width:600px;
         overflow-x:scroll;
     }
    .modal-open .modal {
        overflow-x: auto !important;
    }
 :disabled {background:#cccccc !important; color:#ffffff}
    .scrollable-menu {
        height: auto;
        max-height: 180px;
        overflow-x: hidden;
    }

        .scrollable-menu::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 4px;
        }

        .scrollable-menu::-webkit-scrollbar-thumb {
            border-radius: 3px;
            background-color: lightgray;
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);
        }

    .btn_OverWrite {
        padding: 6px 15px;
        font-size: 15px;
        line-height: 13px;
        border-radius: 3px;
        -webkit-transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        -o-transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        -webkit-font-smoothing: subpixel-antialiased;
    }

    .labelAlignLeft {
        text-align: left !important;
        font-weight: normal !important;
    }
   
     #btnDropState:focus
    {
        background-color:#add8e6;
    }

    .ddlCustom {
        height:28px;
        padding: 3px 15px;
    }

    .divCustom {
        margin-top:10px;
    }
 
  
</style>


<script language="javascript" type="text/javascript">

    function EstBalanceIsRequired() {
        debugger;
       
       // $('#ctl33_ctl00_EstBalance').prop('required', false);
        $('span[title="EstBalance Is Required."]').hide();

    }
    function EstBalanceIsRequiredTrue() {
        debugger;

        // $('#ctl33_ctl00_EstBalance').prop('required', false);
        $('span[title="EstBalance Is Required."]').show();

    }
    //$(document).ready(function () 
    //    var textdata = 'sd';

    //    $('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="1" onclick="setSelectedClientId(this.id)">' + textdata + '</a></li>');
    //    $('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem"  id="2" onclick="setSelectedClientId(this.id)">Another action</a></li>');
    //    $('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem"  id="3" onclick="setSelectedClientId(this.id)">Something else here</a></li>');

    //});

  
    function Clear() {
        debugger;
        
        $("#iframe1").attr("src", "App_Controls/_Custom/LienView/Default.aspx?id=1 &jobid=1");
       
        $("#ModalFileUpload").modal('hide');

    }
    function BindFileUoloadList() {

        document.getElementById('<%=btnFileUpLoadCancel.ClientID%>').click();
    }
   
    function EstBalance_LostFocus() {
        var EstBalanceCntrl = $('#<%=EstBalance.ClientID%>');
        if (EstBalanceCntrl.val().length > 0 && EstBalanceCntrl.val().indexOf("$") == -1) {
            //var value = parseFloat(EstBalanceCntrl.val().replace(",", ""))
            var value = parseFloat(EstBalanceCntrl.val().replace(/,/g, '').replace(/$/g, ''))
            EstBalanceCntrl.val('$' + value.toFixed(2));
        }
      }

 
    $(document).ready(function () {
        SetHeaderBreadCrumb('LienView', 'Jobs', 'Place Jobs');

       
        document.getElementById('<%=StartDate.ClientID%>' + '_txtYui').tabIndex = "17";
        document.getElementById('<%=EndDate.ClientID%>' + '_txtYui').tabIndex = "18";       
       
     
    });
    function DeleteAccount(id) {
        document.getElementById('<%=hdnLegalId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteLegalId.ClientID%>').click();
      });
        return false;
    }
    function BindStateDropdown() {
        PageMethods.GetStateList(LoadStateList);
    }
    function DisplayTablelayout() {
        document.getElementById('<%=btnTableLayout.ClientID%>').click();
    }
    function LoadStateList(result) {
        $('#ulDropDownState li').remove();
        $('#ulDropDownState1 li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                $('#ulDropDownState').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liState_' + result[i].Value + '" onclick="setSelectedStateId(\'' + result[i].Value + '\',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');
                $('#ulDropDownState1').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liState_' + result[i].Value + '" onclick="setSelectedStateId(\'' + result[i].Value + '\',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');

                //$('#             ').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liState_' + result[i].Value + '" onclick="setSelectedStateId(\'' + result[i].Value + '\',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');
                //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
            }
        }
        if ($('#' + '<%=hdnStateSelectedValue.ClientID%>').val() != "") {
            // $('#btnDropState').text($('#' + $('#' + '<%=hdnStateSelectedValue.ClientID%>').val()).text());
            $('#btnDropState').html($('#liState_' + $('#' + '<%=hdnStateSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
            $('#btnDropState1').html($('#liState_' + $('#' + '<%=hdnStateSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedStateId(id, TextData) {
        $('#' + '<%=hdnStateSelectedValue.ClientID%>').val(id);
        document.getElementById('<%=btnDropStateClick.ClientID%>').click();
        $('#btnDropState').html(TextData + "<span class='caret' ></span>");
        $('#btnDropState1').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    function BindServiceTypeDropdown() {
        debugger;
        //Added by Jaywanti on 25-07-2016
        $('#UlServiceType li').remove();
      
        if ($('#' + '<%=hdnStateSelectedValue.ClientID%>').val() == "TX") {
            //alphabetize the drop down 
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job Data As Provided - No Notice Sent\')">Store Job Data As Provided - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerTX" onclick="setSelectedSserviceType(6,\'Verify Owner Only TX\')">Verify Owner Only TX</a></li>');

        }
        else {
            //alphabetize the drop down 
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(2,\'Send Notice With Data Provided\')">Send Notice With Data Provided</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job Data As Provided - No Notice Sent\')">Store Job Data As Provided - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');
           
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerSend" onclick="setSelectedSserviceType(7,\'Verify Owner Only - Send Notice \')">Verify Owner Only - Send Notice</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(5,\'Verify Owner Only - No Notice Sent\')">Verify Owner Only - No Notice Sent</a></li>');
        }
        if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "1") {
            $('#btnServiceType').html($('#liPrelimBox').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "2") {
            $('#btnServiceType').html($('#liPrelimASIS').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "3") {
            $('#btnServiceType').html($('#liVerifyJob').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "4") {
            $('#btnServiceType').html($('#liVerifyJobASIS').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "5") {
            $('#btnServiceType').html($('#liVerifyOwnerOnly').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "6") {
            $('#btnServiceType').html($('#liVerifyOwnerTX').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "7") {
            $('#btnServiceType').html($('#liVerifyOwnerSend').text() + "<span class='caret' ></span>");
        }

    return false;
}
function setSelectedSserviceType(id, TextData) {
    $('#' + '<%=hdnServiceTypeId.ClientID%>').val(id);
    $('#btnServiceType').html(TextData + "<span class='caret' ></span>");
    return false;
}
    function checkClientid(counter,Value)
    {
        //alert('checkClientid ' + Value + 'done counter ' + counter);
        if (counter == 0)
            {
             //alert(Value + "hii");
                $('#' + '<%=hdnClientSelectedtextnew.ClientID%>').val(Value);
            }
    }
function BindClientDropdown(Id) {
    //alert('hii');

    PageMethods.GetClientList(Id, LoadClientList);
}
function LoadClientList(result) {
    
    debugger;
    $('#btndrpClient li').remove();
    if (result.length > 0) {
        for (var i = 0; i < result.length; i++) {
            //alert('hii12 '+i+' check');
            checkClientid(i, result[i].Value);
            var ResultReplace = ReplaceSpecChar(result[i].Text);
            $('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClient_' + result[i].Value + '" onclick="setSelectedClientId(' + result[i].Value + ',\'' + ResultReplace + '\')">' + result[i].Text + '</a></li>');

            
            //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
        }
    }
    if ($('#liClient_' + $('#' + '<%=hdnClientSelectedId.ClientID%>').val()).text() != "") {

        $('#btnClientDropDownSelected').html($('#liClient_' + $('#' + '<%=hdnClientSelectedId.ClientID%>').val()).text() + "<span class='caret' ></span>");
     
    }
  
}
function setSelectedClientId(id, TextData) {
    var ResultReplace = TextData.replace("`", "'");
    $('#' + '<%=hdnClientSelectedId.ClientID%>').val(id);
    
        document.getElementById('<%=btnDrpClient.ClientID%>').click();

        //alert(TextData);
    $('#btnClientDropDownSelected').html(ResultReplace + "<span class='caret' ></span>");
        return false;
    }
    function BindClientListDropdown(Id) {
        //alert(Id);
        BindClientDropdown(Id);
    }

    function ReplaceSpecChar(Textdata) {
        //alert(Textdata);
        var result = Textdata;
        var ResultReplace = result.replace("'", "`");
        //alert("ResultReplace" + ResultReplace);
        return ResultReplace;
    }


    function Successpopup() {
        //alert('Success');
      
        if ($('#' + '<%=hdnIsFileUploadDeleted.ClientID%>').val() != "1") {
            $("#ModaldivReportDetail").modal('show');
        }
        $('#' + '<%=hdnIsFileUploadDeleted.ClientID%>').val("0")
        return false;
    }
    function Cancelpopup() {
        //alert('Cancel');
        location.href = "MainDefault.aspx?lienview.newjobnew&itemid=-1";
        return false;
    }

    function MaintainMenuOpen() {
        MainMenuToggle('liJobs');
        SubMenuToggle('lisubPlaceJobs');
        //$('#liJobs').addClass('site-menu-item has-sub active open');
        //$('#lisubPlaceJobs').addClass('site-menu-item active');
        return false;

    }


    function ActivateTab(TabName) {
        switch (TabName) {
            case "JobInfo":
                if (!$('#JobInfo').hasClass('active')) {
                    $('#JobInfo').addClass('active');
                }
                if (!$('#exampleTabsOne').hasClass('active'))
                    $('#exampleTabsOne').addClass('active');
                $('#Customer').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CssGeneral').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#LegalParty').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#Misc').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#Docs').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                break;
            case "Customer":
                if (!$('#Customer').hasClass('active')) {
                    $('#Customer').addClass('active');
                }
                if (!$('#exampleTabsTwo').hasClass('active'))
                    $('#exampleTabsTwo').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#CssGeneral').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#LegalParty').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#Misc').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#Docs').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                break;
            case "CssGeneral":
                if (!$('#CssGeneral').hasClass('active')) {
                    $('#CssGeneral').addClass('active');
                }
                if (!$('#exampleTabsThree').hasClass('active'))
                    $('#exampleTabsThree').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#Customer').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#LegalParty').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#Misc').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#Docs').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                break;
            case "Owner":
                if (!$('#Owner').hasClass('active')) {
                    $('#Owner').addClass('active');
                }
                if (!$('#exampleTabsFour').hasClass('active'))
                    $('#exampleTabsFour').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#Customer').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CssGeneral').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#LegalParty').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#Misc').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#Docs').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                break;
            case "Lender":
                if (!$('#Lender').hasClass('active')) {
                    $('#Lender').addClass('active');
                }
                if (!$('#exampleTabsFive').hasClass('active'))
                    $('#exampleTabsFive').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#Customer').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CssGeneral').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#LegalParty').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#Misc').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#Docs').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                break;
            case "LegalParty":
                if (!$('#LegalParty').hasClass('active')) {
                    $('#LegalParty').addClass('active');
                }
                if (!$('#exampleTabsSix').hasClass('active'))
                    $('#exampleTabsSix').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#Customer').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CssGeneral').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#Misc').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#Docs').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                break;
            case "Misc":
                if (!$('#Misc').hasClass('active')) {
                    $('#Misc').addClass('active');
                }
                if (!$('#exampleTabsSeven').hasClass('active'))
                    $('#exampleTabsSeven').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#Customer').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CssGeneral').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#LegalParty').removeClass('active');
                $('#exampleTabsSix').removeClass("active");
                $('#Docs').removeClass("active");
                $('#exampleTabsEight').removeClass("active");
                break;
            case "Docs":
                if (!$('#Docs').hasClass('active')) {
                    $('#Docs').addClass('active');
                }
                if (!$('#exampleTabsEight').hasClass('active'))
                    $('#exampleTabsEight').addClass('active');
                $('#JobInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#Customer').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#CssGeneral').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#Owner').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#Lender').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#LegalParty').removeClass('active');
                $('#exampleTabsSix').removeClass("active");
                $('#Misc').removeClass("active");
                $('#exampleTabsSeven').removeClass("active");
                break;
        }
    }

    function OpenCustSearchModal() {
        //alert('val  '+$('#'+'<%=hdnClientSelectedId.ClientID %>').val());
        if($('#'+'<%=hdnClientSelectedId.ClientID %>').val()=="0")
        {
            $("#ModelCustSelect").modal('show');
        }
        else
        {
            document.getElementById("rbtnByName").checked = true;
            document.getElementById("rbtnByRef").checked = false;
            $('#txtSearchStr').val('');
            $('#listCustName option').remove();
            $("#ModalCustSearch").modal('show');
            document.getElementById("listCustName").style.display = 'none';
        }
        return false;
    }


    function enableCustomerControls() {
        document.getElementById('<%=CustName.ClientID%>').disabled = false;
        document.getElementById('<%=CustRef.ClientID%>').disabled = false;
        document.getElementById('<%=CustAdd1.ClientID%>').disabled = false;
        document.getElementById('<%=CustAdd2.ClientID%>').disabled = false;
        document.getElementById('<%=CustCity.ClientID%>').disabled = false;
        document.getElementById('<%=CustState.ClientID%>').disabled = false;
        document.getElementById('<%=CustZip.ClientID%>').disabled = false;
        document.getElementById('<%=CustPhone.ClientID%>').disabled = false;
        document.getElementById('<%=CustFax.ClientID%>').disabled = false;
        document.getElementById('<%=CustEmail.ClientID%>').disabled = false;
        document.getElementById('<%=CustJobNum.ClientID%>').disabled = false;
        document.getElementById('<%=btnEditCustDet.ClientID%>').style.display = 'block';

    }
    function SetTabName(id) {
        document.getElementById('<%=hddbTabName.ClientID%>').value = id;
    }

    function OpenGCModal() {
        if ($('#' + '<%=hdnClientSelectedId.ClientID %>').val() == "0") {
            $("#ModelCustSelect").modal('show');
            
        }
        else {
            document.getElementById("listGCNames").style.display = 'none';
            $("#ModalGCSearch").modal('show');
        }
    }

    function GetGCNameList() {
        debugger;
        var txtGC = document.getElementById("txtGCSearchText").value;
        if (txtGC != "") {
            var ClientId = document.getElementById('<%=GCSearchClientID.ClientID%>').value;
            PageMethods.GetGCNames(txtGC, ClientId, LoadGCNames);
        }
        else {
            $('#listGCNames option').remove();
        }
        return false;
    }

    function LoadGCNames(result) {
        debugger;
        document.getElementById("listGCNames").style.display = 'block';
        $('#listGCNames option').remove();
        if (result.length > 0) {
            var ele = document.getElementById("listGCNames");
            $('#listGCNames option').remove();

            for (var i = 0; i < result.length; i++) {
                var opt = document.createElement('option');
                opt.text = result[i].Text;
                opt.value = result[i].Value;
                ele.appendChild(opt);
            }
        }
    }

    function GetGCDetails(cntrl) {
        //var GCString = cntrl.value;
        var GCString = document.getElementById("listGCNames").value;
        // alert('hi');
        // alert(GCString);
        if (GCString != "") {

            var clientId = document.getElementById('<%=GCSearchClientID.ClientID%>').value;
            //alert(clientId);
            PageMethods.GetGCDetails(GCString, clientId, LoadGCDetails);
        }
    }

    function LoadGCDetails(result) {
        //  alert('hey');
        var cntrl = document.getElementById("listGCNames").value;
        //  alert(cntrl);
        //debugger;
        if (cntrl.substring(0, 1) == "B") {
            document.getElementById('<%=GCName.ClientID%>').value = result.GCName;
            if ($('#<%=GCRef.ClientID%>').length>0)
            {
                document.getElementById('<%=GCRef.ClientID%>').value = result.GCRefNum;
            }
            document.getElementById('<%=GCAdd1.ClientID%>').value = result.GCAdd1;
             document.getElementById('<%=GCAdd2.ClientID%>').value = result.GCAdd2;
             document.getElementById('<%=GCCity.ClientID%>').value = result.GCCity;
             document.getElementById('<%=GCZip.ClientID%>').value = result.GCZip;
             document.getElementById('<%=GCState.ClientID%>').value = result.GCState;
             document.getElementById('<%=GCPhone.ClientID%>').value = result.GCPhone;
             document.getElementById('<%=GCFax.ClientID%>').value = result.GCFax;
         }
         else {
             document.getElementById('<%=GCName.ClientID%>').value = result.GeneralContractor;
            if ($('#<%=GCRef.ClientID%>').length>0)
            {
                document.getElementById('<%=GCRef.ClientID%>').value = result.RefNum;
            }
               document.getElementById('<%=GCAdd1.ClientID%>').value = result.AddressLine1;
             document.getElementById('<%=GCAdd2.ClientID%>').value = result.AddressLine2;
             document.getElementById('<%=GCCity.ClientID%>').value = result.City;
             document.getElementById('<%=GCZip.ClientID%>').value = result.PostalCode;
             document.getElementById('<%=GCState.ClientID%>').value = result.State;
             document.getElementById('<%=GCPhone.ClientID%>').value = result.Telephone1;
             document.getElementById('<%=GCFax.ClientID%>').value = result.Telephone2;


            document.getElementById('<%=GCName.ClientID%>').disabled = true;
            if ($('#<%=GCRef.ClientID%>').length > 0) {
                document.getElementById('<%=GCRef.ClientID%>').disabled = true;
            }
             document.getElementById('<%=GCAdd1.ClientID%>').disabled = true;
             document.getElementById('<%=GCAdd2.ClientID%>').disabled = true;
             document.getElementById('<%=GCCity.ClientID%>').disabled = true;
             document.getElementById('<%=GCZip.ClientID%>').disabled = true;
             document.getElementById('<%=GCState.ClientID%>').disabled = true;
             document.getElementById('<%=GCPhone.ClientID%>').disabled = true;
             document.getElementById('<%=GCFax.ClientID%>').disabled = true;
             document.getElementById('<%=GCEmail.ClientID%>').disabled = true;
             document.getElementById('<%=btnEditGeneralCont.ClientID%>').style.display = 'block';
             document.getElementById("btnEditDiv").style.display = 'block';
         }
         $("#ModalGCSearch").modal('hide');
     }

     function enableCssGeneralControl() {

         document.getElementById('<%=GCName.ClientID%>').disabled = false;
        document.getElementById('<%=GCRef.ClientID%>').disabled = false;
        document.getElementById('<%=GCAdd1.ClientID%>').disabled = false;
        document.getElementById('<%=GCAdd2.ClientID%>').disabled = false;
        document.getElementById('<%=GCCity.ClientID%>').disabled = false;
        document.getElementById('<%=GCZip.ClientID%>').disabled = false;
        document.getElementById('<%=GCState.ClientID%>').disabled = false;
        document.getElementById('<%=GCPhone.ClientID%>').disabled = false;
        document.getElementById('<%=GCFax.ClientID%>').disabled = false;
        document.getElementById('<%=GCEmail.ClientID%>').disabled = false;
    }



    function GetCustomer() {
        // alert('hii');
        var custId = document.getElementById("listCustName").value;
        // alert(custId);
        PageMethods.GetCustomer(custId, LoadCustomer);
        // alert('11');
    }
    function LoadCustomer(result) {
        //debugger;
        //alert(result.CustName);
        //alert('hi');
        var custId = document.getElementById("listCustName").value;
        if (custId.substring(0, 1) == 'B') {
            // alert('1');
            document.getElementById('<%=CustName.ClientID%>').value = result.CustName;
            document.getElementById('<%=CustRef.ClientID%>').value = result.CustRefNum;
            document.getElementById('<%=CustAdd1.ClientID%>').value = result.CustAdd1;
            document.getElementById('<%=CustAdd2.ClientID%>').value = result.CustAdd2;
            document.getElementById('<%=CustCity.ClientID%>').value = result.CustCity;
            document.getElementById('<%=CustState.ClientID%>').value = result.CustState;
            document.getElementById('<%=CustZip.ClientID%>').value = result.CustZip;
            document.getElementById('<%=CustPhone.ClientID%>').value = result.CustPhone1;

        }
        else {

            // alert('2');
            document.getElementById('<%=CustName.ClientID%>').value = result.ClientCustomer;
            document.getElementById('<%=CustRef.ClientID%>').value = result.RefNum;
            document.getElementById('<%=CustAdd1.ClientID%>').value = result.AddressLine1;
            document.getElementById('<%=CustAdd2.ClientID%>').value = result.AddressLine2;
            document.getElementById('<%=CustCity.ClientID%>').value = result.City;
            document.getElementById('<%=CustState.ClientID%>').value = result.State;
            document.getElementById('<%=CustZip.ClientID%>').value = result.PostalCode;
            document.getElementById('<%=CustPhone.ClientID%>').value = result.Telephone1;
            document.getElementById('<%=CustFax.ClientID%>').value = result.Fax;

            document.getElementById('<%=CustName.ClientID%>').disabled = true;
            document.getElementById('<%=CustRef.ClientID%>').disabled = true;
            document.getElementById('<%=CustAdd1.ClientID%>').disabled = true;
            document.getElementById('<%=CustAdd2.ClientID%>').disabled = true;
            document.getElementById('<%=CustCity.ClientID%>').disabled = true;
            document.getElementById('<%=CustState.ClientID%>').disabled = true;
            document.getElementById('<%=CustZip.ClientID%>').disabled = true;
            document.getElementById('<%=CustPhone.ClientID%>').disabled = true;
            document.getElementById('<%=CustFax.ClientID%>').disabled = true;
            document.getElementById('<%=CustEmail.ClientID%>').disabled = true;
            document.getElementById('<%=CustJobNum.ClientID%>').disabled = true;
            document.getElementById('<%=btnEditCustDet.ClientID%>').style.display = 'block';



        }
        $("#ModalCustSearch").modal('hide');
    }
    function callbtnevnt() {

        document.getElementById('<%=btngetstate.ClientID%>').click();
    }
   <%-- function OwnrSameChanged() {

        document.getElementById('=btnOwnrSame.ClientID').click();
    }--%>
    function OpenTreasurySuretyAddressList() {
        var strpath = "http://www.fiscal.treasury.gov/fsreports/ref/suretyBnd/c570_a-z.htm";
        window.open(strpath, "mywindow", "menubar=1,resizable=1,width=1050,height=800,scrollbars=yes");
        return false;
    }
   

    //mail - 02/05/20 - LienView New Job – Validate Prelim Rush control - start
    function AlertValidatePrelimRushcontrolFunc(ItemId) {
        debugger;

        //alert("Something went wrong!" + ItemId);

        swal({
            title: "Error!",
            text: "Please uncheck the 'Prelim Rush' checkbox for 'Send Notice With Data Provided' and 'Store Job Data As Provided - No Notice Sent' Service Type.",
            type: "error",
            showCancelButton: false,
            showConfirmButton: true,
            confirmButtonText: "OK",
            confirmButtonClass: "btn-danger",     
         closeOnConfirm: true
        },
          function () {
              swal.close(); // close the sweet alert
             // return false;
          });
    }

 
    //mail - 02/05/20 - LienView New Job – Validate Prelim Rush control - end

    function AlertSuccessFunc() {
        //alert('hi');
        swal({
            title: "Successfully Submitted!",
            text: "Your New Job Has Been Submitted Successfully!",
            showCancelButton: true,
            confirmButtonClass: "btn btn-success",
            cancelButtonText: "Add New Job",
            confirmButtonText: 'View Job Info', //'View Report',
            showDoneButton: true,
            closeOnDone: false,
            type: "success"
        });
        // $('#exampleSuccessMessage').click();
        document.getElementsByClassName('closedone')[0].setAttribute("onclick", "closeOnDone()");
    }
    function closeOnDone()
    {
        location.href = "MainDefault.aspx?lienview.newjoblist";
        return false;
    }
    $('#btnSucessAddNewJob').click(function () {
        alert('hh');
    });
    function SearchCustName() {
        // alert('hi');
        // alert(document.getElementById("txtSearchStr").value);
        //debugger;
        var SearchStr = document.getElementById("txtSearchStr").value;
        // alert(SearchStr);
        if (SearchStr != "") {
            var type;
            if (document.getElementById("rbtnByName").checked)
                type = "Name";
            else
                type = "Ref";
            // alert(type);
            var ClientId = document.getElementById('<%=hdnCustSearchClientID.ClientID%>').value;// document.getElementById("ctl25_ctl00_CustSearchClientID").value;
            //alert(ClientId);
            PageMethods.CustSearchList(SearchStr, type, ClientId, BindCustomer);
        }
        else {
            $('#listCustName option').remove();
        }
    }
    function BindCustomer(result) {
        // debugger;
        //('p');
        document.getElementById("listCustName").style.display = 'block';
        $('#listCustName option').remove();
        if (result.length > 0) {
            var ele = document.getElementById("listCustName");
            $('#listCustName option').remove();

            for (var i = 0; i < result.length; i++) {
                var opt = document.createElement('option');
                opt.text = result[i].Text;
                opt.value = result[i].Value;
                //opt.setAttribute(result[i].text, result[i].value);
                ele.appendChild(opt);
            }
        }
    }
    function DeleteAccountDoc(id) {
        document.getElementById('<%=hdnDocId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteDoc.ClientID%>').click();
       });
         return false;
    }
    
    //##1


    //$("#ctl32_ctl00_btnSave").on("focus", function () {
    //    alert();
    //})

    document.onkeydown = MoveTabIndex;

    function MoveTabIndex() {
        var TabIndex = document.activeElement.tabIndex;
       // alert(document.activeElement.tabIndex);
        if (TabIndex == 22 || TabIndex == -1 || TabIndex == 0) {        
            document.getElementById("btnClientDropDownSelected").focus();
            document.activeElement.tabIndex = 1;
        }
       
      }

</script>
<script language="javascript" type="text/javascript">
    function noenter(e) {
        // alert('hi');
        // alert(e.target.id);
        //  alert(e.srcElement.id);
        // debugger;
        // alert(e.target.id);
        //Added by jay for the search button click
        //debugger;
        if ((e.target || e.srcElement).id == "txtSearchStr" || (e.target || e.srcElement).id == "txtGCSearchText") {
            e = e || window.event;
            var key = e.keyCode || e.charCode;
            if (key == 13) {
                if ((e.target || e.srcElement).id == "txtSearchStr") {
                    // alert('1');
                    SearchCustName();
                    //callsearch();
                }
                else if ((e.target || e.srcElement).id == "txtGCSearchText") {
                    //alert('2');
                    GetGCNameList();
                    //callsearchGC();
                }
                // event.returnValue = false;
                (event.preventDefault) ? event.preventDefault() : event.returnValue = false;
                //  e.preventDefault();
            }
        }
        else {
            e = e || window.event;
            var key = e.keyCode || e.charCode;
            return key !== 13;
        }
        //return false;
    }
    function IsCheckBoxChecked() {
        var chkStatus = document.getElementById('<%=GreenCard.ClientID%>');
        if (!chkStatus.checked) {
            document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "inline";
        } else { document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "none"; }
    }
    window.onload = function () {

        var chkStatus = document.getElementById('<%=GreenCard.ClientID%>');
        if (!chkStatus.checked) {
            document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "inline";
        } else { document.getElementById('<%=lblReturnRcptMsg.ClientID%>').style.display = "none"; }


    };

 
</script>

<div id="modcontainer" style="width: 100%;">
    <h1 class="panelheader">New Job</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body" onkeypress="return noenter(event)">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                            <asp:Button ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to List"
                                CausesValidation="False" /><asp:HiddenField runat="server" ID="hddbTabName" Value="JobInfo" />
                        </div>
                    </div>
                </div>

                <div class="nav-tabs-horizontal">
                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                        <li id="JobInfo" class="active" role="presentation"><a data-toggle="tab" onclick="SetTabName('JobInfo');" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Job Info</a></li>
                        <li id="Customer" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Customer');" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Customer</a></li>
                        <li id="CssGeneral" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('CssGeneral');" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab" aria-expanded="false">General Contractor</a></li>
                        <li id="Owner" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Owner');" href="#exampleTabsFour" aria-controls="exampleTabsFour" role="tab" aria-expanded="false">Owner</a></li>
                        <li id="Lender" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Lender');" href="#exampleTabsFive" aria-controls="exampleTabsFive" role="tab" aria-expanded="false">Lender/Surety</a></li>
                        <li id="LegalParty" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('LegalParty');" href="#exampleTabsSix" aria-controls="exampleTabsSix" role="tab" aria-expanded="false">Add Legal Parties</a></li>
                        <li id="Misc" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Misc');" href="#exampleTabsSeven" aria-controls="exampleTabsSeven" role="tab" aria-expanded="false">Misc</a></li>
                        <li id="Docs" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Docs');" href="#exampleTabsEight" aria-controls="exampleTabsEight" role="tab" aria-expanded="false">Docs</a></li>
                    </ul>
                    <div class="tab-content padding-top-20" style="border: 1px solid lightgray;">
                        <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                            <div class="form-horizontal">


                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">

                                        <div class="form-group divCustom">
                                            <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Job State:</label>
                                            <div class="col-md-4 col-sm-4 col-xs-4" style="text-align: left;">
                                                <div class="btn-group" style="text-align: left;" align="left">
                                                    <%--##1--%>                                                    
                                                    <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="width: 130px;" align="left"
                                                        data-toggle="dropdown" aria-expanded="false" id="btnDropState1" tabindex="-1">
                                                        --Select State--
                     
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState1" style="overflow-y: auto; height: 250px;">
                                                    </ul>
                                                </div>


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Service Type:</label>
                                            <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">

                                                <div class="example-wrap" style="margin-bottom: 0; white-space: nowrap; display: none;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PrelimBox" runat="server" Text=" Verify Job Data & Send Notice"
                                                            GroupName="JobType" Checked="True"></asp:RadioButton>
                                                    </div>
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PrelimASIS" runat="server" Text=" Send Notice With Data Provided"
                                                            GroupName="JobType"></asp:RadioButton>
                                                    </div>
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="VerifyJob" runat="server" Text=" Verify Job Data Only - No Notice Sent"
                                                            GroupName="JobType"></asp:RadioButton>
                                                    </div>
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="VerifyJobASIS" runat="server" Text=" Store Job Data As Provided - No Notice Sent"
                                                            GroupName="JobType"></asp:RadioButton>
                                                    </div>
                                                    <button type="button" class="btn btn-outline btn-default" style="display: none;" id="exampleSuccessMessage"
                                                        data-plugin="sweetalert" data-title="Successfully Submitted!" data-text="Your New Job Has Been Successfully Submitted!"
                                                        data-type="success">
                                                        Success message</button>
                                                </div>

                                                <div class="btn-group" style="text-align: left;" align="left">
                                                    <%--##1--%>
                                                    <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="width: 328px; text-align: left;" align="left"
                                                        data-toggle="dropdown" aria-expanded="false" id="btnServiceType" tabindex="-1">                                                    
                                                        --Select Service Type--
                     
                                                                                    <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlServiceType" style="overflow-y: auto; height: 150px;">
                                                        <%-- <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,'Verify Job Data & Send Notice')">Verify Job Data & Send Notice</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(1,'Send Notice With Data Provided')">Send Notice With Data Provided</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(1,'Verify Job Data Only - No Notice Sent')">Verify Job Data Only - No Notice Sent</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(1,'Store Job Data As Provided - No Notice Sent')">Store Job Data As Provided - No Notice Sent</a></li>--%>
                                                    </ul>
                                                </div>
                                                <asp:HiddenField ID="hdnServiceTypeId" runat="server" Value="3" />
                                                <%--      <div class="btn-group" >
                                                 
                                                    <asp:DropDownList ID="fgfg" runat="server" CssClass="btn btn-default dropdown-toggle animate " role="menu">
                                                        <asp:ListItem Value="1" >sdsd</asp:ListItem>
                                                        <asp:ListItem Value="13" >sdfdfdsd</asp:ListItem>
                                                    </asp:DropDownList>
                                                </div>--%>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Check Boxes:</label>
                                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                                                <div class="checkbox-custom checkbox-default">
                                                    <asp:CheckBox ID="RushOrder" runat="server" Text=" Prelim Rush" TabIndex="16"></asp:CheckBox><br />
                                                    <asp:CheckBox ID="GreenCard" runat="server" onclick="IsCheckBoxChecked();" Text=" Return Receipt" TabIndex="17"></asp:CheckBox><br />
                                                    <asp:CheckBox ID="JointCk" runat="server" Text=" Joint Check" TabIndex="18"></asp:CheckBox><br />
                                                    <asp:CheckBox ID="NOCBox" runat="server" Text=" Notice of Comp Search" ></asp:CheckBox>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4" style="padding-right: 0px;">
                                                <div class="checkbox-custom checkbox-default">
                                                    <asp:CheckBox ID="PublicJob" runat="server" Text=" Public Job" TabIndex="19"></asp:CheckBox><br />
                                                    <asp:CheckBox ID="FederalJob" runat="server" Text=" Federal Job" TabIndex="20"></asp:CheckBox><br />
                                                    <asp:CheckBox ID="ResidentialBox" runat="server" Text=" Residential" TabIndex="21"></asp:CheckBox>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Client#:</label>
                                            <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;" id="clsdropdownClient">

                                                <div class="btn-group" style="text-align: left;" align="left">
                                                    <%--##1--%>
                                                    <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="overflow: hidden;min-height:28px;text-align: left;" align="left"
                                                        data-toggle="dropdown" aria-expanded="false" id="btnClientDropDownSelected" tabindex="-1" >                                    
                                                           --Select Client--<span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="btndrpClient" style="min-width:450px;overflow-y: auto; height: 150px;overflow-x: auto;">
                                                        <%--  <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>
                                                                                    <li class="divider" role="presentation"></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Separated link</a></li>--%>
                                                    </ul>
                                                </div>
                                                <asp:HiddenField ID="hdnClientSelectedId" runat="server" Value="0" />
                                               
                                                <cc1:ExtendedDropDown ID="ClientTableId" CssClass="form-control" runat="server" 
                                                    AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                                    IsValid="True" Tag="" Value="" Style="display: none;">
                                                </cc1:ExtendedDropDown>
                                                <asp:Button ID="btnDrpClient" runat="server" Style="display: none" OnClick="btnDrpClient_Click" />
                                            <asp:HiddenField ID="hdnClientSelectedtextnew" runat="server" value=""/>
                                                 </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">First Furnished</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <span style="display: flex;">
                                                 <%--   <SiteControls:Calendar ID="StartDate" runat="server" ErrorMessage="First Furnished Date" IsReadOnly="false"
                                                        IsRequired="true" Value="" TabIndex="9" />--%>
                                                    <cc1:DataEntryBox ID="StartDate" runat="server" DataType="datevalue" CssClass="form-control datepicker" IsRequired="True" IsValid="True"
                                                         TabIndex="9" Width="120px" Enabled="true" />&nbsp;

                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Branch#:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <span style="display: flex;">
                                                    <cc1:DataEntryBox ID="BranchNum" runat="server" CssClass="form-control" Width="175px" DataType="Any" ErrorMessage=""
                                                        EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                                        ValidationExpression="" Value="" TabIndex="1">
                                                    </cc1:DataEntryBox>
                                                </span>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Last Furnished</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <span style="display: flex;">
                                                    <%--##1--%>
                                                  <%--  <SiteControls:Calendar ID="EndDate" runat="server" ErrorMessage="Last Furnished Date" IsReadOnly="false"
                                                        IsRequired="false" Value="" TabIndex="10" />--%>
                                                    <cc1:DataEntryBox ID="EndDate" runat="server" DataType="datevalue" CssClass="form-control datepicker" IsRequired="False" 
                                                        IsValid="True" TabIndex="10" Width="120px" Enabled="true" />
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Job#:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <cc1:DataEntryBox ID="JobNum" runat="server" CssClass="form-control" Width="175px" DataType="Any"
                                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="2">
                                                </cc1:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Est Balance:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <span style="display: flex;">
                                                    <cc1:DataEntryBox ID="EstBalance" runat="server" CssClass="form-control" DataType="Any"
                                                        EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                                        FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                                        placeholder ="$0.00" Value="" Width="175px" TabIndex="11"></cc1:DataEntryBox>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Job Name:</label>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <cc1:DataEntryBox ID="JobName" runat="server" CssClass="form-control" DataType="Any"
                                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="3"></cc1:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">APN:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <cc1:DataEntryBox ID="APNNUM" runat="server" CssClass="form-control" Width="175px" DataType="Any"
                                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="12"></cc1:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Address:</label>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <span style="display: flex;">
                                                    <cc1:DataEntryBox ID="JobAddr1" runat="server" CssClass="form-control" DataType="Any"
                                                        ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                        FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="4"></cc1:DataEntryBox>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">PO#:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <cc1:DataEntryBox ID="PONUM" runat="server" CssClass="form-control" Width="175px" DataType="Any"
                                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="13"></cc1:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable"></label>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <span style="display: flex;">
                                                    <cc1:DataEntryBox ID="JobAddr2" runat="server" CssClass="form-control" DataType="Any"
                                                        ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                        FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="5"></cc1:DataEntryBox>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">NOC/Folio#:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <cc1:DataEntryBox ID="FolioNUm" runat="server" CssClass="form-control" Width="175px" DataType="Any"
                                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="14"></cc1:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">City:</label>
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <span style="display: flex;">
                                                    <cc1:DataEntryBox ID="JobCity" runat="server" CssClass="form-control" DataType="Any"
                                                        ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                        FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="6"></cc1:DataEntryBox>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Building Permit#:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <cc1:DataEntryBox ID="BuildingPermitNum" runat="server" CssClass="form-control" Width="175px"
                                                    DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="15"></cc1:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">State/Zip:</label>
                                            <div class="col-md-4 col-sm-4 col-xs-4" style="text-align: left;">
                                                <div class="btn-group" style="text-align: left;" align="left">
                                                    <button type="button" class="btn btn-default dropdown-toggle" style="width: 130px;" align="left"
                                                        data-toggle="dropdown" aria-expanded="false" id="btnDropState" tabindex="7">
                                                        --Select State--
                     
                                                                                    <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState" style="overflow-y: auto; height: 250px;">
                                                        <%--  <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>
                                                                                    <li class="divider" role="presentation"></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Separated link</a></li>--%>
                                                    </ul>
                                                </div>
                                                <%--  <div class="btn-group">
                                                                                <button type="button" class="btn btn-default dropdown-toggle" id="exampleAnimationDropdown1" data-toggle="dropdown" aria-expanded="false">
                                                                                    Animation
                      <span class="caret"></span>
                                                                                </button>
                                                                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu">
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>
                                                                                    <li class="divider" role="presentation"></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Separated link</a></li>
                                                                                </ul>
                                                                            </div>--%>
                                                <asp:HiddenField ID="hdnStateSelectedValue" runat="server" Value="" />
                                                <asp:Button ID="btnDropStateClick" runat="server" Style="display: none;" OnClick="btnDropStateClick_Click" />
                                                <cc1:ExtendedDropDown ID="StateTableId" OnSelectedIndexChanged="StateTableId_SelectedIndexChanged" CssClass="btn btn-default dropdown-toggle" runat="server" Width="125px"
                                                    AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                                    IsValid="True" Tag="" Value="" Style="display: none;">
                                                </cc1:ExtendedDropDown>


                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <span style="display: flex;">
                                                    <cc1:DataEntryBox ID="JobZip" runat="server"  CssClass="form-control" DataType="Any"
                                                        ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                        FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="8"></cc1:DataEntryBox>
                                                </span>
                                            </div>
                                            <%--     <div class="btn-group">
                                        <button type="button" class="btn btn-default dropdown-toggle" id="exampleAnimationDropdown1"
                                            data-toggle="dropdown" aria-expanded="false">
                                            Animation
                     
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu">
                                            <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                            <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                            <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>
                                            <li class="divider" role="presentation"></li>
                                            <li role="presentation"><a href="javascript:void(0)" role="menuitem">Separated link</a></li>
                                        </ul>
                                    </div>--%>
                                        </div>
                                    </div>
                                 <div class="col-md-6  col-sm-6 col-xs-6">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Filter Key:</label>
                                            <div class="col-md-7 col-sm-7 col-xs-7">
                                                <cc1:DataEntryBox ID="FilterKey" runat="server" CssClass="form-control" Width="175px"
                                                    DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="15"></cc1:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 col-xs-12" align="left">
                                        <div class="form-group" align="left">
                                            <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">State Alerts:</label>
                                            <div class="col-md-8 col-sm-8 col-xs-8 col-sm-8 col-xs-8">
                                                <%--##1--%>
                                                <asp:TextBox ID="txtstatealert" runat="server" TextMode="MultiLine" ReadOnly="True"
                                                    Height="75px" ForeColor="Red" CssClass="form-control" Font-Names="Arial" TabIndex="-1"></asp:TextBox>                                               
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="form-group">
                                <div align="left">
                                    <asp:Label ID="lblmsg" runat="server" Text="To request a Mechanics Lien on job, please call us at 805-823-8032."
                                        Font-Names="Trebuchet MS" ForeColor="Red" Font-Size="Small"></asp:Label>
                                    <asp:Label ID="lblReturnRcptMsg" runat="server" Text=" Return Receipts after mailing are no longer available from the US Post Office. Return Receipts must be requested prior to mailing in order to obtain a copy of the recipient’s signature."
                                        Font-Names="Trebuchet MS" ForeColor="Red" Font-Size="Small"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Name:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5">
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="CustName" runat="server" CssClass="form-control" DataType="Any"
                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="1000"></cc1:DataEntryBox>

                                            <%--    <input type="button" id="btn1" onclick="OpenCustSearchModal();" value="Find New" class="btn btn-primary" />--%>
                                          
                                        </span>
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">

                                        <uc1:CustNameSearch ID="CustNameSearch1" runat="server" TabIndex="1001"  />
                                        &nbsp;&nbsp;&nbsp;
                                        <div style="display: flex; float: left;">
                                            <asp:LinkButton ID="btnEditCustDet" OnClientClick="enableCustomerControls();return false;" CssClass="btn_OverWrite btn-primary" Style="display: none; width: 50px; font-size: small; margin-left: 20px;" runat="server" Text="Edit"/>
                                        </div>

                                    </div>
                                    <%--  <div class="col-md-1" style="text-align: left;">
                                      
                                    </div>--%>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Ref#:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                           <span style="display: flex;">
                                        <cc1:DataEntryBox ID="CustRef" runat="server" CssClass="form-control" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="1002"></cc1:DataEntryBox>
                                            </span>
                                               </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address1:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5">
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="CustAdd1" runat="server" CssClass="form-control" DataType="Any"
                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="1003"></cc1:DataEntryBox>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address2:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5">
                                        <cc1:DataEntryBox ID="CustAdd2" runat="server" CssClass="form-control" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="1004"></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">City:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5">
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="CustCity" runat="server" CssClass="form-control" DataType="Any"
                                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="1005"></cc1:DataEntryBox>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="CustState" runat="server" CssClass="form-control" Style="text-align: center; width: 70px;"
                                                DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                                FormatString="" FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression=""
                                                Value=""  TabIndex="1006"></cc1:DataEntryBox>&nbsp;
                                                                                  <cc1:DataEntryBox ID="CustZip" runat="server" CssClass="form-control " DataType="Any"
                                                                                      ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                      FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="" Value="" TabIndex="1007"></cc1:DataEntryBox>
                                        </span>
                                    </div>
                                    <%--<div class="col-md-2  col-sm-2 col-xs-2">
                                                                            <span style="display: flex;">

                                                                            </span>
                                                                                                                                                      </div>--%>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Phone:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="CustPhone" runat="server" Width="135px" CssClass="form-control" Style="text-align: center"
                                                DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                                FormatString="" FriendlyName="" IsRequired="true" IsValid="True" ValidationExpression=""
                                                Value=""  TabIndex="1008"></cc1:DataEntryBox>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Fax:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="CustFax" runat="server" Width="135px" CssClass="form-control" Style="text-align: center"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""  TabIndex="1009"></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Email:</label>
                                    <div class="col-md-6  col-sm-6 col-xs-6">
                                        <cc1:DataEntryBox ID="CustEmail" runat="server" CssClass="form-control" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="1010"></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Cust Job#:</label>
                                    <div class="col-md-2  col-sm-2 col-xs-2">
                                        <cc1:DataEntryBox ID="CustJobNum" runat="server" CssClass="form-control" Width="125px"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""  TabIndex="1011"></cc1:DataEntryBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Name:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5">
                                        <div style="display: flex;">
                                            <cc1:DataEntryBox ID="GCName" TabIndex="31" runat="server" CssClass="form-control"
                                                DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCName" BehaviorID="FTBExtGCName" runat="server" TargetControlID="GCName"
                                                FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`"></ajaxToolkit:FilteredTextBoxExtender>

                                        </div>
                                    </div>

                                    <div id="Div1" class="col-md-4 col-sm-4 col-xs-4" style="text-align: left; display: flex;">
                                        <uc2:GCSearch ID="GCSearch1" TabIndex="9999" runat="server" />

                                        <div id="btnEditDiv" style="margin-left: 10px;">

                                            <asp:LinkButton ID="btnEditGeneralCont" CssClass="btn_OverWrite btn-primary" Style="display: none; font-size: small; width: 50px;" runat="server" Text="Edit" />
                                        </div>
                                        <div class="checkbox-custom checkbox-default" style="padding-top: 0; margin-bottom: -9px; margin-left: 10px;">

                                            <asp:CheckBox ID="GCSameAsCust" runat="server" Text=" Same As Customer" AutoPostBack="True" TabIndex="9999" />
                                        </div>

                                    </div>


                                    <%--<div class="col-md-3 col-sm-3 col-xs-3">
                                       
                                    </div>--%>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Ref#:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="GCRef" TabIndex="32" runat="server" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCRef" BehaviorID="FTBExtGCRef" runat="server" TargetControlID="GCRef"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address1:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5">
                                        <div style="display: flex;">
                                            <cc1:DataEntryBox ID="GCAdd1" runat="server" TabIndex="33" CssClass="form-control"
                                                DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                            <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCAdd1" BehaviorID="FTBExtGCAdd1" runat="server" TargetControlID="GCAdd1"
                                                FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`"></ajaxToolkit:FilteredTextBoxExtender>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address2:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5">
                                        <cc1:DataEntryBox ID="GCAdd2" runat="server" TabIndex="34" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCAdd2" BehaviorID="FTBExtGCAdd2" runat="server" TargetControlID="GCAdd2"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">City:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="GCCity" runat="server" TabIndex="35" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCCity" BehaviorID="FTBExtGCCity" runat="server" TargetControlID="GCCity"
                                            FilterInterval="20" FilterMode="InvalidChars" InvalidChars="!<>~^=|`"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="display: flex;">
                                        <cc1:DataEntryBox ID="GCState" runat="server" TabIndex="36" CssClass="form-control" Style="text-align: center; width: 70px;"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;
                                        <cc1:DataEntryBox ID="GCZip" runat="server" TabIndex="37" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>

                                    </div>
                                    <div class="col-md-2  col-sm-2 col-xs-2">
                                      <%--  <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCState" BehaviorID="FTBExtGCState" runat="server" TargetControlID="GCState"
                                            FilterInterval="20" FilterMode="ValidChars" FilterType="UppercaseLetters"></ajaxToolkit:FilteredTextBoxExtender>--%>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCZip" BehaviorID="FTBExtGCZip" runat="server" TargetControlID="GCZip"
                                            FilterInterval="20" FilterMode="ValidChars" ValidChars="1234567890-"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Phone:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="GCPhone" runat="server" TabIndex="38" Width="135px" CssClass="form-control" Style="text-align: center"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCPhone" BehaviorID="FTBExtGCPhone" runat="server" TargetControlID="GCPhone"
                                            FilterInterval="20" FilterMode="ValidChars" ValidChars="1234567890-"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Fax:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="GCFax" runat="server" TabIndex="39" Width="135px" CssClass="form-control" Style="text-align: center"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>
                                        <ajaxToolkit:FilteredTextBoxExtender ID="FTBExtGCFax" BehaviorID="FTBExtGCFax" runat="server" TargetControlID="GCFax"
                                            FilterInterval="20" FilterMode="ValidChars" ValidChars="1234567890-"></ajaxToolkit:FilteredTextBoxExtender>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Email:</label>
                                    <div class="col-md-6  col-sm-6 col-xs-6  col-sm-6 col-xs-6">
                                        <cc1:DataEntryBox ID="GCEmail" runat="server" CssClass="form-control" DataType="Any"
                                            ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value="" TabIndex="40"></cc1:DataEntryBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exampleTabsFour" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Name:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="OwnrName" TabIndex="41" runat="server" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;&nbsp;
                                
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="display: flex;">
                                        <div class="checkbox-custom checkbox-default" style="padding-top: 0px; margin-bottom: -9px;">
                                            <asp:CheckBox ID="OwnrSameAs" runat="server" Text="  Same As GC" AutoPostBack="True" CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address1:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="OwnrAdd1" TabIndex="42" runat="server" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;&nbsp;
                                  
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="display: flex;">
                                        <div class="checkbox-custom checkbox-default" style="padding-top: 0px; margin-bottom: -9px;">
                                            <asp:CheckBox ID="CopyJob" runat="server" TabIndex="49" Text="  Copy Job Info" AutoPostBack="True" CausesValidation="false" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address2:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="OwnrAdd2" TabIndex="43" runat="server" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;&nbsp;
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="display: flex;">
                                        <div class="checkbox-custom checkbox-default" style="padding-top: 0px; margin-bottom: -9px;">
                                            <asp:CheckBox ID="MLAgent" runat="server" TabIndex="50" Text=" Show As Lien Agent" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">City:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="OwnrCity" runat="server" TabIndex="44" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="display: flex;">
                                        <cc1:DataEntryBox ID="OwnrState" runat="server" TabIndex="45" CssClass="form-control"
                                            Style="text-align: center; width: 70px;" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <cc1:DataEntryBox ID="OwnrZip" runat="server" TabIndex="46" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                    <%--  <div class="col-md-2  col-sm-2 col-xs-2">
                                                                          
                                                                        </div>--%>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Phone:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="OwnrPhone" runat="server" Width="135px" TabIndex="47" CssClass="form-control"
                                            Style="text-align: center" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Fax:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="OwnrFax" runat="server" Width="135px" TabIndex="48" CssClass="form-control" Style="text-align: center"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exampleTabsFive" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Name:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="LenderName" runat="server" TabIndex="51" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;&nbsp;
                                 
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="display: flex;">
                                        <div class="checkbox-custom checkbox-default" style="padding-top: 0px; margin-bottom: -9px;">
                                            <asp:CheckBox ID="SuretyBox" runat="server" Text="  Is Surety" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address1:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="LenderAdd1" TabIndex="52" runat="server" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;&nbsp;

                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4" style="display: flex; margin-left: -24px;">
                                        <asp:LinkButton ID="TreasurySuretyAddressList" CssClass="btn_OverWrite btn-primary" runat="server" Text="Treasury Surety Address List" Style="height: 28px; cursor: pointer;" OnClientClick="OpenTreasurySuretyAddressList();"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Address2:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="LenderAdd2" TabIndex="52" runat="server" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">City:</label>
                                    <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                        <cc1:DataEntryBox ID="LenderCity" TabIndex="53" runat="server" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>&nbsp;&nbsp;
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">State/Zip:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3" style="display: flex;">
                                        <cc1:DataEntryBox ID="LenderState" runat="server" TabIndex="54" CssClass="form-control"
                                            Style="text-align: center; width: 70px;" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>&nbsp;
                                        <cc1:DataEntryBox ID="LenderZip" runat="server" TabIndex="55" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                            FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                            Value=""></cc1:DataEntryBox>
                                    </div>
                                    <%--  <div class="col-md-2  col-sm-2 col-xs-2">
                                                                          
                                                                        </div>--%>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Phone:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="LenderPhone" runat="server" Width="135px" TabIndex="56" CssClass="form-control"
                                            Style="text-align: center" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Fax:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="LenderFax" runat="server" Width="135px" TabIndex="57" CssClass="form-control"
                                            Style="text-align: center" DataType="Any" ErrorMessage="" EnableClientSideScript="False"
                                            FormatMask="" FormatString="" FriendlyName="" IsRequired="False" IsValid="True"
                                            ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Bond #:</label>
                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                        <cc1:DataEntryBox ID="BondNum" runat="server" TabIndex="58" CssClass="form-control"
                                            DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                            FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exampleTabsSix" role="tabpanel">


                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-2 col-sm-2 col-xs-2" style="margin-left: 10px;">
                                        <uc3:AddLegalPty ID="AddLegalPty1" runat="server" />
                                        <button class="btn btn-primary" onclick="OpenLegalPartyModal();return false;">Add Legal Party New</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div style="width: auto; height: auto; overflow: auto;">
                                            <asp:GridView ID="gvwAddLegal" CssClass="table dataTable table-striped" runat="server" AutoGenerateColumns="False"
                                                DataKeyNames="Id" CellPadding="4" PageSize="20" Width="99%">
                                                <%--  <RowStyle CssClass="rowstyle" />
                                                <AlternatingRowStyle CssClass="altrowstyle" />
                                                <HeaderStyle CssClass="headerstyle" />--%>
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <%--                                                            <asp:ImageButton ID="btnEditLP" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png"
                                                                runat="server" />--%>
                                                            <asp:LinkButton ID="btnEditLP" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="15px" Wrap="False" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="TypeCode" SortExpression="TypeCode" HeaderText="TypeCode"></asp:BoundField>
                                                    <asp:BoundField DataField="AddressName" SortExpression="AddressName" HeaderText="AddressName"></asp:BoundField>
                                                    <asp:BoundField DataField="AddressLine1" SortExpression="AddressLine1" HeaderText="AddressLine1"></asp:BoundField>
                                                    <asp:BoundField DataField="City" SortExpression="City" HeaderText="City"></asp:BoundField>
                                                    <asp:BoundField DataField="State" SortExpression="State" HeaderText="State"></asp:BoundField>
                                                    <asp:BoundField DataField="PostalCode" SortExpression="PostalCode" HeaderText="PostalCode"></asp:BoundField>
                                                    <asp:BoundField DataField="TelePhone1" SortExpression="TelePhone1" HeaderText="TelePhone1"></asp:BoundField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteLP" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                            <%--  <asp:ImageButton ID="btnDeleteLP" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif"
                                                                                                    runat="server" />--%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="15px" Wrap="False" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <b>No Records found for the specified criteria</b>
                                                </EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="tab-pane" id="exampleTabsSeven" role="tabpanel">
                            <asp:Panel ID="panInst" runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Special Instruction:</label>
                                        <div class="col-md-7 col-sm-7 col-xs-7">
                                            <cc1:DataEntryBox ID="SpecialInstruction" runat="server" CssClass="form-control"
                                                Tag="Special Instruction" IsRequired="False" Height="60px" TextMode="MultiLine"
                                                TabIndex="53" BackColor="White" BlankOnZeroes="True" DataType="Any" ErrorMessage=""
                                                IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False"
                                                FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc1:DataEntryBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable"></label>
                                        <div class="col-md-7 col-sm-7 col-xs-7">
                                            (If Special Instruction is time sensitive, please contact Client Services at 800-522-3858)
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Legal Description:</label>
                                        <div class="col-md-7 col-sm-7 col-xs-7">
                                            <cc1:DataEntryBox ID="Note" runat="server" CssClass="form-control" Tag="Special Instruction"
                                                IsRequired="False" Height="52px" TextMode="MultiLine" TabIndex="54" BackColor="White"
                                                BlankOnZeroes="True" DataType="Any" ErrorMessage="" IsValid="True" NormalCSS="textbox"
                                                RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask=""
                                                FormatString="" FriendlyName="" ValidationExpression=""></cc1:DataEntryBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panRentalINfo" runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">RA #:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <cc1:DataEntryBox ID="RANum" runat="server" CssClass="form-control" IsRequired="False"
                                                TabIndex="56" tag="RANum" DataType="Any" BackColor="White" BlankOnZeroes="True"
                                                ErrorMessage="" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox"
                                                Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName=""
                                                ValidationExpression=""></cc1:DataEntryBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Equip Rate:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3">
                                            <cc1:DataEntryBox ID="EquipRate" runat="server" CssClass="form-control" IsRequired="False"
                                                TabIndex="57" tag="EquipRate" DataType="Any" BackColor="White"
                                                BlankOnZeroes="True" ErrorMessage="" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox"
                                                Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName=""
                                                ValidationExpression=""></cc1:DataEntryBox>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Equip Description:</label>
                                        <div class="col-md-7 col-sm-7 col-xs-7">
                                            <cc1:DataEntryBox ID="EquipRental" runat="server" CssClass="form-control" IsRequired="False"
                                                TabIndex="58" tag="EquipRental" BackColor="White" BlankOnZeroes="True"
                                                DataType="Any" ErrorMessage="" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox"
                                                Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName=""
                                                ValidationExpression=""></cc1:DataEntryBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <asp:Panel ID="panAZLotAllocate" runat="server">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <%--##1--%>
                                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Lot Allocation:</label>
                                        <div class="col-md-7 col-sm-7 col-xs-7 col-sm-7 col-xs-7">
                                            <cc1:DataEntryBox ID="AZLotAllocate" runat="server" CssClass="form-control"
                                                Tag="Special Instruction" IsRequired="False" Height="52px" MaxLength="150" TextMode="MultiLine"
                                                TabIndex="59" DataType="Any" EnableClientSideScript="False" ErrorMessage="" FormatMask=""
                                                FormatString="" FriendlyName="" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <div class="tab-pane" id="exampleTabsEight" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-2  col-sm-2 col-xs-2" style="margin-left: 10px;">
                                        <asp:Button ID="btnAddDocuments" runat="server" CssClass="btn btn-primary" CausesValidation="False"
                                            Text="Add Document" Style="display: none;" />
                                        <asp:Button ID="btnAddDocumentsNew" runat="server" OnClientClick="$('#ModalFileUpload').modal('show');return false;" CssClass="btn btn-primary" CausesValidation="False"
                                            Text="Add Document" />
                                        <asp:Button ID="btnTableLayout" runat="server" Style="display: none;" OnClick="btnTableLayout_Click" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div style="width: auto; height: auto; overflow: auto;">
                                            <asp:GridView ID="gvwDocs" runat="server" CssClass="table dataTable table-striped" Width="99%" PageSize="20" AutoGenerateColumns="False">
                                                <%--   <RowStyle CssClass="rowstyle" />
                                                <AlternatingRowStyle CssClass="altrowstyle" />
                                                <HeaderStyle CssClass="headerstyle" />--%>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnPrntDocs" CssClass="icon ti-eye" runat="server" />
                                                            <%--  <asp:ImageButton ID="btnPrntDocs" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png"
                                                                                                    runat="server" />--%>
                                                        </ItemTemplate>
                                                        <ItemStyle Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="DateCreated" HeaderText="Date Added" SortExpression="DateCreated">
                                                        <ItemStyle Wrap="False"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UserCode" HeaderText="User" SortExpression="UserCode">
                                                        <ItemStyle Wrap="False"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DocumentType" HeaderText="Type" SortExpression="DocumentType">
                                                        <ItemStyle Wrap="False"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DocumentDescription" HeaderText="Description" SortExpression="DocumentDescription">
                                                        <ItemStyle Wrap="True"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountDoc(this)" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <b>No Items found for the specified criteria</b>
                                                </EmptyDataTemplate>
                                                <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: block">


                                <div style="float: left; margin-top: 30px; height: 100%;">
                                    <ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1" runat="server" BehaviorID="FileUpLoad"
                                        TargetControlID="btnAddDocuments" PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground"
                                        OkControlID="" />
                                    <asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none"
                                        Width="452px" Height="100px">
                                        <h1 class="panelheader">
                                            <div style="width: 100%;">
                                                <div style="text-align: center; width: 90%; float: left;">
                                                    Upload Documents
                                                </div>
                                                <div style="float: right; width: 10%;">
                                                    <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="Return" Width="40px" CssClass="button" />
                                                </div>
                                            </div>
                                        </h1>
                                        <iframe runat="server" id="frm" src="App_Controls/_Custom/LienView/Default.aspx?id=1&jobid=0"
                                            style="width: 450px; height: 270px; background-color: White"></iframe>
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" Style="text-align: left;" HeaderText="Please Correct the Following Field(s): " />
                <cc1:CustomValidator ID="customvalidator1" runat="server">
                </cc1:CustomValidator>
                
            </div>
            <div class="footer">
                <asp:UpdatePanel ID="updPan1" runat="server">
                    <ContentTemplate>
                       <%-- ##1--%>
                        <%--<asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" CausesValidation="false" Text="Submit Job" />--%>
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" CausesValidation="false" Text="Submit Job"  TabIndex="22" />
                        <asp:Button ID="btnRedirectReportView" runat="server" Style="display: none;" OnClick="btnRedirectReportView_Click" />
                        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="TransparentGrayBackground">
                                </div>
                                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                    <div class="PageUpdateProgress">
                                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                                            AlternateText="[image]" />
                                        &nbsp;Please Wait...
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                    TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
                                    VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <asp:Button ID="btnExitViewer" runat="server" CssClass="btn btn-primary" Text="Exit Viewer"></asp:Button>
                <asp:Button ID="btnDownLoad" Text="DownLoad" CssClass="btn btn-primary" runat="server" />
                <br />
                <br />
                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                <br />
            </div>
        </asp:View>
    </asp:MultiView>
    <asp:Button ID="btngetstate" runat="server" Style="display: none;" />
    <asp:HiddenField ID="hdnLegalId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIsLegalParties" runat="server" Value="0" />
    <asp:Button ID="btnDeleteLegalId" runat="server" Style="display: none;" OnClick="btnDeleteLegalId_Click" />
    <%--    <asp:Button ID="btnOwnrSame" runat="server" Style="display: none;" CausesValidation="false" />--%>
</div>
<div class="modal fade modal-primary" id="ModalCustSearch" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">CUSTOMER SEARCH</h4>
                <asp:HiddenField runat="server" ID="hdnCustSearchClientID" Value="0" />
            </div>
            <div class="modal-body">
                <div style="display: flex;">
                    <div class="radio-custom radio-default radio-inline">
                        <input id="rbtnByName" type="radio" name="SearchName" checked="checked">
                        <label for="inputHorizontalMale">By Name</label>
                    </div>
                    <div class="radio-custom radio-default radio-inline">
                        <input id="rbtnByRef" type="radio" name="SearchName">
                        <label for="inputHorizontalMale">By Ref</label>
                    </div>
                    &nbsp;&nbsp;<input type="text" id="txtSearchStr" class="form-control" style="width: 40%;" onkeyup="SearchCustName()" />
                    &nbsp;&nbsp;<input type="button" value="Search" onclick="SearchCustName(); return false;" class="btn btn-primary" />
                </div>
                <div style="margin-top: 10px;">
                    <select size="4" id="listCustName" onchange="GetCustomer();" class="form-control"></select>
                    <%--<asp:ListBox runat="server" id="listCustName" Rows="4" style="width:100%;" ></asp:ListBox>--%>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade  modal-primary" id="ModalGCSearch" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <asp:HiddenField runat="server" ID="GCSearchClientID" />
                <h4 class="modal-title">GENERAL CONTRACTOR SEARCH</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Enter GC Name: </label>
                        <div class="col-md-5  col-sm-5 col-xs-5  col-sm-5 col-xs-5">

                            <input type="text" id="txtGCSearchText" class="form-control" onkeyup="GetGCNameList()" />

                        </div>
                        <div class="col-md-2  col-sm-2 col-xs-2">
                            <button id="btnGCSearch" onclick="GetGCNameList();return false;" class="btn btn-primary" value="Search">Search</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <select size="4" id="listGCNames" style="display: none;" onchange="GetGCDetails();" class="form-control"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                <%-- <a class="btn btn-default margin-0" data-dismiss="modal">&times;</a>--%>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="ModalFileUpload" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-center">
        <div class="modal-content" style="width:550px;">
            <div class="modal-header">
               <%-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">--%>
                    <button type="button" class="close" OnClick ="javascript:Clear()" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">UPLOAD DOCUMENT</h4>
            </div>
            <iframe id="iframe1" src="App_Controls/_Custom/LienView/Default.aspx?id=1&jobid=0" style="height: 540px; width: 550px; border: 0px;"></iframe>
            <div class="modal-footer" >
             
              <%--  <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">--%>
                <button type="button" class="btn btn-default margin-0" OnClick ="javascript:Clear()" aria-label="Close">
                                                <span aria-hidden="true">CLOSE</span> </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary example-modal-lg" id="ModaldivReportDetail" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">JOB DETAIL</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">CRF Solutions#: </label>
                        <div class="col-md-1 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblCRFSolutionId" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Job#: </label>
                        <div class="col-md-2  col-sm-2 col-xs-2 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblJobNumber" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Customer's Name: </label>
                        <div class="col-md-3 col-sm-3 col-xs-3 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblCustomerName" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Branch#: </label>
                        <div class="col-md-1 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblBranchNum" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Job Name: </label>
                        <div class="col-md-2  col-sm-2 col-xs-2 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblJobName" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Customer#: </label>
                        <div class="col-md-3 col-sm-3 col-xs-3 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblCustomerRef" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Return Receipt:</label>
                        <div class="col-md-1 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblReturnReceipt" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Job Add: </label>
                        <div class="col-md-2  col-sm-2 col-xs-2 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblJobAddress" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">General: </label>
                        <div class="col-md-3 col-sm-3 col-xs-3 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblGeneral" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Rush Order:</label>
                        <div class="col-md-1 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblRushOrder" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable"></label>
                        <div class="col-md-2  col-sm-2 col-xs-2 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblJobAddress2" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Owner: </label>
                        <div class="col-md-3 col-sm-3 col-xs-3 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblOwnerName" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable"></label>
                        <div class="col-md-1 labelAlignLeft control-label align-lable">

                            <asp:Label ID="Label2" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Job Amount: </label>
                        <div class="col-md-2  col-sm-2 col-xs-2 labelAlignLeft control-label align-lable">

                            <asp:Label ID="lblJobAmount" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable">Lender: </label>
                        <div class="col-md-3 col-sm-3 col-xs-3  control-label align-lable">

                            <asp:Label ID="lblLender" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable"></label>
                        <div class="col-md-1 labelAlignLeft control-label align-lable">

                            <asp:Label ID="Label1" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2 control-label align-lable"></label>
                        <div class="col-md-2  col-sm-2 col-xs-2 labelAlignLeft control-label align-lable">

                            <asp:Label ID="Label3" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2  col-sm-2 col-xs-2  col-sm-2 col-xs-2 control-label align-lable">Request Type:</label>
                        <div class="col-md-3 col-sm-3 col-xs-3 col-sm-3 col-xs-3 labelAlignLeft control-label align-lable align-lable">

                            <asp:Label ID="lblRequestType" runat="server"></asp:Label>

                        </div>
                    </div>


                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="Button1" runat="server" Text="Print" CssClass="btn btn-primary" OnClick="btnRedirectReportView_Click" />
                <asp:Button ID="btnClose" runat="server" class="btn btn-default margin-0" OnClick="btnRedirectNewJobList_Click" Text="Close" />
         
                 </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary" id="ModelCustSelect" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-center" style="width:300px">
        <div class="modal-content" style="height:150px">
           
           <div class="modal-body" >
          
               <h4>
                  Please Select Client#.
               </h4>
          
               </div>
             <div class="modal-footer" style="text-align: center;" >
                  <button type="button" class="btn btn-primary margin-0" data-dismiss="modal" style="width:70px">Ok</button>
                 </div>
            </div>
        </div>
    </div>

<asp:HiddenField ID="hdnIsFileUploadDeleted" runat="server" Value="0" />
<asp:HiddenField ID="hdnDocId" runat="server" Value="0" />
<asp:Button ID="btnDeleteDoc" runat="server" Style="display: none;" OnClick="btnDeleteDoc_Click" />

