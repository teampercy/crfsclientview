﻿Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Net.Mail
Imports System.Windows.Forms

Partial Class App_Controls__Custom_CollectView_AccountAddNote
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim MYITEM As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.DebtAccountNote
    Public Sub ClearData(ByVal debtaccountid As String)
        Session("debtaccountid") = debtaccountid
        Me.Note.Text = ""

    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim s As String = ""

    End Sub
    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Session("debtaccountid"))

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.DebtAccountNote
        With mynote
            .DebtAccountId = Session("debtaccountid")
            .NoteDate = Now()
            .Note = Me.Note.Text
            ' .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            '.ClientView = True
            .UserCode = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .NoteTypeId = 5

        End With

        CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Create(mynote)

        '10/21/2015 Email web notes to the collector
        Dim sDebtAccountId As String
        Dim myitem As New TABLES.DebtAccount
        sDebtAccountId = Session("debtaccountid")
        ProviderBase.DAL.Read(sDebtAccountId, myitem)

        Dim ssubject = "Web notation was been made on Debtor# " & sDebtAccountId
        Dim MYSQL As String = "Select LocationEmail from Location "
        MYSQL += " Where LocationId = " & myitem.LocationId
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        If MYDT.Tables.Count > 0 Then
            Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
            Dim result As String = EmailToUser(myview.RowItem("Locationemail"), ssubject)
            If (result = "Fail") Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "FailEmailSend", "alert('Email fail to send');", True)
            End If
        End If
        ClearData(Session("debtaccountid"))
        RaiseEvent ItemSaved()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModalAccountAddNote','hide');$('div.modal-backdrop').remove();", True)
    End Sub
    Public Shared Function EmailToUser(ByVal email As String, ByVal subject As String) As String

        Dim MYSITE As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, MYSITE)
        Dim msg As New MailMessage

        msg.From = New MailAddress("CRFIT@crfsolutions.com", "ClientView")
        msg.Subject = subject
        msg.To.Add(email)
        msg.Subject = subject
        msg.IsBodyHtml = True
        'msg.Body = body

        Try
            With MYSITE
                Dim _smtp As New SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
            End With
            Return "Success"
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            ' MsgBox("error on email:" & vbCrLf & mymsg)
            Return "Fail"
            'MessageBox.Show("Customer Not found" + ex.Message)

            'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "MyScript", "alert('Hello World');", True)
        End Try
        Return "Success"
    End Function
End Class
