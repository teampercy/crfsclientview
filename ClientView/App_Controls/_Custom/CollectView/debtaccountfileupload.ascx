<%@ Control Language="VB" AutoEventWireup="false" CodeFile="DebtAccountFileUpload.ascx.vb"
    Inherits="App_Controls__Custom_CollectView_DebtAccountFileUpload" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<meta http-equiv='cache-control' content='no-cache, no-store, must-revalidate' /> 
<meta http-equiv='expires' content='0'> 
<meta http-equiv='pragma' content='no-cache'> 

<script type="text/javascript">
    function setSelectedDocTypeId(id) {
        $('#' + '<%=hdnDocType.ClientID%>').val(id);
        $('#btnDropDocType').html(id + "<span class='caret'></span>");
        return false;
    }
    function EnableFileUploadButton() {
        document.getElementById('<%=btnFileUpLoadOk.ClientID%>').disabled = false;
    }
    function showLodingImage() {

        $('#loadingImageDiv').show();
    } // 5 seconds }
    
    function hideLodingImage() {

        var tmr = setInterval(function () { zzzhideLodingImageActul(tmr) }, 1000);
        //alert('hi..debtAccountfileupload');
       // parent.testjava();
    }
    function zzzhideLodingImageActul(tmr) { $('#loadingImageDiv').hide(); clearInterval(tmr); }
    function BindFileUoload() {        
        parent.BindFileUoloadList();
    }    
    function SetDatePicker() {
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            showButtonPanel: true,
        });
    }

    function uploadFileValidation() {
        debugger;
        // alert('call function');
        var fileUpload = document.getElementById('<%= FileUpLoad1.ClientID %>');
        var fileName = fileUpload.value;
        //alert(fileName);[a-zA-Z0-9\s_\\.\-:\(\)\�(� �)�]
        if (fileName.search(/[<>'\+\"\/;`%,#$&@@]/) > 0) {
            alert('Please upload the file without special characters');
            //$('#btnFileUpLoadOk').prop('disabled', true);
            document.getElementById('<%=btnFileUpLoadOk.ClientID%>').disabled = true;
           // $(':input[type="submit"]').prop('disabled', true);
            return false;
        }
        else {
            //alert('The file "' + fileName + '" has been selected.');
           // $(':input[type="submit"]').prop('disabled', false);
            document.getElementById('<%=btnFileUpLoadOk.ClientID%>').disabled = false;
            // $('#btnFileUpLoadOk').prop('disabled', false);
            //$("#uploadedFileName").empty();
            // $("#uploadedFileName").append(fileName);
        }
    }
</script>

<style>
    .dropify-wrapper {
        height: 160px;
    }
        .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
    }
</style>
<asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Width="100%">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-xs-4  control-label text-right">Document Date:</label>
            <div class="col-xs-4" style="display: flex;">
                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control datepicker"></asp:TextBox>

            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Document Type:</label>

            <div class="col-xs-8" style="display: flex;">
                <div class="btn-group" style="text-align: left;" align="left">
                    <button type="button" class="btn btn-default dropdown-toggle" style="min-width: 200px;" align="left"
                        data-toggle="dropdown" aria-expanded="false" id="btnDropDocType">
                        --Select--
                        <span class="caret" style="float: right; margin-top: 5px;"></span>
                    </button>
                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState" style="overflow-y: auto; height: 250px;">
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Contract')">Contract</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Dispute Docs')">Dispute Docs</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Invoices')">Invoices</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Misc')">Misc</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Personal Guarantee')">Personal Guarantee</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Proof Of Debt')">Proof Of Debt</a></li>
                    </ul>
                </div>
                <asp:HiddenField ID="hdnDocType" runat="server" Value="--Select--" />
                <asp:DropDownList ID="ddldoctype" runat="server" Width="100%" Style="display: none;">
                    <asp:ListItem Text="--SELECT--" Selected="true"></asp:ListItem>
                    <asp:ListItem Text="Contract"></asp:ListItem>
                    <asp:ListItem Text="Dispute Docs"></asp:ListItem>
                    <asp:ListItem Text="Invoices"></asp:ListItem>
                    <asp:ListItem Text="Misc"></asp:ListItem>
                    <asp:ListItem Text="Personal Guarantee"></asp:ListItem>
                    <asp:ListItem Text="Proof Of Debt"></asp:ListItem>

                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Misc Info:</label>
            <div class="col-xs-8">
                <asp:TextBox ID="Txtmiscinfo" runat="server" TextMode="MultiLine" Font-Names="Arial" Font-Size="small" Width="100%"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-4 text-right control-label">
                FIle to Upload:               
            </div>
                   <div class="col-xs-8">
                 <%--<asp:FileUpload ID="FileUpLoad1" AlternateText="You cannot upload files" runat="server"
                        size="40" Height="25px" Visible="false" />--%>
                <asp:FileUpload id="FileUpLoad1" runat="server" AllowMultiple="true" onChange="uploadFileValidation()" />
                  <%--<asp:RegularExpressionValidator id="FileUpLoadValidator" runat="server" ErrorMessage="Select Valid File." ValidationExpression="^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w].*))(.jpg|.jpeg|.gif|.png|.bmp|.doc|.docx|.txt|.csv|.xls|.zip|.pdf|.xlsx)$"  ControlToValidate="FileUpload1">  
</asp:RegularExpressionValidator> --%>
                <asp:RequiredFieldValidator 
             ID="RequiredFieldValidator1"
             runat="server"
             ControlToValidate="FileUpload1"
           
             />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:\(\)\�(� �)�])+(.jpg|.jpeg|.gif|.png|.bmp|.doc|.docx|.txt|.csv|.xls|.zip|.pdf|.xlsx|.PDF|.JPG|.JPEG|.GIF|.PNG|.BMP|.DOC|.DOCX|.TXT|.CSV|.XLS|.ZIP|.XLSX)$"
                 ControlToValidate="FileUpload1" runat="server" ForeColor="Red" ErrorMessage="Please select a valid  File"
    Display="Dynamic" />
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" style="margin-left: -86px;" ValidationExpression ="^([1-9][0-9]{0,5}|[12][0-9]{6}|4(0[0-9]{5}|1([0-4][0-9]{4}|4([0-4][0-9]{4}|5([0-6][0-9]{2}|7([01][0-9]|2[0-8]))))))$" ErrorMessage="File is too large, must select file under 4 Mb." ControlToValidate="FileUpload1" runat="server"></asp:RegularExpressionValidator>
                   --%> 
                <div id="fileList"></div>
                   
                                                                                                                                          
                <%--<asp:Button id="UploadBtn" Text="Upload File" OnClick="UploadBtn_Click" runat="server" Width="105px" />  
   --%>

             <%--   <input id="UploadedFile" name="fileDragnDrop" data-max-file-size="4M" data-plugin="dropify" type="file" data-max-file-limit="1">--%>

              <%-- <span class="btn btn-default btn-file btn_block" style="width: 340px;">
    Select File... <input type="file" id="UploadedFile" multiple ><%--<asp:FileUpload ID="UploadedFile" runat="server" AllowMultiple="true" /> --%> 
<%--</span>--%>

            </div>
         <%--   <div class="col-xs-8">
                <%-- <asp:FileUpload ID="FileUpLoad1" AlternateText="You cannot upload files" runat="server"
                        size="40" Height="25px" Visible="false" />
                --%>


             <%--   <input id="UploadedFile" name="fileDragnDrop" data-max-file-size="4M" data-plugin="dropify" type="file" data-max-file-limit="1">--%>

              <%-- <span class="btn btn-default btn-file btn_block" style="width: 340px;">
    Select File... <input type="file" id="UploadedFile" data-max-file-limit="1" >
</span>--%>

          <%--  </div>--%>

        </div>
        <div class="form-group">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-8" style="text-align: left; text-wrap: normal; font-weight: normal">
                <%--<asp:Label ID="Label3" runat="server"
                    Text="Attached file cannot be larger than 4mbs. "
                    ForeColor="Red" />--%>
                <asp:Label ID="Label1" runat="server"
                    Text="Attached file cannot be larger than 4mbs.If your attachment is larger,please break it up into multiple files and attach them separately."
                    ForeColor="Red" />
                <br />

                <label class="msg row-label" style="text-align: left; padding: 0px">Select File to Upload: (PDF,DOC,TXT,CSV,XLS,JPG,GIF,BMP,ZIP Only)</label>


            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="file-wrap container-fluid">
                    <div class="file-list row"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-4">
            </div>
       <%--     <div class="col-xs-8">
                <asp:Button ID="btnFileUpLoadOk" ClientIDMode="Static" runat="server" Style="float: left" CausesValidation="true"
                    Text="Save Document" CssClass="btn btn-primary" Enabled="false" />

            </div>--%>
                   <div class="col-xs-8">
                <asp:Button ID="btnFileUpLoadOk" ClientIDMode="Static" runat="server" Style="float: none" Enabled="true" CausesValidation="true"
                    OnClick="btnFileUpLoadOk_Click" OnClientClick="showLoadingImage()" Text="Save Document" CssClass="btn btn-primary" />

            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-8">
                <div>
                    <asp:LinkButton ID="LinkButton1" runat="server" CssClass="close" OnClientClick="$find('FileUpLoad').hide(); return false;" />
                    <%--<asp:Label ID="Label1" runat="server" CssClass="msg" Text="Upload" />--%>
                </div>
                <div style="padding-left: 10px; text-align: left">
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDate"
                        ErrorMessage="Enter Date in MM/DD/YYYY Format" Operator="DataTypeCheck" Type="Date" />

                    <br />
                    <asp:Label ID="lblmsg" runat="server" ForeColor="red"></asp:Label>
                </div>
            </div>
        </div>
    </div>



    <%--    <div style="text-align: left">
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUpLoad1"
            Text="Select File To Upload"></asp:RequiredFieldValidator>
    </div>--%>


    <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script>

        $(function () {
debugger;
            //$('#UploadedFile').change(function () {
            //    alert('UploadedFile manulay');

            //});

            $("#btnFileUpLoadOk").click(function (e) {
                var Flag = true;
                var ValidationMessage = "";
                if ($("#<%=hdnDocType.ClientID%>").val() == '--Select--') {
                    Flag = false;
                    ValidationMessage = " Select Document Type<br>";
                }
                $.ajax({
                    type: "POST",
                    url: "Default.aspx/GetFileUploadExtension",
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    dataType: "json",
                    success: function (response) {
                        if (response.d == "") {
                            Flag = false;
                            ValidationMessage = ValidationMessage + " Select File to upload";
                        }
                        else {
                            var AllowedExt = new Array("jpg", "jpeg", "gif", "png", "bmp", "doc", "docx", "txt", "csv", "xls", "zip", "pdf", "xlsx");
                            var ext = response.d;
                            ext = ext.substring(1);
                            if (AllowedExt.indexOf(ext.toLowerCase()) < 0) {
                                Flag = false;
                                ValidationMessage = ValidationMessage + " Select Valid File<br>";
                            }
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                        Flag = false;
                    }
                });
                $("#<%= lblmsg.ClientID%>").html(ValidationMessage);
                if (Flag) {
                    showLodingImage();
                }
                else {
                    hideLodingImage();
                }
                return Flag;
            });
        })

    </script>



</asp:Panel>
