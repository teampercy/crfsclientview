﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewAccount_New.ascx.vb" Inherits="App_Controls__Custom_CollectView_NewAccount_New" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<script language="javascript" type="text/javascript">
    function close() {
        window.close();
    }


</script>

<div id="modcontainer" style="width: 700px">
    <h1 class="panelheader">New Account</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">
                <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to List"
                    CausesValidation="False" /><asp:HiddenField runat="server" ID="hddbTabName" Value="MainInfo" />
                <br />
                <br />
                <div class="nav-tabs-horizontal">
                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                        <li id="MainInfo" class="active" role="presentation"><a data-toggle="tab" onclick="SetTabName('MainInfo');" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Main Info</a></li>
                        <li id="Docs" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Docs');" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Docs</a></li>
                    </ul>
                    <div class="tab-content padding-top-20">
                        <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Client#:</label>
                                    <div class="col-md-7">
                                        <cc1:ExtendedDropDown ID="ClientTableId" CssClass="dropdown" runat="server" Width="296px"
                                            AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                            IsValid="True" Tag="">
                                        </cc1:ExtendedDropDown>
                                        <asp:CheckBox ID="IsTenDayContactPlan" CssClass="redcaption" runat="server" Text="  10 Day Free Demand"
                                            Width="150px" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account#:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="AccountNo" ErrorMessage="Account#" Width="200" DataType="Any"
                                            IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Branch/Region#:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="BranchNum" ErrorMessage="Region#" Width="200" DataType="Any"
                                            IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Debtor Name:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="DebtorName" ErrorMessage="Name" Width="200" DataType="Any"
                                            IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Contact Name:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="ContactName" ErrorMessage="ContactName" Width="200"
                                            DataType="Any" IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Address 1:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="AddressLine1" ErrorMessage="Addr#1" Width="200" DataType="Any"
                                            IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Address 2:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="AddressLine2" ErrorMessage="Addr#2" Width="200" DataType="Any"
                                            IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">City:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="City" ErrorMessage="City" Width="200" MaxLength="20"
                                            DataType="Any" IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">State:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="State" ErrorMessage="State" MaxLength="2"
                                            DataType="Any" IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Zip Code:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="PostalCode" ErrorMessage="ZipCode" MaxLength="10"
                                            DataType="Any" IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Last Charge Date:</label>
                                    <div class="col-md-7">
                                        <span style="display: flex;">
                                            <SiteControls:Calendar ID="LastServiceDate" Value="TODAY" IsReadOnly="false" IsRequired="true"
                                                ErrorMessage="Last Service" runat="server" />
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Balance:</label>
                                    <div class="col-md-7">
                                        <cc1:DataEntryBox ID="TotAsgAmt" runat="server" CssClass="form-control" DataType="Money"
                                            IsRequired="False" Tag="Balance" Align="right" Width="85px" ErrorMessage="Balance">$0.00</cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Phone#:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="PhoneNo" ErrorMessage="Phone#" Width="85px" DataType="PhoneNo" CSSClass="form-control"
                                            IsRequired="FALSE" IsMasked="true" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Fax:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="Fax" ErrorMessage="Fax#" Width="85px" DataType="PhoneNo" CSSClass="form-control"
                                            IsRequired="false" IsMasked="true" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Email:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="Email" ErrorMessage="Email" Width="200" DataType="Any" CSSClass="form-control"
                                            IsRequired="FALSE" IsMasked="False" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Special Instruction:</label>
                                    <div class="col-md-7">
                                        <SiteControls:DataEntryBox ID="DebtAccountNote" ErrorMessage="SpecialInstructions"
                                            Width="300" TextMode="MultiLine" MaxLength="255" DataType="Any" IsRequired="false"
                                            IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div style="display: block">
                                        <div style="margin: 2px 0px 5px 0px;">
                                            <asp:Button ID="btnAddDocuments" runat="server" CssClass="btn btn-primary" CausesValidation="False"
                                                Text="Add Document" Visible="false"/>
                                            <asp:Button ID="btnAddDocumentsNew" runat="server" OnClientClick="$('#ModalFileUpload').modal('show');return false;" CssClass="btn btn-primary" CausesValidation="False"
                                                Text="Add Document" />
                                        </div>
                                        <asp:GridView ID="gvwDocs" runat="server" CssClass="table dataTable table-striped" Width="650px" PageSize="20"
                                            AutoGenerateColumns="False">
                                            <AlternatingRowStyle CssClass="altrowstyle"></AlternatingRowStyle>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Print">
                                                    <ItemTemplate>
                                                        &nbsp;&nbsp;&nbsp;
                                                          <asp:LinkButton ID="btnPrntDocs" CausesValidation="false"  CssClass="icon ti-printer" runat="server" />
                                                    <%--    <asp:ImageButton ID="btnPrntDocs" CausesValidation="false" CssClass="gridbutton"
                                                            ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" Width="15px" />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle Wrap="False" Width="40px"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="DateCreated" HeaderText="Date Added" SortExpression="DateCreated">
                                                    <ItemStyle HorizontalAlign="Center" Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="UserCode" HeaderText="User" SortExpression="UserCode">
                                                    <ItemStyle Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DocumentType" HeaderText="Type" SortExpression="DocumentType">
                                                    <ItemStyle Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DocumentDescription" HeaderText="Description" SortExpression="DocumentDescription">
                                                    <ItemStyle Wrap="False" Height="15px" Width="200px"></ItemStyle>
                                                </asp:BoundField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <b>No Items found for the specified criteria</b>
                                            </EmptyDataTemplate>
                                            <%-- <HeaderStyle CssClass="headerstyle"></HeaderStyle>
                                            <RowStyle CssClass="rowstyle"></RowStyle>--%>
                                        </asp:GridView>
                                        <div style="float: left; margin-top: 30px; height: 100%;">

                                            <ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1" runat="server" BehaviorID="FileUpLoad"
                                                TargetControlID="btnAddDocuments" PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground"
                                                OkControlID="" />
                                            <asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none"
                                                Width="452px" Height="100px">
                                                <h1 class="panelheader">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <div style="width: 100%;">
                                                                <div style="text-align: center; width: 90%; float: left;">
                                                                    Upload Documents
                                                                </div>
                                                                <div style="float: right; width: 10%;">
                                                                    <asp:Button ID="btnFileUpLoadCancel1" runat="server" Text="Return" CausesValidation="False"
                                                                        Width="40px" CssClass="btn btn-primary" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </h1>
                                                <iframe runat="server" id="frm" src="App_Controls/_Custom/LienView/Default.aspx?id=0&debtaccountid=0"
                                                    style="width: 450px; height: 270px; background-color: white"></iframe>
                                            </asp:Panel>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " />
                <cc1:CustomValidator ID="CustomValidator1" runat="server" Display="Static"></cc1:CustomValidator>
            </div>
            <div class="footer">
                <asp:UpdatePanel ID="updPan1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" CausesValidation="false"
                            Text="Submit Account" />&nbsp;
                        <br />
                        <br />
                        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="TransparentGrayBackground">
                                </div>
                                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                    <div class="PageUpdateProgress">
                                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                                            AlternateText="[image]" />
                                        &nbsp;Please Wait...
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                    TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
                                    VerticalSide="Middle" VerticalOffset="0">
                                </ajaxToolkit:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                &nbsp;<asp:LinkButton ID="btnExitViewer" runat="server" CssClass="btn btn-primary" Text="Exit Viewer"></asp:LinkButton>
                &nbsp;
                <asp:LinkButton ID="btnDownLoad" Text="DownLoad" CssClass="btn btn-primary" runat="server" />
                <br />
                <br />
                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                <br />
                <center>
                </center>
            </div>
        </asp:View>
    </asp:MultiView>
</div>

<div class="modal fade modal-primary" id="ModalFileUpload" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">UPLOAD DOCUMENTS</h4>
            </div>
            <iframe runat="server" id="Iframe1" src="App_Controls/_Custom/LienView/Default.aspx?id=0&debtaccountid=0"
               style="height:270px;width:550px;border:0px;"></iframe>
            <div class="modal-footer">             
                <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="Close" CausesValidation="False"
                                                                        Width="40px" CssClass="btn btn-default margin-0" />
            </div>
        </div>
    </div>
</div>
