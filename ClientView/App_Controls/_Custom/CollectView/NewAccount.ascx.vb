﻿Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Data


Public Class App_Controls__Custom_CollectView_NewAccount
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchDebtAccount
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim itemid As String
    Dim ContractId As Integer
    Protected WithEvents btnPrntDocs As LinkButton
    Protected WithEvents btnDeleteItem As LinkButton
    Dim myContractInfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientContract

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        itemid = qs.GetParameter("ItemId")
        Me.ValidationSummary1.Visible = False

        If Me.Page.IsPostBack = False Then
            Session("doc") = "0"
            Session("submit") = "0"
            Me.MultiView1.SetActiveView(Me.View1)
            If itemid > 0 Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("ItemId"), myitem)
            Else
                myitem.SubmittedByUserId = Me.MyPage.CurrentUser.Id
                myitem.BatchId = -2
                myitem.SubmittedOn = Now()
                myitem.ClientId = Me.UserInfo.Client.ClientId
                myitem.ContractId = Me.UserInfo.CollectionContractId.Id
                myitem.BatchType = "COLLWEB"
                CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Create(myitem)
            End If
            Dim li As New ListItem
            'myitem.State = "CA"
            'Me.hdnStateSelectedvalue.Value = "CA"
            Dim VWSTATES As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateTable
            VWSTATES.MoveFirst()
            Do Until VWSTATES.EOF
                li = New ListItem
                mystate = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
                mystate = VWSTATES.FillEntity(mystate)
                li.Value = mystate.StateInitials
                li.Text = mystate.StateName
                Me.StateTableId.Items.Add(li)
                VWSTATES.MoveNext()
            Loop
            ' FillClientListByUser()
            LoadItem()
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
        End If
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        Balance.Attributes.Add("onblur", "Balance_LostFocus();")
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.MyPage.CurrentUser.Id & ");", True)

    End Sub
   

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Session("submit") = "1"
        Session("doc") = "0"
        Me.Page.Validate()
        If Me.Page.IsValid = False Then
            Me.ValidationSummary1.Visible = True
            Exit Sub
        End If

        If Me.Balance.Value < 1 Then
            Me.ValidationSummary1.Visible = False
            Me.CustomValidator1.IsValid = False
            Me.CustomValidator1.ErrorMessage = "A Balance Is Required"
            Exit Sub
        End If

        With myitem
            .AccountNo = Me.AccountNo.Value
            .DebtorName = Me.DebtorName.Value
            .ContactName = Me.ContactName.Value
            .BranchNum = Me.BranchNum.Value
            .AddressLine1 = Me.AddressLine1.Value
            .AddressLine2 = Me.AddressLine2.Value
            .City = Me.City.Value
            .State = Me.State.Value
            .State = Me.hdnStateSelectedvalue.Value
            .PostalCode = Me.PostalCode.Value
            .PhoneNo = Utils.GetPhoneNo(Me.PhoneNo.Value)
            .Fax = Utils.GetPhoneNo(Me.Fax.Value)
            .Email = Me.Email.Value
            .ReferalDate = Today
            .LastServiceDate = Utils.GetDate(Me.LastServiceDate.Value)
            .AccountNote = Me.DebtAccountNote.Value
            .TotAsgAmt = Me.Balance.Value
            .IsTenDayContactPlan = Me.IsTenDayContactPlan.Checked
            .SubmittedByUserId = Me.MyPage.CurrentUser.Id
            .BatchId = -1
            .SubmittedOn = Now()
            .ClientId = hdnClientSelectedId.Value
            ' .ClientId = ClientTableId.SelectedValue 'Commented by Jaywanti

            .ContractId = GetContractId()
            .BatchType = "COLLWEB"

        End With

        If Me.ViewState("BatchDebtAccountId") > 0 Then
            myitem.BatchDebtAccountId = Me.ViewState("BatchDebtAccountId")
            CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Update(myitem)
        Else
            CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Create(myitem)
        End If
        Me.lblClientNumber.Text = hdnClientSlectedIdText.Value
        Me.lblDebtName.Text = Me.DebtorName.Value
        Me.lblAdrress1.Text = Me.AddressLine1.Value
        Me.lblAccountNum.Text = Me.AccountNo.Value
        Me.lblContactName.Text = Me.ContactName.Value
        Me.lblAddress2.Text = Me.City.Value & "," & Me.State.Value & " " & Me.PostalCode.Value
        Me.lblBranchRegion.Text = Me.BranchNum.Value
        Me.lblLastChargedate.Text = Utils.GetDate(Me.LastServiceDate.Value)
        Me.lblBalance.Text = "$" & Me.Balance.Value
        Me.lblPhoneNum.Text = Utils.GetPhoneNo(Me.PhoneNo.Value)
        Me.lblFax.Text = Utils.GetPhoneNo(Me.Fax.Value)
        If (Me.lblPhoneNum.Text = "0000000000") Then
            Me.lblPhoneNum.Text = ""
        End If
        If (Me.lblFax.Text = "0000000000") Then
            Me.lblFax.Text = ""
        End If
        Me.lblEmail.Text = Me.Email.Value
        Me.lblSpecialInstruction.Text = Me.DebtAccountNote.Value
        Dim sreport As String = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetAckReport(myitem.BatchDebtAccountId, True)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            ''Me.MultiView1.SetActiveView(Me.View2) ''Comment by Kedar Hatkar 12 Jan 2016
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            ''Me.MultiView1.SetActiveView(Me.View2) ''Comment by Kedar Hatkar 12 Jan 2016
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "AlertSuccessFunc();", True)

    End Sub

    Private Sub FillClientListByUser()


        Dim dt As DataTable = DirectCast(CRFS.ClientView.CrfsBll.GetClientListForUser(Me.MyPage.CurrentUser.Id), DataSet).Tables(0)
        dt.DefaultView.Sort = "ClientName"
        ClientTableId.DataSource = dt
        ClientTableId.DataTextField = "ClientName"
        ClientTableId.DataValueField = "ClientId"
        ClientTableId.DataBind()


    End Sub

    Private Function GetContractId() As Integer
        Dim dt As New DataTable
        'dt = CRFS.ClientView.CrfsBll.GetContractInfoByClientId(ClientTableId.SelectedValue) 'Commented by Jaywanti
        dt = CRFS.ClientView.CrfsBll.GetContractInfoByClientId(hdnClientSelectedId.Value)
        Return dt.Rows(0)("Id")
    End Function

    Private Sub LoadItem()
        With myitem
            Me.AccountNo.Value = .AccountNo
            Me.DebtorName.Value = .DebtorName
            Me.ContactName.Value = .ContactName
            Me.BranchNum.Value = .BranchNum
            Me.AddressLine1.Value = .AddressLine1
            Me.AddressLine2.Value = .AddressLine2
            Me.City.Value = .City
            Me.State.Value = .State
            Me.hdnStateSelectedvalue.Value = .State
            Me.PostalCode.Value = .PostalCode
            Me.PhoneNo.Value = Utils.FormatPhoneNo(.PhoneNo)
            Me.Fax.Value = Utils.FormatPhoneNo(.Fax)
            Me.Email.Value = .Email
            Me.LastServiceDate.Value = .LastServiceDate
            Me.DebtAccountNote.Value = .AccountNote
            'Me.Balance.Value = Utils.GetDecimal(Me.Balance.Value)
            Me.Balance.Value = .TotAsgAmt
            Me.ClientTableId.SelectedValue = .ClientId
            hdnClientSelectedId.Value = .ClientId
            Me.IsTenDayContactPlan.Checked = .IsTenDayContactPlan
        End With
        If Me.hdnStateSelectedvalue.Value = "" Then
            Me.State.Value = "CA"
            Me.hdnStateSelectedvalue.Value = "CA"
        End If
        '   4/9/2013
        myContractInfo = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetClientContractInfo(myitem.ContractId)
        If myContractInfo.IsTenDayContactPlan = True Then
            Me.IsTenDayContactPlan.Visible = True
        Else
            Me.IsTenDayContactPlan.Visible = False
        End If

        Me.ViewState("BatchDebtAccountId") = myitem.BatchDebtAccountId
        'markparimal-uploadfile:BatchDebtAccountId value is taken in session so that it can be used in file upload page.
        Session("BatchDebtAccountId") = myitem.BatchDebtAccountId
        'Me.Page.Validate()

        'markparimal-uploadfile:To fill Grid on page load .
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where BatchDebtAccountId = " & myitem.BatchDebtAccountId
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()

    End Sub

    Private Sub SaveItem()

    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?collectview.newaccountlist"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub

    Protected Sub gvwDocs_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntDocs"), LinkButton).Click, AddressOf btnPrntDocs_Click
        End If
    End Sub
    Protected Sub gvwDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            btnPrntDocs.Attributes("rowno") = DR("Id")
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteItem.Attributes("rowno") = DR("Id")
            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
        End If
    End Sub

    Protected Sub btnPrntDocs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntDocs.Click
        'markparimal-uploadfile:To display selected upload file .converts file from byte 

        btnPrntDocs = TryCast(sender, LinkButton)
        'Me.ViewState("VIEW") = "Docs"
        Session("doc") = "1"
        Session("submit") = "0"


        Dim myitem As New TABLES.DebtAccountAttachments
        ProviderBase.DAL.Read(btnPrntDocs.Attributes("rowno"), myitem)

        Dim sreport As String = "~/UserData/output/" & myitem.Id & "-" & myitem.FileName
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.DocumentImage, Me.MapPath(sreport))

        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)

                Dim firstpart As String = Filepath.Substring(0, Filepath.LastIndexOf("/"))
                Dim lastpart As String = Filepath.Substring(Filepath.LastIndexOf("/") + 1)
                'lastpart = HttpUtility.UrlEncode(lastpart)  'Remove # with %23 (Html Encoding UTF-8)
                ' lastpart = HttpUtility.UrlPathEncode(lastpart)  'Remove space with %20
                lastpart = Uri.EscapeDataString(lastpart) 'Remove # with %23 , space with %20

                Filepath = firstpart + "/" + lastpart

                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        Me.hddbTabName.Value = "Docs"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub
 

    Protected Sub btnFileUpLoadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadCancel.Click
        'markparimal-uploadfile:To refresh Grid values .
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where BatchDebtAccountId = " & Session("BatchDebtAccountId")
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModalFileUpload','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
    End Sub

    Protected Sub btnExitViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitViewer.Click
        If Session("submit") = "1" Then
            Response.Redirect("~/MainDefault.aspx?collectview.newaccountlist")
        End If

        If Session("doc") = "1" Then
            Me.MultiView1.SetActiveView(Me.View1)
            'tabnewcollectacc.ActiveTabIndex = 1//Commented by Jaywanti on 08-12-2015
            '' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        Else
            Me.MultiView1.SetActiveView(Me.View1)
            ' tabnewcollectacc.ActiveTabIndex = 0//Commented by Jaywanti on 08-12-2015

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub

    Protected Sub ClientTableId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClientTableId.SelectedIndexChanged
    End Sub

    Protected Sub btnRedirectReportView_Click(sender As Object, e As EventArgs)
        Dim result As Boolean = False
        If Me.ViewState("BatchDebtAccountId") > 0 Then
            myitem.BatchDebtAccountId = Me.ViewState("BatchDebtAccountId")
            Dim sreport As String = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetAckReport(myitem.BatchDebtAccountId, True)
            Me.btnDownLoad.Visible = False
            If IsNothing(sreport) = False Then
                Me.btnDownLoad.Visible = True
                Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                        result = True
                    End If
                End If
                'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
                'Me.MultiView1.SetActiveView(Me.View2) ''added by Kedar Hatkar 12 Jan 2016
            Else
                Me.plcReportViewer.Controls.Clear()
                Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
                Me.MultiView1.SetActiveView(Me.View2) ''added by Kedar Hatkar 12 Jan 2016
            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsHideModalPopup", "HideModalPopup();", True)
        'If result = True Then
        '    Dim d As String = "~/MainDefault.aspx?collectview.newaccountlist"
        '    Response.Redirect(Me.Page.ResolveUrl(d), False)
        'End If
       
    End Sub


    Protected Sub btnClose_Click(sender As Object, e As EventArgs)
        Dim s As String = "~/MainDefault.aspx?collectview.newaccountlist"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub

    Protected Sub btnDeleteDoc_Click(sender As Object, e As EventArgs)
        Dim myitem As New TABLES.DebtAccountAttachments
        myitem.Id = hdnDocId.Value
        ProviderBase.DAL.Delete(myitem)
        Me.hdnIsFileUploadDeleted.Value = "1"
        Me.hddbTabName.Value = "Docs"
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where BatchDebtAccountId = " & Session("BatchDebtAccountId")
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)

    End Sub

End Class
