<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AccountInfo.ascx.vb" Inherits="App_Controls__Custom_CollectView_AccountInfo" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Register Src="~/App_Controls/_Custom/CollectView/AccountAddNote.ascx" TagPrefix="SiteControls" TagName="AccountAddNote" %>


<script type="text/javascript">
    $(document).ready(function () {
        // alert('hi');
        SetHeaderBreadCrumb('CollectView', 'Accounts', 'View Accounts');
        CallSuccessFuncNotesList();
        CallSuccessFuncAccountingList();
        CallSuccessFuncDocList();
    });


    //'^^^***CreatedBy Pooja   10/13/20 *** ^^^

    function Clear() {
        debugger;

        $("#Iframe1").attr("src", "App_Controls/_Custom/LienView/Default.aspx?id=1&jobid=0");

        $("#ModalFileUpload").modal('hide');

    }
    //'^^^***      *** ^^^
    function DisplayNotePopUp() {
        // alert('hi');
        $("#ModalJobNoteDetail").modal('show');
    }

    function HideNotePopUp() {
        $("#ModalJobNoteDetail").modal('hide');

        CallSuccessFuncNotesList();
    }
    function BindFileUoloadList() {
        document.getElementById('<%=btnFileUpLoadCancel.ClientID%>').click();
    }
      function SetNoteId(id, Type) {
             debugger;
        document.getElementById('<%=hdnNoteType.ClientID%>').value = Type;
        document.getElementById('<%=hdnBatchNoteId.ClientID%>').value = $(id).attr("rowno");
        document.getElementById('<%=btnNoteDetail.ClientID%>').click();
        return false;
    }
    function CallSuccessFuncNotesList() {
        debugger;
        //alert('1');
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwNotes.ClientID%>').dataTable(options);
    }
    function CallSuccessFuncAccountingList() {
        //alert('2');
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwPayments.ClientID%>').dataTable(options);
    }
    function CallSuccessFuncDocList() {
        //alert('3');
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwDocs.ClientID%>').dataTable(options);
    }

    function MaintainMenuOpen() {
        MainMenuToggle('liAccounts');
        SubMenuToggle('liViewAccounts');
        //$('#liAccounts').addClass('site-menu-item has-sub active open');
        //$('#liViewAccounts').addClass('site-menu-item active');
        return false;

    }

    function SetTabName(id) {
        document.getElementById('<%=hddbTabName.ClientID%>').value = id;
    }
    function ActivateTab(TabName) {
        //alert('hello');
        //alert(TabName);
        document.getElementById('<%=hddbTabName.ClientID%>').value = TabName;
        switch (TabName) {
            case "Notes":
                if (!$('#' + '<%=Notes.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Notes.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsOne').hasClass('active'))
                    $('#exampleTabsOne').addClass('active');
                $('#' + '<%=Accounting.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=InsuranceInfo.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                break;
            case "Accounting":
                if (!$('#' + '<%=Accounting.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Accounting.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsTwo').hasClass('active'))
                    $('#exampleTabsTwo').addClass('active');
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=InsuranceInfo.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                break;
            case "Docs":
                if (!$('#' + '<%=Docs.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Docs.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsThree').hasClass('active'))
                    $('#exampleTabsThree').addClass('active');
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=Accounting.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=InsuranceInfo.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                break;
            case "InsuranceInfo":
                if (!$('#' + '<%=InsuranceInfo.ClientID%>').hasClass('active')) {
                    $('#' + '<%=InsuranceInfo.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsFour').hasClass('active'))
                    $('#exampleTabsFour').addClass('active');
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=Accounting.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                break;

        }
    }
    function DeleteAccount(id) {
        document.getElementById('<%=hdnDocId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=hddbTabName.ClientID%>').value = "Docs";
          document.getElementById('<%=btnDeleteDoc.ClientID%>').click();
      });
        return false;
    }
    function Successpopup() {
        ActivateTab('Docs');
    }
    function hideDiveNotes()
    {
       // alert("hi");
        $('#divnotes').hide();
        $('#divAddDoc').hide();
    }
  
</script>

<div id="modcontainer" style="WIDTH: 100%;">
    <h1 class="panelheader">Account Form</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-3" style="text-align: left; width: 112px;">
                            <asp:Button ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to List" CausesValidation="False" style="float :left"/>
                             </div>
                            <div class="col-md-3" style="text-align: left; width: 120px;">
                              <asp:Button ID="btnBackFilter" CssClass="btn btn-primary" runat="server" Text="Back to Filter" CausesValidation="False" style="float :left" />
                        </div>
                         <div class="col-md-3" style="text-align: left;">
                             <asp:Button ID="btnCollectorInfo"  runat="server"  style="display :none " />
                              <input type ="button" ID="btnCollector" Class="btn btn-primary"  Value="Collector Info" OnClick="OpenAddCollectorInfoModal();"  />
                        </div>
                       
                    </div>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="col-md-4 control-label">
                                <asp:Literal ID="litAccountNo" runat="server" Text="Account#:"></asp:Literal></label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="AccountNo" runat="server" CssClass="form-control" Width="90px" ReadOnly="True" DataType="Any"></cc1:DataEntryBox>

                            </div>
                            <label class="col-md-2 control-label">
                                <asp:Label ID="lblaccno" runat="server" Text="Region#:" Style="width: 100px; margin-left: 10px;"></asp:Label></label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="BranchNum" runat="server" CssClass="form-control" DataType="Any" Width="90px" Style="margin-left: 15px;"
                                    ReadOnly="True"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <label class="col-md-3 control-label">File#:</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="FileNumber" runat="server" CssClass="form-control" ReadOnly="True" DataType="Any" Width="120px"
                                    tag="" Style="text-align: center"></cc1:DataEntryBox>
                            </div>
                            <label class="col-md-3 control-label">Assigned</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="TotAsgAmt" runat="server" CssClass="form-control"
                                    DataType="Money" ReadOnly="True" tag="TotAsgAmt" Style="text-align: right" Width="110px">$0.00</cc1:DataEntryBox>
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="col-md-4 control-label">
                                <asp:Literal ID="litDetorName" runat="server" Text="Name:"></asp:Literal></label>
                            <div class="col-md-8">
                                <cc1:DataEntryBox ID="DebtorName" runat="server" CssClass="form-control" ReadOnly="True" tag="DebtorName" DataType="Any"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <label class="col-md-3 control-label">
                                Client #:</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="ServiceCode" runat="server" CssClass="form-control" ReadOnly="True" DataType="Any"
                                    Style="text-align: center" Width="120px"></cc1:DataEntryBox>
                            </div>
                            <label class="col-md-3 control-label">
                                Adjusted:</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="TotAdjAmt" runat="server" CssClass="form-control"
                                    DataType="Money" ReadOnly="True" tag="TotAdjAmt" Style="text-align: right" Width="110px">$0.00</cc1:DataEntryBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="col-md-4 control-label">
                                Contact:</label>
                            <div class="col-md-8">
                                <cc1:DataEntryBox ID="ContactName" runat="server" CssClass="form-control" ReadOnly="True" tag="AddressLine1" DataType="Any"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <label class="col-md-3 control-label">
                                Placed On:</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="ReferalDate" runat="server" CssClass="form-control" ReadOnly="True" DataType="DateValue"
                                    Style="text-align: center" Width="120px"></cc1:DataEntryBox>
                            </div>
                            <label class="col-md-3 control-label">
                                Payments:</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="TotRcvAmt" runat="server" CssClass="form-control"
                                    DataType="Money" ReadOnly="True" tag="TotRcvAmt" Style="text-align: right" Width="110px">$0.00</cc1:DataEntryBox>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="col-md-4 control-label">
                                Address:</label>
                            <div class="col-md-8">
                                <cc1:DataEntryBox ID="AddressLine1" runat="server" CssClass="form-control" ReadOnly="True" tag="AddressLine2" DataType="Any"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <label class="col-md-3 control-label">
                                Closed On:</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="CloseDate" runat="server" CssClass="form-control" ReadOnly="True" DataType="DateValue"
                                    Style="text-align: center" Width="120px"></cc1:DataEntryBox>
                            </div>
                            <label class="col-md-3 control-label">
                                Balance:</label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="TotBalAmt" runat="server" CssClass="form-control"
                                    DataType="Money" ReadOnly="True" tag="TotBalAmt" Style="text-align: right" Width="110px">$0.00</cc1:DataEntryBox>
                            </div>
                        </div>
                    </div>
                <%--    <div class="form-group">
                        <div class="col-md-5">
                            <label class="col-md-4 control-label">
                                City/St/Zip:</label>
                            <div class="col-md-8" style="display: flex;">
                                <cc1:DataEntryBox ID="City" runat="server" CssClass="form-control" ReadOnly="True" tag="City" DataType="Any"></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="col-md-6" style="display: flex; margin-left: -35px;">
                            <cc1:DataEntryBox ID="State" runat="server" CssClass="form-control" ReadOnly="True" Tag="State" DataType="Any" Style="text-align: center" Width="75px"></cc1:DataEntryBox>&nbsp;
                                                                    <cc1:DataEntryBox ID="PostalCode" runat="server" CssClass="form-control" ReadOnly="True" Tag="PostalCode" DataType="Any" Width="120px"></cc1:DataEntryBox>
                        </div>

                    </div>--%>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="col-md-4 control-label">
                                </label>
                            <div class="col-md-8" style="display: flex;">
                                <cc1:DataEntryBox ID="City" runat="server" CssClass="form-control" ReadOnly="True" tag="City" DataType="Any"></cc1:DataEntryBox>
                            </div>
                        </div>
                      
                    </div>
                   <%--    <div class="form-group">
                           <div class="col-md-5">
                               <label class="col-md-4 control-label">
                                   State/Zip::</label>
                               <div class="col-md-4" style="text-align: left;">
                                   <div class="btn-group" style="text-align: left;" align="left">
                                       <button type="button" class="btn btn-default dropdown-toggle" style="width: 130px;" align="left"
                                           data-toggle="dropdown" aria-expanded="false" id="btnDropState" tabindex="7">
                                           --Select State--
                     
                                                                                    <span class="caret"></span>
                                       </button>
                                       <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState" style="overflow-y: auto; height: 250px;">
                                       
                                       </ul>
                                   </div>
                             
                               </div>
                              
                               <asp:HiddenField ID="State" runat="server" Value="" />
                           </div>
                           <div class="col-md-4" style="display: flex; padding-left: 0px;">
                                 <cc1:DataEntryBox ID="PostalCode" runat="server" CssClass="form-control" ReadOnly="True" Tag="PostalCode" DataType="Any" Width="120px"></cc1:DataEntryBox>
                           </div>

                    </div>--%>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label class="col-md-4 control-label">
                                Status:</label>
                            <div class="col-md-8" >
                                <cc1:DataEntryBox ID="CollectionStatus" runat="server" CssClass="form-control"
                                    DataType="Any" ReadOnly="True" tag="CollectionStatus" Width="400px"
                                    ></cc1:DataEntryBox>
                            </div>
                        </div>
                        <div class="col-md-2" style="display: none;padding-left: 67px; ">
                            <asp:Literal ID="listStatus" runat="server"></asp:Literal>
                            <asp:Literal ID="Literal1" runat="server"></asp:Literal>
                        </div>

                    </div>
                </div>



                <!--
            Set the active tabindex to zero for by default selected
    -->
                <asp:HiddenField runat="server" ID="hddbTabName" Value="Notes" />
                <div class="nav-tabs-horizontal">
                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                        <li id="Notes" class="active" role="presentation" runat="server"><a data-toggle="tab" onclick="SetTabName('Notes');" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Notes</a></li>
                        <li id="Accounting" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Accounting');" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Accounting</a></li>
                        <li id="Docs" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Docs');" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab" aria-expanded="false">Docs</a></li>
                        <li id="InsuranceInfo" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('InsuranceInfo');" href="#exampleTabsFour" aria-controls="exampleTabsFour" role="tab" aria-expanded="false">Insurance Info</a></li>

                    </ul>
                    <div class="tab-content padding-top-20" style="border: 1px solid lightgray;">
                        <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                            <div class="form-horizontal" style="margin-left:5px;">
                                <div class="form-group">
                                    <div class="col-md-12" style="padding-right:0px;text-align:left;" id ="divnotes">
                                        <SiteControls:AccountAddNote runat="server" ID="AccountAddNote"/>
                                         <asp:LinkButton ID="btnPrintNotes" runat="server" CssClass="btn btn-primary" Text="Print Notes"></asp:LinkButton>
                                    </div>
                                   <%-- <div class="col-md-2" style="padding-left: 0px;padding-right: 122px;">
                                       
                                    </div>--%>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div style="width: auto; height: auto; overflow: auto;">
                                            <asp:GridView ID="gvwNotes" runat="server" Width="100%"
                                                CssClass="table dataTable table-striped"
                                                OnRowDataBound="gvwNotes_RowDataBound" AutoGenerateColumns="False">
                                                <%--  <AlternatingRowStyle CssClass="altrowstyle" />
                                                                        <HeaderStyle CssClass="headerstyle" />
                                                                        <RowStyle CssClass="rowstyle" />--%>

                                                <Columns>
                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemTemplate>
                                                            <%--&nbsp;&nbsp;<asp:ImageButton ID="btnViewNote" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />&nbsp;&nbsp;--%>
                             <%--                               <asp:LinkButton ID="btnViewNote" CssClass="icon ti-eye" runat="server" OnClientClick="return SetNoteId(this)"></asp:LinkButton>--%>
                                                              <asp:LinkButton ID="btnViewNote" CssClass="icon ti-eye" runat="server" OnClientClick=<%# "javascript:return SetNoteId(this,'" + Eval("type") + "')" %>></asp:LinkButton>

                                                        </ItemTemplate>
                                                        <ItemStyle Width="25px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="NoteDate" SortExpression="NoteDate" HeaderText="Date">
                                                        <ItemStyle Width="50px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UserCode" SortExpression="UserCode" HeaderText="By">
                                                        <ItemStyle Width="30px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Note" SortExpression="Note" HeaderText="Note"></asp:BoundField>
                                                </Columns>
                                                <EmptyDataTemplate><b>Notes have not been entered on this Account</b></EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="tab-pane active" id="exampleTabsTwo" role="tabpanel">
                            <div class="form-horizontal" style="margin-left:5px;">
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div style="width: auto; height: auto; overflow: auto;">
                                            <asp:GridView ID="gvwPayments" runat="server" Width="100%"
                                                CssClass="table dataTable table-striped"
                                                OnRowDataBound="gvwpayments_RowDataBound" AutoGenerateColumns="False">
                                                <%-- <AlternatingRowStyle CssClass="altrowstyle" />
                                                                        <HeaderStyle CssClass="headerstyle" />
                                                                        <RowStyle CssClass="rowstyle" />--%>
                                                <Columns>
                                                    <asp:BoundField DataField="TrxDate" SortExpression="TrxDate" HeaderText="Date">
                                                        <ItemStyle HorizontalAlign="center" Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AsgAmt" SortExpression="AsgAmt" HeaderText="Assigned">
                                                        <ItemStyle HorizontalAlign="center" Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="AdjAmt" SortExpression="AdjAmt" HeaderText="Adjusted">
                                                        <ItemStyle HorizontalAlign="center" Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="PmtAmt" SortExpression="PmtAmt" HeaderText="Received">
                                                        <ItemStyle HorizontalAlign="center" Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RemitDate" SortExpression="RemitDate" HeaderText="Remit/Inv Date">
                                                        <ItemStyle HorizontalAlign="center" Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RcvByAgencyAmt" SortExpression="RcvByAgency" HeaderText="Paid Agency">
                                                        <ItemStyle HorizontalAlign="center" Width="80px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="RcvByClientAmt" SortExpression="RcvByClient" HeaderText="Paid Client">
                                                        <ItemStyle HorizontalAlign="center" Width="80px" />
                                                    </asp:BoundField>

                                                </Columns>
                                                <EmptyDataTemplate><b>Payment have not been entered on this Account</b></EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div style="display: block">
                            </div>
                        </div>
                        <div class="tab-pane active" id="exampleTabsThree" role="tabpanel">
                            <div class="form-horizontal" style="margin-left:5px;">
                                <div class="form-group">
                                    <div class="col-md-12" style="text-align: left;" id="divAddDoc">
                                        <asp:Button ID="btnAddDocuments" runat="server" CssClass="btn btn-primary" CausesValidation="False" Text="Add Document" Style="display: none;" />
                                        <asp:Button ID="btnAddDocumentsNew" runat="server" OnClientClick="$('#ModalFileUpload').modal('show');return false;" CssClass="btn btn-primary" CausesValidation="False"
                                            Text="Add Document" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div style="width: auto; height: auto; overflow: auto;">
                                            <asp:GridView ID="gvwDocs" Width="100%" runat="server" AutoGenerateColumns="False" PageSize="20" CssClass="table  dataTable table-striped">
                                                <%--    <RowStyle CssClass="rowstyle" />
                                                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                                                        <HeaderStyle CssClass="headerstyle" />--%>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="View">
                                                        <ItemTemplate>
                                                            <%--&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnPrntDocs" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server"/>--%>
                                                            <asp:LinkButton ID="btnPrntDocs" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>

                                                    <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date Added">
                                                        <ItemStyle HorizontalAlign="Center" Width="70px" Height="15px" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UserCode" SortExpression="UserCode" HeaderText="User">
                                                        <ItemStyle Width="70px" Height="15px" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DocumentType" SortExpression="DocumentType" HeaderText="Type">
                                                        <ItemStyle Width="70px" Height="15px" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DocumentDescription" SortExpression="DocumentDescription" HeaderText="Description">
                                                        <ItemStyle Width="200px" Height="15px" Wrap="False" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                            </asp:GridView>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div style="float: left; margin-top: 30px; height: 100%;">

                                <ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1"
                                    runat="server" BehaviorID="FileUpLoad" TargetControlID="btnAddDocuments"
                                    PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" />

                                <asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none" Width="550px">
                                    <h1 class="panelheader">
                                        <div style="width: 100%;">
                                            <div style="text-align: center; width: 90%; float: left;">Upload Documents</div>
                                            <div style="float: right; width: 10%;">
                                                <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="Return" Width="40px" CssClass="btn btn-primary" />
                                            </div>
                                        </div>


                                    </h1>

                                    <iframe runat="server" id="frm" src="App_Controls/_Custom/LienView/Default.aspx?id=0 &debtaccountid=1" style="width: 550px; height: 540px; background-color: white"></iframe>

                                </asp:Panel>


                            </div>
                            <div class="modal fade modal-primary" id="ModalFileUpload" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
                                tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false">
                                <div class="modal-dialog modal-center">
                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">�</span>
                                            </button>
                                            <h4 class="modal-title">UPLOAD DOCUMENT</h4>
                                        </div>
                                        <iframe  id="Iframe1" src="App_Controls/_Custom/LienView/Default.aspx?id=0 &debtaccountid=1" style="width: 550px; height: 540px; border: 0px; background-color: white"></iframe>
                                       <%--  <iframe id="iframe1" src="App_Controls/_Custom/LienView/Default.aspx?id=0 &debtaccountid=1" style="height: 540px; width: 550px; border: 0px;"></iframe>--%>
                                        <div class="modal-footer">
                                          <%--  <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">--%>
                                               <button type="button" class="btn btn-default margin-0" OnClick ="javascript:Clear()" aria-label="Close">
                                                <span aria-hidden="true">CLOSE</span> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane active" id="exampleTabsFour" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Agent Code</label>
                                    <div class="col-md-3">
                                        <cc1:DataEntryBox ID="ClientAgentCode" runat="server" blankonzeroes="False" CssClass="form-control"
                                            DataType="Any" ReadOnly="True" tag="ClientAgentCode" Width="100px"></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Claim #</label>
                                    <div class="col-md-3">
                                        <cc1:DataEntryBox ID="Dataentrybox1" runat="server" blankonzeroes="False" CssClass="form-control"
                                            DataType="Any" ReadOnly="True" tag="ClaimNumber" Width="100px"></cc1:DataEntryBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Estimated Audit</label>
                                    <div class="col-md-3">
                                        <div class="checkbox-custom checkbox-default">
                                            <asp:CheckBox ID="CheckBox1" runat="server" Text=" " />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <%-- <ajaxToolkit:TabContainer ID="TabContainer2" Width="700px" runat="server"
                                                        CssClass="tabs" ActiveTabIndex="0">
                                                        <ajaxToolkit:TabPanel ID="TabPanel1" runat="server" HeaderText="Notes">
                                                            <ContentTemplate>
                                                              
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel2" runat="server" Width="700px" HeaderText="Accounting">
                                                            <ContentTemplate>
                                                               
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel10" runat="server" HeaderText="Docs">
                                                            <ContentTemplate>
                                                               
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                        <ajaxToolkit:TabPanel ID="TabPanel4" runat="server" HeaderText="Insurance Info">
                                                            <ContentTemplate>
                                                              
                                                            </ContentTemplate>
                                                        </ajaxToolkit:TabPanel>
                                                    </ajaxToolkit:TabContainer>--%>
            </div>
            <div class="footer">
                <table width="690PX">
                    <tr>

                        <td align="right">

                            <asp:ImageButton ID="btnFirst" runat="server" Enabled="true"
                                ImageUrl="~/App_Themes/VbJuice/Img/resultset_First.png" OnClick="btnFirst_Click" />

                            <asp:ImageButton ID="btnPrevious" runat="server" Enabled="true"
                                ImageUrl="~/App_Themes/VbJuice/Img/resultset_Previous.png" OnClick="btnPrevious_Click" />

                            <asp:ImageButton ID="btnNext" runat="server" Enabled="true"
                                ImageUrl="~/App_Themes/VbJuice/Img/resultset_Next.png" OnClick="btnNext_Click" />

                            <asp:ImageButton ID="btnLast" runat="server" Enabled="true"
                                ImageUrl="~/App_Themes/VbJuice/Img/resultset_Last.png" OnClick="btnLast_Click" />

                        </td>

                    </tr>
                </table>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <br />
            <div class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-2">
                        <asp:LinkButton ID="btnExitViewer" runat="server" CssClass="btn btn-primary" Text="Exit Viewer"></asp:LinkButton>
                    </div>
                    <div class="col-md-2">
                        <asp:LinkButton ID="btnDownLoad" Text="DownLoad" CssClass="btn btn-primary" runat="server" />
                    </div>
                </div>
            </div>

            <div style="width: auto; height: auto; overflow: auto;">
                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
            </div>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="hdnDocId" runat="server" Value="0" />
<asp:Button ID="btnDeleteDoc" runat="server" Style="display: none;" OnClick="btnDeleteDoc_Click" />
<asp:UpdatePanel ID="updPan1" runat="server">
    <ContentTemplate>
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">

            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage"
                            runat="server"
                            ImageUrl="~/images/ajax-loader.gif"
                            AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender
                    ID="AlwaysVisibleControlExtender1"
                    runat="server"
                    TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center"
                    HorizontalOffset="150"
                    VerticalSide="Middle"
                    VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnDownLoad" />
    </Triggers>
</asp:UpdatePanel>
<div class="modal fade modal-primary" id="ModalJobNoteDetail" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title">Note Detail</h4>

            </div>
            <div class="modal-body">
                <%--  <div id="modcontainer" style="MARGIN: 0px  0px 0px 0px; width: 100%;">--%>

                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">

                            <div class="col-md-12">
                                <asp:TextBox ID="txtNotes" runat="server" Height="402px" Width="100%" TextMode="MultiLine"
                                    CssClass="textbox" ReadOnly="True"></asp:TextBox>
                            </div>
                        </div>
                    </div>

                </div>

                <%-- </div>--%>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default margin-0" onclick="HideNotePopUp()" >Close</button>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hdnBatchNoteId" runat="server" />
<asp:HiddenField ID="hdnNoteType" runat="server" />
<asp:Button ID="btnNoteDetail" runat="server" OnClick="btnNoteDetail_Click" Style="display: none;" />

<script type="text/javascript">
    function OpenAddCollectorInfoModal() {
        $("#ModalCollectorInfo").modal('show');
    }
    function ModalHide() {
        // alert('hello');

        // $("#btnclose").click();
        <%--//document.getElementById('<%=CancelButton.ClientID%>').click();    --%>    
        //ActivateTab('Notes');
        $("#ModalCollectorInfo").modal('hide');
    }
    function ModalSave() {
      <%-- // document.getElementById('<%=btnSave.ClientID%>').click();--%>
        $("#ModalCollectorInfo").modal('hide');
    }
</script>



<div class="modal fade modal-primary" id="ModalCollectorInfo" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center" style="width:700px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btncloseCollector" onclick="ModalHide();">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title">COLLECTOR INFO</h4>
                 </div>
            <div class="modal-body" style="padding-left: 55px;padding-right: 55px;height: 300px;">
                            <div class="body" style="padding-top :40px">
                    <div class="form-horizontal">
                         <div class="form-group col-md-5"  Style="float :left">
                           
                            
                                <asp:Image ID="CollectorImage" BackColor ="#cccccc" runat="server" height="220px" Width ="200px"  Style="float :left"/>
                            
                            </div>
                         <div class="form-group col-md-6" style="padding-top:35px" >
                        <div class="form-group">
                             <label class="col-md-7 control-label">Collector Name : </label>
                            <div class="col-md-3">
                              <cc1:DataEntryBox ID="CollectorName" runat="server" blankonzeroes="False" CssClass="form-control"
                                            DataType="Any"   Width="250px" ReadOnly ="true"></cc1:DataEntryBox>
                                 
                            </div>
                            </div>
                      <div class="form-group">
                             <label class="col-md-7 control-label">Collector Email : </label>
                            <div class="col-md-3">
                               <cc1:DataEntryBox ID="CollectorEmail" runat="server" blankonzeroes="False" CssClass="form-control"
                                            DataType="Any"   Width="250px" ReadOnly ="true"></cc1:DataEntryBox>
                               
                            </div>
                        </div>
                          <div class="form-group">
                             <label class="col-md-7 control-label">Collector Phone : </label>
                            <div class="col-md-3">
                                <cc1:DataEntryBox ID="CollectorPhone" runat="server" blankonzeroes="False" CssClass="form-control"
                                            DataType="Any"   Width="150px" ReadOnly ="true"></cc1:DataEntryBox>
                             
                            </div>
                        </div>
                          <div class="form-group">
                             <label class="col-md-7 control-label">Collector Fax : </label>
                            <div class="col-md-2">
                               <cc1:DataEntryBox ID="CollectorFax" runat="server" blankonzeroes="False" CssClass="form-control"
                                            DataType="Any"   Width="150px" ReadOnly ="true"></cc1:DataEntryBox>
                             
                            </div>
                        </div>
                </div>
                        </div>
                 </div>
                 </div>
           
            <div class="modal-footer">
               
                <%--<button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaves" onclick="ModalSave();">
                    SAVE
                </button>--%>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="ModalHide();">Close</button>
            </div>
            </div>
       
    </div>
  </div>
<asp:Panel ID="panNewNote" runat="server" Style="display: none"
    Width="400px" Font-Bold="True">
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="" OkControlID="" DropShadow="true"
    PopupControlID="panNewNote" TargetControlID="btnCollectorInfo">
</ajaxToolkit:ModalPopupExtender>