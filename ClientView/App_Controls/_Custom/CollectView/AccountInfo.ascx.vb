Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL.CRFDB

Partial Class App_Controls__Custom_CollectView_AccountInfo
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchDebtAccount
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myaccount As New CRF.CLIENTVIEW.BLL.CRFDB.VIEWS.vwAccountList
    Protected WithEvents btnPrntDocs As LinkButton
    Protected WithEvents btnViewNote As LinkButton
    Protected WithEvents btnDeleteItem As LinkButton
    Dim imagedata As Byte()
    Dim itemid As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        myview = DirectCast(Me.Session("accountlist"), HDS.DAL.COMMON.TableView)
        itemid = qs.GetParameter("ItemId")
        'markparimal-uploadfile:debtaccountid value is taken in session so that it can be used in file upload page.
        Session("debtaccountid") = itemid
        If Me.CurrentUser.UserManager = 0 Then

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideDiveNotes", "hideDiveNotes();", True)

        End If
        Me.MultiView1.SetActiveView(Me.View1)
        If Me.Page.IsPostBack = False Then
            LoadItem()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        End If
        DisableTabs()
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDrp", "BindStateDropdown(" & Me.CurrentUser.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncNotesList", "CallSuccessFuncNotesList();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncAccountingList", "CallSuccessFuncAccountingList();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDocList", "CallSuccessFuncDocList();", True)

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        If gvwNotes.Rows.Count > 0 Then
            Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If gvwPayments.Rows.Count > 0 Then
            Me.gvwPayments.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If gvwDocs.Rows.Count > 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If


    End Sub
    ' "Notes"
    Protected Sub JobAddNote1_ItemSaved() Handles AccountAddNote.ItemSaved
        LoadItem()
        hddbTabName.Value = "Notes"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        If gvwNotes.Rows.Count > 0 Then
            Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If gvwPayments.Rows.Count > 0 Then
            Me.gvwPayments.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If gvwDocs.Rows.Count > 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'Me.TabContainer2.ActiveTab = Me.TabPanel1
    End Sub

    Private Sub LoadItem()

        Dim ds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetAccountInfo(Me.itemid)
        Dim dtaccinfo As System.Data.DataRow = ds.Tables(0).Rows(0)
        Dim myview1 As New HDS.DAL.COMMON.TableView(ds.Tables(0), "")
        myview1.FillEntity(myaccount)
        With myaccount
            Me.FileNumber.Value = .FileNumber
            Me.AccountNo.Value = .AccountNo
            Me.DebtorName.Value = .DebtorName
            Me.ContactName.Value = .ContactName
            Me.AddressLine1.Value = .AddressLine1
            Me.City.Value = .City & ", " & .State & " " & .PostalCode
            'Me.State.Value = .State
            'Me.PostalCode.Value = .PostalCode
            Me.TotAsgAmt.Value = .TotAsgAmt
            Me.TotAdjAmt.Value = .TotAdjAmt
            Me.TotRcvAmt.Value = .TotPmtAmt
            Me.TotBalAmt.Value = .TotBalAmt
            Me.ServiceCode.Value = .ClientCode
            Me.ReferalDate.Value = .ReferalDate
            Me.BranchNum.Value = .BranchNum
            Me.CollectionStatus.Value = .CollectionStatus + " " + .CollectionStatusDescr
            Me.CloseDate.Value = .CloseDate
            Me.CollectorName.Value = .ClientAlias
            Me.CollectorEmail.Value = .CollectorEmail
            Me.CollectorPhone.Value = .CollectorPhone
            Me.CollectorFax.Value = .LocationFax
            imagedata = .CollectorPhoto

        End With

        If Not imagedata Is Nothing Then
            CollectorImage.ImageUrl = "image.aspx?ID=" & Me.itemid
        Else
            CollectorImage.ImageUrl = "/images/ResourceImages/noimage.png"
        End If




        '  Me.listStatus.Text = myaccount.CollectionStatusDescr

        Me.gvwPayments.DataSource = ds.Tables(1)
        Me.gvwPayments.DataBind()

        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where DebtAccountId = " & myaccount.DebtAccountId & " And Documenttype <> 'Internal'"

        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()

        If Me.UserInfo.CollInfo.WebNoteAccess = False Then
            '    Me.TabContainer2.Tabs.Remove(Me.TabPanel1)
            'Me.TabPanel1.Visible = False 'Commented by Jaywanti
            Notes.Style.Add("display", "none")

            Me.gvwNotes.DataSource = Nothing
            Me.gvwNotes.DataBind()
        Else
            Me.gvwNotes.DataSource = ds.Tables(2)
            Me.gvwNotes.DataBind()


        End If

    End Sub
    Private Sub DisableTabs()
        If Me.UserInfo.CollInfo.ClientAgentCode = False Then
            Me.litDetorName.Text = "Name:"
            Me.litAccountNo.Text = "Account#:"
            InsuranceInfo.Style.Add("display", "none")
            ' Me.TabPanel4.HeaderText = "" 'Commented by Jaywanti

            '   Me.TabContainer2.Tabs.Remove(Me.TabPanel4)
        Else
            Me.litDetorName.Text = "Insured Name:"
            Me.litAccountNo.Text = "Policy#:"
            InsuranceInfo.Style.Add("display", "block")
            'Me.TabPanel4.HeaderText = "Insurance Info" 'Commented by Jaywanti

        End If

        If Me.UserInfo.CollInfo.WebNoteAccess = False Then
            '    Me.TabContainer2.Tabs.Remove(Me.TabPanel1)
            Notes.Style.Add("display", "none")
            'Me.TabPanel1.Visible = False 'Commented by Jaywanti
            Me.gvwNotes.DataSource = Nothing
        End If

    End Sub
    Protected Sub gvwNotes_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnViewNote = TryCast(e.Row.FindControl("btnViewNote"), LinkButton)
            btnViewNote.Attributes("rowno") = DR("Id")
            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("NoteDate")) & ""
            e.Row.Cells(2).Text = "" & Strings.Left(e.Row.Cells(2).Text, 20) & ""
            e.Row.Cells(3).Wrap = True
            If Me.CurrentUser.UserManager = 0 Then
                btnViewNote.Visible = False
                'gvwNotes.Columns(0).Visible = False
            End If
        End If
    End Sub
    Protected Sub gvwPayments_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem

            e.Row.Cells(0).Text = "" & Utils.FormatDate(DR("TrxDate")) & ""
            e.Row.Cells(4).Text = "" & Utils.FormatDate(DR("RemitDate")) & ""

            e.Row.Cells(1).Text = "" & Utils.FormatDecimal(DR("AsgAmt")) & ""
            e.Row.Cells(2).Text = "" & Utils.FormatDecimal(DR("AdjAmt")) & ""
            e.Row.Cells(3).Text = "" & Utils.FormatDecimal(DR("PmtAmt")) & ""
            e.Row.Cells(6).Text = "" & Utils.FormatDecimal(DR("RcvByClientAmt")) & ""
            e.Row.Cells(5).Text = "" & Utils.FormatDecimal(DR("RcvByAgencyAmt")) & ""

        End If
    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?collectview.accountlist&page=1"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackFilter.Click
        Dim s As String = "~/MainDefault.aspx?collectview.accountlist"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnFirst.Click
        GetJobFromList("F")

    End Sub

    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnPrevious.Click
        GetJobFromList("-1")

    End Sub

    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        GetJobFromList("1")

    End Sub

    Protected Sub btnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnLast.Click
        GetJobFromList("L")

    End Sub
    Private Sub GetJobFromList(ByVal increment As String)
        Select Case increment
            Case "F"
                myview.RowIndex = 0
            Case "L"
                myview.RowIndex = myview.Count - 1
            Case Else
                myview.RowIndex = GetRowIndex() + increment
        End Select

        If myview.RowIndex < 0 Then
            myview.RowIndex = 0
        End If
        If myview.RowIndex >= myview.Count Then
            myview.RowIndex = myview.Count - 1
        End If

        Dim s As String = "~/MainDefault.aspx?collectview.accountinfo&page=1&itemid=" & myview.RowItem("DebtAccountId")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Private Function GetRowIndex() As Integer
        Dim myidx As Integer = 0
        myview.MoveFirst()
        Do Until myview.EOF
            If myview.RowItem("DebtAccountId") = Me.itemid Then
                Return myidx
            End If
            myidx += 1
            myview.MoveNext()
        Loop
        Return myidx = 0

    End Function

    Protected Sub btnPrintNotes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintNotes.Click
        Dim sreport As String = CollectView.Provider.GetNotesForAccount(itemid, True)
        Session("ActiveTab") = 0 'set active tab session for go to back in selected tab
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncNotesList", "CallSuccessFuncNotesList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncAccountingList", "CallSuccessFuncAccountingList();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDocList", "CallSuccessFuncDocList();", True)

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
            If gvwNotes.Rows.Count > 0 Then
                Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwPayments.Rows.Count > 0 Then
                Me.gvwPayments.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            If gvwDocs.Rows.Count > 0 Then
                Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub

    Protected Sub btnExitViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitViewer.Click
        Me.MultiView1.SetActiveView(Me.View1)
        If (Session("ActiveTab") = 0) Then
            hddbTabName.Value = "Notes"
        End If
        If (Session("ActiveTab") = 1) Then
            hddbTabName.Value = "Accounting"
        End If
        If (Session("ActiveTab") = 2) Then
            hddbTabName.Value = "Docs"
        End If
        If (Session("ActiveTab") = 3) Then
            hddbTabName.Value = "InsuranceInfo"
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        'Commented by Jaywanti
        'TabContainer2.ActiveTabIndex = Session("ActiveTab") ' set the active tab

    End Sub

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))
        Me.MultiView1.SetActiveView(Me.View1)
        hddbTabName.Value = "Docs"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        'TabContainer2.ActiveTabIndex = 2 'Commented by Jaywanti

    End Sub
    Protected Sub gvwDocs_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntDocs"), LinkButton).Click, AddressOf btnPrntDocs_Click

        End If
    End Sub
    Protected Sub gvwDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            btnPrntDocs.Attributes("rowno") = DR("Id")
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteItem.Attributes("rowno") = DR("Id")
            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            If Me.CurrentUser.UserManager = 0 Then
                btnPrntDocs.Visible = False
                btnDeleteItem.Visible = False
                'gvwDocs.Columns(0).Visible = False
                'gvwDocs.Columns(5).Visible = False
            End If
        End If

    End Sub
    Protected Sub btnPrntDocs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntDocs.Click
        Me.hddbTabName.Value = "Docs"
        btnPrntDocs = TryCast(sender, LinkButton)
        Session("ActiveTab") = 3 'set active tab session for go to back in selected tab
        Me.ViewState("VIEW") = "Docs"
        Dim myitem As New TABLES.DebtAccountAttachments
        ProviderBase.DAL.Read(btnPrntDocs.Attributes("rowno"), myitem)

        Dim sreport As String = "~/UserData/output/" & myitem.Id & "-" & myitem.FileName
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.DocumentImage, Me.MapPath(sreport))

        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)

                Dim firstpart As String = Filepath.Substring(0, Filepath.LastIndexOf("/"))
                Dim lastpart As String = Filepath.Substring(Filepath.LastIndexOf("/") + 1)
                'lastpart = HttpUtility.UrlEncode(lastpart)  'Remove # with %23 (Html Encoding UTF-8)
                ' lastpart = HttpUtility.UrlPathEncode(lastpart)  'Remove space with %20
                lastpart = Uri.EscapeDataString(lastpart) 'Remove # with %23 , space with %20

                Filepath = firstpart + "/" + lastpart

                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActivateTab", "ActivateTab('" & Me.hddbTabName.Value & "');", True)
    End Sub
    Protected Sub btnFileUpLoadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadCancel.Click
        'markparimal-uploadfile:To refresh Grid values .

        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where DebtAccountId = " & itemid & " And Documenttype <> 'Internal'"

        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If gvwNotes.Rows.Count > 0 Then
            Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If gvwPayments.Rows.Count > 0 Then
            Me.gvwPayments.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If gvwDocs.Rows.Count > 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        hddbTabName.Value = "Docs"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1Data", "modalHideShow('ModalFileUpload','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
    End Sub

    Protected Sub btnAddDocuments_Click(sender As Object, e As System.EventArgs) Handles btnAddDocuments.Click

    End Sub

    Protected Sub btnNoteDetail_Click(sender As Object, e As EventArgs)
        Dim myItem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.DebtAccountNote
        If hdnBatchNoteId.Value > 1 Then

            Dim result As String = ""

            If hdnNoteType.Value = "NH" Then
                Dim MYSQL As String = "Select DebtAccountNoteId As Id,NoteDate, UserCode, OwnerId, Note As Note, NoteTypeId, NoteType From vwCollectionNotes vw Where DebtAccountNoteId =" & hdnBatchNoteId.Value
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                result = myview.RowItem("Note")
            ElseIf hdnNoteType.Value = "AH" Then
                Dim MYSQL As String = "SELECT  CollectionActionHistoryId as Id, DateCreated as NoteDate, RequestedBy, RequestedByUserId, HistoryNote as Note FROM dbo.CollectionActionHistory  (NOLOCK) INNER JOIN dbo.CollectionActions (NOLOCK) ON dbo.CollectionActionHistory.ActionId = dbo.CollectionActions.Id WHERE CollectionActionHistoryId= " & hdnBatchNoteId.Value
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                result = myview.RowItem("Note")
            ElseIf hdnNoteType.Value = "CL" Then
                Dim MYSQL As String = "SELECT  jcl.Id as Id, DateChanged as NoteDate, UserName, UserId, ja.description + ' From: ' + changefrom + ' TO: ' + changeTO as Note, NULL as NoteTypeId, NULL as NoteType FROM dbo.DebtAccountChangeLog jcl  (NOLOCK) INNER JOIN  dbo.CollectionActions ja ON jcl.ActionId = ja.Id  WHERE jcl.Id =" & hdnBatchNoteId.Value
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                result = myview.RowItem("Note")
            Else
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchNoteId.Value, myItem)
                result = myItem.Note
            End If

            Me.txtNotes.Text = result.Replace("<", "[").Replace(">", "]")
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsNotePopUp", "DisplayNotePopUp();", True)



    End Sub
    Protected Sub btnDeleteDoc_Click(sender As Object, e As EventArgs)
        Dim myitem As New TABLES.DebtAccountAttachments
        myitem.Id = hdnDocId.Value
        ProviderBase.DAL.Delete(myitem)
        hddbTabName.Value = "Docs"
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where DebtAccountId = " & itemid & " And Documenttype <> 'Internal'"
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetTabName", "SetTabName('Docs');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)

    End Sub
End Class
