﻿Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL.CRFDB



Partial Class App_Controls__Custom_CollectView_image
    Inherits System.Web.UI.Page
    Dim myaccount As New CRF.CLIENTVIEW.BLL.CRFDB.VIEWS.vwAccountList


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim imagedata As Byte()
        Dim id As Integer = Convert.ToInt32(Request.QueryString("ID"))

        Dim ds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetAccountInfo(id)
        Dim dtaccinfo As System.Data.DataRow = ds.Tables(0).Rows(0)
        Dim myview1 As New HDS.DAL.COMMON.TableView(ds.Tables(0), "")
        myview1.FillEntity(myaccount)
        With myaccount

            imagedata = .CollectorPhoto

        End With

        If Not imagedata Is Nothing Then
            Response.BinaryWrite(imagedata)
        End If
    End Sub


End Class
