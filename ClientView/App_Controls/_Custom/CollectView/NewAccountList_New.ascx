﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script runat="server">
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchDebtAccount
    Protected WithEvents btnDeleteItem As ImageButton
    Protected WithEvents btnEditItem As ImageButton
    Protected WithEvents btnPrintItem As ImageButton

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.HiddenField1.Value = Me.MyPage.CurrentUser.Id
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Session("filter") = ""
            Me.gvItemList.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
            If (gvItemList.Rows.Count > 0) Then
                Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
            End If
           
            'txtFindText.Enabled = gvItemList.Rows.Count > 0
            'btnFindText.Enabled = gvItemList.Rows.Count > 0
            btnRefresh.Enabled = gvItemList.Rows.Count > 0
            btnPrint.Enabled = gvItemList.Rows.Count > 0

            If (gvItemList.Rows.Count > 0) Then
                ' btnFindText.CssClass = "button"
                btnRefresh.CssClass = "button"
                btnPrint.CssClass = "button"
            Else
                ' btnFindText.CssClass = "button_disabled btn btn-primary"
                btnRefresh.CssClass = "button_disabled btn btn-primary"
                btnPrint.CssClass = "button_disabled btn btn-primary"
            End If
        End If

    End Sub
    'Comment by Jaywanti on 15-12-2015
    'Protected Sub btnFindText_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFindText.Click
    '    Dim myfilter As New HDS.DAL.COMMON.FilterBuilder
    '    Dim s As String = ""
    '    myfilter.Clear()
    '    ' myfilter.SearchAnyWords("accountno", Me.txtFindText.Text, "OR")
    '    ' myfilter.SearchAnyWords("debtorname", Me.txtFindText.Text, "")
    '    s = myfilter.GetFilter
    '    Me.Session("filter") = s
    '    Me.gvItemList.DataBind()
    '    If (gvItemList.Rows.Count > 0) Then
    '        Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader
    '        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    '    End If
       
      
    'End Sub
    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        Me.Session("filter") = ""
        Me.gvItemList.DataBind()
        If (gvItemList.Rows.Count > 0) Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If
    End Sub
    Private Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnDeleteItem"), ImageButton).Click, AddressOf btnDeleteItem_Click
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), ImageButton).Click, AddressOf btnEditItem_Click
            btnPrintItem = TryCast(e.Row.FindControl("btnPrintItem"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnPrintItem"), ImageButton).Click, AddressOf btnPrintItem_Click
        
        End If
    End Sub
  
    Private Sub btnadd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        ' Dim s As String = "?collectview.newaccount&itemid=-1" //Comment by Jaywanti on 09-12-2015
        Dim s As String = "~/MainDefault.aspx?collectview.newaccount&itemid=-1"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Private Sub btnDeleteItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnDeleteItem.Click
        btnDeleteItem = TryCast(sender, ImageButton)
        myitem.BatchDebtAccountId = btnDeleteItem.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Delete(myitem)
        Me.gvItemList.DataBind()
        If (gvItemList.Rows.Count > 0) Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If
        Me.MultiView1.SetActiveView(Me.View1)
        
        ' txtFindText.Enabled = gvItemList.Rows.Count > 0
        'btnFindText.Enabled = gvItemList.Rows.Count > 0
        btnRefresh.Enabled = gvItemList.Rows.Count > 0
        btnPrint.Enabled = gvItemList.Rows.Count > 0

        If (gvItemList.Rows.Count > 0) Then
            '  btnFindText.CssClass = "button"
            ' btnRefresh.CssClass = "button"
            btnPrint.CssClass = "button"
        Else
           ' btnFindText.CssClass = "button_disabled btn btn-primary"
            btnRefresh.CssClass = "button_disabled btn btn-primary"
            btnPrint.CssClass = "button_disabled btn btn-primary"
        End If

    End Sub
    Private Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, ImageButton)
        'Dim s As String = "?collectview.newaccount&itemid=" & btnEditItem.Attributes("rowno") //Comment by Jaywanti on 09-12-2015
        Dim s As String = "~/MainDefault.aspx?collectview.newaccount_new&itemid=" & btnEditItem.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Private Sub btnPrintItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditItem.Click
        btnPrintItem = TryCast(sender, ImageButton)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetAckReport(btnPrintItem.Attributes("rowno"), True)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
  
    'Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
    '    Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
    '    Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)
    'End Sub
    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= Me.gvItemList.PageCount Then
            Me.gvItemList.PageIndex = pageNumber - 1
        Else
            Me.gvItemList.PageIndex = 0
        End If
    End Sub

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), ImageButton)
            btnDeleteItem.Attributes("rowno") = dr("BatchDebtAccountId")
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), ImageButton)
            btnEditItem.Attributes("rowno") = dr("BatchDebtAccountId")
            btnPrintItem = TryCast(e.Row.FindControl("btnPrintItem"), ImageButton)
            btnPrintItem.Attributes("rowno") = dr("BatchDebtAccountId")
         
            e.Row.Cells(5).Text = "" & Strings.FormatCurrency(Decimal.Parse(e.Row.Cells(5).Text)) & ""
          
        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
        End If

        'If e.Row.RowType = DataControlRowType.Pager Then
        '    Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
        '    lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

        '    Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
        '    txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

        '    Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
        '    ddlPageSize.SelectedValue = gridView.PageSize.ToString()
        'End If
    End Sub
    Protected Sub gvItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvItemList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)
   
    End Sub
   
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetNewAccountReport(Me.MyPage.CurrentUser.Id)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 550, 600, "true"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
        
    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
</script>

<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetNewAccountList"
    TypeName="CRF.ClientView.BLL.CollectView.Provider">
    <SelectParameters>
        <asp:ControlParameter ControlID="HiddenField1" DefaultValue="-1" Name="userid" PropertyName="Value"
            Type="String" />
        <asp:SessionParameter DefaultValue="" Name="filter" SessionField="filter" Type="String" />
        <asp:SessionParameter Name="sort" SessionField="sort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<div class="form-horizontal">
    <div class="form-group">
        <div class="col-md-12">
            <div id="modcontainer" style="MARGIN: 10px  0px 0px 0px; width: 100%;">

                <asp:MultiView ID="MultiView1" runat="server">
                    <asp:View ID="View1" runat="server">
                        <h1 class="panelheader">New Account List</h1>
                        <div class="body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-3">
                                        <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Enter New Account" />
                                    </div>
                                   <%-- <div class="col-md-2">
                                        <cc1:DataEntryBox ID="txtFindText" runat="server" Text="" CssClass="form-control" />

                                    </div>
                                    <div class="col-md-1">

                                        <asp:Button ID="btnFindText" runat="server" Text="Search" CssClass="btn btn-primary" />
                                    </div>--%>
                                    <div class="col-md-1">
                                        <asp:Button ID="btnRefresh" runat="server" Text="Show All" CssClass="btn btn-primary" />
                                    </div>
                                    <div class="col-md-1">
                                        <asp:Button ID="btnPrint" runat="server" Text="Print Submitted" CssClass="btn btn-primary" OnClick="btnPrint_Click" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <asp:GridView ID="gvItemList" runat="server" DataSourceID="ItemsDataSource"
                                            CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                            BorderWidth="1px" Width="100%">
                                            <%--         <RowStyle CssClass="rowstyle" />
                                    <AlternatingRowStyle CssClass="altrowstyle" />
                                    <HeaderStyle CssClass="headerstyle" />
                                    <PagerStyle CssClass="pagerstyle" />
                                    <PagerTemplate>
                                        <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                        <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                            <asp:ListItem Value="5" />
                                            <asp:ListItem Value="10" />
                                            <asp:ListItem Value="15" />
                                            <asp:ListItem Value="20" />
                                        </asp:DropDownList>
                                        &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                        of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                        &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                        <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                        <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                        <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                                    </PagerTemplate>--%>
                                            <Columns>
                                                <asp:TemplateField HeaderText="Edit/Print">
                                                    <ItemTemplate>
                                                        &nbsp;
            	<asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" Width="15px" />
                                                        &nbsp;
				<asp:ImageButton ID="btnPrintItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" Width="15px" />
                                                        &nbsp;
                                                    </ItemTemplate>
                                                    <ItemStyle Width="65px" Wrap="False" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client#" meta:resourcekey="BoundFieldResource2">
                                                    <ItemStyle Width="80px" Height="15px" Wrap="False" />
                                                </asp:BoundField>

                                                <asp:BoundField DataField="AccountNO" SortExpression="AccountNo" HeaderText="Account#" meta:resourcekey="BoundFieldResource2">
                                                    <ItemStyle Width="80px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DebtorName" SortExpression="DebtorName" HeaderText="Name">
                                                    <ItemStyle Width="140px" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="AddressLine1" SortExpression="AddressLine1" HeaderText="Address">
                                                    <ItemStyle Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TotAsgAmt" SortExpression="TotAsgAmt" HeaderText="Balance">
                                                    <ItemStyle Width="80px" HorizontalAlign="right" Height="15px" Wrap="False" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="btnDeleteItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="15px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="footer">
                            <br />
                        </div>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:LinkButton ID="btnCancel1" runat="server" CssClass="btn btn-primary" Text="Back to List" CausesValidation="False" OnClick="btnCancel1_Click"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                </div>
                            </div>
                        </div>
                    </asp:View>
                </asp:MultiView>


            </div>
        </div>
    </div>
</div>
<asp:UpdateProgress ID="siteUpdateProgress" runat="server">
    <ProgressTemplate>
        <div class="TransparentGrayBackground"></div>
        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
            <div class="PageUpdateProgress">
                <asp:Image ID="ajaxLoadNotificationImage"
                    runat="server"
                    ImageUrl="~/images/ajax-loader.gif"
                    AlternateText="[image]" />
                &nbsp;Please Wait...
            </div>
        </asp:Panel>
        <ajaxToolkit:AlwaysVisibleControlExtender
            ID="AlwaysVisibleControlExtender1"
            runat="server"
            TargetControlID="alwaysVisibleAjaxPanel"
            HorizontalSide="Center"
            HorizontalOffset="150"
            VerticalSide="Middle"
            VerticalOffset="0">
        </ajaxToolkit:AlwaysVisibleControlExtender>
    </ProgressTemplate>
</asp:UpdateProgress>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="HiddenField2" runat="server" />
