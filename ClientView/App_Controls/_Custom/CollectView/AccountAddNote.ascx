﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AccountAddNote.ascx.vb" Inherits="App_Controls__Custom_CollectView_AccountAddNote" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script type="text/javascript">
    function OpenAddNoteModal() {
        $("#ModalAccountAddNote").modal('show');
    }
    function ModalHide() {
        // alert('hello');

        // $("#btnclose").click();
        document.getElementById('<%=CancelButton.ClientID%>').click();        
        //ActivateTab('Notes');
        $("#ModalAccountAddNote").modal('hide');      
    }
    function ModalSave() {
        document.getElementById('<%=btnSave.ClientID%>').click();
        $("#ModalAccountAddNote").modal('hide');
    }
</script>
<%--<asp:LinkButton ID="btnAddNew" CssClass="button" runat="server" Text="Add New Note"  />--%>
<asp:Button ID="btnAddNew" CssClass="button" runat="server" Text="Add Note" Style="display: none;" />
<input type="button" id="btnAccountNote" onclick="OpenAddNoteModal();" value="Add Note" class="btn btn-primary" />
<div class="modal fade modal-primary" id="ModalAccountAddNote" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose" onclick="ModalHide();">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Add Note</h4>
                <asp:HiddenField ID="BatchJobId" runat="server" />
                <asp:HiddenField ID="ItemId" runat="server" />

            </div>
            <div class="modal-body">
                <%--  <div id="modcontainer" style="MARGIN: 0px  0px 0px 0px; width: 100%;">--%>

                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">

                            <div class="col-md-12">
                                <asp:TextBox ID="Note" runat="server" Height="100px" TextMode="MultiLine" Width="500px" CssClass="textboxnote"></asp:TextBox>
                                <asp:Button ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " CausesValidation="False" Style="display: none;" />

                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>

                </div>

                <%-- </div>--%>
            </div>
            <div class="modal-footer">
               <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                  <button type="button" class="btn btn-primary" data-dismiss="modal"  id="btnSaves" style="display:none;" onclick="ModalSave();">
                    SAVE
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="ModalHide();">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:Panel ID="panNewNote" runat="server" Style="display: none"
    Width="400px" Font-Bold="True">
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="" OkControlID="" DropShadow="true"
    PopupControlID="panNewNote" TargetControlID="btnAddNew">
</ajaxToolkit:ModalPopupExtender>
