﻿Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.Data


Partial Class App_Controls__Custom_CollectView_NewAccount_New
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchDebtAccount
    Dim itemid As String
    Dim ContractId As Integer
    Protected WithEvents btnPrntDocs As LinkButton
    Dim myContractInfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientContract

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        itemid = qs.GetParameter("ItemId")
        Me.ValidationSummary1.Visible = False

        If Me.Page.IsPostBack = False Then
            Session("doc") = "0"
            Session("submit") = "0"
            Me.MultiView1.SetActiveView(Me.View1)
            If itemid > 0 Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("ItemId"), myitem)
            Else
                myitem.SubmittedByUserId = Me.MyPage.CurrentUser.Id
                myitem.BatchId = -2
                myitem.SubmittedOn = Now()
                myitem.ClientId = Me.UserInfo.Client.ClientId
                myitem.ContractId = Me.UserInfo.CollectionContractId.Id
                myitem.BatchType = "COLLWEB"
                CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Create(myitem)
            End If
            FillClientListByUser()
            LoadItem()
        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Session("submit") = "1"
        Session("doc") = "0"
        Me.Page.Validate()
        If Me.Page.IsValid = False Then
            Me.ValidationSummary1.Visible = True
            Exit Sub
        End If

        If Me.TotAsgAmt.Value < 1 Then
            Me.ValidationSummary1.Visible = True
            Me.CustomValidator1.IsValid = False
            Me.CustomValidator1.ErrorMessage = "A Balance Is Required"
            Exit Sub
        End If

        With myitem
            .AccountNo = Me.AccountNo.Value
            .DebtorName = Me.DebtorName.Value
            .ContactName = Me.ContactName.Value
            .BranchNum = Me.BranchNum.Value
            .AddressLine1 = Me.AddressLine1.Value
            .AddressLine2 = Me.AddressLine2.Value
            .City = Me.City.Value
            .State = Me.State.Value
            .PostalCode = Me.PostalCode.Value
            .PhoneNo = Utils.GetPhoneNo(Me.PhoneNo.Value)
            .Fax = Utils.GetPhoneNo(Me.Fax.Value)
            .Email = Me.Email.Value
            .ReferalDate = Today
            .LastServiceDate = Utils.GetDate(Me.LastServiceDate.Value)
            .AccountNote = Me.DebtAccountNote.Value
            .TotAsgAmt = Me.TotAsgAmt.Value
            .IsTenDayContactPlan = Me.IsTenDayContactPlan.Checked
            .SubmittedByUserId = Me.MyPage.CurrentUser.Id
            .BatchId = -1
            .SubmittedOn = Now()

            .ClientId = ClientTableId.SelectedValue

            .ContractId = GetContractId()
            .BatchType = "COLLWEB"

        End With

        If Me.ViewState("BatchDebtAccountId") > 0 Then
            myitem.BatchDebtAccountId = Me.ViewState("BatchDebtAccountId")
            CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Update(myitem)
        Else
            CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.Create(myitem)
        End If

        Dim sreport As String = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetAckReport(myitem.BatchDebtAccountId, True)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub

    Private Sub FillClientListByUser()

        ClientTableId.DataSource = CRFS.ClientView.CrfsBll.GetClientListForUser(Me.MyPage.CurrentUser.Id)
        ClientTableId.DataTextField = "ClientName"
        ClientTableId.DataValueField = "ClientId"
        ClientTableId.DataBind()


    End Sub

    Private Function GetContractId() As Integer
        Dim dt As New DataTable
        dt = CRFS.ClientView.CrfsBll.GetContractInfoByClientId(ClientTableId.SelectedValue)
        Return dt.Rows(0)("Id")
    End Function

    Private Sub LoadItem()
        With myitem
            Me.AccountNo.Value = .AccountNo
            Me.DebtorName.Value = .DebtorName
            Me.ContactName.Value = .ContactName
            Me.BranchNum.Value = .BranchNum
            Me.AddressLine1.Value = .AddressLine1
            Me.AddressLine2.Value = .AddressLine2
            Me.City.Value = .City
            Me.State.Value = .State
            Me.PostalCode.Value = .PostalCode
            Me.PhoneNo.Value = Utils.FormatPhoneNo(.PhoneNo)
            Me.Fax.Value = Utils.FormatPhoneNo(.Fax)
            Me.Email.Value = .Email
            Me.LastServiceDate.Value = .LastServiceDate
            Me.DebtAccountNote.Value = .AccountNote
            'Me.TotAsgAmt.Value = Utils.GetDecimal(Me.TotAsgAmt.Value)
            Me.TotAsgAmt.Value = .TotAsgAmt
            Me.ClientTableId.SelectedValue = .ClientId
            Me.IsTenDayContactPlan.Checked = .IsTenDayContactPlan
        End With

        '   4/9/2013
        myContractInfo = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetClientContractInfo(myitem.ContractId)
        If myContractInfo.IsTenDayContactPlan = True Then
            Me.IsTenDayContactPlan.Visible = True
        Else
            Me.IsTenDayContactPlan.Visible = False
        End If

        Me.ViewState("BatchDebtAccountId") = myitem.BatchDebtAccountId
        'markparimal-uploadfile:BatchDebtAccountId value is taken in session so that it can be used in file upload page.
        Session("BatchDebtAccountId") = myitem.BatchDebtAccountId
        'Me.Page.Validate()

        'markparimal-uploadfile:To fill Grid on page load .
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where BatchDebtAccountId = " & myitem.BatchDebtAccountId
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()

    End Sub

    Private Sub SaveItem()

    End Sub
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?collectview.newaccountlist"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub

    Protected Sub gvwDocs_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntDocs"), LinkButton).Click, AddressOf btnPrntDocs_Click

        End If
    End Sub
    Protected Sub gvwDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntDocs = TryCast(e.Row.FindControl("btnPrntDocs"), LinkButton)
            btnPrntDocs.Attributes("rowno") = DR("Id")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
        End If
    End Sub

    Protected Sub btnPrntDocs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntDocs.Click
        'markparimal-uploadfile:To display selected upload file .converts file from byte 

        btnPrntDocs = TryCast(sender, LinkButton)
        'Me.ViewState("VIEW") = "Docs"
        Session("doc") = "1"
        Session("submit") = "0"


        Dim myitem As New TABLES.DebtAccountAttachments
        ProviderBase.DAL.Read(btnPrntDocs.Attributes("rowno"), myitem)

        Dim sreport As String = "~/UserData/output/" & myitem.Id & "-" & myitem.FileName
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.DocumentImage, Me.MapPath(sreport))

        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub btnFileUpLoadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadCancel.Click
        'markparimal-uploadfile:To refresh Grid values .
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from DebtAccountAttachments "
        MYSQL += " Where BatchDebtAccountId = " & Session("BatchDebtAccountId")
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModalFileUpload','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
    End Sub

    Protected Sub btnExitViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitViewer.Click
        If Session("submit") = "1" Then
            Response.Redirect("~/MainDefault.aspx?collectview.newaccountlist")
        End If

        If Session("doc") = "1" Then
            Me.MultiView1.SetActiveView(Me.View1)
            'tabnewcollectacc.ActiveTabIndex = 1//Commented by Jaywanti on 08-12-2015
            '' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        Else
            Me.MultiView1.SetActiveView(Me.View1)
            ' tabnewcollectacc.ActiveTabIndex = 0//Commented by Jaywanti on 08-12-2015

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub

    Protected Sub ClientTableId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClientTableId.SelectedIndexChanged
    End Sub


End Class
