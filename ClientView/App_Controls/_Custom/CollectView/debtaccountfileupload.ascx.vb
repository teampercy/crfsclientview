Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Imports System.Configuration
Imports System.Net.Mail

Partial Class App_Controls__Custom_CollectView_DebtAccountFileUpload
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Private _FileUploadextension As String
    Public Property NewProperty() As String
        Get
            Return _FileUploadextension
        End Get
        Set(ByVal value As String)
            _FileUploadextension = value
        End Set
    End Property

    Protected Sub btnFileUpLoadOk_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'Protected Sub btnFileUpLoadOk_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadOk.Click
        Try
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showLodingImage", "showLodingImage();", True)
            Dim str As String
            Dim debtaccountid As String
            Dim usercode As String
            Dim fileName As String
            Dim userid As Integer
            Dim BatchDebtAccountId As Integer
            Dim Todaydate As Date

            'markparimal-uploadfile:check whether document type is selected or not
            'Commented by Jaywanti
            'If ddldoctype.SelectedIndex = 0 Then
            '    lblmsg.Text = " Select Document Type"
            '    Return
            'Else
            '    lblmsg.Text = ""
            'End If

            If hdnDocType.Value = "--Select--" Or hdnDocType.Value = "--SELECT--" Then
                lblmsg.Text = " Select Document Type"
                Return
            Else
                lblmsg.Text = ""
            End If

            'If Session("UploadedFileExtension") Is Nothing Then
            '    lblmsg.Text = " Please Select File to upload "
            '    Return
            'End If
            'Comment by jaywanti on 10-09-2016
            'Dim UploadedFile As HttpPostedFile = Request.Files("fileDragnDrop")
            'str = UploadedFile.FileName
            'Dim fileData As Byte() = New Byte(UploadedFile.InputStream.Length - 1) {}
            'UploadedFile.InputStream.Read(fileData, 0, fileData.Length)
            'fileName = System.IO.Path.GetFileName(UploadedFile.FileName)
            ' ^^***Comment by Pooja on 09-03-20 ***^^
            'Dim fileData As Byte()
            'If Not Session("UploadedFileName") Is Nothing Then
            '    fileName = Session("UploadedFileName")
            'End If
            'If Not Session("UploadedFileDataBytes") Is Nothing Then
            '    fileData = Session("UploadedFileDataBytes")
            'End If

            '^^***CreatedBy Pooja 09/03/2020 ***^^
            If Context.Request.Files.Count > 0 Then
                Dim files As HttpFileCollection = Context.Request.Files
                For i As Integer = 0 To files.Count - 1
                    Dim file As HttpPostedFile = files(i)
                    Dim fname As String
                    If HttpContext.Current.Request.Browser.Browser.ToUpper() = "IE" OrElse HttpContext.Current.Request.Browser.Browser.ToUpper() = "INTERNETEXPLORER" Then
                        Dim testfiles As String() = file.FileName.Split(New Char() {"\"c})
                        fname = testfiles(testfiles.Length - 1)
                    Else
                        fname = file.FileName
                    End If
                    Dim fileData As Byte() = New Byte(file.InputStream.Length - 1) {}
                    file.InputStream.Read(fileData, 0, fileData.Length)

                    Dim objuploadFile As New UploadFile
                    Dim uploadFiles As List(Of UploadFile) ' = New List(Of UploadFile)()
                    uploadFiles = Context.Session("UploadFiles")
                    If uploadFiles Is Nothing Then
                        uploadFiles = New List(Of UploadFile)()
                    End If
                    objuploadFile.UploadedFileName = fname
                    objuploadFile.UploadedFileDataBytes = fileData
                    objuploadFile.UploadedFileExtension = System.IO.Path.GetExtension(file.FileName)

                    If Not objuploadFile.UploadedFileDataBytes Is Nothing Then
                        fileData = objuploadFile.UploadedFileDataBytes
                    End If
                    If Not objuploadFile.UploadedFileName Is Nothing And Not String.IsNullOrEmpty(objuploadFile.UploadedFileName) Then
                        fileName = objuploadFile.UploadedFileName
                    Else
                        lblmsg.Text = "Please select a file to upload"
                        Return
                    End If
                    '^^***    ***^^
                    usercode = CurrentUser.UserName.ToString

                    'markparimal-uploadfile:sets current user
                    userid = CurrentUser.Id

                    'markparimal-uploadfile:sets Filename
                    ''fileName = FileUpLoad1.FileName



                    'markparimal-uploadfile:sets todaydate
                    Todaydate = txtdate.Text

                    If Session("bitDebtAccountId") = 0 Then
                        debtaccountid = 0
                        BatchDebtAccountId = Session("BatchDebtAccountId")
                    Else
                        debtaccountid = Session("debtaccountid")
                        BatchDebtAccountId = 0
                    End If
                    '^^***CreatedBy Pooja 09/03/2020 ***^^
                    Dim DescriptionWithFileName = "[Filename: " + fileName + "]  " + Txtmiscinfo.Text
                    '^^***end  ***^^
                    'CRFS.ClientView.CrfsBll.InsertDebtAccountAttachments(debtaccountid, userid, usercode, Todaydate, fileName, ddldoctype.SelectedValue, Txtmiscinfo.Text, fileData, BatchDebtAccountId) 'Commented by jaywanti
                    'CRFS.ClientView.CrfsBll.InsertDebtAccountAttachments(debtaccountid, userid, usercode, Todaydate, fileName, hdnDocType.Value, Txtmiscinfo.Text, fileData, BatchDebtAccountId)'Commented by Pooja

                    '^^***CreatedBy Pooja 09/03/2020 ***^^
                    CRFS.ClientView.CrfsBll.InsertDebtAccountAttachments(debtaccountid, userid, usercode, Todaydate, fileName, hdnDocType.Value, DescriptionWithFileName, fileData, BatchDebtAccountId)
                    '^^***end  ***^^

                Next
            End If
            If Session("bitDebtAccountId") = 0 Then
                lblmsg.Text = "File Uploaded For  Temp Debtor#" + " " + BatchDebtAccountId.ToString()
            Else
                lblmsg.Text = "File Uploaded For Debtor#" + " " + debtaccountid.ToString()
            End If

            '4/14/2015 
            If Session("bitDebtAccountId") = "1" Then
                Dim myitem As New TABLES.DebtAccount
                ProviderBase.DAL.Read(debtaccountid, myitem)
                Dim ssubject = "Document Sent on Debtor# " & debtaccountid
                Dim MYSQL As String = "Select LocationEmail from Location "
                MYSQL += " Where LocationId = " & myitem.LocationId
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                If MYDT.Tables.Count > 0 Then
                    'myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
                    Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                    EmailToUser(myview.RowItem("Locationemail"), ssubject)  'Comment for a timing by Jaywanti on 15-04-2016
                End If
            End If

        Catch ex As Exception

        End Try
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideLodingImage", "hideLodingImage();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindFileUoload", "BindFileUoload();", True)

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.Page.IsPostBack = False Then
            txtdate.Text = DateTime.Today.ToString("MM/dd/yyyy")
        Else
            ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If

    End Sub
    Public Shared Function GetSettings() As TABLES.Report_Settings
        Dim myitem As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, myitem)

        Return myitem

    End Function
    Public Shared Sub EmailToUser(ByVal email As String, ByVal subject As String)

        Dim MYSITE As TABLES.Report_Settings = GetSettings()

        Dim msg As New MailMessage

        msg.From = New MailAddress("CRFIT@crfsolutions.com", "ClientView")
        msg.Subject = subject
        msg.To.Add(email)
        msg.Subject = subject
        msg.IsBodyHtml = True
        'msg.Body = body

        Try
            With MYSITE
                Dim _smtp As New SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
            End With
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            MsgBox("error on email:" & vbCrLf & mymsg)

        End Try

    End Sub


End Class

