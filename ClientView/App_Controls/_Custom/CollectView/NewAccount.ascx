﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="NewAccount.ascx.vb" Inherits="App_Controls__Custom_CollectView_NewAccount" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<style type="text/css">

#clsdropdownClient .btn-group {width:100%;}
#clsdropdownClient .btn-group .btn {width:90%;}
#clsdropdownClient .btn-group .btn.dropdown-toggle {width:100%;}
#clsdropdownClient .btn-group .dropdown-menu {width:100%;}
</style>
<script language="javascript" type="text/javascript">
    $(document).ready(function () {
        BindStateDropdown();
     });
    function close() {
        window.close();
    }

    //'^^^***CreatedBy Pooja   10/13/20 *** ^^^

    function Clear() {
        debugger;

        $("#Iframe1").attr("src", "App_Controls/_Custom/LienView/Default.aspx?id=0&debtaccountid=0");
        //setSelectedDocTypeId('--SELECT--');
        $("#ModalFileUpload").modal('hide');

    }
    //'^^^***      *** ^^^

    function Balance_LostFocus() {
        var BalanceCntrl = $('#<%=Balance.ClientID%>');
        if ( BalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(BalanceCntrl.val().replace(",", ""))
            BalanceCntrl.val('$' + value.toFixed(2));
        }
    }

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('CollectView', 'Accounts', 'Place Accounts');
        MainMenuToggle('liAccounts');
        SubMenuToggle('liPlaceAccounts');
        //$('#liAccounts').addClass('site-menu-item has-sub active open');
        //$('#liPlaceAccounts').addClass('site-menu-item active');
        return false;

    }
    function BindClientDropdown(Id) {
        //alert('hii');

        PageMethods.FillClientListByUser(Id, LoadClientList);
    }
    function BindFileUoloadList() {
        document.getElementById('<%=btnFileUpLoadCancel.ClientID%>').click();
    }
    function LoadClientList(result) {
        // alert('hii12');
        $('#btndrpClient li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnClientSelectedId.ClientID%>').val() == "" || $('#' + '<%=hdnClientSelectedId.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnClientSelectedId.ClientID%>').val(result[i].Value);
                    $('#' + '<%=hdnClientSlectedIdText.ClientID%>').val(result[i].Text);
                }
                var ResultReplace = ReplaceSpecChar(result[i].Text);
                $('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClient_' + result[i].Value + '" onclick="setSelectedClientId(' + result[i].Value + ',\'' + ResultReplace + '\')">' + result[i].Text + '</a></li>');
                $('#' + '<%=hdnClientSlectedIdText.ClientID%>').val($('#liClient_' + $('#' + '<%=hdnClientSelectedId.ClientID%>').val()).text());
                //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
            }
        }
        $('#btnClientDropDownSelected').html($('#liClient_' + $('#' + '<%=hdnClientSelectedId.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedClientId(id, TextData) {
        // alert(TextData);
        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnClientSelectedId.ClientID%>').val(id);
        $('#' + '<%=hdnClientSlectedIdText.ClientID%>').val(ResultReplace);
        $('#btnClientDropDownSelected').html(TextData + "<span class='caret' ></span>");
        return false;
    }

    function ReplaceSpecChar(Textdata) {
        //alert(Textdata);
        var result = Textdata;
        var ResultReplace = result.replace("'", "`");
        //alert("ResultReplace" + ResultReplace);
        return ResultReplace;
    }

    function SetTabName(id) {
        //   alert('hh');
        // alert(id);
        document.getElementById('<%=hddbTabName.ClientID%>').value = id;
    }
    function ActivateTab(TabName) {
        // alert(TabName);
        // debugger;
        //alert('hhh');
        // alert(TabName);
        switch (TabName) {
            case "MainInfo":
                if (!$('#MainInfo').hasClass('active')) {
                    $('#MainInfo').addClass('active');
                }
                if (!$('#exampleTabsOne').hasClass('active'))
                    $('#exampleTabsOne').addClass('active');
                $('#Docs').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                break;

            case "Docs":
                if (!$('#Docs').hasClass('active')) {
                    $('#Docs').addClass('active');
                }
                if (!$('#exampleTabsTwo').hasClass('active'))
                    $('#exampleTabsTwo').addClass('active');
                $('#MainInfo').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                break;
        }
    }

    function AlertSuccessFunc() {
        swal({
            title: "Successfully Submitted!",
            text: "Your New Account Has Been Successfully Submitted!",
            confirmButtonClass: "btn btn-success",
            cancelButtonText: "Add New Account",
            confirmButtonText: 'View Report',
            type: "success"
        });
    }

    function Successpopup() {
        // alert($('#' + '<%=hdnIsFileUploadDeleted.ClientID%>').val());
        if ($('#' + '<%=hdnIsFileUploadDeleted.ClientID%>').val() != "1") {
            $("#ModaldivReportDetail").modal('show');
        }
        $('#' + '<%=hdnIsFileUploadDeleted.ClientID%>').val("0")
        return false;
    }
    function Cancelpopup() {
        location.href = "MainDefault.aspx?collectview.newaccount&itemid=-1";
        return false;
    }
    function DeleteAccount(id) {
        document.getElementById('<%=hdnDocId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteDoc.ClientID%>').click();
      });
        return false;
    }
    function BindStateDropdown() {
        $('#ulDropDownState li').remove();
        var x = document.getElementById('<%=StateTableId.ClientID%>');
        var txt = "";
        var i;
        for (i = 0; i < x.length; i++) {
            $('#ulDropDownState').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liStateSel_' + x.options[i].value + '" onclick="setSelectedStateId(\'' + x.options[i].value + '\',\'' + x.options[i].text + '\')">' + x.options[i].text + '</a></li>');
        }
        if ($('#' + '<%=hdnStateSelectedvalue.ClientID%>').val() != "") {
            $('#btnDropState').html($('#liStateSel_' + $('#' + '<%=hdnStateSelectedvalue.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedStateId(id, TextData) {
        //debugger;
        $('#' + '<%=hdnStateSelectedvalue.ClientID%>').val(id);
        $('#btnDropState').html(TextData + "<span class='caret' ></span>");
         return false;
     }
</script>

<style type="text/css">
    .labelAlignLeft {
        text-align: left !important;
        font-weight: normal !important;
    }
</style>

<div id="modcontainer" style="width: 100%;">
    <h1 class="panelheader">New Account</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12" style="text-align: left;">
                            <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to List"
                                CausesValidation="False" /><asp:HiddenField runat="server" ID="hddbTabName" Value="MainInfo" />
                        </div>
                    </div>
                </div>


                <div class="nav-tabs-horizontal">
                    <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                        <li id="MainInfo" class="active" role="presentation"><a data-toggle="tab" onclick="SetTabName('MainInfo');" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Main Info</a></li>
                        <li id="Docs" role="presentation" class=""><a data-toggle="tab" onclick="SetTabName('Docs');" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Docs</a></li>
                    </ul>
                    <div class="tab-content padding-top-20">
                        <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Client#:</label>
                                    <div class="col-md-5" style="text-align: left; display: flex;" id="clsdropdownClient">
                                        <div class="btn-group" style="text-align: left;" align="left;">
                                            <button type="button" class="btn btn-default dropdown-toggle" style="text-align: left;" align="left"
                                                data-toggle="dropdown" aria-expanded="false" id="btnClientDropDownSelected">
                                                --Select Client--
                     
                                                                                    <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="btndrpClient" style="overflow-y: auto; height: 150px;">
                                                <%--  <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>
                                                                                    <li class="divider" role="presentation"></li>
                                                                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem">Separated link</a></li>--%>
                                            </ul>
                                        </div>
                                        <asp:HiddenField ID="hdnClientSelectedId" runat="server" Value="" />
                                        <asp:HiddenField ID="hdnClientSlectedIdText" runat="server" />
                                        <cc1:ExtendedDropDown ID="ClientTableId" CssClass="btn btn-default dropdown-toggle" runat="server"
                                            AutoPostBack="True" BackColor="White" CaseSensitiveKeySort="False" ErrorMessage=""
                                            IsValid="True" Tag="" Style="display: none;">
                                        </cc1:ExtendedDropDown>
                                        <%--       <asp:DropDownList ID="drp1" runat="server" CssClass="btn btn-default dropdown-toggle">
                                                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                                <asp:ListItem Text="2" Value="1"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                            <div class="col-md-3">
                                                                                <div class="btn-group">
                                                                                    <button type="button" class="btn btn-default dropdown-toggle" id="Button1"
                                                                                        data-toggle="dropdown" aria-expanded="false">
                                                                                        Animation
                     
                                                                                        <span class="caret"></span>
                                                                                    </button>
                                                                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu">
                                                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem">Action</a></li>
                                                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem">Another action</a></li>
                                                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem">Something else here</a></li>

                                                                                    </ul>
                                                                                </div>
                                                                            </div>--%>
                                       
                                    </div>
                                    <div class="col-md-3">
                                         <div class="checkbox-custom checkbox-default" style="margin-left: 10px;">
                                            <asp:CheckBox ID="IsTenDayContactPlan" CssClass="redcaption" runat="server" Text="  10 Day Free Demand" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Account#:</label>
                                    <div class="col-md-2">
                                        <SiteControls:DataEntryBox ID="AccountNo" ErrorMessage="Account#" DataType="Any"
                                            IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Branch/Region#:</label>
                                    <div class="col-md-2">
                                        <SiteControls:DataEntryBox ID="BranchNum" ErrorMessage="Region#" DataType="Any"
                                            IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Debtor Name:</label>
                                    <div class="col-md-5">
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="DebtorName" runat="server" CssClass="form-control" DataType="Any"
                                                ErrorMessage="Name" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="true" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        </span>
                                        <%-- <SiteControls:DataEntryBox ID="DebtorName1" ErrorMessage="Name" DataType="Any"
                                            IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />--%>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Contact Name:</label>
                                    <div class="col-md-5">
                                        <SiteControls:DataEntryBox ID="ContactName" ErrorMessage="ContactName"
                                            DataType="Any" IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Address 1:</label>
                                    <div class="col-md-5">
                                        <%-- <SiteControls:DataEntryBox ID="AddressLine11" ErrorMessage="Addr#1" DataType="Any"
                                            IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />--%>
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="AddressLine1" runat="server" CssClass="form-control" DataType="Any"
                                                ErrorMessage="Addr#1" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="true" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Address 2:</label>
                                    <div class="col-md-5">
                                        <SiteControls:DataEntryBox ID="AddressLine2" ErrorMessage="Addr#2" DataType="Any"
                                            IsRequired="false" IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">City:</label>
                                    <div class="col-md-5">
                                        <%-- <SiteControls:DataEntryBox ID="City1" ErrorMessage="City" MaxLength="20"
                                            DataType="Any" IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />--%>
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="City" runat="server" CssClass="form-control" DataType="Any" MaxLength="20"
                                                ErrorMessage="Addr#1" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="true" IsValid="True" ValidationExpression="" Value=""></cc1:DataEntryBox>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">State:</label>
                                    <div class="col-md-5" style="text-align:left;">
                                        <%-- <SiteControls:DataEntryBox ID="State" ErrorMessage="State" MaxLength="2"
                                            DataType="Any" IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />--%>
                                        <span style="display: flex;">
                                          
                                        </span>
                                        <cc1:ExtendedDropDown  ID="StateTableId" runat="server"  CssClass="form-control" Style="display: none;">
                                        </cc1:ExtendedDropDown>
                                        <asp:HiddenField ID="hdnStateSelectedvalue" runat="server" />
                                        <div class="btn-group" style="text-align: left;" align="left">
                                            <button type="button" class="btn btn-default dropdown-toggle"  align="left"
                                                data-toggle="dropdown" aria-expanded="false" id="btnDropState" tabindex="8">
                                                --Select State--
                     
                                                                                    <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState" style="overflow-y: auto; height: 250px;">
                                            </ul>
                                        </div>
                                          <cc1:DataEntryBox ID="State" runat="server" CssClass="form-control" DataType="Any" MaxLength="2"
                                                ErrorMessage="Addr#1" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                FriendlyName="" IsRequired="true" IsValid="True" ValidationExpression="" Value="" style="display:none;"></cc1:DataEntryBox>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Zip Code:</label>
                                    <div class="col-md-2">
                                        <%--  <SiteControls:DataEntryBox ID="PostalCode" ErrorMessage="ZipCode" MaxLength="10"
                                            DataType="Any" IsRequired="true" IsMasked="False" runat="server" CSSClass="form-control" />--%>
                                        <span style="display: flex;">
                                            <cc1:DataEntryBox ID="PostalCode" runat="server" CssClass="form-control" DataType="Any" MaxLength="10"
                                                ErrorMessage="Addr#1" EnableClientSideScript="False" FormatMask="" FormatString="" Width="120px"
                                                FriendlyName="" IsRequired="true" IsValid="True" ValidationExpression="" Value="" ></cc1:DataEntryBox>
                                        </span>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Last Charge Date:</label>
                                    <div class="col-md-5">
                                        <span style="display: flex;">
                                            <SiteControls:Calendar ID="LastServiceDate" Value="TODAY" IsReadOnly="false" IsRequired="true"
                                                ErrorMessage="Last Service" runat="server" />
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Balance:</label>
                                    <div class="col-md-2">
                                        <span style="display: flex;">
                                        <cc1:DataEntryBox ID="Balance" runat="server" CssClass="form-control" DataType="Money"
                                            Tag="Balance" Align="right" ErrorMessage="Balance" IsRequired="true" IsValid="True" Width="120px" >$0.00</cc1:DataEntryBox> </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Phone#:</label>
                                    <div class="col-md-2">
                                        <SiteControls:DataEntryBox ID="PhoneNo" ErrorMessage="Phone#" DataType="PhoneNo" CSSClass="form-control"
                                            IsRequired="FALSE" IsMasked="true" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Fax:</label>
                                    <div class="col-md-2">
                                        <SiteControls:DataEntryBox ID="Fax" ErrorMessage="Fax#" DataType="PhoneNo" CSSClass="form-control"
                                            IsRequired="false" IsMasked="true" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Email:</label>
                                    <div class="col-md-5">
                                        <SiteControls:DataEntryBox ID="Email" ErrorMessage="Email" DataType="Any" CSSClass="form-control"
                                            IsRequired="FALSE" IsMasked="False" runat="server" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Special Instruction:</label>
                                    <div class="col-md-5">
                                        <SiteControls:DataEntryBox ID="DebtAccountNote" ErrorMessage="SpecialInstructions"
                                            TextMode="MultiLine" MaxLength="255" DataType="Any" IsRequired="false"
                                            IsMasked="False" runat="server" CSSClass="form-control" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-md-12" style="text-align: left;">
                                        <asp:Button ID="btnAddDocuments" runat="server" CssClass="btn btn-primary" CausesValidation="False"
                                            Text="Add Document" Visible="false" />
                                        <asp:Button ID="btnAddDocumentsNew" runat="server" OnClientClick="$('#ModalFileUpload').modal('show');return false;" CssClass="btn btn-primary" CausesValidation="False"
                                            Text="Add Document" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div style="width: auto; height: auto; overflow: auto;">
                                            <asp:GridView ID="gvwDocs" runat="server" CssClass="table dataTable table-striped" Width="100%" PageSize="20"
                                                AutoGenerateColumns="False">
                                                <AlternatingRowStyle CssClass="altrowstyle"></AlternatingRowStyle>
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Print">
                                                        <ItemTemplate>
                                                            &nbsp;&nbsp;&nbsp;
                                                                                                  <asp:LinkButton ID="btnPrntDocs" CausesValidation="false" CssClass="icon ti-printer" runat="server" />
                                                            <%--   <asp:ImageButton ID="btnPrntDocs" CausesValidation="false" CssClass="gridbutton"
                                                                                                    ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                                        </ItemTemplate>
                                                        <ItemStyle Wrap="False" Width="40px"></ItemStyle>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="DateCreated" HeaderText="Date Added" SortExpression="DateCreated">
                                                        <ItemStyle HorizontalAlign="Center" Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UserCode" HeaderText="User" SortExpression="UserCode">
                                                        <ItemStyle Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DocumentType" HeaderText="Type" SortExpression="DocumentType">
                                                        <ItemStyle Wrap="False" Height="15px" Width="70px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DocumentDescription" HeaderText="Description" SortExpression="DocumentDescription">
                                                        <ItemStyle Wrap="False" Height="15px" Width="200px"></ItemStyle>
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Delete">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <b>No Items found for the specified criteria</b>
                                                </EmptyDataTemplate>
                                                <%-- <HeaderStyle CssClass="headerstyle"></HeaderStyle>
                                            <RowStyle CssClass="rowstyle"></RowStyle>--%>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="display: block">


                                <div style="float: left; margin-top: 30px; height: 100%;">

                                    <ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1" runat="server" BehaviorID="FileUpLoad"
                                        TargetControlID="btnAddDocuments" PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground"
                                        OkControlID="" />
                                    <asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none"
                                        Width="452px" Height="100px">
                                        <h1 class="panelheader">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div style="width: 100%;">
                                                        <div style="text-align: center; width: 90%; float: left;">
                                                            Upload Documents
                                                        </div>
                                                        <div style="float: right; width: 10%;">
                                                            <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="Return" CausesValidation="False"
                                                                Width="40px" CssClass="btn btn-primary" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </h1>
                                        <iframe runat="server" id="frm" src="App_Controls/_Custom/LienView/Default.aspx?id=0&debtaccountid=0"
                                            style="width: 450px; height: 270px; background-color: white"></iframe>
                                    </asp:Panel>
                                </div>
                            </div>


                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-12" style="text-align: left;">
                                    <asp:HiddenField ID="hdnIsFileUploadDeleted" runat="server" Value="0" />
                                    <asp:HiddenField ID="hdnDocId" runat="server" Value="0" />
                                    <asp:Button ID="btnDeleteDoc" runat="server" Style="display: none;" OnClick="btnDeleteDoc_Click" />
                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="Please Correct the Following Field(s): " DisplayMode="List" />
                                    <cc1:CustomValidator ID="CustomValidator1" runat="server" Display="Static"></cc1:CustomValidator>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="footer">
                <asp:UpdatePanel ID="updPan1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" CausesValidation="false"
                            Text="Submit Account" />&nbsp;
                        <asp:Button ID="btnRedirectReportView" runat="server" Style="display: none;" OnClick="btnRedirectReportView_Click" />
                        <br />
                        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="TransparentGrayBackground">
                                </div>
                                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                    <div class="PageUpdateProgress">
                                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                                            AlternateText="[image]" />
                                        &nbsp;Please Wait...
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                    TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
                                    VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                &nbsp;<asp:LinkButton ID="btnExitViewer" runat="server" CssClass="btn btn-primary" Text="Exit Viewer"></asp:LinkButton>
                &nbsp;
                <asp:LinkButton ID="btnDownLoad" Text="DownLoad" CssClass="btn btn-primary" runat="server" />
                <br />
                <br />
                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                <br />
                <center>
                </center>
            </div>
        </asp:View>
    </asp:MultiView>
</div>

<div class="modal fade modal-primary" id="ModalFileUpload" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none; width: 100%; height: 100%;" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">UPLOAD DOCUMENT</h4>
            </div>
              <iframe  id="Iframe1" src="App_Controls/_Custom/LienView/Default.aspx?id=0&debtaccountid=0"
           <%-- <iframe id="iframe1" src="App_Controls/_Custom/LienView/Default.aspx?id=0&debtaccountid=0" style="height: 540px; width: 550px; border: 0px;"></iframe>--%>
          style="height: 530px; width: 100%; border: 0px;"></iframe>
            <div class="modal-footer" >
           <%--   <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">--%>

                 <button type="button" class="btn btn-default margin-0" OnClick ="javascript:Clear()" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary example-modal-lg" id="ModaldivReportDetail" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center" style="width: 80%;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Account Detail</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Client#: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblClientNumber" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label">Debtor Name: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblDebtName" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label">Address: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblAdrress1" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Account#: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblAccountNum" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label">Contact Name: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblContactName" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label"></label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblAddress2" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Branch/Region#:</label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblBranchRegion" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label">Last Charge Date: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblLastChargedate" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label">Balance: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblBalance" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Phone#:</label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblPhoneNum" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label">Fax:</label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblFax" runat="server"></asp:Label>

                        </div>
                        <label class="col-md-2 control-label">Email: </label>
                        <div class="col-md-2 labelAlignLeft control-label">

                            <asp:Label ID="lblEmail" runat="server"></asp:Label>

                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Special Instruction</label>
                        <div class="col-md-8 labelAlignLeft control-label">

                            <asp:Label ID="lblSpecialInstruction" runat="server"></asp:Label>
                        </div>
                    </div>



                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="Button1" runat="server" Text="Print" CssClass="btn btn-primary" OnClick="btnRedirectReportView_Click" />
                <button type="button" class="btn btn-primary" data-dismiss="modal">Edit</button>
                <%--  <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>--%>
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-default" OnClick="btnClose_Click" />
            </div>
        </div>
    </div>
</div>

