﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<script runat="server">
   
    
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.DebtAccount
    Dim dsclients As New CRFS.ClientView.CrfsBll
    Protected WithEvents btnDeleteItem As ImageButton
    Protected WithEvents btnEditItem As ImageButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If Page.IsPostBack = False Then
            Me.HiddenField1.Value = Me.MyPage.CurrentUser.Id
            Me.MultiView1.SetActiveView(Me.View1)
            Me.panInsuredSearch.Visible = False
            If Me.UserInfo.CollInfo.ClientAgentCode = False Then
                Me.litDetorName.Text = "Name:"
                Me.litAccountNo.Text = "Account#:"
                Me.panInsuredSearch.Visible = False
            Else
                Me.litDetorName.Text = "Insured Name:"
                Me.litAccountNo.Text = "Policy#:"
                Me.panInsuredSearch.Visible = True
            End If
            
            If qs.HasParameter("page") Then
                GetData(GetFilter, 1, "")
            End If
            FillClientListByUser()
          
        End If
    End Sub
    Private Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), ImageButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), ImageButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
   
    Private Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, ImageButton)
        Dim s As String = "~/MainDefault.aspx?collectview.accountinfo&itemid=" & btnEditItem.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= Me.gvItemList.PageCount Then
            Me.gvItemList.PageIndex = pageNumber - 1
        Else
            Me.gvItemList.PageIndex = 0
        End If
    End Sub
    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), ImageButton)
            btnEditItem.Attributes("rowno") = dr("DebtAccountId")

        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
            
            
        End If

        'If e.Row.RowType = DataControlRowType.Pager Then
        '    Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
        '    lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

        '    Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
        '    txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

        '    Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
        '    ddlPageSize.SelectedValue = gridView.PageSize.ToString()
        'End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            e.Row.Cells(5).Text = "" & Utils.FormatDate(DR("ReferalDate")) & ""
            e.Row.Cells(7).Text = "" & Utils.FormatDecimal(DR("TotBalAmt")) & ""
        End If
    End Sub
    Protected Sub gvItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvItemList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)
   
    End Sub
    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList
        With mysproc
            'MarkParimal: Client Filter-To check whether it is group client
            If Me.panClients1.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                End If
            End If
            If Me.AccountName.Text.Trim.Length > 0 Then
                If InStr(Me.AccountNameLike.SelectedValue, "Includes") > 0 Then
                    .DebtorName = "%" & Me.AccountName.Text.Trim & "%"
                Else
                    .DebtorName = Me.AccountName.Text.Trim & "%"
                End If
            End If
            If Me.AccountNo.Text.Trim.Length > 0 Then
                If InStr(Me.AccountNoLike.SelectedValue, "Includes") > 0 Then
                    .ACCOUNTNO = "%" & Me.AccountNo.Text.Trim & "%"
                Else
                    .ACCOUNTNO = Me.AccountNo.Text.Trim & "%"
                End If

            End If
            If Me.FileNumber.Text.Trim.Length > 0 Then
                .FileNumber = Me.FileNumber.Text.Trim
            End If

            If Me.ClaimNo.Text.Trim.Length > 0 Then
                If InStr(Me.ClaimNoLike.SelectedValue, "Includes") > 0 Then
                    .ClaimNumber = "%" & Me.ClaimNo.Text.Trim & "%"
                Else
                    .ClaimNumber = Me.ClaimNo.Text.Trim & "%"
                End If

            End If

            If Me.BranchNum.Text.Trim.Length > 0 Then
                .BranchNum = Me.BranchNum.Text.Trim & "%"
            End If

            
            If Me.AgentCode.Text.Trim.Length > 0 Then
                If InStr(Me.AgentCodeLike.SelectedValue, "Includes") > 0 Then
                    .AgentCode = "%" & Me.AgentCode.Text.Trim & "%"
                Else
                    .AgentCode = Me.AgentCode.Text.Trim & "%"
                End If
            End If

            If Me.chkByAssignDate.Checked = True Then
                .ReferalRange = True
                .FromReferalDate = Me.FromAssignDate.Value
                .ThruReferalDate = Me.ThruAssignDate.Value
            End If

            .UserId = Me.CurrentUser.Id
            .Page = 1
            .PageRecords = 20000
            
          
            
               
            
   
        End With
        
        GetData(mysproc, 1, "F")
        
    End Sub
   
    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList
        'MarkParimal: Client Filter-To check whether it is group client
        With mysproc
            If Me.panClients1.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    mysproc.ClientCode = Me.DropDownList1.SelectedValue
                End If
            End If
            .UserId = Me.CurrentUser.Id
            .Page = 1
            .PageRecords = 20000
        End With
        GetData(mysproc, 1, "F")
        
    End Sub

    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(View1)
        
    End Sub

    Protected Sub btnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "F")
    
    End Sub
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "P")
       
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "N")
       
    End Sub
    Protected Sub btnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "L")
   
    End Sub
    Protected Function GetFilter() As Object
        If IsNothing(Session("AccountListP")) = False Then
            Return Session("AccountListP")
        Else
            Return New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList
        End If
    End Function
    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList, ByVal apage As Integer, ByVal adirection As String)
       
        Dim myview As HDS.DAL.COMMON.TableView
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        asproc.PageRecords = 20000
        asproc.UserId = Me.CurrentUser.Id
        If adirection = "F" Then
            asproc.Page = 1
        End If
        If adirection = "N" Then
            asproc.Page += 1
        End If
        If adirection = "P" Then
            asproc.Page -= 1
        End If
        'If adirection = "I" Then
        '    asproc.Page = ddPage.SelectedIndex + 1
        'End If
        
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        If adirection = "L" Then
            myview = Session("AccountList")
            asproc.Page = myview.RowItem("pages")
        End If

        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
        If myview.Count < 1 Then
            Exit Sub
        End If
        
        Dim totrecs As String = myview.RowItem("totalrecords")
        Dim currpage As String = myview.RowItem("page")
        Dim totpages As Integer = (totrecs / 14 + 1)
        totpages = myview.RowItem("pages")
         
        'Me.lblCurrentPage.Text = currpage
        'Me.lblTotalPages.Text = totpages
        'Me.lblTotRecs.Text = totrecs
        Dim i As Integer = 0

        'ddPage.Items.Clear()
        'Do Until i > totpages - 1
        '    i += 1
        '    ddPage.Items.Add(i.ToString)
        'Loop

        'ddPage.SelectedIndex = currpage - 1
        Dim mydt As System.Data.DataTable = CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.GetDataSet(asproc).Tables(0)
        Me.gvItemList.DataSource = mydt
        Me.gvItemList.DataBind()
        If (gvItemList.Rows.Count > 0) Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If
        Me.Session("AccountList") = New HDS.DAL.COMMON.TableView(mydt)
        Session("AccountListP") = asproc
        
        Me.MultiView1.SetActiveView(View2)
   
    End Sub
    'Protected Sub ddPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddPage.SelectedIndexChanged
    '    GetData(GetFilter, ddPage.SelectedIndex, "I")
    'End Sub
    
    Private Sub FillClientListByUser()
        Dim DS As System.Data.DataSet
        panClients1.Visible = False
        DS = CRFS.ClientView.CrfsBll.GetClientListForUser(Me.MyPage.CurrentUser.Id)
        If DS.Tables(0).Rows.Count > 1 Then
            DropDownList1.DataSource = CRFS.ClientView.CrfsBll.GetClientListForUser(Me.MyPage.CurrentUser.Id)
            DropDownList1.DataTextField = "ClientName"
            DropDownList1.DataValueField = "ClientId"
            DropDownList1.DataBind()
            panClients1.Visible = True
            
        End If
        
        
    End Sub
</script>

<asp:ObjectDataSource ID="ItemsDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetAccountList" TypeName="CRF.ClientView.BLL.CollectView.Provider">
    <SelectParameters>
        <asp:ControlParameter ControlID="HiddenField1" DefaultValue="-1" Name="userid" PropertyName="Value"
            Type="String" />
        <asp:SessionParameter DefaultValue="" Name="filter" SessionField="filter" Type="Object" />
        <asp:SessionParameter Name="sort" SessionField="sort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<div id="modcontainer" style="margin: 10px  0px 0px 0px; width: 720px;">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1 class="panelheader">Account Filter</h1>
            <div class="body" style="float: left;">
                <asp:Panel ID="panClients1" runat="server">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                <asp:Literal ID="Litclient" runat="server" Text="Client Selection:"></asp:Literal></label>

                            <div class="col-md-5">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-5">

                                <div class="radio-custom radio-default">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                        <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL">All Clients</asp:ListItem>
                                        <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED" style="padding-left: 50px;">Selected Only</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>

                            </div>

                        </div>
                    </div>
                </asp:Panel>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            <asp:Literal ID="litDetorName" runat="server" Text="Name:"></asp:Literal></label>

                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="AccountName" CssClass="form-control" />
                        </div>
                        <div class="col-md-5">

                            <div class="radio-custom radio-default">
                                <asp:RadioButtonList ID="AccountNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                    RepeatLayout="Flow">
                                    <asp:ListItem Selected="True">&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                </asp:RadioButtonList>

                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            <asp:Literal ID="litAccountNo" runat="server" Text="Account#"></asp:Literal></label>

                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="AccountNo" CssClass="form-control" />
                        </div>
                        <div class="col-md-5">

                            <div class="radio-custom radio-default">
                                <asp:RadioButtonList ID="AccountNoLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                    RepeatLayout="Flow">
                                    <asp:ListItem Selected="True">&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                    <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                </asp:RadioButtonList>

                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            Assigned Date:</label>

                        <div class="col-md-6">
                            <span style="display: flex;">
                                <SiteControls:Calendar ID="FromAssignDate" Value="TODAY" IsReadOnly="false" IsRequired="true"
                                    ErrorMessage="Thru Date" runat="server" />
                                &nbsp;&nbsp; Thru &nbsp;
                                <SiteControls:Calendar ID="ThruAssignDate" Value="TODAY"
                                    IsReadOnly="false" IsRequired="true" ErrorMessage="Thru Date" runat="server" />
                            </span>
                        </div>
                        <div class="col-md-4">

                            <div class="checkbox-custom checkbox-default">
                                <asp:CheckBox ID="chkByAssignDate" CssClass="redcaption" runat="server" Text="  Filter By Assigned Date Range"
                                    Width="224px" />

                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            Branch/Region#:</label>

                        <div class="col-md-9">
                            <asp:TextBox runat="server" ID="BranchNum" CssClass="form-control" />

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            CRF File#:</label>

                        <div class="col-md-9">
                            <asp:TextBox runat="server" ID="FileNumber" CssClass="form-control" />

                        </div>

                    </div>

                </div>

                <asp:Panel ID="panInsuredSearch" runat="server" Height="50px">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                <asp:Literal ID="Literal3" runat="server" Text="Agent #"></asp:Literal>:</label>

                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="AgentCode" CssClass="form-control" />
                            </div>
                            <div class="col-md-5">

                                <div class="radio-custom radio-default">
                                    <asp:RadioButtonList ID="AgentCodeLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow">
                                        <asp:ListItem Selected="True">&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>

                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                <asp:Literal ID="Literal4" runat="server" Text="Claim #"></asp:Literal>:</label>

                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="ClaimNo" CssClass="form-control" />
                            </div>
                            <div class="col-md-5">

                                <div class="radio-custom radio-default">
                                    <asp:RadioButtonList ID="ClaimNoLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow">
                                        <asp:ListItem Selected="True">&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>

                            </div>

                        </div>
                    </div>

                </asp:Panel>
                <br />
            </div>
            <div class="footer">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">
                                <center>
                        <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="button" OnClick="btnSubmit_Click" />
                        &nbsp;
                        <asp:Button ID="btnViewAll" runat="server" Text="View All" CssClass="button" OnClick="btnViewAll_Click" />
                        &nbsp;
                    </center>
                                <br />
                            </asp:Panel>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <h1 class="panelheader">Account List</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="button" Text="Back to Filter"
                                OnClick="btnBackFilter_Click"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:GridView ID="gvItemList" runat="server" CssClass="table dataTable table-striped"
                                AutoGenerateColumns="False" BorderWidth="1px" Width="100%" >
                                <%--      <RowStyle CssClass="rowstyle" />
                                <AlternatingRowStyle CssClass="altrowstyle" />
                                <HeaderStyle CssClass="headerstyle" />--%>
                                <Columns>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>
<%--                                            <asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png"
                                                runat="server" />--%>
                                            <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="15px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DebtAccountId" SortExpression="DebtAccountId" HeaderText="CRF#">
                                        <ItemStyle Width="40px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client#">
                                        <ItemStyle Width="40px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AccountNO" SortExpression="AccountNo" HeaderText="Account#">
                                        <ItemStyle Width="70px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DebtorName" SortExpression="DebtorName" HeaderText="Debtor Name">
                                        <ItemStyle Width="160px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ReferalDate" SortExpression="ReferalDate" HeaderText="Placed On">
                                        <ItemStyle Width="60px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CollectionStatus" SortExpression="CollectionStatus" HeaderText="Status">
                                        <ItemStyle Width="20px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TotBalAmt" SortExpression="TotBalAmt" HeaderText="Balance">
                                        <ItemStyle Width="50px" HorizontalAlign="right" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <b>No Items found for the specified criteria</b>
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>



            </div>
            <%--     <div class="footer" style="float: right">
                <table width="100%">
                    <tr>
                        <td class="row-label" align="right">(Total Accounts on this List
                            <asp:Label ID="lblTotRecs" runat="server"></asp:Label>
                            ) (Page
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                            of
                            <asp:Label ID="lblTotalPages" runat="server"></asp:Label>)&nbsp; &nbsp;Page: &nbsp;<asp:DropDownList
                                ID="ddPage" runat="server" AutoPostBack="true" Width="48px" OnSelectedIndexChanged="ddPage_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;
                            <asp:ImageButton ID="btnFirst" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_First.png"
                                OnClick="btnFirst_Click" />
                            <asp:ImageButton ID="btnPrevious" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_Previous.png"
                                OnClick="btnPrevious_Click" />
                            <asp:ImageButton ID="btnNext" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_Next.png"
                                OnClick="btnNext_Click" />
                            <asp:ImageButton ID="btnLast" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_Last.png"
                                OnClick="btnLast_Click" />
                        </td>
                    </tr>
                </table>
            </div>--%>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="HiddenField2" runat="server" />
<asp:UpdateProgress ID="siteUpdateProgress" runat="server">
    <ProgressTemplate>
        <div class="TransparentGrayBackground">
        </div>
        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
            <div class="PageUpdateProgress">
                <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                    AlternateText="[image]" />
                &nbsp;Please Wait...
            </div>
        </asp:Panel>
        <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
            TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
            VerticalSide="Middle" VerticalOffset="0">
        </ajaxToolkit:AlwaysVisibleControlExtender>
    </ProgressTemplate>
</asp:UpdateProgress>
