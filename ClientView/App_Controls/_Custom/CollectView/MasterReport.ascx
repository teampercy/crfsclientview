<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Calendar1.Value = Today
            Me.Calendar2.Value = Today
            Me.btnDownLoad.Visible = False
            If IsNothing(Me.Page.Request.QueryString("rpt")) = False Then
                Me.DownLoadReport(Me.Page.Request.QueryString("rpt"))
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
        
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountInventories
        With MYSPROC
            .UserId = Me.MyPage.CurrentUser.Id
            .ReferalFROM = Me.Calendar1.Value
            .ReferalTO = Me.Calendar2.Value
            .SelectByReferalDate = True
            If Me.BranchNum.Text.Trim.Length > 0 Then
                .BranchNum = Me.BranchNum.Text.Trim & "%"
            End If
            If Me.chkActive.Checked = True Then
                .SelectByStatusGroup = 1
                .StatusGroup = 1
            End If
        End With
        
        Dim sreport As String = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetMasterReport(MYSPROC, Me.PDFReport.Checked)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If Me.SpreadSheet.Checked = False Then
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 550, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If
                'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 550, 600, "no"))
                'Me.MultiView1.SetActiveView(Me.View2)
                Me.btnDownLoad.Visible = False
            Else
                Me.btnDownLoad.Visible = True
                Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
                Me.MultiView1.SetActiveView(Me.View2)
            End If
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DownLoadReport(Me.Session("spreadsheet"))
    End Sub
</script>
<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('CollectView', 'Reports', 'Master Reports');
        MainMenuToggle('liCollectViewReport');
        SubMenuToggle('liMasterReports');
        //$('#liCollectViewReport').addClass('site-menu-item has-sub active open');
        //$('#liMasterReports').addClass('site-menu-item active');
        return false;

    }
   
</script>


                                    <div id="modcontainer" style="MARGIN: 0px; WIDTH: 100%;">
                                        <h1 class="panelheader">Master Report</h1>
                                        <asp:MultiView ID="MultiView1" runat="server">
                                            <asp:View ID="View1" runat="server">
                                                <div class="body">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Branch/Region#:</label>
                                                            <div class="col-md-2">
                                                                <asp:TextBox ID="BranchNum" runat="server" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Assigned From:</label>
                                                            <div class="col-md-7">
                                                                <span style="display: flex;">
                                                                    <SiteControls:Calendar ID="Calendar1" Value="TODAY" IsReadOnly="false" IsRequired="true" ErrorMessage="Assigned From" runat="server" />
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Assigned Thru:</label>
                                                            <div class="col-md-7">
                                                                <span style="display: flex;">
                                                                    <SiteControls:Calendar ID="Calendar2" Value="TODAY" IsReadOnly="false" IsRequired="true" ErrorMessage="Assigned Thru" runat="server" />
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">Active Only:</label>
                                                            <div class="col-md-7">

                                                                <div class="checkbox-custom checkbox-default">
                                                                    <asp:CheckBox ID="chkActive" runat="server" Text=" " />
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">PDF Report:</label>
                                                            <div class="col-md-7">
                                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                                    <div class="radio-custom radio-default">
                                                                        <asp:RadioButton ID="PDFReport" runat="server" Checked="True" GroupName="ReportType" Text=" "></asp:RadioButton>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-md-4 control-label">SpreadSheet:</label>
                                                            <div class="col-md-7">
                                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                                    <div class="radio-custom radio-default">
                                                                        <asp:RadioButton ID="SpreadSheet" runat="server" GroupName="ReportType" Text=" "></asp:RadioButton>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <%--<TABLE align="center" width="490">
<tr>
	<TD class="row-label"  style="width: 30% " align="right">Branch/Region#:</TD>
	<td class="row-data" style="width: 200px">
        <asp:TextBox ID="BranchNum" runat="server"></asp:TextBox>
    </td>
</tr>
<TR>
	<TD class="row-label"  style="width: 30% " align="right">From Date:</TD>
	<TD class="row-data" style="width: 200px">
        <SiteControls:Calendar  ID="Calendar1" Value="TODAY" IsReadOnly=false IsRequired=true ErrorMessage="From Date" runat="server" />
  	</TD>
</TR>
<TR>
	<TD class="row-label"  style="width: 30% " align="right">Through Date:</TD>
	<TD class="row-data" style="width: 200px">
 	  <SiteControls:Calendar  ID="Calendar2" Value="TODAY"  IsReadOnly=false IsRequired=true ErrorMessage="Thru Date" runat="server" />
   	</TD>
</TR>
<TR>
	<TD class="row-label"  style="width: 30% " align="right">
        Active Only:</TD>
	<TD class="row-data"  style="width: 200px">
        <asp:CheckBox ID="chkActive" runat="server" /></TD>
</TR>
<TR>
	<TD class="row-label"  style="width: 30% " align="right">PDF Report:</TD>
	<TD class="row-data"  style="width: 200px">
		<asp:RadioButton id="PDFReport" runat="server" Checked="True" GroupName="ReportType" CssClass="label"></asp:RadioButton>
	</TD>
</TR>
<TR>
	<TD class="row-label"  style="width: 30% " align="right">SpreadSheet:</TD>
	<TD class="row-data"  style="width: 200px">
	    <asp:RadioButton id="SpreadSheet" runat="server" GroupName="ReportType"></asp:RadioButton>
	</TD>
</TR>
</TABLE>--%>
                                                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="100%" HeaderText="The Following Field(s) are Required: " />
                                                </div>
                                                <div class="footer">
                                                    <div class="form-group">

                                                        <div class="col-md-12">
                                                            <asp:UpdatePanel ID="updPan1" runat="server">
                                                                <ContentTemplate>
                                                                    <asp:LinkButton ID="btnSubmit"
                                                                        CssClass="btn btn-primary"
                                                                        runat="server" CausesValidation="false"
                                                                        Text="Create Report" OnClick="btnSubmit_Click" />


                                                                    <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                                                                        <ProgressTemplate>
                                                                            <div class="TransparentGrayBackground"></div>
                                                                            <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                                                                <div class="PageUpdateProgress">
                                                                                    <asp:Image ID="ajaxLoadNotificationImage"
                                                                                        runat="server"
                                                                                        ImageUrl="~/images/ajax-loader.gif"
                                                                                        AlternateText="[image]" />
                                                                                    &nbsp;Please Wait...
                                                                                </div>
                                                                            </asp:Panel>
                                                                            <ajaxToolkit:AlwaysVisibleControlExtender
                                                                                ID="AlwaysVisibleControlExtender1"
                                                                                runat="server"
                                                                                TargetControlID="alwaysVisibleAjaxPanel"
                                                                                HorizontalSide="Center"
                                                                                HorizontalOffset="150"
                                                                                VerticalSide="Middle"
                                                                                VerticalOffset="0">
                                                                            </ajaxToolkit:AlwaysVisibleControlExtender>
                                                                        </ProgressTemplate>
                                                                    </asp:UpdateProgress>
                                                                </ContentTemplate>
                                                            </asp:UpdatePanel>
                                                        </div>
                                                    </div>
                                                </div>
                                            </asp:View>
                                            <asp:View ID="View2" runat="server">
                                                <div class="body">
                                                    <div class="form-horizontal">
                                                        <div class="form-group">

                                                            <div class="col-md-12" style="text-align: left;">
                                                                <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Selection" OnClick="Linkbutton2_Click"></asp:LinkButton>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <div style="width: auto; height: auto; overflow: auto;">
                                                                    <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">

                                                            <div class="col-md-12">
                                                                <asp:Button ID="btnDownLoad" Text="DownLoad SpreadSheet" CssClass="btn btn-primary" runat="server" OnClick="btnDownLoad_Click" />
                                                            </div>
                                                        </div>
                                                    </div>




                                                </div>
                                            </asp:View>
                                        </asp:MultiView>
                                    </div>

                             
