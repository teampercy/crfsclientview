﻿<%@ WebHandler Language="VB" Class="SayHello" %>

Imports System
Imports System.Web

Public Class SayHello : Implements IHttpHandler
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest
        context.Response.ContentType = "text/plain"
        Dim param As String
        param = context.Request.Params("Name")
        context.Response.Write("Hello World")
        Dim i As Integer = context.Request.Files.Count
        ' HandleMethod(context)
    End Sub
 ' Handle request based on method
   ' Handle request based on method
    'Private Sub HandleMethod(context As HttpContext)
    '    Select Case context.Request.HttpMethod
    '        Case "HEAD", "GET"
    '            If GivenFilename(context) Then
    '                DeliverFile(context)
    '            Else
    '                ListCurrentFiles(context)
    '            End If
    '            Exit Select

    '        Case "POST", "PUT"
    '            UploadFile(context)
    '            Exit Select

    '        Case "DELETE"
    '            DeleteFile(context)
    '            Exit Select

    '        Case "OPTIONS"
    '            ReturnOptions(context)
    '            Exit Select
    '        Case Else

    '            context.Response.ClearHeaders()
    '            context.Response.StatusCode = 405
    '            Exit Select
    '    End Select
    'End Sub
    '' Delete file from the server
    'Private Sub DeleteFile(context As HttpContext)
    '    Dim filePath = StorageRoot & context.Request("f")
    '    If File.Exists(filePath) Then
    '        File.Delete(filePath)
    '    End If
    'End Sub
    'Private Shared Sub ReturnOptions(context As HttpContext)
    '    context.Response.AddHeader("Allow", "DELETE,GET,HEAD,POST,PUT,OPTIONS")
    '    context.Response.StatusCode = 200
    'End Sub

    '' Upload file to the server
    'Private Sub UploadFile(context As HttpContext)
    '    Dim statuses = New List(Of FilesStatus)()
    '    Dim headers = context.Request.Headers

    '    If String.IsNullOrEmpty(headers("X-File-Name")) Then
    '        UploadWholeFile(context, statuses)
    '    Else
    '        UploadPartialFile(headers("X-File-Name"), context, statuses)
    '    End If

    '    WriteJsonIframeSafe(context, statuses)
    'End Sub
    'Private Shared Function GivenFilename(context As HttpContext) As Boolean
    '    Return Not String.IsNullOrEmpty(context.Request("f"))
    'End Function

    'Private Sub DeliverFile(context As HttpContext)
    '    Dim filename = context.Request("f")
    '    Dim filePath = StorageRoot & filename

    '    If File.Exists(filePath) Then
    '        context.Response.AddHeader("Content-Disposition", "attachment; filename=""" & filename & """")
    '        context.Response.ContentType = "application/octet-stream"
    '        context.Response.ClearContent()
    '        context.Response.WriteFile(filePath)
    '    Else
    '        context.Response.StatusCode = 404
    '    End If
    'End Sub

    'Private Sub ListCurrentFiles(context As HttpContext)
    '    Dim files = New DirectoryInfo(StorageRoot).GetFiles("*", SearchOption.TopDirectoryOnly).Where(Function(f) Not f.Attributes.HasFlag(FileAttributes.Hidden)).[Select](Function(f) New FilesStatus(f)).ToArray()

    '    Dim jsonObj As String = js.Serialize(files)
    '    context.Response.AddHeader("Content-Disposition", "inline; filename=""files.json""")
    '    context.Response.Write(jsonObj)
    '    context.Response.ContentType = "application/json"
    'End Sub
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class