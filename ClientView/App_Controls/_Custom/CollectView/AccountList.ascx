﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<script runat="server">
   
    
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.DebtAccount
    Dim dsclients As New CRFS.ClientView.CrfsBll
    Protected WithEvents btnDeleteItem As ImageButton
    Protected WithEvents btnEditItem As LinkButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If Page.IsPostBack = False Then
            Me.HiddenField1.Value = Me.MyPage.CurrentUser.Id
            Me.MultiView1.SetActiveView(Me.View1)
            Me.panInsuredSearch.Visible = False
            If Me.UserInfo.CollInfo.ClientAgentCode = False Then
                Me.litDetorName.Text = "Name:"
                Me.litAccountNo.Text = "Account#:"
                Me.panInsuredSearch.Visible = False
            Else
                Me.litDetorName.Text = "Insured Name:"
                Me.litAccountNo.Text = "Policy#:"
                Me.panInsuredSearch.Visible = True
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindAccStatusCode(" & Me.CurrentUser.Id & ");", True)
            If qs.HasParameter("page") Then
                GetData(GetFilter, 1, "")
            End If
            FillClientListByUser()
        Else
             ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindAccStatusCode(" & Me.CurrentUser.Id & ");", True)
    End Sub
    Private Sub gvItemList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvItemList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
   
    Private Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?collectview.accountinfo&itemid=" & btnEditItem.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub GoToPage_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
        Dim txtGoToPage As TextBox = DirectCast(sender, TextBox)

        Dim pageNumber As Integer
        If Integer.TryParse(txtGoToPage.Text.Trim(), pageNumber) AndAlso pageNumber > 0 AndAlso pageNumber <= Me.gvItemList.PageCount Then
            Me.gvItemList.PageIndex = pageNumber - 1
        Else
            Me.gvItemList.PageIndex = 0
        End If
    End Sub
    Protected Sub gvItemList_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs) Handles gvItemList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim dr As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = dr("DebtAccountId")

        End If
        Dim gridView As GridView = DirectCast(sender, GridView)

        If gridView.SortExpression.Length > 0 Then
            Dim cellIndex As Integer = -1
            For Each field As DataControlField In gridView.Columns
                If field.SortExpression = gridView.SortExpression Then
                    cellIndex = gridView.Columns.IndexOf(field)
                    Exit For
                End If
            Next

            If cellIndex > -1 Then
                If e.Row.RowType = DataControlRowType.Header Then
                    '  this is a header row,
                    '  set the sort style
                    e.Row.Cells(cellIndex).CssClass = IIf(gridView.SortDirection = SortDirection.Ascending, "sortascheaderstyle", "sortdescheaderstyle")
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    '  this is an alternating row
                    e.Row.Cells(cellIndex).CssClass = IIf(e.Row.RowIndex Mod 2 = 0, "sortalternatingrowstyle", "sortrowstyle")
                End If
            End If
            
            
        End If

        'If e.Row.RowType = DataControlRowType.Pager Then
        '    Dim lblTotalNumberOfPages As Label = DirectCast(e.Row.FindControl("lblTotalNumberOfPages"), Label)
        '    lblTotalNumberOfPages.Text = gridView.PageCount.ToString()

        '    Dim txtGoToPage As TextBox = DirectCast(e.Row.FindControl("txtGoToPage"), TextBox)
        '    txtGoToPage.Text = (gridView.PageIndex + 1).ToString()

        '    Dim ddlPageSize As DropDownList = DirectCast(e.Row.FindControl("ddlPageSize"), DropDownList)
        '    ddlPageSize.SelectedValue = gridView.PageSize.ToString()
        'End If
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            e.Row.Cells(5).Text = "" & Utils.FormatDate(DR("ReferalDate")) & ""
            e.Row.Cells(7).Text = "" & Utils.FormatDecimal(DR("TotBalAmt")) & ""
        End If
    End Sub
    Protected Sub gvItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvItemList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvItemList.PageSize = Integer.Parse(dropDown.SelectedValue)
   
    End Sub
    Protected Sub btnCancel1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList
        With mysproc
            'MarkParimal: Client Filter-To check whether it is group client
            If Me.panClients1.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                    .ClientCode = Me.hdnClientSelectionId.Value
                End If
            End If
            
            'If Me.AccountName.Text.Trim.Length > 0 Then
            '    If InStr(Me.AccountNameLike.SelectedValue, "Includes") > 0 Then
            '        .DebtorName = "%" & Me.AccountName.Text.Trim & "%"
            '    Else
            '        .DebtorName = Me.AccountName.Text.Trim & "%"
            '    End If
            'End If
            If Me.AccountName.Text.Trim.Length > 0 Then
                If Me.AccountNameLike_1.Checked Then
                    .DebtorName = Me.AccountName.Text.Trim & "%"
                End If
            
                If Me.AccountNameLike_2.Checked Then
                    .DebtorName = "%" & Me.AccountName.Text.Trim & "%"
                End If
            End If
            
            'If Me.AccountNo.Text.Trim.Length > 0 Then
            '    If InStr(Me.AccountNoLike.SelectedValue, "Includes") > 0 Then
            '        .ACCOUNTNO = "%" & Me.AccountNo.Text.Trim & "%"
            '    Else
            '        .ACCOUNTNO = Me.AccountNo.Text.Trim & "%"
            '    End If

            'End If
            If Me.AccountNo.Text.Trim.Length > 0 Then
                If Me.AccountNoLike_1.Checked Then
                    .ACCOUNTNO = Me.AccountNo.Text.Trim & "%"
                End If
            
                If Me.AccountNoLike_2.Checked Then
                    .ACCOUNTNO = "%" & Me.AccountNo.Text.Trim & "%"
                End If
            End If
            
            If Me.FileNumber.Text.Trim.Length > 0 Then
                .FileNumber = Me.FileNumber.Text.Trim
            End If

            If Me.ClaimNo.Text.Trim.Length > 0 Then
                'If InStr(Me.ClaimNoLike.SelectedValue, "Includes") > 0 Then
                '    .ClaimNumber = "%" & Me.ClaimNo.Text.Trim & "%"
                'Else
                '    .ClaimNumber = Me.ClaimNo.Text.Trim & "%"
                'End If
                If Me.ClaimNoLike_1.Checked Then
                    .ClaimNumber = Me.ClaimNo.Text.Trim & "%"
                End If
            
                If Me.ClaimNoLike_2.Checked Then
                    .ClaimNumber = "%" & Me.ClaimNo.Text.Trim & "%"
                End If

            End If

            If Me.BranchNum.Text.Trim.Length > 0 Then
                .BranchNum = Me.BranchNum.Text.Trim & "%"
            End If
  
           
            If Me.hdnRadioAccStatusCode.Value = "SELECTED" Then
                .StatusCode = Left(Me.hdnAccStatusCode.Value, 3)
            End If
            
            
            If Me.AgentCode.Text.Trim.Length > 0 Then
                'If InStr(Me.AgentCodeLike.SelectedValue, "Includes") > 0 Then
                '    .AgentCode = "%" & Me.AgentCode.Text.Trim & "%"
                'Else
                '    .AgentCode = Me.AgentCode.Text.Trim & "%"
                'End If
                If Me.AgentCodeLike_1.Checked Then
                    .AgentCode = Me.AgentCode.Text.Trim & "%"
                End If
            
                If Me.AgentCodeLike_2.Checked Then
                    .AgentCode = "%" & Me.AgentCode.Text.Trim & "%"
                End If
            End If

            If Me.chkByAssignDate.Checked = True Then
                .ReferalRange = True
                .FromReferalDate = Me.FromAssignDate.Text
                .ThruReferalDate = Me.ThruAssignDate.Text
            End If

            .UserId = Me.CurrentUser.Id
            .Page = 1
            .PageRecords = 10000 '15 Changed by jaywanti
            
          
            
               
            
   
        End With
        
        GetData(mysproc, 1, "F")
        
    End Sub
   
    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList
        'MarkParimal: Client Filter-To check whether it is group client
        With mysproc
            If Me.panClients1.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    mysproc.ClientCode = Me.DropDownList1.SelectedValue
                    mysproc.ClientCode=Me.hdnClientSelectionId.Value
                End If
            End If
            .UserId = Me.CurrentUser.Id
            .Page = 1
            .PageRecords = 20000
        End With
        GetData(mysproc, 1, "F")
        
    End Sub

    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
         ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        Me.MultiView1.SetActiveView(View1)
         
    End Sub

    Protected Sub btnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "F")
    
    End Sub
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "P")
       
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "N")
       
    End Sub
    Protected Sub btnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetData(GetFilter, 1, "L")
   
    End Sub
    Protected Function GetFilter() As Object
        If IsNothing(Session("AccountListP")) = False Then
            Return Session("AccountListP")
        Else
            Return New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList
        End If
    End Function
    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.cview_ClientView_GetAccountList, ByVal apage As Integer, ByVal adirection As String)
       
        Dim myview As HDS.DAL.COMMON.TableView
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        asproc.PageRecords = 10000 '15 Changed by jaywanti
        asproc.UserId = Me.CurrentUser.Id
        If adirection = "F" Then
            asproc.Page = 1
        End If
        If adirection = "N" Then
            asproc.Page += 1
        End If
        If adirection = "P" Then
            asproc.Page -= 1
        End If
        'If adirection = "I" Then
        '    asproc.Page = ddPage.SelectedIndex + 1
        'End If
        
        If asproc.Page < 1 Then
            asproc.Page = 1
        End If

        If adirection = "L" Then
            myview = Session("AccountList")
            asproc.Page = myview.RowItem("pages")
        End If

        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
        If myview.Count < 1 Then
            Me.gvItemList.DataSource = myview
            Me.gvItemList.DataBind()
            'Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
            Me.MultiView1.SetActiveView(View2)
            'If (gvItemList.Rows.Count > 0) Then
               
            'End If
            Exit Sub
        End If
        
        Dim totrecs As String = myview.RowItem("totalrecords")
        Dim currpage As String = myview.RowItem("page")
        Dim totpages As Integer = (totrecs / 14 + 1)
        totpages = myview.RowItem("pages")
         
        'Me.lblCurrentPage.Text = currpage
        'Me.lblTotalPages.Text = totpages
        'Me.lblTotRecs.Text = totrecs
        Dim i As Integer = 0

        'ddPage.Items.Clear()
        'Do Until i > totpages - 1
        '    i += 1
        '    ddPage.Items.Add(i.ToString)
        'Loop

        'ddPage.SelectedIndex = currpage - 1
        Dim mydt As System.Data.DataTable = CRF.CLIENTVIEW.BLL.CollectView.Provider.DAL.GetDataSet(asproc).Tables(0)
        Me.gvItemList.DataSource = mydt
        Me.gvItemList.DataBind()
        If (gvItemList.Rows.Count > 0) Then
            Me.gvItemList.HeaderRow.TableSection = TableRowSection.TableHeader
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If
        Me.Session("AccountList") = New HDS.DAL.COMMON.TableView(mydt)
        Session("AccountListP") = asproc
        
        Me.MultiView1.SetActiveView(View2)
   
    End Sub
    'Protected Sub ddPage_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddPage.SelectedIndexChanged
    '    GetData(GetFilter, ddPage.SelectedIndex, "I")
    'End Sub
    
    Private Sub FillClientListByUser()
        Dim DS As System.Data.DataSet
        panClients1.Visible = False
        DS = CRFS.ClientView.CrfsBll.GetClientListForUser(Me.MyPage.CurrentUser.Id)
        If DS.Tables(0).Rows.Count > 1 Then
            DropDownList1.DataSource = CRFS.ClientView.CrfsBll.GetClientListForUser(Me.MyPage.CurrentUser.Id)
            DropDownList1.DataTextField = "ClientName"
            DropDownList1.DataValueField = "ClientId"
            DropDownList1.DataBind()
            panClients1.Visible = True
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        End If
        
        
    End Sub
</script>
<script type="text/javascript">   
    var todaycheck;
    function getFormatDate() {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = mm + '/' + dd + '/' + yyyy;

        return today
    }
    function CheckAssignedDateRange() {
        debugger;
        todaycheck = getFormatDate();
    
        if (((document.getElementById('<%=FromAssignDate.ClientID%>').value == todaycheck) && (document.getElementById('<%=ThruAssignDate.ClientID%>').value == todaycheck)) || ((document.getElementById('<%=FromAssignDate.ClientID%>').value == "") && (document.getElementById('<%=ThruAssignDate.ClientID%>').value == ""))) {
           //alert("start");
           
        
            $('#ctl33_ctl00_chkByAssignDate').prop('checked', false);
           // alert("end");
        }
        else {
            $('#ctl33_ctl00_chkByAssignDate').prop('checked', true);
        }
    }



    $(document).ready(function () {
        SetHeaderBreadCrumb('CollectView', 'Accounts', 'View Accounts');
        CallSuccessFunc();
          todaycheck = getFormatDate();
         document.getElementById('<%=FromAssignDate.ClientID%>').value = todaycheck
        document.getElementById('<%=ThruAssignDate.ClientID%>').value = todaycheck
        //$(".radio-custom.radio-default span:even").css("padding-left", "45px");
    });
    function BindClientSelection() {
        $('#UlClientSelectionType li').remove();
        var x = document.getElementById('<%=DropDownList1.ClientID%>');
        var txt = "";
        var i;
        for (i = 0; i < x.length; i++) {
            var ResultReplace = ReplaceSpecChar(x.options[i].text);
            $('#UlClientSelectionType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClientSel_' + x.options[i].value + '" onclick="setSelectedClientId(' + x.options[i].value + ',\'' + ResultReplace + '\')">' + x.options[i].text + '</a></li>');
        }
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val() != "") {
            $('#btnClientSelectionType').html($('#liClientSel_' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedClientId(id, TextData) {
        var ResultReplace = TextData.replace("`", "'");
        $('#' + '<%=hdnClientSelectionId.ClientID%>').val(id);
        $('#btnClientSelectionType').html(ResultReplace + "<span class='caret' ></span>");
        return false;
    }
    function ReplaceSpecChar(Textdata) {
        //alert(Textdata);
        var result = Textdata;
        var ResultReplace = result.replace("'", "`");
        //alert("ResultReplace" + ResultReplace);
        return ResultReplace;
    }
    function myFunctionAccountNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=AccountNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=AccountNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=AccountNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=AccountNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionAccountNoLike(id) {

        if (id == 1) {
            document.getElementById('<%=AccountNoLike_1.ClientID%>').checked = true;
            document.getElementById('<%=AccountNoLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=AccountNoLike_1.ClientID%>').checked = false;
            document.getElementById('<%=AccountNoLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionAgentCodeLike(id) {

        if (id == 1) {
            document.getElementById('<%=AgentCodeLike_1.ClientID%>').checked = true;
            document.getElementById('<%=AgentCodeLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=AgentCodeLike_1.ClientID%>').checked = false;
            document.getElementById('<%=AgentCodeLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionClaimNoLike(id) {

        if (id == 1) {
            document.getElementById('<%=ClaimNoLike_1.ClientID%>').checked = true;
            document.getElementById('<%=ClaimNoLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=ClaimNoLike_1.ClientID%>').checked = false;
            document.getElementById('<%=ClaimNoLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionAccStatusCode(id) {
        if (id == "ALL") {
            document.getElementById('<%=RadioAccStatusCode1.ClientID%>').checked = true;
            document.getElementById('<%=RadioAccStatusCode2.ClientID%>').checked = false;
      
        }
        else {
            document.getElementById('<%=RadioAccStatusCode1.ClientID%>').checked = false;
            document.getElementById('<%=RadioAccStatusCode2.ClientID%>').checked = true;
        }
        document.getElementById('<%=hdnRadioAccStatusCode.ClientID%>').value = id;
    }
    function MaintainMenuOpen() {
        MainMenuToggle('liAccounts');
        SubMenuToggle('liViewAccounts');
        //$('#liAccounts').addClass('site-menu-item has-sub active open');
        //$('#liViewAccounts').addClass('site-menu-item active');
        return false;

    }

    function CallSuccessFunc() {

        var defaults = {
           
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [15, 25, 35, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                 { "bSortable": true },
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-4'i><'col-sm-1'l><'col-sm-7'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };
        var options = $.extend(true, {}, defaults, $(this).data());
        // alert('hi');
        //var gridId = document.getElementById('<%=gvItemList.ClientID%>');
        // alert(gridId);
        $('#' + '<%=gvItemList.ClientID%>').dataTable(options);

    }

    function BindAccStatusCode(Id) {
        //alert('hii');

        PageMethods.FillStatusList(Id, LoadCAccStatusCode);
    }
    function LoadCAccStatusCode(result) {
        // alert('hii12');
        debugger;
        $('#UlAccStatusCode li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                $('#UlAccStatusCode').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClient_' + result[i].Value + '" onclick="setSelectedAccStatusCode(' + result[i].Value + ',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');
                //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
            }
        }
        if ($('#liClient_' + $('#' + '<%=hdnAccStatusCode.ClientID%>').val()).text() != "") {
            $('#btnAccStatusCode').html($('#liClient_' + $('#' + '<%=hdnAccStatusCode.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedAccStatusCode(id, TextData) {
        // alert(TextData);
        $('#' + '<%=hdnAccStatusCode.ClientID%>').val(TextData);
    
        //alert(TextData);
        $('#btnAccStatusCode').html(TextData + "<span class='caret' ></span>");
        return false;
    }
</script>
<style>
    .cssAccountName {
    padding-left:45px;
    }
</style>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
    SelectMethod="GetAccountList" TypeName="CRF.ClientView.BLL.CollectView.Provider">
    <SelectParameters>
        <asp:ControlParameter ControlID="HiddenField1" DefaultValue="-1" Name="userid" PropertyName="Value"
            Type="String" />
        <asp:SessionParameter DefaultValue="" Name="filter" SessionField="filter" Type="Object" />
        <asp:SessionParameter Name="sort" SessionField="sort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>


<div id="modcontainer" style="margin: 10px  0px 0px 0px; width: 100%;">
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <h1 class="panelheader">Account Filter</h1>
          
            <div class="body" style="float: left; width: 100%;">
                <asp:Panel ID="panClients1" runat="server">
               <%--     <div class="form-horizontal">--%>
                        <div class="form-group">

                            <label class="col-md-2 control-label">
                                <asp:Literal ID="Litclient" runat="server" Text="Client Selection:"></asp:Literal></label>

                           <div class="col-md-10" style="text-align: left;padding-left:7px;">
                                <asp:DropDownList ID="DropDownList1" runat="server" CssClass="form-control" style="display:none;">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnClientSelectionId" runat="server" />
                                <div class="btn-group" style="text-align: left;" align="left">
                                    <button type="button" class="btn btn-default dropdown-toggle" style="width: 300px; text-align: left;" align="left"
                                        data-toggle="dropdown" aria-expanded="false" id="btnClientSelectionType" tabindex="1">
                                        --Select Client Selection--
                     
                                                                                    <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlClientSelectionType" style="overflow-y: auto; height: 150px;">
                                    </ul>
                                </div>                        

                                  <div class="radio-custom radio-default radio-inline">
                                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                        <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL" style="white-space:nowrap;">All Clients</asp:ListItem>
                                        <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED" style="white-space:nowrap;padding-left:40px;">Selected Only</asp:ListItem>
                                    </asp:RadioButtonList>

                                </div>

                            </div>
                             
                        </div>
                   <div class="form-group">
                         <div class="col-md-12" style="min-height:7px;">

                         </div>
                   </div>
                   <%-- </div>--%>
                </asp:Panel>
                 <br />
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            <asp:Literal ID="litDetorName" runat="server" Text="Name:"></asp:Literal></label>

                        <div class="col-md-5">
                            <asp:TextBox runat="server" ID="AccountName" CssClass="form-control" TabIndex="1" />
                        </div>
                        <div class="col-md-5">

                            <div class="radio-custom radio-default">
                                <%--<asp:RadioButtonList ID="AccountNameLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                        RepeatLayout="Flow">
                                                                        <asp:ListItem Selected="True">&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                                                        <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                                                    </asp:RadioButtonList>--%>
                                <asp:RadioButton ID="AccountNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionAccountNameLike(1)" CssClass="cssAccountName" TabIndex="2" Width="125px" />
                                <asp:RadioButton ID="AccountNameLike_2" runat="server" Text="Includes" onchange="myFunctionAccountNameLike(2)" TabIndex="3" />
                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            <asp:Literal ID="litAccountNo" runat="server" Text="Account#"></asp:Literal></label>

                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="AccountNo" CssClass="form-control" TabIndex="4" />
                        </div>
                        <div class="col-md-3"></div>
                        <div class="col-md-5">

                            <div class="radio-custom radio-default">
                                <%--<asp:RadioButtonList ID="AccountNoLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                                        RepeatLayout="Flow">
                                                                        <asp:ListItem Selected="True">&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                                                        <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                                                    </asp:RadioButtonList>--%>
                                <asp:RadioButton ID="AccountNoLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionAccountNoLike(1)" CssClass="cssAccountName" TabIndex="5" Width="125px" />
                                <asp:RadioButton ID="AccountNoLike_2" runat="server" Text="Includes" onchange="myFunctionAccountNoLike(2)" TabIndex="6" />
                            
                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            Assigned Date:</label>

                       <%-- <div class="col-md-5">
                            <span style="display: flex;">
                               
                                <SiteControls:Calendar ID="FromAssignDate" Value="TODAY" IsReadOnly="false" IsRequired="true"
                                    ErrorMessage="Thru Date" runat="server" TabIndex="7" />
                                &nbsp;&nbsp; Thru &nbsp;
                                <SiteControls:Calendar ID="ThruAssignDate" Value="TODAY"
                                    IsReadOnly="false" IsRequired="true" ErrorMessage="Thru Date" runat="server" TabIndex="8" />
                            </span>
                        </div>--%>
                            <div class="col-md-5" style="display: flex;">
                                    <asp:TextBox ID="FromAssignDate" runat="server" Width="120px" onblur="CheckAssignedDateRange()" CssClass="form-control datepicker"  IsReadOnly="false" IsRequired="true" TabIndex="7" ></asp:TextBox>
                                                                           <asp:RequiredFieldValidator ID="reqq1" runat="server" ControlToValidate="FromAssignDate" ErrorMessage="From Date is Required">*</asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;Thru&nbsp;

                                      <asp:TextBox ID="ThruAssignDate" runat="server" Width="120px" onblur="CheckAssignedDateRange()" CssClass="form-control datepicker" IsReadOnly="false" IsRequired="true" TabIndex="8" ></asp:TextBox>&nbsp;
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ThruAssignDate" ErrorMessage="Thru Date is Required">*</asp:RequiredFieldValidator>
                                 
                            
                                </div>



                        <div class="col-md-4">

                            <div class="checkbox-custom checkbox-default" >
                                <asp:CheckBox ID="chkByAssignDate" CssClass="redcaption" runat="server" Text="  Filter By Assigned Date Range" TabIndex="9" />

                            </div>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            Branch/Region#:</label>

                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="BranchNum" CssClass="form-control" TabIndex="10" />

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">
                            CRF File#:</label>

                        <div class="col-md-2">
                            <asp:TextBox runat="server" ID="FileNumber" CssClass="form-control" TabIndex="11" />

                        </div>

                    </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">
                            Status Code:</label>

                        <div class="col-md-4" style="text-align:left">
                              <div class="btn-group" style="text-align: left;" >
                                                    <%--##1--%>
                                                    <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="width: 328px; text-align: left;" align="left"
                                                        data-toggle="dropdown" aria-expanded="false" id="btnAccStatusCode" tabindex="12">                                                    
                                                        --Select Status Code--
                     
                                                                                    <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlAccStatusCode" style="overflow-y: auto; height: 150px;">
                                                        <%-- <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,'Verify Job Data & Send Notice')">Verify Job Data & Send Notice</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(1,'Send Notice With Data Provided')">Send Notice With Data Provided</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(1,'Verify Job Data Only - No Notice Sent')">Verify Job Data Only - No Notice Sent</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(1,'Store Job Data As Provided - No Notice Sent')">Store Job Data As Provided - No Notice Sent</a></li>--%>
                                                    </ul>
                                 </div>
                               <asp:HiddenField ID="hdnAccStatusCode" runat="server" Value="" />
                        </div>
                          <div class="col-md-6">

                            <div class="radio-custom radio-default" style="padding-left: 103px;">
                           
                                <asp:RadioButton ID="RadioAccStatusCode1" runat="server" Checked="true" Text="All Status" onchange="myFunctionAccStatusCode('ALL')" CssClass="cssAccountName" TabIndex="13" Width="150px" />
                                <asp:RadioButton ID="RadioAccStatusCode2" runat="server" Text="Selected Status" onchange="myFunctionAccStatusCode('SELECTED')" TabIndex="14" />
                            
                            </div>
                              <asp:HiddenField ID="hdnRadioAccStatusCode" runat="server" Value="" />
                        </div>
                    </div>
                </div>

                <asp:Panel ID="panInsuredSearch" runat="server" Height="50px">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                <asp:Literal ID="Literal3" runat="server" Text="Agent #"></asp:Literal>:</label>

                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="AgentCode" CssClass="form-control" />
                            </div>
                            <div class="col-md-5">

                                <div class="radio-custom radio-default">
                                   <%-- <asp:RadioButtonList ID="AgentCodeLike" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow">
                                        <asp:ListItem  Selected="True">&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                    </asp:RadioButtonList>--%>
                                     <asp:RadioButton ID="AgentCodeLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionAgentCodeLike(1)" CssClass="cssAccountName" TabIndex="13" Width="125px" />
                                   <asp:RadioButton ID="AgentCodeLike_2" runat="server" Text="Includes" onchange="myFunctionAgentCodeLike(2)" TabIndex="14" />
                            

                                </div>

                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">
                                <asp:Literal ID="Literal4" runat="server" Text="Claim #"></asp:Literal>:</label>

                            <div class="col-md-5">
                                <asp:TextBox runat="server" ID="ClaimNo" CssClass="form-control" TabIndex="15" />
                            </div>
                            <div class="col-md-5">

                                <div class="radio-custom radio-default">
                                  <%--  <asp:RadioButtonList ID="ClaimNoLike" runat="server"  CssClass="redcaption" RepeatDirection="Horizontal"
                                        RepeatLayout="Flow">
                                       <asp:ListItem Selected="True" >&nbsp;Starts&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem style="padding-left: 60px;">&nbsp;Includes</asp:ListItem>
                                    </asp:RadioButtonList>--%>
                                      <asp:RadioButton ID="ClaimNoLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionClaimNoLike(1)" CssClass="cssAccountName" TabIndex="16" Width="125px" />
                                      <asp:RadioButton ID="ClaimNoLike_2" runat="server" Text="Includes" onchange="myFunctionClaimNoLike(2)" TabIndex="17" />
                            
                                </div>

                            </div>

                        </div>
                    </div>

                </asp:Panel>

                <br />
            </div>
            <div class="footer">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">

                                <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="btn btn-primary" OnClick="btnSubmit_Click" TabIndex="15" />
                                &nbsp;
                        <asp:Button ID="btnViewAll" runat="server" Text="View All" Visible="false" CssClass="btn btn-primary" OnClick="btnViewAll_Click" />
                              <%--  &nbsp;--%>
                  
                                   
                            </asp:Panel>
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <h1 class="panelheader">Account List</h1>
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12" style="text-align: left;">
                            <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter"
                                OnClick="btnBackFilter_Click"></asp:LinkButton>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <div style="width: auto; height: auto; overflow: auto;">
                                <asp:GridView ID="gvItemList" runat="server" CssClass="table dataTable table-striped"
                                    AutoGenerateColumns="False" BorderWidth="1px" Width="100%">
                                    <%--      <RowStyle CssClass="rowstyle" />
                                <AlternatingRowStyle CssClass="altrowstyle" />
                                <HeaderStyle CssClass="headerstyle" />--%>
                                    <Columns>
                                        <asp:TemplateField HeaderText="View">
                                            <ItemTemplate>
                                                <%--<asp:ImageButton ID="btnEditItem" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png"
                                                                                        runat="server" />--%>
                                                <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="15px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="DebtAccountId" SortExpression="DebtAccountId" HeaderText="CRF#">
                                            <ItemStyle Width="40px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client#">
                                            <ItemStyle Width="40px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AccountNO" SortExpression="AccountNo" HeaderText="Account#">
                                            <ItemStyle Width="70px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DebtorName" SortExpression="DebtorName" HeaderText="Debtor Name">
                                            <ItemStyle Width="160px" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ReferalDate" SortExpression="ReferalDate" HeaderText="Placed On">
                                            <ItemStyle Width="60px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CollectionStatus" SortExpression="CollectionStatus" HeaderText="Status">
                                            <ItemStyle Width="20px" HorizontalAlign="center" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TotBalAmt" SortExpression="TotBalAmt" HeaderText="Balance">
                                            <ItemStyle Width="50px" HorizontalAlign="right" Height="15px" Wrap="False" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <b>No Items found for the specified criteria</b>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
            <%--     <div class="footer" style="float: right">
                <table width="100%">
                    <tr>
                        <td class="row-label" align="right">(Total Accounts on this List
                            <asp:Label ID="lblTotRecs" runat="server"></asp:Label>
                            ) (Page
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                            of
                            <asp:Label ID="lblTotalPages" runat="server"></asp:Label>)&nbsp; &nbsp;Page: &nbsp;<asp:DropDownList
                                ID="ddPage" runat="server" AutoPostBack="true" Width="48px" OnSelectedIndexChanged="ddPage_SelectedIndexChanged">
                            </asp:DropDownList>
                            &nbsp;
                            <asp:ImageButton ID="btnFirst" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_First.png"
                                OnClick="btnFirst_Click" />
                            <asp:ImageButton ID="btnPrevious" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_Previous.png"
                                OnClick="btnPrevious_Click" />
                            <asp:ImageButton ID="btnNext" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_Next.png"
                                OnClick="btnNext_Click" />
                            <asp:ImageButton ID="btnLast" runat="server" Enabled="true" ImageUrl="~/App_Themes/VbJuice/Img/resultset_Last.png"
                                OnClick="btnLast_Click" />
                        </td>
                    </tr>
                </table>
            </div>--%>
        </asp:View>
    </asp:MultiView>
</div>
<asp:HiddenField ID="HiddenField1" runat="server" />
<asp:HiddenField ID="HiddenField2" runat="server" />
<asp:UpdateProgress ID="siteUpdateProgress" runat="server">
    <ProgressTemplate>
        <div class="TransparentGrayBackground">
        </div>
        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
            <div class="PageUpdateProgress">
                <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                    AlternateText="[image]" />
                &nbsp;Please Wait...
            </div>
        </asp:Panel>
        <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
            TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
            VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
    </ProgressTemplate>
</asp:UpdateProgress>


