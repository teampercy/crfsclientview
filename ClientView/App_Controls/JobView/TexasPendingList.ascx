<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>

<style>
    .panel-body, .form-control.input-sm {
        font-size: 11px;
    }

    #ctl32_ctl00_gvwList tbody tr:hover {
        cursor: pointer;
    }

    #ctl32_ctl00_gvwList thead tr th {
        font-weight: bold;
    }

    #ctl32_ctl00_gvwList thead tr th, #ctl32_ctl00_gvwList tbody tr td {
        /*border: 0;*/
    }
     #TableJobViewList thead tr th, #TableJobViewList tbody tr td {
        /*border: 0;*/
        padding:5px;
    }
      #tbltexas thead tr th, #tbltexas tbody tr td {
        /*border: 0;*/
        padding:5px;
    }
    

    #JobFilterBody .radio-custom label {
        width: 75px;
    }

    #JobFilterBody .radio-custom.radio-default.radio-inline {
        min-height: 0px;
        padding-top: 0px;
    }

    .RemovePadding {
        padding: 0px !important;
    }
    .nowordwraptd{
        padding-left: 5px;
white-space: nowrap;
text-overflow: ellipsis;
overflow: hidden;
    }
</style>
<script type="text/javascript">
    $(document).ready(function () {
        CallSuccessFuncTexasList();
    });
    function DeleteAccount(id) {
        document.getElementById('<%=hdnId.ClientID%>').value = $(id).attr("rowno");
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=btnDeleteId.ClientID%>').click();
      });
        return false;
    }
    function CallSuccessFuncTexasList() {

        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "searching": false,
            "lengthMenu": [5,10, 15, 20, 25, 35, 50, 100],
            "pageLength": 10,
            bAutoWidth: false,
            responsive: true,
            "bSort": false,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": true },
                //{ "bSortable": false }
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-4'il><'col-sm-8'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                },

            }
        };

        var options = $.extend(true, {}, defaults, $(this).data());
        var gvwListdiv = $('#' + '<%=gvwList.ClientID%>');


        gvwListdiv.dataTable(options);

     }
    function MaintainMenuOpen() {
        debugger;
        SetHeaderBreadCrumb('RentalView', 'View Jobs', 'Texas Pending List');
        MainMenuToggle('liViewJobs');
        SubMenuToggle('liTexaspendingListJobView');
        //var PageName = window.location.search.substring(1);
        ////alert(PageName);
        //if (PageName == "lienview.texaspendinglist.JobView") {
         
        //}
        //else {
        //    SetHeaderBreadCrumb('LienView', 'Tools', 'Texas Pending List');
        //    MainMenuToggle('liTools');
        //    SubMenuToggle('liTexaspendingList');
        //}
         
         //$('#liTools').addClass('site-menu-item has-sub active open');
         //$('#liTexaspendingList').addClass('site-menu-item active');
         return false;

     }

</script>
<script runat="server">
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Protected WithEvents btnEditItem As LinkButton
    Protected WithEvents btnDeleteTReq As LinkButton
    Protected WithEvents btnEditTReq As LinkButton
    Protected WithEvents btnPrntTReq As LinkButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.UserId.Value = Me.CurrentUser.Id
            Me.fromAsgDate.Value = DateAdd(DateInterval.Day, -180, Today)
            Me.thruAsgDate.Value = DateAdd(DateInterval.Day, 1, Today)
            If (IsNothing(Session("filterOrder")) = False And IsNothing(Session("filterColumn")) = False) Then
                drpSortType.Items.FindByValue(Session("filterOrder")).Selected = True
                drpSortBy.Items.FindByValue(Session("filterColumn")).Selected = True
            End If

            Dim mydt As System.Data.DataTable
            mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverDaysRemaining(Me.CurrentUser.Id).Tables(0)
            'mysproc.FromDate = "06-11-2021"
            'mysproc.ThruDate = "06-11-2021"
            'mysproc.UserId = Me.CurrentUser.Id
            'mysproc.SelectPending = False
            'mysproc.BranchNum = "D69"

            mysproc.FromDate = "01/01/1980"
            mysproc.ThruDate = Today
            mysproc.UserId = Me.CurrentUser.Id
            mysproc.SelectPending = True
            Session("mysproc1") = mysproc
            GetData(mysproc, 1, "F")

            'Me.gvwList.DataSourceID = Me.objWithEndDates.ID
            'Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)

            Me.gvwList.DataBind()
            If gvwList.Rows.Count > 0 Then
                Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasPending", "CallSuccessFuncTexasList();", True)
        End If
    End Sub
    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests, ByVal apage As Integer, ByVal adirection As String)
        myview = TryCast(Me.Session("JobList1"), HDS.DAL.COMMON.TableView)

        'If asproc.Page < 1 Then
        '    asproc.Page = 1
        'End If

        ' asproc.PageRecords = 2500
        'asproc.FromDate = "06-11-2021"
        'asproc.ThruDate = "06-11-2021"
        'asproc.UserId = Me.CurrentUser.Id
        'asproc.SelectPending = False
        'asproc.BranchNum = "D69"

        asproc.FromDate = "01/01/1980"
        asproc.ThruDate = Today
        asproc.UserId = Me.CurrentUser.Id
        asproc.SelectPending = True
        'If adirection = "F" Then
        '    asproc.Page = 1
        'End If
        'If adirection = "N" Then
        '    asproc.Page += 1
        'End If
        'If adirection = "P" Then
        '    asproc.Page -= 1
        'End If

        'If asproc.Page < 1 Then
        '    asproc.Page = 1
        'End If

        'If adirection = "L" Then
        '    myview = Session("JobList1")
        '    asproc.Page = myview.RowItem("pages")
        'End If

        If adirection.Length < 1 And IsNothing(myview) = False Then
            myview = Session("JobList1")
        Else
            ' myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
            'SortJobList()
            If Me.Session("JobList1") Is Nothing Then
                myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
            Else
                myview = TryCast(Me.Session("JobList1"), HDS.DAL.COMMON.TableView)

            End If


        End If
        'myview = Session("JobList")
        If myview.Count < 1 Then
            Me.gvwList.DataSource = Nothing
            Me.gvwList.DataBind()
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasPending", "CallSuccessFuncTexasList();", True)
            Exit Sub
        End If

        Session("JobViewListP") = asproc
        Session("JobList") = myview

        'Dim totrecs As String = myview.RowItem("totalrecords")
        'Dim currpage As String = myview.RowItem("page")
        'Dim totpages As Integer = (totrecs / 14 + 1)
        'totpages = myview.RowItem("pages")

        Dim i As Integer = 0

        Me.gvwList.DataSource = myview
        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader
        End If



        Me.Session("joblist1") = myview

    End Sub
    Protected Sub SortJobList()
        Try
            If Not Me.Session("joblist1") Is Nothing Then
                myviewSorting = Session("joblist1")
            Else

                Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests
                ' obj.Page = 1
                ' obj.PageRecords = 2500 '500 Changed by jaywanti
                'obj.FromDate = "06-11-2021"
                'obj.ThruDate = "06-11-2021"
                'obj.UserId = Me.CurrentUser.Id
                'obj.SelectPending = False
                'obj.BranchNum = "D69"

                obj.FromDate = "01/01/1980"
                obj.ThruDate = Today
                obj.UserId = Me.CurrentUser.Id
                obj.SelectPending = True


                'Harcoded added by priyanka
                myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
                Me.Session("joblist1") = myviewSorting
            End If

            Dim SortType As String = "ASC"
            If drpSortType.SelectedItem.Value = "-1" Then
                SortType = "ASC"
            ElseIf drpSortType.SelectedItem.Value = "1" Then
                SortType = "DESC"
            End If

            Dim SortBy As String
            If drpSortBy.SelectedItem.Value = "-1" Then
                SortBy = "ListId " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "1" Then
                SortBy = "JobId " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "2" Then
                SortBy = "CustRefNum " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "3" Then
                SortBy = "JobName " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "4" Then
                SortBy = "CustName " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "5" Then
                SortBy = "FilterKey " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "6" Then
                SortBy = "FilterKey " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "7" Then
                SortBy = "IsTX60Day " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "8" Then
                SortBy = "IsTX90Day " + SortType
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "9" Then
                SortBy = "AmountOwed " + SortType
                myviewSorting.Sort = SortBy
            End If


            Me.gvwList.DataSource = myviewSorting
            Me.gvwList.DataBind()

            Session("filterOrder") = drpSortType.SelectedItem.Value
            Session("filterColumn") = drpSortBy.SelectedItem.Value
            If gvwList.Rows.Count > 0 Then
                Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasPending", "CallSuccessFuncTexasList();", True)

        Catch ex As Exception

        End Try
    End Sub



    Protected Sub gvwItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvwList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvwList.PageSize = Integer.Parse(dropDown.SelectedValue)

    End Sub
    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntTReq"), LinkButton).Click, AddressOf btnPrntTReq_Click
            Me.btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditTReq"), LinkButton).Click, AddressOf btnEditTreq_Click
            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteTReq"), LinkButton).Click, AddressOf btnDeleteTReq_Click

        End If

    End Sub
    Protected Sub gvwList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            btnEditTReq.Attributes("rowno") = DR("JobId")

            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            btnPrntTReq.Attributes("rowno") = DR("JobId")

            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            btnDeleteTReq.Attributes("rowno") = DR("RequestId")
            'e.Row.Cells(2).Text = "" & Left(DR("JobName").ToString.Trim, 25)
            'e.Row.Cells(3).Text = "" & Left(DR("CustRefNum").ToString.Trim, 25)
            'e.Row.Cells(4).Text = "" & Left(DR("CustName").ToString.Trim, 25)
            'e.Row.Cells(5).Text = "" & Left(DR("JobAdd1").ToString.Trim, 25)
            'e.Row.Cells(5).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            'e.Row.Cells(9).Text = "$" & Left(DR("AmountOwed").ToString.Trim, 35)
            'e.Row.Cells(10).Text = "" & Left(DR("MonthDebtIncurred").ToString.Trim, 35)
        End If

    End Sub
    Private Function FormatDate(ByVal adate As String) As String
        If IsDate(adate) = False Then
            Return ""
        End If
        Return Strings.FormatDateTime(adate)
    End Function


    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mydt As System.Data.DataTable
        mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverDaysRemaining(Me.CurrentUser.Id).Tables(0)
        Me.gvwList.DataSourceID = Me.objWithEndDates.ID
        Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)

        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexas", "CallSuccessFuncTexasList();", True)

    End Sub

    Protected Sub btnEditTreq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        'Dim s As String = "?lienview.jobinfo&itemid=" & myitemid & "&list=tpr"//Commented by Jaywanti
        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myitemid & "&list=tpr" & "&PageName=TexaspendList" 'added by jaywanti

        Dim fullpath As String
        fullpath = Request.Url.AbsoluteUri
        If fullpath.Contains("JobView") Then
            s =  "~/MainDefault.aspx?jobview.jobviewinfo.texaspendingList&itemid=" & myitemid 'added by jaywanti
        End If
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub btnDeleteTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteTReq.Click
        btnDeleteTReq = TryCast(sender, LinkButton)
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
        myitem.Id = btnDeleteTReq.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList", "CallSuccessFuncTexasList();", True)
    End Sub

    Protected Sub Linkbutton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub
    Protected Sub btnPrntTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntTReq.Click
        btnEditItem = TryCast(sender, LinkButton)

        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetTexasRequestAck(btnEditItem.Attributes("rowno"))
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "true")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList1", "CallSuccessFuncTexasList();", True)
    End Sub



    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests
        With MYSPROC
            .FromDate = "01/01/1980"
            .ThruDate = Today
            .UserId = Me.CurrentUser.Id
            .SelectPending = True
        End With
        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetTexasPendingReport(MYSPROC, True))
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList1", "CallSuccessFuncTexasList();", True)
    End Sub

    Protected Sub btnSpreadsheet_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests
        With MYSPROC
            .FromDate = "01/01/1980"
            .ThruDate = Today
            .UserId = Me.CurrentUser.Id
            .SelectPending = True
        End With
        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetTexasPendingReport(MYSPROC, False))
        If IsNothing(sreport) = False Then
            'Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            'Me.btnDownLoad.Visible = True
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.MultiView1.SetActiveView(Me.View2)
            Me.DownLoadReport(Me.Session("spreadsheet"))

        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        Me.gvwList.DataSource = Me.Session("joblist1")
        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasPendingList", "CallSuccessFuncTexasList();", True)
        'Me.btnDownLoad.Visible = False
    End Sub

    'Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Me.DownLoadReport(Me.Session("spreadsheet"))
    'End Sub

    Protected Sub btnDeleteId_Click(sender As Object, e As EventArgs)
        Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
        myitem.Id = hdnId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)
        Me.gvwList.DataBind()
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsTexasList", "CallSuccessFuncTexasList();", True)
    End Sub


    Protected Sub drpSortType_SelectedIndexChanged(sender As Object, e As EventArgs)
        SortJobList()
    End Sub

    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        SortJobList()
    End Sub



    'Custom Pagging for job list



</script>
<style type="text/css">
    
    .overflowhide  {  OVERFLOW:hidden; TEXT-OVERFLOW:ellipsis;}
</style> 
<asp:ObjectDataSource ID="objWithEndDates" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetRequests"
    TypeName="Data.TexasRequestProvider">
    <SelectParameters>
        <asp:Parameter Name="maxrows" Type="Int32" />
        <asp:Parameter Name="afilter" Type="String" />
        <asp:Parameter Name="asort" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="ItemsDataSource" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="uspbo_ClientView_GetWaiverDaysRemaining"
    TypeName="Data.TexasRequestProvider">
    <SelectParameters>
        <asp:Parameter Name="UserId" Type="String" />
        <asp:Parameter Name="ClientCode" Type="String" />
        <asp:Parameter Name="RETURNVALUE" Type="Int16" Direction="Output" />
    </SelectParameters>
</asp:ObjectDataSource>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="MARGIN: 10px  0px 0px 0px; width: 100%;">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Texas Pending List</h1>

                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12" style="text-align: left;">
                                    <asp:LinkButton ID="btnReport" runat="server" CssClass="btn btn-primary"
                                        Text="Print PDF Report" OnClick="btnReport_Click" />
                                    <asp:LinkButton ID="btnSpreadsheet" runat="server" CssClass="btn btn-primary"
                                        OnClick="btnSpreadsheet_Click" Text="Print Spreadsheet" />
                                    <asp:LinkButton ID="btnRefresh" runat="server" CssClass="btn btn-primary"
                                        OnClick="btnRefresh_Click" Text="Refresh List" />
                                 <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="CRFS #" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Cust#" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Job Information" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Customer Name" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Filter Key" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Branch #" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="2nd Month" Value="7"></asp:ListItem>
                                         <asp:ListItem Text="3rd Month" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="AmountOwed" Value="9"></asp:ListItem>
                                        
                                       
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="drpSortType" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortType_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="Ascending" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Descending" Value="1"></asp:ListItem>
                                    </asp:DropDownList>
                           
                                    </div>

                            </div>
                             </div>
                            <div class="form-group">

                                <div class="col-md-12">

                                     <%--  '^^*** Created by pooja 04/24/2021*** ^^--%>
                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:GridView ID="gvwList" runat="server"
                                            CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                            BorderWidth="1px" Width="100%">
                                            <%--    <RowStyle CssClass="rowstyle" />
                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                            <HeaderStyle CssClass="headerstyle" />
                                                                            <PagerStyle CssClass="pagerstyle" />
                                                                            <PagerTemplate>
                                                                                <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                                                                <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                                                    <asp:ListItem Value="5" />
                                                                                    <asp:ListItem Value="10" />
                                                                                    <asp:ListItem Value="15" />
                                                                                    <asp:ListItem Value="20" />
                                                                                </asp:DropDownList>
                                                                                &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                                                                of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                                                                &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                                                                <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                                                                <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                                                                <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                                                                            </PagerTemplate>--%>
                                            <Columns>
                                                       <asp:TemplateField HeaderText="">
                                        <HeaderTemplate>
                                                <table id="tbltexas" style="width: 100%; table-layout:fixed;" rules="cols">
                                                 <tr>
                                                    <td style="width: 7%;">Edit/Print</td>
                                                    <td style="width: 10%;height:15px;  text-align: center;" class="nowordwraptd">CRFS #</td>
                                                    <td style="width: 22%;height:15px;" class="nowordwraptd">Job Information</td>
                                                    <td style="width: 16%;height:15px;" class="nowordwraptd">FilterKey</td>
                                                     <td style="width: 8%;height:15px;" class="nowordwraptd">Date</td>
                                                     <td style="width: 7%;height:15px;" class="nowordwraptd">2nd Mth</td>
                                                      <td style="width: 7%;height:15px;" class="nowordwraptd">3rd Mth</td>
                                                      <td style="width: 10%;height:15px;" class="nowordwraptd">Amount Owed</td>
                                                      <td style="width: 8%;height:15px;" class="nowordwraptd">Month</td>
                                                     <td style="width: 5%;" class="nowordwraptd">Del</td>
                                                    <%--<td style="width: 18%; word-break: break-all;">Job Address</td>--%>
                                                   <%--  <td style="width: 23%;"class="nowordwraptd" ></td>
                                                    <td style="width:75px;" class="nowordwraptd"></td>
                                                    <td style="width: 95px;" class="nowordwraptd"></td>--%>
                                                    
                                                </tr>

                                                     <tr>
                                                    <td style="width: 7%;"></td>
                                                    <td style="width: 10%;height:15px;  text-align: center;" class="nowordwraptd">Cust#</td>
                                                    <td style="width: 22%;height:15px;" class="nowordwraptd">Customer Name</td>
                                                    <td style="width: 16%;height:15px;" class="nowordwraptd">Branch #</td>
                                                     <td style="width: 8%;height:15px;" class="nowordwraptd"></td>
                                                     <td style="width: 7%;height:15px;" class="nowordwraptd"></td>
                                                      <td style="width: 7%;height:15px;" class="nowordwraptd"></td>
                                                      <td style="width: 10%;height:15px;" class="nowordwraptd"></td>
                                                      <td style="width: 8%;height:15px;" class="nowordwraptd"></td>
                                                     <td style="width: 5%;" class="nowordwraptd"></td>
                                                    <%--<td style="width: 18%; word-break: break-all;">Job Address</td>--%>
                                                   <%--  <td style="width: 23%;"class="nowordwraptd" ></td>
                                                    <td style="width:75px;" class="nowordwraptd"></td>
                                                    <td style="width: 95px;" class="nowordwraptd"></td>--%>
                                                    
                                                </tr>
                                                   
                                                   
                                                    
                                                      </table>
                                             </HeaderTemplate>
                                         <HeaderStyle CssClass="RemovePadding" />
                                        <ItemStyle CssClass="RemovePadding" />
                                        <ItemTemplate>
                                            <table id="TableJobViewList" style="width: 100%; font-size: 14px; table-layout:fixed;" rules="cols">
                                                <tr>
                                                    <td style="width: 7%; text-align: center;">
                                                          <asp:LinkButton ID="btnEditTReq" CssClass="icon ti-write" runat="server" />&nbsp;&nbsp;
                                                                                        <%--<asp:ImageButton ID="btnEditTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server"  />--%>
                                                        <asp:LinkButton ID="btnPrntTReq" CssClass="icon ti-printer" runat="server" />
                                                        <%-- <asp:ImageButton ID="btnPrntTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%></td>
                                                    <td style="width:10%;height:15px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobid" runat="server" Text='<%# Eval("JobId")%>'></asp:Label></td>
                                                   
                                                   <td style="width: 22%;height:15px;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobName" runat="server" Text='<%# Eval("JobName")%>'></asp:Label></td>
                                                   
                                                     <td style="width: 16%;height:15px;" class="nowordwraptd">
                                                       <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("FilterKey")%>'></asp:Label>
                                                    </td>
                                                  <%--  <td style="width: 75px; text-align: center; " class="nowordwraptd">
                                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("JVDeskNum")%>'></asp:Label></td>--%>
                                                  

                                                     <td style="width: 8%;height:15px; text-align: center;" class="nowordwraptd">
                                                          <asp:Label ID="lblDateCreated" runat="server" Text='<%#Utils.FormatDate(Eval("DateCreated"))%>'></asp:Label>
                                                     
                                                            </td>
                                                 
                                                 <td style="width: 7%;height:15px; text-align: center;" class="nowordwraptd">
                                                          <asp:Label ID="lblIsTX60Day" runat="server" Text='<%# Eval("IsTX60Day")%>'></asp:Label>
                                                           </td>
                                                    <td style="width: 7%;height:15px; text-align: center;" class="nowordwraptd">
                                                          <asp:Label ID="lblIsTX90Day" runat="server" Text='<%# Eval("IsTX90Day")%>'></asp:Label>
                                                       </td>
                                                    <td style="width: 10%;height:15px; text-align: center;" class="nowordwraptd">
                                                          <asp:Label ID="lblAmountOwed" runat="server" Text='<%# Eval("AmountOwed", "{0:c}")%>'></asp:Label>
                                                      </td>

                                                      <td style="width: 8%;height:15px; text-align: center; " class="nowordwraptd">
                                                     <asp:Label ID="lbl" runat="server" Text='<%# Eval("MonthDebtIncurred")%>'></asp:Label>
                                                  
                                                    </td>
                                                    <td style="width: 5%; text-align: center;" class="nowordwraptd">
                                                           <asp:LinkButton ID="btnDeleteTReq" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                    
                                                        </td>
                                                </tr>
                                                   <tr>
                                                    <td style="width: 7%; text-align: center;">
                                                           <%-- <asp:ImageButton ID="btnPrntTReq" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%></td>
                                                    <td style="width:10%;height:15px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblCustRefNum" runat="server" Text='<%# Eval("CustRefNum")%>'></asp:Label></td>
                                                   
                                                   <td style="width: 22%;height:15px;" class="nowordwraptd">
                                                        <asp:Label ID="CustName" runat="server" Text='<%# Eval("CustName")%>'></asp:Label></td>
                                                   
                                                     <td style="width: 16%;height:15px;" class="nowordwraptd">
                                                        <asp:Label ID="Label3" runat="server" Text='<%# Eval("BranchNumber")%>'></asp:Label>
                                                     
                                                    </td>
                                                  <%--  <td style="width: 75px; text-align: center; " class="nowordwraptd">
                                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("JVDeskNum")%>'></asp:Label></td>--%>
                                                    <td style="width: 8%;height:15px; text-align: center; " class="nowordwraptd">
                                                 <%--    <asp:Label ID="lblJobBalance" runat="server"
                                                            Text='<%# Eval("JobBalance", "{0:c}" %>'></asp:Label>                                                   
                                                        --%>
                                                
                                                            

                                                    </td>

                                                     <td style="width: 7%;height:15px; text-align: center;" class="nowordwraptd">
                                                        
                                                            </td>
                                                 
                                                 <td style="width: 7%;height:15px; text-align: center;" class="nowordwraptd">
                                                            </td>
                                                    <td style="width: 10%;height:15px; text-align: center;" class="nowordwraptd">
                                                          </td>
                                                    <td style="width: 8%;height:15px; text-align: center;" class="nowordwraptd">
                                                         </td>
                                                    <td style="width: 5%; text-align: center;" class="nowordwraptd">
                                                           
                                                        </td>
                                                </tr>
                                               
                                               
                                            </table>
                                        </ItemTemplate>
                                              </asp:TemplateField>
                                     
                                            
                                            </Columns>
                                            <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                        </asp:GridView> 
                                         </div>

                                      <%-- '^^*** *** ^^--%>
                                </div>
                            </div>
                       


                    </div>
                    <div class="footer">
                        <asp:HiddenField ID="hdnId" runat="server" Value="0" />
                        <asp:Button ID="btnDeleteId" runat="server" Style="display: none;" OnClick="btnDeleteId_Click" />
                        <br />
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12">

                                    <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to List" OnClick="Linkbutton2_Click"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-12">

                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="UserId" runat="server" />
        <asp:HiddenField ID="fromAsgDate" runat="server" />
        <asp:HiddenField ID="thruAsgDate" runat="server" />
        <asp:HiddenField ID="CurrentRowIndex" runat="server" />
        <asp:UpdatePanel ID="updatepanel2" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                    <ProgressTemplate>
                        <div class="TransparentGrayBackground"></div>
                        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                            <div class="PageUpdateProgress">
                                <asp:Image ID="ajaxLoadNotificationImage"
                                    runat="server"
                                    ImageUrl="~/images/ajax-loader.gif"
                                    AlternateText="[image]" />
                                &nbsp;Please Wait...
                            </div>
                        </asp:Panel>
                        <ajaxToolkit:AlwaysVisibleControlExtender
                            ID="AlwaysVisibleControlExtender1"
                            runat="server"
                            TargetControlID="alwaysVisibleAjaxPanel"
                            HorizontalSide="Center"
                            HorizontalOffset="150"
                            VerticalSide="Middle"
                            VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                    </ProgressTemplate>
                </asp:UpdateProgress>
            </ContentTemplate>
        </asp:UpdatePanel>

    </ContentTemplate>
</asp:UpdatePanel>

