<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobEditSForm.ascx.vb" Inherits="App_Controls__Custom_LienView_JobEditSForm" %>
<%@ Register Src="AddSigner.ascx" TagName="AddSigner" TagPrefix="uc3" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<style type="text/css">
    .scrollable-menu {
        height: auto;
        max-height: 180px;
        overflow-x: hidden;
    }

        .scrollable-menu::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 4px;
        }

        .scrollable-menu::-webkit-scrollbar-thumb {
            border-radius: 3px;
            background-color: lightgray;
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);
        }
</style>
<script type="text/javascript">
    function EnterKeyFilter() {
        if (window.event.keyCode == 13) {
            event.returnValue = false;
            event.cancel = true;
        }
    }
    $(document).ready(function () {
      
        $('#ctl33_ctl00_btnSAVE').attr('disabled', true);
        $("div").on("click", "#ctl33_ctl00_BalanceDueAmt", function () {
            var textbox = $(this);
            textbox.focus();
            textbox.select();
        });
        $("div").on("blur", "#ctl33_ctl00_BalanceDueAmt", function () {
            var textbox = $(this);
            if (textbox.val().length > 0 && textbox.val().indexOf("$") == -1) {
                var noCommas = $('#ctl33_ctl00_BalanceDueAmt').val().replace(/,/g, '').replace(/$/g, '')
                var value = parseFloat(noCommas)
                textbox.val('$' + value.toFixed(2));
            }
        });
    });
    function selectAllText(textbox) {
        textbox.focus();
        textbox.select();
    }
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('RentalView', 'View All Jobs', 'View Jobs');
        MainMenuToggle('liViewAllJobs');
        $('#liJobs').removeClass('active open');
        $('#lisubJobList').removeClass('active');
        //var query = window.location.search.substring(1);
        //var vars = query.split("&");
        //for (var i = 0; i < vars.length; i++) {
        //    var pair = vars[i].split("=");
        //    if (pair[0] == "PageName") {
            
        //        // SubMenuToggle('lisubJobList');
        //    }
        //    else {
        //        SetHeaderBreadCrumb('LienView', 'Jobs', 'View Jobs');
        //        MainMenuToggle('liJobs');
        //        SubMenuToggle('lisubJobList');
        //        $('#liViewAllJobs').removeClass('active open');
        //        //SubMenuToggle('lisubJobList');
        //    }
        //}
       
        //$('#liJobs').addClass('site-menu-item has-sub active open');
        //$('#lisubJobList').addClass('site-menu-item active');
        return false;

    }
  
    function BindFormTypeDropdown(JobState) {
        debugger;
        PageMethods.GetFormTypeList(JobState, LoadFormTypeList);
    }
    function LoadFormTypeList(result) {
        debugger;
        // alert('hii12');
        $('#ulDropDownFormType li').remove();
        if (result.length > 0) {
            for (var i = 0; i < result.length; i++) {
                if ($('#' + '<%=hdnFormTypeSelectedId.ClientID%>').val() == "" || $('#' + '<%=hdnFormTypeSelectedId.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnFormTypeSelectedId.ClientID%>').val(result[i].Value);
                    $('#' + '<%=hdnFormTypeSelectedText.ClientID%>').val(result[i].Text);
                }
                $('#ulDropDownFormType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liFormType_' + result[i].Value + '" onclick="setSelectedFormTypeId(' + result[i].Value + ',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');

                //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
            }
        }
 $('#btnDropFormType').html($('#liFormType_' + $('#' + '<%=hdnFormTypeSelectedId.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedFormTypeId(id, TextData) {
        debugger;
       // alert('hi');
        $('#' + '<%=hdnFormTypeSelectedId.ClientID%>').val(id);
        $('#' + '<%=hdnFormTypeSelectedText.ClientID%>').val(TextData);
        document.getElementById('<%=btnForm.ClientID%>').click();
        //alert(TextData);
        $('#btnDropFormType').text(TextData);
		  $('#ctl33_ctl00_btnSAVE').attr('disabled', false);
        return false;
    }
    function BindSignerDropdown() {
        //  alert('hi');
        //  alert(Signerid);        
         PageMethods.GetSignerList(LoadSignerList);
     }
     function LoadSignerList(result) {
         // alert('hii12');

         $('#ulDropDownSigner li').remove();
         if (result.length > 0) {
             for (var i = 0; i < result.length; i++) {
                 if ($('#' + '<%=hdnSignerSelectedValue.ClientID%>').val() == "" || $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val() == "0") {
                    $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(result[i].Value);
                }
                $('#ulDropDownSigner').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liSign_' + result[i].Value + '" onclick="setSelectedSignerId(' + result[i].Value + ',\'' + result[i].Text + '\')">' + result[i].Text + '</a></li>');

                //$('#btndrpClient').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="' + '45' + '" onclick="setSelectedClientId(' + '45' + ',\'' + 'sdsdsd' + '\')">' + 'tyty' + '</a></li>');
            }
        }
         $('#btnDropSigner').html($('#liSign_' + $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }
    function setSelectedSignerId(id, TextData) {
        $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(id);
        //alert(TextData);
        $('#btnDropSigner').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    function SetSigner(Id) {
        //alert('hi');
        /// alert(Id);
        // alert($('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(Id));
        $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val(Id);
        $('#btnDropSigner').html($('#liSign_' + $('#' + '<%=hdnSignerSelectedValue.ClientID%>').val()).text() + "<span class='caret' ></span>");
    }

<%--    function disablebtnSAVE() {
        //document.getElementById('<%=btnSAVE.ClientID %>').disabled = true;
        document.getElementById(btnSAVE).setAttribute("disabled", "disabled");
    }

    function enablebtnSAVE() {
        //document.getElementById('<%=btnSAVE.ClientID %>').disabled = false;
        document.getElementById(btnSAVE).setAttribute("disabled", "");
    }--%>

</script>

                                    <div id="modcontainer" onkeypress="javascript:EnterKeyFilter();" style="MARGIN: 10px; WIDTH: 100%;">
                                        <h1>
                                            <asp:Literal ID="litJobInfo" runat="server"></asp:Literal></h1>
                                        <div class="body">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <div class="col-md-2  col-sm-2 col-xs-2">
                                                        <asp:LinkButton ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to State Forms" CausesValidation="False" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label">Form Type:</label>
                                                    <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                                                        <div class="btn-group" style="text-align: left;" align="left">
                                                            <button type="button" class="btn btn-default dropdown-toggle col-md-12 col-sm-12 col-xs-12" style="min-width: 200px;" align="left"
                                                                data-toggle="dropdown" aria-expanded="false" id="btnDropFormType">
                                                                --Select Form Type--
                     
                                                                                    <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu  animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownFormType" style="overflow-y:auto;height:250px;">
                                                            </ul>
                                                        </div>
                                                        <asp:HiddenField ID="hdnFormTypeSelectedId" runat="server" Value="" />
                                                        <asp:HiddenField ID="hdnFormTypeSelectedText" runat="server" Value="" />
                                                        <cc1:ExtendedDropDown ID="FormId" runat="server" CssClass="dropdown" AutoPostBack="True" Style="display: none;"></cc1:ExtendedDropDown>
                                                        <asp:Button ID="btnForm" runat ="server" style="display:none;" OnClick="btnForm_Click"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 col-sm-4 col-xs-4 control-label">Signer:</label>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 col-sm-4 col-xs-4" style="text-align: left;">
                                                        <div class="btn-group" style="text-align: left;" align="left">
                                                            <button type="button" class="btn btn-default dropdown-toggle  col-md-12 col-sm-12 col-xs-12" style="min-width: 200px;" align="left"
                                                                data-toggle="dropdown" aria-expanded="false" id="btnDropSigner">
                                                                --Select Signer--
                     
                                                                                    <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownSigner" style="overflow-y:auto;height:250px;">
                                                            </ul>
                                                        </div>
                                                        <asp:HiddenField ID="hdnSignerSelectedValue" runat="server" Value="" />
                                                        <cc1:ExtendedDropDown ID="SignerId" runat="server" CssClass="dropdown" Style="display: none;"></cc1:ExtendedDropDown>
                                                    </div>
                                                    <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                                                        <uc3:AddSigner ID="AddSigner1" runat="server"></uc3:AddSigner>
                                                    </div>
                                                </div>
                                            </div>


                                            <hr />
                                            <asp:Panel ID="panFields" runat="server" Height="220px" ScrollBars="Vertical"
                                                Width="100%">
                                            </asp:Panel>

                                            <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                            <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
                                             <div id="newText" style ="color:red">
                                                PLEASE NOTE, FORMS CREATED HERE ARE INTENDED FOR CLIENTS TO FILE AND OR SEND THEMSELVES.   IF YOU PREFER CRF SOLUTIONS TO PREPARE AND FILE OR SEND ON YOUR BEHALF, PLEASE CONTACT THE CLIENT SERVICE DEPT AT 800-522-3858 FOR ASSISTANCE.
                                             </div>
                                        </div>
                                        <div class="footer">
                                            <asp:UpdatePanel ID="updPan1" runat="server">
                                                <ContentTemplate>
                                                    <asp:LinkButton ID="btnSAVE"
                                                        CssClass="btn btn-primary"
                                                        runat="server"
                                                        Text="Submit" CausesValidation="False" />&nbsp;
                                                    <br />
                                                    <br />
                                                     
                                                    <asp:UpdateProgress ID="UpdateProgress1" runat="server">
                                                        <ProgressTemplate>
                                                            <div class="TransparentGrayBackground"></div>
                                                            <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                                                <div class="PageUpdateProgress">
                                                                    <asp:Image ID="ajaxLoadNotificationImage"
                                                                        runat="server"
                                                                        ImageUrl="~/images/ajax-loader.gif"
                                                                        AlternateText="[image]" />
                                                                    &nbsp;Please Wait...
                                                                </div>
                                                            </asp:Panel>
                                                            <ajaxToolkit:AlwaysVisibleControlExtender
                                                                ID="AlwaysVisibleControlExtender1"
                                                                runat="server"
                                                                TargetControlID="alwaysVisibleAjaxPanel"
                                                                HorizontalSide="Center"
                                                                HorizontalOffset="150"
                                                                VerticalSide="Middle"
                                                                VerticalOffset="0">
                                                            </ajaxToolkit:AlwaysVisibleControlExtender>
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
