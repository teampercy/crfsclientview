<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddSigner.ascx.vb" Inherits="App_Controls_LienView_AddSigner" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<asp:LinkButton ID="btnAddNew" CssClass="button" runat="server" Text="Add New Signer" Style="display: none;" />
<input type="button" id="btnAddNewSign" onclick="OpenAddNewSignModal()" value="Add New Signer" class="btn btn-primary" />
<div class="modal fade modal-primary" id="ModalAddSign" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose" onclick="ModalHide();">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title">Add New Signer</h4>
        
            </div>
            <div class="modal-body">
               <%-- <div id="modcontainer" style="MARGIN: 0px  0px 0px 0px; width: 550px;">
                    <h1 class="panelheader">Add New Signer</h1>--%>
                    <div class="body">
                        <div class="form-horizontal">                      
                            <div class="form-group">

                                <label class="col-md-4 control-label">Name:</label>
                                <div class="col-md-7">
                                    <cc1:DataEntryBox ID="SignerName" runat="server" CssClass="form-control" IsRequired="False"
                                        TabIndex="43" tag="" Width="300px"></cc1:DataEntryBox>
                                     <asp:LinkButton ID="CancelButton" runat="server" CssClass="btn btn-primary" OnClick="CancelButton_Click" Text="Back " Style="display: none;"  />
                                </div>
                            </div>
                            <div class="form-group">

                                <label class="col-md-4 control-label">Title:</label>
                                <div class="col-md-7">
                                    <cc1:DataEntryBox ID="SignerTitle" runat="server" CssClass="form-control" IsRequired="False"
                                        TabIndex="43" tag="" Width="300px"></cc1:DataEntryBox>
                                </div>
                            </div>
                            <div class="form-group">

                                <label class="col-md-12 control-label">
                                    <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                    <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>

                                </label>
                                <asp:HiddenField ID="BatchJobId" runat="server" />
                                <asp:HiddenField ID="ItemId" runat="server" />
                            </div>
                        </div>
                    </div>
                  
              <%--  </div>--%>
            </div>
            <div class="modal-footer">
                <asp:LinkButton ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" />
             <%--   <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaves" onclick="ModalSave();" >
                    SAVE
                </button>--%>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="ModalHide();">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:Panel ID="panCustNameSuggest" runat="server" Style="display: none"
    Width="400px" Font-Bold="True">
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="" OkControlID="" DropShadow="true"
    PopupControlID="panCustNameSuggest" TargetControlID="btnAddNew">
</ajaxToolkit:ModalPopupExtender>
<script type="text/javascript">
   
    function OpenAddNewSignModal() {
       // alert('1');
        $("#ModalAddSign").modal('show');
    }
    function ModalHide() {       
        document.getElementById('<%=SignerName.ClientID%>').value = "";
        document.getElementById('<%=SignerTitle.ClientID%>').value = "";
        DisplayTablelayout();      
        $("#ModalAddSign").modal('hide');
        ActivateTabSet();
    }
    function ModalSave() {
       // alert('2');
        document.getElementById('<%=btnSave.ClientID%>').click();
        $("#ModalAddSign").modal('hide');
        return false;
    }
    
</script>