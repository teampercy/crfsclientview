Imports CRF.CLIENTVIEW.BLL
Imports System.Data.SqlClient
Imports System.Data
Imports System.Data.Sql
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Net.Mail
Imports System.IO

Partial Class App_Controls__Custom_LienView_BlankWaiver
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchNoJobWaiverNotice
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim mylieninfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientLienInfo
    Dim myclient As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Client

    Dim mysignerid As Integer
    Dim edit As Integer
    Shared BatchNoJobWaiverId As Integer
    Protected WithEvents btnEditWaiver As LinkButton
    Protected WithEvents btnDeleteWaiver As LinkButton
    Dim s1 As String
    Dim notary As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'hdnClientSelectedId.Value = Request.Form(hdnClientSelectedId.UniqueID)
        'hdnSignerSelectedValue.Value = Request.Form(hdnSignerSelectedValue.UniqueID)
        'hdnStateSelectedValue.Value = Request.Form(hdnStateSelectedValue.UniqueID)
        'hdnWaiverTypeSelectedId.Value = Request.Form(hdnWaiverTypeSelectedId.UniqueID)
        'hddbTabName.Value = Request.Form(hddbTabName.UniqueID)
        If Me.Page.IsPostBack = False Then
            Session("sortOrder") = "asc"
            ViewState("sVS_SortExpression") = "DateCreated"
            Me.MultiView1.SetActiveView(Me.View3)
            Me.ValidationSummary1.Visible = False

            Dim li As New ListItem
            myitem.JobState = "CA"
            Dim VWSTATES As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateTable


            Me.StateTableId.Items.Clear()
            VWSTATES.MoveFirst()
            Do Until VWSTATES.EOF
                li = New ListItem
                mystate = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
                mystate = VWSTATES.FillEntity(mystate)
                li.Value = mystate.StateInitials
                li.Text = mystate.StateName
                Me.StateTableId.Items.Add(li)
                VWSTATES.MoveNext()
            Loop

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
            Dim VWCLIENTS As HDS.DAL.COMMON.TableView = LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
            VWCLIENTS.MoveFirst()
            VWCLIENTS.Sort = "ClientName"

            Me.ClientTableId.Items.Clear()
            Do Until VWCLIENTS.EOF
                li = New ListItem
                li.Value = VWCLIENTS.RowItem("ClientId")
                li.Text = VWCLIENTS.RowItem("ClientName")
                Me.ClientTableId.Items.Add(li)
                VWCLIENTS.MoveNext()
            Loop

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
            Dim signerid = 0
            Dim VWSIGNERS As HDS.DAL.COMMON.TableView = LienView.Provider.GetClientSigners
            VWSIGNERS.MoveFirst()
            Me.SignerId.Items.Clear()
            GetLastSignerId()

            Do Until VWSIGNERS.EOF
                li = New ListItem
                Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
                csg = VWSIGNERS.FillEntity(csg)
                li.Value = csg.Id
                If csg.Id = mysignerid Then
                    signerid = mysignerid
                End If
                li.Text = csg.Signer & "," & csg.SignerTitle
                Me.SignerId.Items.Add(li)
                VWSIGNERS.MoveNext()

            Loop
            If signerid > 0 Then
                Me.SignerId.SelectedValue = signerid
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & signerid & ");", True)


            Me.StateTableId.SelectedValue = myitem.JobState
            hdnStateSelectedValue.Value = myitem.JobState
            LoadData()
            BindgGrid("CustName,DateCreated", sortOrder)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If



        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        If gvwWaivers.Rows.Count > 0 Then
            Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'markparimal-Blank Waiver:To Bind GridView.


    End Sub
    Private Sub BindgGrid(ByVal sortExp As String, ByVal sortDir As String)
        hdnflag.Value = "grid"
        gvwWaivers.DataSource = CRFS.ClientView.CrfsBll.GetBlankWaiversByUserId(CurrentUser.Id, sortExp, sortDir)
        gvwWaivers.DataBind()
        If Not Me.gvwWaivers.Rows.Count = 0 Then
            Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub

    Public Function sortOrder() As String

        If Session("sortOrder").ToString() = "desc" Then
            Session("sortOrder") = "asc"
        Else
            Session("sortOrder") = "desc"
        End If
        Return Session("sortOrder").ToString()

    End Function


    Private Sub clearfields()
        'markparimal-Blank Waiver:To Clear All To Values.
        PaymentAmt.Text = "$0.00"
        IsPaidInFull.Checked = False
        DisputedAmt.Value = "$0.00"

        MailtoCU.Checked = True
        MailtoGC.Checked = False
        MailtoOW.Checked = False
        MailtoLE.Checked = False

        CheckbyCU.Checked = True
        CheckByGC.Checked = False
        CheckByOW.Checked = False
        CheckByLE.Checked = False
        CheckbyOT.Checked = False

        OtherChkIssuerName.Value = ""
        JointCheckAgreement.Checked = False
        'Notary.Checked = False
        NotarywithCA.Checked = False
        NotarywithoutCA.Checked = False
        StartDate.Value = String.Empty
        ThroughDate.Value = String.Empty
        WaiverNote.Text = ""
        InvoicePaymentNo.Text = ""
        WaiverDates.Text = ""
        WaiverPayments.Text = ""

        JobNum.Value = ""
        JobName.Value = ""
        JobAddr1.Value = ""
        JobAddr2.Value = ""
        JobCity.Value = ""
        JobState.Value = ""
        JobZip.Value = ""

        CustName.Value = ""
        CustRef.Value = ""
        CustAdd1.Value = ""
        CustCity.Value = ""
        CustState.Value = ""
        CustZip.Value = ""
        hdncustEmail.Value = ""

        GCName.Value = ""
        GCRef.Value = ""
        GCAdd1.Value = ""
        GCCity.Value = ""
        GCState.Value = ""
        GCZip.Value = ""

        OwnrName.Value = ""
        OwnrAdd1.Value = ""
        OwnrCity.Value = ""
        OwnrState.Value = ""
        OwnrZip.Value = ""

        LenderName.Value = ""
        LenderAdd1.Value = ""
        LenderCity.Value = ""
        LenderState.Value = ""
        LenderZip.Value = ""



    End Sub

    Private Sub getwaivers()
        'markparimal-Blank Waiver:To get All To Values of waiver.row
        Dim crfs As New CRFS.ClientView.CrfsBll
        crfs.GetBlankWaiversByBatchNoJobWaiverId(BatchNoJobWaiverId)
        ClientTableId.SelectedValue = crfs.clientname
        hdnClientSelectedId.Value = crfs.clientname
        'FormId.SelectedValue = crfs.FormCode
        PaymentAmt.Value = crfs.pamt

        IsPaidInFull.Checked = crfs.ispaytodate
        DisputedAmt.Value = crfs.disputedamt
        MailtoCU.Checked = crfs.MailtoCU
        MailtoGC.Checked = crfs.MailtoGC
        MailtoOW.Checked = crfs.MailtoOW
        MailtoLE.Checked = crfs.MailtoLE

        CheckbyCU.Checked = crfs.CheckbyCU
        CheckByGC.Checked = crfs.CheckByGC
        CheckByOW.Checked = crfs.CheckbyOW
        CheckByLE.Checked = crfs.CheckbyLE
        CheckbyOT.Checked = crfs.CheckByOT

        OtherChkIssuerName.Value = crfs.OtherChkIssuerName
        JointCheckAgreement.Checked = crfs.JointCheckAgreement
        'Notary.Checked = crfs.Notary
        StartDate.Value = crfs.StartDate
        ThroughDate.Value = crfs.ThroughDate
        ' signer id selected value issue --28/01/2015
        'Commented by Jaywanti
        'If Not SignerId.Items.FindByValue(crfs.Signer) Is Nothing Then
        '    SignerId.SelectedValue = crfs.Signer
        'End If
        hdnSignerSelectedValue.Value = crfs.Signer

        WaiverNote.Value = crfs.additionalnote
        InvoicePaymentNo.Value = crfs.invoicepay

        JobNum.Value = crfs.JobNum
        JobName.Value = crfs.JobName
        JobAddr1.Value = crfs.JobAdd1
        JobAddr2.Value = crfs.JobAdd2
        JobCity.Value = crfs.JobCity
        JobState.Value = crfs.JobState
        JobZip.Value = crfs.JobZip

        CustName.Value = crfs.CustName
        CustRef.Value = crfs.CustNum
        CustAdd1.Value = crfs.CustAdd1
        CustCity.Value = crfs.CustCity
        CustState.Value = crfs.CustState
        CustZip.Value = crfs.CustZip

        GCName.Value = crfs.GCName
        GCRef.Value = crfs.GCNum
        GCAdd1.Value = crfs.GCAdd1
        GCCity.Value = crfs.GCCity
        GCState.Value = crfs.GCState
        GCZip.Value = crfs.GCZip
        If crfs.NotaryType = 1 Then
            NotarywithoutCA.Checked = True
        ElseIf crfs.NotaryType = 2 Then
            NotarywithCA.Checked = True
        End If
        'NotarywithCA.Checked = crfs.Notary
        OwnrName.Value = crfs.OwnerName
        OwnrAdd1.Value = crfs.OwnerAdd1
        OwnrCity.Value = crfs.OwnerCity
        OwnrState.Value = crfs.OwnerState
        OwnrZip.Value = crfs.OwnerZip

        LenderName.Value = crfs.LenderName
        LenderAdd1.Value = crfs.LenderAdd1
        LenderCity.Value = crfs.LenderCity
        LenderState.Value = crfs.LenderState
        LenderZip.Value = crfs.LenderZip
        StateTableId.SelectedValue = crfs.State
        hdnStateSelectedValue.Value = crfs.State
        LoadWaiveronedit(crfs.State)
        FormId.SelectedValue = crfs.FormCode
        hdnWaiverTypeSelectedId.Value = crfs.FormCode

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(crfs.clientname, myclient)
        hdncustEmail.Value = myclient.Email
        Session("edit") = 1

        Me.MultiView1.SetActiveView(Me.View1)
    End Sub
    Private Sub LoadData()
        'Dim vw As HDS.DAL.COMMON.TableView = LienView.Provider.GetWaiverStateForms(Me.StateTableId.SelectedValue) 'Commented by Jaywanti
        Dim vw As HDS.DAL.COMMON.TableView = LienView.Provider.GetWaiverStateForms(hdnStateSelectedValue.Value)
        Dim obj As New CRFDB.TABLES.StateForms
        vw.MoveFirst()
        Me.FormId.Items.Clear()
        Do Until vw.EOF
            Dim li As New ListItem
            obj = vw.FillEntity(obj)
            li.Value = vw.RowItem("Id")
            li.Text = vw.RowItem("FormCode") & "-" & vw.RowItem("Description")
            Me.FormId.Items.Add(li)
            vw.MoveNext()
        Loop
        Me.CustNameSearch1.ClearData()
        Me.GCSearch1.ClearData()

        Me.CustNameSearch1.SelectClientId = Me.ClientTableId.SelectedValue
        Me.GCSearch1.SelectClientId = Me.ClientTableId.SelectedValue
        Me.CustNameSearch1.SelectClientId = hdnClientSelectedId.Value
        Me.GCSearch1.SelectClientId = hdnClientSelectedId.Value

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub
    Private Sub LoadWaiveronedit(ByVal stateid As String)
        Dim vw As HDS.DAL.COMMON.TableView = LienView.Provider.GetWaiverStateForms(stateid)
        Dim obj As New CRFDB.TABLES.StateForms
        vw.MoveFirst()
        Me.FormId.Items.Clear()
        Do Until vw.EOF
            Dim li As New ListItem
            obj = vw.FillEntity(obj)
            li.Value = vw.RowItem("Id")
            li.Text = vw.RowItem("FormCode") & "-" & vw.RowItem("Description")
            Me.FormId.Items.Add(li)
            vw.MoveNext()
        Loop
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDownTab", "BindWaiverTypeDropdown('" & stateid & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub

    Private Function GetLastSignerId() As Integer
        Try
            Dim mysql As String = "select top 1 jobwaiverlogid,datecreated,signerid,requestedbyuserid from jobwaiverlog"
            mysql += " where SignerId Is Not null and requestedbyuserid='" & 999 & "' "
            mysql += " order by jobwaiverlogid desc"
            Dim myds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetDataSet(mysql)
            If myds.Tables(0).Rows.Count < 1 Then
                mysignerid = 0
            End If
            mysignerid = myds.Tables(0).Rows(0).Item("SignerId")
        Catch
            mysignerid = 0
        End Try

    End Function
    Protected Sub ClientTableId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ClientTableId.SelectedIndexChanged
        LoadData()

    End Sub

    Protected Sub StateTableId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles StateTableId.SelectedIndexChanged
        LoadData()

    End Sub

    Protected Sub FormId_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles FormId.SelectedIndexChanged

    End Sub
    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Linkbutton2.Click
        BindgGrid("DateCreated", sortOrder)
        Me.MultiView1.SetActiveView(Me.View1)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & hdnStateSelectedValue.Value & "');", True)
        If (hdnSignerSelectedValue.Value = "") Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0)", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & Convert.ToInt32(hdnSignerSelectedValue.Value) & ");", True)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub

    Protected Sub GCSearch1_ItemSelected() Handles GCSearch1.ItemSelected
        Dim myid As String = Strings.Replace(GCSearch1.SelectedValue, "C", "")
        myid = Strings.Replace(myid, "B", "")
        If IsNothing(myid) Then Exit Sub
        If Left(Me.GCSearch1.SelectedValue, 1) = "B" Then
            Dim mynewJob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mynewJob)
            Me.GCName.Text = Left(mynewJob.GCName, 50)
            Me.GCAdd1.Text = mynewJob.GCAdd1
            Me.GCCity.Value = mynewJob.GCCity
            Me.GCState.Value = mynewJob.GCState
            Me.GCZip.Value = mynewJob.GCZip
            Me.GCRef.Text = mynewJob.GCRefNum
        Else
            Dim mygc As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientGeneralContractor
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mygc)
            Me.GCName.Text = mygc.GeneralContractor
            Me.GCAdd1.Text = mygc.AddressLine1
            Me.GCCity.Value = mygc.City
            Me.GCState.Value = mygc.State
            Me.GCZip.Value = mygc.PostalCode
            Me.GCRef.Text = mygc.RefNum
        End If
    End Sub

    Protected Sub GCSearch1_SearchSelected(ByVal searchkey As String) Handles GCSearch1.SearchSelected
        GCSearch1.ShowList(searchkey)
    End Sub

    Protected Sub CustNameSearch1_ItemSelected() Handles CustNameSearch1.ItemSelected
        Dim myid As String = Strings.Replace(CustNameSearch1.SelectedValue, "C", "")
        myid = Strings.Replace(myid, "B", "")
        If IsNothing(myid) Then Exit Sub
        If Left(Me.CustNameSearch1.SelectedValue, 1) = "B" Then
            Dim mynewcust As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mynewcust)
            Me.CustName.Text = Left(mynewcust.CustName, 50)
            Me.CustAdd1.Text = mynewcust.CustAdd1
            Me.CustCity.Value = mynewcust.CustCity
            Me.CustState.Value = mynewcust.CustState
            Me.CustZip.Value = mynewcust.CustZip
            Me.hdncustEmail.Value = mynewcust.CustEmail
            Me.CustRef.Text = mynewcust.CustRefNum
        Else
            Dim mycust As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomer
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mycust)
            If mycust.CustId <> myid Then Exit Sub
            Me.CustName.Text = Left(mycust.ClientCustomer, 50)
            Me.CustAdd1.Text = mycust.AddressLine1
            Me.CustCity.Value = mycust.City
            Me.CustState.Value = mycust.State
            Me.CustZip.Value = mycust.PostalCode
            Me.CustRef.Text = mycust.RefNum
            Me.hdncustEmail.Value = mycust.Email

        End If

    End Sub
    Protected Sub CustNameSearch1_SearchSelected(ByVal searchkey As String) Handles CustNameSearch1.SearchSelected
        CustNameSearch1.ShowList(searchkey)
    End Sub

    Protected Sub AddSigner1_ItemSaved() Handles AddSigner1.ItemSaved
        Dim li As New ListItem
        Dim VWSIGNERS As HDS.DAL.COMMON.TableView = LienView.Provider.GetClientSigners
        VWSIGNERS.MoveFirst()
        Me.SignerId.Items.Clear()
        Do Until VWSIGNERS.EOF
            li = New ListItem
            Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
            csg = VWSIGNERS.FillEntity(csg)
            li.Value = csg.Id
            li.Text = csg.Signer & "," & csg.SignerTitle
            Me.SignerId.Items.Add(li)
            VWSIGNERS.MoveNext()
        Loop
        HttpContext.Current.Session("SignerList") = Nothing
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & hdnStateSelectedValue.Value & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0)", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        With myitem
            .FormId = Me.FormId.SelectedValue
            .FormCode = hdnWaiverTypeSelectedText.Value
            .PaymentAmt = CRFVIEW.Utils.GetDecimal(Me.PaymentAmt.Value)
            .DisputedAmt = CRFVIEW.Utils.GetDecimal(Me.DisputedAmt.Value)
            .BatchNoJobWaiverId = BatchNoJobWaiverId
            .MailtoCU = Me.MailtoCU.Checked
            .MailtoGC = Me.MailtoGC.Checked
            .MailtoLE = Me.MailtoLE.Checked
            .MailtoOW = Me.MailtoOW.Checked

            .CheckbyCU = Me.CheckbyCU.Checked
            .CheckByGC = Me.CheckByGC.Checked
            .CheckbyLE = Me.CheckByLE.Checked
            .CheckByOT = Me.CheckbyOT.Checked
            .CheckbyOW = Me.CheckByOW.Checked

            .JointCheckAgreement = Me.JointCheckAgreement.Checked
            .OtherChkIssuerName = Me.OtherChkIssuerName.Text
            '.Notary = Me.Notary.Checked

            '.Notary = NotarywithCA.Checked
            If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                .Notary = True
                If (NotarywithoutCA.Checked = True And NotarywithCA.Checked = False) Then
                    .NotaryType = 1
                ElseIf (NotarywithCA.Checked = True And NotarywithoutCA.Checked = False) Then
                    .NotaryType = 2
                End If
            Else
                .Notary = False
            End If


            .StartDate = Nothing
            .ThroughDate = Nothing
            If Me.StartDate.Value.Length > 1 Then
                If IsDate(Me.StartDate.Value) Then
                    .StartDate = Utils.GetDate(Me.StartDate.Value)
                End If
            End If
            If Me.ThroughDate.Value.Length > 1 Then
                If IsDate(Me.ThroughDate.Value) Then
                    .ThroughDate = Utils.GetDate(Me.ThroughDate.Value)
                End If
            End If


            .RequestedBy = Me.CurrentUser.UserName
            .RequestedByUserId = Me.CurrentUser.Id

            .JobAdd1 = Me.JobAddr1.Text
            .JobAdd2 = Me.JobAddr2.Text
            .JobCity = Me.JobCity.Text
            .JobName = Me.JobName.Text
            .JobNum = Me.JobNum.Text
            '.JobState = Me.StateTableId.SelectedValue 'Commented by Jaywanti
            .JobState = hdnStateSelectedValue.Value
            '.JobState = Me.JobState.Value
            .JobZip = Me.JobZip.Text

            .CustAdd1 = Me.CustAdd1.Text
            .CustCity = Me.CustCity.Text
            .CustName = Me.CustName.Text
            .CustNum = Me.CustRef.Text
            .CustState = Me.CustState.Text
            .CustZip = Me.CustZip.Text

            .GCAdd1 = Me.GCAdd1.Text
            .GCCity = Me.GCCity.Text
            .GCName = Me.GCName.Text
            .GCNum = Me.GCRef.Text
            .GCState = Me.GCState.Text
            .GCCity = Me.GCCity.Text
            .GCZip = Me.GCZip.Text

            .OwnerAdd1 = Me.OwnrAdd1.Text
            .OwnerCity = Me.OwnrCity.Text
            .OwnerName = Me.OwnrName.Text
            .OwnerState = Me.OwnrState.Text
            .OwnerCity = Me.OwnrCity.Text
            .OwnerZip = Me.OwnrZip.Text

            .LenderAdd1 = Me.LenderAdd1.Text
            .LenderCity = Me.LenderCity.Text
            .LenderName = Me.LenderName.Text
            .LenderState = Me.LenderState.Text
            .LenderZip = Me.LenderCity.Text

            .ClientId = Me.ClientTableId.SelectedValue
            .ClientId = hdnClientSelectedId.Value
            'If Me.SignerId.SelectedIndex >= 0 Then 'Commented by jaywanti
            '    .SignerId = Me.SignerId.SelectedValue
            'End If
            If hdnClientSelectedId.Value <> "" Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(.ClientId, myclient)
                hdncustEmail.Value = myclient.Email
            End If

            If (hdnSignerSelectedValue.Value <> "") Then
                If Convert.ToInt32(hdnSignerSelectedValue.Value) >= 0 Then
                    .SignerId = Convert.ToInt32(hdnSignerSelectedValue.Value)
                End If
            End If

            .FormId = Me.FormId.SelectedValue 'Commented by Jaywanti
            .FormId = hdnWaiverTypeSelectedId.Value
            .IsPaidInFull = Me.IsPaidInFull.Checked
            .WaiverNote = Me.WaiverNote.Text
            If Me.StateTableId.SelectedValue = "NV" Then
                .InvoicePaymentNo = Me.InvoicePaymentNo.Text
            End If

            If hdnStateSelectedValue.Value = "NV" Then
                .InvoicePaymentNo = Me.InvoicePaymentNo.Text
            End If

            .WaiverDates = Me.WaiverDates.Text
            .WaiverPayments = Me.WaiverPayments.Text

        End With

        If BatchNoJobWaiverId > 1 Then
            BatchNoJobWaiverId = myitem.BatchNoJobWaiverId
            CRFVIEW.Globals.DBO.Update(myitem)
        Else
            CRFVIEW.Globals.DBO.Create(myitem)
            BatchNoJobWaiverId = myitem.BatchNoJobWaiverId
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayWaiverPopUp", "DisplayWaiverPopUp();", True)




        'If Not Me.gvwWaivers.Rows.Count = 0 Then
        '    Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub btnPrintWaiver_Click(sender As Object, e As EventArgs)
        Dim JobWaiverLogId As String = BatchNoJobWaiverId
        printwaiver()
        editwaiver(JobWaiverLogId)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fadeout", "fadeout();", True)
    End Sub
    Protected Sub hdnbtnclose_Click(sender As Object, e As EventArgs)
        Dim JobWaiverLogId As String = BatchNoJobWaiverId
        editwaiver(JobWaiverLogId)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fadeout", "fadeout();", True)
    End Sub
    Public Sub setMaildetails()
        Dim EmailUserName As String = Me.UserInfo.UserInfo.UserName
        Dim EmailUserEmail As String = Me.UserInfo.UserInfo.Email
        'If hdncustEmail.Value <> "" Then
        '    txtToList.Text = hdncustEmail.Value
        'Else
        '    txtToList.Text = ""
        'End If

        txtToList.Text = ""
        txtSubject.Text = "New Waiver Document."
        Me.txtBody.Text = "PLEASE Do Not RESPOND To THIS EMAIL." + vbCrLf + "If you have any questions, Or need any assistance, please contact " + EmailUserName + " at " + EmailUserEmail + "."


        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideWaiverPopUp", "HideWaiverPopUp();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayEmailPopUp", "DisplayEmailPopUp();", True)
    End Sub
    Protected Sub btnEmailWaiver_Click(sender As Object, e As EventArgs)
        '  Dim JobAddSubjectLine As String = ""
        setMaildetails()
    End Sub
    Public Sub printwaiver()
        Dim JobWaiverLogId As String = BatchNoJobWaiverId
        ViewState("JobWaiverLogId") = BatchNoJobWaiverId
        Dim crfs As New CRFS.ClientView.CrfsBll
        crfs.GetBlankWaiversByBatchNoJobWaiverId(JobWaiverLogId)
        Dim SREPORT As String = LienView.Provider.PrintNoJobWaiver(JobWaiverLogId)

        Me.btnDownLoad.Visible = False
        If IsNothing(SREPORT) = False Then
            Dim s1 As String = Me.ResolveUrl("~/UserData/Output/" & (System.IO.Path.GetFileName(SREPORT)))
            'If (crfs.Notary = True) Then
            If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                PrintNotary(BatchNoJobWaiverId)
                ' PrintNotary()
                MergeNotary(SREPORT)
            End If

            Me.btnDownLoad.Visible = True
            'Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(SREPORT))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(SREPORT)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s1), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            ' Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Public Sub PrintNotary(ByVal JobWaiverLogId As String)

        'JobWaiverLogId = ViewState("JobWaiverLogId").ToString
        Dim sreport As String = LienView.Provider.PrintNoJobNotary(JobWaiverLogId)
        'Dim sreport As String = LienView.Provider.PrintNoJobNotary(0)

        If IsNothing(sreport) = False Then

            s1 = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))

        End If

        ' vv ***** Static PDF Report ***** vv
        'Dim sreportNotary As String
        'If NotarywithCA.Checked = True Then
        '    sreportNotary = "Notary(Jurat).pdf"
        'Else
        '    sreportNotary = "Notary(Jurat)1.pdf"
        'End If

        'If IsNothing(sreportNotary) = False Then

        '    notary = Server.MapPath("~/Images/ResourceImages/Liens/" & (sreportNotary))

        'End If
        ' ^^ ***** Static PDF Report ***** ^^

    End Sub


    'Public Sub PrintNotary(ByVal JobWaiverLogId As String)
    '    Dim sreport As String = LienView.Provider.PrintJobNotary(JobWaiverLogId)
    '    Dim sreportNotary As String

    '    If NotarywithCA.Checked = True Then
    '        sreportNotary = "Notary(Jurat).pdf"
    '    Else
    '        sreportNotary = "Notary(Jurat)1.pdf"
    '    End If


    '    If IsNothing(sreportNotary) = False Then

    '        s1 = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreportNotary)))

    '    End If

    'End Sub
    Public Sub MergeNotary(ByVal sreport As String)
        Dim s2 As String = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
        Dim path As String = ConfigurationManager.AppSettings("path").ToString()
        File.Create(path & (System.IO.Path.GetFileName(sreport))).Close()

        'vv *** Uncommented Pooja 27/08/2020 *** vv
        Dim notary As String
        notary = s1
        '^^                                ^^

        Dim result As String = path & (System.IO.Path.GetFileName(sreport))
        Dim source_files() As String = {s2, notary}
        Dim document As iTextSharp.text.Document = New iTextSharp.text.Document
        Dim copy As iTextSharp.text.pdf.PdfCopy = New iTextSharp.text.pdf.PdfCopy(document, New FileStream(result, FileMode.Create))
        'open the document
        document.Open()
        Dim reader As iTextSharp.text.pdf.PdfReader
        Dim i As Integer = 0
        Do While (i < source_files.Length)
            'create PdfReader object
            reader = New iTextSharp.text.pdf.PdfReader(source_files(i))
            'merge combine pages
            Dim page As Integer = 1
            Do While (page <= reader.NumberOfPages)
                copy.AddPage(copy.GetImportedPage(reader, page))
                page = (page + 1)
            Loop

            i = (i + 1)
        Loop

        'close the document object
        document.Close()
        '-------------------------------------------------------------
        File.Copy(result, sreport, True)
        File.Delete(result)

    End Sub

    Public Sub EmailWaiver(ByVal JobWaiverLogId As String)
        Dim crfs As New CRFS.ClientView.CrfsBll
        crfs.GetBlankWaiversByBatchNoJobWaiverId(JobWaiverLogId)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintNoJobWaiver(JobWaiverLogId)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            'If (crfs.Notary = True) Then
            If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                PrintNotary(JobWaiverLogId)
                MergeNotary(sreport)
            End If

            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            Dim Flagerror = EmailToUser(txtToList.Text, txtSubject.Text, txtBody.Text, Filename, fileArray) 'For a timing
            If Flagerror = "0" Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EmailSendPopup", "EmailSendPopup();", True)
            End If
        End If


    End Sub
    Public Shared Function GetSettings() As TABLES.Report_Settings
        Dim myitem As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, myitem)

        Return myitem

    End Function
    Public Shared Function EmailToUser(ByVal email As String, ByVal subject As String, ByVal strBody As String, ByVal FileName As String, ByVal pdfcontent As Byte())

        Dim Flagerror As String = "0"


        Dim ToArrayList As String()
        ToArrayList = email.Split(";")

        Dim MYSITE As TABLES.Report_Settings = GetSettings()

        Dim msg As New MailMessage
        'msg.From = New MailAddress("priyanka", "ClientView")
        msg.From = New MailAddress("CRFIT@crfsolutions.com", "Job Waiver Central")
        msg.Subject = subject
        For index = 0 To ToArrayList.Length - 1
            msg.To.Add(ToArrayList(index))
        Next
        msg.IsBodyHtml = True
        msg.Body = strBody
        msg.Attachments.Add(New Attachment(New MemoryStream(pdfcontent), FileName))

        'msg.Body = body

        Try
            With MYSITE
                Dim _smtp As New System.Net.Mail.SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
            End With
            ''Dim _smtp As New System.Net.Mail.SmtpClient("smtp.rhealtech.com")
            ''If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
            ''_smtp.Port = 25
            ''smtp.Credentials = New System.Net.NetworkCredential(vbNull, vbNull)
            ''_smtp.Credentials = New System.Net.NetworkCredential("pooja.p@rhealtech.com", "L6E8BgT3tP60")
            ''_smtp.EnableSsl = False
            ''End If
            ''_smtp.Send(msg)
            ''_smtp.Dispose()
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            MsgBox("Error On email:" & vbCrLf & mymsg)
            Flagerror = "1"
        End Try
        Return Flagerror
    End Function
    Protected Sub btnEmailsend_Click(sender As Object, e As EventArgs)
        Dim JobWaiverLogId As String = ""
        JobWaiverLogId = BatchNoJobWaiverId
        If Me.txtToList.Text.Trim() <> "" Then
            EmailWaiver(JobWaiverLogId)
        End If
        If (hdnflag.Value = "grid") Then
            Me.MultiView1.SetActiveView(Me.View3)
            If Not Me.gvwWaivers.Rows.Count = 0 Then
                Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        ElseIf (hdnflag.Value = "edit") Then
            editwaiver(JobWaiverLogId)
        End If

    End Sub
    Protected Sub btnNew_Click(sender As Object, e As EventArgs)

        AddWaiver()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "fadeout", "fadeout();", True)
    End Sub
    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))

    End Sub

    Protected Sub btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnback.Click
        hdnflag.Value = "grid"
        lblmsg.Text = ""
        Me.MultiView1.SetActiveView(Me.View3)
        If Not Me.gvwWaivers.Rows.Count = 0 Then
            Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'BindgGrid("DateCreated", sortOrder)
        BindgGrid("datecreated", "desc")
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub

    Protected Sub btnAddWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWaiver.Click
        AddWaiver()
    End Sub
    Public Sub AddWaiver()
        clearfields()
        BatchNoJobWaiverId = 0
        hddbTabName.Value = "WaiverInfo"
        hdnStateSelectedValue.Value = "CA"
        hdnWaiverTypeSelectedId.Value = ""
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & hdnStateSelectedValue.Value & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0)", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        Me.MultiView1.SetActiveView(Me.View1)
        hdnflag.Value = "edit"
    End Sub
    Public Sub editwaiver(ByVal WaiverId As String)
        hdnflag.Value = "edit"
        BatchNoJobWaiverId = WaiverId
        getwaivers()
        hddbTabName.Value = "WaiverInfo"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & hdnStateSelectedValue.Value & "');", True)
        If (hdnSignerSelectedValue.Value = "") Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0)", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & Convert.ToInt32(hdnSignerSelectedValue.Value) & ");", True)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)

    End Sub
    Protected Sub gvwWaivers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvwWaivers.RowCommand
        'markparimal-Blank Waiver:To Edit Selected Blank Waiver.
        If e.CommandName = "editwaiver" Then
            editwaiver(Convert.ToInt32(e.CommandArgument))
        End If

        'markparimal-Blank Waiver:To Print Selected Blank Waiver.
        If e.CommandName = "printwaiver" Then
            BatchNoJobWaiverId = Convert.ToInt32(e.CommandArgument)
            Dim crfs As New CRFS.ClientView.CrfsBll
            crfs.GetBlankWaiversByBatchNoJobWaiverId(BatchNoJobWaiverId)
            Dim SREPORT As String = LienView.Provider.PrintNoJobWaiver(BatchNoJobWaiverId)

            Me.btnDownLoad.Visible = False
            If IsNothing(SREPORT) = False Then
                'If (crfs.Notary = True) Then
                If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                    PrintNotary(BatchNoJobWaiverId)
                    MergeNotary(SREPORT)
                End If

                Me.btnDownLoad.Visible = True
                Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(SREPORT))
                Me.Session("spreadsheet") = System.IO.Path.GetFileName(SREPORT)

                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If
                'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
                'Me.MultiView1.SetActiveView(Me.View2)
            Else
                Me.plcReportViewer.Controls.Clear()
                Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
                Me.MultiView1.SetActiveView(Me.View2)
            End If
            If Not Me.gvwWaivers.Rows.Count = 0 Then
                Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        End If

        'markparimal-Blank Waiver:To Delete Selected Blank Waiver.
        If e.CommandName = "deletewaiver" Then
            Dim myitem As New CRFDB.TABLES.BatchNoJobWaiverNotice
            BatchNoJobWaiverId = Convert.ToInt32(e.CommandArgument)
            myitem.BatchNoJobWaiverId = BatchNoJobWaiverId
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
            Me.MultiView1.SetActiveView(Me.View3)

            'markparimal-Blank Waiver:To Bind GridView.
            BindgGrid("DateCreated", sortOrder)


        End If
        If e.CommandName = "emailwaiver" Then
            BatchNoJobWaiverId = Convert.ToInt32(e.CommandArgument)
            setMaildetails()
        End If

        If e.CommandName = "copywaiver" Then
            clearfields()
            BatchNoJobWaiverId = Convert.ToInt32(e.CommandArgument)
            hddbTabName.Value = "JobInfo"
            hdnStateSelectedValue.Value = "CA"
            hdnWaiverTypeSelectedId.Value = ""

            Dim copyjobcrfs As New CRFS.ClientView.CrfsBll
            copyjobcrfs.GetBlankWaiversByBatchNoJobWaiverId(BatchNoJobWaiverId)

            ClientTableId.SelectedValue = copyjobcrfs.clientname
            hdnClientSelectedId.Value = copyjobcrfs.clientname
            PaymentAmt.Value = copyjobcrfs.pamt
            IsPaidInFull.Checked = copyjobcrfs.ispaytodate
            DisputedAmt.Value = copyjobcrfs.disputedamt

            MailtoCU.Checked = copyjobcrfs.MailtoCU
            MailtoGC.Checked = copyjobcrfs.MailtoGC
            MailtoOW.Checked = copyjobcrfs.MailtoOW
            MailtoLE.Checked = copyjobcrfs.MailtoLE

            CheckbyCU.Checked = copyjobcrfs.CheckbyCU
            CheckByGC.Checked = copyjobcrfs.CheckByGC
            CheckByOW.Checked = copyjobcrfs.CheckbyOW
            CheckByLE.Checked = copyjobcrfs.CheckbyLE
            CheckbyOT.Checked = copyjobcrfs.CheckByOT

            OtherChkIssuerName.Value = copyjobcrfs.OtherChkIssuerName

            JobNum.Value = copyjobcrfs.JobNum
            JobName.Value = copyjobcrfs.JobName
            JobAddr1.Value = copyjobcrfs.JobAdd1
            JobAddr2.Value = copyjobcrfs.JobAdd2
            JobCity.Value = copyjobcrfs.JobCity
            JobState.Value = copyjobcrfs.JobState
            JobZip.Value = copyjobcrfs.JobZip

            If (copyjobcrfs.JobState = "") Then
                hdnStateSelectedValue.Value = "CA"
            Else
                hdnStateSelectedValue.Value = copyjobcrfs.JobState
            End If

            CustName.Value = copyjobcrfs.CustName
            CustRef.Value = copyjobcrfs.CustNum
            CustAdd1.Value = copyjobcrfs.CustAdd1
            CustCity.Value = copyjobcrfs.CustCity
            CustState.Value = copyjobcrfs.CustState
            CustZip.Value = copyjobcrfs.CustZip

            GCName.Value = copyjobcrfs.GCName
            GCRef.Value = copyjobcrfs.GCNum
            GCAdd1.Value = copyjobcrfs.GCAdd1
            GCCity.Value = copyjobcrfs.GCCity
            GCState.Value = copyjobcrfs.GCState
            GCZip.Value = copyjobcrfs.GCZip

            OwnrName.Value = copyjobcrfs.OwnerName
            OwnrAdd1.Value = copyjobcrfs.OwnerAdd1
            OwnrCity.Value = copyjobcrfs.OwnerCity
            OwnrState.Value = copyjobcrfs.OwnerState
            OwnrZip.Value = copyjobcrfs.OwnerZip

            BatchNoJobWaiverId = 0
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & hdnStateSelectedValue.Value & "');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0)", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
            Me.MultiView1.SetActiveView(Me.View1)
            hdnflag.Value = "edit"
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)

    End Sub

    Protected Sub gvwWaivers_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwWaivers.RowCreated
        Dim DR As System.Data.DataRowView = e.Row.DataItem
        If e.Row.RowType = DataControlRowType.DataRow Then

            'btnEditWaiver = TryCast(e.Row.FindControl("btnEditWaiver"), ImageButton)
            'AddHandler CType(e.Row.FindControl("btnEditWaiver"), ImageButton).Click, AddressOf btnEditWaiver_Click
            'btnEditWaiver.Attributes("BatchNoJobWaiverId") = DR("BatchNoJobWaiverId")
        End If

        If e.Row.RowType = DataControlRowType.Header Then
            '  addSortImage(e.Row) 'Commented by Jaywanti
        End If
    End Sub

    Protected Sub gvwWaivers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwWaivers.RowDataBound
        Dim DR As System.Data.DataRowView = e.Row.DataItem
        If e.Row.RowType = DataControlRowType.DataRow Then
            TryCast(e.Row.FindControl("btnDeleteWaiver"), LinkButton).Attributes("rowno") = DR("BatchNoJobWaiverId")
            'btnEditWaiver = TryCast(e.Row.FindControl("btnEditWaiver"), ImageButton)
            'AddHandler CType(e.Row.FindControl("btnEditWaiver"), ImageButton).Click, AddressOf btnEditWaiver_Click
            'btnEditWaiver.Attributes("BatchNoJobWaiverId") = DR("BatchNoJobWaiverId")
        End If
    End Sub


    Protected Sub gvwWaivers_Sorted(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub gvwWaivers_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs)
        ViewState("sVS_SortExpression") = e.SortExpression
        BindgGrid(e.SortExpression, sortOrder)

        If Session("sortOrder") = "desc" Then

        End If


    End Sub

    Private Sub addSortImage(ByVal paramHeaderRow As GridViewRow)
        Dim imgSortDirection As New Image, iSortColumnIndex As Integer = 0, iSortColumnIndex1 As Integer = 2
        getSortColumnIndex(iSortColumnIndex)
        'getSortColumnIndex(iSortColumnIndex1)
        If iSortColumnIndex >= 0 Then
            If Session("sortOrder").ToString.ToUpper = "ASC" Then
                ' imgSortDirection.ImageUrl = "~\images\triangle_0_up.gif"
            Else
                ' imgSortDirection.ImageUrl = "~\images\triangle_0_down.GIF"
            End If
            paramHeaderRow.Cells(iSortColumnIndex).Controls.Add(imgSortDirection)
            'getSortColumnIndex1(iSortColumnIndex1)
            'paramHeaderRow.Cells(iSortColumnIndex1).Controls.Add(imgSortDirection)
        End If
    End Sub
    Private Sub getSortColumnIndex(ByRef iSortColumnIndex As Integer)
        Dim sSortExpression As String = ViewState("sVS_SortExpression").ToString
        If Not String.IsNullOrEmpty(sSortExpression) Then
            For Each gvColumn As DataControlField In gvwWaivers.Columns
                If gvColumn.SortExpression = sSortExpression Then
                    iSortColumnIndex = gvwWaivers.Columns.IndexOf(gvColumn)
                    Exit For
                End If
            Next

        End If
    End Sub
    Protected Sub btnDrpClient_Click(sender As Object, e As EventArgs)
        hdnWaiverTypeSelectedId.Value = "0"
        LoadData()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & hdnStateSelectedValue.Value & "');", True)
        If (hdnSignerSelectedValue.Value = "") Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0)", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & Convert.ToInt32(hdnSignerSelectedValue.Value) & ");", True)
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & hdnSignerSelectedValue.Value & ");", True)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDrp", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
    End Sub
    Protected Sub btnDropStateClick_Click(sender As Object, e As EventArgs)
        hdnWaiverTypeSelectedId.Value = "0"
        LoadData()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsStateDropDown", "BindStateDropdown();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsClientDropDown", "BindClientDropdown(" & Me.CurrentUser.Id & ");", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsWaiverTypeDropDown", "BindWaiverTypeDropdown('" & hdnStateSelectedValue.Value & "');", True)
        If (hdnSignerSelectedValue.Value = "") Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(0)", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & Convert.ToInt32(hdnSignerSelectedValue.Value) & ");", True)
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsSignerDropDown", "BindSignerDropdown(" & hdnSignerSelectedValue.Value & ");", True)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub

    Protected Sub btnDeleteId_Click(sender As Object, e As EventArgs)
        Dim myitem As New CRFDB.TABLES.BatchNoJobWaiverNotice
        BatchNoJobWaiverId = Convert.ToInt32(hdnId.Value)
        myitem.BatchNoJobWaiverId = BatchNoJobWaiverId
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)
        Me.MultiView1.SetActiveView(Me.View3)

        'markparimal-Blank Waiver:To Bind GridView.
        BindgGrid("CustName,DateCreated", sortOrder)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub
End Class
