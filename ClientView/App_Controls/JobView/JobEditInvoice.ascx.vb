
Partial Class App_Controls__Custom_LienView_JobEditInvoice
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobInvoices
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim ItemId As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Me.ItemId = qs.GetParameter("ItemId")
        myjob = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobById(ItemId)
        Me.litJobInfo.Text = " Invoice Log for CRF# (" & myjob.Id & ") "
        Me.litJobInfo.Text += " Job# (" & myjob.JobNum.Trim & ") "
        Me.litJobInfo.Text += " JobName (" & myjob.JobName.Trim & ") "

        If Me.Page.IsPostBack = False Then

            If qs.HasParameter("SubId") Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
            End If
            If myreq.Id < 1 Then
                myreq.InvoiceDate = Today
            End If
            With myreq
                Me.InvoiceDate.Value = .InvoiceDate
                Me.InvoiceRefNum.Value = .InvoiceRefNum
                Me.InvoiceAmount.Value = .InvoiceAmount
                Me.PmtAmount.Value = .PmtAmount
                Me.AdjAmount.Value = .AdjAmount
                'Me.AmountOwed.Value = .AmountOwed
                Me.Description.Text = .Description
            End With
            Me.MyPage.SetFocus(Me.InvoiceAmount)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Me.Page.Validate()
        If Page.IsValid = False Then
            Exit Sub
        End If

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If qs.HasParameter("SubId") Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("SubId"), myreq)
        End If
        With myreq
            .JobId = ItemId
            .DateCreated = Now()
            .InvoiceDate = Me.InvoiceDate.Value
            .InvoiceRefNum = Me.InvoiceRefNum.Value
            .InvoiceAmount = Me.InvoiceAmount.Value
            .PmtAmount = Me.PmtAmount.Value
            .AdjAmount = Me.AdjAmount.Value
            .AmountOwed = .InvoiceAmount - .PmtAmount - .AdjAmount
            .Description = Me.Description.Text
            .SubmittedBy = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .SubmittedByUserId = Me.CurrentUser.Id
        End With

        Me.CustomValidator1.IsValid = True
        Me.CustomValidator1.ErrorMessage = ""
        If myreq.Id > 0 Then
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
        Else
            myreq.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myreq)
        End If

        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&view=" & "INVC"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Private Function setdate(ByVal adate As String) As String
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return ""
        End If
        If s = Date.MinValue Then
            Return ""
        End If
        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Private Function getdate(ByVal adate As String) As Date
        Dim s As Date = Date.MinValue
        If Date.TryParse(adate, s) = False Then
            Return Date.MinValue
        End If

        Return Strings.FormatDateTime(s, DateFormat.ShortDate)

    End Function
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&view=" & "INVC"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
End Class
