﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Printing" %>
<%@ Import Namespace="System.IO" %>

<style>
    #JobFilterBody .radio-custom label {
        width: 75px;
    }

    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 4px;
    }

    .dataTables_length {
        margin-top: 10px;
    }

    .nowordwraptd {
        /*padding-left: 5px;*/
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .activePage {
        z-index: 3;
        color: white !important;
        cursor: default;
        background-color: #3a6dae !important;
        border-color: #3a6dae !important;
    }
    .hide-et{
        display:none;
    }
</style>

<script type="text/javascript">
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('RentalView', 'Tools', 'Bulk Notice Request');
        MainMenuToggle('liRentalToolsJobView');
        SubMenuToggle('liBulkNoticeRequestJobView');

        return false;
    }

    function ClearControl() {
        $('.datepicker').val("");
    }

    function modalsuccessShow() {               
        $("#ModelRequestverification").modal('show');
        return false;
    }

    function modalsuccessHide() {        
        $("#ModelRequestverification").modal('hide');
        $('div.modal-backdrop').remove()
        return false;
    }

    function VerifyRequestNotice() {
        swal({
            title: "Are you sure?",
            text: "Request Notice on this Job?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: 'No',
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            closeOnConfirm: true
        },
function () {
    alert();
    <%--document.getElementById('<%=btnBackFilter.ClientID%>').oncommand();--%>
});
        return false;
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {

        validateJobFilter();

        $("div").on('blur change keyup', "input", function () {
            var $element = $(this);
            var etype = $element.attr('type');

            if (etype == "text" || etype == "checkbox") {
                validateJobFilter();
            }
        });

        $("div").on("click", "input:text[id*=txtJobBalance]", function () {
            var textbox = $(this);
            textbox.focus();
            textbox.select();

        });
        $("div").on("blur", "input:text[id*=txtJobBalance]", function () {
            var textbox = $(this);
            if (textbox.val().length > 0 && textbox.val().indexOf("$") == -1) {
                var value = parseFloat(textbox.val())
                textbox.val('$' + value.toFixed(2));
            }
        });


        $("div").on("click", "input:text[id*=txtEstBalance]", function () {
            var textbox = $(this);
            textbox.focus();
            textbox.select();

        });
        $("div").on("blur", "input:text[id*=txtEstBalance]", function () {
            var textbox = $(this);
            if (textbox.val().length > 0 && textbox.val().indexOf("$") == -1) {
                var value = parseFloat(textbox.val())
                textbox.val('$' + value.toFixed(2));
            }
        });
    });

    function validateJobFilter() {
        var ISValid = true;
        $("#modcontainer").find("input").each(function (idx) {
            var $element = $(this);
            var eType = $element.attr('type');
            // console.log($element);
            if ($element.attr('id') == '<%=Calendar3.ClientID %>' || $element.attr('id') == '<%=Calendar4.ClientID %>' || $element.attr('id') == '<%=FromBalance.ClientID %>' || $element.attr('id') == '<%=ThruBalance.ClientID %>' ||
                $element.attr('id') == '<%=EstFromBalance.ClientID %>' || $element.attr('id') == '<%=EstThruBalance.ClientID %>') {
                return true;
            }

            if (eType == "text") {
                var textBox = $.trim($element.val());
                if (textBox == "") {
                    ISValid = false;
                }
                else {
                    ISValid = true;
                }
            }
            else if (eType == "checkbox") {
                if ($element.prop("checked") == false) {
                    ISValid = false;
                } else {
                    ISValid = true;
                }
            }

            if (ISValid == true) {
                return false;
            }
        });

        if (ISValid == false) {
            $("#ctl33_ctl00_btnSubmit").prop("disabled", true);
        }
        else {
            $("#ctl33_ctl00_btnSubmit").prop("disabled", false);
        }
    }

    function myFunctionCustomerNameLike(id) {
        if (id == 1) {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionCustomerRefLike(id) {
        if (id == 1) {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNameLike(id) {
        if (id == 1) {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobAddressLike(id) {
        if (id == 1) {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNoLike(id) {
        if (id == 1) {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = true;
        }
    }

    function CheckDeadlineDateRange() {
        if ((document.getElementById('<%=Calendar3.ClientID%>').value != "") &&
            (document.getElementById('<%=Calendar4.ClientID%>').value != "")) {
            $('#<%=chkByDeadlineDate.ClientID%>').prop('checked', true);
            validateJobFilter()
        }
        else {
            $('#<%=chkByDeadlineDate.ClientID%>').prop('checked', false);
        }
    }

    var withoutcurrfrombal;
    var withoutcurrthrubal;
    function FromBalance_LostFocus() {
        var FromBalanceCntrl = $('#<%=FromBalance.ClientID%>');
        withoutcurrfrombal = FromBalanceCntrl.val();
        if (FromBalanceCntrl.val().length > 0 && FromBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(FromBalanceCntrl.val().replace(",", ""))
            FromBalanceCntrl.val('$' + value.toFixed(2));
            checkJobBalanceRange();
        }
    }

    function ThruBalance_LostFocus() {
        var ThruBalanceCntrl = $('#<%=ThruBalance.ClientID%>');
        withoutcurrthrubal = ThruBalanceCntrl.val();
        if (ThruBalanceCntrl.val().length > 0 && ThruBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(ThruBalanceCntrl.val().replace(",", ""))
            ThruBalanceCntrl.val('$' + value.toFixed(2));
            checkJobBalanceRange();
        }
    }

    function checkJobBalanceRange() {
        if (withoutcurrfrombal > 0 && withoutcurrthrubal > 0) {
            $('#<%=chkByBalance.ClientID%>').prop('checked', true);
            validateJobFilter();
        }
        else {
            $('#<%=chkByBalance.ClientID%>').prop('checked', false);
        }
    }

    var withoutcurrEstfrombal;
    var withoutcurrEstthrubal;
    function EstFromBalance_LostFocus() {
        var EstFromBalanceCntrl = $('#<%=EstFromBalance.ClientID%>');
        withoutcurrEstfrombal = EstFromBalanceCntrl.val();
        if (EstFromBalanceCntrl.val().length > 0 && EstFromBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(EstFromBalanceCntrl.val().replace(",", ""))
            EstFromBalanceCntrl.val('$' + value.toFixed(2));
            checkEstBalanceRange();
        }
    }

    function EstThruBalance_LostFocus() {
        var EstThruBalanceCntrl = $('#<%=EstThruBalance.ClientID%>');
        withoutcurrEstthrubal = EstThruBalanceCntrl.val();
        if (EstThruBalanceCntrl.val().length > 0 && EstThruBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(EstThruBalanceCntrl.val().replace(",", ""));
            EstThruBalanceCntrl.val('$' + value.toFixed(2));
            checkEstBalanceRange();
        }
    }

    function checkEstBalanceRange() {
        if (withoutcurrEstfrombal > 0 && withoutcurrEstthrubal > 0) {
            $('#<%=chkByEstBalance.ClientID%>').prop('checked', true);
            validateJobFilter();
        }
        else {
            $('#<%=chkByEstBalance.ClientID%>').prop('checked', false);
        }
    }
    ////////////////////////////////////////////////////
        function ShowServiceTypeModal()
        {
            BindServiceTypeDropdown();
        $("#ModalServiceType").modal('show');
    }
        function BindServiceTypeDropdown() {
        debugger;
        $('#UlServiceType li').remove();
      
        if ($('#' + '<%=hdnState.ClientID%>').val() == "TX") {
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerTX" onclick="setSelectedSserviceType(5,\'Verify Owner TX\')">Verify Owner TX</a></li>');


            $('#btnServiceType').html('Verify Job Data Only - No Notice Sent' + "<span class='caret' ></span>");
                var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                x.value = '2';
        }
            else  if ( $('#' + '<%=hdnState.ClientID%>').val() == "TN") {
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(8,\'Verify Owner Only\')">Verify Owner Only</a></li>');


            $('#btnServiceType').html('Verify Job Data Only - No Notice Sent' + "<span class='caret' ></span>");
                var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                x.value = '2';
        }

        else {

            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(6,\'Send Notice With Data Provided\')">Send Notice With Data Provided</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerSend" onclick="setSelectedSserviceType(7,\'Verify Owner Send\')">Verify Owner Send</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(8,\'Verify Owner Only\')">Verify Owner Only</a></li>');

                $('#btnServiceType').html('Verify Job Data - Send Notice' + "<span class='caret' ></span>");
                var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                x.value = '1';            
        }

    return false;
        }

        function setSelectedSserviceType(id, TextData) {
    //$('#' + '<%=hdnServiceTypeId.ClientID%>').val(id);
        $('#btnServiceType').html(TextData + "<span class='caret' ></span>");
        var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
        x.value = '1';
        if (TextData == "Verify Job Data Only - No Notice Sent")
        {
            x.value = '2';
        }
        else if (TextData == "Verify Job Data - Send Notice") {
            x.value = '1';
        }
        else if (TextData == "Store Job With Data Provided") {
            x.value = '3';
        }
        else if (TextData == "Verify Owner TX") {
            x.value = '7';
        }
        else if (TextData == "Send Notice With Data Provided") {
            x.value = '4';
        }
        else if (TextData == "Verify Owner Send") {
            x.value = '6';
        }
        else if (TextData == "Verify Owner Only") {
            x.value = '5';
        }
        else
        {
            x.value = '0';
        }

    return false;
    }
    ////////////////////////////////////////////////////
</script>

<script runat="server">
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobNoticeListJV
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Protected WithEvents btnGridNoticeRequest As Button
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim myjobTmp As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        FromBalance.Attributes.Add("onblur", "FromBalance_LostFocus();")
        ThruBalance.Attributes.Add("onblur", "ThruBalance_LostFocus();")
        EstFromBalance.Attributes.Add("onblur", "EstFromBalance_LostFocus();")
        EstThruBalance.Attributes.Add("onblur", "EstThruBalance_LostFocus();")
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)

        If Me.Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)

            'If qs.HasParameter("page") Then
            '    If (IsNothing(Session("filterOrder")) = False And IsNothing(Session("filterColumn")) = False) Then
            '        drpSortType.Items.FindByValue(Session("filterOrder")).Selected = True
            '        drpSortBy.Items.FindByValue(Session("filterColumn")).Selected = True
            '    End If

            '    If IsNothing(Session("mysproc")) Then
            '        mysproc.UserId = Me.CurrentUser.Id
            '        Session("mysproc") = mysproc
            '    Else
            '        mysproc = Session("mysproc")
            '    End If

            '    If IsNothing(Session("pageIndex")) Then
            '        GetJobsPageWise(mysproc, 1)
            '    Else
            '        GetJobsPageWise(mysproc, Convert.ToInt32(Session("pageIndex")))
            '    End If
            'Else
            '    mysproc.UserId = Me.CurrentUser.Id
            '    Session("mysproc") = mysproc
            '    GetJobsPageWise(mysproc, 1)
            'End If
        Else
            If (IsNothing(Session("filterOrderBNR")) = False And IsNothing(Session("filterColumnBNR")) = False) Then
                drpSortType.Items.FindByValue(Session("filterOrderBNR")).Selected = True
                drpSortBy.Items.FindByValue(Session("filterColumnBNR")).Selected = True
            End If

            If IsNothing(Session("mysprocBNR")) Then
                mysproc.UserId = Me.CurrentUser.Id
                Session("mysprocBNR") = mysproc
            Else
                mysproc = Session("mysprocBNR")
            End If

            If IsNothing(Session("pageIndexBNR")) Then
                GetJobsPageWise(mysproc, 1)
            Else
                GetJobsPageWise(mysproc, Convert.ToInt32(Session("pageIndexBNR")))
            End If

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        With mysproc
            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike_1.Checked Then
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If
                If Me.CustomerNameLike_2.Checked Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                End If
            Else
                .ClientCustomer = ""
            End If


            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike_1.Checked Then
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If
                If Me.CustomerRefLike_2.Checked Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                End If
            Else
                .ClientCustomerRef = ""
            End If

            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobName = Me.JobName.Text.Trim & "%"
                End If
                If Me.JobNameLike_2.Checked Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                End If
            Else
                .JobName = ""
            End If

            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike_1.Checked Then
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If

                If Me.JobAddressLike_2.Checked Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                End If
            Else
                .JobAdd1 = ""
            End If

            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNoLike_1.Checked Then
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If
                If Me.JobNoLike_2.Checked Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                End If
            Else
                .JobNumber = ""
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            Else
                .JobState = ""
            End If

            .FromJobBalance = Utils.GetDecimal(Me.FromBalance.Value)
            .ThruJobBalance = Utils.GetDecimal(Me.ThruBalance.Value)
            If chkByBalance.Checked Then
                .JobBalanceRange = 1
            Else
                .JobBalanceRange = 0
            End If

            .FromEstBalance = Utils.GetDecimal(Me.EstFromBalance.Value)
            .ThruEstBalance = Utils.GetDecimal(Me.EstThruBalance.Value)
            If chkByEstBalance.Checked Then
                .EstBalanceRange = 1
            Else
                .EstBalanceRange = 0
            End If

            .FromDeadlineDate = Me.Calendar3.Text
            .ThruDeadlineDate = Me.Calendar4.Text
            If chkByDeadlineDate.Checked Then
                .NoticeDeadlineRange = 1
            Else
                .NoticeDeadlineRange = 0
            End If

            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            Else
                .BRANCHNUMBER = ""
            End If

            ' If chkAllJobs.Checked Then
            .ActiveOnly = False
            'Else
            ' .ActiveOnly = True
            ' End If

            .UserId = Me.CurrentUser.Id

            If Me.txtFilterKey.Text.Trim.Length > 0 Then
                .FilterKey = Me.txtFilterKey.Text.Trim & "%"
            Else
                .FilterKey = ""
            End If



        End With

        Session("mysprocBNR") = mysproc
        Session("pageIndexBNR") = 1
        GetJobsPageWise(mysproc, 1)
        Me.MultiView1.SetActiveView(Me.View2)
        If gvwJobs.Rows.Count > 0 Then
            Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)

    End Sub

    Private Sub GetJobsPageWise(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobNoticeListJV, ByVal pageIndex As Integer)
        asproc.Page = pageIndex
        asproc.PageRecords = Integer.Parse(ddlPageSize.SelectedValue)

        myview = GetSortedData(drpSortType.SelectedItem.Value, drpSortBy.SelectedItem.Value, asproc)

        If myview.Count < 1 Then
            Me.gvwJobs.DataSource = Nothing
            Me.gvwJobs.DataBind()

            Exit Sub
        End If

        Dim totrecs As String = myview.RowItem("totalrecords")
        Session("TotalRecordsBNR") = totrecs
        Me.gvwJobs.DataSource = myview
        Me.gvwJobs.DataBind()
        If gvwJobs.Rows.Count > 0 Then
            Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        Me.MultiView1.SetActiveView(Me.View2)
        Me.PopulatePager(totrecs, pageIndex)
    End Sub

    Private Sub PopulatePager(ByVal recordCount As Integer, ByVal currentPage As Integer)
        Dim dblPageCount As Double = CType((CType(recordCount, Decimal) / Decimal.Parse(ddlPageSize.SelectedValue)), Double)
        Dim pageCount As Integer = CType(Math.Ceiling(dblPageCount), Integer)
        Dim pages As New List(Of ListItem)

        If (pageCount > 0) Then

            Dim showMax As Integer = 5
            Dim startPage As Integer
            Dim endPage As Integer

            If (pageCount <= showMax) Then
                startPage = 1
                endPage = pageCount
            Else
                startPage = currentPage
                If (pageCount - currentPage <= showMax - 1) Then
                    endPage = pageCount
                Else
                    endPage = currentPage + showMax - 1
                End If
            End If

            pages.Add(New ListItem("<<", "1", (currentPage > 1)))
            'pages.Add(New ListItem("<", currentPage - 1.ToString, (currentPage > 1)))

            For i As Integer = startPage To endPage
                pages.Add(New ListItem(i.ToString, i.ToString, (i <> currentPage)))
            Next

            'pages.Add(New ListItem(">", currentPage + 1.ToString, (currentPage < pageCount)))
            pages.Add(New ListItem(">>", pageCount.ToString, (currentPage < pageCount)))

        End If
        rptPager.DataSource = pages
        rptPager.DataBind()
        Dim recordsperpage As Integer = CType(ddlPageSize.SelectedValue, Integer)
        Dim FromRecord As Integer = ((recordsperpage * currentPage) - recordsperpage) + 1
        Dim ToRecord As Integer
        If currentPage = pageCount Then
            ToRecord = recordCount
        Else
            ToRecord = recordsperpage * currentPage
        End If

        Dim info As String = "Showing " & FromRecord & " to " & ToRecord & " of " & recordCount & " entries"
        lblTableInfo.Text = info
    End Sub

    Private Function GetSortedData(ByVal SortTypeValue As String, ByVal SortByValue As String, mysprocSort As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobNoticeListJV)
        Dim SortType As String = "ASC"
        If SortTypeValue = "-1" Then
            SortType = "ASC"
        ElseIf SortTypeValue = "1" Then
            SortType = "DESC"
        End If

        Dim SortBy As String
        If SortByValue = "-1" Then
            SortBy = "ListId " + SortType
        ElseIf SortByValue = "1" Then
            SortBy = "ClientCustomer " + SortType
        ElseIf SortByValue = "2" Then
            SortBy = "CustRefNum " + SortType
        ElseIf SortByValue = "3" Then
            SortBy = "JobNum " + SortType
        ElseIf SortByValue = "4" Then
            SortBy = "JobName " + SortType
        ElseIf SortByValue = "5" Then
            SortBy = "JobBalance " + SortType
        ElseIf SortByValue = "6" Then
            SortBy = "EstBalance " + SortType
        ElseIf SortByValue = "7" Then
            SortBy = "NoticeDeadlineDate " + SortType
        ElseIf SortByValue = "8" Then
            SortBy = "JobState " + SortType
        ElseIf SortByValue = "9" Then
            SortBy = "BranchNum " + SortType
        ElseIf SortByValue = "10" Then
            SortBy = "FilterKey " + SortType
        End If

        mysprocSort.SortColType = SortBy
        myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(mysprocSort)

        If myviewSorting.Count < 1 Then
            Me.gvwJobs.DataSource = Nothing
            Me.gvwJobs.DataBind()
        End If

        Session("filterOrderBNR") = SortTypeValue
        Session("filterColumnBNR") = SortByValue
        Session("JobListBNR") = mysprocSort
        Return myviewSorting
    End Function

    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        mysproc = Session("mysprocBNR")
        GetJobsPageWise(mysproc, 1)
    End Sub

    Protected Sub drpSortType_SelectedIndexChanged(sender As Object, e As EventArgs)
        mysproc = Session("mysprocBNR")
        GetJobsPageWise(mysproc, 1)
    End Sub

    'Custom Pagging for job list
    Protected Sub PageSize_Changed(ByVal sender As Object, ByVal e As EventArgs)
        mysproc = Session("mysprocBNR")
        Session("pageIndexBNR") = 1
        Me.GetJobsPageWise(mysproc, 1)
    End Sub

    Protected Sub Page_Changed(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageIndex As Integer = Integer.Parse(CType(sender, LinkButton).CommandArgument)
        Session("pageIndexBNR") = pageIndex
        mysproc = Session("mysprocBNR")
        Me.GetJobsPageWise(mysproc, pageIndex)
    End Sub

    Protected Sub btnRequestNotice_Command(sender As Object, e As CommandEventArgs)
        ''''''''''''''''''''''''''''''''''
        Dim myitemidNewTest As String = e.CommandArgument.ToString()
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myitemidNewTest, myjobTmp)
        hdnState.Value = myjobTmp.JobState
        HdnJobId.Value = myjobTmp.Id
        '''''''''''''''''''''''''''''''''
        btnGridNoticeRequest = TryCast(sender, Button)
        Dim txtJobBalance As TextBox = CType(btnGridNoticeRequest.Parent.FindControl("txtJobBalance"), TextBox)
        Dim txtEstBalance As TextBox = CType(btnGridNoticeRequest.Parent.FindControl("txtEstBalance"), TextBox)
        Dim myitemidNew As String = e.CommandArgument.ToString()
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myitemidNew, myjob)
        With myjob
            If (txtEstBalance.Text.Replace("$", "").Trim() <> "") Then
                .EstBalance = txtEstBalance.Text.Replace("$", "").Trim()
            End If

            If (txtJobBalance.Text.Replace("$", "").Trim() <> "") Then
                .JobBalance = txtJobBalance.Text.Replace("$", "").Trim()
            End If

        End With
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        If (e.CommandName.Equals("Save")) Then
            Dim myitemid As String = e.CommandArgument.ToString()

            Dim myNoticeRequestsproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_JobView_NoticeRequest
            myNoticeRequestsproc.JobId = myitemid
            myNoticeRequestsproc.submittedbyuserid = Me.CurrentUser.Id
            myNoticeRequestsproc.JobTypeOption = 0
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecNonQuery(myNoticeRequestsproc)

            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = myitemid
                .DateCreated = Now()
                .Note = "Job assigned for information verification. Job is now being researched."
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
            End With

            Dim pageIndex As Integer = Session("pageIndexBNR")
            mysproc = Session("mysprocBNR")
            Me.GetJobsPageWise(mysproc, pageIndex)
            'Commented the code which shows success modal And inserted service type modal code. after that
            ' modal success will be shown
            ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalsuccessShow();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jstmp", "ShowServiceTypeModal();", True)
        End If

    End Sub

    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackFilter.Click
        Me.MultiView1.SetActiveView(Me.View1)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ClearControl", "ClearControl();", True)
        Me.CustomerName.Text = ""
        Me.CustomerNameLike_1.Checked = True
        Me.CustomerNameLike_2.Checked = False
        Me.CustomerRef.Text = ""
        Me.CustomerRefLike_1.Checked = True
        Me.CustomerRefLike_2.Checked = False
        Me.JobName.Text = ""
        Me.JobNameLike_1.Checked = True
        Me.JobNameLike_2.Checked = False
        Me.JobAddress.Text = ""
        Me.JobAddressLike_1.Checked = True
        Me.JobAddressLike_2.Checked = False
        Me.JobNo.Text = ""
        Me.JobNoLike_1.Checked = True
        Me.JobNoLike_2.Checked = False
        Me.JobState.Text = ""
        Me.FromBalance.Text = "$0.00"
        Me.ThruBalance.Text = "$0.00"
        Me.chkByBalance.Checked = False
        Me.EstFromBalance.Text = "$0.00"
        Me.EstThruBalance.Text = "$0.00"
        Me.chkByEstBalance.Checked = "$0.00"
        Me.chkByDeadlineDate.Checked = False
        Me.BranchNo.Text = ""
        Me.txtFilterKey.Text = ""
        ' Me.chkAllJobs.Checked = False

    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''
    Protected Sub BtnOk_Click(sender As Object, e As CommandEventArgs)
        Dim CheckServiceTypeId = Me.hdnServiceTypeId.Value
        Dim JobId = HdnJobId.Value

        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_JobView_NoticeRequest
        mysproc.JobId = JobId
        mysproc.submittedbyuserid = Me.CurrentUser.Id
        mysproc.JobTypeOption = CheckServiceTypeId
        CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecNonQuery(mysproc)

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobId, myjob)
        If (CheckServiceTypeId = "2") Then
            With myjob
                .VerifyJob = 1
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 11
                '.StatusCode = "AVO"
            End With
        End If

        If (CheckServiceTypeId = "1") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 1
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 3
                '.StatusCode = "ACT"
            End With
        End If

        If (CheckServiceTypeId = "3") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 1
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 39
                '.StatusCode = "CVA"
            End With
        End If

        If (CheckServiceTypeId = "7") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 1
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 11
                '.StatusCode = "AVO"
            End With
        End If

        If (CheckServiceTypeId = "4") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 1
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 29
                '.StatusCode = "CNA"
            End With
        End If

        If (CheckServiceTypeId = "6") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 1
                .VerifyOWOnly = 0
                '.StatusCodeId = 76
                '.StatusCode = "AOO"
            End With
        End If

        If (CheckServiceTypeId = "5") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 1
                '.StatusCodeId = 76
                '.StatusCode = "AOO"
            End With
        End If

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalsuccessShow();", True)
    End Sub
    '''''''''''''''''''''''''''''''''''''''''''''
</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="width: 100%;" class="margin-bottom-0">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Job Filter</h1>
                    <div class="body" id="JobFilterBody">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CustomerName" TabIndex="1" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="CustomerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerNameLike(1)" TabIndex="2" />
                                        <asp:RadioButton ID="CustomerNameLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerNameLike(2)" TabIndex="3" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer Ref:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CustomerRef" TabIndex="4" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="CustomerRefLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerRefLike(1)" TabIndex="5" />
                                        <asp:RadioButton ID="CustomerRefLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerRefLike(2)" TabIndex="6" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Job Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobName" CssClass="form-control" meta:resourcekey="JobNameResource1" MaxLength="45" TabIndex="7" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="JobNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNameLike(1)" TabIndex="8" />
                                        <asp:RadioButton ID="JobNameLike_2" runat="server" Text="Includes" onchange="myFunctionJobNameLike(2)" TabIndex="9" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Job Address:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobAddress" CssClass="form-control" meta:resourcekey="JobAddressResource1" MaxLength="45" TabIndex="10" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="JobAddressLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobAddressLike(1)" TabIndex="11" />
                                        <asp:RadioButton ID="JobAddressLike_2" runat="server" Text="Includes" onchange="myFunctionJobAddressLike(2)" TabIndex="12" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Job #:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobNo" CssClass="form-control" meta:resourcekey="JobNoResource1" MaxLength="45" TabIndex="13" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="JobNoLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNoLike(1)" TabIndex="14" />
                                        <asp:RadioButton ID="JobNoLike_2" runat="server" Text="Includes" onchange="myFunctionJobNoLike(2)" TabIndex="15" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Job State:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="JobState" CssClass="form-control" Width="50%" Style="text-align: center" meta:resourcekey="JobStateResource1" MaxLength="2" TabIndex="16" />
                                </div>
                            </div>

                            <%-- FetChed from Job View List --%>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Balance:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5" style="display: flex;">
                                    <cc1:DataEntryBox ID="FromBalance" runat="server" CssClass="form-control" DataType="Money"
                                        EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                        FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                        Value="0.00" Width="120px" TabIndex="17" AutoPostBack="false"></cc1:DataEntryBox>
                                    &nbsp; &nbsp;&nbsp;Thru&nbsp;
                                       <cc1:DataEntryBox ID="ThruBalance" runat="server" CssClass="form-control" DataType="Money"
                                           EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                           FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                           Value="0.00" Width="120px" TabIndex="18" AutoPostBack="false"></cc1:DataEntryBox>
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByBalance" CssClass="redcaption"
                                            runat="server" Text=" Use Job Balance Range"
                                            Width="224px" TabIndex="19" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Estimated Balance:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5" style="display: flex;">
                                    <cc1:DataEntryBox ID="EstFromBalance" runat="server" CssClass="form-control" DataType="Money"
                                        EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                        FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                        Value="0.00" Width="120px" TabIndex="20" AutoPostBack="false"></cc1:DataEntryBox>
                                    &nbsp; &nbsp;&nbsp;Thru&nbsp;
                                       <cc1:DataEntryBox ID="EstThruBalance" runat="server" CssClass="form-control" DataType="Money"
                                           EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                           FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                           Value="0.00" Width="120px" TabIndex="21" AutoPostBack="false"></cc1:DataEntryBox>
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByEstBalance" CssClass="redcaption"
                                            runat="server" Text=" Use Estimated Balance Range"
                                            Width="224px" TabIndex="22" />
                                    </div>
                                </div>
                            </div>

                            <%-- FetChed from Job View List --%>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Notice Deadline Date:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <asp:TextBox ID="Calendar3" runat="server" Width="120px" onblur="CheckDeadlineDateRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="23"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqq1" runat="server" ControlToValidate="Calendar3" ErrorMessage=""></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;&nbsp;Thru&nbsp;

                                      <asp:TextBox ID="Calendar4" runat="server" Width="120px" onblur="CheckDeadlineDateRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="24"></asp:TextBox>&nbsp;
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Calendar4" ErrorMessage=""></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByDeadlineDate" CssClass="redcaption"
                                            runat="server" Text=" Use Deadline Date Range"
                                            Width="224px" TabIndex="25" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Branch #:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="BranchNo" CssClass="form-control" meta:resourcekey="BranchNoResource1" MaxLength="10" TabIndex="26" Width="95%" />
                                </div>
                            </div>

                            
                             <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable ">Filter Key:</label>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="txtFilterKey" CssClass="form-control" TabIndex="27" Width="95%" />
                                </div>
                            </div>
                             <%-- <div class="form-group">
                                <label class="col-md-3 control-label"></label>
                                <div class="col-md-2">
                                     <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkAllJobs" CssClass="redcaption" runat="server"  Width="224px" Text="Include Archived Jobs" />
                                     </div>
                                </div>
                            </div>--%>

                        </div>
                        <div class="footer">
                            <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">
                                <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="btn btn-primary" CausesValidation="False" Style="margin-top: 5px;" />&nbsp;                                
                                <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>&nbsp;
                            </asp:Panel>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <h1 class="panelheader">Bulk Notice Request</h1>
                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-6" style="text-align: left;">
                                    <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter" CausesValidation="False"></asp:LinkButton>
                                    <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Customer Name" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Cust#" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Job#" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Job Name" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Job Balance" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Estimated Balance" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Notice Deadline" Value="7"></asp:ListItem>
                                         <asp:ListItem Text="Job State" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Branch#" Value="9"></asp:ListItem>
                                         <asp:ListItem Text="Filter Key" Value="10"></asp:ListItem>
                                       
                                    </asp:DropDownList>
                                    <asp:DropDownList ID="drpSortType" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortType_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="Ascending" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Descending" Value="1"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div style="width: auto; height: auto; overflow:auto;">
                            <asp:GridView ID="gvwJobs" runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 14px !important;" EmptyDataText="No Matching Records!">
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <table class="rows" style="width: 100%;table-layout:fixed;">
                                                <tr>
                                                    <td class="col-md-1" style="width: 115px; text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Customer Name:</span></td>
                                                    <td class="col-md-1 nowordwraptd" style="width: 180px;">
                                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%#Eval("ClientCustomer")%>' />
                                                    </td>

                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;width: 90px;"><span style="font-weight: bold;">Job Name:</span></td>
                                                    <td class="col-md-1 nowordwraptd" style="width: 180px;">
                                                        <asp:Label ID="lblJobName" runat="server" Text='<%#Eval("JobName")%>' />
                                                    </td>

                                                    <td class="col-md-1" style="width: 70px;text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Job#:</span></td>
                                                    <td class="col-md-1 nowordwraptd" style="width: 100px;">
                                                        <asp:Label ID="lblJobNum" runat="server" Text='<%#Eval("JobNum")%>' />
                                                    </td>                                                    

                                                    <td class="col-md-1" style="width: 130px; text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Job Balance:</span></td>
                                                    <td class="col-md-1" style="width: 80px;">
                                                        <asp:Label ID="lblJobBalance" runat="server" CssClass="hide-et"   Text='<%#Eval("JobBalance", "{0:c}") %>'></asp:Label>
                                                                  <cc1:DataEntryBox ID="txtJobBalance" onkeydown = "return (event.keyCode!=13);" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px; width: 100px;" runat="server" CssClass="form-control" DataType="Money"
                                                                BlankOnZeroes="False" Text='<%#Eval("JobBalance", "{0:c}")%>'>$0.00</cc1:DataEntryBox>
                                                    </td>

                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px; width: 140px;"><span style="font-weight: bold;">Notice Deadline Date:</span></td>
                                                    <td class="col-md-1" style="width: 55px;">
                                                        <asp:Label ID="lblNoticeDeadlineDate" runat="server" Text='<%#Utils.FormatDate(Eval("NoticeDeadlineDate"))%>' />
                                                    </td>                                                    

                                                    <td class="col-md-1" rowspan="2" style="width: 55px;">                                                        
                                                        <asp:Button ID="btnVerify" runat="server" CssClass="btn btn-primary" Style="padding-top: 0px; padding-bottom: 0px; margin: 2px;white-space: normal;" Text="Request Notice" CommandName="Save" CommandArgument='<%#Eval("JobId")%>' OnCommand="btnRequestNotice_Command" onkeydown = "return (event.keyCode!=13);"/>                                                        
                                                    </td>
                                                    <td class="col-md-1" style="width: 1px;"></td> 
                                                </tr>

                                                <tr>
                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Cust Ref #:</span></td>
                                                    <td class="col-md-1" style="">
                                                        <asp:Label ID="lblCustRefNum" runat="server"  Text='<%#Eval("CustRefNum") %>' />
                                                     / <span style="font-weight: bold;">Key:&nbsp;</span>
                                                         <asp:Label ID="Label1" runat="server"  Text='<%#Eval("FilterKey") %>' />
                                                    </td>

                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Job Address:</span></td>
                                                    <td class="col-md-1 nowordwraptd" style="">
                                                        <asp:Label ID="lblJobAddress" runat="server" Text='<%#Eval("JobAdd1") %>' />&nbsp;
                                                        <asp:Label ID="LabelCity" runat="server" Text='<%#Eval("JobCity") %>' />
                                                    </td>

                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Job State:</span></td>
                                                    <td class="col-md-1" style="">
                                                        <asp:Label ID="lblJobState" runat="server" Text='<%#Eval("JobState")%>' />
                                                    </td>

                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Estimated Balance:</span></td>
                                                    <td class="col-md-1" style="">
                                                        <asp:Label ID="lblEstBalance" CssClass="hide-et" runat="server" Text='<%#Eval("EstBalance", "{0:c}") %>'></asp:Label>
                                                         <cc1:DataEntryBox ID="txtEstBalance" onkeydown = "return (event.keyCode!=13);" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px; width: 100px;" runat="server" CssClass="form-control" DataType="Money"
                                                                BlankOnZeroes="False" Text='<%#Eval("EstBalance", "{0:c}")%>'>$0.00</cc1:DataEntryBox>
                                                    </td>

                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Branch:</span></td>
                                                    <td class="col-md-1" style="">
                                                        <asp:Label ID="lblBranch" runat="server" Text='<%#Eval("BranchNum") %>'></asp:Label>
                                                    </td>   
                                                    
                                                    <%--<td class="col-md-1"></td> --%>                                                
                                                </tr>

                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <br />
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-right: 0px;padding-bottom: 5px;">
                                <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left">
                                    <asp:Label ID="lblTableInfo" runat="server" Style="font-size: 14px"></asp:Label>
                                    <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageSize_Changed">
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="25" Value="25" />
                                        <asp:ListItem Text="50" Value="50" />
                                    </asp:DropDownList>
                                </div>
                                <div style="padding-right: 0px" class="col-md-5 col-sm-5 col-xs-5">
                                    <ul class="pagination pull-right">
                                        <asp:Repeater ID="rptPager" runat="server">
                                            <ItemTemplate>
                                                <li class="page-item">
                                                    <asp:LinkButton ID="lnkPage" CssClass='<%# If(Eval("Enabled") Or (Eval("Enabled") = False And Eval("Text") = "<<") Or (Eval("Enabled") = False And Eval("Text") = ">>"), "page-link ", "page-link activePage")%>' runat="server" CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' Text='<%#Eval("Text") %>' OnClick="Page_Changed">
                                                    </asp:LinkButton>

                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>                                
                            </div>                            
                        </div>

                    </div>
                </asp:View>
            </asp:MultiView>
        </div>

        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif" AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center" HorizontalOffset="150" VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>

<div class="modal fade modal-primary example-modal-lg" id="ModelRequestverification" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">

                <h3 class="modal-title">THANK YOU</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <h3>Your Notice Request Has Been Received</h3>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">                
                <button type="button" class="btn btn-primary margin-0" onclick="modalsuccessHide();" aria-label="Close" style="height: 40px; width: 100px;">
                    <span aria-hidden="true">OK</span>
                </button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-primary example-modal-lg" id="ModalServiceType" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title">SERVICE TYPE</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Service Type:</label>
                                            <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">

                                                <div class="example-wrap" style="margin-bottom: 0; white-space: nowrap; display: none;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PrelimBox" runat="server" Text=" Verify Job Data & Send Notice"
                                                            GroupName="JobType" Checked="True"></asp:RadioButton>
                                                    </div>
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PrelimASIS" runat="server" Text=" Send Notice With Data Provided"
                                                            GroupName="JobType"></asp:RadioButton>
                                                    </div>
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="VerifyJob" runat="server" Text=" Verify Job Data Only - No Notice Sent"
                                                            GroupName="JobType"></asp:RadioButton>
                                                    </div>
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="VerifyJobASIS" runat="server" Text=" Store Job Data As Provided - No Notice Sent"
                                                            GroupName="JobType"></asp:RadioButton>
                                                    </div>
                                                    <button type="button" class="btn btn-outline btn-default" style="display: none;" id="exampleSuccessMessage"
                                                        data-plugin="sweetalert" data-title="Successfully Submitted!" data-text="Your New Job Has Been Successfully Submitted!"
                                                        data-type="success">
                                                        Success message</button>
                                                </div>

                                                <div class="btn-group" style="text-align: left;" align="left">
                                                    <%--##1--%>
                                                    <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="width: 328px; text-align: left;" align="left"
                                                        data-toggle="dropdown" aria-expanded="false" id="btnServiceType" tabindex="-1">                                                    
                                                        --Select Service Type--
                     
                                                     <span class="caret"></span>
                                                    </button>
                                                         <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlServiceType" style="overflow-y: auto;">
                                                        <%-- <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,'Verify Job Data & Send Notice')">Verify Job Data & Send Notice</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(1,'Send Notice With Data Provided')">Send Notice With Data Provided</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(1,'Verify Job Data Only - No Notice Sent')">Verify Job Data Only - No Notice Sent</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(1,'Store Job Data As Provided - No Notice Sent')">Store Job Data As Provided - No Notice Sent</a></li>--%>
                                                    </ul>
                                                </div>
                                                <asp:HiddenField ID="hdnServiceTypeId" runat="server"  Value="1"  />
                                            </div>
                                        </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">
<%--                <asp:Button ID="Button1" runat="server" Text="OK" CssClass="btn btn-primary" OnClick="btnSaveNoticeRequest_Click" Style="display: none" />
                <button type="button" class="btn btn-primary margin-0" onclick="modalsuccessHideServiceType();" aria-label="Close" style="height: 40px; width: 100px;display: none;">
                    <span aria-hidden="true">OK</span>
                </button>--%>               
              <%--  <asp:Button ID="BtnOK" runat="server" Text="OK" CssClass="btn btn-primary" OnClick="BtnOk_Click" />--%>
                 <asp:Button ID="BtnOkNew" runat="server" CssClass="btn btn-primary margin-0" Style="" Text="OK" CommandName="OK"  OnCommand="BtnOk_Click" onkeydown = "return (event.keyCode!=13);"/>     
               <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>
            </div>
        </div>
    </div>
</div>

<asp:HiddenField ID="hdnState" runat="server" />
<asp:HiddenField ID="HdnJobId" runat="server" />
