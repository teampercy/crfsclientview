<%@ Control Language="VB" AutoEventWireup="false" CodeFile="CustNameSearch.ascx.vb" Inherits="ClientViewLiens_CustNameSearch" %>

&nbsp;<asp:LinkButton ID="btnSearch" CssClass="btn_OverWrite btn-primary" runat="server" Text="Find" style="display:none;"/>&nbsp;
<%--<input type="button" id="btn1" onclick="OpenCustSearchModal();" value="Find" class="btn_OverWrite btn-primary" style="font-size:small"  />--%>
<a onclick="OpenCustSearchModal()" class="btn_OverWrite btn-primary" style="font-size:small;float:left;">Find </a>
  
<asp:Panel ID="panCustNameSuggest" runat="server" Style="display: none"
    Width="460px" Font-Bold="True">
    <div id="modcontainer" style="MARGIN: 0px; WIDTH: 460px">
        <h1 class="panelheader">Customer Search</h1>
        <div class="body">
            <asp:LinkButton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Exit" CausesValidation="False" />
            <br />
            <br />
            <hr />
            <table align="center" cellpadding="0" cellspacing="0" border="0" width="460">
                <tr>
                    <td style="vertical-align: top">
                        <br />

                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                            <asp:ListItem Selected="True" Value="NAME">&nbsp;By Name&nbsp;&nbsp;</asp:ListItem>
                            <asp:ListItem Value="REF">&nbsp;By Ref#</asp:ListItem>
                        </asp:RadioButtonList>
                        <asp:TextBox ID="TextBox1" CssClass="textbox" runat="server" Width="210px"></asp:TextBox>&nbsp;&nbsp;
                        <asp:LinkButton ID="btnSearchIt" runat="server" Text="Search" CssClass="button" OnClick="btnSearchIt_Click" />&nbsp;
                    </td>
            </table>
            <div align="center">
                <asp:ListBox ID="ListBox1" Visible="false" runat="server" Width="448px" AutoPostBack="True"></asp:ListBox>
            </div>
            <asp:HiddenField ID="myValue" runat="server" />
            <asp:HiddenField ID="myClientId" runat="server" />
            <asp:HiddenField ID="mylist" runat="server" />
        </div>
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="CancelButton" OkControlID="" DropShadow="true"
    PopupControlID="panCustNameSuggest" TargetControlID="btnSearch">
</ajaxToolkit:ModalPopupExtender>
<script type="text/javascript">
    function callsearch()
    { 
        document.getElementById('<%=btnSearchIt.ClientID%>').click();
      
    }
</script>
<style type="text/css">
    .btn_OverWrite {
        padding: 6px 15px;
        font-size: 15px;
        line-height: 13px;
        font-weight:normal;
        text-decoration:none !important;
        border-radius: 3px;
        -webkit-transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        -o-transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        -webkit-font-smoothing: subpixel-antialiased;
    }
</style>