Imports CRF.CLIENTVIEW.BLL

Partial Class App_Controls_crfLienView_AddLegalPty
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim MYJOB As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim MYITEM As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobLegalParties
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Me.Page.IsPostBack = False Then
            Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
            Me.ViewState("JobId") = qs.GetParameter("ItemId")
            Dim myuser As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users
            If IsNothing(Me.Session("JobState")) = True Then
                CRFVIEW.Globals.DBO.Read(Me.CurrentUser.Id, myuser)
                If myuser.LastJobState.Length > 1 Then
                    Me.GCSearchClientID.Value = myuser.LastClientId
                Else
                    Me.GCSearchClientID.Value = myuser.ClientId
                End If
            Else
                Me.GCSearchClientID.Value = Me.Session("ClientTableId")
            End If
        End If

        'CheckGC()
    End Sub

    Public Sub ClearData(ByVal idjobid As String)
        Me.ViewState("JobId") = idjobid
        Me.ViewState("ItemId") = -1
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(idjobid, MYJOB)
        mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(MYJOB.JobState)
        MYITEM = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobLegalParties
        MYITEM.TypeCode = "GC"

        '4/10/13
        If mystate.MLAgent = True Then
            Me.MLAGENT.Visible = True
        Else
            Me.MLAGENT.Visible = False
        End If
        'Me.MLAGENT.Checked = False

        'Me.RadioButtonList1.Items.Clear()
        'Dim myli As New System.Web.UI.WebControls.ListItem
        'myli = New System.Web.UI.WebControls.ListItem
        'myli.Value = "CU"
        'myli.Text = "Customer"
        'Me.RadioButtonList1.Items.Add(myli)

        'myli = New System.Web.UI.WebControls.ListItem
        'myli.Value = "GC"
        'myli.Text = "Gen Contractor"
        'Me.RadioButtonList1.Items.Add(myli)

        ' Me.MLAgent.Visible = True
        'If MYJOB.JobState = "VA" And MYITEM.TypeCode = "OW" And MYJOB.MLAgent = False Then
        '    'myli = New System.Web.UI.WebControls.ListItem
        '    'myli.Value = "MLAGENT"
        '    'myli.Text = "ML Agent"
        '    'Me.RadioButtonList1.Items.Add(myli)
        '    Me.MLAgent.Visible = True
        'End If

        'myli = New System.Web.UI.WebControls.ListItem
        'myli.Value = "LE"
        'myli.Text = "Surety Lender"
        'Me.RadioButtonList1.Items.Add(myli)

        LoadItem()

    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim s As String = "1"

    End Sub
    Public Sub EditItem(ByVal abatchjobid As String, ByVal aitemid As String)

        Me.ViewState("BatchJobId") = abatchjobid
        Me.ViewState("ItemId") = aitemid

        ItemId.Value = aitemid
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(abatchjobid, MYJOB)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(aitemid, MYITEM)
        LoadItemInModal()
        'LoadItem()
        'Me.ModalPopupExtender1.Show()

    End Sub
    Private Sub LoadItem()


        If mystate.MLAgent = True Then
            Me.MLAGENT.Visible = True
        Else
            Me.MLAGENT.Visible = False
        End If

        With MYITEM
            Me.LPAdd1.Text = .AddressLine1
            Me.LPAdd2.Text = .AddressLine2
            Me.RefNum.Text = .RefNum
            Me.LPCity.Text = .City
            Me.LPName.Text = .AddressName
            Me.LPState.Text = .State
            Me.LPZip.Text = .PostalCode
            Me.LPPhone.Text = Utils.FormatPhoneNo(.Telephone1)
            Me.LPFax.Text = Utils.FormatPhoneNo(.Fax)
            Me.RadioButtonList1.SelectedValue = .TypeCode
            Me.MLAGENT.Checked = False
            If .MLAgent = True Then
                Me.RadioButtonList1.SelectedValue = "OW"
                Me.MLAGENT.Checked = True
            End If
        End With



    End Sub

    Private Sub LoadItemInModal()
        'If mystate.MLAgent = True Then
        '    Me.chkMLAgent.Visible = True
        'Else
        '    Me.chkMLAgent.Visible = False
        'End If

        'With MYITEM
        '    Me.partyAddLine1.Text = .AddressLine1
        '    Me.partyAddLine2.Text = .AddressLine2
        '    Me.partyBondNum.Text = .RefNum
        '    Me.partyCity.Text = .City
        '    Me.partyName.Text = .AddressName
        '    Me.partyState.Text = .State
        '    Me.partyZip.Text = .PostalCode
        '    Me.partyPhone.Text = Utils.FormatPhoneNo(.Telephone1)
        '    Me.partyFax.Text = Utils.FormatPhoneNo(.Fax)
        '    ' Me.RadioButtonList2.SelectedValue = .TypeCode
        '    Me.MLAGENT.Checked = False
        '    If .MLAgent = True Then
        '        'Me.RadioButtonList2.SelectedValue = "OW"
        '        Me.chkMLAgent.Checked = True
        '    End If
        'End With
    End Sub

    Private Sub EnforceRules()
        Select Case Me.RadioButtonList1.SelectedValue
            Case "CU", "LE"
                '     Me.lblRefNUm.Visible = True
                Me.RefNum.Visible = True
                If Me.RadioButtonList1.SelectedValue = "CU" Then
                    '        Me.lblRefNUm.Text = "Reference#"
                Else
                    '       Me.lblRefNUm.Text = "Bond#"
                End If
            Case Else
                '  Me.lblRefNUm.Visible = False
                Me.RefNum.Visible = False
                Me.RefNum.Text = ""
        End Select
    End Sub
    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Me.ViewState("JobId"))

    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        With MYITEM
            .JobId = Me.ViewState("JobId")
            .Id = Me.ViewState("ItemId")
            .TypeCode = Me.RadioButtonList1.SelectedValue
            .AddressLine1 = Me.LPAdd1.Text
            .AddressLine2 = Me.LPAdd2.Text
            .City = Me.LPCity.Text
            .AddressName = Me.LPName.Text
            .State = Me.LPState.Text
            .PostalCode = Me.LPZip.Text
            .Telephone1 = Utils.GetPhoneNo(Me.LPPhone.Text)
            .Fax = Utils.GetPhoneNo(Me.LPFax.Text)
            .RefNum = Me.RefNum.Value
            .IsPrimary = 0
            .MLAgent = False
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(.JobId, MYJOB)
            mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(MYJOB.JobState)
            'If MYJOB.JobState = "VA" And MYITEM.TypeCode = "OW" And MYJOB.MLAgent = False Then
            .MLAgent = Me.MLAGENT.Checked
            '.TypeCode = "OW"

        End With

        If MYITEM.Id > 0 Then
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYITEM)
        Else
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(MYITEM)
        End If

        RaiseEvent ItemSaved()

    End Sub

    Protected Sub btnAddPArty_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddParty.Click
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Dim primarycheck As Integer = 0
        With MYITEM
            .JobId = qs.GetParameter("ItemId")
            .Id = Me.ViewState("ItemId")
            If Me.hdnTabNameLegalParty.Value = "liGneralContractor" Then
                '.TypeCode = "GC"
                '.AddressName = Me.partyNameGC.Text
                '.AddressLine1 = Me.partyAddLine1GC.Text
                '.AddressLine2 = Me.partyAddLine2GC.Text
                '.City = Me.partyCityGC.Text

                '.State = Me.partyStateGC.Text
                '.PostalCode = Me.partyZipGC.Text
                '.Telephone1 = Utils.GetPhoneNo(Me.partyPhoneGC.Text)
                '.Fax = Utils.GetPhoneNo(Me.partyFaxGC.Text)
                '.RefNum = Me.RefGC.Text
                '.IsPrimary = 0
                '.MLAgent = Me.chkMLAgentGC.Checked


                Dim mygc As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientGeneralContractor
                mygc.GeneralContractor = Me.partyNameGC.Text
                mygc.AddressLine1 = Me.partyAddLine1GC.Text
                mygc.AddressLine2 = Me.partyAddLine2GC.Text
                mygc.City = Me.partyCityGC.Text
                mygc.State = Me.partyStateGC.Text
                mygc.PostalCode = Me.partyZipGC.Text
                mygc.RefNum = Me.RefGC.Text
                mygc.Telephone1 = Utils.GetPhoneNo(Me.partyPhoneGC.Text)
                mygc.Telephone2 = Utils.GetPhoneNo(Me.partyFaxGC.Text)
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mygc)

                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("ItemId"), MYJOB)
                MYJOB.GenId = mygc.GenId
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYJOB)

            ElseIf Me.hdnTabNameLegalParty.Value = "liOwnerTenant" Then

                .TypeCode = "OW"
                    .AddressName = Me.partyNameOT.Text
                    .AddressLine1 = Me.partyAddLine1OW.Text
                    .AddressLine2 = Me.partyAddLine2OW.Text
                    .City = Me.partyCityOW.Text
                    .State = Me.partyStateOW.Text
                    .PostalCode = Me.partyZipOW.Text
                    .Telephone1 = Utils.GetPhoneNo(Me.partyPhoneOW.Text)
                    .Fax = Utils.GetPhoneNo(Me.partyFaxOW.Text)
                    '.RefNum = Me.RefGC.Text


                    .MLAgent = Me.chkMLAgentOw.Checked
                    If MYITEM.Id > 0 Then
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYITEM)
                    Else
                        If (CheckLegalParties(.JobId, "OW") = 0) Then
                            .IsPrimary = 1
                            primarycheck = 1
                        Else
                            .IsPrimary = 0
                        End If
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(MYITEM)
                        If primarycheck = 1 Then
                            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("ItemId"), MYJOB)
                            MYJOB.OwnrId = MYITEM.Id
                            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYJOB)
                        End If
                    End If

                ElseIf Me.hdnTabNameLegalParty.Value = "liSuretyLender" Then
                    .TypeCode = "LE"
                .AddressName = Me.partyNameSL.Text
                .AddressLine1 = Me.partyAddLine1LS.Text
                .AddressLine2 = Me.partyAddLine2LS.Text
                .City = Me.partyCityLS.Text
                .State = Me.partyStateLS.Text
                .PostalCode = Me.partyZipLS.Text
                .Telephone1 = Utils.GetPhoneNo(Me.partyPhoneLS.Text)
                .Fax = Utils.GetPhoneNo(Me.partyFaxLS.Text)
                .RefNum = Me.BondLS.Text
                '.IsPrimary = 1
                .RefNum = Me.BondLS.Text
                '.MLAgent = Me.chkMLAgentLS.Checked
                If MYITEM.Id > 0 Then
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYITEM)
                Else
                    If (CheckLegalParties(.JobId, "LE") = 0) Then
                        .IsPrimary = 1
                        primarycheck = 1
                    Else
                        .IsPrimary = 0
                    End If
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(MYITEM)
                    If primarycheck = 1 Then
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(qs.GetParameter("ItemId"), MYJOB)
                        MYJOB.LenderId = MYITEM.Id
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(MYJOB)
                    End If

                End If

            End If
        End With



        RaiseEvent ItemSaved()
    End Sub
    Private Function CheckLegalParties(ByVal jobid As String, ByVal TypeCode As String) As Integer
        Dim record As Integer = 0
        Try
            Dim mysql As String = "select * from joblegalparties"
            mysql += " where jobid ='" & jobid & "' "
            mysql += " and typecode='" & TypeCode & "'"
            mysql += " and isprimary='1'"
            Dim myds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetDataSet(mysql)
            If myds.Tables(0).Rows.Count < 1 Then
                record = 0
            Else
                record = 1
            End If
        Catch ex As Exception

        End Try
        Return record

    End Function

    Public Sub CheckGC()

        Try
            Dim mysql As String = "select GenId from job"
            mysql += " where id ='" & Me.ViewState("JobId") & "' "

            Dim myds As System.Data.DataSet = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetDataSet(mysql)
            If myds.Tables(0).Rows.Count > 0 Then
                Dim myview As New HDS.DAL.COMMON.TableView(myds.Tables(0))
                Dim result As String = myview.RowItem("GenId")
                If result = "" Or result = "0" Then
                    GCSearch.Visible = True
                    Me.partyNameGC.ReadOnly = False
                    Me.partyAddLine1GC.ReadOnly = False
                    Me.partyAddLine2GC.ReadOnly = False
                    Me.partyCityGC.ReadOnly = False
                    Me.partyStateGC.ReadOnly = False
                    Me.partyZipGC.ReadOnly = False
                    Me.RefGC.ReadOnly = False
                    Me.partyPhoneGC.ReadOnly = False
                    Me.partyFaxGC.ReadOnly = False
                Else
                    GCSearch.Visible = False
                    Me.partyNameGC.ReadOnly = True
                    Me.partyAddLine1GC.ReadOnly = True
                    Me.partyAddLine2GC.ReadOnly = True
                    Me.partyCityGC.ReadOnly = True
                    Me.partyStateGC.ReadOnly = True
                    Me.partyZipGC.ReadOnly = True
                    Me.RefGC.ReadOnly = True
                    Me.partyPhoneGC.ReadOnly = True
                    Me.partyFaxGC.ReadOnly = True
                End If
            End If
        Catch ex As Exception
            'ex.Message

        End Try
    End Sub

    Protected Sub hdnGC_Click(sender As Object, e As EventArgs)
        CheckGC()
        ScriptManager.RegisterStartupScript(Page, Me.GetType(), "popupLegelparty", "showlegalpopup();", True)
    End Sub
End Class
