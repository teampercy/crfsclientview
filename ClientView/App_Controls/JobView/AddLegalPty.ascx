<%@ Control Language="VB" AutoEventWireup="false" CodeFile="AddLegalPty.ascx.vb" Inherits="App_Controls_crfLienView_AddLegalPty" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<%@ Register Src="~/App_Controls/JobView/GCSearch.ascx" TagPrefix="SiteControls" TagName="GCSearch" %>

<style>
     .btn_OverWrite {
        padding: 6px 15px;
        font-size: 15px;
        line-height: 13px;
        border-radius: 3px;
        -webkit-transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        -o-transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        transition: border .2s linear,color .2s linear,width .2s linear,background-color .2s linear;
        -webkit-font-smoothing: subpixel-antialiased;
    }
     .model-size{
         min-width:600px;
         overflow-x:scroll;
     }
    .modal-open .modal {
        overflow-x: auto !important;
    }
</style>
<script type="text/javascript">
    function SetLegalPartiesTabName(id) {
        // alert(id);
        document.getElementById('<%=hdnTabNameLegalParty.ClientID%>').value = id;
    }

    $('#' + '<%=btnAddParty.ClientID%>').click(function () {
        var type;
        if (document.getElementById("chkPartyGC").checked) { type = 'GC' }
        else if (document.getElementById("chkPartyOwner").checked) { type = 'Owner' }
        else if (document.getElementById("chkPartySuretyLender").checked) { type = 'Surety/Lender' }
        if (rowIndex == -1) {
            tbl.row.add([
                "<a href='#' class='btn btn-sm btn-icon btn-pure btn-default on-default edit-row' " +
            "data-toggle='tooltip' data-original-title='Edit'><i class='icon wb-edit' aria-hidden='true'></i></a>",
            type,
            $("#partyName").val(),
            $("#partyAddLine1").val(),
            $("#partyAddLine2").val(),
            $("#partyCity").val(),
            $("#partyState").val(),
            $("#partyZip").val(),
            $("#partyPhone").val(),
            $("#partyFax").val(),
            $("#partyBondNum").val(),
            "<a href='#' class='btn btn-sm btn-icon btn-pure btn-default on-default remove-row'" +
                "data-toggle='tooltip' data-original-title='Remove'><i class='icon wb-trash' aria-hidden='true'></i></a>"
            ]).draw(false);
            $("#ModalLegalParty").modal('hide');
        }
        else {
            rowData = tbl.row(rowIndex).data();
            rowData[1] = type;
            rowData[2] = $("#partyName").val(),
            rowData[3] = $("#partyAddLine1").val(),
            rowData[4] = $("#partyAddLine2").val(),
            rowData[5] = $("#partyCity").val(),
            rowData[6] = $("#partyState").val(),
            rowData[7] = $("#partyZip").val(),
            rowData[8] = $("#partyPhone").val(),
            rowData[9] = $("#partyFax").val(),
            rowData[10] = $("#partyBondNum").val(),
            tbl.row(rowIndex).data(rowData);
            $("#ModalLegalParty").modal('hide');
        }
        rowIndex = -1;
    });
    function OpenLegalPartyModal() {
        //document.getElementById("chkPartyGC").checked = true;
        //document.getElementById("chkPartyOwner").checked = false;
        //document.getElementById("chkPartySuretyLender").checked = false;
        debugger;
        $('#' + '<%=hdnGC.ClientID%>').click();
        //PageMethods.CheckGC();
        $('#' + '<%=chkMLAgentGC.ClientID%>').prop("checked", false);
        $('#' + '<%=chkMLAgentOw.ClientID%>').prop("checked", false);
       <%-- $('#' + '<%=chkMLAgentLS.ClientID%>').prop("checked", false);--%>
        $('#' + '<%=partyNameGC.ClientID%>').val('');
        $('#' + '<%=RefGC.ClientID%>').val('');
        $('#' + '<%=partyAddLine1GC.ClientID%>').val('');
        $('#' + '<%=partyAddLine2GC.ClientID%>').val('');
        $('#' + '<%=partyCityGC.ClientID%>').val('');
        $('#' + '<%=partyStateGC.ClientID%>').val('');
        $('#' + '<%=partyZipGC.ClientID%>').val('');
        $('#' + '<%=partyPhoneGC.ClientID%>').val('');
        $('#' + '<%=partyFaxGC.ClientId%>').val('');
        $('#' + '<%=partyNameOT.ClientID%>').val('');
        $('#' + '<%=partyAddLine1OW.ClientID%>').val('');
        $('#' + '<%=partyAddLine2OW.ClientID%>').val('');
        $('#' + '<%=partyCityOW.ClientID%>').val('');
        $('#' + '<%=partyStateOW.ClientID%>').val('');
        $('#' + '<%=partyZipOW.ClientID%>').val('');
        $('#' + '<%=partyPhoneOW.ClientID%>').val('');
        $('#' + '<%=partyFaxOW.ClientID%>').val('');
        $('#' + '<%=partyNameSL.ClientID%>').val('');
        $('#' + '<%=partyAddLine1LS.ClientId%>').val('');
        $('#' + '<%=partyAddLine2LS.ClientID%>').val('');
        $('#' + '<%=partyCityLS.ClientID%>').val('');
        $('#' + '<%=partyStateLS.ClientID%>').val('');
        $('#' + '<%=partyZipLS.ClientID%>').val('');
        $('#' + '<%=partyPhoneLS.ClientID%>').val('');
        $('#' + '<%=partyFaxLS.ClientID%>').val('');
        $('#' + '<%=BondLS.ClientID%>').val('');
        if (!$('#' + '<%=liGneralContractor.ClientID%>').hasClass('active')) {
            $('#' + '<%=liGneralContractor.ClientID%>').addClass('active');
        }
        if (!$('#TabsliGneralContractor').hasClass('active'))
            $('#TabsliGneralContractor').addClass('active');
        $('#' + '<%=liOwnerTenant.ClientID%>').removeClass("active");
        $('#TabsliOwnerTenant').removeClass("active");
        $('#' + '<%=liSuretyLender.ClientID%>').removeClass("active");
        $('#TabsliSuretyLender').removeClass("active");
        document.getElementById('<%=hdnTabNameLegalParty.ClientID%>').value = "liGneralContractor";
           
    }
    function showlegalpopup()
    {
        
        $("#ModalLegalParty").modal('show');
    }

    function GetGCNameList() {
        debugger;
        var txtGC = document.getElementById("txtGCSearchText").value;
        if (txtGC != "") {
            var ClientId = document.getElementById('<%=GCSearchClientID.ClientID%>').value;
            PageMethods.GetGCNames(txtGC, ClientId, LoadGCNames);
        }
        else {
            $('#listGCNames option').remove();
        }
        return false;
    }
    function LoadGCNames(result) {
        debugger;
        document.getElementById("listGCNames").style.display = 'block';
        $('#listGCNames option').remove();
        if (result.length > 0) {
            var ele = document.getElementById("listGCNames");
            $('#listGCNames option').remove();

            for (var i = 0; i < result.length; i++) {
                var opt = document.createElement('option');
                opt.text = result[i].Text;
                opt.value = result[i].Value;
                ele.appendChild(opt);
            }
        }
    }
    function GetGCDetails(cntrl) {
        //var GCString = cntrl.value;
        var GCString = document.getElementById("listGCNames").value;
        // alert('hi');
        // alert(GCString);
        if (GCString != "") {

            var clientId = document.getElementById('<%=GCSearchClientID.ClientID%>').value;
            //alert(clientId);
            PageMethods.GetGCDetails(GCString, clientId, LoadGCDetails);
        }
    }

   function LoadGCDetails(result) {
        //  alert('hey');
        var cntrl = document.getElementById("listGCNames").value;
        //  alert(cntrl);
        //debugger;
        if (cntrl.substring(0, 1) == "B") {
            document.getElementById('<%=partyNameGC.ClientID%>').value = result.GCName;
            if ($('#<%=RefGC.ClientID%>').length > 0) {
                document.getElementById('<%=RefGC.ClientID%>').value = result.GCRefNum;
            }
            document.getElementById('<%=partyAddLine1GC.ClientID%>').value = result.GCAdd1;
            document.getElementById('<%=partyAddLine2GC.ClientID%>').value = result.GCAdd2;
            document.getElementById('<%=partyCityGC.ClientID%>').value = result.GCCity;
            document.getElementById('<%=partyZipGC.ClientID%>').value = result.GCZip;
            document.getElementById('<%=partyStateGC.ClientID%>').value = result.GCState;
            if(result.GCPhone){
                document.getElementById('<%=partyPhoneGC.ClientID%>').value =  result.GCPhone
            }
            document.getElementById('<%=partyFaxGC.ClientID%>').value = result.GCFax;
        }
        else {
            document.getElementById('<%=partyNameGC.ClientID%>').value = result.GeneralContractor;
            if ($('#<%=RefGC.ClientID%>').length > 0) {
                document.getElementById('<%=RefGC.ClientID%>').value = result.RefNum;
            }
            document.getElementById('<%=partyAddLine1GC.ClientID%>').value = result.AddressLine1;
            document.getElementById('<%=partyAddLine2GC.ClientID%>').value = result.AddressLine2;
            document.getElementById('<%=partyCityGC.ClientID%>').value = result.City;
            document.getElementById('<%=partyZipGC.ClientID%>').value = result.PostalCode;
            document.getElementById('<%=partyStateGC.ClientID%>').value = result.State;
            document.getElementById('<%=partyPhoneGC.ClientID%>').value = result.Telephone1;
            document.getElementById('<%=partyFaxGC.ClientID%>').value = result.Telephone2;
        }
     
        $("#ModalGCSearch").modal('hide');
    }

</script>
<div style="margin: 2px 0px 5px 0px;">
     <asp:Button ID="hdnGC" runat="server" OnClick ="hdnGC_Click" Style="display: none;" />
    <asp:Button ID="btnAddNew" CssClass="btn btn-primary" runat="server" Text="Add Legal Party" Style="display: none;" />
</div>
<asp:HiddenField runat="server" ID="hdnTabNameLegalParty" Value="liGneralContractor" />
<div class="modal fade  modal-primary" id="ModalLegalParty" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;">
    <div class="modal-dialog modal-center">
        <div class="modal-content model-size" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <asp:HiddenField runat="server" ID="hddnRowIndex" />
                <h4 class="modal-title">ADDITIONAL LEGAL PARTY</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li id="liGneralContractor" class="active" role="presentation" runat="server"><a data-toggle="tab" onclick="SetLegalPartiesTabName('liGneralContractor');" href="#TabsliGneralContractor" aria-controls="TabsliGneralContractor" role="tab" aria-expanded="false">GC/Contractor</a></li>
                            <li id="liOwnerTenant" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetLegalPartiesTabName('liOwnerTenant');" href="#TabsliOwnerTenant" aria-controls="TabsliOwnerTenant" role="tab" aria-expanded="false">Owner/Tenant</a></li>
                            <li id="liSuretyLender" role="presentation" runat="server"><a data-toggle="tab" onclick="SetLegalPartiesTabName('liSuretyLender');" href="#TabsliSuretyLender" aria-controls="TabsliSuretyLender" role="tab" aria-expanded="false">Surety/Lender</a></li>

                            <%--   <li id="NoticeRequest" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('NoticeRequest');" href="#exampleTabsEight" aria-controls="exampleTabsEight" role="tab" aria-expanded="false">Notice Request</a></li>
                            <li id="TXRequests" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('TXRequests');" href="#exampleTabsNine" aria-controls="exampleTabsNine" role="tab" aria-expanded="false">TX Requests</a></li>--%>
                        </ul>
                        <div class="tab-content padding-top-20" style="border: 1px solid lightgray;">
                            <div class="tab-pane active" id="TabsliGneralContractor" role="tabpanel">
                                <div class="form-group" style="display:none">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label"></label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <div class="checkbox-custom checkbox-default" style="min-height: 20px; font-size: 12px;">
                                            <asp:CheckBox ID="chkMLAgentGC" runat="server" Text="Show as ML Agent (If Owner checked)"  TabIndex="4" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Name:</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyNameGC" CssClass="form-control" TabIndex="5"></asp:TextBox>
                                    </div>
                                     <div id="Div1" class="col-md-2 col-sm-2 col-xs-2" >
                                         <SiteControls:GCSearch runat="server" ID="GCSearch" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Ref #:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="RefGC" Width="135px" TabIndex="6" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Address:</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8 ">
                                        <asp:TextBox runat="server" ID="partyAddLine1GC" CssClass="form-control" TabIndex="7"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label"></label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyAddLine2GC" CssClass="form-control" TabIndex="8"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">City/State/Zip:</label>

                                    <div class="col-md-10 col-sm-10 col-xs-10" style="display: flex;">
                                        <asp:TextBox runat="server" ID="partyCityGC" CssClass="form-control" Width="160px" TabIndex="9"></asp:TextBox>&nbsp;
                        <asp:TextBox runat="server" ID="partyStateGC" CssClass="form-control" Width="65px" TabIndex="10"></asp:TextBox>&nbsp;
                            <asp:TextBox runat="server" ID="partyZipGC" CssClass="form-control" Width="130px" TabIndex="11"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Phone#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="partyPhoneGC" Width="135px" TabIndex="12" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Fax#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="partyFaxGC" Width="135px" TabIndex="13" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="TabsliOwnerTenant" role="tabpanel">
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label"></label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <div class="checkbox-custom checkbox-default" style="min-height: 20px; font-size: 12px;">
                                            <asp:CheckBox ID="chkMLAgentOw" runat="server" Text="Show as ML Agent" TabIndex="4" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Name:</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyNameOT" CssClass="form-control" TabIndex="5"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Address:</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyAddLine1OW" CssClass="form-control" TabIndex="6"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label"></label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyAddLine2OW" CssClass="form-control" TabIndex="7"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">City/State/Zip:</label>

                                    <div class="col-md-10 col-sm-10 col-xs-10" style="display: flex;">
                                        <asp:TextBox runat="server" ID="partyCityOW" CssClass="form-control" Width="160px" TabIndex="8"></asp:TextBox>&nbsp;
                        <asp:TextBox runat="server" ID="partyStateOW" CssClass="form-control" Width="65px" TabIndex="9"></asp:TextBox>&nbsp;
                            <asp:TextBox runat="server" ID="partyZipOW" CssClass="form-control" Width="130px" TabIndex="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Phone#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="partyPhoneOW" Width="135px" TabIndex="11" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Fax#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="partyFaxOW" Width="135px" TabIndex="12" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="TabsliSuretyLender" role="tabpanel">
                           <%--     <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label"></label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <div class="checkbox-custom checkbox-default" style="min-height: 20px; font-size: 12px;">
                                            <asp:CheckBox ID="chkMLAgentLS" runat="server" Text="Show as ML Agent (If Owner checked)" TabIndex="4" />
                                        </div>
                                    </div>
                                </div>--%>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Name:</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyNameSL" CssClass="form-control" TabIndex="5"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Address:</label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyAddLine1LS" CssClass="form-control" TabIndex="6"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label"></label>
                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                        <asp:TextBox runat="server" ID="partyAddLine2LS" CssClass="form-control" TabIndex="7"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">City/State/Zip:</label>

                                    <div class="col-md-10 col-sm-10 col-xs-10 col-sm-10 col-xs-10" style="display: flex;">
                                        <asp:TextBox runat="server" ID="partyCityLS" CssClass="form-control" Width="160px" TabIndex="8"></asp:TextBox>&nbsp;
                        <asp:TextBox runat="server" ID="partyStateLS" CssClass="form-control" Width="65px" TabIndex="9"></asp:TextBox>&nbsp;
                            <asp:TextBox runat="server" ID="partyZipLS" CssClass="form-control" Width="130px" TabIndex="10"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Phone#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="partyPhoneLS" Width="135px" TabIndex="11" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Fax#:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="partyFaxLS" Width="135px" TabIndex="12" CssClass="form-control" data-plugin="formatter" data-pattern="[[999]]-[[999]]-[[9999]]"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 col-sm-2 col-xs-2 control-label">Bond #:</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <asp:TextBox runat="server" ID="BondLS" Width="135px" TabIndex="13" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>
            <div class="modal-footer">
                <%--<button type="button" class="btn btn-primary margin-0" id="btnAddParty" >Add</button>--%>
                <asp:Button runat="server" ID="btnAddParty" CssClass="btn btn-primary margin-0" Text="Save" TabIndex="14" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" tabindex="15">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:Panel ID="panDialog" runat="server" Style="display: none"
    Width="575px" Font-Bold="True">
    <div id="modcontainer" style="margin: 0px  0px 0px 0px; width: 575px;">
        <h1 class="panelheader">Additional Legal Party</h1>
        <div class="body">
            <asp:LinkButton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " />
            <table id="Table3" cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td class="row-label" style="width: 75px;">Type</td>
                    <td class="row-data" width="475">
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal" Width="256px" AutoPostBack="False">
                            <asp:ListItem Value="GC"> GC</asp:ListItem>
                            <asp:ListItem Value="OW"> Owner</asp:ListItem>
                            <asp:ListItem Value="LE"> Surety/Lender</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px; height: 24px;">Name:</td>
                    <td class="row-data" width="475" style="height: 24px">
                        <cc1:DataEntryBox ID="LPName" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="11" tag="Name" Width="224px"></cc1:DataEntryBox>&nbsp;
                    <asp:CheckBox ID="MLAGENT" runat="server" Text="  Show as ML Agent (If Owner checked)" />
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">Address:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPAdd1" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="13" tag="Address" Width="224px"></cc1:DataEntryBox>

                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;"></td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPAdd2" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="14" tag="Address2" Width="224px"></cc1:DataEntryBox>

                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px; height: 24px;">City/State/Zip:</td>
                    <td class="row-data" width="475" style="height: 24px">
                        <cc1:DataEntryBox ID="LPCity" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="15" Width="104px" tag="City"></cc1:DataEntryBox>
                        <cc1:DataEntryBox ID="LPState" runat="server" CssClass="textbox" IsRequired="False" TabIndex="16" tag="State" Width="32px"></cc1:DataEntryBox>
                        <cc1:DataEntryBox ID="LPZip" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="17" Width="78px" tag="Zip"></cc1:DataEntryBox>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">Phone#:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPPhone" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="18" Tag="Legal Party Phone" Width="80px"></cc1:DataEntryBox>
                        <ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender4" runat="server" AcceptNegative="None"
                            ClearMaskOnLostFocus="False" DisplayMoney="None" InputDirection="LeftToRight"
                            Mask="999-999-9999" MaskType="NUMBER" MessageValidatorTip="true" OnFocusCssClass="focus"
                            OnInvalidCssClass="error" TargetControlID="LPPhone"></ajaxToolkit:MaskedEditExtender>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">Fax#:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="LPFax" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="19" Tag="Legal Party Fax" Width="80px"></cc1:DataEntryBox><ajaxToolkit:MaskedEditExtender ID="MaskedEditExtender1" runat="server" AcceptNegative="None"
                                ClearMaskOnLostFocus="False" DisplayMoney="None" InputDirection="LeftToRight"
                                Mask="999-999-9999" MaskType="NUMBER" MessageValidatorTip="true" OnFocusCssClass="focus"
                                OnInvalidCssClass="error" TargetControlID="LPFax"></ajaxToolkit:MaskedEditExtender>
                    </td>
                </tr>
                <tr>
                    <td class="row-label" style="width: 75px;">BondNum:</td>
                    <td class="row-data" width="475">
                        <cc1:DataEntryBox ID="RefNum" runat="server" CssClass="textbox" IsRequired="False"
                            TabIndex="20" Tag="Address" Width="250px"></cc1:DataEntryBox></td>
                </tr>
            </table>
        </div>
         <div class="form-group">

                                <div class="col-md-12">
                                    <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                    <asp:Label ID="Message1" runat="server" CssClass="normalred"></asp:Label>
                                </div>
                            </div>
        <div class="footer">
            <asp:LinkButton ID="btnSave" CssClass="button" runat="server" Text="Save" />&nbsp;&nbsp;
            <br />
            <asp:HiddenField ID="BatchJobId" runat="server" />
            <asp:HiddenField ID="ItemId" runat="server" />
        </div>
    </div>
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="" OkControlID="" DropShadow="true"
    PopupControlID="panDialog" TargetControlID="btnAddNew">
</ajaxToolkit:ModalPopupExtender>
<div class="modal fade  modal-primary" id="ModalGCSearch" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
                <asp:HiddenField runat="server" ID="GCSearchClientID" />
                <h4 class="modal-title">GENERAL CONTRACTOR SEARCH</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Enter GC Name: </label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                                  <input type="text" id="txtGCSearchText" class="form-control" onkeyup="GetGCNameList()" />

                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <button id="btnGCSearch" onclick="GetGCNameList();return false;" class="btn btn-primary" value="Search">Search</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <select size="4" id="listGCNames" style="display: none;" onchange="GetGCDetails();" class="form-control"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                <%-- <a class="btn btn-default margin-0" data-dismiss="modal">&times;</a>--%>
            </div>
        </div>
    </div>
</div>