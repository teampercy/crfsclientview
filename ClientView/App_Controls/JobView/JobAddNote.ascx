<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobAddNote.ascx.vb" Inherits="App_Controls_LienView_JobAddNote" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script type="text/javascript">
    function OpenJobAddNoteModal() {
        $("#ModalJobAddNote").modal('show');
    }
    function ModalHide() {
        // alert('hello');

        // $("#btnclose").click();
        document.getElementById('<%=CancelButton.ClientID%>').click();
        DisplayTablelayout();
        ActivateTab('Notes');
        $("#ModalJobAddNote").modal('hide');
        ActivateTabSet();
    }
    function ModalSave() {
        document.getElementById('<%=btnSave.ClientID%>').click();
        $("#ModalJobAddNote").modal('hide');
    }
    function ShowReviewModel() {
        swal({
            title: "Job Review",
            text: "Is your job review completed?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No',
            closeOnConfirm: true
        },
 function (isConfirm) {
     if (isConfirm) {
         document.getElementById('<%=btnReview.ClientID%>').click();
      }
      else {
          document.getElementById('<%=btnReviewCancel.ClientID%>').click();
      }
  });
    return false;
}




</script>
<%--<asp:LinkButton ID="btnAddNew" CssClass="button" runat="server" Text="Add New Note"  />--%>
<asp:Button ID="btnAddNew" CssClass="button" runat="server" Text="Add New Note" Style="display: none;" />
<input type="button" id="btnJobNew" onclick="OpenJobAddNoteModal();" value="Add New Note" class="btn btn-primary" />


<div class="modal fade modal-primary" id="ModalJobAddNote" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose" onclick="ModalHide();">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title">Add Note to File</h4>
                <asp:HiddenField ID="BatchJobId" runat="server" />
                <asp:HiddenField ID="ItemId" runat="server" />
                <asp:HiddenField ID="hdnIsReviewStatus" runat="server" />
                <asp:HiddenField ID="hdnIsNotifyStatus" runat="server" />
            </div>
            <div class="modal-body">
                <%--  <div id="modcontainer" style="MARGIN: 0px  0px 0px 0px; width: 100%;">--%>

                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <asp:TextBox ID="Note" runat="server" Height="100px" TextMode="MultiLine" Width="100%" CssClass="textboxnote"></asp:TextBox>
                                <asp:Button ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " CausesValidation="False" Style="display: none;" />
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                <asp:Label ID="Message" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>

                </div>

                <%-- </div>--%>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" Style="display: none;" />
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaves" onclick="ModalSave();">
                    SAVE
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="ModalHide();">Close</button>
                <asp:Button ID="btnReview" CssClass="btn btn-primary" runat="server" Text="" Style="display: none;" />
                <asp:Button ID="btnReviewCancel" CssClass="btn btn-primary" runat="server" Text="" Style="display: none;" />
            </div>
        </div>
    </div>
</div>
<asp:Panel ID="panCustNameSuggest" runat="server" Style="display: none"
    Width="400px" Font-Bold="True">
</asp:Panel>
<ajaxToolkit:ModalPopupExtender ID="ModalPopupExtender1" runat="server" BackgroundCssClass="modalBackground"
    CancelControlID="" OkControlID="" DropShadow="true"
    PopupControlID="panCustNameSuggest" TargetControlID="btnAddNew">
</ajaxToolkit:ModalPopupExtender>
