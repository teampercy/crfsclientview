﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobViewInfo.ascx.vb" Inherits="App_Controls_JobView_JobViewInfo" %>

<%@ Register Src="~/App_Controls/JobView/JobAddNote.ascx" TagName="JobAddNote" TagPrefix="uc7" %>
<%@ Register Src="~/App_Controls/JobView/JobEdit.ascx" TagName="JobEdit" TagPrefix="uc6" %>
<%@ Register Src="~/App_Controls/JobView/FileUpload.ascx" TagName="uploadFile" TagPrefix="uc7" %>
<%@ Register Src="~/App_Controls/JobView/AddLegalPty.ascx" TagName="AddLegalPty" TagPrefix="uc3" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc2" %>

<style>
    .model-size {
        min-width: 600px;
        overflow-x: scroll;
    }

    .modal-open .modal {
        overflow-x: auto !important;
    }

    .TabBorder {
        border-color: #3a6dae;
    }

    .PaddingleftRemove {
        padding-left: 0px;
    }

    .DisplayNone {
        display: none;
    }

    .HeaderInvoiceGrid {
        text-align: center;
        font-weight: bold;
    }

    .HeaderStyleInvoiceGrid {
        text-align: center;
    }

    .deadlinecolor {
        color: red !important;
    }

    .loading-image {
        position: absolute;
        top: 50%;
        left: 50%;
        margin: -50px 0px 0px -50px;
    }
</style>
<script language="javascript" type="text/javascript">

    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
    function EndRequestHandler(sender, args) {
        if (args.get_error() != undefined) {
            args.set_errorHandled(true);
        }
    }
    function setLoadingImage() {
        //debugger;
       // alert('setLoadingImage');
        console.log("hi");
        var ht = screen.height + 50;
        ht = 540;
        var wt = screen.width + 50;
        wt = 550;
        var imgmarTop = (ht / 2) - 100;

        var imgmarLeft = (wt / 2) - 100;

        var lyrStl = "width: " + wt + "px; height:" + ht + "px; background-color: rgba(255, 255, 255, 0); position: fixed; z-index: 10051 !important; margin: 0px 0px 0px 0px;top:410px;left:470px;padding-left: 0px !important;";

        var img = '<img src="../../../images/LoadingImage.gif" style=" margin-left:' + imgmarLeft + 'px; margin-top:' + imgmarTop + 'px;  z-index: 10052 !important;">';
        //var img = '<img src="../../../images/LoadingImage.gif" style="z-index: 10052 !important;">';
        console.log(img);
        var div = '<div style="' + lyrStl + '">';

        div = div + img;

        div = div + '<div>';
        console.log(div);
        $('#loadingImageDiv').html(div);
       // alert('setLoadingImage1');
        $('#loadingImageDiv').show();

    }
    function showLodingImage() {
        //alert('showLodingImage');
        debugger;
        $('#loadingImageDiv').show();
    } // 5 seconds }

    function hideLodingImage() {
        // alert('hideLodingImage');
        // debugger;             
        var tmr = setInterval(function () { zzzhideLodingImageActul(tmr) }, 1000);
        //window.opener.testjava();

    }

    function zzzhideLodingImageActul(tmr) { $('#loadingImageDiv').hide(); clearInterval(tmr); }



    //'^^^***CreatedBy Pooja   01/09/20 *** ^^^
        function DeleteAccount(id) {
            debugger;
            document.getElementById('<%=hdnDocId.ClientID%>').value = $(id).attr("rowno");
            
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: false
        },
      function () {
          document.getElementById('<%=hddbTabName.ClientID%>').value = "Docs";
          document.getElementById('<%=btnDeleteDoc.ClientID%>').click();
      });
        return false;
        }

    //'^^^***CreatedBy Pooja   10/13/20 *** ^^^
    function Clear() {
        debugger;
      
        $("#iframe1").attr("src", "App_Controls/_Custom/LienView/Default.aspx?id=1 &jobid=1");
        //setSelectedDocTypeId('--SELECT--');
        $("#ModalFileUpload").modal('hide');
       
    }
    //'^^^***   *** ^^^

    function hideDiveNotes() {
        //alert("hi");
        $('#divnotes').hide();
        $('#divAddDoc').hide();
        $('#divWaiver').hide();
        $('#divStateForm').hide();
        $('#divmergejob').hide();
        disableRequestNotice();
        // ctl33_ctl00_gvwDocs.Columns("ID").ColumnMapping = MappingType.Hidden

    }
    function DisplayDocPopup(obj) {
        debugger;
        var DocId = obj.getAttribute("rowno")
        $('#' + '<%=hdnDocId.ClientID%>').val(DocId);
        $("#ModalDocsView").modal('show');
        return false;
    }
    function HideDocPopup() {
        $("#ModalDocsView").modal('hide');
    }

    function RemoveBackdrop() {
        $('div.modal-backdrop').remove()
    }


    function DisplayEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('show');
    }
    function HideEmailPopUp() {
        // alert('hi');
        $("#divModalEmail").modal('hide');
    }

    function SendMail() {
        if (document.getElementById('<%=txtToList.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the 'To' EmailId.";
            return false;
        }
        if (document.getElementById('<%=txtSubject.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the Subject.";
            return false;
        }
        document.getElementById('<%=btnEmailsend.ClientID%>').click();


        HideEmailPopUp();

        return false;
    }
    function SendServiceRequestMail() {
        debugger;
        if (document.getElementById('<%=txtServiceType.ClientID%>').value == "") {
            document.getElementById('<%=lblreqerror.ClientID%>').innerText = "Please enter the Service Type.";
            return false;
        }
        if (document.getElementById('<%=txtAmountOwed.ClientID%>').value == "") {
            document.getElementById('<%=lblreqerror.ClientID%>').innerText = "Please enter Amount Owed.";
            return false;
        }
        document.getElementById('<%=btnEmailsend.ClientID%>').click();


        HideRequestServicesPopup();

        return false;
    }

    function EmailSendPopup(textdata) {
        debugger;
        swal({
            title: "Successfully Sent!",
            text: textdata,
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false

        });

    }

    function Cancelpopup() {
        location.href = currURL;
        return false;
    }

    function Showloading() {

        document.getElementById('loading').style.visibility = 'visible';
    }

    function Hideloading() {
        document.getElementById('loading').style.visibility = "hidden";
    }


    function disableRequestNotice() {

        document.getElementById('<%=btnRequestNotice.ClientID %>').disabled = true;
    }

    function enableRequestNotice() {
        document.getElementById('<%=btnRequestNotice.ClientID %>').disabled = false;
    }

    function disableAdditionalInfoBtn() {

        document.getElementById('<%=btnAdditionalInfo.ClientID %>').disabled = true;
    }

    function enableAdditionalInfoBtn() {
        document.getElementById('<%=btnAdditionalInfo.ClientID %>').disabled = false;
    }

      


    function disableAdditionalServiceRequest() {

        document.getElementById("btnRequestAdditionalServices").disabled = true;
    }


    function modalsuccessShow(assign) {
        //alert('modal');
        if (assign == true) {
            $('#BeforeAssign').hide();
        }
        else if (assign == false) {
            $('#AfterAssign').hide();
        }

        $("#ModelRequestverification").modal('show');
        return false;
    }
    ///////////////////////////////////////////////////////////
    function ShowServiceTypeModal() {
        $("#ModalServiceType").modal('show');
    }
    function BindServiceTypeDropdown() {
        debugger;
        //Added by Jaywanti on 25-07-2016
        $('#UlServiceType li').remove();

<%--        if ($('#' + '<%=hdnStateSelectedValue.ClientID%>').val() == "TX") {
            //alphabetize the drop down 
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job Data As Provided - No Notice Sent\')">Store Job Data As Provided - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerTX" onclick="setSelectedSserviceType(6,\'Verify Owner Only TX\')">Verify Owner Only TX</a></li>');

        }
        else {--%>
        //alphabetize the drop down 
        //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(2,\'Send Notice With Data Provided\')">Send Notice With Data Provided</a></li>');
        //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job Data As Provided - No Notice Sent\')">Store Job Data As Provided - No Notice Sent</a></li>');
        $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(2,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
        $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

        //Commented as per mail 02/05/20 start 
        //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
        //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(5,\'Send Notice With Data Provided\')">Send Notice With Data Provided</a></li>');
        //Commented as per mail 02/05/20 end

        $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerSend" onclick="setSelectedSserviceType(6,\'Verify Owner Send\')">Verify Owner Send</a></li>');
        $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(7,\'Verify Owner Only\')">Verify Owner Only</a></li>');
        $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liSendNoticewithDataProvided" onclick="setSelectedSserviceType(11,\'Send Notice with Data Provided\')">Send Notice with Data Provided</a></li>');


        // $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerSend" onclick="setSelectedSserviceType(7,\'Verify Owner Only - Send Notice \')">Verify Owner Only - Send Notice</a></li>');
        //  $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(5,\'Verify Owner Only - No Notice Sent\')">Verify Owner Only - No Notice Sent</a></li>');

<%--        if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "1") {
            $('#btnServiceType').html($('#liPrelimBox').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "2") {
            $('#btnServiceType').html($('#liPrelimASIS').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "3") {
            $('#btnServiceType').html($('#liVerifyJob').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "4") {
            $('#btnServiceType').html($('#liVerifyJobASIS').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "5") {
            $('#btnServiceType').html($('#liVerifyOwnerOnly').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "6") {
            $('#btnServiceType').html($('#liVerifyOwnerTX').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "7") {
            $('#btnServiceType').html($('#liVerifyOwnerSend').text() + "<span class='caret' ></span>");
        }--%>
        if ($('#' + '<%=hdnJobState.ClientID%>').val() == "TX" || $('#' + '<%=hdnJobState.ClientID%>').val() == "TN") {
            $('#btnServiceType').html('Verify Job Data Only - No Notice Sent' + "<span class='caret' ></span>");
            var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                x.value = '2';
            }
            else {
                $('#btnServiceType').html('Verify Job Data - Send Notice' + "<span class='caret' ></span>");
                var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                x.value = '1';
            }

            return false;
    }

    function BindServiceTypeDropdown1() {
        //alert('BindServiceTypeDropdown1call');
        debugger;
        //Added by Jaywanti on 25-07-2016
        $('#UlServiceType li').remove();

<%--        if ($('#' + '<%=hdnStateSelectedValue.ClientID%>').val() == "TX") {
            //alphabetize the drop down 
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job Data As Provided - No Notice Sent\')">Store Job Data As Provided - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerTX" onclick="setSelectedSserviceType(6,\'Verify Owner Only TX\')">Verify Owner Only TX</a></li>');

        }
        else {--%>
          //alphabetize the drop down 
          //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(2,\'Send Notice With Data Provided\')">Send Notice With Data Provided</a></li>');
          //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job Data As Provided - No Notice Sent\')">Store Job Data As Provided - No Notice Sent</a></li>');
          $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(2,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
          $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

          //Commented as per mail 02/05/20 start 
          //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
          //$('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(5,\'Send Notice With Data Provided\')">Send Notice With Data Provided</a></li>');
          //Commented as per mail 02/05/20 end

          $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerSend" onclick="setSelectedSserviceType(6,\'Verify Owner Send\')">Verify Owner Send</a></li>');
          $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(7,\'Verify Owner Only\')">Verify Owner Only</a></li>');
        $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(11,\'Send Notice with Data Provided \')">Send Notice with Data Provided </a></li>');


        // $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerSend" onclick="setSelectedSserviceType(7,\'Verify Owner Only - Send Notice \')">Verify Owner Only - Send Notice</a></li>');
        //  $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(5,\'Verify Owner Only - No Notice Sent\')">Verify Owner Only - No Notice Sent</a></li>');

<%--        if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "1") {
            $('#btnServiceType').html($('#liPrelimBox').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "2") {
            $('#btnServiceType').html($('#liPrelimASIS').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "3") {
            $('#btnServiceType').html($('#liVerifyJob').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "4") {
            $('#btnServiceType').html($('#liVerifyJobASIS').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "5") {
            $('#btnServiceType').html($('#liVerifyOwnerOnly').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "6") {
            $('#btnServiceType').html($('#liVerifyOwnerTX').text() + "<span class='caret' ></span>");
        }
        else if ($('#' + '<%=hdnServiceTypeId.ClientID%>').val() == "7") {
            $('#btnServiceType').html($('#liVerifyOwnerSend').text() + "<span class='caret' ></span>");
        }--%>
          if ($('#' + '<%=hdnJobState.ClientID%>').val() == "TX" || $('#' + '<%=hdnJobState.ClientID%>').val() == "TN") {
            $('#btnServiceType').html('Verify Job Data Only - No Notice Sent' + "<span class='caret' ></span>");
            var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                x.value = '2';
            }
            else {
                $('#btnServiceType').html('Verify Job Data - Send Notice' + "<span class='caret' ></span>");
            var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
            x.value = '1';
        }

        return false;
    }


    function setSelectedSserviceType(id, TextData) {
            debugger;
            //$('#' + '<%=hdnServiceTypeId.ClientID%>').val(id);
            $('#btnServiceType').html(TextData + "<span class='caret' ></span>");
            var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
        x.value = '1';
        if (TextData == "Verify Job Data Only - No Notice Sent") {
            x.value = '2';
        }
        else if (TextData == "Verify Job Data - Send Notice") {
            x.value = '1';
        }
        else if (TextData == "Store Job With Data Provided") {
            x.value = '3';
        }
        else if (TextData == "Send Notice With Data Provided") {
            x.value = '4';
        }
        else if (TextData == "Verify Owner Send") {
            x.value = '6';
        }
        else if (TextData == "Verify Owner Only") {
            x.value = '5';
        }
        else if (TextData == "Send Notice with Data Provided") {
            x.value = '11';
        }
        else {
            x.value = '0';
        }
 
        return false;
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    function setSelectedSserviceTypeTN(id, TextData) {
        debugger;
        //$('#' + '<%=hdnServiceTypeId.ClientID%>').val(id);
        $('#btnServiceTypeTN').html(TextData + "<span class='caret' ></span>");
        var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
        x.value = '1';
        if (TextData == "Verify Job Data Only - No Notice Sent") {
            x.value = '1';
        }
        else if (TextData == "Store Job With Data Provided") {
            x.value = '4';
        }
        else if (TextData == "Verify Owner Only") {
            x.value = '8';
        }
        else {
            x.value = '0';
        }

        return false;
    }

    function setSelectedSserviceTypeTX(id, TextData) {
        debugger;
        //$('#' + '<%=hdnServiceTypeId.ClientID%>').val(id);
        $('#btnServiceTypeTX').html(TextData + "<span class='caret' ></span>");
        var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
        x.value = '1';
        if (TextData == "Verify Job Data Only - No Notice Sent") {
            x.value = '1';
        }
        else if (TextData == "Store Job With Data Provided") {
            x.value = '4';
        }
        else if (TextData == "Verify Owner TX") {
            x.value = '5';
        }
        else {
            x.value = '0';
        }

        return false;
    }
    function BindDropDownForTX() {
        debugger;
        $('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceTypeTX(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
        //$('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceTypeTX(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

        $('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceTypeTX(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
        $('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerTX" onclick="setSelectedSserviceTypeTX(5,\'Verify Owner TX\')">Verify Owner TX</a></li>');

        $('#btnServiceTypeTX').html('Verify Job Data Only - No Notice Sent' + "<span class='caret' ></span>");
        var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
        x.value = '1';
    }
    ////////////////////

    function modalsuccessHideServiceType() {
        //alert('modal');
        $("#ModalServiceType").modal('hide');
        $('div.modal-backdrop').remove()
        return false;
    }
    function ChangeHdnValue() {
        var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
       x.value = '123';
       return true;
   }
   //////////////////////////////////////////////////////////
   function modalsuccessHide() {
       //alert('modal');
       $("#ModelRequestverification").modal('hide');
       $('div.modal-backdrop').remove()
       return false;
   }
   function PopupMergeJob() {

       $("#ModalJobMerge").modal('show');
       return false;
   }
   var resultMerge = false;
   function MergeJob() {
       debugger;
       if (resultMerge == true) {
            <%--var txtMergeJobId = $('#txtMergeJobId').val();          
            if (txtMergeJobId.trim() == "") {
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please fill the Enter Job#.";
                return false;
            }--%>
            var txtMergeJobId = $('#ModalJobMerge').find('input[name="txtMergeJobId"]').val();
            //  alert(txtMergeJobId);
            if (txtMergeJobId == "") {
                document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please fill the Enter Job#.";
                return false;
            }
            var CurrentJobId = "0";
            CurrentJobId = '<%= Session("jobid") %>';
            $.ajax({
                type: "POST",
                url: "MainDefault.aspx/SaveMergeJob",
                data: '{CurrentJobId: "' + CurrentJobId + '",MergeJobId:"' + txtMergeJobId + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.d == "Success") {

                        $('#ModalJobMerge').hide();
                        swal({
                            title: "Successfully Merge!",
                            text: "Your job has been merged.",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: '#DD6B55',
                            cancelButtonText: 'cancelOK',
                            confirmButtonText: 'OK',
                            closeOnConfirm: true
                        },

                      function () {
                          //alert("hi");
                          $('.modal-backdrop').remove();
                          document.getElementById('<%=btnMergeJob.ClientID%>').click();
                      });
                    }
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }
        else {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please first find the Job by Job#.";
        }
        return false;
    }
    function FindMergeJob() {
        // alert("hi");
        resultMerge = false;
        debugger;
        //var txtMergeJobIdVal = $('#txtMergeJobId').val();
        var txtMergeJobIdVal = $('#ModalJobMerge').find('input[name="txtMergeJobId"]').val();
        //  alert(txtMergeJobIdVal);
        <%--  if (txtMergeJobId.trim() == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please fill the Enter Job#.";
            return false;
        }--%>
        if (txtMergeJobIdVal == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please fill the Enter Job#.";
            return false;
        }
        var CurrentJobId = "0";
        CurrentJobId = '<%= Session("jobid") %>';
        $.ajax({
            type: "POST",
            url: "MainDefault.aspx/FindMergeJob",
            data: '{CurrentJobId: "' + CurrentJobId + '",MergeJobId:"' + txtMergeJobIdVal + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (response) {
                if (response.d != "") {
                    var data = [];
                    var result = "";
                    result = response.d;
                    data = result.split("~");
                    if (data.length > 1) {
                        document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "";
                        document.getElementById('<%=lblJobmergeName.ClientID%>').innerText = data[0];
                        document.getElementById('<%=lblJobmergeAddress.ClientID%>').innerText = data[1];
                        document.getElementById('<%=lblJobMergeCustNum.ClientID%>').innerText = data[2];
                        document.getElementById('<%=lblJobmergeCustName.ClientID%>').innerText = data[3];
                        resultMerge = true;
                    }
                    else {
                        document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = response.d;
                        document.getElementById('<%=lblJobmergeName.ClientID%>').innerText = "";
                        document.getElementById('<%=lblJobmergeAddress.ClientID%>').innerText = "";
                        document.getElementById('<%=lblJobMergeCustNum.ClientID%>').innerText = "";
                        document.getElementById('<%=lblJobmergeCustName.ClientID%>').innerText = "";
                        resultMerge = false;
                    }
                }
                else {
                    resultMerge = false;

                }
                // window.open("../Pages/NewCharge.aspx", "_self");
            },
            failure: function (response) {
                alert(response.d);
            }
        });
        return false;
    }
    function CalculatedTotalOwed(id) {
        if ($(id).prop("checked") == true) {
            var TotalOwed = $('#' + '<%=AmountOwed.ClientID%>').val();
            TotalOwed = TotalOwed.replace("$", "");
            if (TotalOwed == "") {
                TotalOwed = "0";
            }
            if ($(id).parents('tr').find('.wrapperDivHidden input[type="hidden"]').val() != "") {
                TotalOwed = (parseFloat(TotalOwed) + parseFloat($(id).parents('tr').find('.wrapperDivHidden input[type="hidden"]').val())).toFixed(2)
            }
            TotalOwed = parseFloat(TotalOwed).toFixed(2);
            $('#' + '<%=AmountOwed.ClientID%>').val(TotalOwed);
        }
        else if ($(id).prop("checked") == false) {
            var TotalOwed = $('#' + '<%=AmountOwed.ClientID%>').val();
            TotalOwed = TotalOwed.replace("$", "");
            if (TotalOwed == "") {
                TotalOwed = "0";
            }
            if ($(id).parents('tr').find('.wrapperDivHidden input[type="hidden"]').val() != "") {
                TotalOwed = (parseFloat(TotalOwed) - parseFloat($(id).parents('tr').find('.wrapperDivHidden input[type="hidden"]').val())).toFixed(2)
            }
            TotalOwed = parseFloat(TotalOwed).toFixed(2);
            $('#' + '<%=AmountOwed.ClientID%>').val(TotalOwed);
        }
    }
    function DisplayNotePopUp() {
        // alert('hi');
        $("#ModalJobNoteDetail").modal('show');
    }
    function DisplayAdditionalInfo() {
        // alert('hi');
        $("#ModaldivAdditionalInfo").modal('show');
        return false;
    }
    function DisplayCustomerInfo() {
        // alert('hi');
        $("#ModaldivCustomer").modal('show');
        return false;
    }
    function SaveNoticeRequest() {
        debugger;
        // alert("SaveNoticeRequest");
        debugger;
        <%--if (document.getElementById('<%=hdnMonthly.ClientID%>').value == "True") {--%>
        //alert('1');
        if ((document.getElementById('<%=hdnUseJobViewMenu.ClientID%>').value.trim() == "True") && ((document.getElementById('<%=hdnState.ClientID%>').value.trim() !== "TX") && (document.getElementById('<%=hdnState.ClientID%>').value.trim() !== "TN") && (document.getElementById('<%=hdnState.ClientID%>').value.trim() !== "LA"))) {
            // alert('new test');
            console.log(document.getElementById('<%=hdnUseJobViewMenu.ClientID%>').value);
            console.log(document.getElementById('<%=hdnState.ClientID%>').value);
            BindServiceTypeDropdown();
           
            
            ShowServiceTypeModal();
        }
        else if ((document.getElementById('<%=hdnUseJobViewMenu.ClientID%>').value.trim() == "True") && ((document.getElementById('<%=hdnState.ClientID%>').value.trim() == "TX") || (document.getElementById('<%=hdnState.ClientID%>').value.trim() == "TN") || (document.getElementById('<%=hdnState.ClientID%>').value.trim() == "LA"))) {
            //alert('h');

            $('#UlServiceType li').remove();


            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(2,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
            $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(7,\'Verify Owner Only\')">Verify Owner Only</a></li>');
            
            ShowServiceTypeModal();
            //***^^^Created By Pooja On 10/28/20 ^^^***
            //BindServiceTypeDropdown();
            // alert('2');
            // ShowServiceTypeModal();
            //***^^^    ^^^***

        }

        else if ((document.getElementById('<%=hdnUseJobViewMenu.ClientID%>').value.trim() == "True" || document.getElementById('<%=hdnUseJobViewMenu.ClientID%>').value.trim() == "False") && (document.getElementById('<%=hdnIsClientViewJob.ClientID%>').value.trim() == "True") && ((document.getElementById('<%=hdnState.ClientID%>').value.trim() == "TX") || (document.getElementById('<%=hdnState.ClientID%>').value.trim() == "TN"))) {
                 //alert('hP');
            $('#UlServiceType li').remove();

          $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(2,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
          $('#UlServiceType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceType(7,\'Verify Owner Only\')">Verify Owner Only</a></li>');
          
            ShowServiceTypeModal();
            //***^^^    ^^^***

        }

         else if (document.getElementById('<%=hdnState.ClientID%>').value.trim() == "TX") {
            if (document.getElementById('<%=hdnDateAssigned.ClientID%>').value.trim() != "") {
                //alert("hi");
                //////////////////////////////////////////////////////////////////////////////////
                $('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceTypeTX(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
                //$('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceTypeTX(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

                $('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceTypeTX(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
                $('#UlServiceTypeTX').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerTX" onclick="setSelectedSserviceTypeTX(5,\'Verify Owner TX\')">Verify Owner TX</a></li>');

                $('#btnServiceTypeTX').html('Verify Job Data Only - No Notice Sent' + "<span class='caret' ></span>");

                var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                x.value = '1';
                //////////////////////////////////////////////////////////////////////////////////
                $("#ModaldivRequestNoticeTX").modal('show');
            }
            else {
                //alert("1");
                //***^^^Commented By Pooja On 10/28/20 ^^^***
                <%--document.getElementById('<%=btnSaveNoticeRequest.ClientID%>').click();--%>
                //***^^^    ^^^***

                //***^^^Created By Pooja On 10/28/20 ^^^***
                BindServiceTypeDropdown();
                // alert('2');
                ShowServiceTypeModal();
                //***^^^    ^^^***
            }
        }
        else if (document.getElementById('<%=hdnState.ClientID%>').value.trim() == "TN") {
            if (document.getElementById('<%=hdnDateAssigned.ClientID%>').value.trim() != "") {
               // alert('2');
                //////////////////////////////////////////////////////////////////////////////////
                $('#UlServiceTypeTN').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceTypeTN(3,\'Verify Job Data Only - No Notice Sent\')">Verify Job Data Only - No Notice Sent</a></li>');
                //$('#UlServiceTypeTN').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceTypeTN(1,\'Verify Job Data - Send Notice\')">Verify Job Data - Send Notice</a></li>');

                $('#UlServiceTypeTN').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceTypeTN(4,\'Store Job With Data Provided\')">Store Job With Data Provided</a></li>');
                $('#UlServiceTypeTN').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyOwnerOnly" onclick="setSelectedSserviceTypeTN(8,\'Verify Owner Only\')">Verify Owner Only</a></li>');

                $('#btnServiceTypeTN').html('Verify Job Data Only - No Notice Sent' + "<span class='caret' ></span>");
                var x = document.getElementById('<%= hdnServiceTypeId.ClientID %>');
                    x.value = '1';
                //////////////////////////////////////////////////////////////////////////////////
                    $("#ModaldivRequestNoticeTN").modal('show');
                }
            else {

                //***^^^Commented By Pooja On 10/28/20 ^^^***
                <%--  // document.getElementById('<%=btnSaveNoticeRequest.ClientID%>').click();--%>
                //***^^^    ^^^***
                //alert("3");
                //***^^^Created By Pooja On 10/28/20 ^^^***
                BindServiceTypeDropdown();
               // alert('1');
                ShowServiceTypeModal();
                //***^^^    ^^^***
            }
        }
        else {
            //***^^^Commented By Pooja On 10/28/20 ^^^***
            <%--   document.getElementById('<%=btnSaveNoticeRequest.ClientID%>').click();--%>
            //***^^^    ^^^***
            
            //alert("test");
            //***^^^Created By Pooja On 10/28/20 ^^^***
            BindServiceTypeDropdown();
            //alert('3');
            ShowServiceTypeModal();
            //***^^^    ^^^***

        }
    //}
<%--    else {
    <%--document.getElementById('<%=btnSaveNoticeRequest.ClientID%>').click();
            //alert('4');
            //***^^^Created By Pooja On 10/28/20 ^^^***
            BindServiceTypeDropdown();
            //alert('3');
            ShowServiceTypeModal();
            //***^^^    ^^^***
            // $("#ModelRequestverification").modal('show');
        }--%>
    }
    function DisplayRequestNotice() {
        //  alert("DisplayRequestNotice");
        debugger;
        console.log(document.getElementById('<%=hdnIsCancelled.ClientID%>').value);
        document.getElementById('<%=CustomValidator2.ClientID%>').innerHTML = "";
        if (document.getElementById('<%=hdnIsCancelled.ClientID%>').value == "True") {

            VerifyCancelledRequest();
        }
        else {

        SaveNoticeRequest();
    }
    return false;
}

function VerifyCancelledRequest() {
    //  alert("VerifyCancelledRequest");
    debugger;
    swal({
        title: "Are you sure?",
        text: "A Notice Request was previously cancelled on this job. Do you still wish to proceed with this current request?",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: 'No',
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes',
        closeOnConfirm: true
    },
function () {
    SaveNoticeRequest();
});
    return false;
}
function SetNoteId(id, Type) {
    document.getElementById('<%=hdnNoteType.ClientID%>').value = Type;
    document.getElementById('<%=hdnBatchJobNoteId.ClientID%>').value = $(id).attr("rowno");
    document.getElementById('<%=btnNoteDetail.ClientID%>').click();
        return false;
    }
    function DeleteAccountWaiver(id) {
        debugger;
        document.getElementById('<%=hdnWaiverId.ClientID%>').value = $(id).attr("rowno");
        debugger;
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes, delete it!',
            closeOnConfirm: true
        },
            function () {
                document.getElementById('<%=hddbTabName.ClientID%>').value = "Waivers";
                document.getElementById('<%=btnDeleteWaiverId.ClientID%>').click();
            });
        return false;
    }
    function DeleteAccountStateForm(id) {
        debugger;
        document.getElementById('<%=hdnStateFormId.ClientID%>').value = $(id).attr("rowno");
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: true
    },
  function () {
      document.getElementById('<%=btnStateFormId.ClientID%>').click();
  });
    return false;
}
function DeleteAccountTexas(id) {
    document.getElementById('<%=hdnTXReqId.ClientID%>').value = $(id).attr("rowno");
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false
    },
  function () {
      document.getElementById('<%=btnTXReqId.ClientID%>').click();
  });
    return false;
}
function DeleteAccountTexasRequest(id) {
    document.getElementById('<%=hdnTexasRequest.ClientID%>').value = $(id).attr("rowno");
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false
    },
  function () {
      document.getElementById('<%=btnTexasRequest.ClientID%>').click();
  });
    return false;
}
function DeleteAccountInvoice(id) {
    document.getElementById('<%=hdnInvoiceid.ClientID%>').value = $(id).attr("rowno");
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes, delete it!',
        closeOnConfirm: false
    },
  function () {
      document.getElementById('<%=btnInvoiceid.ClientID%>').click();
  });
    return false;
}
function MaintainMenuOpen() {

    SetHeaderBreadCrumb('RentalView', 'View Jobs', 'Find Jobs');
    MainMenuToggle('liViewJobs');
    SubMenuToggle('liViewAllJobs')

}

function ActivateTabSet() {
    // alert('hello:Notes');
    ActivateTab('Notes')
}
function DisplayTablelayout() {
    // document.getElementById('<%=btnTableLayout.ClientID%>').click();
}
    function SetHeaderInformation(PageName) {
        //   alert(PageName);
        if (PageName == "") {
            //alert('hi');
            $('#liTools').removeClass('active open');
            $('#liTexaspendingList').removeClass('active');
            $('#liJobsTitle').text("Jobs");
            $('#liJobSSubTitle').text("View Jobs");
            $('#liJobs').addClass('site-menu-item has-sub active open');
            $('#lisubJobList').addClass('site-menu-item active');

        }
        else {
            $('#liJobs').removeClass('active open');
            $('#lisubJobList').removeClass('active');
            $('#liJobsTitle').text("Tools");
            $('#liJobSSubTitle').text("Texas Pending List");
            $('#liTools').addClass('site-menu-item has-sub active open');
            $('#liTexaspendingList').addClass('site-menu-item active');

        }

    }
    function ActivateTab(TabName) {
        // alert('hello');
        // alert(TabName);
        switch (TabName) {
            case "MainInfo":
                // alert('1');
                if (!$('#' + '<%=MainInfo.ClientID%>').hasClass('active')) {
                    $('#' + '<%=MainInfo.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsOne').hasClass('active'))
                    $('#exampleTabsOne').addClass('active');
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "LegalParties":
                //  alert('2');
                if (!$('#' + '<%=LegalParties.ClientID%>').hasClass('active')) {
                    $('#' + '<%=LegalParties.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsTwo').hasClass('active'))
                    $('#exampleTabsTwo').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "Notes":

                if (!$('#' + '<%=Notes.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Notes.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsThree').hasClass('active'))
                    $('#exampleTabsThree').addClass('active');

                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                //  alert('callend');               
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "NoticesSent":
                // alert('4');
                if (!$('#' + '<%=NoticesSent.ClientID%>').hasClass('active')) {
                    $('#' + '<%=NoticesSent.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsFour').hasClass('active'))
                    $('#exampleTabsFour').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "Docs":
                if (!$('#' + '<%=Docs.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Docs.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsFive').hasClass('active'))
                    $('#exampleTabsFive').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "Waivers":
                // alert('hi');

                if (!$('#' + '<%=Waivers.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Waivers.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsSix').hasClass('active'))
                    $('#exampleTabsSix').addClass('active');
                // alert('callend');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");


                break;
            case "StateForms":
                if (!$('#' + '<%=StateForms.ClientID%>').hasClass('active')) {
                    $('#' + '<%=StateForms.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsSeven').hasClass('active'))
                    $('#exampleTabsSeven').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");

                break;
            case "MergeJob":
                if (!$('#' + '<%=MergeJob.ClientID%>').hasClass('active')) {
                    $('#' + '<%=MergeJob.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsEight').hasClass('active'))
                    $('#exampleTabsEight').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");

                break;
            case "TXRequests":
                if (!$('#' + '<%=TXRequests.ClientID%>').hasClass('active')) {
                    $('#' + '<%=TXRequests.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsNine').hasClass('active'))
                    $('#exampleTabsNine').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");
                $('#' + '<%=Invoices.ClientID%>').removeClass("active");
                $('#exampleTabsTen').removeClass("active");
                break;
            case "Invoices":
                if (!$('#' + '<%=Invoices.ClientID%>').hasClass('active')) {
                    $('#' + '<%=Invoices.ClientID%>').addClass('active');
                }
                if (!$('#exampleTabsTen').hasClass('active'))
                    $('#exampleTabsTen').addClass('active');
                $('#' + '<%=MainInfo.ClientID%>').removeClass("active");
                $('#exampleTabsOne').removeClass("active");
                $('#' + '<%=LegalParties.ClientID%>').removeClass("active");
                $('#exampleTabsTwo').removeClass("active");
                $('#' + '<%=Notes.ClientID%>').removeClass("active");
                $('#exampleTabsThree').removeClass("active");
                $('#' + '<%=NoticesSent.ClientID%>').removeClass("active");
                $('#exampleTabsFour').removeClass("active");
                $('#' + '<%=Docs.ClientID%>').removeClass("active");
                $('#exampleTabsFive').removeClass("active");
                $('#' + '<%=Waivers.ClientID%>').removeClass("active");
                $('#exampleTabsSix').removeClass("active");
                $('#' + '<%=StateForms.ClientID%>').removeClass('active');
                $('#exampleTabsSeven').removeClass("active");
                $('#' + '<%=MergeJob.ClientID%>').removeClass('active');
                $('#exampleTabsEight').removeClass("active");

                $('#exampleTabsNine').removeClass("active");
                break;
        }
    }
    function SetTabName(id) {
        // alert(id);
        document.getElementById('<%=hddbTabName.ClientID%>').value = id;
    }
    function opengooglemap() {

        var strpath = "http://maps.google.com/maps?q=" + $get('<%=JobAdd1.ClientID %>').value + "," + $get('<%=JobCity.ClientID %>').value + "," + $get('<%=JobState.ClientID %>').value + "," + $get('<%=JobZip.ClientID %>').value;


        window.open(strpath, "mywindow", "menubar=1,resizable=1,width=700,height=450");
    }

    function openWin(cert) {

        var str = "./App_Controls/_Custom/LienView/TrackAndConfirm.aspx?CertNum=" + cert
        window.open(str, "mywindow", "menubar=1,resizable=1,width=800,height=550");
        return false;
    }

</script>
<script type="text/javascript">
    $(document).ready(function () {
        debugger;
       // alert("call document"); 
        if ($('#' + '<%=gvwLegalParties.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncLegalPartiesList();
        }
        if ($('#' + '<%=gvwNotes.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncNotesList();
        }
        if ($('#' + '<%=gvwDocs.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncDocsList();
        }
        if ($('#' + '<%=gvwWaivers.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncWaiverList();
        }
        if ($('#' + '<%=gvwStateForms.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncStateList();
        }
        if ($('#' + '<%=gvwNotices.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncNoticesList();
        }
        if ($('#' + '<%=gvInvoiceGrid.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncInvoiceList();
        }
        if ($('#' + '<%=gvwMergeJobs.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncJobMergeList();
        }
        if ($('#' + '<%=gvwTexasReq.ClientID%>' + ' thead').length > 0) {
            CallSuccessFuncTexasList();
        }
        if (getResponse() == "True") {
            //alert(getResponse());
            $('#btnAddLegalParty').prop('disabled', true);
        }
        else {
            //alert(getResponse());
            $('#btnAddLegalParty').prop('disabled', false);
        }
    });
    function CallSuccessFuncNoticesReqList() {
        // alert('hello');
        var defaults = {
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());


    }
    function CallSuccessFuncInvoiceList() {
        
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                 { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                 { "bSortable": true }
                //{ "bSortable": false },
                //{ "bSortable": false },
                //{ "bSortable": false }
            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvInvoiceGrid.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncLegalPartiesList() {
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            // "lengthMenu": [10, 20, 30, 50, 100],
            "bFilter": false,      //Remove Dropdown         
            "bLengthChange": false, //restrict to chnage Dropdown
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }
            ],
            "bPaginate": false, //Remove Pagination
            "bInfo": false //Remove showing entries
        };



        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwLegalParties.ClientID%>').dataTable(options);
    }
   
    function CallSuccessFuncNotesList() {
        debugger;
        //alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwNotes.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncJobMergeList() {

        //  alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [5, 10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
              { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwMergeJobs.ClientID%>').dataTable(options);
        // alert('hello2');
    }
    function CallSuccessFuncNoticesList() {
        debugger;
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                  { "bSortable": true }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwNotices.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncDocsList() {
        debugger;
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false}


            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwDocs.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncWaiverList() {
        debugger;
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwWaivers.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncStateList() {
        debugger;
        // alert('hello');
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }


            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwStateForms.ClientID%>').dataTable(options);

    }
    function CallSuccessFuncTexasList() {
        // alert('hello');
        var defaults = {
            "lengthMenu": [10, 20, 30, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false }

            ],
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());


    }
</script>
<script lang="javascript" type="text/javascript">
    function divexpandcollapse(divname) {
        debugger;
        var div = document.getElementById(divname);
        var img = document.getElementById('img' + divname);
        // alert($("." + divname).css('display'));
        if ($("." + divname).css('display') == "none") {
            //div.style.display = "";
            //div.style.display = "inline-grid";
            $("." + divname).css('display', '');
            img.src = "images/minus.png";
        } else {
            //div.style.display = "";
            //div.style.display = "none";
            $("." + divname).css('display', 'none');
            img.src = "images/plus.png";
        }
    }
    function divexpandcollapseNR(divname) {
        //debugger;
        var div = document.getElementById(divname);
        var img = document.getElementById('imgNR' + divname);
        if ($("." + divname).css('display') == "none") {
            //div.style.display = "";
            //div.style.display = "inline-grid";
            img.src = "images/minus.png";
            $("." + divname).css('display', '');
        } else {
            //div.style.display = "";
            //div.style.display = "none";
            img.src = "images/plus.png";
            $("." + divname).css('display', 'none');
        }
    }
    function BindFileUoloadList() {

        document.getElementById('<%=btnFileUpLoadCancel.ClientID%>').click();
    }
    function SetDisabledNoticeRequest() {
        if ($('#' + '<%= IsNoticeRequested.ClientID %>').val() == "True") {
            $('#btnAddLegalParty').prop('disabled', true);
        }
        else {
            $('#btnAddLegalParty').prop('disabled', false);
        }
    }
    function getResponse() {

        return $('#' + '<%= IsNoticeRequested.ClientID %>').val();
    }
    function OpenGCModal() {
        //alert('hi');
        document.getElementById("listGCNames").style.display = 'none';
        $("#ModalGCSearch").modal('show');
    }
    function DisplayRequestServicesPopup() {
        debugger;
        var SubjectLine = "New Service Request for (" + $('#' + '<%= hdnJobId.ClientID %>').val() + ")";
        $('#' + '<%= txtreqsubject.ClientID %>').val(SubjectLine);
        $('#' + '<%= hdnEmailflag.ClientID %>').val('RS');
        $("#divModalRequestService").modal('show');
    }
    function HideRequestServicesPopup() {
        $("#divModalRequestService").modal('hide');
    }

    //Recension Letter Popup
    function RecensionLetterCreatedPopup(AddressName) {
        debugger;
        swal({
            title: "Recension Letter Created",
            text: "Your Recension Letter has been created for \"" + AddressName + "\" Legal Party.",
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
        });
        ActivateTab('LegalParties');
        return false;
    }

    var currURL;
    currURL = window.location.href;
    function Cancelpopup() {
        debugger;
        location.href = currURL;
        //$("ctl33_ctl00_MainInfo").removeClass("active");
        //$("ctl33_ctl00_LegalParties").addClass("active");
        ActivateTab('LegalParties');
        return false;
    }

    function ExitLegalParties() {
        debugger;
        alert('Legal Party Details are not present.');
        ActivateTab('LegalParties');
        return false;
    }

    function RecensionLetterIsAlreadyCreatedToday(AddressName) {
        debugger;
        alert("Today Recension Letter is already created for \"" + AddressName + "\" Legal Party. Please check in Waivers Tab.");
        ActivateTab('LegalParties');
        return false;
    }

    function showRecensionLetterCreatedPopup(AddressName) {
        debugger;
        swal.close(); // closes sweet alert popup.
        var mymodal = $('#ModalRecensionLetterSubmit');
        document.getElementById("RecensionLetterModaldiv").innerHTML = "<h3>Your Recension Letter has been created for \"" + AddressName + "\" Legal Party.</h3>";
        //mymodal.find('.modal-body').text("<h3>Your Recension Letter has been created for \"" + AddressName + "\" Legal Party.</h3>");
        mymodal.modal('show');
        return false;
    }

    function SendRecensionLetterMail() {
        debugger;
        if (document.getElementById('<%=txtToListForRecensionLetter.ClientID%>').value == "") {
            document.getElementById('<%=lblError.ClientID%>').innerText = "Please enter the 'To' Email Id.";
            return false;
        }
         if (document.getElementById('<%=txtRecensionLetterSubject.ClientID%>').value == "") {
            document.getElementById('<%=lblError.ClientID%>').innerText = "Please enter the Subject.";
            return false;
        }
        
            document.getElementById('<%=btnEmailsend.ClientID%>').click();
            HideEmailRecensionLetterPopUp();
            return false;
  
    }
    function HideRecensionLetterPopUp() {
        // alert('hi');
        $("#ModalRecensionLetterSubmit").modal('hide');
    }

    function ShowRecensionLetterPopUp() {
        // alert('hi');
        $("#ModalRecensionLetterSubmit").modal('show');
    }

    function DisplayEmailRecensionLetterPopUp() {
        // alert('hi');
        $("#divModalEmailRecensionLetter").modal('show');
    }
    function HideEmailRecensionLetterPopUp() {
        // alert('hi');
        $("#divModalEmailRecensionLetter").modal('hide');
        $('div').removeClass('modal-backdrop fade in');
        //fadeout();
    }

    function fadeout() {
        debugger;
        //alert(currURL);
        location.href = currURL;
        $('div').removeClass('modal-backdrop fade in');
        return false();
    }

    function RecensionLetterEmailSendPopup(textdata) {
        debugger;
        swal({
            title: "Successfully Sent!",
            text: textdata,
            type: "success",
            //showDoneButton: true,
            //closeOnDone: true,
            showCancelButton: true,
            cancelButtonText: "OK",
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false

        });
    }

    function RecensionLetterNotAvailableForLegalParties(AddressName) {
        debugger;
        alert("Recension Letter is Not Available For \""+AddressName+"\" Legal Party.");
        ActivateTab('LegalParties');
        return false;
    }

    function CreateAnotherRecensionLetter(AddressName) {
        swal({
            title: "Are you sure?",
            text: "You want to create another Recension Letter for \"" + AddressName + "\" Legal Party.",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Yes',
            closeOnConfirm: false
        },
      function (isConfirm) {
          if (isConfirm) {
              document.getElementById('<%=btnCreateAnotherRecensionLetter.ClientID%>').click(); 
            }
      });
        return false;
    }

     function IsSendRequestServiceChecked()
     {
         debugger
         
         var checkvalue = document.getElementById('<%=ChkSendRequestService.ClientID%>').checked;
         if (checkvalue == true) {
             $("#btnSendRequestService").attr("disabled", false);
         }
         else if (checkvalue == false) {
             $("#btnSendRequestService").attr("disabled", true);
         }
         
        
     }
</script>
<asp:HiddenField ID="IsNoticeRequested" runat="server" />
<asp:HiddenField ID="hdnJobId" runat="server" />
<asp:HiddenField ID="hdnJobState" runat="server" />


<div id="modcontainer" style="margin: 10px; width: 100%;">
    <h1>
        <asp:Literal ID="litJobInfo" runat="server"></asp:Literal></h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <asp:Panel ID="panJobInfo" runat="server">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                <asp:Button ID="btnBack" CssClass="btn btn-primary" runat="server" Text="Back to List" CausesValidation="False" />
                                <asp:HiddenField runat="server" ID="hddbTabName" Value="MainInfo" />
                                <%--##101--%>
                                <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter" CausesValidation="False" OnClick="btnBackFilter_Click"></asp:LinkButton>
                                <asp:Button ID="btngooglejob" runat="server" CssClass="btn btn-primary" Text="Google Job" OnClientClick="opengooglemap();" />
                                <asp:Button ID="btnTableLayout" runat="server" Style="display: none;" OnClick="btnTableLayout_Click" />
                                <input type="button" id="btnRequestNotice" runat="server" class="btn btn-primary" value="Request Notice" style="float: right" onclick="DisplayRequestNotice()" />
                                <asp:Button ID="btnNoticeRequest" runat="server" Text="Notice Request new" CausesValidation="false" OnClick="btnNoticeRequest_Click" Style="display: none;" />
                            </div>
                            <%--  <div class="col-md-2" style="text-align: left;">
                              
                            </div>
                            <div class="col-md-2" style="text-align: left;">
                            
                            </div>--%>
                        </div>
                    </div>
                    <div class="nav-tabs-horizontal">
                        <ul class="nav nav-tabs" data-plugin="nav-tabs" role="tablist">
                            <li id="MainInfo" class="active" role="presentation" runat="server"><a data-toggle="tab" onclick="SetTabName('MainInfo');" href="#exampleTabsOne" aria-controls="exampleTabsOne" role="tab" aria-expanded="false">Main Info</a></li>

                            <%--<li id="Invoices" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Invoices');" href="#exampleTabsTen" aria-controls="exampleTabsTen" role="tab" aria-expanded="false">Rental Invoice</a></li>--%>
                            <li id="Invoices" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Invoices');" href="#exampleTabsTen" aria-controls="exampleTabsTen" role="tab" aria-expanded="false">Invoice</a></li>
                            <li id="LegalParties" role="presentation" runat="server"><a data-toggle="tab" onclick="SetTabName('LegalParties');" href="#exampleTabsTwo" aria-controls="exampleTabsTwo" role="tab" aria-expanded="false">Legal Parties</a></li>
                            <li id="Notes" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Notes');" href="#exampleTabsThree" aria-controls="exampleTabsThree" role="tab" aria-expanded="false">Notes</a></li>
                            <li id="NoticesSent" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('NoticesSent');" href="#exampleTabsFour" aria-controls="exampleTabsFour" role="tab" aria-expanded="false">Notices Sent</a></li>
                            <li id="Docs" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Docs');" href="#exampleTabsFive" aria-controls="exampleTabsFive" role="tab" aria-expanded="false">Docs</a></li>
                            <li id="Waivers" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('Waivers');" href="#exampleTabsSix" aria-controls="exampleTabsSix" role="tab" aria-expanded="false">Waivers</a></li>
                            <li id="StateForms" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('StateForms');" href="#exampleTabsSeven" aria-controls="exampleTabsSeven" role="tab" aria-expanded="false">State Forms</a></li>
                            <li id="MergeJob" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('MergeJob');" href="#exampleTabsEight" aria-controls="exampleTabsEight" role="tab" aria-expanded="false">Merge Job</a></li>
                            <%-- <li id="NoticeRequest" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('NoticeRequest');" href="#exampleTabsEight" aria-controls="exampleTabsEight" role="tab" aria-expanded="false">Notice Request</a></li>--%>
                            <%--##100--%>
                            <li id="TXRequests" role="presentation" class="" runat="server"><a data-toggle="tab" onclick="SetTabName('TXRequests');" href="#exampleTabsNine" aria-controls="exampleTabsNine" role="tab" aria-expanded="false">TX Requests</a></li>
                        </ul>
                        <div class="tab-content padding-top-20" style="border: 1px solid lightgray;">
                            <div class="tab-pane active" id="exampleTabsOne" role="tabpanel">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable PaddingleftRemove">Job Number:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobNumber" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3  control-label align-lable PaddingleftRemove">First Furnished:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="StartDate" runat="server" CssClass="form-control" ReadOnly="True" DataType="DateValue" Width="115px"
                                                    Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control"
                                                    RequiredCSS="requiredtextbox" Tag="EndDate" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask=""
                                                    FormatString="{0:dd/MM/yyyy}" FriendlyName=""
                                                    ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>

                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Bond Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="BondDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}"
                                                    FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Job Name:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobName" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobName" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Last Furnished:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="EndDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="DateAssigned" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Bond Sent:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondSent" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="BondDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Address:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobAdd1" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage=""
                                                IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobAdd1" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" style="">Notice of Comp:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NOCDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="NoticeSent" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">SN Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="SNDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="SNDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove"></label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobAdd2" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobAdd2" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" style="">Notice Requested:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NoticeRequested" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="NoticeRequested" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                                <asp:HiddenField ID="hdnDateAssigned" runat="server" Value="" />
                                                <asp:HiddenField ID="hdnisclientviewmanager" runat="server" Value="" />

                                            </div>

                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">SN Sent:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="SNSent" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="SNDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove"></label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobAdd3" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobAdd2" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" style="">Notice Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NOIDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="LienSent" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" style="">Foreclose Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="ForeclosureDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="NoticeRequested" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                                <asp:HiddenField ID="HiddenField1" runat="server" Value="" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Property Type:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3  PaddingleftRemove">
                                            <cc2:DataEntryBox ID="PropertyType" runat="server" CssClass="form-control" ReadOnly="True" Tag="PropertyType" BackColor="White" DataType="Any"
                                                ErrorMessage="" IsRequired="False" Style="width: 150px" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-1 PaddingleftRemove"></div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Notice Sent:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NoticeSent" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="LienSent" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" style="">Foreclose Filed:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="ForeclosureDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="NoticeRequested" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                                <asp:HiddenField ID="HiddenField2" runat="server" Value="" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Job Balance:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3  PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobBalance" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Money" ErrorMessage=""
                                                IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobAdd2" Value="0.00"
                                                EnableClientSideScript="False" FormatMask="" Style="width: 150px" FormatString="{0:d3}" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                            <%--  <cc1:DataEntryBox ID="EstBalance" runat="server" CssClass="form-control" DataType="Money"
                                                        EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                                        FriendlyName="" IsRequired="True" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                                        Value="0.00" Width="175px" TabIndex="11">$0.00</cc1:DataEntryBox>--%>
                                        </div>
                                        <div class="col-md-1 PaddingleftRemove"></div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3  control-label align-lable PaddingleftRemove">NOIDeadline Date:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="NOIDeadlineDate2" runat="server" CssClass="form-control" ReadOnly="True" DataType="DateValue" Width="115px"
                                                    Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control"
                                                    RequiredCSS="requiredtextbox" Tag="EndDate" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask=""
                                                    FormatString="{0:dd/MM/yyyy}" FriendlyName=""
                                                    ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" style="">Bond Suit Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondSuitDeadline" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="NoticeRequested" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                                <asp:HiddenField ID="HiddenField3" runat="server" Value="" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Est Balance:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3  PaddingleftRemove">
                                            <cc2:DataEntryBox ID="EstBalance" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Money" ErrorMessage=""
                                                IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobAdd2" Value="0.00"
                                                EnableClientSideScript="False" Style="width: 150px" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>

                                        </div>
                                        <div class="col-md-1 PaddingleftRemove"></div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Lien Deadline:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="LienDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="LienDeadlineDate" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                                            </div>
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove" style="">Bond Suit Filed:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="BondSuitDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" Tag="NoticeRequested" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                                    NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                                                <asp:HiddenField ID="HiddenField4" runat="server" Value="" />

                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">


                                       <%-- <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Desk:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="Desk" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>--%>

                                         <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Filter Key:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="txtFilterKey" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="FilterKey" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>



                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Lien Filed:</label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                                <cc2:DataEntryBox ID="LienDate" runat="server" CssClass="form-control" ReadOnly="True" Width="115px"
                                                    DataType="DateValue" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="EstBalance" Value="0.00" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}" FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$">$0.00</cc2:DataEntryBox>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Job Id:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="JobNo" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="StatusCode" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <div class="col-md-7 col-sm-7 col-xs-7 PaddingleftRemove">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove"></label>
                                            <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            </div>
                                            <div class="col-md-5 col-sm-5 col-xs-5">
                                                <input type="button" id="btnRequestAdditionalServices" class="btn btn-primary" value="Request Additional Services" style="float: right" onclick="DisplayRequestServicesPopup()" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Cust Ref#:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="txtCustRef" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>

                                    </div>
                                    <div class="form-group">

                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Customer:</label>
                                        <div class="col-md-3 col-sm-3 col-xs-3 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="Customers" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">Job Status:</label>
                                        <div class="col-md-5 col-sm-5 col-xs-5 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="StatusCode" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="StatusCode" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <%--  <div class="row">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Federal:</label>
                                            <div class="col-md-2 col-sm-2 col-xs-2  PaddingleftRemove">
                                                <div class="checkbox-custom checkbox-default">
                                                    <asp:CheckBox ID="chkFederal" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                                                </div>
                                            </div>
                                        </div>--%>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-md-2 col-sm-2 col-xs-2  control-label align-lable PaddingleftRemove">CRFS Status:</label>
                                        <div class="col-md-5 col-sm-5 col-xs-5 PaddingleftRemove">
                                            <cc2:DataEntryBox ID="CRFSStatus" runat="server" CssClass="form-control" ReadOnly="True"
                                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="StatusCode" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        </div>
                                        <%--    <div class="row">
                                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable PaddingleftRemove">Residential:</label>
                                            <div class="col-md-2 col-sm-2 col-xs-2  PaddingleftRemove">
                                                <div class="checkbox-custom checkbox-default">
                                                    <asp:CheckBox ID="chkResindential" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                                                </div>
                                            </div>
                                        </div>--%>
                                    </div>

                                    <div class="form-group" style="display: none;">
                                        <cc2:DataEntryBox ID="JobId" runat="server" CssClass="form-control" ReadOnly="True" DataType="Any"
                                            Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobId" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression="">12:00:00 AM</cc2:DataEntryBox>
                                        <cc2:DataEntryBox ID="JobState" runat="server" Style="text-align: center; display: none;" CssClass="form-control" Width="60px" ReadOnly="True" Tag="JobSate" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                        <cc2:DataEntryBox ID="JobZip" runat="server" BackColor="White"
                                            CssClass="form-control" DataType="Any"
                                            NormalCSS="form-control" ReadOnly="True" RequiredCSS="requiredtextbox" Tag="JobSate"
                                            Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" Style="display: none;"></cc2:DataEntryBox>
                                        <cc2:DataEntryBox ID="JobCity" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="textbox" RequiredCSS="requiredtextbox" Tag="JobCity" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12" style="text-align: left;">
                                            <uc6:JobEdit ID="JobEdit1" runat="server"  />
                                            <input type="button" id="btnCustomer" class="btn btn-primary" value="Customer Info" onclick="DisplayCustomerInfo()" />
                                            <input type="button" id="btnAdditionalInfo"  runat="server" class="btn btn-primary" value="Additional Info" onclick="DisplayAdditionalInfo()" />
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="tab-pane" id="exampleTabsTen" role="tabpanel">

                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-5 col-sm-5 col-xs-5 " style="text-align: left; margin-left: 10px;">
                                                <asp:Button ID="btnViewAll" runat="server" CssClass="btn btn-primary" CausesValidation="False" Text="View All" OnClick="btnViewAll_Click" />
                                                <asp:Button ID="btnViewOpen" runat="server" CssClass="btn btn-primary" CausesValidation="False" Text="View Open" OnClick="btnViewOpen_Click" />
                                            </div>
                                        </div>
                                        <%--  <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-2 ">
                                                <asp:LinkButton ID="btnAddNewINVC" runat="server" Text="Add New Invoice" CssClass="btn btn-primary" />
                                            </div>
                                            <div class="col-md-2 col-sm-2 col-xs-2 ">
                                                <asp:LinkButton ID="btnPrintINVC" runat="server" Text="Print Detail" CssClass="btn btn-primary" />
                                            </div>
                                        </div>--%>
                                        <div class="form-group" style="margin-left: 0px; margin-right: 0px;">

                                            <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #3a6dae; text-align: left;">
                                                <span style="color: white;">Invoices</span>
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                <div style="width: auto; max-height: 350px; overflow: auto;">

                                                    <asp:Repeater ID="rptRentalInvoice" runat="server" OnItemDataBound="rptRentalInvoice_ItemDataBound">
                                                        <HeaderTemplate>
                                                            <table id="TableJobViewInfo" cellspacing="0" rules="all" border="1" style="font-family: Arial; width: 100%;">
                                                                <tr style="background-color: #add8e6; height: 40px;">
                                                                    <th scope="col" style="width: 25px;"></th>
                                                                    <th scope="col" style="width: 80px; text-align: center; font-weight: bold; font-family: Arial;">Invoice #</th>
                                                                    <th scope="col" style="width: 80px; font-weight: bold; text-align: center; font-family: Arial;">Seq #</th>
                                                                    <th scope="col" style="width: 160px; font-weight: bold; text-align: center; font-family: Arial;">Invoice Date</th>
                                                                    <th scope="col" style="width: 70px; font-weight: bold; text-align: center; font-family: Arial;">Status</th>
                                                                    <th scope="col" style="width: 120px; font-weight: bold; text-align: center; font-family: Arial;">Original Balance</th>
                                                                    <th scope="col" style="width: 120px; font-weight: bold; text-align: center; font-family: Arial;">Current Balance</th>
                                                                    <th scope="col" style="width: 50px; font-weight: bold; text-align: center; font-family: Arial;">Date Updated</th>
                                                                    <th scope="col" style="width: 50px; font-weight: bold; text-align: center; font-family: Arial;">TX Noticed?</th>
                                                                    <th scope="col" style="width: 100px; font-weight: bold; text-align: center; font-family: Arial;">Deadline 1</th>
                                                                    <th scope="col" style="width: 100px; font-weight: bold; text-align: center; font-family: Arial;">Deadline 2</th>
                                                                    <th scope="col" style="width: 0.5px;"></th>
                                                                </tr>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <tr style="background-color: #f0f8ff; height: 40px;">
                                                                <td style="font-weight: bold;">
                                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("RANum")%>');">
                                                                        <img id='imgdiv<%# Eval("RANum")%>' width="9px" border="0" src="images/plus.png" alt="plus" />
                                                                    </a>
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    <asp:Label ID="lblInvoiceNum" runat="server" Text='<%# Eval("RANum") %>' />
                                                                    <asp:HiddenField ID="hdnBatchJobrentalId" runat="server" Value='<%# Eval("RANum") %>' />
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    <asp:Label ID="lblSeqNum" runat="server" Text='<%# Eval("BranchNum")%>' />
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    <asp:Label ID="lblInvoiceDate" runat="server" />
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    <asp:Label ID="lblClientStatus" runat="server" Text='<%# Eval("ClientStatus") %>' />
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                   <asp:Label ID="lblOriginalBalance" runat="server" Text='<%#"$" + " " + Eval("OriginalBalance").ToString()%>' />
                                                                 
                                                             
                                                                 
                                                                </td>
                                                                <td style="font-weight: bold;">
                                                                    <asp:Label ID="lblCurrentBalance" runat="server" Text='<%#"$" + " " + Eval("CurrentBalance").ToString()%>' />
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>

                                                                <td>

                                                                    <asp:Repeater ID="rptRentalInvoiceDetail" runat="server" OnItemDataBound="rptRentalInvoiceDetail_ItemDataBound">
                                                                        <ItemTemplate>

                                                                            <tr class='div<%# Eval("InvoiceNum")%>' style="display: none; height: 40px;">
                                                                                <td></td>
                                                                                <td style="text-align: center;">
                                                                                    <asp:Label ID="lblContractSeq" runat="server" />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblSeqNum" runat="server" Text='<%# Eval("SeqNo")%>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblInvoiceDate" runat="server" Text='<%# Eval("InvoiceDate") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblOriginalBalance" runat="server" Text='<%#"$" + " " + Eval("OriginalBalance").ToString()%>' />
                                                                                    
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblCurrentBalance" runat="server" Text='<%#"$" + " " + Eval("CurrentBalance").ToString()%>' />
                                                                                </td>
                                                                                <td>
                                                                                    <%--<asp:Label ID="lblNSF" runat="server" Text='<%# Eval("NSFFlag") %>' />--%>
                                                                                    <asp:Label ID="lblDateUpdated" runat="server" Text='<%# Eval("DateUpdated")%>'></asp:Label>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblNoticed" runat="server" Text='<%# Eval("IsNoticed") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblTXDeadLine1" runat="server" Text='<%# Eval("DeadlineDate60Days", "{0:MM/dd/yy}") %>' />
                                                                                </td>
                                                                                <td>
                                                                                    <asp:Label ID="lblTXDeadLine2" runat="server" Text='<%# Eval("DeadlineDate90Days", "{0:MM/dd/yy}") %>' />
                                                                                </td>


                                                                                <%--<td>
                                                                                    <div class="checkbox-custom checkbox-default" style="text-align: center;">
                                                                                        <asp:CheckBox ID="chkInvoice" runat="server" Text=" " />
                                                                                    </div>
                                                                                </td>--%>

                                                                                <%--   <td></td>--%>
                                                                            </tr>

                                                                        </ItemTemplate>
                                                                    </asp:Repeater>

                                                                </td>
                                                            </tr>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            </table>
                                                        </FooterTemplate>
                                                    </asp:Repeater>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <div class="form-group" style="margin-left: 0px; margin-right: 0px;">
                                            <div class="col-md-12 col-sm-12 col-xs-12" style="background-color: #3a6dae; text-align: left;">
                                                <span style="color: white;">Other Invoices</span>
                                            </div>
                                        </div>
                                        <div class="form-group">

                                            <div class="col-md-12 col-sm-12 col-xs-12">

                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvInvoiceGrid" runat="server" AutoGenerateColumns="false" OnRowDataBound="gvInvoiceGrid_RowDataBound" Width="100%"
                                                        CssClass="table dataTable table-striped">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Invoice #" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblContractSeq" runat="server" Width="105px"></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="SeqNo" SortExpression="SeqNo" HeaderText="Seq #">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="BranchNum" SortExpression="BranchNum" HeaderText="Br #">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField ItemStyle-Width="20px" HeaderText="Type" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("ContractType") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Invoice Date" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblInvoiceDate" runat="server" Text='<%# Eval("InvoiceDate")%>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField ItemStyle-Width="20px" HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Original Balance" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblOriginalBalance" runat="server" Text='<%# Eval("OriginalBalance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Current Balance" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblCurrentBalance" runat="server" Text='<%# Eval("CurrentBalance") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField HeaderText="NSF" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblNSSflag" runat="server" Text='<%# Eval("NSFFlag") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>--%>
                                                            <asp:TemplateField HeaderText="Date Updated" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblDateUpdated" runat="server" Text='<%# Eval("DateUpdated") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="UCC" ItemStyle-HorizontalAlign="Center">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUCC" runat="server" Text='<%# Eval("UCCFlag") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <HeaderStyle CssClass="HeaderStyleInvoiceGrid" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsTwo" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left; padding-left: 18px;">
                                                <uc3:AddLegalPty ID="AddLegalPty1" runat="server" />
                                                <button id="btnAddLegalParty" class="btn btn-primary" onclick="OpenLegalPartyModal();return false;" style="margin: 10px 0px; text-align: left;">Add Legal Party</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwLegalParties" Width="100%" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="table dataTable table-striped">
                                                        <%--    <RowStyle CssClass="rowstyle" />
                                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:BoundField DataField="TypeCode" SortExpression="TypeCode" HeaderText="Legal Party">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AddressName" SortExpression="AddressName" HeaderText="Name">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AddressLine1" SortExpression="AddressLine1" HeaderText="Address1">
                                                                <ItemStyle Height="15px" Wrap="True" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AddressLine2" SortExpression="AddressLine2" HeaderText="Address2">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="City" SortExpression="City" HeaderText="City">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="State" SortExpression="State" HeaderText="State">
                                                                <ItemStyle HorizontalAlign="Center" Width="20px" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                       <%--     <asp:BoundField DataField="PostalCode" SortExpression="PostalCode" HeaderText="Zip">
                                                                <ItemStyle Width="40px" Height="15px" Wrap="False" />
                                                            </asp:BoundField>--%>
                                                            <asp:BoundField DataField="TelePhone1" SortExpression="TelePhone1" HeaderText="Phone">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>

                                                                <asp:TemplateField HeaderText="Recension">
                                                                <ItemTemplate>
                                                                       <asp:LinkButton ID="btnRecension" CssClass="btn btn-primary" Text="Recension" runat="server" style="color: white !important;" />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="70px" Wrap="False" />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsThree" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left; padding-left: 18px;" id="divnotes">
                                                <uc7:JobAddNote ID="JobAddNote1" runat="server" />
                                                <asp:Button ID="btnPrintNotes" runat="server" Text="Print Notes" CssClass="btn btn-primary" />
                                            </div>
                                            <%--   <div class="col-md-2 col-sm-2 col-xs-2 ">
                                              
                                            </div>--%>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwNotes" Width="100%" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" CssClass="table dataTable table-striped">
                                                        <%--   <RowStyle CssClass="rowstyle" />
                                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <%--&nbsp;&nbsp;<asp:ImageButton ID="btnViewNote" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />&nbsp;&nbsp;--%>
                                                                    <%--<asp:LinkButton ID="btnViewNote" CssClass="icon ti-eye" runat="server" OnClientClick="return SetNoteId(this)"></asp:LinkButton>--%>
                                                                    <asp:LinkButton ID="btnViewNote" CssClass="icon ti-eye" runat="server" OnClientClick=<%# "javascript:return SetNoteId(this,'" + Eval("type") + "')"%>></asp:LinkButton>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="25px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateCreated" HeaderText="Date" SortExpression="DateCreated">
                                                                <ItemStyle Height="15px" HorizontalAlign="Center" Width="100px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="UserId" HeaderText="By" SortExpression="UserId">
                                                                <ItemStyle Height="15px" Width="100px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Note" HeaderText="Note" SortExpression="Note">
                                                                <ItemStyle Wrap="True" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>

                                        </div>
                                    </div>



                                </div>
                            </div>
                            <div class="tab-pane active" id="exampleTabsFour" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwNotices" Width="100%" runat="server" AutoGenerateColumns="False" DataKeyNames="JobNoticeHistoryId" CssClass="table dataTable table-striped">
                                                        <%-- <RowStyle CssClass="rowstyle" />
                                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <%--&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnPrntNotice" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />--%>
                                                                    <asp:LinkButton ID="btnPrntNotice" CssClass="icon ti-eye" runat="server" OnClientClick="Showloading();"></asp:LinkButton>

                                                                </ItemTemplate>
                                                                <ItemStyle Width="40px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="DatePrinted" SortExpression="DatePrinted" HeaderText="Printed On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FormCode" SortExpression="FormCode" HeaderText="Form">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="LegalPartyType" SortExpression="LegalPartyType" HeaderText="Legal Party">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Cert #">
                                                                <ItemTemplate>
                                                                    <asp:HyperLink ID="hyperlnk" runat="server" Style="cursor: pointer" Text='<%# Eval("CertNum", "({0})")%>'></asp:HyperLink>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="MailStatus" SortExpression="MailStatus" HeaderText="Mail Status">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsFive" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div style="padding-left: 20px; text-align: left;" id="divAddDoc">
                                                <asp:Button ID="btnAddDocuments" runat="server" CssClass="btn btn-primary" CausesValidation="False" Text="Add Document" Style="display: none;" />
                                                <asp:Button ID="btnAddDocumentsNew" runat="server" OnClientClick="$('#ModalFileUpload').modal('show');return false;" CssClass="btn btn-primary" CausesValidation="False"
                                                    Text="Add Document" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                        <%--'^^^***CreatedBy Pooja   01/09/20 *** ^^^--%>
                                                   <%--  <asp:HiddenField ID="hdnDocId1" runat="server" Value="0" />--%>
                              <asp:Button ID="btnDeleteDoc" runat="server" Style="display: none;" OnClick="btnDeleteDoc_Click" />
                                                    <asp:GridView ID="gvwDocs" runat="server" CssClass="table dataTable table-striped" Width="100%" PageSize="20" AutoGenerateColumns="False">
                                                        <%-- <AlternatingRowStyle CssClass="altrowstyle"></AlternatingRowStyle>--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="View">
                                                                <ItemTemplate>
                                                                    <%--&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ImageButton ID="btnPrntDocs" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/zoom.png" runat="server" />--%>

                                                                    <asp:LinkButton ID="btnViewDoc" CssClass="icon ti-eye" runat="server" OnClientClick="return DisplayDocPopup(this);"></asp:LinkButton>
                                                                    <%--   <asp:LinkButton ID="btnEmailDocs" CssClass="icon ti-email" runat="server" />    &nbsp; &nbsp;--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Wrap="False" Width="40px" HorizontalAlign="Center" VerticalAlign="Middle"></ItemStyle>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateCreated" HeaderText="Date Added" SortExpression="DateCreated">
                                                                <ItemStyle HorizontalAlign="Center" Wrap="False" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="UserCode" HeaderText="User" SortExpression="UserCode">
                                                                <ItemStyle Wrap="False" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DocumentType" HeaderText="Type" SortExpression="DocumentType">
                                                                <ItemStyle Wrap="False" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DocumentDescription" HeaderText="Description" SortExpression="DocumentDescription">
                                                                <ItemStyle Wrap="True" Height="15px"></ItemStyle>
                                                            </asp:BoundField>
                                                                <%--'^^^***CreatedBy Pooja   01/09/20 *** ^^^--%>
                                                               <asp:TemplateField HeaderText="Delete">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDeleteItem" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccount(this)" />
                                                      
                                                             
                                                                </ItemTemplate>
                                                                <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate>
                                                            <b>No Items found for the specified criteria</b>
                                                        </EmptyDataTemplate>

                                                        <%--  <HeaderStyle CssClass="headerstyle"></HeaderStyle>

                                                                                            <RowStyle CssClass="rowstyle"></RowStyle>--%>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>





                                    <%--   <div style="float: left; margin-top: 30px; height: 100%;">--%>


                                    <ajaxToolkit:ModalPopupExtender ID="ModalFileUpLoadExtender1"
                                        runat="server" BehaviorID="FileUpLoad" TargetControlID="btnAddDocuments"
                                        PopupControlID="pnlFileUpLoad" BackgroundCssClass="modalBackground" DynamicServicePath="" Enabled="True" />

                                    <asp:Panel ID="pnlFileUpLoad" runat="server" CssClass="modcontainer" Style="display: none" Width="90%" Height="100px">
                                        <h1 class="panelheader">
                                            <div style="width: 100%;">
                                                <div style="text-align: center; width: 90%; float: left;">Upload Documents</div>
                                                <div style="float: right; width: 10%;">
                                                    <asp:Button ID="btnFileUpLoadCancel" runat="server" Text="Return" Width="40px" CssClass="btn btn-primary" />
                                                </div>
                                            </div>


                                        </h1>

                                        <iframe runat="server" id="frm" src="App_Controls/_Custom/LienView/Default.aspx?id=1 &jobid=1" style="width: 90%; height: 270px; background-color: White"></iframe>

                                    </asp:Panel>



                                    <%--   </div>--%>
                                </div>
                            </div>
                            <div class="tab-pane" id="exampleTabsSix" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div style="padding-left: 20px; text-align: left;" id="divWaiver">
                                                <asp:LinkButton ID="btnAddWaiver" runat="server" Text="Create New Waiver" CssClass="btn btn-primary" OnClick="btnAddWaiver_Click" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwWaivers" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyNames="JobWaiverLogId" CssClass="table dataTable table-striped">
                                                        <%--  <RowStyle CssClass="rowstyle" />
                                                                            <AlternatingRowStyle CssClass="altrowstyle" />
                                                                            <HeaderStyle CssClass="headerstyle" />--%>
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit/Print/Email ">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                          <asp:LinkButton ID="btnEditWaiver" CssClass="icon ti-write" runat="server" />
                                                                    <%--     <asp:ImageButton ID="btnEditWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                                                                        &nbsp;&nbsp;
                                                                                                          <asp:LinkButton ID="btnPrntWaiver" CssClass="icon ti-printer" runat="server" />
                                                                    <%--   <asp:ImageButton ID="btnPrntWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                                                                                        &nbsp; &nbsp;
                                                                     <asp:LinkButton ID="btnEmailWaiver" CssClass="icon ti-email" runat="server" />
                                                                    &nbsp; &nbsp;
                                                                </ItemTemplate>
                                                                <ItemStyle Width="70px" Wrap="False" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="RequestedBy" SortExpression="RequestedBY" HeaderText="User">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Created On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DatePrinted" SortExpression="DatePrinted" HeaderText="Printed On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FORMDESCRIPTION" SortExpression="FORMDESCRIPTION" HeaderText="Waiver Type">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ThroughDate" SortExpression="ThroughDate" HeaderText="Through Date">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;
                                                                                                        <asp:LinkButton ID="btnDeleteWaiver" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountWaiver(this)" />
                                                                    <%--                                                                                                        <asp:ImageButton ID="btnDeleteWaiver" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="tab-pane active" id="exampleTabsSeven" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div style="padding-left: 20px; text-align: left;" id="divStateForm">
                                                <asp:LinkButton ID="btnAddStateFrom" runat="server" Text="Create New State Form" CssClass="btn btn-primary" OnClick="btnAddStateFrom_Click" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwStateForms" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyNames="JobNoticeLogId" CssClass="table dataTable table-striped">
                                                        <RowStyle CssClass="rowstyle" />
                                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                                        <HeaderStyle CssClass="headerstyle" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit/Print">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                         <asp:LinkButton ID="btnEditSFORM" CssClass="icon ti-write" runat="server" />
                                                                    <%--<asp:ImageButton ID="btnEditSFORM" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/edit.gif" runat="server" />--%>
                                                                                                        &nbsp;
                                                                                                         <asp:LinkButton ID="btnPrntSFORM" CssClass="icon ti-printer" runat="server" />
                                                                    <%--	<asp:ImageButton ID="btnPrntSFORM" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/printer.png" runat="server" />--%>
                                                                                                        &nbsp; &nbsp;
                                                                </ItemTemplate>
                                                                <ItemStyle Width="68px" Wrap="False" />
                                                            </asp:TemplateField>

                                                            <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Created On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DatePrinted" SortExpression="DatePrinted" HeaderText="Printed On">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FormCode" SortExpression="FormCode" HeaderText="Form Type">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;
                                                                                                         <asp:LinkButton ID="btnDeleteSForm" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountStateForm(this)" />
                                                                    <%--<asp:ImageButton ID="btnDeleteSForm" CssClass="gridbutton" ImageUrl="~/app_themes/vbjuice/img/delete.gif" runat="server" OnClientClick="javascript:return confirm ( 'Are you sure want to delete Record?')" />--%>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />


                                </div>
                            </div>
                            <div class="tab-pane active" id="exampleTabsEight" role="tabpanel">
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div style="padding-left: 20px; text-align: left;" id="divmergejob">
                                            <asp:LinkButton ID="LinkButton1" runat="server" Text="Merge Job" CssClass="btn btn-primary" OnClientClick="return PopupMergeJob();" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div style="width: auto; height: auto; overflow: auto;">
                                                <asp:GridView ID="gvwMergeJobs" runat="server" Width="100%" AutoGenerateColumns="False" DataKeyNames="JobId" CssClass="table dataTable table-striped" OnRowDataBound="gvwMergeJobs_RowDataBound">
                                                    <RowStyle CssClass="rowstyle" />
                                                    <AlternatingRowStyle CssClass="altrowstyle" />
                                                    <HeaderStyle CssClass="headerstyle" />
                                                    <Columns>

                                                        <asp:BoundField DataField="JobId" SortExpression="JobId" HeaderText="JobId">
                                                            <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ParentJobId" SortExpression="ParentJobId" HeaderText="Parent JobId">
                                                            <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="JobNum" SortExpression="JobNum" HeaderText="Job Num">
                                                            <ItemStyle Height="15px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="JobName" SortExpression="JobName" HeaderText="Job Name">
                                                            <ItemStyle Height="15px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:TemplateField HeaderText="Job Address">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblJobAddress" runat="server" Text='<%# Eval("JobAdd1")%>'></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="20px" Wrap="False" />

                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="RefNum" SortExpression="RefNum" HeaderText="Cust#">
                                                            <ItemStyle Height="15px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ClientCustomer" SortExpression="ClientCustomer" HeaderText="Customer Name">
                                                            <ItemStyle Height="15px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CustId" SortExpression="CustId" HeaderText="CustId">
                                                            <ItemStyle Height="15px" Wrap="False" />
                                                        </asp:BoundField>
                                                        <%--      <asp:BoundField DataField="JobOrigBalance" SortExpression="JobOrigBalance" HeaderText="Job Original Balance">
                                                            <ItemStyle Height="15px" Wrap="False" />
                                                        </asp:BoundField>--%>
                                                        <asp:TemplateField HeaderText="Job Original Balance" ControlStyle-Width="120px" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblJobOriginalBalance" runat="server" Text='<%# Eval("JobOrigBalance", "{0:c}")%>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <%--  <asp:BoundField DataField="JobBalance" SortExpression="JobBalance" HeaderText="Job Balance">
                                                            <ItemStyle Height="15px" Wrap="False" />
                                                        </asp:BoundField>--%>
                                                        <asp:TemplateField HeaderText="Job Balance" ItemStyle-HorizontalAlign="Right">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblJobBalance" runat="server" Text='<%# Eval("JobBalance", "{0:c}") %>'></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <%--##100--%>
                            <div class="tab-pane" id="exampleTabsNine" role="tabpanel">
                                <div style="display: block">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <%-- <div class="col-md-2 col-sm-2 col-xs-2 ">                                               
                                                <asp:LinkButton ID="btnAddTXStmt" runat="server" Text="Add New Request" CssClass="btn btn-primary" OnClick="btnAddTXStmt_Click" />                                                
                                            </div>--%>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div style="width: auto; height: auto; overflow: auto;">
                                                    <asp:GridView ID="gvwTexasReq" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table dataTable table-striped" OnRowDataBound="gvwTexasReq_RowDataBound">

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Edit/Print">
                                                                <ItemTemplate>
                                                                    &nbsp;&nbsp;
                                                                                                          <asp:LinkButton ID="btnEditTReq" CssClass="icon ti-write" runat="server" />

                                                                    <asp:LinkButton ID="btnPrntTReq" CssClass="icon ti-printer" runat="server" />

                                                                    &nbsp; &nbsp;
                                                                </ItemTemplate>
                                                                <ItemStyle Width="68px" Wrap="False" />
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="DateCreated" SortExpression="DateCreated" HeaderText="Date">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="SubmittedBy" SortExpression="SubmittedBy" HeaderText="User">
                                                                <ItemStyle Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IsTX60Day" SortExpression="IsTX60Day" HeaderText="2nd Month">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IsTX90Day" SortExpression="IsTX90Day" HeaderText="3rd Month">
                                                                <ItemStyle HorizontalAlign="Center" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="AmountOwed" SortExpression="AmountOwed" HeaderText="Amount Owed">
                                                                <ItemStyle HorizontalAlign="Right" Height="15px" Wrap="False" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MonthDebtIncurred" SortExpression="MonthDebtIncurred" HeaderText="Month(s) Debt Incurred">
                                                                <ItemStyle Height="15px" Wrap="True" />
                                                            </asp:BoundField>


                                                            <asp:TemplateField HeaderText=" Delete ">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="btnDeleteTReq" CssClass="icon ti-trash" runat="server" OnClientClick="return DeleteAccountTexasRequest(this)" />

                                                                </ItemTemplate>
                                                                <ItemStyle Width="20px" Wrap="False" />

                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>

                                                    </asp:GridView>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br />
                                    <br />


                                </div>
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnMonthly" runat="server" Value="False" />
                        <asp:HiddenField ID="hdnState" runat="server" Value="False" />
                        <asp:HiddenField ID="hdnIsCancelled" runat="server" Value="False" />
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" Style="text-align: left;" HeaderText="Please Correct the Following Field(s): " />
                        <cc2:CustomValidator ID="customvalidator1" runat="server">
                        </cc2:CustomValidator>
                    </div>


                </div>
                <div class="footer">
                    <table width="100%">
                        <tr>

                            <td align="right">

                                <asp:ImageButton ID="btnFirst" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_First.png" OnClick="btnFirst_Click" />

                                <asp:ImageButton ID="btnPrevious" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_Previous.png" OnClick="btnPrevious_Click" />

                                <asp:ImageButton ID="btnNext" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_Next.png" OnClick="btnNext_Click" />

                                <asp:ImageButton ID="btnLast" runat="server" Enabled="true"
                                    ImageUrl="~/App_Themes/VbJuice/Img/resultset_Last.png" OnClick="btnLast_Click" />

                            </td>

                        </tr>
                    </table>
                </div>

            </asp:Panel>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <asp:Button ID="btnExitViewer" runat="server" CssClass="btn btn-primary" Text="Exit Viewer" /><%--<asp:LinkButton ID="btnExitViewer" runat="server" CssClass="button" Text="Exit Viewer" ></asp:LinkButton>--%>

                <asp:Button ID="btnDownLoad" Text="DownLoad" CssClass="btn btn-primary" runat="server" /><%--<asp:LinkButton ID="btnDownLoad" Text="DownLoad" CssClass="button" runat="server"  />--%>

                <br />
                <br />

                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
            </div>
        </asp:View>
    </asp:MultiView>
    <div id="loadingImageDiv">
        </div>

    <asp:HiddenField ID="hdnWaiverId" runat="server" Value="0" />
    <asp:Button ID="btnDeleteWaiverId" runat="server" Style="display: none;" OnClick="btnDeleteWaiverId_Click" />
    <asp:HiddenField ID="hdnStateFormId" runat="server" Value="0" />
    <asp:Button ID="btnStateFormId" runat="server" Style="display: none;" OnClick="btnStateFormId_Click" />
    <asp:HiddenField ID="hdnTXReqId" runat="server" Value="0" />
    <asp:Button ID="btnTXReqId" runat="server" Style="display: none;" OnClick="btnTXReqId_Click" />
    <asp:HiddenField ID="hdnTexasRequest" runat="server" Value="0" />
    <asp:Button ID="btnTexasRequest" runat="server" Style="display: none;" OnClick="btnTexasRequest_Click" />
    <asp:HiddenField ID="hdnInvoiceid" runat="server" Value="0" />
    <asp:Button ID="btnInvoiceid" runat="server" Style="display: none;" OnClick="btnInvoiceid_Click" />
    <div class="modal fade modal-primary" id="ModalFileUpload" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
        tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-center">
            <div class="modal-content model-size">
                <div class="modal-header">
                   <%-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">--%>
                         <button type="button" class="close" OnClick ="javascript:Clear()" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">UPLOAD DOCUMENT</h4>
                </div>
                <iframe id="iframe1" src="App_Controls/_Custom/LienView/Default.aspx?id=1 &jobid=1" style="height: 540px; width: 550px; border: 0px;"></iframe>
                <div class="modal-footer">
                 <%--   <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">--%>
                    <button type="button" class="btn btn-default margin-0" OnClick ="javascript:Clear()" aria-label="Close">
                        <span aria-hidden="true">CLOSE</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade modal-primary" id="ModalJobNoteDetail" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
        <div class="modal-dialog modal-center">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose">
                        <span aria-hidden="true">×</span>
                    </button>
                    <h4 class="modal-title">Note Detail</h4>

                </div>
                <div class="modal-body">
                    <%--  <div id="modcontainer" style="MARGIN: 0px  0px 0px 0px; width: 100%;">--%>

                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12 col-sm-12 col-xs-12 col-sm-12 col-xs-12">
                                    <asp:TextBox ID="txtNotes" runat="server" Height="402px" Width="100%" TextMode="MultiLine"
                                        CssClass="textbox" ReadOnly="True"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                    </div>

                    <%-- </div>--%>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnBatchJobNoteId" runat="server" />
    <asp:HiddenField ID="hdnNoteType" runat="server" />
     <asp:HiddenField ID="hdnUseJobViewMenu" runat="server" />
       <asp:HiddenField ID="hdnIsClientViewJob" runat="server" />
    <asp:Button ID="btnNoteDetail" runat="server" OnClick="btnNoteDetail_Click" Style="display: none;" />
</div>
<div class="modal fade modal-primary example-modal-lg" id="ModaldivAdditionalInfo" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">ADDITIONAL INFO</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">APN Num:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtAPNNumber" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">PO Num:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtPONumber" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Bond Num:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtBondNum" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Permit Num:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtpermitNum" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Special Inst:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtSpecialInst" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Legal Desc:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtLegalDesc" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Equip Rate (Rental Only):</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtEquiprate" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Equip Desc (Rental Only):</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <cc2:DataEntryBox ID="txtEquipDesc" runat="server" CssClass="form-control" DataType="Any"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSaveAdditionalInfo" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSaveAdditionalInfo_Click" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>

            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-primary example-modal-lg" id="ModaldivCustomer" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">CUSTOMER</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable ">Customer Name:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8 ">
                            <cc2:DataEntryBox ID="txtCustomerName" runat="server" CssClass="form-control" BackColor="White" DataType="Any" ReadOnly="true"
                                ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value=""
                                EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                            <asp:HiddenField ID="hdnCustId" runat="server" Value="" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable ">Contact:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8 ">
                            <cc2:DataEntryBox ID="txtCustomerContact" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable ">Address1:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8 ">
                            <cc2:DataEntryBox ID="txtCustAdd1" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any"
                                ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox"
                                Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName=""
                                ValidationExpression=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable ">Address2:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8 ">
                            <cc2:DataEntryBox ID="txtCustAdd2" runat="server" CssClass="form-control" ReadOnly="True" BackColor="White" DataType="Any" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="JobNumber" Value="" EnableClientSideScript="False" FormatMask="" FormatString="" FriendlyName="" ValidationExpression=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">City:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span style="display: flex;">
                                <cc2:DataEntryBox ID="txtCustCity" runat="server" ReadOnly="True" CssClass="form-control" DataType="Any"
                                    ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                    FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">State/Zip:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span style="display: flex;">
                                <cc2:DataEntryBox ID="txtCustState" runat="server" CssClass="form-control" Style="text-align: center; width: 70px;"
                                    DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                    FormatString="" FriendlyName="" IsRequired="False" IsValid="False" ValidationExpression="" ReadOnly="True"
                                    Value=""></cc2:DataEntryBox>
                                &nbsp;
                                                                                  <cc2:DataEntryBox ID="txtCustZip" ReadOnly="True" runat="server" Width="130px" CssClass="form-control " DataType="Any"
                                                                                      ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                                                                      FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Email:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <cc2:DataEntryBox ID="txtCustEmail" runat="server" CssClass="form-control" DataType="Any" ReadOnly="True"
                                ErrorMessage="" EnableClientSideScript="False" FormatMask="" FormatString=""
                                FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression="" Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Phone:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <span style="display: flex;">
                                <cc2:DataEntryBox ID="txtCustPhone" runat="server" Width="135px" CssClass="form-control" Style="text-align: center"
                                    DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" ReadOnly="True"
                                    FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                    Value=""></cc2:DataEntryBox>
                            </span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Fax:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8">
                            <cc2:DataEntryBox ID="txtCustFax" runat="server" Width="135px" CssClass="form-control" Style="text-align: center"
                                DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask="" ReadOnly="True"
                                FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                Value=""></cc2:DataEntryBox>
                        </div>
                    </div>
                    <%--<div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Credit Limit:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <cc2:DataEntryBox ID="txtCreditLimit" runat="server" CssClass="form-control" ReadOnly="True"
                                DataType="Money" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control"
                                RequiredCSS="requiredtextbox" Tag="EstBalance" Value="0.00" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}"
                                FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$">$0.00</cc2:DataEntryBox>
                        </div>
                    </div>--%>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Credit Rating:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <cc2:DataEntryBox ID="txtCreditrating" runat="server" CssClass="form-control" Width="135px" ReadOnly="True"
                                DataType="Any" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control"
                                RequiredCSS="requiredtextbox" EnableClientSideScript="False" FormatMask=""
                                FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">CCR Updated:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">

                            <cc2:DataEntryBox ID="CCRUpdated" runat="server" CssClass="form-control" Width="135px" ReadOnly="True"
                                DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                NormalCSS="form-control" RequiredCSS="requiredtextbox" Value="12:00:00 AM" EnableClientSideScript="False"
                                FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName=""
                                ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Oldest Open Invoice:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <cc2:DataEntryBox ID="txtOldestOpenInvoice" runat="server" CssClass="form-control" Width="135px" ReadOnly="True"
                                DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True"
                                NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="DateAssigned" Value="12:00:00 AM" EnableClientSideScript="False"
                                FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName=""
                                ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Last Payment Date:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <cc2:DataEntryBox ID="txtLastPaymentDate" runat="server" CssClass="form-control" Width="135px" ReadOnly="True"
                                DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control"
                                RequiredCSS="requiredtextbox" Tag="DateAssigned" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}"
                                FriendlyName=""
                                ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Do Not Notice Box:</label>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="checkbox-custom checkbox-default">
                                <asp:CheckBox ID="chkDonotNotice" runat="server" Enabled="False" Text=" "></asp:CheckBox>
                            </div>
                            <%-- <cc2:DataEntryBox ID="txtDonotNotice" runat="server" Width="135px" CssClass="form-control" ReadOnly="True" Style="text-align: center"
                                                DataType="Any" ErrorMessage="" EnableClientSideScript="False" FormatMask=""
                                                FormatString="" FriendlyName="" IsRequired="False" IsValid="True" ValidationExpression=""
                                                Value=""></cc2:DataEntryBox>--%>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Total Owed:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <cc2:DataEntryBox ID="txtTotalOwed" runat="server" CssClass="form-control" ReadOnly="True" Width="135px"
                                DataType="Money" BackColor="White" ErrorMessage="" IsRequired="False" IsValid="True" NormalCSS="form-control"
                                RequiredCSS="requiredtextbox" Tag="EstBalance" Value="0.00" EnableClientSideScript="False" FormatMask="" FormatString="{0:d3}"
                                FriendlyName="" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$">$0.00</cc2:DataEntryBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--   <asp:Button ID="btnSaveCustomer" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSaveCustomer_Click" />--%>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary example-modal-lg" id="ModaldivRequestNoticeTX" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center" style="width: 800px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">NOTICE REQUEST</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group" style="display: none;">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Service Type:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">

                            <div class="btn-group" style="text-align: left;" align="left">
                                <%--##1--%>
                                <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="width: 328px; text-align: left;" align="left"
                                    data-toggle="dropdown" aria-expanded="false" id="btnServiceTypeTX" tabindex="-1">
                                    --Select Service Type--
                               <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlServiceTypeTX" style="overflow-y: auto;">
                                </ul>
                            </div>
                            <asp:HiddenField ID="HiddenField6" runat="server" Value="2" />
                        </div>
                    </div>
                    <div class="form-group divnoticetype">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Notice Type:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7" style="text-align: left;">
                            <div class="example-wrap" style="margin-bottom: 0;">
                                <div class="radio-custom radio-default" style="display: flex; padding-left: 5px;">
                                    <asp:RadioButton ID="IsTX60Day" runat="server" Text=" 2nd Month" GroupName="NoticeType"
                                        CssClass="row-label col-md-4 col-sm-4 col-xs-4" Checked="True" Style="text-align: left;"></asp:RadioButton>
                                    <asp:RadioButton ID="IsTX90Day" runat="server" Text=" 3rd Month" GroupName="NoticeType"
                                        CssClass="row-label col-md-4 col-sm-4 col-xs-4" Style="text-align: left;"></asp:RadioButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Amount Owed:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <cc2:DataEntryBox ID="AmountOwed" runat="server" DataType="Money" CssClass="form-control"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Month/Year Work Performed:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <cc2:DataEntryBox ID="MonthDebtIncurred" runat="server" DataType="Any" MaxLength="50" CssClass="form-control"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">

                            <div style="width: auto; max-height: 250px; overflow: auto;">
                                <asp:Repeater ID="rptRentalInvoiceNoticeRequest" runat="server" OnItemDataBound="rptRentalInvoice_ItemDataBound">
                                    <HeaderTemplate>
                                        <table cellspacing="0" rules="all" border="1" style="font-family: Arial; width: 100%; font-size: 12px;">
                                            <tr style="background-color: #add8e6; height: 40px;">
                                                <th scope="col" style="width: 25px;"></th>
                                                <th scope="col" style="width: 80px; text-align: center; font-weight: bold; font-family: Arial;">Invoice #
                                                </th>
                                                <th scope="col" style="width: 80px; font-weight: bold; text-align: center; font-family: Arial;">Seq #
                                                </th>
                                                <th scope="col" style="width: 200px; font-weight: bold; text-align: center; font-family: Arial;">Invoice Date
                                                </th>
                                                <th scope="col" style="width: 80px; font-weight: bold; text-align: center; font-family: Arial;">Status
                                                </th>
                                                <th scope="col" style="width: 120px; font-weight: bold; text-align: center; font-family: Arial;">Original Balance
                                                </th>
                                                <th scope="col" style="width: 120px; font-weight: bold; text-align: center; font-family: Arial;">Current Balance
                                                </th>
                                                <th scope="col" style="width: 50px; font-weight: bold; text-align: center; font-family: Arial;">Date Updated</th>
                                                <th scope="col" style="width: 50px; font-weight: bold; text-align: center; font-family: Arial;">TX Noticed?</th>
                                                <th scope="col" style="width: 50px; font-weight: bold; text-align: center; font-family: Arial;"></th>
                                                <th scope="col" style="width: 0.5px;"></th>
                                            </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr style="background-color: #f0f8ff; height: 40px;">
                                            <td style="font-weight: bold;">
                                                <a href="JavaScript:divexpandcollapse('divNR<%# Eval("RANum ")%>');">
                                                    <img id='imgdivNR<%# Eval("RANum ")%>' width="9px" border="0" src="images/plus.png" alt="plus" />
                                                </a>
                                            </td>
                                            <td style="text-align: left; font-weight: bold;">
                                                <asp:Label ID="lblInvoiceNum" runat="server" Text='<%# Eval("RANum") %>' />
                                                <asp:HiddenField ID="hdnBatchJobrentalId" runat="server" Value='<%# Eval("RANum ") %>' />
                                            </td>
                                            <td style="font-weight: bold;">
                                                <asp:Label ID="lblSeqNum" runat="server" Text='<%# Eval("BranchNum")%>' />
                                            </td>
                                            <td style="font-weight: bold;">
                                                <asp:Label ID="lblInvoiceDate" runat="server" />
                                            </td>
                                            <td style="font-weight: bold;">
                                                <asp:Label ID="lblClientStatus" runat="server" Text='<%# Eval("ClientStatus") %>' />
                                            </td>
                                            <td style="text-align: right; font-weight: bold;">
                                                <asp:Label ID="lblOriginalBalance" runat="server" />
                                            </td>
                                            <td style="text-align: right; font-weight: bold;">
                                                <asp:Label ID="lblCurrentBalance" runat="server" />
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <asp:Repeater ID="rptRentalInvoiceDetail" runat="server" OnItemDataBound="rptRentalInvoiceDetail_ItemDataBound">
                                                    <ItemTemplate>

                                                        <tr id='divNR<%# Eval("InvoiceNum")%>' class='divNR<%# Eval("InvoiceNum")%>' style="display: none; height: 40px;">
                                                            <td></td>
                                                            <td style="text-align: center;">
                                                                <asp:Label ID="lblContractSeq" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblSeqNum" runat="server" Text='<%# Eval("SeqNo")%>' />
                                                 Additional Service Request           </td>
                                                            <td>
                                                                <asp:Label ID="lblInvoiceDate" runat="server" Text='<%# Eval("InvoiceDate") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOriginalBalance" runat="server" Text='<%# Eval("OriginalBalance")%>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblCurrentBalance" runat="server" Text='<%# Eval("CurrentBalance")%>' />
                                                                <div class="wrapperDivHidden">
                                                                    <asp:HiddenField ID="hdnCurrentBalance" runat="server" Value='<%# Eval("CurrentBalance")%>' />
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <%--<asp:Label ID="lblNSF" runat="server" Text='<%# Eval("NSFFlag") %>' />--%>
                                                                <asp:Label ID="lblDateUpdated" runat="server" Text='<%# Eval("DateUpdated") %>' />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblNoticed" runat="server" Text='<%# Eval("IsNoticed") %>' />
                                                            </td>
                                                            <td>

                                                                <div class="checkbox-custom checkbox-default" style="text-align: center;">
                                                                    <asp:CheckBox ID="chkInvoice" runat="server" Text=" " OnClick='<%# "CalculatedTotalOwed(this);"%>' />
                                                                </div>
                                                            </td>

                                                            <td></td>
                                                        </tr>

                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </div>
                        </div>
                    </div>
                    <div class="form-group divaccountstatement">
                        <label class="col-md-12 col-sm-12 col-xs-12 control-label align-lable" style="text-align: left;">
                            <h3>Enter Account Statement Info:</h3>
                        </label>
                        <div class="col-md-11 col-sm-11 col-xs-11">
                            <cc2:DataEntryBox ID="StmtofAccts" runat="server" DataType="Any" Height="150px" tag="StmtofAccts"
                                TextMode="MultiLine" CssClass="form-control"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <cc2:CustomValidator ID="CustomValidator2" Display="Static" runat="server"></cc2:CustomValidator>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSaveTXRequest" runat="server" Text="SAVE" CssClass="btn btn-primary btnSaveTXRequest" OnClick="btnSaveTXRequest_Click" />

                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>

            </div>
        </div>
    </div>
</div>
<%--<div class="modal fade modal-primary example-modal-lg" id="ModaldivRequestNoticeTXW" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">NOTICE REQUEST</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Amount Owed / Est. Balance:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                            <cc2:DataEntryBox ID="AmountOwedNotice" runat="server" DataType="Money" CssClass="form-control"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">First Furnished:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                            <cc2:DataEntryBox ID="FirstFurnishedNotice" runat="server" CssClass="form-control datepicker" DataType="DateValue" Width="115px"
                                Style="text-align: center" BackColor="White" ErrorMessage="" NormalCSS="form-control"
                                RequiredCSS="requiredtextbox" Tag="EndDate" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask=""
                                FormatString="{0:dd/MM/yyyy}" FriendlyName=""
                                ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>


                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Last Furnished:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                            <cc2:DataEntryBox ID="LastFurnishedNotice" runat="server" CssClass="form-control datepicker" Width="115px"
                                DataType="DateValue" Style="text-align: center" BackColor="White" ErrorMessage="" NormalCSS="form-control" RequiredCSS="requiredtextbox" Tag="DateAssigned" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask="" FormatString="{0:dd/MM/yyyy}" FriendlyName="" ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>

                        </div>
                    </div>
                   <div class="form-group">
                        <label class="col-md-12 col-sm-12 col-xs-12 control-label align-lable" style="text-align: left;">
                            <h3>Enter Account Statement Info:</h3>
                        </label>
                        <div class="col-md-11">
                            <cc2:DataEntryBox ID="AccountStatementNotice" runat="server" DataType="Any" Height="150px" tag="StmtofAccts"
                                TextMode="MultiLine" CssClass="form-control"></cc2:DataEntryBox>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSaveNoticeRequest" runat="server" Text="SAVE" CssClass="btn btn-primary" OnClick="btnSaveNoticeRequest_Click" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>

            </div>
        </div>
    </div>
</div>--%>
<div class="modal fade modal-primary example-modal-lg" id="ModaldivRequestNoticeTN" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">NOTICE REQUEST</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group" style="display: none;">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Service Type:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">
                            <div class="btn-group" style="text-align: left;" align="left">
                                <%--##1--%>
                                <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="width: 328px; text-align: left;" align="left"
                                    data-toggle="dropdown" aria-expanded="false" id="btnServiceTypeTN" tabindex="-1">
                                    --Select Service Type--
                                                     <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlServiceTypeTN" style="overflow-y: auto;">
                                </ul>
                            </div>
                            <asp:HiddenField ID="HiddenField5" runat="server" Value="2" />
                        </div>
                    </div>
                    <div class="form-group">


                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Amount Owed :</label>
                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                            <cc2:DataEntryBox ID="TNAmountOwed" runat="server" DataType="Money" CssClass="form-control"></cc2:DataEntryBox>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 col-sm-5 col-xs-5 control-label align-lable">Last Date Work Performed :</label>
                        <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                            <cc2:DataEntryBox ID="TNLastDateWorkPerformed" runat="server" CssClass="form-control datepicker" DataType="DateValue"
                                BackColor="White" ErrorMessage="" NormalCSS="form-control"
                                RequiredCSS="requiredtextbox" Tag="EndDate" Value="12:00:00 AM" EnableClientSideScript="False" FormatMask=""
                                FormatString="{0:dd/MM/yyyy}" FriendlyName=""
                                ValidationExpression="^(?=\d)^(?:(?!(?:10\D(?:0?[5-9]|1[0-4])\D(?:1582))|(?:0?9\D(?:0?[3-9]|1[0-3])\D(?:1752)))((?:0?[13578]|1[02])|(?:0?[469]|11)(?!\/31)(?!-31)(?!\.31)|(?:0?2(?=.?(?:(?:29.(?!000[04]|(?:(?:1[^0-6]|[2468][^048]|[3579][^26])00))(?:(?:(?:\d\d)(?:[02468][048]|[13579][26])(?!\x20BC))|(?:00(?:42|3[0369]|2[147]|1[258]|09)\x20BC))))))|(?:0?2(?=.(?:(?:\d\D)|(?:[01]\d)|(?:2[0-8])))))([-.\/])(0?[1-9]|[12]\d|3[01])\2(?!0000)((?=(?:00(?:4[0-5]|[0-3]?\d)\x20BC)|(?:\d{4}(?!\x20BC)))\d{4}(?:\x20BC)?)(?:$|(?=\x20\d)\x20))?((?:(?:0?[1-9]|1[012])(?::[0-5]\d){0,2}(?:\x20[aApP][mM]))|(?:[01]\d|2[0-3])(?::[0-5]\d){1,2})?$"></cc2:DataEntryBox>


                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnSaveTNRequest" runat="server" Text="SAVE" CssClass="btn btn-primary btnSaveTNRequest" OnClick="btnSaveTNRequest_Click" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-primary example-modal-lg" id="ModelRequestverification" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">

                <h3 class="modal-title">THANK YOU</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group" id="BeforeAssign">
                        <h3>Your Verification Request Has Been Received</h3>
                    </div>
                    <div class="form-group" id="AfterAssign">
                        <h3>Your Notice Request has been submitted and will be included in our next print run</h3>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">
                <asp:Button ID="btnSaveNoticeRequest" runat="server" Text="OK" CssClass="btn btn-primary" OnClick="btnSaveNoticeRequest_Click" Style="display: none" />
                <button type="button" class="btn btn-primary margin-0" onclick="modalsuccessHide();" data-dismiss="modal" aria-label="Close" style="height: 40px; width: 100px;">
                    <span aria-hidden="true">OK</span>
                </button>

            </div>
        </div>
    </div>
</div>
<div class="modal fade modal-primary example-modal-lg" id="ModalJobMerge" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">JOB MERGE</h4>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-sm-12 col-xs-12" style="text-align: left;">
                            <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Enter Job#:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5 col-sm-5 col-xs-5" style="text-align: left;">
                            <%--<cc2:DataEntryBox ID="txtMergeJobId1" runat="server" CssClass="form-control"></cc2:DataEntryBox>--%>
                            <%-- <asp:TextBox ID="txtMergeJobId" runat="server" CssClass="form-control" Text=""></asp:TextBox>--%>
                            <input type="text" id="txtMergeJobId" name="txtMergeJobId" />
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 " style="text-align: left;">
                            <asp:Button ID="btnJobMergeFind" runat="server" Text="Find" CssClass="btn btn-primary" OnClientClick="return FindMergeJob()" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Job Name:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">
                            <asp:Label ID="lblJobmergeName" runat="server" Text="" Font-Bold="false"></asp:Label>


                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Job Address:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">
                            <asp:Label ID="lblJobmergeAddress" runat="server" Text="" Font-Bold="false"></asp:Label>


                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Cust#:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">
                            <asp:Label ID="lblJobMergeCustNum" runat="server" Text="" Font-Bold="false"></asp:Label>


                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 col-sm-4 col-xs-4 control-label align-lable align-lable">Cust Name:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8 col-sm-8 col-xs-8" style="text-align: left;">
                            <asp:Label ID="lblJobmergeCustName" runat="server" Text="" Font-Bold="false"></asp:Label>


                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <asp:Button ID="btnMerge" runat="server" Text="Merge" CssClass="btn btn-primary" OnClientClick="return MergeJob();" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>
                <asp:Button ID="btnMergeJob" runat="server" Style="display: none;" OnClick="btnMergeJob_Click" />

            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-primary example-modal-lg" id="ModalServiceType" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static" data-keyboard="false" data-target=".example-modal-lg">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title">SERVICE TYPE</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4 control-label align-lable">Service Type:</label>
                        <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left;">

                            <div class="example-wrap" style="margin-bottom: 0; white-space: nowrap; display: none;">
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="PrelimBox" runat="server" Text=" Verify Job Data & Send Notice"
                                        GroupName="JobType" Checked="True"></asp:RadioButton>
                                </div>
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="PrelimASIS" runat="server" Text=" Send Notice With Data Provided"
                                        GroupName="JobType"></asp:RadioButton>
                                </div>
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="VerifyJob" runat="server" Text=" Verify Job Data Only - No Notice Sent"
                                        GroupName="JobType"></asp:RadioButton>
                                </div>
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="VerifyJobASIS" runat="server" Text=" Store Job Data As Provided - No Notice Sent"
                                        GroupName="JobType"></asp:RadioButton>
                                </div>
                                <button type="button" class="btn btn-outline btn-default" style="display: none;" id="exampleSuccessMessage"
                                    data-plugin="sweetalert" data-title="Successfully Submitted!" data-text="Your New Job Has Been Successfully Submitted!"
                                    data-type="success">
                                    Success message</button>
                            </div>

                            <div class="btn-group" style="text-align: left;" align="left">
                                <%--##1--%>
                                <button type="button" class="btn btn-default dropdown-toggle ddlCustom" style="width: 328px; text-align: left;" align="left"
                                    data-toggle="dropdown" aria-expanded="false" id="btnServiceType" tabindex="-1">
                                    --Select Service Type--
                     
                                                     <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlServiceType" style="overflow-y: auto;">
                                    <%-- <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimBox" onclick="setSelectedSserviceType(1,'Verify Job Data & Send Notice')">Verify Job Data & Send Notice</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPrelimASIS" onclick="setSelectedSserviceType(1,'Send Notice With Data Provided')">Send Notice With Data Provided</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJob" onclick="setSelectedSserviceType(1,'Verify Job Data Only - No Notice Sent')">Verify Job Data Only - No Notice Sent</a></li>
                                                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liVerifyJobASIS" onclick="setSelectedSserviceType(1,'Store Job Data As Provided - No Notice Sent')">Store Job Data As Provided - No Notice Sent</a></li>--%>
                                </ul>
                            </div>
                            <asp:HiddenField ID="hdnServiceTypeId" runat="server" Value="1" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">
                <asp:Button ID="Button1" runat="server" Text="OK" CssClass="btn btn-primary" OnClick="btnSaveNoticeRequest_Click" Style="display: none" />
                <button type="button" class="btn btn-primary margin-0" onclick="modalsuccessHideServiceType();" aria-label="Close" style="height: 40px; width: 100px; display: none;">
                    <span aria-hidden="true">OK</span>
                </button>

                <asp:Button ID="btnOK" runat="server" Text="OK" CssClass="btn btn-primary" OnClick="btnOK_Click" OnClientClick="setLoadingImage()" />
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">CLOSE</span>
                </button>
            </div>
        </div>
    </div>
</div>

<div id="loading" style="visibility: hidden;">
    <div class="TransparentGrayBackground"></div>


    <asp:Image ID="ajaxLoadNotificationImage" CssClass="loading-image" runat="server" ImageUrl="../../global/vendor/slick-carousel/images/ajax-loader.gif" AlternateText="[image]" />

</div>
<div class="modal fade modal-primary" id="divModalEmail" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose1" onclick="RemoveBackdrop();">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Send Email</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4">To:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtToList" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4"></div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <label style="color: red; text-align: left;">Note:- Multiple email ids can be added separated by semicolon</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4">Subject:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" Text="New Waiver Document"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4 col-sm-4 col-xs-4">Message:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtBody" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12 col-sm-12 col-xs-12">
                                <asp:Label ID="Label1" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--<asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Send" Style="display: none;" />--%>
                <button type="button" class="btn btn-primary" id="btnSaves1" onclick="SendMail();">
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="RemoveBackdrop();">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:Button ID="btnEmailsend" runat="server" OnClick="btnEmailsend_Click" Style="display: none;" />

<div class="modal fade modal-primary example-modal-lg" id="ModalDocsView" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return CloseEmailLoadPage();">
                    <span aria-hidden="true">×</span>

                </button>
                <asp:Button ID="hdnbtnclose" CssClass="btn btn-primary" runat="server" Text="" Style="display: none;" />
                <h3 class="modal-title">Document Created !!</h3>
            </div>
            <div class="modal-body">
                <div class="form-horizontal">
                    <h3 class="modal-title">What Would You Like To Do With This Document? </h3>
                </div>
            </div>
            <div class="modal-footer" style="text-align: center">
                <asp:Button ID="btnPrntDocs" Text="View Document" CssClass="btn btn-primary" runat="server"></asp:Button>
                <asp:Button ID="btnEmailDocs" runat="server" Text="Email Document" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hdnDocId" runat="server" />
<asp:HiddenField ID="hdnEmailflag" runat="server" />

<div class="modal fade modal-primary" id="divModalRequestService" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content" style="width:961px;margin-left:-180px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="RemoveBackdrop();">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Additional Service Request</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4">Service Type:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtServiceType" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4"></div>
                            <div class="col-md-8  col-sm-8 col-xs-8" style="text-align: left;">
                                <label style="color: red; text-align: left;">Mechanics Lien, Bond Claim, etc. </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4">Amount Owed:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtAmountOwed" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4">Subject:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtreqsubject" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4 col-sm-4 col-xs-4 col-sm-4 col-xs-4">Message:</div>
                            <div class="col-md-8  col-sm-8 col-xs-8  col-sm-8 col-xs-8">
                                <asp:TextBox ID="txtMessage" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <asp:Label ID="lblreqerror" runat="server" CssClass="normalred" Style="font-weight:500;margin-left:-115px;"></asp:Label>
                            </div>
                        </div>

                          

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                  <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="col-md-9 col-sm-9 col-xs-9" style="text-align:justify;">
                            
                                   <asp:CheckBox ID="ChkSendRequestService" class="checkbox-sm" style="padding:0" runat="server" onclick="IsSendRequestServiceChecked();" ></asp:CheckBox>&nbsp;
                                       <b>Client acknowledges personal knowledge of the requested claim and assumes all risks associated with incomplete or rush requests. Rush requests (made with less than 15 days prior to the recording deadline) will be charged a rush fee and may not be filed timely.</b>
                          
                            </div>
                           
                        <div class="col-md-3 col-sm-3 col-xs-3">
                <button type="button" class="btn btn-primary" id="btnSendRequestService"   onclick="SendServiceRequestMail();" disabled>
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="RemoveBackdrop();">Close</button>
            </div>
                     

                      </div>
                 </div>
                </div>
        </div>
    </div>
</div>

<%--Recension Letter Modal--%>
<div class="modal fade modal-primary example-modal-lg" id="ModalRecensionLetterSubmit" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
    tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="RemoveBackdrop();">
                    <span aria-hidden="true">×</span>
                </button>
                <h3 class="modal-title">Recension Letter Created</h3>
            </div>
            <div class="modal-body">
                <div id ="RecensionLetterModaldiv"></div>
            </div>
            <div class="modal-footer" style="text-align: center">
            <asp:Button ID="btnEmail" runat="server" Text="Email Letter" CssClass="btn btn-primary" OnClick="btnEmailRecensionLetter_Click" />
                <asp:Button ID="btnPrint" runat="server" Text="Print Letter" CssClass="btn btn-primary" OnClick="btnPrintRecensionLetter_Click" />
               <button type="button" class="btn btn-primary"  data-dismiss="modal" onclick="RemoveBackdrop();">Cancel</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade modal-primary" id="divModalEmailRecensionLetter" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="RemoveBackdrop();">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Send Email</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                          <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12 col-sm-12 col-xs-12" style="text-align: left;">
                            <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-md-4">To:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtToListForRecensionLetter" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <label style="color: red; text-align: left;">Note:- Multiple email ids can be added separated by semicolon</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Subject:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtRecensionLetterSubject" runat="server" CssClass="form-control" Text="New Waiver Document"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Message:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtRecensionLetterBody" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <asp:Label ID="Label2" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="SendRecensionLetterMail();">
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="return fadeout();">Close</button>
            </div>
        </div>
    </div>
</div>
<asp:HiddenField ID="hdnLegalPartyId" runat="server" />
<asp:HiddenField ID="hdnJobLegalPartiesTypeCode" runat="server" />
<asp:Button ID="btnCreateAnotherRecensionLetter" runat="server" OnClick="btnCreateAnotherRecensionLetter_Click" Style="display: none;" />