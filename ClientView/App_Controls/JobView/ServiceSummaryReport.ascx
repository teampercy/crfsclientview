﻿<%--<%@ Control Language="C#" AutoEventWireup="true" CodeFile="ServiceSummaryReport.ascx.cs" Inherits="App_Controls_JobView_ServiceSummaryReport" %>--%>
<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>


<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('RentalView', 'Reports', 'Service Summary');
        MainMenuToggle('liRentalViewReport');
        SubMenuToggle('liServiceSummaryJobView');

        return false;
    }



</script>

<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Calendar1.Value = Today
            Me.Calendar2.Value = Today

            If IsNothing(Me.Page.Request.QueryString("rpt")) = False Then
                Me.DownLoadReport(Me.Page.Request.QueryString("rpt"))
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_CLIENTVIEW_GetServiceSummaryReport
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetServiceSummaryJV
        With MYSPROC
            .USERID = Me.CurrentUser.Id
            .JOBSTATE = Me.txtJobState.Text
            .DOCUMENTFROM = Me.Calendar1.Value
            .DOCUMENTTO = Me.Calendar2.Value
        End With
        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetServiceSummaryReportJV(MYSPROC, False))
        Me.btnDownLoad.Visible = False
        Me.plcReportViewer.Visible = True
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.btnDownLoad.Visible = True
            Me.plcReportViewer.Visible = False
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

        Me.plcReportViewer.Controls.Clear()
        Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
        Me.MultiView1.SetActiveView(Me.View2)
    End Sub



    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        Me.btnDownLoad.Visible = False

    End Sub

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.DownLoadReport(Me.Session("spreadsheet"))

    End Sub



</script>


<div id="modcontainer" style="margin: 10px; width: 100%;">
    <h1 class="panelheader">Service Summary Report</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">

                 
                <div class="form-horizontal">

                    

                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Job State:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5 ">
                            <asp:TextBox runat="server" ID="txtJobState" CssClass="form-control" Width="11%" MaxLength="2" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Service Date:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5 ">
                            <div style="float: left;">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="Calendar1" Value="TODAY" IsReadOnly="false" runat="server" />
                                </span>
                            </div>

                            <div>
                                <label class="control-label align-lable" style="float: left; padding-left: 20px; padding-right: 20px;">Thru:</label>
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="Calendar2" Value="TODAY" IsReadOnly="false" runat="server" />
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="footer">
                <div class="form-group">

                    <div class="col-md-12" style="text-align: right;">
                        <asp:UpdatePanel ID="updPan1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSubmit"
                                    CssClass="btn btn-primary"
                                    runat="server"
                                    Text="Create Report" OnClick="btnSubmit_Click" />&nbsp;
                                                            <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                                                                <ProgressTemplate>
                                                                    <div class="TransparentGrayBackground"></div>
                                                                    <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                                                        <div class="PageUpdateProgress">
                                                                            <asp:Image ID="ajaxLoadNotificationImage"
                                                                                runat="server"
                                                                                ImageUrl="~/images/ajax-loader.gif"
                                                                                AlternateText="[image]" />
                                                                            &nbsp;Please Wait...
                                                                        </div>
                                                                    </asp:Panel>
                                                                    <ajaxToolkit:AlwaysVisibleControlExtender
                                                                        ID="AlwaysVisibleControlExtender1"
                                                                        runat="server"
                                                                        TargetControlID="alwaysVisibleAjaxPanel"
                                                                        HorizontalSide="Center"
                                                                        HorizontalOffset="150"
                                                                        VerticalSide="Middle"
                                                                        VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                                                                </ProgressTemplate>
                                                            </asp:UpdateProgress>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>

            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">
                        <div class="col-md-12" style="text-align: left;">
                            <asp:Button ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Selection" OnClick="Linkbutton2_Click"></asp:Button>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <div style="width: auto; height: auto; overflow: auto;">
                                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:Button ID="btnDownLoad" Text="DownLoad SpreadSheet" runat="server" CssClass="btn btn-primary" OnClick="btnDownLoad_Click" />
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
    </asp:MultiView>
</div>