<%@ Control Language="VB" AutoEventWireup="false" CodeFile="JobEdit.ascx.vb" Inherits="App_Controls_LienView_JobEdit" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script type="text/javascript">
    function OpenJobNewModal() {
        //if ($('#' + '<%= IsClientViewManager.ClientID %>').val() == 'True') {
        BindJobStatus();
        BindDesk();
        BindState();
        BindPropertyType();
        usrSpecsCtrls();
        SetDatePicker();

        $('#editable-selectJobStatus').val($('#liJobStatus_' + $('#' + '<%= hdnDrpJobStatus.ClientID%>').val()).text());

        $('#editable-selectDesk').val($('#liDesk_' + $('#' + '<%= hdnDrpDesk.ClientID%>').val()).text());

        $('#editable-selectState').val($('#' + '<%= hdnDrpState.ClientID%>').val());

        //var value = $('#' + '<%= hdnDrpPropertyType.ClientID%>').val();
      //  alert("load" + $('#' + '<%= hdnDrpPropertyType.ClientID%>').val());
        $('#editable-selectPropertyType').val($('#' + '<%= hdnDrpPropertyType.ClientID%>').val());
        // $('#editable-selectPropertyType').val($('#liPropertyType_' + $('#' + '<%= hdnDrpPropertyType.ClientID%>').val()).text());

        $("#ModalJobNew").modal('show');
        // }
    }
    function ModalHide() {
        // alert('nodelhide');
        document.getElementById('<%=CancelButton.ClientID%>').click();
        //DisplayTablelayout();
        $("#ModalJobNew").modal('hide');
    }
    function ModalHide1() {
        // alert('nodelhide');
       // document.getElementById('<%=CancelButton.ClientID%>').click();
        $("#ModalJobNew").modal('hide');
    }


    function usrSpecsCtrls() {
        //disabled when dateassigned is not null
        if (parent.getResponse() == "True") {
            $('#' + '<%= txtJobName.ClientID%>').prop('disabled', true);
            $('#' + '<%= txtJobAddress1.ClientID%>').prop('disabled', true);
            $('#' + '<%= txtJobAddress2.ClientID %>').prop('disabled', true);
            $('#' + '<%= txtJobCity.ClientID %>').prop('disabled', true);
            $('#editable-selectState').prop('disabled', true);
            $('#' + '<%= txtJobZip.ClientID %>').prop('disabled', true);
            $('#editable-selectPropertyType').prop('disabled', true);
            $('#' + '<%= EstBalance.ClientID %>').prop('disabled', true).addClass('cstmCntrl');
            $('#' + '<%=NoticeSentDate.ClientID %>' + '_txtYui').prop('disabled', true).addClass('cstmCntrl');
           
        }
        else {
            $('#' + '<%= txtJobName.ClientID %>').prop('disabled', false);
            $('#' + '<%= txtJobAddress1.ClientID %>').prop('disabled', false);
            $('#' + '<%= txtJobAddress2.ClientID %>').prop('disabled', false);
            $('#' + '<%= txtJobCity.ClientID %>').prop('disabled', false);
            $('#editable-selectState').prop('disabled', false);
            $('#' + '<%= txtJobZip.ClientID %>').prop('disabled', false);
            $('#editable-selectPropertyType').prop('disabled', true);
            $('#' + '<%= EstBalance.ClientID %>').prop('disabled', false).removeClass('cstmCntrl');
            $('#' + '<%=NoticeSentDate.ClientID %>'+'_txtYui').prop('disable', false).removeClass('cstmCntrl');
        }
      
    }

    $(document).ready(function () {
   if ($('#' + '<%= IsClientViewManager.ClientID %>').val() == 'True') {
         $('#btnJobNew').prop('disabled', false);
        }
        else {
            $('#btnJobNew').prop('disabled', true);
    }

     

        $(document).click(function (event) {
            //alert('Hello');
            if (event.target.id != "editable-selectJobStatus" && event.target.id != "editable-selectDesk" && event.target.id != "editable-selectPropertyType") {
                $(".es-list").css("display", "none");
                //  console.log(event.target.id);
            }
            //if (event.target.id = "editable-selectPropertyType") {
            //    alert("if");
            //    $("editable-selectPropertyType.es-list").css("display", "block");
            //    //  console.log(event.target.id);
            //}

        })
    });

      function disableJobEditBtn() {
        document.getElementById('<%=btnJobNew.ClientID %>').disabled = true;
    }
    
    function enableJobEditBtn() {
        document.getElementById('<%=btnJobNew.ClientID %>').disabled = false;
    }


    function binDDlist() {

    }

    function BindJobStatus() {
        $('#UlJobStatus').html();
        var x = document.getElementById('<%=DrpJobStatus.ClientID%>');
        var txt = "<select class=\"form-control\" id=\"editable-selectJobStatus\" style=\"width:280px;overflow-y: auto; \">";
        var i;
        txt = txt + '<option role="presentation"><a href="javascript:void(0)" role="menuitem" id="liJobStatus_0">--Select Job Status--</a></option>';
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                txt = txt + '<option role="menuitem" id="liJobStatus_' + x.options[i].value + '" value="' + x.options[i].value + '" JobStatusId="' + $(x.options[i]).attr('jobstatusid') + '">' + x.options[i].text + '</option>';
            }
        }

        txt = txt + "</select>";
        $('#UlJobStatus').html(txt);
        $('#editable-selectJobStatus').editableSelect();

        $('#editable-selectJobStatus').on('select.editable-select', function (e, li) {
            //  alert('hi123');
            if (li) {
                // alert(li.attr('jobstatusid'));
                $('#' + '<%= hdnDrpJobStatus.ClientID%>').val(li.attr('value'));
                $('#' + '<%= hdnDrpJobStatusID.ClientID%>').val(li.attr('jobstatusid'));
                // alert($('#' + '<%= hdnDrpJobStatusID.ClientID%>').val());
                //alert(li.attr('value'));
                //alert(li.attr('deskid'));
            }
        });
    }

    function BindDesk() {
        $('#UlDesk').html();
        var x = document.getElementById('<%=DrpDesk.ClientID%>');
        var txt = "<select class=\"form-control\" id=\"editable-selectDesk\" style=\"overflow-y: auto; \">";
        var i;
        txt = txt + '<option role="presentation"><a href="javascript:void(0)" role="menuitem" id="liDesk_0">--Select Desk--</a></option>';
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                txt = txt + '<option role="menuitem" id="liDesk_' + x.options[i].value + '" value="' + x.options[i].value + '" deskid="' + $(x.options[i]).attr('deskid') + '">' + x.options[i].text + '</option>';
            }
        }

        txt = txt + "</select>";
        $('#UlDesk').html(txt);
        $('#editable-selectDesk').editableSelect().on('select.editable-select', function (e, li) {
            if (li) {
                $('#' + '<%= hdnDrpDesk.ClientID%>').val(li.attr('value'));
                $('#' + '<%= hdnDrpDeskID.ClientID%>').val(li.attr('deskid'));
             
            }
        });
    }

    function BindState() {
        $('#UlState').html();

        var x = document.getElementById('<%=DrpState.ClientID%>');
        var txt = "<select class=\"form-control\" id=\"editable-selectState\" style=\"overflow-y: auto; \">";
        var i;
        txt = txt + '<option role="presentation"><a href="javascript:void(0)" role="menuitem" id="liState_0">--Select State--</a></option>';
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                txt = txt + '<option role="menuitem" id="liState_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
            }
        }

        txt = txt + "</select>";
        $('#UlState').html(txt);

        $('#editable-selectState').editableSelect().on('select.editable-select', function (e, li) {
            if (li) {
                $('#' + '<%= hdnDrpState.ClientID%>').val(li.attr('value'));
            }
        });

    }
    function BindPropertyType() {
       // alert("BindPropertyType");
        debugger;
        $('#UlPropertyType').html();
        $('#UlPropertyType').empty();

        var x = document.getElementById('<%=DrpPropertyType.ClientID%>');
        var txt = "<select class=\"form-control\" id=\"editable-selectPropertyType\" style=\"overflow-y: auto; \">";
        var i;
        txt = txt + '<option role="presentation"><a href="javascript:void(0)" role="menuitem" id="liPropertyType_0">--Select Property--</a></option>';
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                txt = txt + '<option role="menuitem" id="liPropertyType_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
            }
        }

        txt = txt + "</select>";
        $('#UlPropertyType').empty();
        $('#UlPropertyType').html(txt);
        //var y = document.getElementById('UlPropertyType');
        //$('#' + y).html(txt);
        //alert("hitt " + y);

        $('#editable-selectPropertyType').editableSelect().on('select.editable-select', function (e, li) {

            if (li) {

                $('#' + '<%= hdnDrpPropertyType.ClientID%>').val(li.attr('value'));
            }
        });

    }



    function noenter(e) {
        var a = 10;
        var target = (e.target || e.srcElement).id;
        if (target == "ctl33_ctl00_JobEdit1_StartDate_txtYui" ||
            target == "ctl33_ctl00_JobEdit1_EndDate_txtYui" ||
            target == "ctl33_ctl00_JobEdit1_NOCDate_txtYui" ||
            target == "ctl33_ctl00_JobEdit1_LienSent_txtYui" ||
            target == "ctl33_ctl00_JobEdit1_BondSent_txtYui") {
            a = 20;
            e.preventDefault();
        }
    }

    function SetDatePicker() {
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            showButtonPanel: true,
        });
    }

</script>
<style>
    .cstmCntrl {
        background-color: #f3f7f9 !important;
    }
</style>

<%--<script src="assets/js/editableDDL/jquery-1.12.4.min.js"></script>--%>
<link href="assets/js/editableDDL/jquery-editable-select.min.css" rel="stylesheet" />
<script type="text/javascript" src="assets/js/editableDDL/jquery-editable-select.min.js"></script>

<input type="button" runat="server" id="btnJobNew" onclick="OpenJobNewModal();" value="Job Edit" class="btn btn-primary" />
<div class="modal fade modal-primary" id="ModalJobNew" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center modal-lg">
        <div class="modal-content" style="min-width:900px;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose" onclick="ModalHide1();">
                    <span aria-hidden="true">�</span>
                </button>
                <h4 class="modal-title" style="text-align: left !important;">Edit Job Info</h4>
                <asp:HiddenField ID="BatchJobId" runat="server" />
                <asp:HiddenField ID="ItemId" runat="server" />
                <asp:HiddenField ID="IsClientViewManager" runat="server" />
                <asp:HiddenField ID="IsNoticeRequested" runat="server" />
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Job Name:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <asp:TextBox ID="txtJobName" CssClass="form-control" runat="server" Style="width: 280px;"></asp:TextBox>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">First Furnished:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="StartDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                        IsRequired="true" Value="TODAY" />
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Job Address 1:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <asp:TextBox ID="txtJobAddress1" CssClass="form-control" runat="server" Style="width: 280px;"></asp:TextBox>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Last Furnished/End Date:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="EndDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                        Value="TODAY" />
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Job Address 2:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <asp:TextBox ID="txtJobAddress2" CssClass="form-control" runat="server" Style="width: 280px;"></asp:TextBox>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Notice of Completion:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="NOCDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                        Value="TODAY" />
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Job City:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <asp:TextBox ID="txtJobCity" CssClass="form-control" runat="server"></asp:TextBox>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Notice Sent Date:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="NoticeSentDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false" Value="TODAY" />
                                </span>
                            </div>
                             <asp:HiddenField ID="hdnDateAssigned" runat="server" />
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Job State:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <div id="UlState" style="width: 66%;">
                                </div>
                                <asp:DropDownList ID="DrpState" runat="server" Style="display: none;">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnDrpState" runat="server" />
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Notice of Intent Sent:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="NOIDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false" Value="TODAY" />
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Job Zip:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <asp:TextBox ID="txtJobZip" CssClass="form-control" runat="server" Style="width: 66%;"></asp:TextBox>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Lien Filed:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="LienSent" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                        Value="TODAY" />
                                </span>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Est Balance:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <cc1:DataEntryBox ID="EstBalance" runat="server" CssClass="form-control"
                                    Text="" DataType="Money"></cc1:DataEntryBox>
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Foreclose Filed:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="ForecloseDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                        Value="TODAY" />
                                </span>
                            </div>

                        </div>

                         <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Job Status:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                                <div id="UlJobStatus">
                                </div>
                                <asp:DropDownList ID="DrpJobStatus" runat="server" Style="display: none;">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnDrpJobStatus" runat="server" />
                                <asp:HiddenField ID="hdnDrpJobStatusID" runat="server" />
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Bond Suit Filed:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="BondSuitDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                        Value="TODAY" />
                                </span>
                            </div>

                        </div>

                           <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%;">Property Type:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                                <div id="UlPropertyType">
                                </div>
                                    <asp:DropDownList ID = "DrpPropertyType" runat="server" style="display:none;">
                                    
                                    <asp:ListItem Text = "Public" Value="Public"> </asp:ListItem>
                                      <asp:ListItem Text = "Federal" Value="Federal"></asp:ListItem>
                                      <asp:ListItem Text = "Residential" Value="Residential"></asp:ListItem>
                                     <asp:ListItem Text = "Private" Value="Private"></asp:ListItem>
        
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnDrpPropertyType" runat="server" />
                            </div>
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">SN Date:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="SNDate" CSSClass="form-control datepicker" runat="server" ErrorMessage="SNDate" IsReadOnly="false"
                                        Value="" />
                                </span>
                            </div>
                        </div>


                         <div class="form-group">
                            <label class="col-md-2 col-sm-2 col-xs-2 control-label align-lable" style="width: 18%;">Amend Notice:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <div class="checkbox-custom checkbox-default">
                                    <asp:CheckBox ID="chkSendAmended" runat="server" Text=" " />
                                </div>
                            </div>
                            <asp:Label ID="lblPaidStatus" CssClass="col-md-3 col-sm-3 col-xs-3 control-label align-lable" runat="server" Text="Job Paid In Full:" Style="margin-left: 7%;"></asp:Label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <div class="checkbox-custom checkbox-default">
                                    <asp:CheckBox ID="chkJobPaidInFull" runat="server" Text=" " />
                                </div>
                            </div>
                        </div>

                        <div class="form-group">

                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="width: 18%; visibility: collapse">Desk: </label>
                            <div class="col-md-3 col-sm-3 col-xs-3" style ="visibility:collapse">
                                <div id="UlDesk" style="width: 280px; visibility:collapse" >
                                </div>
                                <asp:DropDownList ID="DrpDesk" runat="server" Style="display: none; width: 280px;">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnDrpDesk" runat="server" />
                                <asp:HiddenField ID="hdnDrpDeskID" runat="server" />
                            </div>

                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable" style="margin-left: 7%;">Bond Sent:</label>
                            <div class="col-md-3 col-sm-3 col-xs-3">
                                <span style="display: flex;">
                                    <SiteControls:Calendar ID="BondSent" CSSClass="form-control datepicker" runat="server" ErrorMessage="From Date" IsReadOnly="false"
                                        Value="TODAY" />
                                </span>
                            </div>
                        </div>


                       

                        <div class="form-group">
                            <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable align-lable">
                                <asp:Label ID="lblNOCompBox" runat="server" Text="Notice of Comp Search:" Style="margin-left: 7%;"></asp:Label></label>
                            <div class="col-md-3 col-sm-3 col-xs-3 col-sm-3 col-xs-3">
                                <div class="checkbox-custom checkbox-default">
                                    <asp:CheckBox ID="chkNOCompBox" runat="server" Text=" " />
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:LinkButton ID="CancelButton" runat="server" CssClass="button" OnClick="CancelButton_Click" Text="Back " CausesValidation="False" Style="display: none;" />
                </div>
            </div>
            <div class="modal-footer">
                <asp:UpdatePanel ID="updPan1" runat="server">
                    <ContentTemplate>
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save" />
                       <%-- <button type="button" class="btn btn-default margin-0" data-dismiss="modal" onclick="ModalHide1();">Close</button>--%>
                         <asp:Button ID="btncancel" CssClass="btn btn-default margin-0" runat="server" Text="Close" />
                        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                            <ProgressTemplate>
                                <div class="TransparentGrayBackground">
                                </div>
                                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                    <div class="PageUpdateProgress">
                                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif"
                                            AlternateText="[image]" />
                                        &nbsp;Please Wait...
                                    </div>
                                </asp:Panel>
                                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server"
                                    TargetControlID="alwaysVisibleAjaxPanel" HorizontalSide="Center" HorizontalOffset="150"
                                    VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </div>
        </div>
    </div>
</div>
