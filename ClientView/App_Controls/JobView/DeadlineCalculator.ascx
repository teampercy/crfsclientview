<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<style type="text/css">
    .style1 {
        width: 28%;
        height: 15px;
    }

    .button {
    }

    .style3 {
        width: 28%;
    }

    .style5 {
        width: 259px;
    }

    .style6 {
        width: 259px;
        height: 23px;
    }

   

    .style7 {
        width: 28%;
        height: 36px;
    }

    .style8 {
        width: 259px;
        height: 36px;
    }

    .style9 {
        width: 28%;
        height: 38px;
    }

    .style10 {
        width: 259px;
        height: 38px;
    }

    .style11 {
        width: 28%;
        height: 35px;
    }

    .style12 {
        width: 259px;
        height: 35px;
    }

    .style17 {
        width: 28%;
        height: 31px;
    }

    .style18 {
        width: 259px;
        height: 31px;
    }

    .style19 {
        width: 28%;
        height: 64px;
    }

    .style20 {
        width: 259px;
        height: 64px;
    }

    .style21 {
        width: 28%;
        height: 63px;
    }

    .style22 {
        width: 259px;
        height: 63px;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('RentalView', 'Tools', 'Deadline Calculator');
        MainMenuToggle('liRentalToolsJobView');
        SubMenuToggle('liDeadlineCalculatorJobView');
        //var PageName = window.location.search.substring(1);
        //if (PageName == "lienview.DeadlineCalculator.JobView") {
           
        //}
        //else {
        //    SetHeaderBreadCrumb('LienView', 'Tools', 'Deadline Calculator');
        //    MainMenuToggle('liTools');
        //    SubMenuToggle('liDeadlineCalculator');
        //}
       
        //$('#liTools').addClass('site-menu-item has-sub active open');
        //$('#liDeadlineCalculator').addClass('site-menu-item active');
        return false;

    }
   
</script>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Me.Calendar1.Value = Today
        Else
             ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
       
    End Sub

    Protected Sub btnGetDate_Click(sender As Object, e As System.EventArgs)
        Me.Page.Validate()
        'If Me.Page.IsValid = False Then
        '    Me.ValidationSummary1.Visible = True
        '    Exit Sub
        'End If

        Me.CustomValidator1.ErrorMessage = ""
        Me.CustomValidator1.IsValid = True
        
        If Me.Calendar1.Value.Length < 1 And Me.Calendar2.Value.Length < 1 And Me.Calendar3.Value.Length < 1 Then
            Dim s As String = "One of the Date is Required"
            Me.CustomValidator1.ErrorMessage = s
            Me.CustomValidator1.IsValid = False
        End If
        If Me.JobState.Text.Length < 1 Then
            Dim s As String = "A Job State is Required"
            Me.CustomValidator1.ErrorMessage = s
            Me.CustomValidator1.IsValid = False
            Exit Sub
        End If
        
        
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetDeadlineDate
        With MYSPROC
            .StartDate = Me.Calendar1.Value
            .EndDate = Me.Calendar2.Value
            .NOCDate = Me.Calendar3.Value
 
            If Me.JobState.Text.Trim.Length > 1 Then
                .JobState = Me.JobState.Text.Trim
            End If
            
            If Me.Private.Checked = True Then
                .JobType = "Private"
            ElseIf Me.Public.Checked = True Then
                .JobType = "Public"
            Else
                .JobType = "Residential"
            End If
            
            If Me.Lien.Checked = True Then
                .NoticeType = "Lien"
            ElseIf Me.Bond.Checked = True Then
                .NoticeType = "Bond"
            Else
                .NoticeType = "StopNotice"
            End If
        End With

        Dim MYVIEW As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
        If MYVIEW.RowItem("NewDate") = Date.MinValue Then
            Dim s As String = "There is no defined value for the requested filters"
            Me.CustomValidator1.ErrorMessage = s
            Me.CustomValidator1.IsValid = False
            Exit Sub
        Else
            Me.txtDeadlineDate.Text = CDate(MYVIEW.RowItem("NewDate"))
            Me.Label3.Text = MYVIEW.RowItem("LienAlert")
        End If
        
    End Sub

    Protected Sub Bond_CheckedChanged(sender As Object, e As System.EventArgs)

    End Sub

</script>

                                    <div id="modcontainer" style="MARGIN: 10px; WIDTH: 100%">
                                        <h1 class="panelheader">Deadline Date Calculator</h1>
                                        <div class="body">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                  
                                                    <div class="col-md-12" style="text-align:left;">
                                                         <label class="control-label" style="text-align:left;"> * This is not legal advice. These guidelines are presented as a courtesy. We recommend you consult your attorney for state specific legal advice.</label>
                                                       
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                
                                                    <div class="col-md-12" style="color: red;text-align:left;">
                                                      <label class="control-label"> <span style="color:red;"> ** A more accurate result is achieved by providing the Last Furnished Date or Completion Date.</span> </label>
                                                           
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">First Furnished Date:</label>
                                                    <div class="col-md-7">
                                                        <span style="display: flex;">
                                                            <SiteControls:Calendar ID="Calendar1" Value="TODAY" IsReadOnly="false"
                                                                IsRequired="False" ErrorMessage="First Furnished Date" runat="server" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Last Furnished Date:</label>
                                                    <div class="col-md-7">
                                                        <span style="display: flex;">
                                                            <SiteControls:Calendar ID="Calendar2" IsReadOnly="false"
                                                                IsRequired="False" ErrorMessage="Last Furnished Date" runat="server" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Completion Date</label>

                                                    <div class="col-md-7">
                                                        <span style="display: flex;">
                                                            <SiteControls:Calendar ID="Calendar3" IsReadOnly="false"
                                                                IsRequired="False" ErrorMessage="Completion Date" runat="server" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Job State:</label>
                                                    <div class="col-md-1">
                                                        <%--##1--%>
                                                        <asp:TextBox ID="JobState" runat="server" Width="60px" CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">JobType:</label>
                                                    <div class="col-md-7" style="font-size:14px;">
                                                        <div class="example-wrap" style="margin-bottom: 0;">
                                                            <div class="radio-custom radio-default">
                                                                <asp:RadioButton ID="Private" runat="server" Checked="True"
                                                                    GroupName="optJobType"  Text="Private" class="col-md-2"></asp:RadioButton>
                                                             
		                                            <asp:RadioButton ID="Public" runat="server"
                                                        GroupName="optJobType" Text="Public" class="col-md-2"></asp:RadioButton>
                                                               
		                                            <asp:RadioButton ID="Residential" runat="server"
                                                        GroupName="optJobType" Text="Residential" class="col-md-4"></asp:RadioButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Notice Type:</label>
                                                    <div class="col-md-7"  style="font-size:14px;">
                                                        <div class="example-wrap" style="margin-bottom: 0;">
                                                            <div class="radio-custom radio-default">
                                                                <asp:RadioButton ID="Lien" runat="server" GroupName="optNoticeType"
                                                                    Checked="True" Text="Lien" class="col-md-2"></asp:RadioButton>
                                                             
	                                                <asp:RadioButton ID="Bond" runat="server" GroupName="optNoticeType"
                                                        OnCheckedChanged="Bond_CheckedChanged" Text="Bond" CssClass="col-md-2"></asp:RadioButton>
                                                           
	                                                <asp:RadioButton ID="StopNotice" runat="server" GroupName="optNoticeType"
                                                        Text="Stop Notice" CssClass="col-md-4"></asp:RadioButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-3">
                                                        <asp:Button ID="btnGetDate" runat="server" Text="GET DEADLINE DATE"
                                                            CssClass="btn btn-primary"
                                                            CausesValidation="true" Font-Bold="True" OnClick="btnGetDate_Click"
                                                            Height="37px" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">State Alert:</label>
                                                    <div class="col-md-7">
                                                        <asp:Label ID="Label3" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Deadline Date:</label>
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtDeadlineDate" runat="server" CssClass="form-control" ReadOnly="True" BackColor="#CCFFFF"
                                                            ></asp:TextBox>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-7">
                                                        <asp:CustomValidator ID="CustomValidator1" runat="server"
                                                            ErrorMessage="CustomValidator"></asp:CustomValidator>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                              




