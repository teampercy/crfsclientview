<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script runat="server">
    Protected WithEvents btnEditItem As LinkButton
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
      
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.UserId.Value = Me.CurrentUser.Id
            Me.fromAsgDate.Value = DateAdd(DateInterval.Day, -365, Today)
            Me.thruAsgDate.Value = DateAdd(DateInterval.Day, 1, Today)
            Me.Calendar1.Value = Me.fromAsgDate.Value
            Me.Calendar2.Value = Me.thruAsgDate.Value
            Dim mydt As System.Data.DataTable
            If Me.RadioButton1.Checked = True Then
                mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlert30Days(Me.CurrentUser.Id, Me.fromAsgDate.Value, Me.thruAsgDate.Value, True)
                Me.gvwList.DataSourceID = Me.obj30Days.ID
                Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
            ElseIf Me.RadioButton2.Checked = True Then
                mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlert60Days(Me.CurrentUser.Id, Me.fromAsgDate.Value, Me.thruAsgDate.Value, True)
                Me.gvwList.DataSourceID = Me.obj60Days.ID
                Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
            Else
                mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlertAll(Me.CurrentUser.Id, Me.fromAsgDate.Value, Me.thruAsgDate.Value, True)
                Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
                Me.gvwList.DataSourceID = Me.objAll.ID
            End If
            Me.gvwList.DataBind()
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
        
    End Sub
   
    Protected Sub gvwItemList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.gvwList.DataBind()

    End Sub
    Protected Sub ddlPageSize_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dropDown As DropDownList = DirectCast(sender, DropDownList)
        Me.gvwList.PageSize = Integer.Parse(dropDown.SelectedValue)

    End Sub
    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub gvwList_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwList.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = DR("JobId")
      
            e.Row.Cells(3).Text = Strings.Left(e.Row.Cells(3).Text, 25)
            e.Row.Cells(4).Text = Strings.Left(e.Row.Cells(4).Text, 25)

            e.Row.Cells(5).Text = FormatDate(e.Row.Cells(5).Text)
            e.Row.Cells(6).Text = FormatDate(e.Row.Cells(6).Text)
            e.Row.Cells(7).Text = FormatDate(e.Row.Cells(7).Text)
            e.Row.Cells(8).Text = FormatDate(e.Row.Cells(8).Text)
            e.Row.Cells(9).Text = FormatDate(e.Row.Cells(9).Text)
        End If
        If e.Row.RowType = DataControlRowType.Header Then
            e.Row.TableSection = TableRowSection.TableHeader
            
        End If
    End Sub
    Private Function FormatDate(ByVal adate As String) As String
        If IsDate(adate) = False Then
            Return ""
        End If
        Return Strings.FormatDateTime(adate)
    End Function
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myitemid & "&list=ldr"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
      

        
    End Sub

    Protected Sub btnRefresh_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim mydt As System.Data.DataTable
        If Me.RadioButton1.Checked = True Then
            mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlert30Days(Me.CurrentUser.Id, Me.fromAsgDate.Value, Me.thruAsgDate.Value, True)
            Me.gvwList.DataSourceID = Me.obj30Days.ID
            Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
        ElseIf Me.RadioButton2.Checked = True Then
            mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlert60Days(Me.CurrentUser.Id, Me.fromAsgDate.Value, Me.thruAsgDate.Value, True)
            Me.gvwList.DataSourceID = Me.obj60Days.ID
            Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
        Else
            mydt = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlertAll(Me.CurrentUser.Id, Me.fromAsgDate.Value, Me.thruAsgDate.Value, True)
            Me.gvwList.DataSourceID = Me.objAll.ID
            Me.Session("joblist") = New HDS.DAL.COMMON.TableView(mydt)
        End If

        Me.gvwList.DataBind()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)


    End Sub

    Protected Sub btnReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View2)

    End Sub

    Protected Sub Linkbutton1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim sreport As String = ""
        If Me.rbt30Days.Checked = True Then
            sreport = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlert30DaysReport(Me.CurrentUser.Id, Me.Calendar1.Value, Me.Calendar2.Value, True)
        ElseIf Me.rbt60Days.Checked = True Then
            sreport = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlert60DaysReport(Me.CurrentUser.Id, Me.Calendar1.Value, Me.Calendar2.Value, True)
        Else
            sreport = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLienAlertReport(Me.CurrentUser.Id, Me.Calendar1.Value, Me.Calendar2.Value, True)
        End If
        If IsNothing(sreport) = False Then
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 600, 600, "no"))
            Me.MultiView1.SetActiveView(Me.View3)
        End If
    End Sub

    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View2)
        If gvwList.Rows.Count > 0 Then
            Me.gvwList.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub

    Protected Sub rbt30Days_CheckedChanged(sender As Object, e As System.EventArgs)

    End Sub

    Protected Sub rpt60Days_CheckedChanged(sender As Object, e As System.EventArgs)

    End Sub
</script>
<script>

    function MaintainMenuOpen() {

        SetHeaderBreadCrumb('RentalView', 'Tools', 'DeadLine Alert');
        MainMenuToggle('liRentalToolsJobView');
        SubMenuToggle('liLiensDayRemainingListJobView');
        //var PageName = window.location.search.substring(1);
        //if (PageName == "lienview.liensdayremaininglist.JobView") {
          
        //}
        //else {
        //    MainMenuToggle('liTools');
        //    SubMenuToggle('liLiensDayRemainingList');
        //    SetHeaderBreadCrumb('LienView', 'Tools', 'DeadLine Alert');
        //}
    }

    $(function () {
    
        CallSuccessFunc();
    });


    function CallSuccessFunc() {
        var defaults = {

            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [15, 25, 35, 50, 100],
            "searching": false,
            bAutoWidth: false,
            responsive: true,
            "bSort": true,
            "aaSorting": [[0]],
            "aoColumns": [
              { "bSortable": false, "searchable": false },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true }
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-4'i><'col-sm-1'l><'col-sm-7'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },

        };

        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwList.ClientID%>').dataTable(options);



    <%--    var Columns = [
             { "bSortable": false, "searchable": false },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true },
             { "bSortable": true }
        ]

        MakeDataTable('#<%=gvwList.ClientID%>', Columns);--%>

    }

</script>
<style>
    #ctl33_ctl00_gvwList_wrapper {
        width:100%;
    }

    #ctl33_ctl00_gvwList thead th {
        white-space:nowrap;
    }
</style>


<asp:ObjectDataSource ID="obj30Days" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetLienAlert30Days"
    TypeName="CRF.CLIENTVIEW.BLL.LienView.Provider">
    <SelectParameters>
        <asp:ControlParameter ControlID="UserId" Name="userid" PropertyName="Value" Type="String" />
        <asp:ControlParameter ControlID="fromAsgDate" Name="fromdate" PropertyName="Value"
            Type="String" />
        <asp:ControlParameter ControlID="thruAsgDate" Name="thrudate" PropertyName="Value"
            Type="String" />
        <asp:Parameter Name="pdf" Type="Boolean" DefaultValue="False" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="obj60Days" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetLienAlert60Days"
    TypeName="CRF.CLIENTVIEW.BLL.LienView.Provider">
    <SelectParameters>
        <asp:ControlParameter ControlID="UserId" Name="userid" PropertyName="Value" Type="String" />
        <asp:ControlParameter ControlID="fromAsgDate" Name="fromdate" PropertyName="Value"
            Type="String" />
        <asp:ControlParameter ControlID="thruAsgDate" Name="thrudate" PropertyName="Value"
            Type="String" />
        <asp:Parameter Name="pdf" Type="Boolean" DefaultValue="False" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="objAll" runat="server"
    OldValuesParameterFormatString="original_{0}" SelectMethod="GetLienAlertAll"
    TypeName="CRF.CLIENTVIEW.BLL.LienView.Provider">
    <SelectParameters>
        <asp:ControlParameter ControlID="UserId" Name="userid" PropertyName="Value" Type="String" />
        <asp:ControlParameter ControlID="fromAsgDate" Name="fromdate" PropertyName="Value"
            Type="String" />
        <asp:ControlParameter ControlID="thruAsgDate" Name="thrudate" PropertyName="Value"
            Type="String" />
        <asp:Parameter Name="pdf" Type="Boolean" DefaultValue="False" />
    </SelectParameters>
</asp:ObjectDataSource>




<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" class="margin-bottom-0" style="width: 100%">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Deadline Alert List</h1>
                    <div class="body">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <div class="col-md-2">
                                    <asp:LinkButton ID="btnReport" runat="server" CssClass="btn btn-primary" Text="Print Report" OnClick="btnReport_Click" />
                                </div>
                                <div class="col-md-7">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="RadioButton1" runat="server" GroupName="reportype" Text="Less Than 30 Days" Checked="True" />
                                    </div>

                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="RadioButton2" runat="server" GroupName="reportype" Text="Less Than 60 Days" />
                                    </div>

                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="RadioButton3" runat="server" GroupName="reportype" Text="All Jobs" />
                                    </div>


                                </div>
                                <div class="col-md-1">
                                    <asp:LinkButton ID="btnRefresh" runat="server" Text="Refresh List" CssClass="btn btn-primary" OnClick="btnRefresh_Click" />
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:GridView ID="gvwList" runat="server" DataSourceID="ItemsDataSource"
                                        CssClass="table dataTable table-striped" AutoGenerateColumns="False"
                                        BorderWidth="1px" Width="100%">
                                        <%--<RowStyle CssClass="rowstyle" />
                                        <AlternatingRowStyle CssClass="altrowstyle" />
                                        <HeaderStyle CssClass="headerstyle" />
                                        <PagerStyle CssClass="pagerstyle" />
                                        <PagerTemplate>
                                            <asp:Label ID="Label1" runat="server" Text="Show rows:" />
                                            <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPageSize_SelectedIndexChanged">
                                                <asp:ListItem Value="5" />
                                                <asp:ListItem Value="10" />
                                                <asp:ListItem Value="15" />
                                                <asp:ListItem Value="20" />
                                            </asp:DropDownList>
                                            &nbsp;
                            Page 
                            <asp:TextBox ID="txtGoToPage" runat="server" AutoPostBack="true" Text="1" CssClass="gotopage" />
                                            of
                            <asp:Label ID="lblTotalNumberOfPages" runat="server" />
                                            &nbsp;
                            <asp:Button ID="btnFirstPage" runat="server" CommandName="Page" ToolTip="First page" CommandArgument="First" CssClass="firstpage" />
                                            <asp:Button ID="btnPrevPage" runat="server" CommandName="Page" ToolTip="Previous Page" CommandArgument="Prev" CssClass="prevpage" />
                                            <asp:Button ID="btnNextPage" runat="server" CommandName="Page" ToolTip="Next Page" CommandArgument="Next" CssClass="nextpage" />
                                            <asp:Button ID="btnLastPage" runat="server" CommandName="Page" ToolTip="Last Page" CommandArgument="Last" CssClass="lastpage" />
                                        </PagerTemplate>--%>
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="15px" Wrap="False" HorizontalAlign="Center" VerticalAlign ="Middle"/>

                                            </asp:TemplateField>
                                            <asp:BoundField DataField="JobId" HeaderText="CRF#" SortExpression="JobId" >
                                                <ItemStyle Width="25px" Height="15px" Wrap="False" />
                                            </asp:BoundField>
                                            <%--<asp:BoundField HeaderText="Branch#" DataField="BranchNumber" SortExpression="BranchNumber">--%>
                                            <asp:BoundField HeaderText="Branch#">
                                                <ItemStyle Width="20px" Height="15px" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JobNum" SortExpression="JobNum" HeaderText="Job#">
                                                <ItemStyle Width="25px" Height="15px" Wrap="False" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="JobAdd1" HeaderText="Job Address"
                                                SortExpression="JobAdd1">
                                                <ItemStyle Width="50%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ClientCustomer" HeaderText="Customer"
                                                SortExpression="ClientCustomer">
                                                <ItemStyle Width="50%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="StartDate" HeaderText="Start Date" SortExpression="StartDate" >
                                                <ItemStyle HorizontalAlign="Center" Width="70px" CssClass="date" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EndDate" HeaderText="End Date" SortExpression="EndDate" ItemStyle-CssClass="date">
                                                <ItemStyle HorizontalAlign="Center" Width="60px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOCDate" HeaderText="NOC Date"  ItemStyle-CssClass="date"
                                                SortExpression="NOCDate">
                                                <ItemStyle HorizontalAlign="Center" Width="70px" CssClass="date"  />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DeadlineDate" HeaderText="Deadline"
                                                SortExpression="DeadlineDate">
                                                <ItemStyle HorizontalAlign="Center" Width="60px" CssClass="date"  />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DateType" HeaderText="Type"
                                                SortExpression="DateType">
                                                <ItemStyle HorizontalAlign="Center" Width="15px" />
                                            </asp:BoundField>
                                        </Columns>
                                        <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- <div class="footer">
                        <br />
                    </div>--%>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <h1 class="panelheader">Deadline Alert Report</h1>
                    <div class="body">

                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-2">
                                    <asp:LinkButton ID="Linkbutton1" runat="server" CssClass="btn btn-primary" Text="Back to List" OnClick="Linkbutton1_Click"></asp:LinkButton>
                                </div>
                                <div class="col-md-10">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Assigned Date:</label>
                                <div class="col-md-8" style="display: flex;">
                                    <SiteControls:Calendar ID="Calendar1" runat="server" ErrorMessage="From Date" IsReadOnly="false" IsRequired="false" Value="TODAY" />
                                    &nbsp;&nbsp;Thru&nbsp;
                                 <SiteControls:Calendar ID="Calendar2" runat="server" ErrorMessage="From Date" IsReadOnly="false" IsRequired="false" Value="TODAY" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-4 control-label">Report Type:</label>
                                <div class="col-md-8">

                                    <div class="radio-custom radio-default">
                                        <asp:RadioButton ID="rbt30Days" runat="server" Checked="True"
                                            GroupName="ReportType" Text=" Less Than 30 Day Deadline" />
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">

                                    <div class="radio-custom radio-default">
                                        <asp:RadioButton ID="rbt60Days" runat="server" GroupName="ReportType"
                                            Text=" Less Than 60 Day Deadline" />
                                    </div>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">

                                    <div class="radio-custom radio-default">
                                        <asp:RadioButton ID="rbtAll" runat="server" GroupName="ReportType"
                                            Text=" All Jobs" />
                                    </div>

                                </div>
                            </div>

                        </div>

                        <div class="footer">
                            <div class="col-md-12" style="text-align: right;">
                                <asp:LinkButton ID="btnSubmit"
                                    CssClass="btn btn-primary"
                                    runat="server"
                                    Text="Create Report" />
                            </div>
                        </div>
                </asp:View>
                <asp:View ID="View3" runat="server">

                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-2">
                                    &nbsp;
                                    <asp:LinkButton ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Report Selection" OnClick="Linkbutton2_Click"></asp:LinkButton>
                                </div>
                            </div>
                            <div class="form-group">
                                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                    </div>




                </asp:View>
            </asp:MultiView>
        </div>
        <asp:HiddenField ID="HiddenField1" runat="server" />
        <asp:HiddenField ID="UserId" runat="server" />
        <asp:HiddenField ID="fromAsgDate" runat="server" />
        <asp:HiddenField ID="thruAsgDate" runat="server" />
        <asp:HiddenField ID="CurrentRowIndex" runat="server" />
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage"
                            runat="server"
                            ImageUrl="~/images/ajax-loader.gif"
                            AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender
                    ID="AlwaysVisibleControlExtender1"
                    runat="server"
                    TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center"
                    HorizontalOffset="150"
                    VerticalSide="Middle"
                    VerticalOffset="0">
                </ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
