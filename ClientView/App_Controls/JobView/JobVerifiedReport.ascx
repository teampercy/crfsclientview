<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('RentalView', 'Reports', 'Jobs Verified');
        MainMenuToggle('liRentalViewReport');
        SubMenuToggle('liJobsVerifiedJobView');
        //$('#liLienViewReport').addClass('site-menu-item has-sub active open');
        //$('#liJobsVerified').addClass('site-menu-item active');
        return false;

    }
    function BindClientSelection() {
        $('#UlClientSelectionType li').remove();
        var x = document.getElementById('<%=DropDownList1.ClientID%>');
        var txt = "";
        var i;
        for (i = 0; i < x.length; i++) {
            $('#UlClientSelectionType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClientSel_' + x.options[i].value + '" onclick="setSelectedClientId(' + x.options[i].value + ',\'' + x.options[i].text + '\')">' + x.options[i].text + '</a></li>');
        }
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val() != "") {
            $('#btnClientSelectionType').html($('#liClientSel_' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedClientId(id, TextData) {
        $('#' + '<%=hdnClientSelectionId.ClientID%>').val(id);
        $('#btnClientSelectionType').html(TextData + "<span class='caret' ></span>");
         if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val().length > 0)
        {
            <%-- alert('hii ' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val() );--%>
            var value = "SELECTED";
            var radio = $("[id*=ctl33_ctl00_RadioButtonList1] input[value=" + value + "]");
            radio.attr("checked", "checked");

        }
        return false;
    }


  <%--  function myFunctionJobStateLike(id) {
        if (id == 1) {
            document.getElementById('<%=JobStateLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobStateLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobStateLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobStateLike_2.ClientID%>').checked = true;
        }
     }--%>
    function myFunctionCustNameLike(id) {
        if (id == 1) {
            document.getElementById('<%=CustNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustNameLike_2.ClientID%>').checked = true;
        }
     }
    function myFunctionCustRefNumLike(id) {
        if (id == 1) {
            document.getElementById('<%=CustRefNum_1.ClientID%>').checked = true;
            document.getElementById('<%=CustRefNum_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustRefNum_1.ClientID%>').checked = false;
            document.getElementById('<%=CustRefNum_2.ClientID%>').checked = true;
        }
     }
   <%-- function myFunctionBranchNumLike(id) {
        if (id == 1) {
            document.getElementById('<%=BranchNum_1.ClientID%>').checked = true;
            document.getElementById('<%=BranchNum_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=BranchNum_1.ClientID%>').checked = false;
            document.getElementById('<%=BranchNum_2.ClientID%>').checked = true;
        }
    }--%>

</script>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            Me.Calendar1.Value = Today
            Me.Calendar2.Value = Today
            LoadClientList()
            If IsNothing(Me.Page.Request.QueryString("rpt")) = False Then
                Me.DownLoadReport(Me.Page.Request.QueryString("rpt"))
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
        If hdnClientSelectionId.Value <> "" Then
            RadioButtonList1.Items.FindByValue("SELECTED").Selected = True
        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_CLIENTVIEW_GetJobVerifiedSent
        With MYSPROC
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                    .ClientCode = Me.hdnClientSelectionId.Value
                End If
            End If

            If Me.CustName.Text.Trim.Length > 0 Then
                If Me.CustNameLike_1.Checked Then
                    .CustName = Me.CustName.Text.Trim & "%"
                End If
                If Me.CustNameLike_2.Checked Then
                    .CustName = "%" & Me.CustName.Text.Trim & "%"
                End If
            End If
            If Me.CustRefNum.Text.Trim.Length > 0 Then
                If CustRefNum_1.Checked Then
                    .CustRefNum = Me.CustRefNum.Text.Trim & "%"
                End If
                If CustRefNum_2.Checked Then
                    .CustRefNum = "%" & Me.CustRefNum.Text.Trim & "%"
                End If
            End If

            .BranchNum = Me.BranchNum.Text
            .JobState = Me.JobState.Text
            .FromDate = Me.Calendar1.Value
            .ThruDate = Me.Calendar2.Value
            .UserId = Me.CurrentUser.Id
            .SelectByNoticeSent = True

            If Me.rdoPrivate.Checked = True Then
                .JobType = 1
            ElseIf Me.rdoPublic.Checked = True Then
                .JobType = 2
            ElseIf Me.rdoFederal.Checked = True Then
                .JobType = 3
            Else
                .JobType = 0
            End If
        End With

        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.GetVerifiedReport(MYSPROC, Me.PDFReport.Checked))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If Me.SpreadSheet.Checked = False Then
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If
                'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
                'Me.MultiView1.SetActiveView(Me.View2)
            Else
                Me.btnDownLoad.Visible = True
                Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
                Me.MultiView1.SetActiveView(Me.View2)
            End If
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub Linkbutton2_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.MultiView1.SetActiveView(Me.View1)
        Me.btnDownLoad.Visible = False
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)

    End Sub


    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        Me.DownLoadReport(Me.Session("spreadsheet"))
    End Sub
    Private Sub LoadClientList()

        Dim vwclients As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop
            Me.panClients.Visible = True
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        End If
    End Sub

    Protected Sub rdoAll_CheckedChanged(sender As Object, e As System.EventArgs)

    End Sub
</script>


<div id="modcontainer" style="MARGIN: 10px; WIDTH: 100%;">
    <h1 class="panelheader">Jobs Verified Report</h1>
    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
            <div class="body">
                <asp:Panel ID="panClients" Visible="false" runat="server">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-md-4 col-sm-4 col-xs-4 control-label  align-lable">Client Select:</label>
                            <div class="col-md-4 col-sm-4 col-xs-4  " style="text-align: left;">
                                <asp:DropDownList ID="DropDownList1" runat="server" Style="display: none;">
                                </asp:DropDownList>
                                <asp:HiddenField ID="hdnClientSelectionId" runat="server" />
                                <div class="btn-group" style="text-align: left;width:100%">
                                    <button type="button" class="btn btn-default dropdown-toggle" style="width: 100%; text-align: left;" align="left"
                                        data-toggle="dropdown" aria-expanded="false" id="btnClientSelectionType" tabindex="1">
                                        --Select Client Selection--
                     
                                                                                    <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlClientSelectionType" style="overflow-y: auto; height: 150px;">
                                    </ul>
                                </div>
    
                                </div>
                              <div class="radio-custom radio-default radio-inline col-md-4 col-sm-4 col-xs-4">
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                            RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                            <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL" style="white-space:nowrap;"> All Clients  </asp:ListItem>
                                            <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED"   style="white-space:nowrap;padding-left:40px;"> Selected Client</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                        
                    </div>


                </asp:Panel>
                <div class="form-horizontal">
                  
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Customer Name:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5 ">
                            <asp:TextBox ID="CustName" runat="server" CssClass="form-control" style="width:100%;"></asp:TextBox>
                        </div>
                                                 <div class="col-md-3 col-sm-3 col-xs-3 " style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="CustNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustNameLike(1)" style="white-space:nowrap;" />
                                        <asp:RadioButton ID="CustNameLike_2" runat="server" Text="Includes" onchange="myFunctionCustNameLike(2)" style="white-space:nowrap;padding-left:40px;"  />
                                    </div>
                                </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Customer Ref #:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <asp:TextBox ID="CustRefNum" runat="server" CssClass="form-control" style="width:250px"></asp:TextBox>
                        </div>
                                                 <div class="col-md-3 col-sm-3 col-xs-3" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="CustRefNum_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustRefNumLike(1)"  style="white-space:nowrap;" />
                                        <asp:RadioButton ID="CustRefNum_2" runat="server" Text="Includes" onchange="myFunctionCustRefNumLike(2)" style="white-space:nowrap;padding-left:40px;"  />
                                    </div>
                                </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label  align-lable">Branch #:</label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <asp:TextBox ID="BranchNum" runat="server" CssClass="form-control"></asp:TextBox>
                        </div>

                    </div>
                      <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Job State:</label>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                            <asp:TextBox ID="JobState" runat="server" CssClass="form-control" style="width:100px;"></asp:TextBox>
                        </div>
                       
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Verified Date:</label>
                        <div class="col-md-5 col-sm-5 col-xs-5 ">
                            <div style="float: left;">
                            <span style="display: flex;">
                                <SiteControls:Calendar ID="Calendar1" Value="TODAY" IsReadOnly="false" IsRequired="true" ErrorMessage="Verified From" runat="server" />
                            </span>
                            </div>
                        
                        <div>
                             <label class="control-label align-lable" style="float: left;padding-left: 20px;padding-right: 20px;">Thru:</label>
                            <span style="display: flex;">
                                <SiteControls:Calendar ID="Calendar2" Value="TODAY" IsReadOnly="false" IsRequired="true" ErrorMessage="Verified Through" runat="server" />
                            </span>
                        </div>
                    </div>
                   </div>
                    <div class="form-group">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Job Type:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="example-wrap" style="margin-bottom: 0;">
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="rdoAll" runat="server" Checked="True"
                                        GroupName="JobFilter" Text="All Jobs" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-sm-4 col-xs-4 "></div>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="example-wrap" style="margin-bottom: 0;">
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="rdoPrivate" runat="server" GroupName="JobFilter"
                                        Text="Private" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-sm-4 col-xs-4 "></div>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="example-wrap" style="margin-bottom: 0;">
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="rdoPublic" runat="server" GroupName="JobFilter"
                                        Text="Public" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-sm-4 col-xs-4 "></div>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="example-wrap" style="margin-bottom: 0;">
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="rdoFederal" runat="server" GroupName="JobFilter"
                                        Text="Federal" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group" style="padding-top :20px">
                        <label class="col-md-4 col-sm-4 col-xs-4  control-label align-lable">Report Type:</label>
                        <div class="col-md-7 col-sm-7 col-xs-7">
                            <div class="example-wrap" style="margin-bottom: 0;">
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="PDFReport" runat="server" GroupName="ReportType"
                                        Checked="True" Text="PDF"></asp:RadioButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 col-sm-4 col-xs-4 "></div>
                        <div class="col-md-7 col-sm-7 col-xs-7 ">
                            <div class="example-wrap" style="margin-bottom: 0;">
                                <div class="radio-custom radio-default">
                                    <asp:RadioButton ID="SpreadSheet" runat="server" GroupName="ReportType" Text="Spreadsheet" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="490px" HeaderText="The Following Field(s) are Required: " />
            </div>
            <div class="footer">
                <div class="form-group">

                    <div class="col-md-12" style="text-align: right;">
                        <asp:UpdatePanel ID="updPan1" runat="server">
                            <ContentTemplate>
                                <asp:Button ID="btnSubmit"
                                    CssClass="btn btn-primary"
                                    runat="server"
                                    Text="Create Report" OnClick="btnSubmit_Click" />
                                <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
                                    <ProgressTemplate>
                                        <div class="TransparentGrayBackground"></div>
                                        <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                                            <div class="PageUpdateProgress">
                                                <asp:Image ID="ajaxLoadNotificationImage"
                                                    runat="server"
                                                    ImageUrl="~/images/ajax-loader.gif"
                                                    AlternateText="[image]" />
                                                &nbsp;Please Wait...
                                            </div>
                                        </asp:Panel>
                                        <ajaxToolkit:AlwaysVisibleControlExtender
                                            ID="AlwaysVisibleControlExtender1"
                                            runat="server"
                                            TargetControlID="alwaysVisibleAjaxPanel"
                                            HorizontalSide="Center"
                                            HorizontalOffset="150"
                                            VerticalSide="Middle"
                                            VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>


            </div>
        </asp:View>
        <asp:View ID="View2" runat="server">
            <div class="body">
                <div class="form-horizontal">
                    <div class="form-group">

                        <div class="col-md-12" style="text-align: left;">
                            <asp:Button ID="Linkbutton2" runat="server" CssClass="btn btn-primary" Text="Back to Selection" OnClick="Linkbutton2_Click"></asp:Button>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <div style="width: auto; height: auto; overflow: auto;">
                                <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-12">
                            <asp:Button ID="btnDownLoad" Text="DownLoad SpreadSheet" CssClass="btn btn-primary" runat="server" OnClick="btnDownLoad_Click" />
                        </div>
                    </div>
                </div>

            </div>
        </asp:View>
    </asp:MultiView>
</div>

