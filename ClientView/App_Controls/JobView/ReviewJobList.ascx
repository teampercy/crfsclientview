﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<style>
    .panel-body, .form-control.input-sm {
        font-size: 11px;
    }

    #ctl32_ctl00_gvJobViewList tbody tr:hover {
        cursor: pointer;
    }

    #ctl32_ctl00_gvJobViewList thead tr th {
        font-weight: bold;
    }

    #ctl32_ctl00_gvJobViewList thead tr th, #ctl32_ctl00_gvJobViewList tbody tr td {
        /*border: 0;*/
    }

    #JobFilterBody .radio-custom label {
        width: 75px;
    }

    #JobFilterBody .radio-custom.radio-default.radio-inline {
        min-height: 0px;
        padding-top: 0px;
    }

    .RemovePadding {
        padding: 0px !important;
    }
    .nowordwraptd{
        padding-left: 5px;
white-space: nowrap;
text-overflow: ellipsis;
overflow: hidden;
    }

    .activePage {
        z-index: 3;
        color: white !important;
        cursor: default;
        background-color: #3a6dae !important;
        border-color: #3a6dae !important;
    }
</style>
<script type="text/javascript">
   function modalshow() {
        debugger;
        // alert("hii");
        $("#modelDivExport").modal('show');
        return false;
    }
    function modalhide() {
        //alert("hide");

        //alert($('input[type=radio]:checked').val());
        if ($('input[type=radio]:checked').val() == "PDFReport")
        {
            //$("#LodingPanel").show();
        }
        else
        {
            HideLoadingPanel();
            
        }
       
        $("#modelDivExport").modal('hide');
        document.getElementById("<%= btnExportReport.ClientID %>").click();
        return false;
    }
    function MaintainMenuOpen() {
        // SetHeaderBreadCrumb('RentalView', 'View All Jobs', 'View Jobs');
        SetHeaderBreadCrumb('RentalView', 'View Jobs', 'Requires Review');
        MainMenuToggle('liViewJobs');
        SubMenuToggle('liReviewJobList')
        return false;

    }
  
   

    function HideLoadingPanel() {
        //alert("hii");
        $(".TransparentGrayBackground").hide();

        $(".PageUpdateProgress").hide();
    }


</script>
<script runat="server">
    'Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV

    'New SP created by Laura for Requires Review
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetRequiresReviewListJV
    Protected WithEvents btnEditItem As LinkButton
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        If Me.Page.IsPostBack = False Then

            Me.MultiView1.SetActiveView(Me.View1)
            If qs.HasParameter("page") Then
                If (IsNothing(Session("filterOrder")) = False And IsNothing(Session("filterColumn")) = False) Then
                    drpSortType.Items.FindByValue(Session("filterOrder")).Selected = True
                    drpSortBy.Items.FindByValue(Session("filterColumn")).Selected = True
                End If

                If IsNothing(Session("mysproc")) Then
                    mysproc.ReviewStatus = True
                    mysproc.NotifyStatus = True
                    mysproc.UserId = Me.CurrentUser.Id
                    'mysproc.NoDeadLine = 1
                    'mysproc.ActiveOnly = True
                    Session("mysproc") = mysproc
                Else
                    mysproc = Session("mysproc")
                End If

                If IsNothing(Session("pageIndex")) Then
                    GetJobsPageWise(mysproc, 1)
                Else
                    GetJobsPageWise(mysproc, Convert.ToInt32(Session("pageIndex")))
                End If
            Else
                mysproc.ReviewStatus = True
                mysproc.NotifyStatus = True
                mysproc.UserId = Me.CurrentUser.Id
                'mysproc.NoDeadLine = 1
                'mysproc.ActiveOnly = True
                Session("mysproc") = mysproc
                GetJobsPageWise(mysproc, 1)
            End If
        Else
            If (IsNothing(Session("filterOrder")) = False And IsNothing(Session("filterColumn")) = False) Then
                drpSortType.Items.FindByValue(Session("filterOrder")).Selected = True
                drpSortBy.Items.FindByValue(Session("filterColumn")).Selected = True
            End If

            If IsNothing(Session("mysproc")) Then
                mysproc.ReviewStatus = True
                mysproc.NotifyStatus = True
                'mysproc.NoDeadLine = 1
                mysproc.UserId = Me.CurrentUser.Id
                'mysproc.ActiveOnly = True
                Session("mysproc") = mysproc
            Else
                mysproc = Session("mysproc")
            End If

            If IsNothing(Session("pageIndex")) Then
                GetJobsPageWise(mysproc, 1)
            Else
                GetJobsPageWise(mysproc, Convert.ToInt32(Session("pageIndex")))
            End If

        End If
    End Sub


    Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
        End If
    End Sub
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myitemid & "&list=RRL" & "&PageName=RequiresReview"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
            btnEditItem.Attributes("rowno") = DR("JobId")

            Dim lblNDLDate As Label = TryCast(e.Row.FindControl("lblNoticeDate"), Label)
            Dim lblLienDate As Label = TryCast(e.Row.FindControl("lblLienDate"), Label)
            Dim lblBondDate As Label = TryCast(e.Row.FindControl("lblBondDate"), Label)
            Dim lblSNDate As Label = TryCast(e.Row.FindControl("lblSNDate"), Label)
            Dim lblForeClose As Label = TryCast(e.Row.FindControl("lblForeClose"), Label)
            Dim lblSuit As Label = TryCast(e.Row.FindControl("lblSuit"), Label)
            Dim lblJobid As Label = TryCast(e.Row.FindControl("lblJobid"), Label)
            Dim lblNOI As Label = TryCast(e.Row.FindControl("lblNOIDeadline"), Label)

            Dim hdnNoticeSent As HiddenField = TryCast(e.Row.FindControl("hdnNoticeSent"), HiddenField)
            Dim hdnLienDate As HiddenField = TryCast(e.Row.FindControl("hdnLienDate"), HiddenField)
            Dim hdnBondDate As HiddenField = TryCast(e.Row.FindControl("hdnBondDate"), HiddenField)
            Dim hdnSNDate As HiddenField = TryCast(e.Row.FindControl("hdnSNDate"), HiddenField)
            Dim hdnForeclosureDate As HiddenField = TryCast(e.Row.FindControl("hdnForeclosureDate"), HiddenField)
            Dim hdnBondSuitDate As HiddenField = TryCast(e.Row.FindControl("hdnBondSuitDate"), HiddenField)
            Dim hdnPublicJob As HiddenField = TryCast(e.Row.FindControl("hdnPublicJob"), HiddenField)
            Dim hdndateassigned As HiddenField = TryCast(e.Row.FindControl("hdndateassigned"), HiddenField)


            If lblNDLDate.Text IsNot "" Then
                Dim NDLDate As Date = CType(lblNDLDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, NDLDate) < 10 And hdnNoticeSent.Value Is "" Then
                    lblNDLDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblLienDate.Text IsNot "" Then
                Dim LDLDate As Date = CType(lblLienDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, LDLDate) < 30 And hdnLienDate.Value Is "" Then
                    lblLienDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblBondDate.Text IsNot "" Then
                Dim BDLDate As Date = CType(lblBondDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, BDLDate) < 30 And hdnBondDate.Value Is "" Then
                    lblBondDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblSNDate.Text IsNot "" Then
                Dim SNDate As Date = CType(lblSNDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, SNDate) < 30 And hdnSNDate.Value Is "" Then
                    lblSNDate.ForeColor = Drawing.Color.Red
                End If
            End If

            If lblForeClose.Text IsNot "" Then
                Dim ForeClose As Date = CType(lblForeClose.Text, Date)
                If DateDiff(DateInterval.Day, Now, ForeClose) < 30 And hdnForeclosureDate.Value Is "" Then
                    lblForeClose.ForeColor = Drawing.Color.Red
                End If
            End If

            If lblSuit.Text IsNot "" Then
                Dim Suit As Date = CType(lblSuit.Text, Date)
                If DateDiff(DateInterval.Day, Now, Suit) < 30 And hdnBondSuitDate.Value Is "" And hdnPublicJob.Value = True Then
                    lblSuit.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblNOI.Text IsNot "" Then
                Dim NOI As Date = CType(lblNOI.Text, Date)
                If DateDiff(DateInterval.Day, Now, NOI) < 10 And hdnLienDate.Value Is "" Then
                    lblNOI.ForeColor = Drawing.Color.Red
                End If
            End If

            If hdndateassigned.Value IsNot "" Then
                lblJobid.ForeColor = Drawing.Color.Blue
            End If
        End If
    End Sub

    Private Function GetSortedData(ByVal SortTypeValue As String, ByVal SortByValue As String, mysprocSort As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetRequiresReviewListJV)
        Dim SortType As String = "ASC"
        If SortTypeValue = "-1" Then
            SortType = "ASC"
        ElseIf SortTypeValue = "1" Then
            SortType = "DESC"
        End If

        Dim SortBy As String
        If SortByValue = "-1" Then
            SortBy = "ListId " + SortType
        ElseIf SortByValue = "1" Then
            SortBy = "JobId " + SortType
        ElseIf SortByValue = "2" Then
            SortBy = "CustRefNum " + SortType
        ElseIf SortByValue = "3" Then
            SortBy = "JobNum " + SortType
        ElseIf SortByValue = "4" Then
            SortBy = "JobName " + SortType
        ElseIf SortByValue = "5" Then
            SortBy = "JobAdd1 " + SortType
        ElseIf SortByValue = "6" Then
            SortBy = "CityStateZip " + SortType
        ElseIf SortByValue = "7" Then
            SortBy = "JobState " + SortType
        ElseIf SortByValue = "8" Then
            SortBy = "JobBalance " + SortType
        ElseIf SortByValue = "9" Then
            SortBy = "JVStatusCode " + SortType
        ElseIf SortByValue = "10" Then
            SortBy = "NoticeDeadlineDate " + SortType
        ElseIf SortByValue = "11" Then
            SortBy = "LienDeadlineDate " + SortType
        ElseIf SortByValue = "12" Then
            SortBy = "BondDeadlineDate " + SortType
        ElseIf SortByValue = "13" Then
            SortBy = "SNDeadlineDate " + SortType
        ElseIf SortByValue = "14" Then
            SortBy = "NOIDeadlineDate " + SortType
        ElseIf SortByValue = "15" Then
            SortBy = "FilterKey " + SortType
        End If

        mysprocSort.SortColType = SortBy
        myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(mysprocSort)

        If (SortType = "ASC") Then
            If (SortByValue = "10" Or SortByValue = "11" Or SortByValue = "12" Or SortByValue = "13" Or SortByValue = "14") Then
                If (SortByValue = "10") Then
                    myviewSorting = SetNullDate("NoticeDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "11") Then
                    myviewSorting = SetNullDate("LienDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "12") Then
                    myviewSorting = SetNullDate("BondDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "13") Then
                    myviewSorting = SetNullDate("SNDeadlineDate", myviewSorting)
                ElseIf (SortByValue = "14") Then
                    myviewSorting = SetNullDate("NOIDeadlineDate", myviewSorting)
                End If
            End If
        End If

        If myviewSorting.Count < 1 Then
            Me.gvJobViewList.DataSource = Nothing
            Me.gvJobViewList.DataBind()


        End If
        Session("filterOrder") = SortTypeValue
        Session("filterColumn") = SortByValue
        Session("JobList") = mysprocSort
        Return myviewSorting
    End Function

    Protected Function SetNullDate(ByVal DateField As String, ByVal viewcheckdate As HDS.DAL.COMMON.TableView)
        For Each dr As System.Data.DataRow In viewcheckdate.ToTable.Rows
            If (dr(DateField).ToString = "2079-01-01 00:00:00.000") Then
                dr(DateField) = DBNull.Value

            End If

        Next
        Return viewcheckdate
    End Function


    Protected Sub drpSortType_SelectedIndexChanged(sender As Object, e As EventArgs)
        mysproc = Session("mysproc")
        GetJobsPageWise(mysproc, 1)
    End Sub

    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        mysproc = Session("mysproc")
        GetJobsPageWise(mysproc, 1)
    End Sub
    Protected Sub btnExportReport_Click(sender As Object, e As EventArgs)

        mysproc = Session("mysproc")
        mysproc.Page = 1
        If IsNothing(Session("TotalRecords")) = False Then
            mysproc.PageRecords = Session("TotalRecords")
        Else
            mysproc.PageRecords = 2500
        End If

        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.JobListExportJV(mysproc, Me.PDFReport.Checked))

        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If Me.SpreadSheet.Checked = False Then
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If

            Else

                Dim reportexcel As String = System.IO.Path.GetFileName(sreport)
                Session("reportexcel") = reportexcel

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideLoadingPanel", "HideLoadingPanel()", True)

                Me.DownLoadReport(Session("reportexcel"))


            End If
        End If

        Dim pageURL1 As String = "MainDefault.aspx?jobview.ReviewJobList"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + pageURL1 + "'", True)

    End Sub


    Public Function GetPropertyName(ByVal prPublic As String, ByVal prPrivate As String, ByVal prResidential As String, ByVal prFederal As String)
        Dim type As String = ""
        If prPublic Then
            type = "Public"
        ElseIf prPrivate = True And prResidential = False Then
            type = "Private"
        ElseIf prPrivate = True And prResidential = True Then
            type = "Residential"
        ElseIf prFederal Then
            type = "Federal"

        End If
        Return type
    End Function

    'Custom Pagging for job list
    Protected Sub PageSize_Changed(ByVal sender As Object, ByVal e As EventArgs)
        mysproc = Session("mysproc")
        Session("pageIndex") = 1
        Me.GetJobsPageWise(mysproc, 1)
    End Sub

    Protected Sub Page_Changed(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageIndex As Integer = Integer.Parse(CType(sender, LinkButton).CommandArgument)
        Session("pageIndex") = pageIndex
        mysproc = Session("mysproc")
        Me.GetJobsPageWise(mysproc, pageIndex)
        'End If
    End Sub

    Private Sub PopulatePager(ByVal recordCount As Integer, ByVal currentPage As Integer)
        Dim dblPageCount As Double = CType((CType(recordCount, Decimal) / Decimal.Parse(ddlPageSize.SelectedValue)), Double)
        Dim pageCount As Integer = CType(Math.Ceiling(dblPageCount), Integer)
        Dim pages As New List(Of ListItem)

        If (pageCount > 0) Then

            Dim showMax As Integer = 5
            Dim startPage As Integer
            Dim endPage As Integer
            If (pageCount <= showMax) Then

                startPage = 1
                endPage = pageCount

            Else

                startPage = currentPage
                If (pageCount - currentPage <= showMax - 1) Then
                    endPage = pageCount
                Else

                    endPage = currentPage + showMax - 1
                End If

            End If

            pages.Add(New ListItem("<<", "1", (currentPage > 1)))

            For i As Integer = startPage To endPage

                pages.Add(New ListItem(i.ToString, i.ToString, (i <> currentPage)))
            Next

            pages.Add(New ListItem(">>", pageCount.ToString, (currentPage < pageCount)))

        End If
        rptPager.DataSource = pages
        rptPager.DataBind()
        Dim recordsperpage As Integer = CType(ddlPageSize.SelectedValue, Integer)
        Dim FromRecord As Integer = ((recordsperpage * currentPage) - recordsperpage) + 1
        Dim ToRecord As Integer
        If currentPage = pageCount Then
            ToRecord = recordCount
        Else
            ToRecord = recordsperpage * currentPage
        End If

        Dim info As String = "Showing " & FromRecord & " to " & ToRecord & " of " & recordCount & " entries"
        lblTableInfo.Text = info
    End Sub
    Private Sub GetJobsPageWise(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetRequiresReviewListJV, ByVal pageIndex As Integer)
        asproc.Page = pageIndex
        asproc.PageRecords = Integer.Parse(ddlPageSize.SelectedValue)

        myview = GetSortedData(drpSortType.SelectedItem.Value, drpSortBy.SelectedItem.Value, asproc)


        If myview.Count < 1 Then
            Me.gvJobViewList.DataSource = Nothing
            Me.gvJobViewList.DataBind()

            Exit Sub
        End If

        Dim totrecs As String = myview.RowItem("totalrecords")
        Session("TotalRecords") = totrecs
        Me.gvJobViewList.DataSource = myview
        Me.gvJobViewList.DataBind()
        If gvJobViewList.Rows.Count > 0 Then
            Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        Me.MultiView1.SetActiveView(Me.View1)

        Me.PopulatePager(totrecs, pageIndex)
    End Sub
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="WIDTH: 100%;" class="margin-bottom-0">
            <asp:MultiView ID="MultiView1" runat="server">

           
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Requires Review Job List</h1>
                    <div class="body">
                        <div class="form-horizontal">
                              <div class="form-group">

                                <div class="col-md-12">
                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="text-align: left;">
                                <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" CausesValidation="False" OnClientClick="return modalshow();"></asp:LinkButton>
                                    <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                     <asp:ListItem Text="CRFS #" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Cust #" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Job #" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Job Name" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Job Address" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Job City" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Job State" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Job Balance" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Job Status" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="Notice Deadline Date" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Lien Deadline Date" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Bond Deadline Date" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="SN Deadline Date" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="NOI Deadline Date" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="Filter Key" Value="15"></asp:ListItem>
                                    </asp:DropDownList>
                                    <%--##101--%>
                                    <asp:DropDownList ID="drpSortType" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortType_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="Ascending" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Descending" Value="1"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            
                            </div>
                        </div>
                        <div style="width: auto; height: auto; overflow: auto;">

                           <%--  '^^*** Created by pooja 04/24/2021*** ^^--%>
                            <asp:GridView ID="gvJobViewList"  runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 12px !important;">
                                <Columns>

                                    <asp:TemplateField HeaderText="">
                                        <HeaderTemplate>
                                            <table style="width: 100%; table-layout:fixed;color:black !important" rules="cols">
                                                <tr>
                                                    <td style="width: 35px;"></td>
                                                    <td style="width: 71px;  text-align: center;" class="nowordwraptd">CRFS #</td>
                                                    <td style="width: 25%;" class="nowordwraptd">Job Information</td>
                                                    <td style="width: 23%; max-width: 18%;" class="nowordwraptd">Job #</td>
                                                     <td style="width: 95px;" class="nowordwraptd">Job Balance</td>
                                                     <td style="width: 95px;" class="nowordwraptd">Filter Key</td>
                                                    <%--<td style="width: 18%; word-break: break-all;">Job Address</td>--%>
                                                   <%--  <td style="width: 23%;"class="nowordwraptd" ></td>
                                                    <td style="width:75px;" class="nowordwraptd"></td>
                                                    <td style="width: 95px;" class="nowordwraptd"></td>--%>
                                                    <td colspan="3" style="width: 393px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">View</td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">Cust #</td>
                                                    <td style="width: 25%;" class="nowordwraptd">Customer Name</td>
                                                    <td style="width: 23%; max-width: 18%;" class="nowordwraptd">Job Status</td>  
                                                    <td style="width: 95px;" class="nowordwraptd">Property Type</td>
                                                    <td style="width: 95px;"  class="nowordwraptd">Branch #</td>
                                                     <td colspan="3" style="width: 393px" class="nowordwraptd">Deadline Dates</td>
                                              
                                                    <%--<td style="width: 23%;" class="nowordwraptd">Job Information</td>--%>
                                                    <%--<td style="width: 75px; text-align: center;" class="nowordwraptd">Job Desk</td>--%>
                                                       <%--<td style="width: 95px; text-align: center;" class="nowordwraptd">Filter Key</td>
                                                    <td style="width: 95px; text-align: center;" class="nowordwraptd">Job Balance</td>
                                                    <td colspan="3" style="width: 393px" class="nowordwraptd">Deadline Dates</td>--%>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle CssClass="RemovePadding" />
                                        <ItemStyle CssClass="RemovePadding" />
                                        <ItemTemplate>
                                            <table id="TableJobViewList" style="width: 100%; font-size: 14px; table-layout:fixed;" rules="cols">
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">
                                                        <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server" /></td>
                                                    <td style="width:71px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobid" runat="server" Text='<%# Eval("JobId")%>'></asp:Label></td>
                                                   
                                                      <td style="width: 25%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobName" runat="server" Text='<%# Eval("JobName")%>'></asp:Label></td>
                                                   
                                                    <td style="width: 23%; max-width: 18%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobNum" runat="server" Text='<%# Eval("JobNum")%>'></asp:Label>
                                                     
                                                    </td>
                                                   
                                                      <td style="width: 95px; text-align: center; " class="nowordwraptd">
                                                 <%--    <asp:Label ID="lblJobBalance" runat="server"
                                                            Text='<%# Eval("JobBalance", "{0:c}" %>'></asp:Label>                                                   
                                                        --%>
                                                
                                                            <asp:Label ID="lblJobBalance" runat="server"
                                                            Text='<%# If(Eval("JobId").ToString() = Eval("ParentJobId").ToString() And Eval("MergedJobBalance").ToString() <> "0.00", Eval("MergedJobBalance", "{0:c}"), Eval("JobBalance", "{0:c}"))%>'></asp:Label>                                                   
                                                        

                                                    </td>

                                                    <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("FilterKey")%>'></asp:Label></td>
                                                   <%-- <td style="width: 75px; text-align: center; " class="nowordwraptd">
                                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("JVDeskNum")%>'></asp:Label></td>--%>
                                                  
                                                    <td style="width: 135px;" class="nowordwraptd"><b style="font-weight: bold;">Notice - </b>
                                                        <asp:Label ID="lblNoticeDate" runat="server" Text='<%#Utils.FormatDate(Eval("NoticeDeadlineDate"))%>'></asp:Label>                                               
                                                         <asp:HiddenField ID="hdnNoticeSent" runat="Server" Value='<%#Eval("NoticeSent")%>' />
                                                    </td>
                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">Bond - </b>
                                                        <asp:Label ID="lblBondDate" runat="server" Text='<%#Utils.FormatDate(Eval("BondDeadlineDate"))%>'></asp:Label>
                                                          <asp:HiddenField ID="hdnBondDate" runat="Server" Value='<%#Eval("BondDate")%>' />
                                                    </td>
                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">SN - </b>
                                                        <asp:Label ID="lblSNDate" runat="server" Text='<%#Utils.FormatDate(Eval("SNDeadlineDate"))%>'></asp:Label>
                                                         <asp:HiddenField ID="hdnSNDate" runat="Server" Value='<%#Eval("SNDate")%>' />
                                                                              <asp:HiddenField ID="hdndateassigned" runat="server" Value='<%#Utils.FormatDate(Eval("DateAssigned")) %>' />
                                                         </td>
                                                </tr>
                                                  <tr>
                                                    <td></td>
                                                    <td style="text-align: center; width:71px" class="nowordwraptd">
                                                        <asp:Label ID="lblCustRefNum" runat="server" Text='<%# Eval("CustRefNum")%>'></asp:Label></td>
                                                   

                                                          <td style="width: 25%;" class="nowordwraptd">
                                                       <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityStateZip")%>'></asp:Label></td>
                                                  
                                                      <td style="width:23%; max-width: 18%;" class="nowordwraptd">
                                                       <asp:Label ID="lblStatus" runat="server" Text='<%# String.Format("{0} - {1}", Eval("JVStatusCode"), Eval("JVStatusDesc")) %>'></asp:Label>
                                                      </td>

                                                        <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <%--    <asp:Label ID="lblJobBalance" runat="server"
                                                            Text='<%# Eval("JobBalance", "{0:c}" %>'></asp:Label>                                                   
                                                        --%>

                                                     <asp:Label ID="lblPropertyType" runat="server" Text='<%# GetPropertyName(DataBinder.GetPropertyValue(Container.DataItem, "PublicJob"), DataBinder.GetPropertyValue(Container.DataItem, "PrivateJob"), DataBinder.GetPropertyValue(Container.DataItem, "ResidentialBox"), DataBinder.GetPropertyValue(Container.DataItem, "FederalJob")) %>'></asp:Label>


                                                    </td>
                                                     <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblBranchNum" runat="server" Text='<%# Eval("BranchNum")%>'></asp:Label></td>
                                                    </td>

                                                    <%--  <td style="width: 18%; max-width: 16%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobAdd1" runat="server" Text='<%# Eval("JobAdd1")%>'></asp:Label></td>        --%> 
                                                       
<%--                                                    <td style="word-break: break-all;"></td>
                                                    <td style="word-break: break-all;"></td>--%>
                                                    <td class="nowordwraptd"><b style="font-weight: bold;">Lien - </b>
                                                        <asp:Label ID="lblLienDate" runat="server" Text='<%#Utils.FormatDate(Eval("LienDeadlineDate"))%>'></asp:Label>
                                                         <asp:HiddenField ID="hdnLienDate" runat="Server" Value='<%#Eval("LienDate")%>' />
                                                    </td>
                                                    <td  class="nowordwraptd"><b style="font-weight: bold;">Suit - </b>
                                                        <asp:Label ID="lblSuit" runat="server" Text='<%#Utils.FormatDate(Eval("BondSuitDeadlineDate")) %>'></asp:Label></td>
                                                    <asp:HiddenField id="hdnBondSuitDate" runat="server" Value='<%#Utils.FormatDate(Eval("BondSuitDate")) %>' />
                                                    <asp:HiddenField id="hdnPublicJob" runat="server" Value='<%#Utils.FormatDate(Eval("PublicJob")) %>' />
                                                   
                                                     <td  class="nowordwraptd"><b style="font-weight: bold;">NOI - </b>
                                                        <asp:Label ID="lblNOIDeadline" runat="server" Text='<%# Utils.FormatDate(Eval("NOIDeadlineDate"))%>'></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                       <td style="width: 23%; max-width: 18%;" class="nowordwraptd">
                                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("ClientCustomer")%>'></asp:Label>

                                                    <td class="nowordwraptd">
                                                       <asp:Label ID="lblCRFSstatus" runat="server" Text='<%# String.Format("{0} - {1}", Eval("StatusCode"), Eval("JobStatusDescr")) %>'></asp:Label>
                                                      </td>
                                                 <%--  <td class="nowordwraptd">
                                                        <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityStateZip")%>'></asp:Label></td>--%>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="nowordwraptd"><b style="font-weight: bold;">Foreclose - </b>
                                                        <asp:Label ID="lblForeClose" runat="server" Text='<%#Utils.FormatDate(Eval("ForeclosureDeadlineDate"))%>'></asp:Label>
                                                      <asp:HiddenField ID="hdnForeclosureDate" runat="Server" Value='<%#Eval("ForeclosureDate")%>' />
                                                    </td>
                                                    <td class="nowordwraptd"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                            </asp:GridView>
                            <br />
                            <%-- '^^*** *** ^^--%>


                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-right: 0px">
                                <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left">
                                    <asp:Label ID="lblTableInfo" runat="server" Style="font-size: 14px"></asp:Label>
                                    <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageSize_Changed">
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="25" Value="25" />
                                        <asp:ListItem Text="50" Value="50" />
                                    </asp:DropDownList>
                                </div>
                                <div style="padding-right: 0px" class="col-md-4 col-sm-4 col-xs-4">
                                    <ul class="pagination pull-right">
                                        <asp:Repeater ID="rptPager" runat="server">
                                            <ItemTemplate>
                                                <li class="page-item">
                                                    <asp:LinkButton ID="lnkPage" CssClass='<%# If(Eval("Enabled") Or (Eval("Enabled") = False And Eval("Text") = "<<") Or (Eval("Enabled") = False And Eval("Text") = ">>"), "page-link ", "page-link activePage")%>' runat="server" CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' Text='<%#Eval("Text") %>' OnClick="Page_Changed">
                                                    </asp:LinkButton>

                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade modal-primary example-modal-lg" id="modelDivExport" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
                        tabindex="-1" style="display: none;" data-backdrop="static">
                        <div class="modal-dialog modal-center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>

                                    </button>

                                    <h3 class="modal-title">Export Job List !!</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">

                                        <h4 class="modal-title">What Type Of Export Would You Like ? </h4>
                                        <div class="form-group" style="padding-top: 20px; padding-left: 150px">

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PDFReport" runat="server" Checked="True" GroupName="ReportType" Text="PDF" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="SpreadSheet" runat="server" GroupName="ReportType" Text="Spreadsheet" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer" style="text-align: center">
                                    <asp:Button ID="btnExportReport" runat="server" CssClass="btn btn-primary" OnClick="btnExportReport_Click" Style="display: none" />
                                    <button type="button" id="btnSubmitReport" aria-label="Submit" class="btn btn-primary" onclick="modalhide();" />
                                    <span>Submit</span>
                                   

                                    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Cancel">
                                        <span>Cancel</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif" AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center" HorizontalOffset="150" VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
