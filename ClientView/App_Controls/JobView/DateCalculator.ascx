<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<style type="text/css">
    .style1 {
        width: 30%;
        height: 23px;
    }

    .style2 {
        width: 200px;
        height: 23px;
    }

    .button {
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('RentalView', 'Tools', 'Date Calculator');
        MainMenuToggle('liRentalToolsJobView');
        SubMenuToggle('liDateCalculatorJobView');
        //var PageName = window.location.search.substring(1);
        //if (PageName == "lienview.DateCalculator.JobView") {
         
        //}
        //else {
        //    SetHeaderBreadCrumb('LienView', 'Tools', 'Date Calculator');
        //    MainMenuToggle('liTools');
        //    SubMenuToggle('liDateCalculator');
        //}
       
        //$('#liTools').addClass('site-menu-item has-sub active open');
        //$('#liDateCalculator').addClass('site-menu-item active');
        return false;

    }
   
</script>
<script runat="server">
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Page.IsPostBack = False Then
            Me.Calendar1.Value = Today
        Else
             ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If
       
    End Sub


    
    Protected Sub btnGetDate_Click(sender As Object, e As System.EventArgs)
        
        Dim Days As Double
        Days = Val(Me.NumOfDays.Text)
        If Me.Add.Checked = True Then
            Me.txtNewDate.Text = DateAdd(DateInterval.Day, Days, CDate(Me.Calendar1.Value))
        Else
            Me.txtNewDate.Text = DateAdd(DateInterval.Day, Days * -1, CDate(Me.Calendar1.Value))
        End If
        
    End Sub
</script>

                                    <div id="modcontainer" style="MARGIN: 10px; WIDTH: 100%">
                                        <h1 class="panelheader">Date Calculator</h1>
                                        <div class="body">
                                            <div class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Start Date:</label>
                                                    <div class="col-md-7">
                                                        <span style="display: flex;">
                                                            <SiteControls:Calendar ID="Calendar1" Value="TODAY" IsReadOnly="false" IsRequired="true" ErrorMessage="From Date" runat="server" />
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Add:</label>
                                                    <div class="col-md-7">
                                                        <div class="example-wrap" style="margin-bottom: 0;">
                                                            <div class="radio-custom radio-default">
                                                                <asp:RadioButton ID="Add" runat="server" Checked="True"
                                                                    GroupName="optAddOrSubtract"  Text=" "></asp:RadioButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Subtract:</label>
                                                    <div class="col-md-7">
                                                        <div class="example-wrap" style="margin-bottom: 0;">
                                                            <div class="radio-custom radio-default">
                                                                <asp:RadioButton ID="Subtract" runat="server" GroupName="optAddOrSubtract" Text=" "></asp:RadioButton>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Number Of Days:</label>
                                                    <div class="col-md-7">
                                                        <asp:TextBox ID="NumOfDays" IsRequired="true" runat="server" Width="65px" CssClass="form-control"></asp:TextBox>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                                                            ControlToValidate="NumOfDays"
                                                            ErrorMessage="Please enter the Number of Days"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-2">
                                                        <asp:Button ID="btnGetDate" runat="server" Text="GET NEW DATE"
                                                            CssClass="btn btn-primary"
                                                            CausesValidation="true" Font-Bold="True" OnClick="btnGetDate_Click"
                                                            Height="37px" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">New Date:</label>
                                                    <div class="col-md-2">
                                                        <asp:TextBox ID="txtNewDate" runat="server" ReadOnly="True" BackColor="#CCFFFF"
                                                            CssClass="form-control"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                           