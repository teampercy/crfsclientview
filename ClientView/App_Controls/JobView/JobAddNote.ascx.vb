
Imports CRF.CLIENTVIEW.BLL

Partial Class App_Controls_LienView_JobAddNote
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim MYITEM As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
    Public Sub ClearData(ByVal JobId As String)
        Me.ViewState("JobId") = JobId
        Me.Note.Text = ""

    End Sub
    Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
        Dim s As String = ""

    End Sub
    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Me.ViewState("JobId"))
        'Dim hdnActicetab As New System.Web.UI.WebControls.HiddenField
        'hdnActicetab = Me.Parent.FindControl("hddbTabName")
        'Dim asd As String
        'asd = hdnActicetab.Value
        'hdnActicetab.Value = "Notes"
        'TextBox txtBuyer = (TextBox)this.Parent.FindControl("txtBuyer");
        'Dim hdnTabName As System.Web.UI.hdn  Parent.FindControl("hddbTabName")
        'Session("currenttab") = "NOTES"
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NewCompanyNotSaved", "ActivateTabSet()", True)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsSets", "ActivateTabSet();", True)
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        With mynote
            .JobId = Me.ViewState("JobId")
            .DateCreated = Now()
            .Note = Me.Note.Text
            .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .ClientView = True
            .EnteredByUserId = Me.UserInfo.UserInfo.ID
            .ActionTypeId = 0
            .NoteTypeId = 1
        End With
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
        ClearData(Me.ViewState("JobId"))
        RaiseEvent ItemSaved()
        hdnIsReviewStatus.Value = False
        hdnIsNotifyStatus.Value = False
        Dim MYSQL As String = "Select isnull(IsNotifyStatus,0) as IsNotifyStatus ,isnull(IsReviewStatus,0) as IsReviewStatus from Job , jobstatus where job.statuscode=jobstatus.jobstatus "
        MYSQL += "and Job.Id = " & Me.ViewState("JobId")
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        If (MYDT.Tables(0).Rows.Count > 0) Then
            hdnIsReviewStatus.Value = MYDT.Tables(0).Rows(0)("IsReviewStatus")
            hdnIsNotifyStatus.Value = MYDT.Tables(0).Rows(0)("IsNotifyStatus")
        End If

        If (hdnIsReviewStatus.Value = True Or hdnIsNotifyStatus.Value = True) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ShowReviewModel", "ShowReviewModel()", True)
        End If

    End Sub
    Protected Sub btnReview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReview.Click
        Dim myAction As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActionHistory
        With myAction
            .JobId = Me.ViewState("JobId")
            .ActionId = 487
            .DateCreated = Now()
            .RequestedBy = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .RequestedByUserId = Me.UserInfo.UserInfo.ID
        End With
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myAction)
        If (hdnIsReviewStatus.Value = True Or hdnIsNotifyStatus.Value = True) Then
            Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.ViewState("JobId"), myjob)
            If (hdnIsReviewStatus.Value = True) Then
                myjob.StatusCodeId = GetStatusId("AJR")
                myjob.StatusCode = "AJR"
            End If
            If (hdnIsNotifyStatus.Value = True) Then
                Dim MYSQL As String = "Select Id from jobviewstatus where Jobviewstatus='UNRC'"
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                If (MYDT.Tables(0).Rows.Count > 0) Then
                    myjob.JVStatusCodeId = MYDT.Tables(0).Rows(0)("Id")
                End If
                myjob.JVStatusCode = "UNRC"
            End If
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        End If
        RaiseEvent ItemSaved()
    End Sub
    Protected Sub btnReviewCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReviewCancel.Click
        If (hdnIsReviewStatus.Value = True) Then
            Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.ViewState("JobId"), myjob)
            If (hdnIsReviewStatus.Value = True) Then
                Dim MYSQL As String = "Select Id from jobviewstatus where Jobviewstatus='RJIP'"
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                If (MYDT.Tables(0).Rows.Count > 0) Then
                    myjob.JVStatusCodeId = MYDT.Tables(0).Rows(0)("Id")
                End If
                myjob.JVStatusCode = "RJIP"
            End If
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        End If
        RaiseEvent ItemSaved()
    End Sub

    Public Function GetStatusId(ByVal statuscode As String)
        Dim statusid As Integer = 0
        Dim MYSQL As String = "Select Id from jobstatus where Jobstatus='" + statuscode + "'"
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        If (MYDT.Tables(0).Rows.Count > 0) Then
            statusid = MYDT.Tables(0).Rows(0)("Id")
        End If
        Return statusid
    End Function
End Class
