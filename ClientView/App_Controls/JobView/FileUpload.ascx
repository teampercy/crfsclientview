<%@ Control Language="VB" AutoEventWireup="false" CodeFile="FileUpload.ascx.vb" Inherits="App_Controls__Custom_LienView_FileUpload" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<script type="text/javascript">
    function setSelectedDocTypeId(id) {
        $('#' + '<%=hdnDocType.ClientID%>').val(id);
        $('#btnDropDocType').html(id + "<span class='caret' ></span>");
        return false;
    }
    function EnableFileUploadButton()
    {        
        document.getElementById('<%=btnFileUpLoadOk.ClientID%>').disabled = false;
    }
    function showLodingImage() {
                
        $('#loadingImageDiv').show();
    } // 5 seconds }

    function hideLodingImage() {
        // alert('hideLodingImage');
      
        // debugger;             
        var tmr = setInterval(function () { zzzhideLodingImageActul(tmr) }, 1000);
    }
    function zzzhideLodingImageActul(tmr) { $('#loadingImageDiv').hide(); clearInterval(tmr); }   
    function BindFileUoload() {
        parent.BindFileUoloadList();
    }
    function SetDatePicker() {
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            showButtonPanel: true,
        });
    }
</script>
<style type="text/css">
    .scrollable-menu {
        height: auto;
        max-height: 180px;
        overflow-x: hidden;
    }

        .scrollable-menu::-webkit-scrollbar {
            -webkit-appearance: none;
            width: 4px;
        }

        .scrollable-menu::-webkit-scrollbar-thumb {
            border-radius: 3px;
            background-color: lightgray;
            -webkit-box-shadow: 0 0 1px rgba(255,255,255,.75);
        }

    .dropify-wrapper {
        height: 160px;
    }
        .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
    }
</style>
<div style="float: left;">
    <div class="form-horizontal">
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Document Date:</label>
            <div class="col-xs-4" style="display: flex;">
                <asp:TextBox ID="txtdate" runat="server" CssClass="form-control datepicker"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Document Type:</label>
            <div class="col-xs-7" style="text-align: left;">
                <div class="btn-group" style="text-align: left;" align="left">
                    <button type="button" class="btn btn-default dropdown-toggle" style="min-width: 200px;" align="left"
                        data-toggle="dropdown" aria-expanded="false" id="btnDropDocType">
                        --Select--
                        <span class="caret" style="float: right; margin-top: 5px;"></span>
                    </button>
                    <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="ulDropDownState" style="overflow-y: auto; height: 250px;">
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('AZ Owner Ack')">AZ Owner Ack</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Bond Copy')">Bond Copy</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Building Permit')">Building Permit</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Job Info Sheet')">Job Info Sheet</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Job Info Fax')">Job Info Fax</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Miscellaneous')">Miscellaneous</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Notice of Commencement')">Notice of Commencement</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Notice of Completion')">Notice of Completion</a></li>
                        <li role="presentation"><a href="javascript:void(0)" role="menuitem" onclick="setSelectedDocTypeId('Surety Letter')">Surety Letter</a></li>
                    </ul>
                </div>
                <asp:HiddenField ID="hdnDocType" runat="server" Value="--Select--" />
                <asp:DropDownList ID="ddldoctype" runat="server" Style="display: none;">
                    <asp:ListItem Text="--SELECT--" Selected="true"></asp:ListItem>
                    <asp:ListItem Text="AZ Owner Ack"></asp:ListItem>
                    <asp:ListItem Text="Bond Copy"></asp:ListItem>
                    <asp:ListItem Text="Building Permit"></asp:ListItem>
                    <asp:ListItem Text="Job Info Sheet"></asp:ListItem>
                    <asp:ListItem Text="Job Info Fax"></asp:ListItem>
                    <asp:ListItem Text="Miscellaneous"></asp:ListItem>
                    <asp:ListItem Text="Notice of Commencement"></asp:ListItem>
                    <asp:ListItem Text="Notice of Completion"></asp:ListItem>
                    <asp:ListItem Text="Surety Letter"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="form-group">
            <label class="col-xs-4 text-right control-label">Misc Info:</label>
            <div class="col-xs-8">
                <asp:TextBox ID="Txtmiscinfo" runat="server" TextMode="MultiLine" Font-Names="Arial" Font-Size="small" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-4 control-label text-right">
                File To Upload :               
            </div>
            <div class="col-xs-8">
                <%-- <asp:FileUpload ID="FileUpLoad1" AlternateText="You cannot upload files" runat="server"
                        size="40" Height="25px" Visible="false" />
                --%>


             <%--   <input id="UploadedFile" name="fileDragnDrop" data-max-file-size="4M" data-plugin="dropify" type="file" data-max-file-limit="1">--%>

               <span class="btn btn-default btn-file btn_block" style="width: 340px;">
    Select File... <input type="file" id="UploadedFile" data-max-file-limit="1" >
</span>

            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-8" style="text-align: left; text-wrap: normal; font-weight: normal">
                <%--<asp:Label ID="Label3" runat="server"
                    Text="Attached file cannot be larger than 4mbs. If your attachment is larger,please break it up into multiple files and attach them separately."
                    ForeColor="Red" />--%>
                <asp:Label ID="Label1" runat="server"
                    Text="Attached file cannot be larger than 4mbs.If your attachment is larger,please break it up into multiple files and attach them separately."
                    ForeColor="Red" />
                <br />
                <label class="msg row-label" style="text-align: left; padding: 0px">Select File to Upload: (PDF,DOC,TXT,CSV,XLS,JPG,GIF,BMP,ZIP Only)</label>


            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <div class="file-wrap container-fluid">
                    <div class="file-list row"></div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-8">
                <asp:Button ID="btnFileUpLoadOk" ClientIDMode="Static" runat="server" Style="float: left" Enabled="false" CausesValidation="true"
                    Text="Save Document" CssClass="btn btn-primary" />

            </div>
        </div>
           <div class="form-group">
            <div class="col-xs-4">
            </div>
            <div class="col-xs-8">
                <div>
                   <%-- <asp:LinkButton ID="LinkButton1" runat="server" CssClass="close" OnClientClick="$find('FileUpLoad').hide(); return false;" />--%>
                     <asp:LinkButton ID="LinkButton1" runat="server" CssClass="close" OnClientClick ="javascript:DeletFileUpload()" />
                </div>
                <div style="padding-left: 10px; text-align: left">
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtDate"
                        ErrorMessage="Enter Date in MM/DD/YYYY Format" Operator="DataTypeCheck" Type="Date" />

                    <br />
                    <asp:Label ID="lblmsg" runat="server" ForeColor="red"></asp:Label>
                </div>

            </div>
        </div>
    </div>

</div>
<%--    <div style="text-align: left">
          <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FileUpLoad1"
            Text="Select File To Upload"></asp:RequiredFieldValidator>
    </div>--%>



<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
<script>


    function DeletFileUpload() {
        debugger;
        //alert("The button clicked.");
        $('#UploadFile7_FileUpLoad1').val('');
        $('#UploadFile7_Txtmiscinfo').val('');
        setSelectedDocTypeId('--SELECT--');

    }

    $(function () {

        //$("#UploadedFile").on("change", function () {
        //    alert('hi');
        //    if($("#UploadedFile")[0].files.length > 1); {
        //        alert("You can select only 2 images");
        //    }//else alert('success');
        ////else
        ////{ alert("Ysuccess");
        ////}

        //});

        $("#btnFileUpLoadOk").click(function (e) {         
            debugger;
            var Flag = true;
            var ValidationMessage = "";
            if ($("#<%=hdnDocType.ClientID%>").val() == '--Select--') {
                    Flag = false;
                    ValidationMessage = " Select Document Type1<br>";
                }
           
        $.ajax({
            type: "POST",
            url: "Default.aspx/GetFileUploadExtension",
            contentType: "application/json; charset=utf-8",
            async: false,
            dataType: "json",
            success: function (response) {
                if (response.d == "") {
                    Flag = false;
                    ValidationMessage = ValidationMessage + " Select File to upload";
                }
                else {
                    var AllowedExt = new Array("jpg", "jpeg", "gif", "png", "bmp", "doc", "docx", "txt", "csv", "xls", "zip", "pdf","xlsx");
                    var ext = response.d;
                    ext = ext.substring(1);
                    if (AllowedExt.indexOf(ext.toLowerCase()) < 0) {
                        Flag = false;
                        ValidationMessage = ValidationMessage + " Select Valid File<br>";
                    }
                }
            },
            failure: function (response) {
                alert(response.d);
                Flag = false;
            }
        });
            //PageMethods.GetFileUploadExtension(FileUploadExtension);
            //var AllowedExt = new Array("jpg", "jpeg", "gif", "png", "bmp", "doc","docx", "txt", "csv", "xls", "zip");
            //if ($("#UploadedFile").val() == '') {
            //    Flag = false;
            //    ValidationMessage = ValidationMessage + " Select File to upload";
            //}
            //else {
            //    var ext = $("#UploadedFile").val().substring($("#UploadedFile").val().lastIndexOf(".") + 1, $("#UploadedFile").val().length);
            //    if (AllowedExt.indexOf(ext.toLowerCase()) < 0) {
            //        Flag = false;
            //        ValidationMessage = ValidationMessage + " Select Valid File<br>";
            //    }
            //}


        $("#<%= lblmsg.ClientID%>").html(ValidationMessage);
            if (Flag) {
                showLodingImage();
            }
            else {
                hideLodingImage();
            }
            return Flag;
        });

    })

</script>
