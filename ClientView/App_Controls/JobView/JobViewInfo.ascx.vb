﻿Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Imports System.Data
Imports System.Net.Mail
Imports System.IO

Partial Class App_Controls_JobView_JobViewInfo
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Dim myview As HDS.DAL.COMMON.TableView
    Dim dsjobinfo As System.Data.DataSet
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobTexasRequest
    Dim myverified As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobVerifiedSentRequest
    Dim mywaiver As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobWaiverLog
    Dim itemid As String
    Dim tabindex As String
    Dim s1 As String
    Dim sremail As String
    Dim JobAddSubjectLine As String
    Protected WithEvents btnDeleteItem As LinkButton
    Protected WithEvents btnPrntNotice As LinkButton
    Protected WithEvents btnPrntWaiver As LinkButton
    Protected WithEvents btnPrntSForm As LinkButton

    Protected WithEvents btnEditWaiver As LinkButton
    Protected WithEvents btnDeleteWaiver As LinkButton
    Protected WithEvents btnEmailWaiver As LinkButton
    Protected WithEvents btnEditSForm As LinkButton
    Protected WithEvents btnDeleteSForm As LinkButton

    Protected WithEvents btnEditTReq As LinkButton
    Protected WithEvents btnPrntTReq As LinkButton
    Protected WithEvents btnDeleteTReq As LinkButton

    Protected WithEvents btnEditVReq As LinkButton
    Protected WithEvents btnPrntVReq As LinkButton
    Protected WithEvents btnDeleteVReq As LinkButton

    Protected WithEvents btnEditINVC As LinkButton
    'Protected WithEvents btnPrntINVC As LinkButton
    Protected WithEvents btnDeleteINVC As LinkButton

    Protected WithEvents btnViewDoc As LinkButton
    Protected WithEvents btnlnk As LinkButton
    Protected WithEvents btnViewNote As LinkButton
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim myStateInfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim mycustomer As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomer
    Dim mycustomeranalytic As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomerAnalytic
    Dim myportalusers As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users
    Public Property [Me] As Object

    Protected WithEvents btnRecension As LinkButton
    Dim myJobLegalParties As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobLegalParties
    Dim myStateForms As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateForms
    Dim StateCode As String
    Dim dsJobWaiverLogByInfo As System.Data.DataTable
    Dim dsJobWaiverLogByRecensionLetter As System.Data.DataTable
    Dim dsJobLegalParties As System.Data.DataTable
    Dim JobWaiverLogId As String
    Dim LegalPartyId As String
    Dim JobLegalPartiesTypeCode As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Me.itemid = qs.GetParameter("ItemId")
        'markparimal-uploadfile:jobid value is taken in session so that it can be used in file upload page.
        Session("jobid") = Me.itemid

        Session("bitjobid") = 1
        Dim test As String = Request.Url.Segments.Last()
        hddbTabName.Value = Request.Form(hddbTabName.UniqueID)
        If (hddbTabName.Value = "") Then
            hddbTabName.Value = "MainInfo"
        End If
        If Me.CurrentUser.UserManager = 0 Then

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideDiveNotes", "hideDiveNotes();", True)

        End If

        If Me.Page.IsPostBack = False Then
            'markparimal-navigation issues 1/2/2011
            Me.Session("WaiverTab") = qs.GetParameter("tabstate")
            'Me.Page.ClientScript.RegisterStartupScript(Page.GetType, "Upload Completed", "test('" & Session("WaiverTab") & "');", True)
            If Me.Session("WaiverTab") = "WAIVER" Then
                WaiverTab()
            End If
            If Me.Session("WaiverTab") = "SFORM" Then
                SFORMTab()
            End If
            ' end navigation issues 


            Me.ViewState("VIEW") = "MAIN"
            'If qs.HasParameter("VIEW") Then
            '    Me.ViewState("VIEW") = qs.GetParameter("VIEW").ToUpper()
            'End If
            Me.Session("LIST") = Nothing
            If qs.HasParameter("LIST") Then
                Me.Session("LIST") = qs.GetParameter("LIST")
            End If

            Me.MultiView1.SetActiveView(Me.View1)
            LoadData(qs.GetParameter("ItemId"))

            If qs.HasParameter("VIEW") Then
                Me.ViewState("VIEW") = qs.GetParameter("VIEW").ToUpper()
                If qs.HasParameter("formid") = True And qs.GetParameter("VIEW") = "WAIVER" Then
                    PrintWaiver(qs.GetParameter("formid"))
                End If
                If qs.HasParameter("formid") = True And qs.GetParameter("VIEW") = "SFORM" Then
                    PrintSFORM(qs.GetParameter("formid"))
                End If
                '##100
                If Me.ViewState("VIEW") = "TREQ" Then
                    hddbTabName.Value = "TXRequests"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActives", "ActivateTab('" & hddbTabName.Value & "');", True)
                End If

                If Me.ViewState("VIEW") = "INVC" Then

                    hddbTabName.Value = "Invoices"
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActives", "ActivateTab('" & hddbTabName.Value & "');", True)
                End If
            End If

        Else
            If gvwLegalParties.Rows.Count > 0 Then
                Me.gvwLegalParties.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncLegalPartiesList", "CallSuccessFuncLegalPartiesList();", True)
            End If
            If gvwNotes.Rows.Count > 0 Then
                Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncNotesList", "CallSuccessFuncNotesList();", True)
            End If
            If gvwNotices.Rows.Count > 0 Then
                Me.gvwNotices.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncNoticesList", "CallSuccessFuncNoticesList();", True)
            End If
            If gvwDocs.Rows.Count > 0 Then
                Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDocsList", "CallSuccessFuncDocsList();", True)
            End If
            If gvwWaivers.Rows.Count > 0 Then
                Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncWaiverList", "CallSuccessFuncWaiverList();", True)
            End If
            If gvwStateForms.Rows.Count > 0 Then
                Me.gvwStateForms.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncStateList", "CallSuccessFuncStateList();", True)
            End If
            If gvInvoiceGrid.Rows.Count > 0 Then
                Me.gvInvoiceGrid.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncInvoiceList", "CallSuccessFuncInvoiceList();", True)
            End If
            If gvwMergeJobs.Rows.Count > 0 Then
                Me.gvwMergeJobs.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncJobMergeList", "CallSuccessFuncJobMergeList();", True)
            End If
            If gvwTexasReq.Rows.Count > 0 Then
                Me.gvwTexasReq.HeaderRow.TableSection = TableRowSection.TableHeader
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncTexasList", "CallSuccessFuncTexasList();", True)
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDisabledNoticeRequest", "SetDisabledNoticeRequest();", True)

            LoadData(qs.GetParameter("ItemId"))

        End If
        Dim strpagename As String
        If (Not qs.GetParameter("PageName") Is Nothing) Then
            strpagename = qs.GetParameter("PageName")
        Else
            strpagename = String.Empty
        End If

            SetCurrentTab()
        Catch ex As Exception
            Dim mymsg As String = ex.Message
        End Try

    End Sub
    Public Overloads Sub LoadData(ByVal ajobid As String)
        Try


            Me.ViewState("myviewjobid") = ajobid

            Me.ValidationSummary1.Visible = False
        dsjobinfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobViewByInfo(ajobid)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(ajobid, myjob)
        'dtjobinfo = dsjobinfo.Tables(17)
        'Me.ViewState("myviewjobid") = dsjobinfo
        Me.Session("currentjobinfo") = dsjobinfo
        Dim myview As New VIEWS.vwJobInfoJV
        HDS.WEBLIB.Common.Mapper.FillEntity(dsjobinfo.Tables(0), myview)
        Me.JobId.Text = myview.JobId
        Me.JobNumber.Text = myview.JobNum
        Me.JobName.Text = myview.JobName
        Me.JobAdd1.Text = myview.JobAdd1
        Me.JobAdd2.Text = myview.JobAdd2
        Me.JobAdd3.Text = myview.JobCity & ", " & myview.JobState & " " & myview.JobZip
        Me.JobCity.Text = myview.JobCity
        Me.JobState.Text = myview.JobState
        StateCode = myview.JobState
        Me.JobZip.Text = myview.JobZip
        Me.JobNo.Text = myview.JobId
        Me.EstBalance.Text = Strings.FormatCurrency(myview.EstBalance)
        Me.BondDeadlineDate.Value = myview.BondDeadlineDate
        Me.BondSent.Value = myview.BondDate
        Me.NOCDate.Value = myview.NOCDate
        Me.SNDeadlineDate.Value = Utils.FormatDate(myview.SNDeadlineDate)
        Me.NoticeRequested.Value = myview.DateAssigned
        ViewState("JobAddSubjectLine") = "(" + Convert.ToString(myview.JobId) + ") " + myview.JobAdd1 + " " + myview.JobCity + " " + myview.JobState
        'If Me.NoticeRequested.Value <> "" Then
        '    IsNoticeRequested.Value = True
        'Else
        '    IsNoticeRequested.Value = False
        'End If

            '^^*** Created by pooja 04/24/2021*** ^^
            Dim ClientContractinfo As HDS.DAL.COMMON.TableView
            ' Dim s As Boolean
            ClientContractinfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientContractList(myview.ClientId)
            ' For i As Integer = 0 To ClientContractinfo.Count - 1
            Dim UseJobViewMenuValue As String = ClientContractinfo.RowItem("UseJobViewMenu")
            Dim IsClientViewJob As String = ClientContractinfo.RowItem("IsClientViewJob")
            Me.hdnUseJobViewMenu.Value = UseJobViewMenuValue
            Me.hdnIsClientViewJob.Value = IsClientViewJob

            'Me.hdnUseJobViewMenu.Value = "True"
            'Me.hdnIsClientViewJob.Value = "False"

            ' Next

            '^^***     *** ^^
            hdnJobId.Value = Convert.ToString(myview.JobId)

        Me.hdnDateAssigned.Value = Utils.FormatDate(myview.DateAssigned)
        If Me.hdnDateAssigned.Value <> "" Then
            IsNoticeRequested.Value = True
        Else
            IsNoticeRequested.Value = False
        End If


        If myview.NoticeDeadlineDate.ToString <> "" Then
            Dim NDLDate As Date = myview.NoticeDeadlineDate
            If DateDiff(DateInterval.Day, Now, NDLDate) < 10 And myview.NoticeSent = DateTime.MinValue Then
                Me.NOIDeadlineDate.CssClass += "  deadlinecolor"
            End If
        End If

        If myview.LienDeadlineDate.ToString <> "" Then
            Dim LDLDate As Date = myview.LienDeadlineDate
            If DateDiff(DateInterval.Day, Now, LDLDate) < 30 And myview.LienDate = DateTime.MinValue Then
                Me.LienDeadlineDate.CssClass += "  deadlinecolor"
            End If
        End If
        If myview.BondDeadlineDate.ToString <> "" Then
            Dim BDLDate As Date = myview.BondDeadlineDate
            If DateDiff(DateInterval.Day, Now, BDLDate) < 30 And myview.BondDate = DateTime.MinValue Then
                Me.BondDeadlineDate.CssClass += "  deadlinecolor"
            End If
        End If
        If myview.SNDeadlineDate.ToString <> "" Then
            Dim SNDLDate As Date = myview.SNDeadlineDate
            If DateDiff(DateInterval.Day, Now, SNDLDate) < 30 And myview.SNDate = DateTime.MinValue Then
                Me.SNDeadlineDate.CssClass += "  deadlinecolor"
            End If
        End If
        If myjob.ForeclosureDeadlineDate.ToString <> "" Then
            Dim FCDLDate As Date = myjob.ForeclosureDeadlineDate
            If DateDiff(DateInterval.Day, Now, FCDLDate) < 30 And myjob.ForeclosureDate = DateTime.MinValue Then
                Me.ForeclosureDeadlineDate.CssClass += "  deadlinecolor"
            End If
        End If
        If myjob.BondSuitDeadlineDate.ToString <> "" Then
            Dim BSDLDate As Date = myjob.BondSuitDeadlineDate
            If DateDiff(DateInterval.Day, Now, BSDLDate) < 30 And myjob.BondSuitDate = DateTime.MinValue Then
                Me.BondSuitDeadline.CssClass += "  deadlinecolor"
            End If
        End If

        Me.SNSent.Value = myview.SNDate

        Me.NOIDeadlineDate.Value = myview.NoticeDeadlineDate
        Me.NOIDeadlineDate2.Value = myview.NOIDeadlineDate
        'Me.chkPublicJob.Checked = myview.PublicJob
        Me.JobBalance.Value = myview.JobBalance
        Me.NoticeSent.Value = myview.NoticeSent
        'Me.chkFederal.Checked = myview.FederalJob
        Me.LienDeadlineDate.Value = myview.LienDeadlineDate
        'Me.chkResindential.Checked = myview.ResidentialBox
        If myview.PublicJob = True Then
            Me.PropertyType.Text = "Public"
        ElseIf myview.FederalJob = True Then
            Me.PropertyType.Text = "Federal"
        ElseIf myview.ResidentialBox = True Then
            Me.PropertyType.Text = "Residential"
        Else
            Me.PropertyType.Text = "Private"
        End If
        'Me.LienDate.Value = myview.LienDate
        Me.LienDate.Value = myview.FileDate
        'Me.Desk.Text = myview.JVDeskNum & " - " & myview.JVDeskName
        Me.txtFilterKey.Text = myview.FilterKey
        Me.Customers.Text = myview.CustName
        Me.txtCustRef.Text = myview.CustRefNum
        'Me.StatusCode.Value = myview.StatusCode & "-" & Strings.Left(myview.JobStatusDescr, 90)
        Me.StatusCode.Value = myview.JVStatus & " - " & Strings.Left(myview.JVStatusDesc, 90)
        Me.CRFSStatus.Text = myview.StatusCode & " - " & Strings.Left(myview.JobStatusDescr, 90)
        'commented on 2/9/2017
        ' Me.txtCreditrating.Text = myview.CreditRating
        If (Not (dsjobinfo.Tables(4) Is Nothing) And dsjobinfo.Tables(4).Rows.Count > 0) Then
            Me.hdnCustId.Value = dsjobinfo.Tables(4).Rows(0)("CustId").ToString()
            Me.txtCustomerName.Text = dsjobinfo.Tables(4).Rows(0)("ClientCustomer").ToString()
            Me.txtCustomerContact.Text = dsjobinfo.Tables(4).Rows(0)("ContactName").ToString()
            Me.txtCustAdd1.Text = dsjobinfo.Tables(4).Rows(0)("AddressLine1").ToString()
            Me.txtCustAdd2.Text = dsjobinfo.Tables(4).Rows(0)("AddressLine2").ToString()
            Me.txtCustCity.Text = dsjobinfo.Tables(4).Rows(0)("City").ToString()
            Me.txtCustState.Text = dsjobinfo.Tables(4).Rows(0)("State").ToString()
            Me.txtCustZip.Text = dsjobinfo.Tables(4).Rows(0)("PostalCode").ToString()
            Me.txtCustEmail.Text = dsjobinfo.Tables(4).Rows(0)("Email").ToString()
            Me.txtCustPhone.Text = Utils.FormatPhoneNo(dsjobinfo.Tables(4).Rows(0)("Telephone1").ToString())
            Me.txtCustFax.Text = Utils.FormatPhoneNo(dsjobinfo.Tables(4).Rows(0)("Fax").ToString())
            Me.CCRUpdated.Value = dsjobinfo.Tables(4).Rows(0)("RiskRatingUpdateDate").ToString()
            Me.txtOldestOpenInvoice.Value = dsjobinfo.Tables(4).Rows(0)("OldestInvoiceDate").ToString()
            Me.txtLastPaymentDate.Value = dsjobinfo.Tables(4).Rows(0)("LastPayDate").ToString()
            Me.txtTotalOwed.Value = dsjobinfo.Tables(4).Rows(0)("TotalBalance").ToString()
            Me.chkDonotNotice.Checked = Convert.ToBoolean(dsjobinfo.Tables(4).Rows(0)("NoNTO"))

            '^^*** commented by pooja 11/09/2020*** ^^
            ' Me.txtCreditrating.Text = dsjobinfo.Tables(4).Rows(0)("RiskRating").ToString()
            '^^***     *** ^^

            '^^*** commented by pooja 11/09/2020*** ^^
            Me.txtCreditrating.Text = dsjobinfo.Tables(4).Rows(0)("CreditScore").ToString()
            '^^***     *** ^^
        End If

        Me.txtAPNNumber.Text = myview.APNNum
        Me.txtPONumber.Text = myview.PONum
        Me.txtBondNum.Text = myview.BondNum
        Me.txtpermitNum.Text = myview.BuildingPermitNum
        Me.txtSpecialInst.Text = myview.SpecialInstruction.ToString()
        Me.txtLegalDesc.Text = myview.Note.ToString()
        Me.txtEquiprate.Text = myview.EquipRate.ToString()
        Me.txtEquipDesc.Text = myview.EquipRental.ToString()

        If (Not (myjob Is Nothing)) Then
            Me.StartDate.Value = myjob.StartDate
            Me.EndDate.Value = myjob.EndDate
            Me.ForeclosureDeadlineDate.Value = myjob.ForeclosureDeadlineDate
            Me.ForeclosureDate.Value = myjob.ForeclosureDate
            Me.BondSuitDeadline.Value = myjob.BondSuitDeadlineDate
            Me.BondSuitDate.Value = myjob.BondSuitDate
            Me.TNLastDateWorkPerformed.Value = myjob.EndDate
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            Me.hdnJobState.Value = myjob.JobState
            '''''''''''''''''''''''''''''''''''''''''''''''''''
        End If


        Me.gvwNotes.DataSource = dsjobinfo.Tables(1)
        Me.gvwNotes.DataBind()

        If Not Me.gvwNotes.Rows.Count = 0 Then
            Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        Me.gvwLegalParties.DataSource = dsjobinfo.Tables(3)
        Me.gvwLegalParties.DataBind()
        If Not Me.gvwLegalParties.Rows.Count = 0 Then
            Me.gvwLegalParties.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncLegalPartiesList", "CallSuccessFuncLegalPartiesList();", True)
        Me.gvwNotices.DataSource = dsjobinfo.Tables(2)
        Me.gvwNotices.DataBind()
        If Not Me.gvwNotices.Rows.Count = 0 Then
            Me.gvwNotices.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        Me.gvwWaivers.DataSource = dsjobinfo.Tables(8)
        Me.gvwWaivers.DataBind()
        If Not Me.gvwWaivers.Rows.Count = 0 Then
            Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        Me.gvwStateForms.DataSource = dsjobinfo.Tables(11)
        Me.gvwStateForms.DataBind()
        If Not Me.gvwStateForms.Rows.Count = 0 Then
            Me.gvwStateForms.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        Me.gvwMergeJobs.DataSource = dsjobinfo.Tables(16)
        Me.gvwMergeJobs.DataBind()
        If Not Me.gvwMergeJobs.Rows.Count = 0 Then
            Me.gvwMergeJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If myview.JobId = myview.ParentJobId And myview.MergedJobBalance > 0 Then
            Me.JobBalance.Text = Strings.FormatCurrency(myview.MergedJobBalance)
        Else
            Me.JobBalance.Text = Strings.FormatCurrency(myview.JobBalance)
        End If

        Me.gvwTexasReq.DataSource = dsjobinfo.Tables(10)
        Me.gvwTexasReq.DataBind()
        If Not Me.gvwTexasReq.Rows.Count = 0 Then
            Me.gvwTexasReq.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If (Not (dsjobinfo.Tables(17) Is Nothing) And dsjobinfo.Tables(17).Rows.Count > 0) Then
            Dim view As New DataView
            Dim strquery As String = ""
            strquery = "ContractType In ('O','E') AND NOT (ClientStatus = 'C' AND CurrentBalance <= 0)"
            view.Table = dsjobinfo.Tables(17)
            view.RowFilter = strquery
            Session("openinvoice") = view

            'We are facing problem where in Table 17 duplicate RANum record generated due to which duplicate record display in Invoice Tab
            'Code to remove duplicate rows
            Dim dtCopyTable As DataTable = New DataTable()
            Dim dtResults As DataTable = dsjobinfo.Tables(17).Clone
            Dim prevDistinctRANum As String = ""
            dtCopyTable = view.ToTable()

                ''^^*** commented by pooja 05/26/2021*** ^^
                'For Each row As DataRow In dtCopyTable.Rows
                '    Dim DistinctRANum As String = row("RANum").ToString
                '    If (prevDistinctRANum <> DistinctRANum) Then
                '        dtResults.ImportRow(row)
                '        prevDistinctRANum = DistinctRANum
                '    End If
                'Next
                ''^^***  *** ^^
                ''^^*** Created by pooja 05/27/2021*** ^^
                'Distinct RANum not showing OriginalBalance and  CurrentBalance value'
                'code to show OriginalBalance and  CurrentBalance value of Distinct RANum'
                'For Each row As DataRow In dtCopyTable.Rows
                '    Dim DistinctRANum As String = row("RANum").ToString
                '    If (prevDistinctRANum <> DistinctRANum And (Not IsDBNull(row("OriginalBalance")) And Not IsDBNull(row("CurrentBalance")))) Then
                '        dtResults.ImportRow(row)
                '        prevDistinctRANum = DistinctRANum
                '    End If
                'Next
                ''^^***  *** ^^
                ''^^*** commented by pooja 05/26/2021*** ^^
                For Each row As DataRow In dtCopyTable.Rows
                    Dim DistinctRANum As String = row("RANum").ToString
                    If (prevDistinctRANum <> DistinctRANum) Then

                        If (IsDBNull(row("OriginalBalance"))) Then
                            row("OriginalBalance") = "0.00"

                        End If
                        If (IsDBNull(row("CurrentBalance"))) Then
                            row("CurrentBalance") = "0.00"

                        End If
                        dtResults.ImportRow(row)
                        prevDistinctRANum = DistinctRANum
                    End If
                Next

                If (dtResults.Rows.Count = 0) Then
                dtResults = Nothing
            End If
            'End Code

            Me.rptRentalInvoice.DataSource = dtResults
            Me.rptRentalInvoice.DataBind()

            'Me.rptRentalInvoice.DataSource = view.ToTable()
            'Me.rptRentalInvoice.DataBind()
            Me.rptRentalInvoiceNoticeRequest.DataSource = view.ToTable()
            Me.rptRentalInvoiceNoticeRequest.DataBind()

            End If
            If (Not (dsjobinfo.Tables(18) Is Nothing) And dsjobinfo.Tables(18).Rows.Count > 0) Then
                Dim view As New DataView
                Dim strquery As String = ""
                'strquery = "ContractType NOT In ('O','E')"
                strquery = "ContractType  In ('C','E')"
                view.Table = dsjobinfo.Tables(18)
                view.RowFilter = strquery
                Me.gvInvoiceGrid.DataSource = view.ToTable()
                Me.gvInvoiceGrid.DataBind()
                If Not Me.gvInvoiceGrid.Rows.Count = 0 Then
                    Me.gvInvoiceGrid.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
            End If
            Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
            MYSQL += " Where JobId = " & myview.JobId
            'Dim view1 As New DataView
            Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
            'view1.Table = MYDT.Tables(0)

            Me.gvwDocs.DataSource = MYDT
            Me.gvwDocs.DataBind()
            If Not Me.gvwDocs.Rows.Count = 0 Then
                Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
            Me.panJobInfo.Visible = True

        Me.litJobInfo.Text = " CRF# (" & myview.ClientCode & "-" & Me.JobId.Text.Trim & ") "
        Me.litJobInfo.Text += " Job# (" & Me.JobNumber.Text.Trim & ") "
        Me.litJobInfo.Text += " JobName (" & Me.JobName.Text.Trim & ") "
        Me.litJobInfo.Text += " Cust Ref# (" & myview.CustRefNum.Trim & ") "
        'For Notice Request and TX Request Popup
        myStateInfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(myview.JobState)
        If myStateInfo.Monthly = True Then
            Me.hdnMonthly.Value = "True"
        Else
            Me.hdnMonthly.Value = "False"
        End If
        hdnState.Value = myview.JobState.Trim()
        hdnIsCancelled.Value = myview.IsCancelled
        If myview.JobState = "TX" Then
            IsTX60Day.Enabled = True
            IsTX90Day.Enabled = True
        Else
            IsTX60Day.Enabled = False
            IsTX90Day.Enabled = False
        End If

        Dim MYSQLdata As String = "Select * from JobVerifiedSentRequest where isprocessed = 0 and JobId = '" & Me.itemid & "' "
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetItem(MYSQLdata, myverified)
        With myverified
            'Me.AmountOwedNotice.Value = .AmountOwed
            'Me.AmountOwedNotice.Value = myview.EstBalance
            Me.TNAmountOwed.Value = .AmountOwed
            Me.TNAmountOwed.Value = myview.EstBalance
        End With
        TXRequests.Style.Add("display", "none")
        MYSQL = " SELECT * FROM JOBTEXASREQUEST WHERE DATEPROCESSED Is NULL"
        MYSQL += " And JobId = " & Me.itemid
        MYSQL += " And (IsTx60day = 1"
        MYSQL += " Or IsTx90day = 1)"

        Dim MYVIEW1 As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView(MYSQL)
        If MYVIEW1.Count > 0 Then
            If myview.JobState = "TX" Then
                TXRequests.Style.Add("display", "block")
            End If
        End If

        Me.JobAddNote1.ClearData(ajobid)
        Me.JobEdit1.ClearData(ajobid)

        If myview.JVStatus = "JBIM" Or myview.JVStatus = "DNN" Or Me.chkDonotNotice.Checked = True Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "InitDiag", "disableRequestNotice();", True)
        End If
        If myview.JVStatus = "DNN" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "disableAdditionalServiceRequest", "disableAdditionalServiceRequest();", True)
        End If

        '^^*** commented by pooja 10/09/2020*** ^^
        'If (myview.JobState = "TX" Or myview.JobState = "TN") Then
        '    'Me.btnRequestNotice.Attributes.Remove("disabled")
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "enableRequestNotice", "enableRequestNotice();", True)
        'ElseIf ((myview.DateAssigned <> "1/1/0001 12:00:00 AM" Or myview.DateAssigned <> Nothing) And (myview.JobState <> "TX" Or myview.JobState <> "TN")) Then
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "InitDiag", "disableRequestNotice();", True)
        'Else
        '    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "enableRequestNotice", "enableRequestNotice();", True)
        'End If
        '^^***  *** ^^

            '^^*** createdby by pooja 10/09/2020*** ^^
            If (myview.JobState = "TX" Or (myview.JobState = "TN" And (myview.DateAssigned = "1/1/0001 12:00:00 AM" Or myview.DateAssigned = Nothing))) Then
                'Me.btnRequestNotice.Attributes.Remove("disabled")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "enableRequestNotice", "enableRequestNotice();", True)
            ElseIf (myview.JobState = "TX" Or (myview.JobState = "TN" And (myview.VerifiedDate > "01/01/1980" Or myview.NoticeSent > "01/01/1980") And (myview.DateAssigned <> "1/1/0001 12:00:00 AM" Or myview.DateAssigned <> Nothing))) Then
                'Me.btnRequestNotice.Attributes.Remove("disabled")
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "enableRequestNotice", "enableRequestNotice();", True)
            ElseIf ((myview.DateAssigned <> "1/1/0001 12:00:00 AM" Or myview.DateAssigned <> Nothing) And (myview.JobState <> "TX" Or myview.JobState <> "TN")) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "InitDiag", "disableRequestNotice();", True)

            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "enableRequestNotice", "enableRequestNotice();", True)
                ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "disableRequestNotice", "disableRequestNotice();", True)
            End If

            ''^^***  *** ^^

            '^^*** createdby by pooja 10/21/2020*** ^^
            ' condition for StartDate Is NULL then disable RequestNotice button
            ' If myview.JVStatus = "DNN" Then
            If (myview.JVStatus = "DNN" Or (myview.StartDate = "1/1/0001 12:00:00 AM" Or myview.StartDate = Nothing)) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "disableRequestNotice", "disableRequestNotice();", True)
            End If
            ''^^***  *** ^^

            If myview.IsCancelled = True Then
            'Me.btnRequestNotice.Attributes.Add("disabled", "disable")
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "InitDiag", "disableRequestNotice();", True)
        End If


        'Additional Info Bitton Enable Disable
        myportalusers = CRF.CLIENTVIEW.BLL.LienView.Provider.GetPortalUsers(Me.CurrentUser.Id)

        If (myportalusers.IsClientViewAdmin = False And myportalusers.IsClientViewManager = 0) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "disableAdditionalInfoBtn", "disableAdditionalInfoBtn();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "disableAdditionalServiceRequest", "disableAdditionalServiceRequest();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "disableRequestNotice", "disableRequestNotice();", True)

        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "enableAdditionalInfoBtn", "enableAdditionalInfoBtn();", True)
        End If

        SetCurrentTab()

        Session("exitviewer") = "0"

        Catch ex As Exception
            Dim mymsg As String = ex.Message
        End Try

    End Sub

    Protected Sub btnViewAll_Click(sender As Object, e As EventArgs)

        dsjobinfo = Me.Session("currentjobinfo")
        If (Not (dsjobinfo.Tables(17) Is Nothing) And dsjobinfo.Tables(17).Rows.Count > 0) Then
            Dim view As New DataView
            Dim strquery As String = ""
            'strquery = "ContractType='O'"
            strquery = "ContractType In ('O','E')"
            view.Table = dsjobinfo.Tables(17)
            view.RowFilter = strquery

            'We are facing problem where in Table 17 duplicate RANum record generated due to which duplicate record display in Invoice Tab
            Dim dtCopyTable As DataTable = New DataTable()
            Dim dtResults As DataTable = dsjobinfo.Tables(17).Clone
            Dim prevDistinctRANum As String = ""
            dtCopyTable = view.ToTable()

            For Each row As DataRow In dtCopyTable.Rows
                Dim DistinctRANum As String = row("RANum").ToString
                If (prevDistinctRANum <> DistinctRANum) Then
                    dtResults.ImportRow(row)
                    prevDistinctRANum = DistinctRANum
                End If
            Next

            If (dtResults.Rows.Count = 0) Then
                dtResults = Nothing
            End If
            'End Code

            Me.rptRentalInvoice.DataSource = dtResults
            Me.rptRentalInvoice.DataBind()

            'Me.rptRentalInvoice.DataSource = view.ToTable()
            'Me.rptRentalInvoice.DataBind()
            Me.rptRentalInvoiceNoticeRequest.DataSource = view.ToTable()
            Me.rptRentalInvoiceNoticeRequest.DataBind()

        End If
        LoadData(Me.itemid)
    End Sub
    Protected Sub btnViewOpen_Click(sender As Object, e As EventArgs)
        dsjobinfo = Me.Session("currentjobinfo")
        If (Not (dsjobinfo.Tables(17) Is Nothing) And dsjobinfo.Tables(17).Rows.Count > 0) Then
            Dim view As New DataView
            Dim strquery As String = ""
            strquery = "ContractType In ('O','E') AND NOT (ClientStatus = 'C' AND CurrentBalance <= 0)"
            view.Table = dsjobinfo.Tables(17)
            view.RowFilter = strquery

            'We are facing problem where in Table 17 duplicate RANum record generated due to which duplicate record display in Invoice Tab
            Dim dtCopyTable As DataTable = New DataTable()
            Dim dtResults As DataTable = dsjobinfo.Tables(17).Clone
            Dim prevDistinctRANum As String = ""
            dtCopyTable = view.ToTable()

            For Each row As DataRow In dtCopyTable.Rows
                Dim DistinctRANum As String = row("RANum").ToString
                If (prevDistinctRANum <> DistinctRANum) Then
                    dtResults.ImportRow(row)
                    prevDistinctRANum = DistinctRANum
                End If
            Next

            If (dtResults.Rows.Count = 0) Then
                dtResults = Nothing
            End If
            'End Code

            Me.rptRentalInvoice.DataSource = dtResults
            Me.rptRentalInvoice.DataBind()

            'Me.rptRentalInvoice.DataSource = view.ToTable()
            'Me.rptRentalInvoice.DataBind()
            Me.rptRentalInvoiceNoticeRequest.DataSource = view.ToTable()
            Me.rptRentalInvoiceNoticeRequest.DataBind()

        End If
        LoadData(Me.itemid)

    End Sub

    Private Function GetInvoiceTotal() As Decimal
        Dim myamt As Decimal = 0
        Dim dr As System.Data.DataRow
        If dsjobinfo.Tables(14).Rows.Count > 0 Then
            For Each dr In dsjobinfo.Tables(14).Rows
                myamt += dr("amountowed")
            Next
        End If

        Return myamt

    End Function
    Private Function GetInvoicePercent() As String
        Dim myamt As Decimal = GetInvoiceTotal()
        Dim mytot As Decimal = 0 'Me.EstBalance.Value Not used commented by Jaywanti
        Dim myrate As String = Math.Floor(myamt / mytot * 100).ToString
        Return myrate & "%"
    End Function

    Private Sub WaiverTab()

        If Me.Session("WaiverTab") = "WAIVER" Then

            hddbTabName.Value = "Waivers"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaa", "ActivateTab('" & hddbTabName.Value & "');", True)
            Me.Session("WaiverTab") = Nothing

        End If
    End Sub

    Private Sub SFORMTab()
        If Me.Session("WaiverTab") = "SFORM" Then
            hddbTabName.Value = "StateForms"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListable", "ActivateTab('" & hddbTabName.Value & "');", True)
            Me.Session("WaiverTab") = Nothing
        End If
    End Sub
    'end navigation issues 
    Private Sub SetCurrentTab()

        If Session("exitviewer") = "1" Then

            If Session("currenttab") = "WAIVER" Then
                'Me.TabContainer.ActiveTab = Me.TabPanel5
                'TabContainer.ActiveTabIndex = 5
                hddbTabName.Value = "Waivers"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaaa", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "SFORM" Then
                'Me.TabContainer.ActiveTab = Me.TabPanel6
                'TabContainer.ActiveTabIndex = 6
                hddbTabName.Value = "StateForms"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaLi", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "NOTES" Then
                'Me.TabContainer.ActiveTab = Me.TabPanel2
                'TabContainer.ActiveTabIndex = 2
                hddbTabName.Value = "Notes"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaLis", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "Notices Sent" Then
                'TabContainer.ActiveTabIndex = 3
                hddbTabName.Value = "NoticesSent"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaLists", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "Docs" Then
                'TabContainer.ActiveTabIndex = 4
                hddbTabName.Value = "Docs"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListData", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "TREQ" Then
                'TabContainer.ActiveTabIndex = 3
                hddbTabName.Value = "TXRequests"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListss", "ActivateTab('" & hddbTabName.Value & "');", True)
            ElseIf Session("currenttab") = "INVC" Then
                'TabContainer.ActiveTabIndex = 3
                hddbTabName.Value = "Invoices"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListss", "ActivateTab('" & hddbTabName.Value & "');", True)
            End If

        Else
            'hddbTabName.Value = "MainInfo"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListSet", "ActivateTab('" & hddbTabName.Value & "');", True)
        End If

    End Sub

#Region "Notices"
    Protected Sub gvwNotices_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotices.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntNotice = TryCast(e.Row.FindControl("btnPrntNotice"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntNotice"), LinkButton).Click, AddressOf btnPrntNotice_Click
        End If

        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    btnlnk = TryCast(e.Row.FindControl("lnk"), LinkButton)
        '    AddHandler CType(e.Row.FindControl("lnk"), LinkButton).Click, AddressOf btnlnk_Click
        'End If
    End Sub
    Protected Sub gvwNotices_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotices.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntNotice = TryCast(e.Row.FindControl("btnPrntNotice"), LinkButton)
            btnPrntNotice.Attributes("rowno") = DR("JobNoticeHistoryId")
            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DatePrinted")) & ""

        End If

        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    ' Get the database row bound to this Grid row...
        '    Dim dataRowView As System.Data.DataRowView = DirectCast(e.Row.DataItem, System.Data.DataRowView)

        '    Dim idValue As String = dataRowView("CertNum").ToString()

        '    'Dim textBoxId As String = TextBox1.ClientID

        '    Dim gridLinkButton As LinkButton = DirectCast(e.Row.FindControl("lnk"), LinkButton)
        '    If gridLinkButton IsNot Nothing Then
        '        Dim eventHandler As String = String.Format("return openWin('../../../App_Controls/_Custom/LienView/TrackAndConfirm.aspx?CertNum={0}');", idValue)
        '        gridLinkButton.Attributes.Add("onclick", eventHandler)
        '        gridLinkButton.Text = idValue
        '    End If
        'End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            ' Get the database row bound to this Grid row...
            Dim dataRowView As System.Data.DataRowView = DirectCast(e.Row.DataItem, System.Data.DataRowView)

            Dim idValue As String = dataRowView("CertNum").ToString()

            'Dim textBoxId As String = TextBox1.ClientID

            Dim gridHyperLink As HyperLink = DirectCast(e.Row.FindControl("hyperlnk"), HyperLink)
            If gridHyperLink IsNot Nothing Then
                Dim eventHandler As String = String.Format("javascript:return openWin('{0}');", idValue)
                gridHyperLink.Attributes.Add("onclick", eventHandler)
                gridHyperLink.Text = idValue
            End If
        End If
    End Sub
    Protected Sub btnPrntNotice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntNotice.Click
        btnPrntNotice = TryCast(sender, LinkButton)
        Session("currenttab") = "Notices Sent"
        SetCurrentTab()

        Dim sreport As String = GetNoticeCopy(btnPrntNotice.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False AndAlso sreport = "NotExists" Then

            'If sreport = "NotExists" Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "AlertMessage", "alert('This notice copy has been archived due to age, or was just created today, and is not yet available online. Please contact us at 805-823-8032 and we can send a copy of the notice to you.')", True)
            'TabContainer.ActiveTabIndex = 3
            hddbTabName.Value = "NoticesSent"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsGens", "DisplayTablelayout();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsGen", "ActivateTab('" & hddbTabName.Value & "');", True)
        ElseIf IsNothing(sreport) = False Then

            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Hideloading", "Hideloading();", True)

    End Sub
    Public Function GetNoticeCopy(ByVal ANOTICEID As String) As String

        Dim myfilename As String = ""
        myfilename = ""
        myfilename = Me.JobId.Value 'Comment for a timing
        myfilename += "-" & ANOTICEID & ".PDF"

        'Dim mydest As String = CRFVIEW.COMMON.GetKeyValue("JOBNOTICEIMAGEPATH") & myfilename
        Dim mydest As String = "E:\JOBNOTICEIMAGES\" & myfilename

        Dim mydest1 As String = Me.MyPage.MapPath("~/UserData/Output/" & myfilename)
        If System.IO.File.Exists(mydest1) = True Then
            System.IO.File.Delete(mydest1)
        End If

        If System.IO.File.Exists(mydest) = True Then
            System.IO.File.Copy(mydest, mydest1, True)
            System.Threading.Thread.Sleep(300)
            If System.IO.File.Exists(mydest1) = True Then
                Return mydest1
            End If
        Else
            Return "NotExists"

        End If

        Return LienView.Provider.PrintPrelimNotice(btnPrntNotice.Attributes("rowno"))

    End Function
#End Region
#Region "Waivers"
    Protected Sub btnAddWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddWaiver.Click
        Dim s As String = "~/MainDefault.aspx?jobview.jobeditwaiver&itemid=" & Me.itemid & "&subid=" & -1 & "&PageName=JobView"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwWaivers_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwWaivers.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnEmailWaiver = TryCast(e.Row.FindControl("btnEmailWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntWaiver"), LinkButton).Click, AddressOf btnPrntWaiver_Click
            btnPrntWaiver = TryCast(e.Row.FindControl("btnPrntWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntWaiver"), LinkButton).Click, AddressOf btnPrntWaiver_Click
            btnEditWaiver = TryCast(e.Row.FindControl("btnEditWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditWaiver"), LinkButton).Click, AddressOf btnEditWaiver_Click
            btnDeleteWaiver = TryCast(e.Row.FindControl("btnDeleteWaiver"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteWaiver"), LinkButton).Click, AddressOf btnDeleteWaiver_Click

        End If
    End Sub
    Protected Sub gvwWaivers_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwWaivers.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEmailWaiver = TryCast(e.Row.FindControl("btnEmailWaiver"), LinkButton)
            btnEmailWaiver.Attributes("rowno") = DR("JobWaiverLogId")
            btnPrntWaiver = TryCast(e.Row.FindControl("btnPrntWaiver"), LinkButton)
            btnPrntWaiver.Attributes("rowno") = DR("JobWaiverLogId")
            btnEditWaiver = TryCast(e.Row.FindControl("btnEditWaiver"), LinkButton)
            btnEditWaiver.Attributes("rowno") = DR("JobWaiverLogId")
            btnDeleteWaiver = TryCast(e.Row.FindControl("btnDeleteWaiver"), LinkButton)
            btnDeleteWaiver.Attributes("rowno") = DR("JobWaiverLogId")

            e.Row.Cells(2).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            e.Row.Cells(3).Text = "" & Utils.FormatDate(DR("DatePrinted")) & ""
            If Me.CurrentUser.UserManager = 0 Then
                btnPrntWaiver.Visible = False
                btnEditWaiver.Visible = False
                btnDeleteWaiver.Visible = False
                btnEmailWaiver.Visible = False
                'gvwWaivers.Columns(0).Visible = False
                'gvwWaivers.Columns(6).Visible = False
            End If
        End If

    End Sub
    Protected Sub btnPrntWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntWaiver.Click
        btnPrntWaiver = TryCast(sender, LinkButton)
        Session("currenttab") = "WAIVER"
        SetCurrentTab()

        Dim sreport As String = LienView.Provider.PrintJobWaiver(btnPrntWaiver.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(btnPrntWaiver.Attributes("rowno"), mywaiver)
            With mywaiver
                If (.Notary = True) Then
                    PrintNotary(btnPrntWaiver.Attributes("rowno"))
                    MergeNotary(sreport)
                End If

            End With
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Public Sub PrintNotary(ByVal JobWaiverLogId As String)

        Dim sreport As String = LienView.Provider.PrintJobNotary(JobWaiverLogId)

        If IsNothing(sreport) = False Then

            s1 = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))

        End If

    End Sub
    Public Sub MergeNotary(ByVal sreport As String)
        Dim s2 As String = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
        Dim path As String = ConfigurationManager.AppSettings("path").ToString()
        File.Create(path & (System.IO.Path.GetFileName(sreport))).Close()
        Dim notary As String

        notary = s1

        Dim result As String = path & (System.IO.Path.GetFileName(sreport))
        Dim source_files() As String = {s2, notary}
        Dim document As iTextSharp.text.Document = New iTextSharp.text.Document
        Dim copy As iTextSharp.text.pdf.PdfCopy = New iTextSharp.text.pdf.PdfCopy(document, New FileStream(result, FileMode.Create))
        'open the document
        document.Open()
        Dim reader As iTextSharp.text.pdf.PdfReader
        Dim i As Integer = 0
        Do While (i < source_files.Length)
            'create PdfReader object
            reader = New iTextSharp.text.pdf.PdfReader(source_files(i))
            'merge combine pages
            Dim page As Integer = 1
            Do While (page <= reader.NumberOfPages)
                copy.AddPage(copy.GetImportedPage(reader, page))
                page = (page + 1)
            Loop

            i = (i + 1)
        Loop

        'close the document object
        document.Close()
        '-------------------------------------------------------------
        File.Copy(result, sreport, True)
        File.Delete(result)

    End Sub
    Private Sub PrintWaiver(ByVal itemid As String)

        Me.ViewState("VIEW") = "WAIVER"
        SetCurrentTab()

        Dim sreport As String = LienView.Provider.PrintJobWaiver(itemid)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub
    Protected Sub btnEmailWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailWaiver.Click
        btnEmailWaiver = TryCast(sender, LinkButton)
        Session("currenttab") = "WAIVER"
        SetCurrentTab()

        sremail = btnEmailWaiver.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(sremail, mywaiver)
        Me.txtToList.Text = mywaiver.EmailTo
        JobAddSubjectLine = ViewState("JobAddSubjectLine").ToString
        Dim EmailUserName As String = Me.UserInfo.UserInfo.UserName
        Dim EmailUserEmail As String = Me.UserInfo.UserInfo.Email

        ViewState("JobWaiverLogId") = sremail
        If (JobAddSubjectLine.Trim() <> "") Then
            txtSubject.Text = "Waiver Document for " + JobAddSubjectLine

        Else
            txtSubject.Text = "Waiver Document for "
        End If
        Me.txtBody.Text = "PLEASE DO NOT RESPOND TO THIS EMAIL." + vbCrLf + "If you have any questions, or need any assistance, please contact " + EmailUserName + " at " + EmailUserEmail + "."
        hdnEmailflag.Value = "W"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayEmailPopUp", "DisplayEmailPopUp();", True)


    End Sub
    Protected Sub btnEmailsend_Click(sender As Object, e As EventArgs)

        If hdnEmailflag.Value = "W" Then
            sremail = ViewState("JobWaiverLogId").ToString
            EmailWaiver(sremail)
        ElseIf hdnEmailflag.Value = "D" Then
            sremail = hdnDocId.Value
            EmailDocument(sremail)
        ElseIf hdnEmailflag.Value = "RS" Then
            EmailRequestService()
        ElseIf hdnEmailflag.Value = "RL" Then
            sremail = ViewState("JobWaiverLogId").ToString
            RecensionLetterEmail(sremail)
        End If

    End Sub
    Public Sub EmailWaiver(ByVal JobWaiverLogId As String)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(JobWaiverLogId)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobWaiverLogId, mywaiver)
            With mywaiver
                If (.Notary = True) Then
                    PrintNotary(JobWaiverLogId)
                    MergeNotary(sreport)
                End If

            End With
            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            Dim FlagError = EmailToUser("Job Waiver Central", txtToList.Text, txtSubject.Text, txtBody.Text, Filename, fileArray) 'For a timing

            If FlagError = "0" Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EmailSendPopup", "EmailSendPopup('Your Waiver Has Been Emailed.');", True)

            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RemoveBackdrop", "RemoveBackdrop();", True)
    End Sub

    Public Sub EmailRequestService()
        Try

            Dim EmailReqBody As String = "Request for " + txtServiceType.Text + " made in the amount of " + txtAmountOwed.Text + ". " + txtMessage.Text + "."
            Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(hdnJobId.Value, myjob)
            '^^*** commented by pooja 07/14/2021*** ^^
            'If IsNothing(myjob.DateAssigned) Or myjob.DateAssigned = DateTime.MinValue Then
            '    With myjob
            '        .DeskNum = "M1"
            '        .DeskNumId = GetDeskId("M1")
            '        .DateAssigned = Now()
            '        .RevDate = Now()
            '        .StatusCode = "ACT"
            '        .StatusCodeId = GetStatusId("ACT")
            '        .VerifyOWOnly = 1
            '    End With
            '    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
            'End If
            '^^*** *** ^^
            '^^*** Created by pooja 07/14/2021*** ^^
            If IsNothing(myjob.DateAssigned) Or myjob.DateAssigned = DateTime.MinValue Then
                With myjob
                    .DeskNum = "M1"
                    .DeskNumId = GetDeskId("M1")
                    .DateAssigned = Now()
                    .RevDate = Now()
                    .StatusCode = "AVO"
                    .StatusCodeId = GetStatusId("AVO")
                    .VerifyJob = 1
                End With
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
            End If
            '^^***  *** ^^
            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = hdnJobId.Value
                .DateCreated = Now()
                .Note = EmailReqBody
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                .NoteTypeId = 1
            End With
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
            Dim FlagError = EmailToUser("Service Request Central", "mechlien@crfsolutions.com", txtreqsubject.Text, EmailReqBody, "", Nothing)
            'Dim FlagError = EmailToUser("Service Request Central", "pooja.p@rhealtech.com", txtreqsubject.Text, EmailReqBody, "", Nothing)
            If FlagError = "0" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EmailSendPopup", "EmailSendPopup('Your Service Request Has Been Emailed.');", True)
            End If
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RemoveBackdrop", "RemoveBackdrop();", True)

        Catch ex As Exception

        End Try
    End Sub

    Public Function GetDeskId(ByVal Desknum As String)
        Dim deskId As Integer = 0
        Try
            Dim query As String = "select * from jobdesks where desknum='" + Desknum + "'"
            Dim MYVIEW As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView(query)
            deskId = MYVIEW.RowItem("DeskId")

        Catch ex As Exception

        End Try
        Return deskId
    End Function
    Public Function GetStatusId(ByVal statuscode As String)
        Dim statusid As Integer = 0
        Try
            Dim MYSQL As String = "Select Id from jobstatus where Jobstatus='" + statuscode + "'"
            Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
            If (MYDT.Tables(0).Rows.Count > 0) Then
                statusid = MYDT.Tables(0).Rows(0)("Id")
            End If
        Catch ex As Exception

        End Try
        Return statusid
    End Function
    Public Shared Function EmailToUser(ByVal DisplayName As String, ByVal email As String, ByVal subject As String, ByVal strBody As String, ByVal FileName As String, ByVal pdfcontent As Byte())

        Dim FlagError As String = "0"

        Dim ToArrayList As String()
        ToArrayList = email.Split(";")

        Dim MYSITE As TABLES.Report_Settings = GetSettings()

        Dim msg As New MailMessage
        'msg.From = New MailAddress("bhagyashri.raikwad@rhealtech.com", DisplayName)
        msg.From = New MailAddress("CRFIT@crfsolutions.com", DisplayName)
        msg.Subject = subject
        For index = 0 To ToArrayList.Length - 1
            msg.To.Add(ToArrayList(index))
        Next
        msg.IsBodyHtml = True
        msg.Body = strBody
        If (FileName <> "") Then
            msg.Attachments.Add(New Attachment(New MemoryStream(pdfcontent), FileName))
        End If
        Try
            With MYSITE
                Dim _smtp As New System.Net.Mail.SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
                'Dim _smtp As New System.Net.Mail.SmtpClient("smtp.rhealtech.com")
                ''If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                '_smtp.Port = 25
                '_smtp.Credentials = New System.Net.NetworkCredential(vbNull, vbNull)
                '_smtp.Credentials = New System.Net.NetworkCredential("pooja.p@rhealtech.com", "L6E8BgT3tP60")
                '_smtp.EnableSsl = False
                ''End If
                '_smtp.Send(msg)
                '_smtp.Dispose()
            End With
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            MsgBox("Error On email:" & vbCrLf & mymsg)
            FlagError = "1"
        End Try
        Return FlagError
    End Function
    Public Shared Function GetSettings() As TABLES.Report_Settings
        Dim myitem As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, myitem)

        Return myitem

    End Function
    Protected Sub btnEditWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditWaiver.Click
        Session("currenttab") = "WAIVER"
        btnEditWaiver = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?jobview.jobeditwaiver&itemid=" & Me.itemid & "&subid=" & btnEditWaiver.Attributes("rowno") & "&PageName=JobView"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnDeleteWaiver_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteWaiver.Click
        'Me.ViewState("VIEW") = "WAIVER"
        Session("currenttab") = "WAIVER"
        Session("exitviewer") = "1"
        SetCurrentTab()

        btnDeleteWaiver = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobWaiverLog
        myitem.JobWaiverLogId = btnDeleteWaiver.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)

    End Sub
#End Region
    '#Region "Verified Requests"
    '    Protected Sub btnAddVREQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddVReq.Click
    '        Dim s As String = "~/MainDefault.aspx?lienview.jobeditvreq&itemid=" & Me.itemid & "&subid=" & -1
    '        Response.Redirect(Me.Page.ResolveUrl(s), True)
    '    End Sub
    '    'Protected Sub gvwVReq_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwVReq.RowCreated
    '    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '    '        Me.btnEditVReq = TryCast(e.Row.FindControl("btnEditVReq"), LinkButton)
    '    '        AddHandler CType(e.Row.FindControl("btnEditVReq"), LinkButton).Click, AddressOf btnEditVReq_Click
    '    '        btnDeleteVReq = TryCast(e.Row.FindControl("btnDeleteVReq"), LinkButton)
    '    '        AddHandler CType(e.Row.FindControl("btnDeleteVReq"), LinkButton).Click, AddressOf btnDeleteVREQ_Click

    '    '    End If
    '    'End Sub
    '    'Protected Sub gvwVReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwVReq.RowDataBound
    '    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '    '        Dim DR As System.Data.DataRowView = e.Row.DataItem
    '    '        btnEditVReq = TryCast(e.Row.FindControl("btnEditVReq"), LinkButton)
    '    '        btnEditVReq.Attributes("rowno") = DR("Id")

    '    '        btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteVReq"), LinkButton)
    '    '        btnDeleteTReq.Attributes("rowno") = DR("Id")

    '    '        e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
    '    '    End If

    '    'End Sub
    '    Protected Sub btnEditVReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditVReq.Click
    '        btnEditVReq = TryCast(sender, LinkButton)
    '        Dim s As String = "~/MainDefault.aspx?lienview.jobeditvreq&itemid=" & Me.itemid & "&subid=" & btnEditVReq.Attributes("rowno")
    '        Response.Redirect(Me.Page.ResolveUrl(s), True)

    '    End Sub
    '    Protected Sub btnDeleteVREQ_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteINVC.Click
    '        Me.ViewState("VIEW") = "VREQ"
    '        SetCurrentTab()

    '        btnDeleteVReq = TryCast(sender, LinkButton)
    '        Dim myitem As New CRFDB.TABLES.JobVerifiedSentRequest
    '        myitem.Id = btnDeleteVReq.Attributes("rowno")
    '        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
    '        LoadData(Me.itemid)

    '    End Sub
    '#End Region
#Region "Texas Requests"
    '##100
    'Protected Sub btnAddTXStmt_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddTXStmt.Click
    '    Dim s As String = "~/MainDefault.aspx?JobView.JobViewEditTReq&itemid=" & Me.itemid & "&subid=" & -1
    '    Response.Redirect(Me.Page.ResolveUrl(s), True)
    'End Sub
    Protected Sub gvwTexasReq_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwTexasReq.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntTReq"), LinkButton).Click, AddressOf btnPrntTReq_Click
            Me.btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditTReq"), LinkButton).Click, AddressOf btnEditTReq_Click
            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteTReq"), LinkButton).Click, AddressOf btnDeleteTReq_Click

        End If
    End Sub
    Protected Sub gvwTexasReq_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnEditTReq = TryCast(e.Row.FindControl("btnEditTReq"), LinkButton)
            btnEditTReq.Attributes("rowno") = DR("Id")

            btnPrntTReq = TryCast(e.Row.FindControl("btnPrntTReq"), LinkButton)
            btnPrntTReq.Attributes("rowno") = DR("Id")

            btnDeleteTReq = TryCast(e.Row.FindControl("btnDeleteTReq"), LinkButton)
            btnDeleteTReq.Attributes("rowno") = DR("Id")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
        End If

    End Sub
    '##100
    Protected Sub btnEditTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditTReq.Click
        btnEditTReq = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?JobView.JobViewEditTReq&itemid=" & Me.itemid & "&subid=" & btnEditTReq.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    '##100
    Protected Sub btnPrntTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntTReq.Click
        btnPrntWaiver = TryCast(sender, LinkButton)
        Me.ViewState("VIEW") = "TREQ"
        Session("currenttab") = "TREQ"
        SetCurrentTab()

        Dim sreport As String = LienView.Provider.GetTexasRequestAck(btnPrntWaiver.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
    '##100
    Protected Sub btnDeleteTReq_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteTReq.Click
        Me.ViewState("VIEW") = "TREQ"
        SetCurrentTab()

        btnDeleteTReq = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobTexasRequest
        myitem.Id = btnDeleteTReq.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)
        If Me.ViewState("VIEW") = "TREQ" Then
            'Me.TabContainer.ActiveTabIndex = 7
            'Me.TabContainer.TabIndex = 7
            hddbTabName.Value = "NoticeRequest"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsDta", "ActivateTab('" & hddbTabName.Value & "');", True)
        End If
    End Sub
#End Region
#Region "Invoice"
    'Protected Sub btnAddNewINVCE_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNewINVC.Click
    '    Dim s As String = "~/MainDefault.aspx?lienview.jobeditInvoice&itemid=" & Me.itemid & "&subid=" & -1
    '    Response.Redirect(Me.Page.ResolveUrl(s), True)
    'End Sub

    Protected Sub btnEditINVC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditINVC.Click
        btnEditINVC = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?jobview.jobeditInvoice&itemid=" & Me.itemid & "&subid=" & btnEditINVC.Attributes("rowno")
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnDeleteINVC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteINVC.Click
        Me.ViewState("VIEW") = "INVC"
        SetCurrentTab()

        btnDeleteINVC = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobInvoices
        myitem.Id = btnDeleteINVC.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)

    End Sub
    'Protected Sub btnPrintINVC_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintINVC.Click
    '    btnPrntWaiver = TryCast(sender, LinkButton)
    '    Me.ViewState("VIEW") = "INVC"
    '    Session("currenttab") = "INVC"
    '    SetCurrentTab()

    '    Dim sreport As String = LienView.Provider.GetInvoiceReport(Me.JobId.Value)
    '    Me.btnDownLoad.Visible = False
    '    If IsNothing(sreport) = False Then
    '        Me.btnDownLoad.Visible = True
    '        Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
    '        Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

    '        Me.plcReportViewer.Controls.Clear()
    '        Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
    '        Me.MultiView1.SetActiveView(Me.View2)
    '    Else
    '        Me.plcReportViewer.Controls.Clear()
    '        Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
    '        Me.MultiView1.SetActiveView(Me.View2)
    '    End If
    'End Sub
#End Region
#Region "Legal Parties"
    Protected Sub gvwLegalParties_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwLegalParties.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnRecension = TryCast(e.Row.FindControl("btnRecension"), LinkButton)
            btnRecension.Attributes("rowno") = DR("Id") & "-" & e.Row.Cells(0).Text.ToUpper

            Select Case e.Row.Cells(0).Text.ToUpper
                Case "CU"
                    e.Row.Cells(0).Text = "Customer"
                Case "GC"
                    e.Row.Cells(0).Text = "General Contractor"
                Case "OW"
                    e.Row.Cells(0).Text = "Owner"
                Case "LE"
                    e.Row.Cells(0).Text = "Lender/Surety"

            End Select
            e.Row.Cells(1).Text = "" & Strings.Left(e.Row.Cells(1).Text, 25) & ""
            e.Row.Cells(2).Text = "" & Strings.Left(e.Row.Cells(2).Text, 20) & ""
            e.Row.Cells(3).Text = "" & Strings.Left(e.Row.Cells(3).Text, 25) & ""
            e.Row.Cells(4).Text = "" & Strings.Left(e.Row.Cells(4).Text, 25) & ""
            e.Row.Cells(5).Text = "" & Strings.Left(e.Row.Cells(5).Text, 15) & ""
            e.Row.Cells(6).Text = "" & Utils.FormatPhoneNo(e.Row.Cells(6).Text) & ""

        End If

    End Sub

    Protected Sub gvwLegalParties_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwLegalParties.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnRecension = TryCast(e.Row.FindControl("btnRecension"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnRecension"), LinkButton).Click, AddressOf btnRecension_Click
        End If
    End Sub

    Protected Sub btnRecension_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRecension.Click
        btnRecension = TryCast(sender, LinkButton)
        Dim btnRecensionDetails As String() = Strings.Split(btnRecension.Attributes("rowno"), "-")
        LegalPartyId = btnRecensionDetails(0)
        JobLegalPartiesTypeCode = btnRecensionDetails(1)

        hdnLegalPartyId.Value = LegalPartyId
        hdnJobLegalPartiesTypeCode.Value = JobLegalPartiesTypeCode
        myJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetails(LegalPartyId, JobLegalPartiesTypeCode)

        myStateForms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateForms(StateCode)

        dsJobWaiverLogByInfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByInfo(itemid, myStateForms.Id, myJobLegalParties.AddressName)

        dsJobWaiverLogByRecensionLetter = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByRecensionLetter(itemid, myStateForms.Id, myJobLegalParties.AddressName)

        If (dsJobWaiverLogByRecensionLetter.Rows.Count > 0) Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CreateAnotherRecensionLetter", "CreateAnotherRecensionLetter('" + myJobLegalParties.AddressName + "');", True)
        Else
            CreateRecensionLetter()
        End If

    End Sub

    Protected Sub btnCreateAnotherRecensionLetter_Click(sender As Object, e As EventArgs)

        CreateRecensionLetter()

    End Sub


    Public Sub CreateRecensionLetter()
        Try
            LegalPartyId = hdnLegalPartyId.Value
            JobLegalPartiesTypeCode = hdnJobLegalPartiesTypeCode.Value

            myJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetails(LegalPartyId, JobLegalPartiesTypeCode)

            myStateForms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateForms(StateCode)

            dsJobWaiverLogByInfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByInfo(itemid, myStateForms.Id, myJobLegalParties.AddressName)

            dsJobWaiverLogByRecensionLetter = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobWaiverLogByRecensionLetter(itemid, myStateForms.Id, myJobLegalParties.AddressName)

            If (myStateForms.Id > 0) Then
                If (Not (myJobLegalParties.AddressName Is Nothing Or myJobLegalParties.AddressName = "")) Then
                    With mywaiver
                        .JobId = itemid
                        .FormId = myStateForms.Id
                        .FormCode = Strings.Left((myStateForms.FormCode + "-" + myStateForms.Description), 50)
                        .DateCreated = Today
                        .DatePrinted = Today
                        .RequestedBy = Left(Me.UserInfo.UserInfo.LoginCode, 10)
                        .RequestedByUserId = Me.CurrentUser.Id
                        .MailToName = myJobLegalParties.AddressName
                        .MailToAddr1 = myJobLegalParties.AddressLine1
                        .MailToCity = myJobLegalParties.City
                        .MailToState = myJobLegalParties.State
                        .MailToZip = myJobLegalParties.PostalCode

                        If (myJobLegalParties.TypeCode = "CU") Then
                            .MailtoCU = True
                            .MailtoGC = False
                            .MailtoLE = False
                            .MailtoOW = False
                        ElseIf (myJobLegalParties.TypeCode = "GC") Then
                            .MailtoGC = True
                            .MailtoCU = False
                            .MailtoLE = False
                            .MailtoOW = False
                        ElseIf (myJobLegalParties.TypeCode = "LE") Then
                            .MailtoLE = True
                            .MailtoGC = False
                            .MailtoCU = False
                            .MailtoOW = False
                        ElseIf (myJobLegalParties.TypeCode = "OW") Then
                            .MailtoOW = True
                            .MailtoLE = False
                            .MailtoGC = False
                            .MailtoCU = False
                        Else
                            .MailtoOW = False
                            .MailtoLE = False
                            .MailtoGC = False
                            .MailtoCU = False
                        End If
                    End With
                    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(mywaiver)
                    'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RecensionLetterCreatedPopup", "RecensionLetterCreatedPopup('" + myJobLegalParties.AddressName + "');", True)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showRecensionLetterCreatedPopup", "showRecensionLetterCreatedPopup('" + myJobLegalParties.AddressName + "');", True)
                    ViewState("JobWaiverLogId") = mywaiver.JobWaiverLogId
                Else
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ExitLegalParties", "ExitLegalParties();", True)
                End If
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RecensionLetterNotAvailableForLegalParties", "RecensionLetterNotAvailableForLegalParties('" + myJobLegalParties.AddressName + "');", True)
            End If


        Catch ex As Exception

        End Try
    End Sub

    Protected Sub btnPrintRecensionLetter_Click(sender As Object, e As EventArgs)
        PrintRecensionLetter()
    End Sub
    Public Sub PrintRecensionLetter()
        Dim btnRecensionDetails As String() = Strings.Split(btnRecension.Attributes("rowno"), "-")
        Dim LegalPartyId As String = btnRecensionDetails(0)
        Dim JobLegalPartiesTypeCode As String = btnRecensionDetails(1)

        myJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetails(LegalPartyId, JobLegalPartiesTypeCode)

        JobWaiverLogId = ViewState("JobWaiverLogId").ToString
        Dim sreport As String = LienView.Provider.PrintJobWaiver(JobWaiverLogId)

        If IsNothing(sreport) = False Then

            Dim s1 As String = Me.ResolveUrl("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", s1, 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)

                End If
            End If
            'Dim URL As String = "MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + URL + "'", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showRecensionLetterCreatedPopup", "showRecensionLetterCreatedPopup('" + myJobLegalParties.AddressName + "');", True)
        End If

    End Sub

    Protected Sub btnEmailRecensionLetter_Click(sender As Object, e As EventArgs)

        LegalPartyId = hdnLegalPartyId.Value
        JobLegalPartiesTypeCode = hdnJobLegalPartiesTypeCode.Value

        dsJobLegalParties = CRF.CLIENTVIEW.BLL.LienView.Provider.GetLegalPartyDetailsDataTable(LegalPartyId, JobLegalPartiesTypeCode)

        Dim EmailUserName As String = Me.UserInfo.UserInfo.UserName
        Dim EmailUserEmail As String = Me.UserInfo.UserInfo.Email

        If dsJobLegalParties.Rows.Count <> 0 Then
            Me.txtToListForRecensionLetter.Text = Convert.ToString(dsJobLegalParties.Rows(0)("Email"))

        Else
            Me.txtToListForRecensionLetter.Text = " "
        End If


        JobAddSubjectLine = ViewState("JobAddSubjectLine").ToString

        If (JobAddSubjectLine.Trim() <> "") Then
            txtRecensionLetterSubject.Text = "Recension Letter for " + JobAddSubjectLine
        Else
            txtRecensionLetterSubject.Text = "Recension Letter for "
        End If

        Me.txtRecensionLetterBody.Text = "PLEASE DO NOT RESPOND TO THIS EMAIL." + vbCrLf + "If you have any questions, or need any assistance, please contact " + EmailUserName + " at " + EmailUserEmail + "."
        hdnEmailflag.Value = "RL"

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideRecensionLetterPopUp", "HideRecensionLetterPopUp();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayEmailRecensionLetterPopUp", "DisplayEmailRecensionLetterPopUp();", True)

    End Sub

    Public Sub RecensionLetterEmail(ByVal JobWaiverLogId As String)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(JobWaiverLogId)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobWaiverLogId, mywaiver)
            With mywaiver
                If (.Notary = True) Then
                    PrintNotary(JobWaiverLogId)
                    MergeNotary(sreport)
                End If

            End With
            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            Dim FlagError = EmailToUser("Job Waiver Central", txtToListForRecensionLetter.Text, txtRecensionLetterSubject.Text, txtRecensionLetterBody.Text, Filename, fileArray) 'For a timing

            If FlagError = "0" Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RecensionLetterEmailSendPopup", "RecensionLetterEmailSendPopup('Your Recension Letter Has Been Emailed.');", True)
            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RemoveBackdrop", "RemoveBackdrop();", True)
    End Sub


#End Region
#Region "Notes"
    Protected Sub JobAddNote1_ItemSaved() Handles JobAddNote1.ItemSaved
        LoadData(Me.itemid)
        hddbTabName.Value = "Notes"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsSet", "ActivateTab('" & hddbTabName.Value & "');", True)

    End Sub
    Protected Sub AddLegalPty1_ItemSaved() Handles AddLegalPty1.ItemSaved
        Me.LoadData(Me.itemid)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModalLegalParty','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');", True)
    End Sub
    Protected Sub gvwNotes_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotes.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnViewNote = TryCast(e.Row.FindControl("btnViewNote"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnViewNote"), LinkButton).Click, AddressOf btnViewNote_Click

        End If
    End Sub

    Protected Sub gvwNotes_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwNotes.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnViewNote = TryCast(e.Row.FindControl("btnViewNote"), LinkButton)
            btnViewNote.Attributes("rowno") = DR("Id")

            ' e.Row.Cells(0).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            '  e.Row.Cells(1).Text = "" & Strings.Left(e.Row.Cells(1).Text, 10) & ""
            ' e.Row.Cells(1).Width = "80"

            ' e.Row.Cells(2).Wrap = True
            If Me.CurrentUser.UserManager = 0 Then
                btnViewNote.Visible = False

                'gvwWaivers.Columns(0).Visible = False
                'gvwWaivers.Columns(6).Visible = False
            End If
        End If
    End Sub
    Protected Sub btnViewNote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewNote.Click
        btnViewNote = TryCast(sender, LinkButton)
        Dim myItem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        Dim test As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActionHistory
        Dim test1 As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActions
        If btnViewNote.Attributes("rowno") > 1 Then

            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(btnViewNote.Attributes("rowno"), myItem)
            Me.txtNotes.Text = myItem.Note.Replace("<", "[").Replace(">", "]")
            If Me.txtNotes.Text.Trim = "" Then
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(btnViewNote.Attributes("rowno"), test)
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(test.ActionId, test1)
                Me.txtNotes.Text = test1.HistoryNote.Replace("<", "[").Replace(">", "]")
            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsNotePopUp", "DisplayNotePopUp();", True)

    End Sub
#End Region

#Region "Job Info"
    Protected Sub JobEdit1_ItemSaved() Handles JobEdit1.ItemSaved
        Dim s As String = Me.Page.Request.Url.ToString.ToUpper
        '  s = Strings.Replace(s, "DEFAULT.ASPX", "")
        Me.Page.Response.Redirect(s, True)

        'LoadData(Me.itemid)
        'Me.TabContainer.ActiveTab = Me.TabPanel3

    End Sub
#End Region
#Region "Docs"
    Protected Sub btnPrintNotes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrintNotes.Click
        Me.ViewState("VIEW") = "NOTES"
        Session("currenttab") = "NOTES"

        SetCurrentTab()
        hddbTabName.Value = "Notes"
        Dim sreport As String = LienView.Provider.GetNotesForJob(itemid, True)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub

    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))
        Me.MultiView1.SetActiveView(Me.View1)

    End Sub

    Protected Sub gvwDocs_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
        End If
    End Sub
    Protected Sub gvwDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwDocs.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnViewDoc = TryCast(e.Row.FindControl("btnViewDoc"), LinkButton)
            btnViewDoc.Attributes("rowno") = DR("Id")
            btnDeleteItem = TryCast(e.Row.FindControl("btnDeleteItem"), LinkButton)
            btnDeleteItem.Attributes("rowno") = DR("Id")
            'btnEmailDocs = TryCast(e.Row.FindControl("btnEmailDocs"), LinkButton)
            'btnEmailDocs.Attributes("rowno") = DR("Id")
            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            If Me.CurrentUser.UserManager = 0 Then
                btnViewDoc.Visible = False

                'gvwWaivers.Columns(0).Visible = False
                'gvwWaivers.Columns(6).Visible = False
            End If
        End If

    End Sub
    Protected Sub btnPrntDocs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntDocs.Click
        Dim DocID As String = hdnDocId.Value
        Session("currenttab") = "Docs"
        SetCurrentTab()
        Dim myitem As New TABLES.JobAttachments
        ProviderBase.DAL.Read(DocID, myitem)
        Dim sreport As String = "~/UserData/output/" & myitem.Id & "-" & myitem.FileName
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.DocumentImage, Me.MapPath(sreport))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)

                Dim firstpart As String = Filepath.Substring(0, Filepath.LastIndexOf("/"))
                Dim lastpart As String = Filepath.Substring(Filepath.LastIndexOf("/") + 1)
                'lastpart = HttpUtility.UrlEncode(lastpart)  'Remove # with %23 (Html Encoding UTF-8)
                'lastpart = HttpUtility.UrlPathEncode(lastpart)  'Remove space with %20
                lastpart = Uri.EscapeDataString(lastpart) 'Remove # with %23 , space with %20

                Filepath = firstpart + "/" + lastpart

                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RemoveBackdrop", "RemoveBackdrop();", True)
    End Sub
    Public Sub EmailDocument(ByVal DocId As String)
        Dim myitem As New TABLES.JobAttachments
        ProviderBase.DAL.Read(DocId, myitem)
        Dim sreport As String = "~/UserData/output/" & myitem.Id & "-" & myitem.FileName
        CRF.CLIENTVIEW.BLL.PDFLIB.WriteStream(myitem.DocumentImage, Me.MapPath(sreport))
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            Dim FlagError = EmailToUser("Job Document Central", txtToList.Text, txtSubject.Text, txtBody.Text, Filename, fileArray) 'For a timing
            If FlagError = "0" Then

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EmailSendPopup", "EmailSendPopup('Your Document Has Been Emailed.');", True)

            End If
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "RemoveBackdrop", "RemoveBackdrop();", True)
    End Sub

    Protected Sub btnEmailDocs_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEmailDocs.Click
        Dim DocID As String = hdnDocId.Value
        Session("currenttab") = "Docs"
        SetCurrentTab()
        Dim myitem As New TABLES.JobAttachments
        ProviderBase.DAL.Read(DocID, myitem)
        Me.txtToList.Text = ""
        JobAddSubjectLine = ViewState("JobAddSubjectLine").ToString
        Dim EmailUserName As String = Me.UserInfo.UserInfo.UserName
        Dim EmailUserEmail As String = Me.UserInfo.UserInfo.Email
        If (JobAddSubjectLine.Trim() <> "") Then
            txtSubject.Text = "New Document for " + JobAddSubjectLine
        Else
            txtSubject.Text = "New Document for "
        End If
        Me.txtBody.Text = "PLEASE DO NOT RESPOND TO THIS EMAIL." + vbCrLf + "If you have any questions, or need any assistance, please contact " + EmailUserName + " at " + EmailUserEmail + "."
        hdnEmailflag.Value = "D"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DisplayEmailPopUp", "DisplayEmailPopUp();", True)
    End Sub

    Protected Sub frm_Unload(ByVal sender As Object, ByVal e As System.EventArgs) Handles frm.Unload
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where JobId = " & Me.itemid
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub


    Protected Sub btnFileUpLoadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpLoadCancel.Click
        'markparimal-uploadfile:To refresh Grid values .

        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where JobId = " & Me.itemid
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsGens", "DisplayTablelayout();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModalFileUpload','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
    End Sub
#End Region
#Region "State Forms"
    Protected Sub btnAddStateFrom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddStateFrom.Click
        Dim s As String = "~/MainDefault.aspx?jobview.jobeditsform&itemid=" & Me.itemid & "&subid=" & -1 & "&PageName=JobView"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    Protected Sub gvwStateForms_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwStateForms.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            btnPrntSForm = TryCast(e.Row.FindControl("btnPrntSForm"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnPrntSForm"), LinkButton).Click, AddressOf btnPrntSForm_Click
            btnEditSForm = TryCast(e.Row.FindControl("btnEditSForm"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnEditSForm"), LinkButton).Click, AddressOf btnEditSForm_Click
            btnDeleteSForm = TryCast(e.Row.FindControl("btnDeleteSForm"), LinkButton)
            AddHandler CType(e.Row.FindControl("btnDeleteSForm"), LinkButton).Click, AddressOf btnDeleteSForm_Click

        End If
    End Sub
    Protected Sub gvwStateForms_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwStateForms.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            btnPrntSForm = TryCast(e.Row.FindControl("btnPrntSForm"), LinkButton)
            btnPrntSForm.Attributes("rowno") = DR("JobNoticeLogId")
            btnEditSForm = TryCast(e.Row.FindControl("btnEditSForm"), LinkButton)
            btnEditSForm.Attributes("rowno") = DR("JobNoticeLogId")
            btnDeleteSForm = TryCast(e.Row.FindControl("btnDeleteSForm"), LinkButton)
            btnDeleteSForm.Attributes("rowno") = DR("JobNoticeLogId")

            e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DateCreated")) & ""
            e.Row.Cells(2).Text = "" & Utils.FormatDate(DR("DatePrinted")) & ""
            If Me.CurrentUser.UserManager = 0 Then
                btnPrntSForm.Visible = False
                btnEditSForm.Visible = False
                btnDeleteSForm.Visible = False

            End If
        End If

    End Sub
    Protected Sub btnPrntSForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrntSForm.Click
        btnPrntSForm = TryCast(sender, LinkButton)
        Session("currenttab") = "SFORM"
        SetCurrentTab()
        Dim sreport As String = LienView.Provider.PrintStateForm(btnPrntSForm.Attributes("rowno"))
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If
    End Sub
    Private Sub PrintSFORM(ByVal itemid As String)

        Me.ViewState("VIEW") = "SFORM"
        SetCurrentTab()

        Dim sreport As String = LienView.Provider.PrintStateForm(itemid)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.Session("spreadsheet") = System.IO.Path.GetFileName(sreport)

            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View2)
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View2)
        End If

    End Sub
    Protected Sub btnEditSForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditSForm.Click
        Session("currenttab") = "SFORM"
        btnEditSForm = TryCast(sender, LinkButton)
        Dim s As String = "~/MainDefault.aspx?jobview.jobeditsform&itemid=" & Me.itemid & "&subid=" & btnEditSForm.Attributes("rowno") & "&PageName=JobView"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnDeleteSForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteSForm.Click
        Session("currenttab") = "SFORM"
        Session("exitviewer") = "1"
        'Me.ViewState("VIEW") = "SFORM"
        SetCurrentTab()

        btnDeleteSForm = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobOtherNoticeLog
        myitem.JobNoticeLogId = btnDeleteSForm.Attributes("rowno")
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        LoadData(Me.itemid)

    End Sub
#End Region
#Region "Navigation"
    Protected Sub btnFirst_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetJobFromList("F")
    End Sub
    Protected Sub btnLast_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetJobFromList("L")
    End Sub
    Protected Sub btnPrevious_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        GetJobFromList(-1)
    End Sub
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles btnNext.Click
        GetJobFromList(+1)
    End Sub
    Private Sub GetJobFromList(ByVal increment As String)
        Select Case increment
            Case "F"
                myview.RowIndex = 0
            Case "L"
                myview.RowIndex = myview.Count - 1
            Case Else
                myview.RowIndex = GetRowIndex() + increment
        End Select

        If myview.RowIndex < 0 Then
            myview.RowIndex = 0
        End If
        If myview.RowIndex >= myview.Count Then
            myview.RowIndex = myview.Count - 1
        End If

        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myview.RowItem("JobId")
        If Me.Session("LIST") <> Nothing Then
            s += "&list=" & Me.Session("LIST")
        End If

        Response.Redirect(Me.Page.ResolveUrl(s), True)


    End Sub
    Private Function GetRowIndex() As Integer
        Dim myidx As Integer = 0
        myview.MoveFirst()
        Do Until myview.EOF
            If myview.RowItem("jobid") = Me.itemid Then
                Return myidx
            End If
            myidx += 1
            myview.MoveNext()
        Loop
        Return myidx = 0

    End Function
    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Dim s As String = "~/MainDefault.aspx?jobview.jobview&page=1"
        If IsNothing(Me.Session("LIST")) = False Then
            If Me.Session("LIST").ToString.ToUpper = "LDR" Then
                s = "~/MainDefault.aspx?jobview.liensdayremaininglist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "TPR" Then
                s = "~/MainDefault.aspx?jobview.texaspendinglist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "WDR" Then
                s = "~/MainDefault.aspx?jobview.waiversdayremaininglist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "INV" Then
                s = "~/MainDefault.aspx?view.invoicealertlist&page=1"
            End If
            If Me.Session("LIST").ToString.ToUpper = "TJR" Then
                s = "~/MainDefault.aspx?jobview.TexasReview"
            End If
            If Me.Session("LIST").ToString.ToUpper = "RRL" Then
                s = "~/MainDefault.aspx?jobview.ReviewJobList&page=1"
            End If
        End If
        Dim fullpath As String
        fullpath = Request.Url.AbsoluteUri
        If fullpath.Contains("texaspendingList") Then
            s = "~/MainDefault.aspx?jobview.texaspendinglist" 'added by jaywanti
        End If
        Me.Session("LIST") = Nothing
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
    Protected Sub btnExitViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitViewer.Click
        'Me.MultiView1.SetActiveView(Me.View1)
        'SetCurrentTab()
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Session("tabindex") = "main"
        'selected tab issue -- add 28/01/2015
        'If Not String.IsNullOrEmpty(qs.GetParameter("tabstate")) Then
        '    Session("currenttab") = qs.GetParameter("VIEW")
        'End If

        'If Not String.IsNullOrEmpty(qs.GetParameter("tabstate")) Then
        '    Session("currenttab") = qs.GetParameter("tabstate")
        'End If
        'navigation issues ---add TabState in queryString 1/2/2012
        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & qs.GetParameter("itemid") & "&tabindex=" & 4 & "&tabstate=" & Session("currenttab")
        Session("exitviewer") = "1"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub
#End Region
#Region "Print Forms"
    Public Sub PrintNotice()

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")
        'Dim sreportname As String = CRF.BLL.Providers.LiensClientView.PrintPrelimNotice(CRF.CLIENTVIEW.BLL.LienView.Provider.GetCurrentUser, s(1))
        'If System.IO.File.Exists(sreportname) = False Then Exit Sub
        'Dim sreport = "~/userdata/output/" & System.IO.Path.GetFileName(sreportname)
        'Me.panJobInfo.Visible = False
        'Me.panReportViewer.Visible = True
        'Me.plcReportViewer.Controls.Clear()
        'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 550, 650, "true"))

    End Sub
    Public Sub PrintWaivers()
        Session("currenttab") = "WAIVER"
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)

        Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")
        'Dim sreportname As String = CRF.BLL.Providers.LiensClientView.PrintJobWaiver(CRF.CLIENTVIEW.BLL.LienView.Provider.GetCurrentUser, s(1))
        'If System.IO.File.Exists(sreportname) = False Then Exit Sub
        'Dim sreport = "~/userdata/output/" & System.IO.Path.GetFileName(sreportname)
        'Me.panJobInfo.Visible = False
        'Me.panReportViewer.Visible = True
        'Me.plcReportViewer.Controls.Clear()
        'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 550, 650, "true"))

    End Sub
    Public Sub PrintStateForm()

        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        Dim s As String() = Strings.Split(qs.GetParameter("action"), ".")

        'Dim sreportname As String = CRF.BLL.Providers.LiensClientView.PrintOtherNotice(CRF.CLIENTVIEW.BLL.LienView.Provider.GetCurrentUser, s(1))
        'If System.IO.File.Exists(sreportname) = False Then Exit Sub
        'Dim sreport = "~/userdata/output/" & System.IO.Path.GetFileName(sreportname)
        'Me.panJobInfo.Visible = False
        'Me.panReportViewer.Visible = True
        'Me.plcReportViewer.Controls.Clear()
        'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(sreport), 550, 650, "true"))

    End Sub


#End Region

    Protected Sub btnTableLayout_Click(sender As Object, e As EventArgs)
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwNotes.Rows.Count = 0 Then
            Me.gvwNotes.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        If Not Me.gvwLegalParties.Rows.Count = 0 Then
            Me.gvwLegalParties.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwNotices.Rows.Count = 0 Then
            Me.gvwNotices.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwWaivers.Rows.Count = 0 Then
            Me.gvwWaivers.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwStateForms.Rows.Count = 0 Then
            Me.gvwStateForms.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        If Not Me.gvwMergeJobs.Rows.Count = 0 Then
            Me.gvwMergeJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        If Not Me.gvwTexasReq.Rows.Count = 0 Then
            Me.gvwTexasReq.HeaderRow.TableSection = TableRowSection.TableHeader
        End If

        ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncLegalPartiesList", "CallSuccessFuncLegalPartiesList();", True)
    End Sub

    Protected Sub btnDeleteWaiverId_Click(sender As Object, e As EventArgs)
        Session("currenttab") = "WAIVER"
        Session("exitviewer") = "1"
        SetCurrentTab()

        btnDeleteWaiver = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobWaiverLog
        myitem.JobWaiverLogId = hdnWaiverId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)
        'LoadData(Me.itemid)
        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&view=" & "WAIVER"
        Response.Redirect(Me.Page.ResolveUrl(s), True)

    End Sub

    Protected Sub btnStateFormId_Click(sender As Object, e As EventArgs)
        Session("currenttab") = "SFORM"
        Session("exitviewer") = "1"
        'Me.ViewState("VIEW") = "SFORM"
        SetCurrentTab()

        btnDeleteSForm = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobOtherNoticeLog
        myitem.JobNoticeLogId = hdnStateFormId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeletes", "CallSuccessFuncDelete();", True)
        'LoadData(Me.itemid)
        Dim s = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myjob.Id & "&view=" & "SFORM"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub

    Protected Sub btnTXReqId_Click(sender As Object, e As EventArgs)
        Me.ViewState("VIEW") = "VREQ"
        SetCurrentTab()

        btnDeleteVReq = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobVerifiedSentRequest
        myitem.Id = hdnTXReqId.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeletesData", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
    End Sub

    Protected Sub btnTexasRequest_Click(sender As Object, e As EventArgs)
        Me.ViewState("VIEW") = "TREQ"
        SetCurrentTab()

        btnDeleteTReq = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobTexasRequest
        myitem.Id = hdnTexasRequest.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeleted", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
        If Me.ViewState("VIEW") = "TREQ" Then
            'Me.TabContainer.ActiveTabIndex = 7
            'Me.TabContainer.TabIndex = 7
            hddbTabName.Value = "NoticeRequest"
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsDta", "ActivateTab('" & hddbTabName.Value & "');", True)
        End If
    End Sub

    Protected Sub btnInvoiceid_Click(sender As Object, e As EventArgs)
        Me.ViewState("VIEW") = "INVC"
        SetCurrentTab()
        btnDeleteINVC = TryCast(sender, LinkButton)
        Dim myitem As New CRFDB.TABLES.JobInvoices
        myitem.Id = hdnInvoiceid.Value
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Delete(myitem)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDeletedInvoices", "CallSuccessFuncDelete();", True)
        LoadData(Me.itemid)
    End Sub

    Protected Sub btnNoteDetail_Click(sender As Object, e As EventArgs)
        Dim myItem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        Dim test As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActionHistory
        Dim test1 As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobActions
        If hdnBatchJobNoteId.Value > 1 Then
            Dim result As String = ""

            If hdnNoteType.Value = "NH" Then
                Dim MYSQL As String = "SELECT Id,DateCreated,UserId,EnteredByUserId,Note as Note FROM JOBHISTORY (NOLOCK) WHERE ID =" & hdnBatchJobNoteId.Value
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                result = myview.RowItem("Note")
            ElseIf hdnNoteType.Value = "AH" Then
                Dim MYSQL As String = "SELECT JobActionHistoryId as Id,DateCreated,RequestedBy,RequestedByUserId,HistoryNote as Note FROM dbo.JobActionHistory (NOLOCK) INNER JOIN dbo.JobActions (NOLOCK) ON dbo.JobActionHistory.ActionId = dbo.JobActions.Id WHERE JobActionHistoryId =" & hdnBatchJobNoteId.Value
                Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                result = myview.RowItem("Note")
            Else
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchJobNoteId.Value, myItem)
                result = myItem.Note
            End If

            Me.txtNotes.Text = result.Replace("<", "[").Replace(">", "]")

            'CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchJobNoteId.Value, myItem)
            'Me.txtNotes.Text = myItem.Note.Replace("<", "[").Replace(">", "]")
            'If Me.txtNotes.Text.Trim = "" Then
            '    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(hdnBatchJobNoteId.Value, test)
            '    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(test.ActionId, test1)
            '    Me.txtNotes.Text = test1.HistoryNote.Replace("<", "[").Replace(">", "]")
            'End If

        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsNotePopUp", "DisplayNotePopUp();", True)
    End Sub

    Protected Sub btnNoticeRequest_Click(sender As Object, e As EventArgs)
        If Me.MultiView1.ActiveViewIndex > 0 Then Exit Sub

        Me.Page.Validate()
        Me.ValidationSummary1.Visible = True
        If Me.Page.IsValid = False Then
            Me.ValidationSummary1.Visible = True
            Exit Sub
        End If
    End Sub
    Protected Sub btnSaveAdditionalInfo_Click(sender As Object, e As EventArgs)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)
        With myjob
            .APNNum = Me.txtAPNNumber.Text.Trim()
            .PONum = Me.txtPONumber.Text.Trim()
            .BondNum = Me.txtBondNum.Text.Trim()
            .BuildingPermitNum = Me.txtpermitNum.Text.Trim()
            .SpecialInstruction = Me.txtSpecialInst.Text.Trim()
            .Note = Me.txtLegalDesc.Text.Trim()
            .EquipRate = txtEquiprate.Text.Trim()
            .EquipRental = txtEquipDesc.Text.Trim()
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        End With
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivAdditionalInfo','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
    End Sub

    Protected Sub rptRentalInvoice_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim DR As System.Data.DataRowView = e.Item.DataItem
            Dim lblInvoiceDate As Label = CType(e.Item.FindControl("lblInvoiceDate"), Label)
            Dim lblClientStatus As Label = DirectCast(e.Item.FindControl("lblClientStatus"), Label)
            Dim lblSeqNum As Label = DirectCast(e.Item.FindControl("lblSeqNum"), Label)
            lblInvoiceDate.Text = "" & Utils.FormatDate(DR("StartDate")) & " thru " & Utils.FormatDate(DR("EndDate"))
            Dim gv As Repeater = DirectCast(e.Item.FindControl("rptRentalInvoiceDetail"), Repeater)
            Dim hdnBatchJobrentalId As HiddenField = CType(e.Item.FindControl("hdnBatchJobrentalId"), HiddenField)
            Dim lblOriginalBalance As Label = DirectCast(e.Item.FindControl("lblOriginalBalance"), Label)
            Dim lblCurrentBalance As Label = DirectCast(e.Item.FindControl("lblCurrentBalance"), Label)
            If lblClientStatus.Text = "C" Then
                lblClientStatus.Text = "Closed"
            ElseIf lblClientStatus.Text = "O" Then
                lblClientStatus.Text = "Open"
            End If
            If lblSeqNum.Text.Trim() <> "" Then
                lblSeqNum.Text = "Br #" + lblSeqNum.Text
            End If
            If Not (hdnBatchJobrentalId.Value Is Nothing) Then
                Dim view As New DataView
                Dim strquery As String = ""
                strquery = "InvoiceNum=" + "'" + hdnBatchJobrentalId.Value + "'" + " AND ContractType='O'"
                view.Table = dsjobinfo.Tables(18)
                view.RowFilter = strquery
                gv.DataSource = view.ToTable()
                'gv.DataSource = view.Table()
                gv.DataBind()
                'Dim dtall As New DataTable
                'dtall.Merge(view.ToTable())
                'If (Not (dsjobinfo.Tables(20) Is Nothing) And dsjobinfo.Tables(20).Rows.Count > 0) Then
                '    Dim view1 As New DataView
                '    Dim strquery1 As String = ""
                '    strquery1 = "ContractType<>'O'"
                '    view1.Table = dsjobinfo.Tables(20)
                '    view1.RowFilter = strquery1
                '    dtall.Merge(view1.ToTable())
                'End If
                Dim TotalOriginalBalance As Decimal
                Dim TotalCurrentBalance As Decimal
                For index = 0 To view.ToTable().Rows.Count - 1
                    '    TotalOriginalBalance += view.ToTable().Rows(index)("OriginalBalance")
                    '    TotalCurrentBalance += view.ToTable().Rows(index)("CurrentBalance")
                    'For index = 0 To view.Table().Rows.Count - 1
                    TotalOriginalBalance += view.Table().Rows(index)("OriginalBalance")
                    TotalCurrentBalance += view.Table().Rows(index)("CurrentBalance")
                    'TotalOriginalBalance = view.Table().Rows(index)("OriginalBalance")
                    'TotalCurrentBalance = view.Table().Rows(index)("CurrentBalance")

                    'lblOriginalBalance.Text = Strings.FormatCurrency(TotalOriginalBalance)
                    'lblCurrentBalance.Text = Strings.FormatCurrency(TotalCurrentBalance)
                Next
                'lblOriginalBalance.Text = Strings.FormatCurrency(TotalOriginalBalance)
                'lblCurrentBalance.Text = Strings.FormatCurrency(TotalCurrentBalance)
            End If
        End If

    End Sub

    Protected Sub rptRentalInvoiceDetail_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem) Then
            Dim DR As System.Data.DataRowView = e.Item.DataItem
            Dim lblContractSeq As Label = DirectCast(e.Item.FindControl("lblContractSeq"), Label)
            Dim lblStatus As Label = DirectCast(e.Item.FindControl("lblStatus"), Label)
            Dim lblInvoiceDate As Label = CType(e.Item.FindControl("lblInvoiceDate"), Label)
            Dim lblDateUpdated As Label = CType(e.Item.FindControl("lblDateUpdated"), Label)
            Dim lblOriginalBalance As Label = DirectCast(e.Item.FindControl("lblOriginalBalance"), Label)
            Dim lblCurrentBalance As Label = DirectCast(e.Item.FindControl("lblCurrentBalance"), Label)
            'Dim lblNSSflag As Label = DirectCast(e.Item.FindControl("lblNSSflag"), Label)
            Dim lblNoticed As Label = DirectCast(e.Item.FindControl("lblNoticed"), Label)
            lblContractSeq.Text = DR("InvoiceNum") & "-" & DR("SeqNo")
            lblInvoiceDate.Text = "" & Utils.FormatDate(DR("InvoiceDate")) & ""

            If (Not String.IsNullOrEmpty(lblDateUpdated.Text)) Then
                lblDateUpdated.Text = "" & Utils.FormatDate(DR("DateUpdated")) & ""
            End If

            If lblStatus.Text = "OP" Then
                lblStatus.Text = "Open"
            ElseIf lblStatus.Text = "PD" Then
                lblStatus.Text = "Paid"
            ElseIf lblStatus.Text = "PP" Then
                lblStatus.Text = "Partial Paid"
            End If
            If lblNoticed.Text.Trim <> "" Then
                If lblNoticed.Text = "1" Then
                    lblNoticed.Text = "Yes"
                Else
                    lblNoticed.Text = "No"
                End If
            End If
            'lblOriginalBalance.Text = Strings.FormatCurrency(lblOriginalBalance.Text)
            'lblCurrentBalance.Text = Strings.FormatCurrency(lblCurrentBalance.Text)
        End If


    End Sub
    Protected Sub gvInvoiceGrid_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            Dim lblContractSeq As Label = DirectCast(e.Row.FindControl("lblContractSeq"), Label)
            Dim lblStatus As Label = DirectCast(e.Row.FindControl("lblStatus"), Label)
            Dim lblOriginalBalance As Label = DirectCast(e.Row.FindControl("lblOriginalBalance"), Label)
            Dim lblType As Label = DirectCast(e.Row.FindControl("lblType"), Label)
            Dim lblInvoiceDate As Label = DirectCast(e.Row.FindControl("lblInvoiceDate"), Label)
            Dim lblDateUpdated As Label = DirectCast(e.Row.FindControl("lblDateUpdated"), Label)
            Dim lblCurrentBalance As Label = DirectCast(e.Row.FindControl("lblCurrentBalance"), Label)
            'Dim lblNSSflag As Label = DirectCast(e.Row.FindControl("lblNSSflag"), Label)
            'DataBinder.Eval(e.Item.DataItem, "ProductID").ToString();
            If CType(e.Row.DataItem, DataRowView).Row.Table.Columns.Contains("RANum") Then
                lblContractSeq.Text = DR("RANum") & "-" & DR("SeqNo")
            End If

            lblInvoiceDate.Text = "" & Utils.FormatDate(DR("InvoiceDate")) & ""
            If (Not String.IsNullOrEmpty(lblDateUpdated.Text)) Then
                lblDateUpdated.Text = "" & Utils.FormatDate(DR("DateUpdated")) & ""
            End If

            If lblStatus.Text = "OP" Then
                lblStatus.Text = "Open"
            ElseIf lblStatus.Text = "PD" Then
                lblStatus.Text = "Paid"
            ElseIf lblStatus.Text = "PP" Then
                lblStatus.Text = "Partial Paid"
            End If
            If lblType.Text = "O" Then
                lblType.Text = "Rental"
            ElseIf lblType.Text = "W" Then
                lblType.Text = "Work"
            ElseIf lblType.Text = "S" Then
                lblType.Text = "Sales"
            End If
            lblOriginalBalance.Text = Strings.FormatCurrency(lblOriginalBalance.Text)
            lblCurrentBalance.Text = Strings.FormatCurrency(lblCurrentBalance.Text)
        End If
    End Sub

    Protected Sub btnSaveTNRequest_Click(sender As Object, e As EventArgs)
        '''''''''''''''''''''''''''
        Dim CheckServiceTypeId = Me.hdnServiceTypeId.Value

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)
        If (CheckServiceTypeId = "1") Then
            With myjob
                .VerifyJob = 1
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
            End With
        End If


        If (CheckServiceTypeId = "4") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 1
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
            End With
        End If

        If (CheckServiceTypeId = "8") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 1
            End With
        End If
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        '''''''''''''''''''''''''''''
        If (Me.TNLastDateWorkPerformed.Value = "") Then
            Me.CustomValidator2.ErrorMessage = "Last Date Work Performed is required for this notice request"
            Me.CustomValidator2.IsValid = False
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTN','show');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTN','hide'); $('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)

            Exit Sub
        End If
        With myverified
            .JobId = itemid
            .DateCreated = Now()
            .AmountOwed = Me.TNAmountOwed.Value
            .MonthDebtIncurred = Me.TNLastDateWorkPerformed.Value
            '.StmtOfAccts = Me.StmtofAccts.Text
            .SubmittedBy = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .SubmittedByUserId = Me.CurrentUser.Id
        End With
        Dim MYSQL As String = " SELECT * FROM JobVerifiedSentRequest WHERE DATEPROCESSED Is NULL"
        MYSQL += " And JobId = " & myreq.JobId
        MYSQL += " And Id <> " & myreq.Id
        Dim MYVIEW As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView(MYSQL)

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)

        If myverified.Id > 0 Then
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myverified)
        Else
            myverified.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myverified)
            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = itemid
                .DateCreated = Now()
                .Note = "Notice request made. Request is now being processed."
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)

            End With
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalsuccessShow(true);", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTN','hide'); $('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)


        LoadData(Me.itemid)

        hddbTabName.Value = "NoticeRequest"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsDta", "ActivateTab('" & hddbTabName.Value & "');", True)

    End Sub
    Protected Sub btnSaveTXRequest_Click(sender As Object, e As EventArgs)
        ''''''''''''''''''''''''''''''''''''''''''''''
        Dim CheckServiceTypeId = Me.hdnServiceTypeId.Value

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)
        If (CheckServiceTypeId = "1") Then
            With myjob
                .VerifyJob = 1
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
            End With
        End If

        If (CheckServiceTypeId = "4") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 1
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
            End With
        End If

        If (CheckServiceTypeId = "5") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 1
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
            End With
        End If

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
        ''''''''''''''''''''''''''''''''''''''''''''''
        If (Me.MonthDebtIncurred.Value = "") Then
            Me.CustomValidator2.ErrorMessage = "Month/Year Work Performed is required for this notice request"
            Me.CustomValidator2.IsValid = False
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('.btnSaveTNRequest').remove(); $('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTX','show');$('.btnSaveTNRequest').remove();", True)
            '''''''''''''''''''''''''''''''''''''
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js12367", "BindDropDownForTX()", True)
            '''''''''''''''''''''''''''''''''''''
            Exit Sub
        End If

        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js12367", "BindDropDownForTX()", True)

        With myreq
            .JobId = itemid
            .DateCreated = Now()
            .AmountOwed = Me.AmountOwed.Value
            .IsTX60Day = Me.IsTX60Day.Checked
            .IsTX90Day = Me.IsTX90Day.Checked
            .MonthDebtIncurred = Me.MonthDebtIncurred.Value
            .StmtOfAccts = Me.StmtofAccts.Text
            .SubmittedBy = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .SubmittedByUserId = Me.CurrentUser.Id
        End With
        Dim MYSQL As String = " SELECT * FROM JOBTEXASREQUEST WHERE DATEPROCESSED Is NULL"
        MYSQL += " And JobId = " & myreq.JobId
        MYSQL += " And Id <> " & myreq.Id
        If Me.IsTX60Day.Checked = True Then
            MYSQL += " And IsTx60day = 1"
        Else
            MYSQL += " And IsTx90day = 1"
        End If
        Dim MYVIEW As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView(MYSQL)

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)
        If myjob.FederalJob = True Then
            Me.CustomValidator2.ErrorMessage = "This is a Federal Job and requires a Miller Act Notice 90 days after last furnishing. Call 805-823-8032 for assistance"
            Me.CustomValidator2.IsValid = False
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('.btnSaveTNRequest').remove();$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTX','show');$('.btnSaveTNRequest').remove();", True)
            Exit Sub
        End If

        If MYVIEW.Count > 0 Then
            If Me.JobState.Text = "TX" Then
                If Me.IsTX60Day.Checked = True Then
                    Me.CustomValidator2.ErrorMessage = "You already have a 60 DAY request for this Job"
                    Me.CustomValidator2.IsValid = False
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('.btnSaveTNRequest').remove();$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTX','show');$('.btnSaveTNRequest').remove();", True)
                    Exit Sub
                Else
                    Me.CustomValidator2.ErrorMessage = "You already have a 90 DAY request for this Job"
                    Me.CustomValidator2.IsValid = False
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('.btnSaveTNRequest').remove();$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTX','show');$('.btnSaveTNRequest').remove();", True)
                    Exit Sub
                End If
            End If
        End If
        If myjob.PublicJob = True And Me.StmtofAccts.Text.Length < 4 And Me.IsTX90Day.Checked Then
            Me.CustomValidator2.ErrorMessage = "A statement of account is required for this notice request"
            Me.CustomValidator2.IsValid = False
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('.btnSaveTNRequest').remove();$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTX','show');$('.btnSaveTNRequest').remove();", True)
            Exit Sub
        End If
        If Me.JobState.Text = "TX" Then
            If myjob.PublicJob = True And Me.IsTX90Day.Checked And Me.StmtofAccts.Text.Length < 4 Then
                Me.CustomValidator2.ErrorMessage = "A statement of account is required for this notice request"
                Me.CustomValidator2.IsValid = False
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('.btnSaveTNRequest').remove();$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTX','show');$('.btnSaveTNRequest').remove();", True)
                Exit Sub
            End If
            ' 8/10/2015 - LEC   
            If myjob.PrivateJob = True And myjob.ResidentialBox = True And Me.IsTX60Day.Checked Then
                Me.CustomValidator2.ErrorMessage = "For a residential construction project in Texas, you must send the owner and the GC a notice of the unpaid balance by the 15th of the 2nd month.  Please select the 3rd month notice for this purpose as the 2nd month notice to the GC 'only' is not required or appropriate. TX Sec. 53.252"
                Me.CustomValidator2.IsValid = False
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js123", "modalHideShow('ModaldivRequestNoticeTX','show');", True)
                Exit Sub
            End If
        End If
        If myreq.Id > 0 Then
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
        Else
            myreq.DateCreated = Today
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myreq)
            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = itemid
                .DateCreated = Now()
                .Note = "Notice request made. Request is now being processed."
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)

            End With
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalHideShow('ModaldivRequestNoticeTX','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');", True)
        'Response.Redirect(Request.RawUrl)
        LoadData(Me.itemid)

        hddbTabName.Value = "NoticeRequest"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActiveaListsDta", "ActivateTab('" & hddbTabName.Value & "');", True)

    End Sub
    <System.Web.Services.WebMethod()>
    Public Shared Function GetCurrentTime(ByVal name As String) As String
        Return "Hello " & name & Environment.NewLine & "The Current Time is: " &
                 DateTime.Now.ToString()
    End Function

    Protected Sub btnSaveNoticeRequest_Click(sender As Object, e As EventArgs)

        'Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_JobView_NoticeRequest
        'mysproc.JobId = Me.itemid
        'mysproc.submittedbyuserid = Me.CurrentUser.Id
        'CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecNonQuery(mysproc)
        ''result = CRFS.ClientView.CrfsBll.UpdateJobView_NoticeRequest(Me.itemid, Me.CurrentUser.Id, Me.AmountOwedNotice.Text)

        'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)
        '    With myjob
        '        Me.hdnDateAssigned.Value = Utils.FormatDate(.DateAssigned)
        '        Me.NoticeRequested.Value = Utils.FormatDate(.DateAssigned)
        '    End With
        '    If Me.hdnDateAssigned.Value <> "" Then
        '        IsNoticeRequested.Value = True
        '    Else
        '        IsNoticeRequested.Value = False
        '    End If
        '    Me.JobEdit1.ClearData(Me.itemid)
        '    Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        '    With mynote
        '        .JobId = itemid
        '        .DateCreated = Now()
        '        .Note = "Job assigned for information verification. Job is now being researched."
        '        .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
        '        .ClientView = True
        '        .EnteredByUserId = Me.UserInfo.UserInfo.ID
        '        .ActionTypeId = 0
        '        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)

        '    End With

        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDisabledNoticeRequest", "SetDisabledNoticeRequest();", True)
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalsuccessShow(false);", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsServiceTypeDropdown", "BindServiceTypeDropdown();", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp1", "ShowServiceTypeModal();", True)
    End Sub
    Protected Sub gvwMergeJobs_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            Dim lblAddress As Label = TryCast(e.Row.FindControl("lblJobAddress"), Label)
            lblAddress.Text = DR("JobAdd1") & " " & DR("JobCity") & " " & DR("JobState") & " " & DR("JobZip")
            'btnPrntNotice = TryCast(e.Row.FindControl("btnPrntNotice"), LinkButton)
            'btnPrntNotice.Attributes("rowno") = DR("JobNoticeHistoryId")
            'e.Row.Cells(1).Text = "" & Utils.FormatDate(DR("DatePrinted")) & ""

        End If
    End Sub

    Protected Sub btnMergeJob_Click(sender As Object, e As EventArgs)
        LoadData(Session("jobid"))
        hddbTabName.Value = "MergeJob"
        If gvwMergeJobs.Rows.Count > 0 Then
            Me.gvwMergeJobs.HeaderRow.TableSection = TableRowSection.TableHeader
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncJobMergeList", "CallSuccessFuncJobMergeList();", True)
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1DataModalJobMerge", "modalHideShow('ModalJobMerge','hide');$('div.modal-backdrop').remove();ActivateTab('" & hddbTabName.Value & "');$('body').removeClass('modal-open');$('.sweet-alert').hide();$('.sweet-overlay').hide();", True)
    End Sub

    Protected Sub btnBackFilter_Click(sender As Object, e As EventArgs)
        Dim s As String = "~/MainDefault.aspx?jobview.jobview"
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''
    Protected Sub btnOK_Click(sender As Object, e As EventArgs)

        Try


            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "temp344", "modalsuccessHideServiceType();", True)
            'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "showLoadingImage", "showLoadingImage();", True)

            ' get service type id which is set from javascript code
            Dim CheckServiceTypeId = Me.hdnServiceTypeId.Value
            'Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_JobView_NoticeRequest
            'mysproc.JobId = Me.itemid
            'mysproc.submittedbyuserid = Me.CurrentUser.Id
            'mysproc.JobTypeOption = CheckServiceTypeId
            'CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecNonQuery(mysproc)
            'result = CRFS.ClientView.CrfsBll.UpdateJobView_NoticeRequest(Me.itemid, Me.CurrentUser.Id, Me.AmountOwedNotice.Text)

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.itemid, myjob)

        If (CheckServiceTypeId = "2") Then
            With myjob
                .VerifyJob = 1
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 11
                '.StatusCode = "AVO"
            End With
        End If

        If (CheckServiceTypeId = "1") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 1
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 3
                '.StatusCode = "ACT"
            End With
        End If

        If (CheckServiceTypeId = "3") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 1
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 39
                '.StatusCode = "CVA"
            End With
        End If

        If (CheckServiceTypeId = "4") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 1
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 0
                '.StatusCodeId = 29
                '.StatusCode = "CNA"
            End With
        End If

        If (CheckServiceTypeId = "6") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                    .VerifyOWOnlyTX = 0
                    .SendAsIsBox = 0
                .VerifyOWOnlySend = 1
                .VerifyOWOnly = 0
                '.StatusCodeId = 76
                '.StatusCode = "AOO"
            End With
        End If

        If (CheckServiceTypeId = "5") Then
            With myjob
                .VerifyJob = 0
                .PrelimBox = 0
                .TitleVerifiedBox = 0
                .VerifyOWOnlyTX = 0
                .SendAsIsBox = 0
                .VerifyOWOnlySend = 0
                .VerifyOWOnly = 1
                '.StatusCodeId = 76
                '.StatusCode = "AOO"
            End With
        End If

            If (CheckServiceTypeId = "11") Then
                With myjob
                    .VerifyJob = 0
                    .PrelimBox = 0
                    .TitleVerifiedBox = 0
                    .VerifyOWOnlyTX = 0
                    .SendAsIsBox = 1
                    .VerifyOWOnlySend = 0
                    .VerifyOWOnly = 0
                    '.StatusCodeId = 76
                    '.StatusCode = "AOO"
                End With
            End If


            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
            '''''''''''''''''''''''''''''''
            With myjob
                Me.hdnDateAssigned.Value = Utils.FormatDate(.DateAssigned)
                Me.NoticeRequested.Value = Utils.FormatDate(.DateAssigned)
            End With
            If Me.hdnDateAssigned.Value <> "" Then
                IsNoticeRequested.Value = True
            Else
                IsNoticeRequested.Value = False
            End If
            Me.JobEdit1.ClearData(Me.itemid)

            Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_JobView_NoticeRequest
            mysproc.JobId = Me.itemid
            mysproc.submittedbyuserid = Me.CurrentUser.Id
            mysproc.JobTypeOption = CheckServiceTypeId
            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.ExecNonQuery(mysproc)


            Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
            With mynote
                .JobId = itemid
                .DateCreated = Now()
                .Note = "Job assigned for information verification. Job is now being researched."
                .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                .ClientView = True
                .EnteredByUserId = Me.UserInfo.UserInfo.ID
                .ActionTypeId = 0
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)

            End With

            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDisabledNoticeRequest", "SetDisabledNoticeRequest();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "hideLodingImage", "hideLodingImage();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "modalsuccessShow(false);", True)

        Catch ex As Exception
             Dim mymsg As String = ex.Message
        End Try
    End Sub

    '''''''''''''''''''''''''''''''''''''''''''''''''
    '^^^***CreatedBy Pooja   01/09/20 *** ^^^
    Protected Sub btnDeleteDoc_Click(sender As Object, e As EventArgs)
        Dim myitem As New TABLES.JobAttachments
        myitem.Id = hdnDocId.Value
        ProviderBase.DAL.Delete(myitem)
        hddbTabName.Value = "Docs"
        Dim MYSQL As String = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments "
        MYSQL += " Where JobId = " & Me.JobId.Text
        Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
        Me.gvwDocs.DataSource = MYDT
        Me.gvwDocs.DataBind()
        If Not Me.gvwDocs.Rows.Count = 0 Then
            Me.gvwDocs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetTabName", "SetTabName('Docs');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js1", "ActivateTab('" & hddbTabName.Value & "');", True)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallSuccessFuncDelete", "CallSuccessFuncDelete();", True)

    End Sub
End Class
