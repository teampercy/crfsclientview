﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<style>
    .panel-body, .form-control.input-sm {
        font-size: 11px;
    }

    #ctl32_ctl00_gvJobViewList tbody tr:hover {
        cursor: pointer;
    }

    #ctl32_ctl00_gvJobViewList thead tr th {
        font-weight: bold;
    }

    #ctl32_ctl00_gvJobViewList thead tr th, #ctl32_ctl00_gvJobViewList tbody tr td {
        /*border: 0;*/
    }

    #JobFilterBody .radio-custom label {
        width: 95px;
    }

    #JobFilterBody .radio-custom.radio-default.radio-inline {
        min-height: 0px;
        padding-top: 0px;
    }

    .RemovePadding {
        padding: 0px !important;
    }

    .nowordwraptd {
        padding-left: 5px;
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }

    .activePage {
        z-index: 3;
        color: white !important;
        cursor: default;
        background-color: #3a6dae !important;
        border-color: #3a6dae !important;
    }
</style>

<script type="text/javascript">


    function modalshow() {
        debugger;
        // alert("hii");
        $("#modelDivExport").modal('show');
        return false;
    }
    function modalhide() {
        //alert("hide");

        //alert($('input[type=radio]:checked').val());
        if ($('input[type=radio]:checked').val() == "PDFReport") {
            //$("#LodingPanel").show();
        }
        else {
            HideLoadingPanel();

        }

        $("#modelDivExport").modal('hide');
        document.getElementById("<%= btnExportReport.ClientID %>").click();
        return false;
    }

    var withoutcurrfrombal;
    var withoutcurrthrubal;
    function FromBalance_LostFocus() {
        var FromBalanceCntrl = $('#<%=FromBalance.ClientID%>');
        withoutcurrfrombal = FromBalanceCntrl.val();
        if (FromBalanceCntrl.val().length > 0 && FromBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(FromBalanceCntrl.val().replace(",", ""))
            FromBalanceCntrl.val('$' + value.toFixed(2));
            checkJobBalanceRange();
        }
    }

    function ThruBalance_LostFocus() {
        var ThruBalanceCntrl = $('#<%=ThruBalance.ClientID%>');
        withoutcurrthrubal = ThruBalanceCntrl.val();
        if (ThruBalanceCntrl.val().length > 0 && ThruBalanceCntrl.val().indexOf("$") == -1) {
            var value = parseFloat(ThruBalanceCntrl.val().replace(",", ""))
            ThruBalanceCntrl.val('$' + value.toFixed(2));
            checkJobBalanceRange();
        }
    }

    function checkJobBalanceRange() {
        debugger;
        //alert(withoutcurrfrombal);
        if (withoutcurrfrombal > 0 && withoutcurrthrubal > 0) {
            $('#<%=chkByBalance.ClientID%>').prop('checked', true);
            validateJobFilter();
        }
        else {
            $('#<%=chkByBalance.ClientID%>').prop('checked', false);
        }


    }

    function CheckDeadlineDateRange() {
        debugger;
        //alert("start"+document.getElementById('<%=Calendar3.ClientID%>').value);
         if ((document.getElementById('<%=Calendar3.ClientID%>').value != "") && (document.getElementById('<%=Calendar4.ClientID%>').value != "")) {
             // alert("start"+document.getElementById('<%=Calendar3.ClientID%>').value);

             $('#<%=chkByDeadlineDate.ClientID%>').prop('checked', true);
             validateJobFilter();
             // alert("end");
         }
         else {
             $('#<%=chkByDeadlineDate.ClientID%>').prop('checked', false);
         }
     }
     function CheckNoticerequestRange() {
         debugger;

         if ((document.getElementById('<%=dtpDateAssignedFrom.ClientID%>').value != "") && (document.getElementById('<%=dtpDateAssignedThru.ClientID%>').value != "")) {

             $('#<%=chkDateAssigned.ClientID%>').prop('checked', true);
             validateJobFilter();

         }
         else {
             $('#<%=chkDateAssigned.ClientID%>').prop('checked', false);
         }
     }
     function CheckNoticesentRange() {
         debugger;

         if ((document.getElementById('<%=dtpNoticeSentFrom.ClientID%>').value != "") && (document.getElementById('<%=dtpNoticeSentThru.ClientID%>').value != "")) {

             $('#<%=chkNoticeSent.ClientID%>').prop('checked', true);
             validateJobFilter();

         }
         else {
             $('#<%=chkNoticeSent.ClientID%>').prop('checked', false);
         }
     }
     function MaintainMenuOpen() {
         SetHeaderBreadCrumb('RentalView', 'View Jobs', 'Find Jobs');
         MainMenuToggle('liViewJobs');
         SubMenuToggle('liViewAllJobs')
         return false;

     }
     function ClearControl() {
         $('.datepicker').val("");
     }
     function BindClientSelection() {
         $('#UlClientSelectionType li').remove();
         var x = document.getElementById('<%=DropDownList1.ClientID%>');
        var txt = "";
        var i;
        if (x != null) {
            for (i = 0; i < x.length; i++) {
                $('#UlClientSelectionType').append('<li role="presentation"><a href="javascript:void(0)" role="menuitem" id="liClientSel_' + x.options[i].value + '" onclick="setSelectedClientId(' + x.options[i].value + ',\'' + x.options[i].text + '\')">' + x.options[i].text + '</a></li>');
            }
        }
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val() != "") {
            $('#btnClientSelectionType').html($('#liClientSel_' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val()).text() + "<span class='caret' ></span>");
        }
    }
    function setSelectedClientId(id, TextData) {
        debugger;
        $('#' + '<%=hdnClientSelectionId.ClientID%>').val(id);
        $('#btnClientSelectionType').html(TextData + "<span class='caret' ></span>");
        if ($('#' + '<%=hdnClientSelectionId.ClientID%>').val().length > 0) {
            <%-- alert('hii ' + $('#' + '<%=hdnClientSelectionId.ClientID%>').val() );--%>
            var value = "SELECTED";
            var radio = $("[id*=ctl33_ctl00_RadioButtonList1] input[value=" + value + "]");
            radio.attr("checked", "checked");

        }
        return false;
    }
    //Bind List to Job Status Dropdown 
    function BindJobStatus() {
        debugger;
        $('#UlJobStatus').html();
        var x = document.getElementById('<%=DrpJobStatus.ClientID%>');
         var txt = "<select class=\"form-control\" id=\"editable-select-Job-status\" tabindex=\"26\" style=\"overflow-y: auto; \">";
         var i;
         txt = txt + '<option role="presentation"><a href="javascript:void(0)" role="menuitem" id="liJobStatus_0" >--Select Job Status--</a></option>';
         if (x != null) {
             if ($('#' + '<%=hdnJobStatusSelectedId.ClientID%>').val() != "") {
                 var oldVal = $('#' + '<%=hdnJobStatusSelectedId.ClientID%>').val();
                 for (i = 0; i < x.length; i++) {
                     if (x.options[i].value == oldVal) {
                         txt = txt + '<option selected="selected" role="menuitem" id="liJobStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '<option>';
                     }
                     else {
                         txt = txt + '<option role="menuitem" id="liJobStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                     }
                 }
             }
             else {
                 for (i = 0; i < x.length; i++) {
                     txt = txt + '<option role="menuitem" id="liJobStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                 }
             }
         }

         txt = txt + "</select>";
         $('#UlJobStatus').html(txt);
         $('#editable-select-Job-status').editableSelect();

         $('#editable-select-Job-status').on('select.editable-select', function (e, li) {
             //alert('hi123');
             if (li) {
                 // alert(li.attr('jobstatusid'));
                 $('#' + '<%= hdnJobStatusSelectedId.ClientID%>').val(li.attr('value'));
                // $('#' + '<%= hdnJobStatusSelectedId.ClientID%>').val(li.attr('jobstatusid'));
                // alert($('#' + '<%= hdnJobStatusSelectedId.ClientID%>').val());
                //alert(li.attr('value'));
                //alert(li.attr('deskid'));
            }
        });
     }
    function setSelectedJobStatusId(id, TextData) {
        debugger;
        $('#UlJobStatus li:eq(0)').before("<li>first</li>");
        $('#' + '<%=hdnJobStatusSelectedId.ClientID%>').val(id);
        $('#btnJobStatus').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    //end here
    //Bind List to Desk Dropdown
    function BindDesk() {
        $('#UlDesk').html();
       <%-- var x = document.getElementById('<%=DrpDesk.ClientID%>');--%>
        var txt = "<select class=\"form-control\" id=\"editable-select-desk\" tabindex=\"33\" style=\"overflow-y: auto; \">";
        var i;
        txt = txt + '<option role="menuitem" value="0" id="liDesk_0">--Select Desk--</option>';
        if (x != null) {
          <%--  if ($('#' + '<%=hdnDeskSelectedId.ClientID%>').val() != "") {
                var oldVal = $('#' + '<%=hdnDeskSelectedId.ClientID%>').val();
                for (i = 0; i < x.length; i++) {
                    if (x.options[i].value == oldVal) {
                        txt = txt + '<option selected="selected" role="menuitem" id="liDesk_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                    }
                    else {
                        txt = txt + '<option role="menuitem" id="liDesk_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                    }
                }
            }
            else {
                for (i = 0; i < x.length; i++) {
                    txt = txt + '<option role="menuitem" id="liDesk_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                }
            }--%>
        }
        txt = txt + "</select>";
        $('#UlDesk').html(txt);
        $('#editable-select-desk').editableSelect();
        $('#editable-select-desk').on('select.editable-select', function (e, li) {
            //alert('hi123');
           <%-- if (li) {
                $('#' + '<%= hdnDeskSelectedId.ClientID%>').val(li.attr('value'));
            }--%>
        });

    }
    function setSelectedDeskId(id, TextData) {
        alert(2);
        alert(id);
     <%--   $('#' + '<%=hdnDeskSelectedId.ClientID%>').val(id);--%>
        $('#btnDesk').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    //Bind List to CRFSStatus Dropdown
    function BindCRFSStatus() {
        debugger;
       
        $('#UlCrfsStatus').html();
        var x = document.getElementById('<%=DrpCrfsStatus.ClientID%>');
       
        <%--//var x = document.getelementbyid('<%=drpcrfsstatus.clientid%>');--%>
        var txt = "<select class=\"form-control\" id=\"editable-select-Crfs-status\" tabindex=\"27\" style=\"overflow-y: auto; \">";
        var i;
        txt = txt + '<option role="presentation"><a href="javascript:void(0)" role="menuitem" id="liCrfsStatus_0" >--Select Crfs Status--</a></option>';
        if (x != null) {
            if ($('#' + '<%=hdnCrfsStatusSelectedId.ClientID%>').val() != "") {
                var oldVal = $('#' + '<%=hdnCrfsStatusSelectedId.ClientID%>').val();
                for (i = 0; i < x.length; i++) {
                    if (x.options[i].value == oldVal) {
                        txt = txt + '<option selected="selected" role="menuitem" id="liCrfsStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '<option>';
                    }
                    else {
                        txt = txt + '<option role="menuitem" id="liCrfsStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                    }
                }
            }
            else {
                for (i = 0; i < x.length; i++) {
                    txt = txt + '<option role="menuitem" id="liCrfsStatus_' + x.options[i].value + '" value="' + x.options[i].value + '">' + x.options[i].text + '</option>';
                }
            }
        }

        txt = txt + "</select>";
        $('#UlCrfsStatus').html(txt);
        $('#editable-select-Crfs-status').editableSelect();

        $('#editable-select-Crfs-status').on('select.editable-select', function (e, li) {
            //alert('hi123');
            if (li) {

                $('#' + '<%= hdnCrfsStatusSelectedId.ClientID%>').val(li.attr('value'));

            }
        });
    }
    function setSelectedCRFSStatusId(id, TextData) {

        $('#' + '<%=hdnCrfsStatusSelectedId.ClientID%>').val(id);
        $('#btnDesk').html(TextData + "<span class='caret' ></span>");
        return false;
    }
    //end here
    function myFunctionCustomerNameLike(id) {
        if (id == 1) {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionCustomerRefLike(id) {

        if (id == 1) {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = true;
        }
    }



    function myFunctionJobNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = true;
             document.getElementById('<%=JobNameLike_2.ClientID%>').checked = false;
         }
         else {
             document.getElementById('<%=JobNameLike_1.ClientID%>').checked = false;
             document.getElementById('<%=JobNameLike_2.ClientID%>').checked = true;
         }
     }
     function myFunctionJobCityLike(id) {

         if (id == 1) {
             document.getElementById('<%=JobCityLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobCityLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobCityLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobCityLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobAddressLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNoLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionOwnerNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = true;
        }
    }
    function myFunctionContractorNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=ContractorNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=ContractorNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=ContractorNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=ContractorNameLike_2.ClientID%>').checked = true;
        }
    }
    $(document).ready(function () {
        debugger;
        //MakeJobList();

        BindJobStatus();
        BindCRFSStatus();
        BindDesk();
      
        validateJobFilter();

        //$("#modcontainer").find('input').on('blur', function () {


        $("div").on('blur change keyup', "input", function () {
            var $element = $(this);
            var eType = $element.attr('type');

            if (eType == "text" || eType == "checkbox") {
                validateJobFilter();
            }

        });
        $(document).click(function (event) {
            if (event.target.id != "editable-select-Job-status" && event.target.id != "editable-select-desk" && event.target.id != "editable-select-Crfs-status") {
                $(".es-list").css("display", "none");
                //  console.log(event.target.id);
            }
        })
    });

    function validateJobFilter() {
        var ISValid = true;
        //  $("#modcontainer input[type=text]").each(function() {
        $("#modcontainer").find("input").each(function (idx) {

            var $element = $(this);
            var eType = $element.attr('type');
            // console.log($element);
            if ($element.attr('id') == "ctl33_ctl00_Calendar3_txtYui" || $element.attr('id') == "ctl33_ctl00_Calendar4_txtYui" || $element.attr('id') == "ctl33_ctl00_FromBalance" ||
                $element.attr('id') == "ctl33_ctl00_ThruBalance" || $element.attr('id') == '<%=dtpDateAssignedFrom.ClientID%>' || $element.attr('id') == '<%=dtpDateAssignedThru.ClientID%>' || $element.attr('id') == '<%=dtpNoticeSentFrom.ClientID%>' || $element.attr('id') == '<%=dtpNoticeSentThru.ClientID%>') {
                return true;
            }

            //if (eType == "radio") {
            //    if ($('input[type=radio][name=DeadlineFilter]:selected').length != 0) {
            //        //             alert('hii1');
            //        if ($element.attr('id') == "ctl33_ctl00_rdoLien" || $element.attr('id') == "ctl33_ctl00_rdoForeclosure" || $element.attr('id') == "ctl33_ctl00_rdoStopNotice" || $element.attr('id') == "ctl33_ctl00_rdoBond" || $element.attr('id') == "ctl33_ctl00_rdoBondSuit" || $element.attr('id') == "ctl33_ctl00_rdonotice" || $element.attr('id') == "ctl33_ctl00_rdoAll") {
            //            //         alert('hii');
            //            ISValid = true;
            //        }
            //        else {
            //            ISValid = false;
            //        }
            //    }
            //}
            if (eType == "text") {
                var textBox = $.trim($element.val());
                if (textBox == "") {
                    ISValid = false;

                }
                else {
                    ISValid = true;

                }
            }
            else if (eType == "checkbox") {
                if ($element.prop("checked") == false) {
                    ISValid = false;

                } else {
                    ISValid = true;

                }
            }

            // console.log(ISValid);
            if (ISValid == true) {
                return false;
            }
        });
        // console.log(ISValid);

       //old code start 02/05/20
       // if (ISValid == false) {
       //     $("#ctl33_ctl00_btnSubmit").prop("disabled", true);
       //     //  $("#ctl33_ctl00_btnViewAll").prop("disabled", true);

       // }
       // else {
       //     $("#ctl33_ctl00_btnSubmit").prop("disabled", false);
       //     //    $("#ctl33_ctl00_btnViewAll").prop("disabled", false);
       // }
        // old code end
    }

    function HideLoadingPanel() {
        //alert("hii");
        $(".TransparentGrayBackground").hide();

        $(".PageUpdateProgress").hide();
    }


</script>
<script runat="server">

    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV

    Protected WithEvents btnEditItem As LinkButton
    Dim dtBeforeSort As New System.Data.DataTable
    Dim dtAfterSort As New System.Data.DataTable
    Dim vwclients As HDS.DAL.COMMON.TableView
    Dim vwJobStatus As HDS.DAL.COMMON.TableView
    Dim vwDesk As HDS.DAL.COMMON.TableView
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Dim reportds As New System.Data.DataSet
    Dim reporttbl As HDS.DAL.COMMON.TableView
    Dim CheckSort As Int32 = 0

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
        FromBalance.Attributes.Add("onblur", "FromBalance_LostFocus();")
        ThruBalance.Attributes.Add("onblur", "ThruBalance_LostFocus();")
        If Me.Page.IsPostBack = False Then
            Me.MultiView1.SetActiveView(Me.View1)
            LoadClientList()
            LoadDropDown()
            'Me.FromBalance.Text=Strings.FormatCurrency("1")
            If qs.HasParameter("page") Then

                If (IsNothing(Session("filterOrder")) = False And IsNothing(Session("filterColumn")) = False) Then
                    drpSortType.Items.FindByValue(Session("filterOrder")).Selected = True
                    drpSortBy.Items.FindByValue(Session("filterColumn")).Selected = True
                End If

                If IsNothing(Session("pageIndex")) Then
                    GetJobsPageWise(GetFilter, 1)
                Else
                    GetJobsPageWise(GetFilter, Convert.ToInt32(Session("pageIndex")))
                End If
            End If
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindJobStatus", "BindJobStatus();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindCRFSStatus", "BindCRFSStatus();", True)
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindDesk", "BindDesk();", True)
        End If
        If hdnClientSelectionId.Value <> "" Then
            RadioButtonList1.Items.FindByValue("SELECTED").Selected = True
        End If
    End Sub
#Region "Bind List to Job Status Dropdown and Desk Dropdown"
    Private Sub LoadDropDown()
        Dim obj As CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users = CurrentUser.UserInfo
        Dim strquery As String = ""

        Dim vwJobStatusWithoutSort As HDS.DAL.COMMON.TableView
        vwJobStatusWithoutSort = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVStatusClientList(obj.ClientId)
        strquery = "(JOBVIEWSTATUS <> null or JOBVIEWSTATUS <>'') "
        vwJobStatusWithoutSort.RowFilter = strquery

        If vwJobStatusWithoutSort.Count > 0 Then
            Dim li As ListItem
            vwJobStatusWithoutSort.MoveFirst()
            vwJobStatusWithoutSort.Sort = "JobViewStatus"
            Do Until vwJobStatusWithoutSort.EOF
                li = New ListItem()
                li.Value = vwJobStatusWithoutSort.RowItem("JobviewStatus")
                li.Text = vwJobStatusWithoutSort.RowItem("FriendlyName")
                Me.DrpJobStatus.Items.Add(li)
                vwJobStatusWithoutSort.MoveNext()
            Loop
        End If

        vwDesk = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVDeskClientList(obj.ClientId)
        strquery = "DESKNUM <> null or DESKNUM <>''"
        vwDesk.RowFilter = strquery
        If vwDesk.Count > 0 Then
            vwDesk.MoveFirst()
            vwDesk.Sort = "DESKNUM"
            'Me.DrpDesk.Items.Clear()
            Dim li As ListItem
            Do Until vwDesk.EOF
                li = New ListItem()
                li.Value = vwDesk.RowItem("DESKNUM")
                li.Text = vwDesk.RowItem("FriendlyName")
                'Me.DrpDesk.Items.Add(li)
                vwDesk.MoveNext()
            Loop
        End If

        Dim MYSQL As String = "Select *, JobStatus + '-' + JobStatusDescr as FriendlyName FROM jobstatus "
        'MYSQL += "where JobStatus <> null"
        Dim vwCrfsStatus As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.GetTableView(MYSQL)
        If vwCrfsStatus.Count > 0 Then
            vwCrfsStatus.MoveFirst()
            vwCrfsStatus.Sort = "FriendlyName"
            Me.DrpCrfsStatus.Items.Clear()
            Dim li As ListItem
            Do Until vwCrfsStatus.EOF
                li = New ListItem()
                li.Value = vwCrfsStatus.RowItem("JobStatus")
                li.Text = vwCrfsStatus.RowItem("FriendlyName")
                Me.DrpCrfsStatus.Items.Add(li)

                vwCrfsStatus.MoveNext()
            Loop
        End If

    End Sub
#End Region

    Private Sub LoadClientList()

        vwclients = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientListForUser(Me.CurrentUser.Id)
        Me.panClients.Visible = False
        If vwclients.Count > 1 Then
            Dim li As ListItem
            Me.DropDownList1.Items.Clear()
            vwclients.MoveFirst()
            vwclients.Sort = "ClientName"

            Do Until vwclients.EOF
                li = New ListItem
                li.Value = vwclients.RowItem("ClientId")
                li.Text = vwclients.RowItem("ClientName")
                Me.DropDownList1.Items.Add(li)
                vwclients.MoveNext()
            Loop

            Me.panClients.Visible = True
            '^^^*** Add by pooja on 12/17/2020 ***^^^
            Session("mysproc") = mysproc
            '^^^***     ***^^^
            mysproc = GetFilter()
            If mysproc.ClientCode.Length > 4 Then
                Me.DropDownList1.SelectedValue = mysproc.ClientCode
                Me.hdnClientSelectionId.Value = mysproc.ClientCode
            End If


        End If

    End Sub

    Protected Function GetFilter() As Object
        'Dim sessionvalue As String
        'sessionvalue = Session("mysproc")
        If IsNothing(Session("mysproc")) = False Then
            Return Session("mysproc")
        Else
            Return New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV
        End If
    End Function


    Protected Sub btnRequiresReview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRequiresReview.Click
        If (Me.Calendar3.Text <> "" And Me.Calendar4.Text <> "") Then
            If (rdoLien.Checked = False And rdoBond.Checked = False And rdoStopNotice.Checked = False And rdoForeclosure.Checked = False And rdoBondSuit.Checked = False And rdonotice.Checked = False And rdoNOI.Checked = False) Then
                CustomValidator1.ErrorMessage = "Select DeadLine Type"
                CustomValidator1.IsValid = False
                Exit Sub
            Else
                CustomValidator1.ErrorMessage = ""
                CustomValidator1.IsValid = True

            End If
        End If

        With mysproc
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                    .ClientCode = Me.hdnClientSelectionId.Value
                End If
            End If

            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike_1.Checked Then
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If

                If Me.CustomerNameLike_2.Checked Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                End If
            End If
            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike_1.Checked Then
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If

                If Me.CustomerRefLike_2.Checked Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                End If
            End If

            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobName = Me.JobName.Text.Trim & "%"
                End If

                If Me.JobNameLike_2.Checked Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                End If
            End If
            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If

                If Me.JobNameLike_2.Checked Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                End If
            End If
            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike_1.Checked Then
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If

                If Me.JobAddressLike_2.Checked Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                End If
            End If
            If Me.JobCity.Text.Trim.Length > 0 Then
                If Me.JobCityLike_1.Checked Then
                    .JobCity = Me.JobCity.Text.Trim & "%"
                End If

                If Me.JobCityLike_2.Checked Then
                    .JobCity = "%" & Me.JobCity.Text.Trim & "%"
                End If
            End If
            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            End If
            If Me.OwnerName.Text.Trim.Length > 0 Then
                If Me.OwnerNameLike_1.Checked Then
                    .OwnerName = Me.OwnerName.Text.Trim & "%"
                End If

                If Me.OwnerNameLike_2.Checked Then
                    .OwnerName = "%" & Me.OwnerName.Text.Trim & "%"
                End If
            End If
            If Me.ContractorName.Text.Trim.Length > 0 Then
                If Me.ContractorNameLike_1.Checked Then
                    .GeneralContractor = Me.ContractorName.Text.Trim & "%"
                End If

                If Me.ContractorNameLike_2.Checked Then
                    .GeneralContractor = "%" & Me.ContractorName.Text.Trim & "%"
                End If
            End If
            If Me.hdnJobStatusSelectedId.Value.Trim.Length > 0 Then
                If Not Me.hdnJobStatusSelectedId.Value = "0" Then
                    .StatusCode = Me.hdnJobStatusSelectedId.Value.Trim & "%"
                End If
            End If
            If Me.hdnCrfsStatusSelectedId.Value.Trim.Length > 0 Then
                If Not Me.hdnCrfsStatusSelectedId.Value = "0" Then
                    .LVStatusCode = Me.hdnCrfsStatusSelectedId.Value.Trim & "%"
                End If
            End If


            ' Added 5/16/2016
            .FromDeadlineDate = Me.Calendar3.Text
            .ThruDeadlineDate = Me.Calendar4.Text
            If Me.chkByDeadlineDate.Checked Then
                .DeadlineRange = 1
            End If
            'added  5/11/2018
            .FromDateAssigned = Me.dtpDateAssignedFrom.Text
            .ThruDateAssigned = Me.dtpDateAssignedThru.Text
            If Me.chkDateAssigned.Checked Then
                .DateAssignedRange = 1
            End If

            .FromNoticeSent = Me.dtpNoticeSentFrom.Text
            .ThruNoticeSent = Me.dtpNoticeSentThru.Text
            If Me.chkNoticeSent.Checked Then
                .NoticeSentRange = 1
            End If


            If rdoLien.Checked = True Then
                .LienDeadline = 1
            End If
            If rdoBond.Checked = True Then
                .BondDeadline = 1
            End If
            If rdoStopNotice.Checked = True Then
                .SNDeadline = 1
            End If
            If rdoForeclosure.Checked = True Then
                .ForeclosureDeadline = 1
            End If
            If rdoBondSuit.Checked = True Then
                .BondsuitDeadline = 1
            End If
            If rdonotice.Checked = True Then
                .NoticeDeadline = 1
            End If
            If rdoNOI.Checked = True Then
                .NOIDeadline = 1
            End If

            If rdoLien.Checked = False And rdoBond.Checked = False And rdoStopNotice.Checked = False And rdoForeclosure.Checked = False And rdoBondSuit.Checked = False And rdonotice.Checked = False And rdoNOI.Checked = False Then
                .NoDeadLine = 1
            End If


            ' Added 5/17/2016
            .FromBalance = Utils.GetDecimal(Me.FromBalance.Value)
            .ThruBalance = Utils.GetDecimal(Me.ThruBalance.Value)
            If Me.chkByBalance.Checked Then
                .BalanceRange = 1
            End If

            If Me.txtFilterKey.Text.Trim.Length > 0 Then
                .FilterKey = Me.txtFilterKey.Text.Trim & "%"
            End If

            'If Me.hdnDeskSelectedId.Value.Trim.Length > 0 Then
            '    If Not Me.hdnDeskSelectedId.Value = "0" Then
            '        .DESKNUM = Me.hdnDeskSelectedId.Value.Trim & "%"
            '    End If
            'End If
            If Me.txtRA.Text.Trim.Length > 0 Then
                .RANum = Me.txtRA.Text.Trim & "%"
            End If
            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            End If
            If Me.txtCreditRating.Text.Trim.Length > 0 Then
                .CreditRating = Me.txtCreditRating.Text.Trim & "%"
            End If
            If Me.txtCRFS.Text.Trim.Length > 0 Then
                .JobId = Me.txtCRFS.Text.Trim
            End If

            'If chkAllJobs.Checked Then
            '    .ActiveOnly = False
            'Else
            '    .ActiveOnly = True
            'End If

            .ActiveOnly = False

            .ReviewStatus = True
            .NotifyStatus = True

            .UserId = Me.CurrentUser.Id

        End With
        Session("mysproc") = mysproc
        Session("pageIndex") = 1
        GetJobsPageWise(mysproc, 1)
        Me.MultiView1.SetActiveView(Me.View2)
        If gvJobViewList.Rows.Count > 0 Then
            Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
    End Sub
    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackFilter.Click
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "BindClientSelection", "BindClientSelection();", True)
        Me.MultiView1.SetActiveView(Me.View1)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "ClearControl", "ClearControl();", True)
        Me.CustomerName.Text = ""
        Me.CustomerNameLike_1.Checked = True
        Me.CustomerNameLike_2.Checked = False
        Me.CustomerRef.Text = ""
        Me.CustomerRefLike_1.Checked = True
        Me.CustomerRefLike_2.Checked = False
        Me.JobName.Text = ""
        Me.JobNameLike_1.Checked = True
        Me.JobNameLike_2.Checked = False
        Me.JobNo.Text = ""
        Me.JobNoLike_1.Checked = True
        Me.JobNoLike_2.Checked = False
        Me.JobAddress.Text = ""
        Me.JobAddressLike_1.Checked = True
        Me.JobAddressLike_2.Checked = False
        Me.JobCity.Text = ""
        Me.JobCityLike_1.Checked = True
        Me.JobCityLike_2.Checked = False
        Me.JobState.Text = ""
        Me.OwnerName.Text = ""
        Me.OwnerNameLike_1.Checked = True
        Me.OwnerNameLike_2.Checked = False
        Me.ContractorName.Text = ""
        Me.ContractorNameLike_1.Checked = True
        Me.ContractorNameLike_2.Checked = False
        Me.FromBalance.Text = "$0.00"
        Me.ThruBalance.Text = "$0.00"
        Me.chkByBalance.Checked = False
        Me.chkByDeadlineDate.Checked = False
        Me.chkDateAssigned.Checked = False
        Me.chkNoticeSent.Checked = False

        'Me.chkAllJobs.Checked = False 'Old Code for Archive Jobs
        Me.chkAllJobs.Checked = True

        Me.txtRA.Text = ""
        Me.BranchNo.Text = ""
        Me.txtCreditRating.Text = ""
        Me.txtCRFS.Text = ""
        Me.hdnJobStatusSelectedId.Value = ""
        Me.hdnCrfsStatusSelectedId.Value = ""
        'Me.hdnDeskSelectedId.Value = ""

        rdoBond.Checked = False
        rdoLien.Checked = False
        rdoStopNotice.Checked = False
        rdoBondSuit.Checked = False
        rdoForeclosure.Checked = False
        rdonotice.Checked = False
        rdoNOI.Checked = False

    End Sub

    'Protected Sub gvwList_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowCreated
    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
    '        AddHandler CType(e.Row.FindControl("btnEditItem"), LinkButton).Click, AddressOf btnEditItem_Click
    '    End If
    'End Sub
    Protected Sub btnEditItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditItem.Click
        btnEditItem = TryCast(sender, LinkButton)
        'Dim myitemid As String = btnEditItem.Attributes("rowno")
        Dim myitemid As String = btnEditItem.CommandArgument
        Dim s As String = "~/MainDefault.aspx?jobview.jobviewinfo&itemid=" & myitemid
        Response.Redirect(Me.Page.ResolveUrl(s), True)
    End Sub
    'Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvJobViewList.RowDataBound

    '    If e.Row.RowType = DataControlRowType.DataRow Then
    '        Dim DR As System.Data.DataRowView = e.Row.DataItem
    '        btnEditItem = TryCast(e.Row.FindControl("btnEditItem"), LinkButton)
    '        btnEditItem.Attributes("rowno") = DR("JobId")


    '    End If

    'End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If (Me.Calendar3.Text <> "" And Me.Calendar4.Text <> "") Then
            If (rdoLien.Checked = False And rdoBond.Checked = False And rdoStopNotice.Checked = False And rdoForeclosure.Checked = False And rdoBondSuit.Checked = False And rdonotice.Checked = False And rdoNOI.Checked = False) Then
                CustomValidator1.ErrorMessage = "Select DeadLine Type"
                CustomValidator1.IsValid = False
                Exit Sub
            Else
                CustomValidator1.ErrorMessage = ""
                CustomValidator1.IsValid = True

            End If
        End If

        With mysproc
            If Me.panClients.Visible = True Then
                If Me.RadioButtonList1.SelectedValue = "SELECTED" Then
                    .ClientCode = Me.DropDownList1.SelectedValue
                    .ClientCode = Me.hdnClientSelectionId.Value
                End If
            End If

            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike_1.Checked Then
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If

                If Me.CustomerNameLike_2.Checked Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                End If
            End If
            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike_1.Checked Then
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If

                If Me.CustomerRefLike_2.Checked Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                End If
            End If

            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobName = Me.JobName.Text.Trim & "%"
                End If

                If Me.JobNameLike_2.Checked Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                End If
            End If
            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If

                If Me.JobNameLike_2.Checked Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                End If
            End If
            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike_1.Checked Then
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If

                If Me.JobAddressLike_2.Checked Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                End If
            End If
            If Me.JobCity.Text.Trim.Length > 0 Then
                If Me.JobCityLike_1.Checked Then
                    .JobCity = Me.JobCity.Text.Trim & "%"
                End If

                If Me.JobCityLike_2.Checked Then
                    .JobCity = "%" & Me.JobCity.Text.Trim & "%"
                End If
            End If
            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            End If
            If Me.OwnerName.Text.Trim.Length > 0 Then
                If Me.OwnerNameLike_1.Checked Then
                    .OwnerName = Me.OwnerName.Text.Trim & "%"
                End If

                If Me.OwnerNameLike_2.Checked Then
                    .OwnerName = "%" & Me.OwnerName.Text.Trim & "%"
                End If
            End If
            If Me.ContractorName.Text.Trim.Length > 0 Then
                If Me.ContractorNameLike_1.Checked Then
                    .GeneralContractor = Me.ContractorName.Text.Trim & "%"
                End If

                If Me.ContractorNameLike_2.Checked Then
                    .GeneralContractor = "%" & Me.ContractorName.Text.Trim & "%"
                End If
            End If
            If Me.hdnJobStatusSelectedId.Value.Trim.Length > 0 Then
                If Not Me.hdnJobStatusSelectedId.Value = "0" Then
                    .StatusCode = Me.hdnJobStatusSelectedId.Value.Trim & "%"
                End If
            End If
            If Me.hdnCrfsStatusSelectedId.Value.Trim.Length > 0 Then
                If Not Me.hdnCrfsStatusSelectedId.Value = "0" Then
                    .LVStatusCode = Me.hdnCrfsStatusSelectedId.Value.Trim & "%"
                End If
            End If


            ' Added 5/16/2016
            .FromDeadlineDate = Me.Calendar3.Text
            .ThruDeadlineDate = Me.Calendar4.Text
            If Me.chkByDeadlineDate.Checked Then
                .DeadlineRange = 1
            End If
            'added  5/11/2018
            .FromDateAssigned = Me.dtpDateAssignedFrom.Text
            .ThruDateAssigned = Me.dtpDateAssignedThru.Text
            If Me.chkDateAssigned.Checked Then
                .DateAssignedRange = 1
            End If

            .FromNoticeSent = Me.dtpNoticeSentFrom.Text
            .ThruNoticeSent = Me.dtpNoticeSentThru.Text
            If Me.chkNoticeSent.Checked Then
                .NoticeSentRange = 1
            End If

            If rdoLien.Checked = True Then
                .LienDeadline = 1
            End If
            If rdoBond.Checked = True Then
                .BondDeadline = 1
            End If
            If rdoStopNotice.Checked = True Then
                .SNDeadline = 1
            End If
            If rdoForeclosure.Checked = True Then
                .ForeclosureDeadline = 1
            End If
            If rdoBondSuit.Checked = True Then
                .BondsuitDeadline = 1
            End If
            If rdonotice.Checked = True Then
                .NoticeDeadline = 1
            End If
            If rdoNOI.Checked = True Then
                .NOIDeadline = 1
            End If

            If rdoLien.Checked = False And rdoBond.Checked = False And rdoStopNotice.Checked = False And rdoForeclosure.Checked = False And rdoBondSuit.Checked = False And rdonotice.Checked = False And rdoNOI.Checked = False Then
                .NoDeadLine = 1
            End If


            ' Added 5/17/2016
            .FromBalance = Utils.GetDecimal(Me.FromBalance.Value)
            .ThruBalance = Utils.GetDecimal(Me.ThruBalance.Value)
            If Me.chkByBalance.Checked Then
                .BalanceRange = 1
            End If


            'If Me.hdnDeskSelectedId.Value.Trim.Length > 0 Then
            '    If Not Me.hdnDeskSelectedId.Value = "0" Then
            '        .DESKNUM = Me.hdnDeskSelectedId.Value.Trim & "%"
            '    End If
            'End If



            If Me.txtFilterKey.Text.Trim.Length > 0 Then
                .FilterKey = Me.txtFilterKey.Text.Trim & "%"
            End If



            If Me.txtRA.Text.Trim.Length > 0 Then
                .RANum = Me.txtRA.Text.Trim & "%"
            End If
            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            End If
            If Me.txtCreditRating.Text.Trim.Length > 0 Then
                .CreditRating = Me.txtCreditRating.Text.Trim & "%"
            End If
            If Me.txtCRFS.Text.Trim.Length > 0 Then
                .JobId = Me.txtCRFS.Text.Trim
            End If

            'If chkAllJobs.Checked Then
            '    .ActiveOnly = False
            'Else
            '    .ActiveOnly = True
            'End If

            .ActiveOnly = False

            .UserId = Me.CurrentUser.Id

        End With
        Session("mysproc") = mysproc
        Session("pageIndex") = 1
        GetJobsPageWise(mysproc, 1)
        Me.MultiView1.SetActiveView(Me.View2)
        If gvJobViewList.Rows.Count > 0 Then
            Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        '  ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc(1);", True)
    End Sub
    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        mysproc = Session("mysproc")
        GetJobsPageWise(mysproc, 1)
    End Sub
    Protected Sub drpSortType_SelectedIndexChanged(sender As Object, e As EventArgs)
        mysproc = Session("mysproc")
        GetJobsPageWise(mysproc, 1)
    End Sub



    Private Function GetSortedData(ByVal SortTypeValue As String, ByVal SortByValue As String, mysprocSort As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV)
        Dim SortType As String = "ASC"
        If SortTypeValue = "-1" Then
            SortType = "ASC"
        ElseIf SortTypeValue = "1" Then
            SortType = "DESC"
        End If

        Dim SortBy As String
        If SortByValue = "-1" Then
            SortBy = "ListId " + SortType
        ElseIf SortByValue = "1" Then
            SortBy = "JobId " + SortType
        ElseIf SortByValue = "2" Then
            SortBy = "CustRefNum " + SortType
        ElseIf SortByValue = "3" Then
            SortBy = "JobNum " + SortType
        ElseIf SortByValue = "4" Then
            SortBy = "JobName " + SortType
        ElseIf SortByValue = "5" Then
            SortBy = "JobAdd1 " + SortType
        ElseIf SortByValue = "6" Then
            SortBy = "CityStateZip " + SortType
        ElseIf SortByValue = "7" Then
            SortBy = "JobState " + SortType
        ElseIf SortByValue = "8" Then
            SortBy = "JobBalance " + SortType
        ElseIf SortByValue = "9" Then
            SortBy = "JVStatusCode " + SortType
        ElseIf SortByValue = "10" Then
            SortBy = "NoticeDeadlineDate " + SortType
        ElseIf SortByValue = "11" Then
            SortBy = "LienDeadlineDate " + SortType
        ElseIf SortByValue = "12" Then
            SortBy = "BondDeadlineDate " + SortType
        ElseIf SortByValue = "13" Then
            SortBy = "SNDeadlineDate " + SortType
        ElseIf SortByValue = "14" Then
            SortBy = "NOIDeadlineDate " + SortType
        ElseIf SortByValue = "15" Then
            SortBy = "BranchNum " + SortType
        ElseIf SortByValue = "16" Then
            SortBy = "FilterKey " + SortType
            '^^^*** Add by pooja on 01/28/2021 ***^^^
        ElseIf SortByValue = "17" Then
            SortBy = "ClientCustomer " + SortType
            '^^^***  ***^^^
        End If
        mysprocSort.SortColType = SortBy
        myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(mysprocSort)

        'If (SortType = "ASC") Then
        '    If (SortByValue = "10" Or SortByValue = "11" Or SortByValue = "12" Or SortByValue = "13" Or SortByValue = "14") Then
        '        If (SortByValue = "10") Then
        '            myviewSorting = SetNullDate("NoticeDeadlineDate", myviewSorting)
        '        ElseIf (SortByValue = "11") Then
        '            myviewSorting = SetNullDate("LienDeadlineDate", myviewSorting)
        '        ElseIf (SortByValue = "12") Then
        '            myviewSorting = SetNullDate("BondDeadlineDate", myviewSorting)
        '        ElseIf (SortByValue = "13") Then
        '            myviewSorting = SetNullDate("SNDeadlineDate", myviewSorting)
        '        ElseIf (SortByValue = "14") Then
        '            myviewSorting = SetNullDate("NOIDeadlineDate", myviewSorting)
        '        End If
        '    End If
        'End If

        If myviewSorting.Count < 1 Then
            Me.gvJobViewList.DataSource = Nothing
            Me.gvJobViewList.DataBind()


        End If
        Session("filterOrder") = SortTypeValue
        Session("filterColumn") = SortByValue
        Session("JobList") = mysprocSort
        Return myviewSorting
    End Function





    'Protected Function SetNullDate(ByVal DateField As String, ByVal viewcheckdate As HDS.DAL.COMMON.TableView)
    '    For Each dr As System.Data.DataRow In viewcheckdate.ToTable.Rows
    '        If (dr(DateField).ToString = "2079-01-01 00:00:00.000") Then
    '            dr(DateField) = DBNull.Value

    '        End If

    '    Next
    '    Return viewcheckdate
    'End Function

    'Protected Sub ThruBalance_TextChanged(sender As Object, e As EventArgs)
    '    If (ThruBalance.Text <> "") Then
    '        checkJobBalanceRange()
    '    End If

    'End Sub


    '10-25-2018 Commented for speed issue
    Protected Sub gvJobViewList_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            'Dim lbl As Label = TryCast(e.Row.FindControl("lblJobid"), Label)
            'If lbl.Text <> "" Then
            '    lbl.ForeColor=Drawing.Color.Blue

            'End If
            Dim lblNDLDate As Label = TryCast(e.Row.FindControl("lblNoticeDate"), Label)
            Dim lblLienDate As Label = TryCast(e.Row.FindControl("lblLienDate"), Label)
            Dim lblBondDate As Label = TryCast(e.Row.FindControl("lblBondDate"), Label)
            Dim lblSNDate As Label = TryCast(e.Row.FindControl("lblSNDate"), Label)
            Dim lblForeClose As Label = TryCast(e.Row.FindControl("lblForeClose"), Label)
            Dim lblSuit As Label = TryCast(e.Row.FindControl("lblSuit"), Label)
            Dim lblJobid As Label = TryCast(e.Row.FindControl("lblJobid"), Label)
            Dim lblNOI As Label = TryCast(e.Row.FindControl("lblNOIDeadline"), Label)

            Dim hdnNoticeSent As HiddenField = TryCast(e.Row.FindControl("hdnNoticeSent"), HiddenField)
            Dim hdnLienDate As HiddenField = TryCast(e.Row.FindControl("hdnLienDate"), HiddenField)
            Dim hdnBondDate As HiddenField = TryCast(e.Row.FindControl("hdnBondDate"), HiddenField)
            Dim hdnSNDate As HiddenField = TryCast(e.Row.FindControl("hdnSNDate"), HiddenField)
            Dim hdnForeclosureDate As HiddenField = TryCast(e.Row.FindControl("hdnForeclosureDate"), HiddenField)
            Dim hdnBondSuitDate As HiddenField = TryCast(e.Row.FindControl("hdnBondSuitDate"), HiddenField)
            Dim hdnPublicJob As HiddenField = TryCast(e.Row.FindControl("hdnPublicJob"), HiddenField)
            Dim hdndateassigned As HiddenField = TryCast(e.Row.FindControl("hdndateassigned"), HiddenField)


            If lblNDLDate.Text IsNot "" Then
                Dim NDLDate As Date = CType(lblNDLDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, NDLDate) < 10 And hdnNoticeSent.Value Is "" Then
                    lblNDLDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblLienDate.Text IsNot "" Then
                Dim LDLDate As Date = CType(lblLienDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, LDLDate) < 30 And hdnLienDate.Value Is "" Then
                    lblLienDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblBondDate.Text IsNot "" Then
                Dim BDLDate As Date = CType(lblBondDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, BDLDate) < 30 And hdnBondDate.Value Is "" Then
                    lblBondDate.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblSNDate.Text IsNot "" Then
                Dim SNDate As Date = CType(lblSNDate.Text, Date)
                If DateDiff(DateInterval.Day, Now, SNDate) < 30 And hdnSNDate.Value Is "" Then
                    lblSNDate.ForeColor = Drawing.Color.Red
                End If
            End If

            If lblForeClose.Text IsNot "" Then
                Dim ForeClose As Date = CType(lblForeClose.Text, Date)
                If DateDiff(DateInterval.Day, Now, ForeClose) < 30 And hdnForeclosureDate.Value Is "" Then
                    lblForeClose.ForeColor = Drawing.Color.Red
                End If
            End If

            If lblSuit.Text IsNot "" Then
                Dim Suit As Date = CType(lblSuit.Text, Date)
                If DateDiff(DateInterval.Day, Now, Suit) < 30 And hdnBondSuitDate.Value Is "" And hdnPublicJob.Value = True Then
                    lblSuit.ForeColor = Drawing.Color.Red
                End If
            End If
            If lblNOI.Text IsNot "" Then
                Dim NOI As Date = CType(lblNOI.Text, Date)
                If DateDiff(DateInterval.Day, Now, NOI) < 10 And hdnLienDate.Value Is "" Then
                    lblNOI.ForeColor = Drawing.Color.Red
                End If
            End If

            If hdndateassigned.Value IsNot "" Then
                lblJobid.ForeColor = Drawing.Color.Blue
            End If
        End If
    End Sub


    Protected Sub btnExportReport_Click(sender As Object, e As EventArgs)

        mysproc = Session("mysproc")
        mysproc.Page = 1
        If IsNothing(Session("TotalRecords")) = False Then
            mysproc.PageRecords = Session("TotalRecords")
        Else
            mysproc.PageRecords = 2500
        End If

        Dim sreport As String = (CRF.CLIENTVIEW.BLL.LienView.Provider.JobListExportJV(mysproc, Me.PDFReport.Checked))
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            If Me.SpreadSheet.Checked = False Then
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If

            Else

                Dim reportexcel As String = System.IO.Path.GetFileName(sreport)
                Session("reportexcel") = reportexcel

                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "HideLoadingPanel", "HideLoadingPanel()", True)

                Me.DownLoadReport(Session("reportexcel"))

            End If
        End If

        Dim pageURL1 As String = "MainDefault.aspx?jobview.jobview&page=1"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "redirectList", "window.location.href='" + pageURL1 + "'", True)

    End Sub
    Public Function GetPropertyName(ByVal prPublic As String, ByVal prPrivate As String, ByVal prResidential As String, ByVal prFederal As String)
        Dim type As String = ""
        If prPublic Then
            type = "Public"
        ElseIf prPrivate = True And prResidential = False Then
            type = "Private"
        ElseIf prPrivate = True And prResidential = True Then
            type = "Residential"
        ElseIf prFederal Then
            type = "Federal"

        End If
        Return type
    End Function

    Public Function GetStatusDescription(ByVal StatusCode As String)
        Dim StatusDesc As String = ""
        Try
            Dim MYSQL As String = "Select JobStatusDescr from jobstatus where Jobstatus='" + StatusCode + "'"
            Dim MYDT As System.Data.DataSet = CRF.CLIENTVIEW.BLL.ProviderBase.DAL.GetDataSet(MYSQL)
            If (MYDT.Tables(0).Rows.Count > 0) Then
                StatusDesc = MYDT.Tables(0).Rows(0)("JobStatusDescr")
            End If
        Catch ex As Exception
        End Try
        Return StatusDesc
    End Function

    Public Function GetJobStatusDescription(ByVal StatusCode As String)
        Dim StatusDesc As String = ""
        Try
            Dim MYSQL As String = "Select Description from jobviewstatus where jobviewstatus='" + StatusCode + "'"
            Dim MYDT As System.Data.DataSet = CRF.CLIENTVIEW.BLL.ProviderBase.DAL.GetDataSet(MYSQL)
            If (MYDT.Tables(0).Rows.Count > 0) Then
                StatusDesc = MYDT.Tables(0).Rows(0)("Description")
            End If
        Catch ex As Exception
        End Try
        Return StatusDesc
    End Function


    'Custom Pagging for job list
    Protected Sub PageSize_Changed(ByVal sender As Object, ByVal e As EventArgs)
        mysproc = Session("mysproc")
        Session("pageIndex") = 1
        Me.GetJobsPageWise(mysproc, 1)
    End Sub

    Protected Sub Page_Changed(ByVal sender As Object, ByVal e As EventArgs)
        Dim pageIndex As Integer = Integer.Parse(CType(sender, LinkButton).CommandArgument)
        Session("pageIndex") = pageIndex
        mysproc = Session("mysproc")
        Me.GetJobsPageWise(mysproc, pageIndex)
        'End If
    End Sub

    Private Sub PopulatePager(ByVal recordCount As Integer, ByVal currentPage As Integer)
        Dim dblPageCount As Double = CType((CType(recordCount, Decimal) / Decimal.Parse(ddlPageSize.SelectedValue)), Double)
        Dim pageCount As Integer = CType(Math.Ceiling(dblPageCount), Integer)
        Dim pages As New List(Of ListItem)

        If (pageCount > 0) Then

            Dim showMax As Integer = 5
            Dim startPage As Integer
            Dim endPage As Integer
            If (pageCount <= showMax) Then

                startPage = 1
                endPage = pageCount

            Else

                startPage = currentPage
                If (pageCount - currentPage <= showMax - 1) Then
                    endPage = pageCount
                Else

                    endPage = currentPage + showMax - 1
                End If


            End If

            pages.Add(New ListItem("<<", "1", (currentPage > 1)))
            pages.Add(New ListItem("<", currentPage - 1.ToString, (currentPage > 1)))
            For i As Integer = startPage To endPage

                pages.Add(New ListItem(i.ToString, i.ToString, (i <> currentPage)))
            Next
            pages.Add(New ListItem(">", currentPage + 1.ToString, (currentPage < pageCount)))
            pages.Add(New ListItem(">>", pageCount.ToString, (currentPage < pageCount)))

        End If
        rptPager.DataSource = pages
        rptPager.DataBind()
        Dim recordsperpage As Integer = CType(ddlPageSize.SelectedValue, Integer)
        Dim FromRecord As Integer = ((recordsperpage * currentPage) - recordsperpage) + 1
        Dim ToRecord As Integer
        If currentPage = pageCount Then
            ToRecord = recordCount
        Else
            ToRecord = recordsperpage * currentPage
        End If

        Dim info As String = "Showing " & FromRecord & " to " & ToRecord & " of " & recordCount & " entries"
        lblTableInfo.Text = info
    End Sub
    Private Sub GetJobsPageWise(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobListJV, ByVal pageIndex As Integer)
        asproc.Page = pageIndex
        asproc.PageRecords = Integer.Parse(ddlPageSize.SelectedValue)
        'If (IsSort) Then

        myview = GetSortedData(drpSortType.SelectedItem.Value, drpSortBy.SelectedItem.Value, asproc)
        'Else
        '    myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
        'End If

        If myview.Count < 1 Then
            Me.gvJobViewList.DataSource = Nothing
            Me.gvJobViewList.DataBind()

            Exit Sub
        End If

        Dim totrecs As String = myview.RowItem("totalrecords")
        Session("TotalRecords") = totrecs
        Me.gvJobViewList.DataSource = myview
        Me.gvJobViewList.DataBind()
        If gvJobViewList.Rows.Count > 0 Then
            Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        'Me.gvJobViewList.HeaderRow.TableSection = TableRowSection.TableHeader
        Me.MultiView1.SetActiveView(Me.View2)

        Me.PopulatePager(totrecs, pageIndex)
    End Sub


</script>

<asp:UpdatePanel ID="UpdatePanel1" runat="server" >
    <ContentTemplate>
        <div id="modcontainer" style="width: 100%;" class="margin-bottom-0">

            <asp:MultiView ID="MultiView1" runat="server">

                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Job Filter</h1>
                    <div class="body" id="JobFilterBody">
                        <div class="form-horizontal">
                            <asp:Panel ID="panClients" runat="server">
                                <div class="form-group">
                                    <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Client Selection</label>
                                    <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left; padding-left: 7px;">
                                        <asp:DropDownList ID="DropDownList1" runat="server" Style="display: none;">
                                        </asp:DropDownList>&nbsp;
                                        <asp:HiddenField ID="hdnClientSelectionId" runat="server" />
                                        <div class="btn-group" style="text-align: left;" align="left">
                                            <button type="button" class="btn btn-default dropdown-toggle" style="width: 300px; text-align: left;" align="left"
                                                data-toggle="dropdown" aria-expanded="false" id="btnClientSelectionType" tabindex="1">
                                                --Select Client Selection--
                     
                                                                                    <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu animate" aria-labelledby="exampleAnimationDropdown1" role="menu" id="UlClientSelectionType" style="overflow-y: auto; height: 150px;">
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                        <div class="radio-custom radio-default radio-inline">
                                            <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="redcaption" RepeatDirection="Horizontal"
                                                RepeatLayout="Flow" meta:resourcekey="CustomerNameLikeResource1">
                                                <asp:ListItem Selected="True" meta:resourcekey="ListItemResource1" Value="ALL" style="white-space: nowrap;"> All Clients</asp:ListItem>
                                                <asp:ListItem meta:resourcekey="ListItemResource2" Value="SELECTED" style="white-space: nowrap; padding-left: 40px;">Selected Only</asp:ListItem>
                                            </asp:RadioButtonList>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <div class="form-group">
                                <label class="col-md-3  col-sm-3 col-xs-3 control-label align-lable">Customer Name:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="CustomerName" TabIndex="1" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="CustomerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerNameLike(1)" TabIndex="2" />
                                        <asp:RadioButton ID="CustomerNameLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerNameLike(2)" TabIndex="3" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  col-sm-3 col-xs-3 control-label align-lable">Customer Ref:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="CustomerRef" TabIndex="4" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="CustomerRefLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerRefLike(1)" TabIndex="5" />
                                        <asp:RadioButton ID="CustomerRefLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerRefLike(2)" TabIndex="6" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  col-sm-3 col-xs-3 control-label align-lable">Owner Name:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="OwnerName" CssClass="form-control" TabIndex="7" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="OwnerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionOwnerNameLike(1)" TabIndex="8" />
                                        <asp:RadioButton ID="OwnerNameLike_2" runat="server" Text="Includes" onchange="myFunctionOwnerNameLike(2)" TabIndex="9" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3  col-sm-3 col-xs-3 control-label align-lable">Contractor Name:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="ContractorName" CssClass="form-control" TabIndex="10" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="ContractorNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionContractorNameLike(1)" TabIndex="11" />
                                        <asp:RadioButton ID="ContractorNameLike_2" runat="server" Text="Includes" onchange="myFunctionContractorNameLike(2)" TabIndex="12" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Name:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobName" CssClass="form-control" MaxLength="45" TabIndex="13" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="JobNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNameLike(1)" TabIndex="14" />
                                        <asp:RadioButton ID="JobNameLike_2" runat="server" Text="Includes" onchange="myFunctionJobNameLike(2)" TabIndex="15" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job #:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobNo" CssClass="form-control" MaxLength="45" TabIndex="16" />
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="JobNoLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNoLike(1)" TabIndex="17" />
                                        <asp:RadioButton ID="JobNoLike_2" runat="server" Text="Includes" onchange="myFunctionJobNoLike(2)" TabIndex="18" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Address:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobAddress" CssClass="form-control" MaxLength="45" TabIndex="19" />
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="JobAddressLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobAddressLike(1)" TabIndex="20" />
                                        <asp:RadioButton ID="JobAddressLike_2" runat="server" Text="Includes" onchange="myFunctionJobAddressLike(2)" TabIndex="21" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job City:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5">
                                    <asp:TextBox runat="server" ID="JobCity" CssClass="form-control" MaxLength="20" TabIndex="22" />
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="JobCityLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobCityLike(17)" TabIndex="23" />
                                        <asp:RadioButton ID="JobCityLike_2" runat="server" Text="Includes" onchange="myFunctionJobCityLike(18)" TabIndex="24" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job State:</label>
                                <div class="col-md-2  col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="JobState" CssClass="form-control" Width="70px" MaxLength="2" TabIndex="25" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Status:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5" style="text-align: left;">
                                    <div id="UlJobStatus">
                                    </div>
                                    <asp:DropDownList ID="DrpJobStatus" runat="server" Style="display: none;" TabIndex="26">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnJobStatusSelectedId" runat="server" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">CRFS Status:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5" style="text-align: left;">
                                    <div id="UlCrfsStatus">
                                    </div>
                                    <asp:DropDownList ID="DrpCrfsStatus" runat="server" Style="display: none;" TabIndex="27">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnCrfsStatusSelectedId" runat="server" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Job Balance:</label>
                                <div class="col-md-5 col-sm-5 col-xs-5" style="display: flex;">
                                    <%--   <asp:TextBox runat="server" ID="FromBalance" DataType="Money"
                                        CssClass="form-control " Width="96px"
                                        MaxLength="15" TabIndex="30" />--%>
                                    <cc1:DataEntryBox ID="FromBalance" runat="server" CssClass="form-control" DataType="Money"
                                        EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                        FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                        Value="0.00" Width="120px" TabIndex="28" AutoPostBack="false"></cc1:DataEntryBox>
                                    &nbsp; &nbsp;&nbsp;Thru&nbsp;
                                       <cc1:DataEntryBox ID="ThruBalance" runat="server" CssClass="form-control" DataType="Money"
                                           EnableClientSideScript="False" ErrorMessage="" FormatMask="" FormatString="{0:d3}"
                                           FriendlyName="" IsValid="True" ValidationExpression="^\$?\-?([1-9]{1}[0-9]{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\-?\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))$|^\(\$?([1-9]{1}\d{0,2}(\,\d{3})*(\.\d{0,2})?|[1-9]{1}\d{0,}(\.\d{0,2})?|0(\.\d{0,2})?|(\.\d{1,2}))\)$"
                                           Value="0.00" Width="120px" TabIndex="29" AutoPostBack="false"></cc1:DataEntryBox>
                                    <%--  <asp:TextBox runat="server" ID="ThruBalance" CssClass="form-control "
                                                                          Width="97px" MaxLength="15" TabIndex="31" />--%>
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByBalance" CssClass="redcaption"
                                            runat="server" Text=" Use Job Balance Range"
                                            Width="224px" TabIndex="30" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Notice Request Date :</label>
                                <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <asp:TextBox ID="dtpDateAssignedFrom" runat="server" Width="120px" onblur="CheckNoticerequestRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="31"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="dtpDateAssignedFrom" ErrorMessage=""></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;Thru&nbsp;

                                      <asp:TextBox ID="dtpDateAssignedThru" runat="server" Width="120px" onblur="CheckNoticerequestRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="32"></asp:TextBox>&nbsp;
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="dtpDateAssignedThru" ErrorMessage=""></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkDateAssigned" CssClass="redcaption"
                                            runat="server" Text=" Use Notice Request Range "
                                            Width="224px" TabIndex="33" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Notice Sent Date:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <asp:TextBox ID="dtpNoticeSentFrom" runat="server" Width="120px" onblur="CheckNoticesentRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="34"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="dtpNoticeSentFrom" ErrorMessage=""></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;Thru&nbsp;

                                      <asp:TextBox ID="dtpNoticeSentThru" runat="server" Width="120px" onblur="CheckNoticesentRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="35"></asp:TextBox>&nbsp;
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="dtpNoticeSentThru" ErrorMessage=""></asp:RequiredFieldValidator>
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkNoticeSent" CssClass="redcaption"
                                            runat="server" Text=" Use Notice Sent Range"
                                            Width="224px" TabIndex="36" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Deadline Date:</label>
                                <div class="col-md-5  col-sm-5 col-xs-5" style="display: flex;">
                                    <asp:TextBox ID="Calendar3" runat="server" Width="120px" onblur="CheckDeadlineDateRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="37"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="reqq1" runat="server" ControlToValidate="Calendar3" ErrorMessage=""></asp:RequiredFieldValidator>&nbsp;&nbsp;&nbsp;Thru&nbsp;

                                      <asp:TextBox ID="Calendar4" runat="server" Width="120px" onblur="CheckDeadlineDateRange()" CssClass="form-control datepicker" IsReadOnly="false" TabIndex="38"></asp:TextBox>&nbsp;
                                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Calendar4" ErrorMessage=""></asp:RequiredFieldValidator>

                                    <%--    <cc1:DataEntryBox ID="Calendar3" runat="server" DataType="datevalue" CssClass="form-control datepicker" Width="120px" Enabled="true" ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" TabIndex="27" />&nbsp;
                                    &nbsp;&nbsp;Thru&nbsp;
                                    <cc1:DataEntryBox ID="Calendar4" runat="server" DataType="datevalue" CssClass="form-control datepicker" Width="120px" Enabled="true" ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" TabIndex="28" />&nbsp;--%>
                                </div>
                                <div class="col-md-4  col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByDeadlineDate" CssClass="redcaption"
                                            runat="server" Text=" Use Deadline Date Range"
                                            Width="224px" TabIndex="39" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Deadline Type:</label>
                                <div class="col-md-7 col-sm-7 col-xs-7">
                                    <div class="example-wrap" style="margin-bottom: 0;">
                                        <div class="checkbox-custom checkbox-default" style="float: left; width: 100px;">
                                            <asp:CheckBox ID="rdoLien" runat="server"
                                                Text="Lien" TabIndex="40" />
                                        </div>
                                        <div class="checkbox-custom checkbox-default" style="float: left; width: 150px;">
                                            <asp:CheckBox ID="rdoForeclosure" runat="server"
                                                Text="Foreclosure" TabIndex="41" />
                                        </div>

                                        <div class="checkbox-custom checkbox-default" style="float: left; width: 150px;">
                                            <asp:CheckBox ID="rdoStopNotice" runat="server" Text="Stop Notice" CssClass="Widthlable"
                                                TabIndex="42" />
                                        </div>
                                        <div class="checkbox-custom checkbox-default">
                                            <asp:CheckBox ID="rdoNOI" runat="server" Text="NOI"
                                                TabIndex="43" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3 col-sm-3 col-xs-3"></div>
                                <div class="col-md-7 col-sm-7 col-xs-7" style="padding-bottom: 10px">
                                    <div class="example-wrap" style="margin-bottom: 0;">
                                        <div class="checkbox-custom checkbox-default" style="float: left; width: 100px;">
                                            <asp:CheckBox ID="rdoBond" runat="server" Text="Bond"
                                                TabIndex="44" />
                                        </div>
                                        <div class="checkbox-custom checkbox-default" style="float: left; width: 150px;">
                                            <asp:CheckBox ID="rdoBondSuit" runat="server"
                                                Text="Bond Suit" TabIndex="45" />
                                        </div>

                                        <div class="checkbox-custom checkbox-default" style="float: left; width: 150px;">
                                            <asp:CheckBox ID="rdonotice" runat="server" Text="Notice"
                                                TabIndex="46" />
                                        </div>

                                    </div>
                                </div>
                            </div>

                           <%-- <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable ">Desk:</label>
                                <div class="col-md-3 col-sm-3 col-xs-3" style="text-align: left;">
                                    <div id="UlDesk">
                                    </div>
                                    <asp:DropDownList ID="DrpDesk" runat="server" Style="display: none;" TabIndex="47">
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnDeskSelectedId" runat="server" />
                                </div>
                            </div>--%>

                             <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable ">Filter Key:</label>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="txtFilterKey" CssClass="form-control" TabIndex="48" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable ">RA #:</label>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="txtRA" CssClass="form-control" TabIndex="48" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Branch #:</label>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="BranchNo" CssClass="form-control" MaxLength="10" TabIndex="49" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">Credit Rating:</label>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="txtCreditRating" CssClass="form-control" TabIndex="50" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable">CRFS #:</label>
                                <div class="col-md-2 col-sm-2 col-xs-2">
                                    <asp:TextBox runat="server" ID="txtCRFS" CssClass="form-control" TabIndex="51" />
                                </div>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable"></label>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default">
                                        <asp:CheckBox ID="chkAllJobs" CssClass="redcaption" runat="server" Text="Include Archived Jobs" TabIndex="52" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-12">
                                    <cc1:CustomValidator ID="CustomValidator1" Display="Static" runat="server"></cc1:CustomValidator>
                                    <asp:Label ID="Message1" runat="server" CssClass="normalred"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="footer" style="width: 100%">
                            <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">


                                <asp:Button ID="btnSubmit" runat="server" Text="Search Jobs" CssClass="btn btn-primary pull-right" CausesValidation="False" Style="margin-top: 5px;" TabIndex="53" />
                                <asp:Button ID="btnRequiresReview" runat="server" Text="Requires Review" CssClass="btn btn-primary pull-left" CausesValidation="False" Style="margin-top: 5px;" TabIndex="54" />
                                <%--  <asp:Button ID="btnViewAll" runat="server" Text="Search All Jobs" CssClass="btn btn-primary" CausesValidation="False" Style="margin-top: 5px;" TabIndex="47" />--%>
                                <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>&nbsp;
                            </asp:Panel>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <h1 class="panelheader">Job List</h1>
                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-md-12">
                                    <div style="width: auto; height: auto; overflow: auto;">
                                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">

                                <div class="col-md-12" style="text-align: left;">
                                    <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter" CausesValidation="False"></asp:LinkButton>
                                    <asp:LinkButton ID="btnExport" runat="server" CssClass="btn btn-primary" Text="Export" CausesValidation="False" OnClientClick="return modalshow();"></asp:LinkButton>
                                    <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="CRFS #" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Cust #" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Job #" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Job Name" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Job Address" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Job City" Value="6"></asp:ListItem>
                                        <asp:ListItem Text="Job State" Value="7"></asp:ListItem>
                                        <asp:ListItem Text="Job Balance" Value="8"></asp:ListItem>
                                        <asp:ListItem Text="Job Status" Value="9"></asp:ListItem>
                                        <asp:ListItem Text="Notice Deadline Date" Value="10"></asp:ListItem>
                                        <asp:ListItem Text="Lien Deadline Date" Value="11"></asp:ListItem>
                                        <asp:ListItem Text="Bond Deadline Date" Value="12"></asp:ListItem>
                                        <asp:ListItem Text="SN Deadline Date" Value="13"></asp:ListItem>
                                        <asp:ListItem Text="NOI Deadline Date" Value="14"></asp:ListItem>
                                        <asp:ListItem Text="Branch #" Value="15"></asp:ListItem>
                                        <asp:ListItem Text="Filter Key" Value="16"></asp:ListItem>
                                        <asp:ListItem Text="Customer Name" Value="17"></asp:ListItem>
                                        <%--'##101--%>
                                    </asp:DropDownList>
                                    <%--##101--%>
                                    <asp:DropDownList ID="drpSortType" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortType_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="Ascending" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Descending" Value="1"></asp:ListItem>
                                    </asp:DropDownList>

                                </div>
                            </div>
                        </div>
                        <div style="width: auto; height: auto; overflow: auto;">
                           <%-- '10-25-2018 Commented for speed issue--%>
                            <%--<asp:GridView ID="gvJobViewList" OnRowDataBound="gvJobViewList_RowDataBound" runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 12px !important;">--%>
                            <asp:GridView ID="gvJobViewList" runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 12px !important;">
                                <Columns>

                                    <asp:TemplateField HeaderText="">
                                        <HeaderTemplate>
                                            <table style="width: 100%; table-layout: fixed; font-weight: 500; color: black!important;" rules="cols">
                                                <tr>
                                                    <td style="width: 35px;"></td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">CRFS #</td>
                                                    <td style="width: 25%;" class="nowordwraptd">Job Information</td>
                                                    <%--<td style="width: 18%; word-break: break-all;">Job Address</td>--%>
                                                    <td style="width: 23%; max-width: 18%;" class="nowordwraptd">Job #</td>
                                                      <%--<td style="width: 75px; text-align: center;" class="nowordwraptd">Job Desk</td>--%>
                                                    <td style="width: 95px;" class="nowordwraptd">Job Balance</td>
                                                    <td style="width: 95px;" class="nowordwraptd">Filter Key</td>
                                                    <td colspan="3" style="width: 393px;"></td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">View</td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">Cust #</td>
                                                    <td style="width: 25%;" class="nowordwraptd">Customer Name</td>
                                                    <td style="width: 23%;max-width: 18%;" class="nowordwraptd">Job Status</td>
                                                    
                                                     <td style="width: 95px;" class="nowordwraptd">Property Type</td>
                                                    <td style="width: 95px;"  class="nowordwraptd">Branch #</td>
                                                    <td colspan="3" style="width: 393px" class="nowordwraptd">Deadline Dates</td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <HeaderStyle CssClass="RemovePadding" />
                                        <ItemStyle CssClass="RemovePadding" />
                                        <ItemTemplate>
                                            <table id="TableJobViewList" style="width: 100%; font-size: 14px; table-layout: fixed;" rules="cols">
                                                <tr>
                                                    <td style="width: 35px; text-align: center;">
                                                        <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server" CommandArgument='<%# Eval("JobId")%>' OnClick="btnEditItem_Click" /></td>
                                                    <td style="width: 71px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobid" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("JobIDColor").ToString()) %>' Text='<%# Eval("JobId")%>'></asp:Label></td>
                
                                                    <td style="width: 25%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobName" runat="server" Text='<%# Eval("JobName")%>'></asp:Label></td>
                                                                                        
                                                    <td style="width: 23%; max-width: 18%;" class="nowordwraptd">
                                                        <asp:Label ID="lblJobNum" runat="server" Text='<%# Eval("JobNum")%>'></asp:Label>

                                                    </td>
                                                    <%-- <td style="width: 78px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("FilterKey")%>'></asp:Label></td>--%>


                                                    <%--<td style="width: 75px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("JVDeskNum")%>'></asp:Label></td>--%>
                                                    <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <%--    <asp:Label ID="lblJobBalance" runat="server"
                                                            Text='<%# Eval("JobBalance", "{0:c}" %>'></asp:Label>                                                   
                                                        --%>

                                                        <asp:Label ID="lblJobBalance" runat="server"
                                                            Text='<%# If(Eval("JobId").ToString() = Eval("ParentJobId").ToString() And Eval("MergedJobBalance").ToString() <> "0.00", Eval("MergedJobBalance", "{0:c}"), Eval("JobBalance", "{0:c}"))%>'></asp:Label>


                                                    </td>
                                                     <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblStatus1" runat="server" Text='<%# Eval("FilterKey")%>'></asp:Label></td>

                                                    <td style="width: 135px;" class="nowordwraptd"><b style="font-weight: bold;">Notice - </b>
                                                        <asp:Label ID="lblNoticeDate" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("NoticeDeadLineDateColor").ToString()) %>' Text='<%#Utils.FormatDate(Eval("NoticeDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnNoticeSent" runat="Server" Value='<%#Eval("NoticeSent")%>' />
                                                    </td>
                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">Bond - </b>
                                                        <asp:Label ID="lblBondDate" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("BondDeadlineDateColor").ToString()) %>' Text='<%#Utils.FormatDate(Eval("BondDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnBondDate" runat="Server" Value='<%#Eval("BondDate")%>' />
                                                    </td>
                                                    <td style="width: 120px;" class="nowordwraptd"><b style="font-weight: bold;">SN - </b>
                                                        <asp:Label ID="lblSNDate" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("SNDeadlineDateColor").ToString()) %>' Text='<%#Utils.FormatDate(Eval("SNDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnSNDate" runat="Server" Value='<%#Eval("SNDate")%>' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td style="text-align: center; width: 71px" class="nowordwraptd">
                                                        <asp:Label ID="lblCustRefNum" runat="server" Text='<%# Eval("CustRefNum")%>'></asp:Label></td>
                                                   
                                                    <td style="width: 25%;" class="nowordwraptd">
                                                        <asp:Label ID="lblCity" runat="server" Text='<%# Eval("CityStateZip")%>'></asp:Label></td>
                                                  <td style="width: 23%; max-width: 18%;" class="nowordwraptd">
                                                        <%--<asp:Label ID="lblStatus" runat="server" Text='<%# String.Format("{0} - {1}", Eval("JVStatusCode"), GetJobStatusDescription(Eval("JVStatusCode").ToString())) %>'></asp:Label>--%>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("JVStatusDesc") %>'></asp:Label>
                                                    </td>
                                                    
                                                    
                                                    
                                                      
                                                   
                                                       <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <%--    <asp:Label ID="lblJobBalance" runat="server"
                                                            Text='<%# Eval("JobBalance", "{0:c}" %>'></asp:Label>                                                   
                                                        --%>

                                                     <asp:Label ID="lblPropertyType" runat="server" Text='<%# GetPropertyName(DataBinder.GetPropertyValue(Container.DataItem, "PublicJob"), DataBinder.GetPropertyValue(Container.DataItem, "PrivateJob"), DataBinder.GetPropertyValue(Container.DataItem, "ResidentialBox"), DataBinder.GetPropertyValue(Container.DataItem, "FederalJob")) %>'></asp:Label>


                                                    </td>
                                                     <td style="width: 95px; text-align: center;" class="nowordwraptd">
                                                        <asp:Label ID="lblBranchNum" runat="server" Text='<%# Eval("BranchNum")%>'></asp:Label></td>
                                                   
                                                   <%-- <td style="word-break: break-all;"></td>--%>
                                                    <td class="nowordwraptd"><b style="font-weight: bold;">Lien - </b>
                                                        <asp:Label ID="lblLienDate" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("LienDeadLineDateColor").ToString()) %>' Text='<%#Utils.FormatDate(Eval("LienDeadlineDate"))%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnLienDate" runat="Server" Value='<%#Eval("LienDate")%>' />
                                                    </td>
                                                    <td class="nowordwraptd"><b style="font-weight: bold;">Suit - </b>
                                                        <asp:Label ID="lblSuit" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("BondSuitDeadlineDateColor").ToString()) %>' Text='<%#Utils.FormatDate(Eval("BondSuitDeadlineDate")) %>'></asp:Label></td>
                                                    <asp:HiddenField ID="hdnBondSuitDate" runat="server" Value='<%#Utils.FormatDate(Eval("BondSuitDate")) %>' />
                                                    <asp:HiddenField ID="hdnPublicJob" runat="server" Value='<%# Eval("PublicJob")%>' />

                                                    <td class="nowordwraptd"><b style="font-weight: bold;">NOI - </b>

                                                        <asp:Label ID="lblNOIDeadline" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("NOIDeadlineDateColor").ToString()) %>' Text='<%# Utils.FormatDate(Eval("NOIDeadlineDate"))%>'></asp:Label></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                   
                                                    <td style="width: 25%;" class="nowordwraptd">
                                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%# Eval("ClientCustomer")%>'></asp:Label>

                                                    </td>
                                                    <%--<td class="nowordwraptd">
                                                        <asp:Label ID="lbl" runat="server" Text='<%# GetPropertyName(DataBinder.GetPropertyValue(Container.DataItem, "PublicJob"), DataBinder.GetPropertyValue(Container.DataItem, "PrivateJob"), DataBinder.GetPropertyValue(Container.DataItem, "ResidentialBox"), DataBinder.GetPropertyValue(Container.DataItem, "FederalJob")) %>'></asp:Label></td>
                                                 --%>   
                                                   <td style="width: 23%; max-width: 18%;" class="nowordwraptd" style="color: blue">
                                                        <%--<asp:Label ID="lblCRFSstatus" runat="server" Text='<%# String.Format("{0} - {1}", Eval("StatusCode"), GetStatusDescription(Eval("StatusCode").ToString())) %>'></asp:Label>--%>                                                        
                                                        <asp:Label ID="lblCRFSstatus" runat="server" Text='<%# Eval("JobStatusDescr") %>'></asp:Label>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="nowordwraptd"><b style="font-weight: bold;">Foreclose - </b>
                                                        <asp:Label ID="lblForeClose" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("ForeclosureDeadlineDateColor").ToString()) %>' Text='<%#Utils.FormatDate(Eval("ForeclosureDeadlineDate"))%>'></asp:Label>

                                                        <asp:HiddenField ID="hdnForeclosureDate" runat="Server" Value='<%#Eval("ForeclosureDate")%>' />
                                                        <asp:HiddenField ID="hdndateassigned" runat="server" Value='<%#Utils.FormatDate(Eval("DateAssigned")) %>' />
                                                    </td>
                                                    <td class="nowordwraptd"></td>
                                                    <td></td>
                                                </tr>
                                                 <tr>
                                                    <td></td>
                                                    <td></td>
                                                   <td></td>
                                                    <%--<td class="nowordwraptd">
                                                        <asp:Label ID="lbl" runat="server" Text='<%# GetPropertyName(DataBinder.GetPropertyValue(Container.DataItem, "PublicJob"), DataBinder.GetPropertyValue(Container.DataItem, "PrivateJob"), DataBinder.GetPropertyValue(Container.DataItem, "ResidentialBox"), DataBinder.GetPropertyValue(Container.DataItem, "FederalJob")) %>'></asp:Label></td>
                                                 --%>   <td></td>
                                                    <td></td>
                                                    <td></td>
                                                  <%--  <td class="nowordwraptd"><b style="font-weight: bold;">Foreclose - </b>
                                                        <asp:Label ID="Label2" runat="server" ForeColor='<%# System.Drawing.Color.FromName(Eval("ForeclosureDeadlineDateColor").ToString()) %>' Text='<%#Utils.FormatDate(Eval("ForeclosureDeadlineDate"))%>'></asp:Label>

                                                        <asp:HiddenField ID="HiddenField1" runat="Server" Value='<%#Eval("ForeclosureDate")%>' />
                                                        <asp:HiddenField ID="HiddenField2" runat="server" Value='<%#Utils.FormatDate(Eval("DateAssigned")) %>' />
                                                    </td>--%>
                                                    <td class="nowordwraptd"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                            </asp:GridView>
                            <br />
                            <div class="col-md-12 col-sm-12 col-xs-12" style="padding-right: 0px">
                                <div class="col-md-8 col-sm-8 col-xs-8" style="text-align: left">
                                    <asp:Label ID="lblTableInfo" runat="server" Style="font-size: 14px"></asp:Label>
                                    <asp:DropDownList ID="ddlPageSize" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageSize_Changed">
                                        <asp:ListItem Text="10" Value="10" />
                                        <asp:ListItem Text="25" Value="25" />
                                        <asp:ListItem Text="50" Value="50" />
                                    </asp:DropDownList>
                                </div>
                                <div style="padding-right: 0px" class="col-md-4 col-sm-4 col-xs-4">
                                    <ul class="pagination pull-right">
                                        <asp:Repeater ID="rptPager" runat="server">
                                            <ItemTemplate>
                                                <li class="page-item">
                                                    <asp:LinkButton ID="lnkPage" CssClass='<%# If(Eval("Enabled") Or (Eval("Enabled") = False And Eval("Text") = "<<") Or (Eval("Enabled") = False And Eval("Text") = ">>") Or (Eval("Enabled") = False And Eval("Text") = ">") Or (Eval("Enabled") = False And Eval("Text") = "<"), "page-link ", "page-link activePage")%>' runat="server" CommandArgument='<%# Eval("Value") %>' Enabled='<%# Eval("Enabled") %>' Text='<%#Eval("Text") %>' OnClick="Page_Changed">
                                                    </asp:LinkButton>

                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade modal-primary example-modal-lg" id="modelDivExport" aria-hidden="true" aria-labelledby="examplePositionCenter" role="dialog"
                        tabindex="-1" style="display: none;" data-backdrop="static">
                        <div class="modal-dialog modal-center">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">×</span>

                                    </button>

                                    <h3 class="modal-title">Export Job List !!</h3>
                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">

                                        <h4 class="modal-title">What Type Of Export Would You Like ? </h4>
                                        <div class="form-group" style="padding-top: 20px; padding-left: 150px">

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="PDFReport" runat="server" Checked="True" GroupName="ReportType" Text="PDF" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4">
                                                <div class="example-wrap" style="margin-bottom: 0;">
                                                    <div class="radio-custom radio-default">
                                                        <asp:RadioButton ID="SpreadSheet" runat="server" GroupName="ReportType" Text="Spreadsheet" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer" style="text-align: center">
                                    <asp:Button ID="btnExportReport" runat="server" CssClass="btn btn-primary" OnClick="btnExportReport_Click" Style="display: none" />
                                    <button type="button" id="btnSubmitReport" aria-label="Submit" class="btn btn-primary" onclick="modalhide();" />
                                    <span>Submit</span>


                                    <button type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Cancel">
                                        <span>Cancel</span></div>
                            </div>
                        </div>
                    </div>

                </asp:View>
            </asp:MultiView>
        </div>
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif" AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center" HorizontalOffset="150" VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
