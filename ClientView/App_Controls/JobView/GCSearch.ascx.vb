
Partial Class ClientViewLiens_GCSearch
    Inherits System.Web.UI.UserControl
    Public Event ItemSelected()
    Public Event SearchSelected(ByVal searchkey As String)

    Public Sub ClearData()
        Me.ListBox1.Items.Clear()
        Me.TextBox1.Text = ""
        Me.ListBox1.Visible = False

    End Sub
    Public Sub ShowList(ByVal searchkey As String)
        Me.ListBox1.Items.Clear()
        Me.TextBox1.Text = searchkey
        LoadList()

        Me.ModalPopupExtender1.Show()

    End Sub
    Public ReadOnly Property SelectedValue() As String
        Get
            Return Me.myValue.Value
        End Get
    End Property
    Public Property SelectClientId() As String
        Get
            Return Me.myClientId.Value
        End Get
        Set(ByVal value As String)
            Me.myClientId.Value = value
            LoadList()
        End Set
    End Property

    Private Sub LoadList()
        Me.ListBox1.Visible = False
        If Me.TextBox1.Text.Length < 1 Then Exit Sub
        Me.ListBox1.Items.Clear()

        Dim aOut As ArrayList = New ArrayList
        Dim myview As HDS.DAL.COMMON.TableView
        Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetGeneralContractorList
        MYSPROC.ClientId = SelectClientId
        MYSPROC.Name = Me.TextBox1.Text & "%"
        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
        myview.Sort = "Name"
        If myview.Count = 0 Then
            Exit Sub
        End If

        myview.MoveFirst()
        Do Until myview.EOF
            Dim LI As New System.Web.UI.WebControls.ListItem
            LI.Text = Trim(myview.RowItem("Name")) & ", "
            LI.Text += Trim(myview.RowItem("Address"))
            LI.Value = myview.RowItem("ID")
            Me.ListBox1.Items.Add(LI)
            myview.MoveNext()
        Loop

        Me.ListBox1.DataBind()
        Me.Session("CustSearchView") = myview
        Me.ListBox1.Visible = True

    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            LoadList()
        End If
        Me.ListBox1.DataBind()

    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim S As String = Me.ListBox1.SelectedIndex

    End Sub

    Protected Sub btnSearchIt_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        RaiseEvent SearchSelected(Me.TextBox1.Text)

    End Sub

    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        Me.ClearData()

    End Sub

    Protected Sub ListBox1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ListBox1.SelectedIndexChanged
        Me.ListBox1.DataBind()
        Me.myValue.Value = Me.ListBox1.SelectedValue
        Me.ClearData()
        RaiseEvent ItemSelected()

    End Sub
End Class
