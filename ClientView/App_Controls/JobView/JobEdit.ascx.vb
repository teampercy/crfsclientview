
Imports CRF.CLIENTVIEW.BLL
Imports HDS.WEBLIB.Common
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.BLL

Imports System.Data.SqlTypes

Partial Class App_Controls_LienView_JobEdit
    Inherits HDS.WEBSITE.UI.BaseUserControl
    Public Event ItemSaved()
    Public Event AddItem()
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobClientViewUpdates
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim myClient As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Client
    Dim dsjobinfo As System.Data.DataSet
    Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
    Dim mylieninfo As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientLienInfo
    Dim vwJobStatus As HDS.DAL.COMMON.TableView
    Dim vwDesk As HDS.DAL.COMMON.TableView

    Public Sub ClearData(ByVal JobId As String)

        Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(Me.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
        myuser = Session("UserInfo")
        If myuser.UserInfo.IsClientViewManager = 0 Then
            IsClientViewManager.Value = False
        Else
            IsClientViewManager.Value = True
        End If

        'disable Jobedit button for staff user
        If myuser.UserInfo.IsClientViewManager = 0 And myuser.UserInfo.IsClientViewAdmin = 0 Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "disableJobEditBtn", "disableJobEditBtn();", True)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "enableJobEditBtn", "enableJobEditBtn();", True)
        End If

        Me.ViewState("JobId") = JobId
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobId, myjob)

        With Me
            .StartDate.Value = myjob.StartDate
            .EndDate.Value = myjob.EndDate
            .EstBalance.Value = myjob.EstBalance
            .NOCDate.Value = myjob.NOCDate
            .chkSendAmended.Checked = False
            .chkSendAmended.Checked = False
            .chkJobPaidInFull.Checked = False
            '.chkJobAlert.Checked = myjob.JobAlertBox
            .chkNOCompBox.Checked = myjob.NOCompBox

            .txtJobName.Text = myjob.JobName
            .txtJobAddress1.Text = myjob.JobAdd1
            .txtJobAddress2.Text = myjob.JobAdd2
            .txtJobCity.Text = myjob.JobCity
            .hdnDrpState.Value = myjob.JobState
            .txtJobZip.Text = myjob.JobZip
            .LienSent.Value = myjob.FileDate
            .BondSent.Value = myjob.BondDate
            .ForecloseDate.Value = myjob.ForeclosureDate
            .BondSuitDate.Value = myjob.BondSuitDate
            .hdnDrpJobStatus.Value = myjob.JVStatusCode
            .hdnDrpDesk.Value = myjob.JVDeskNum
            .hdnDrpDeskID.Value = myjob.JVDeskNumId
            .hdnDrpJobStatusID.Value = myjob.JVStatusCodeId
            .NoticeSentDate.Value = myjob.NoticeSent
            .NOIDate.Value = myjob.NOIDate
            .SNDate.Value = myjob.SNDate
            If myjob.DateAssigned > DateTime.MinValue Then
                .hdnDateAssigned.Value = myjob.DateAssigned
            Else
                .hdnDateAssigned.Value = ""
            End If

            If myjob.PublicJob = True Then
                .hdnDrpPropertyType.Value = "Public"
            ElseIf myjob.FederalJob = True Then
                .hdnDrpPropertyType.Value = "Federal"
            ElseIf myjob.ResidentialBox = True Then
                .hdnDrpPropertyType.Value = "Residential"
            Else
                .hdnDrpPropertyType.Value = "Private"
            End If

        End With
        mystate = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateInfo(myjob.JobState)
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myjob.ClientId, mylieninfo)
        If mystate.NoticeOfCompBox = True And mylieninfo.IsNOCApproved = True Then
            Me.lblNOCompBox.Visible = True
            Me.chkNOCompBox.Visible = True
        Else
            Me.lblNOCompBox.Visible = False
            Me.chkNOCompBox.Visible = False
        End If

        If myjob.StatusCode = "CJP" Then
            Me.lblPaidStatus.Text = "Mark Paid Job Active:"
        Else
            Me.lblPaidStatus.Text = "Job Paid In Full:"
        End If

        '------ New Code Added --------------
        Me.LoadDropDown()
        '------------------------------------

        'If myjob.JobAlertBox = False Then
        '    Me.lblJobAlert.Text = "Add to Lien Alert"
        'Else
        '    Me.lblJobAlert.Text = "Remove from Lien Alert"
        'End If

    End Sub

#Region "Bind List to Job Status Dropdown and Desk Dropdown"
    Private Sub LoadDropDown()
        'Dim obj As CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users = CurrentUser.UserInfo
        'vwJobStatus = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVStatusClientList(obj.ClientId)
        'Dim strquery As String = ""
        'strquery = "(IsManualStatus='True') and (JOBVIEWSTATUS <> null or JOBVIEWSTATUS <>'')"
        'vwJobStatus.RowFilter = strquery
        'If vwJobStatus.Count > 0 Then
        '    vwJobStatus.MoveFirst()
        '    vwJobStatus.Sort = "FriendlyName"
        '    Me.DrpJobStatus.Items.Clear()

        '    Me.DrpJobStatus.Items.Insert(0, New ListItem("OATL-Outstanding balance to low", "OATL"))
        '    Me.DrpJobStatus.Items(0).Attributes.Add("JobStatusId", "92")
        '    Me.DrpJobStatus.Items.Insert(1, New ListItem("NJBD-No job site data", "NJBD"))
        '    Me.DrpJobStatus.Items(1).Attributes.Add("JobStatusId", "49")
        '    Me.DrpJobStatus.Items.Insert(2, New ListItem("CADN-Customer added to the do Not notice list", "CADN"))
        '    Me.DrpJobStatus.Items(2).Attributes.Add("JobStatusId", "90")
        '    Me.DrpJobStatus.Items.Insert(3, New ListItem("JBNL-Job type Not lien able", "JBNL"))
        '    Me.DrpJobStatus.Items(3).Attributes.Add("JobStatusId", "50")
        '    Me.DrpJobStatus.Items.Insert(4, New ListItem("EQNL-Equipment type Not lienable", "EQNL"))
        '    Me.DrpJobStatus.Items(4).Attributes.Add("JobStatusId", "61")
        '    Me.DrpJobStatus.Items.Insert(5, New ListItem("PIF-Job Paid In Full", "PIF"))
        '    Me.DrpJobStatus.Items(5).Attributes.Add("JobStatusId", "46")
        '    Me.DrpJobStatus.Items.Insert(6, New ListItem("BCFD-Bond claim filed", "BCFD"))
        '    Me.DrpJobStatus.Items(6).Attributes.Add("JobStatusId", "94")
        '    Me.DrpJobStatus.Items.Insert(7, New ListItem("LCFR-Lien Claim Filing Requested", "LCFR"))
        '    Me.DrpJobStatus.Items(7).Attributes.Add("JobStatusId", "93")
        '    Me.DrpJobStatus.Items.Insert(8, New ListItem("SNFD-Stop notice Filed", "SNFD"))
        '    Me.DrpJobStatus.Items(8).Attributes.Add("JobStatusId", "34")
        '    Dim li As ListItem
        '    Do Until vwJobStatus.EOF
        '        li = New ListItem()
        '        li.Value = vwJobStatus.RowItem("JobviewStatus")
        '        li.Text = vwJobStatus.RowItem("FriendlyName")
        '        li.Attributes.Add("JobStatusId", vwJobStatus.RowItem("Id"))
        '        If (li.Value <> "OATL" And li.Value <> "NJBD" And li.Value <> "CADN" And li.Value <> "JBNL" And li.Value <> "EQNL" And li.Value <> "PIF" And li.Value <> "BCFD" And li.Value <> "LCFR" And li.Value <> "SNFD") Then
        '            Me.DrpJobStatus.Items.Add(li)
        '        End If

        '        vwJobStatus.MoveNext()
        '    Loop
        'End If
        Dim obj As CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users = CurrentUser.UserInfo
        vwJobStatus = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVStatusClientList(obj.ClientId)
        Dim strquery As String = ""
        strquery = "(JOBVIEWSTATUS <> null or JOBVIEWSTATUS <>'') and (StatusSort <> 0 or StatusSort <> null) and jobeditstatus=1 "
        vwJobStatus.RowFilter = strquery
        vwJobStatus.Sort = "StatusSort"
        Me.DrpJobStatus.Items.Clear()
        If vwJobStatus.Count > 0 Then
            For index As Integer = 0 To vwJobStatus.Count - 1

                If (vwJobStatus.RowItem("StatusSort") <> "0") Then
                    vwJobStatus.RowIndex = index
                    Me.DrpJobStatus.Items.Insert(index, New ListItem(vwJobStatus.RowItem("FriendlyName"), vwJobStatus.RowItem("JobViewStatus")))
                    Me.DrpJobStatus.Items(index).Attributes.Add("JobStatusId", vwJobStatus.RowItem("JobViewStatusId"))
                End If
            Next

        End If
        Dim vwJobStatusWithoutSort As HDS.DAL.COMMON.TableView
        vwJobStatusWithoutSort = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVStatusClientList(obj.ClientId)
        strquery = "(JOBVIEWSTATUS <> null or JOBVIEWSTATUS <>'') and jobeditstatus=1 "
        vwJobStatusWithoutSort.RowFilter = strquery

        If vwJobStatusWithoutSort.Count > 0 Then
            Dim li As ListItem
            vwJobStatusWithoutSort.MoveFirst()
            vwJobStatusWithoutSort.Sort = "JobViewStatus"
            Do Until vwJobStatusWithoutSort.EOF
                li = New ListItem()
                li.Value = vwJobStatusWithoutSort.RowItem("JobviewStatus")
                li.Text = vwJobStatusWithoutSort.RowItem("FriendlyName")
                li.Attributes.Add("JobStatusId", vwJobStatusWithoutSort.RowItem("JobViewStatusId"))
                If (IsNothing(vwJobStatusWithoutSort.RowItem("StatusSort")) Or vwJobStatusWithoutSort.RowItem("StatusSort") = 0) Then
                    Me.DrpJobStatus.Items.Add(li)
                End If
                vwJobStatusWithoutSort.MoveNext()
            Loop
        End If


        vwDesk = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJVDeskClientList(obj.ClientId)
        strquery = "DESKNUM <> null or DESKNUM <>''"
        vwDesk.RowFilter = strquery
        If vwDesk.Count > 0 Then
            vwDesk.MoveFirst()
            vwDesk.Sort = "FriendlyName"
            Me.DrpDesk.Items.Clear()
            Dim li As ListItem
            Do Until vwDesk.EOF
                li = New ListItem()
                li.Value = vwDesk.RowItem("DESKNUM")
                li.Text = vwDesk.RowItem("FriendlyName")
                li.Attributes.Add("DeskId", vwDesk.RowItem("JobViewDeskId"))
                Me.DrpDesk.Items.Add(li)
                vwDesk.MoveNext()
            Loop
        End If


        Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
        If Not HttpContext.Current.Session("StateList") Is Nothing Then
            objList = HttpContext.Current.Session("StateList")
        Else
            Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
            Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
            Dim VWSTATES As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateTable

            VWSTATES.MoveFirst()
            Do Until VWSTATES.EOF
                mystate = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
                mystate = VWSTATES.FillEntity(mystate)
                Dim LI As New System.Web.UI.WebControls.ListItem
                LI.Text = mystate.StateName
                LI.Value = mystate.StateInitials
                objList.Add(LI)
                VWSTATES.MoveNext()
            Loop


        End If

        DrpState.DataSource = objList
        DrpState.DataTextField = "Text"
        DrpState.DataValueField = "Value"
        DrpState.DataBind()
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "DDL", "binDDlist();", True)

    End Sub
#End Region

    'Protected Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
    '    Dim s As String = ""

    'End Sub


    Public Function GetStatus(ByVal StatusId As Integer) As String
        Dim StatusCode As String = ""
        If (StatusId > 0) Then
            Dim MYSQL As String = "Select JobviewStatus + '-' + Description as FriendlyName FROM vwJobViewStatusClient"
            MYSQL += " Where JobViewStatusId = " & StatusId
            Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
            If MYDT.Tables(0).Rows.Count > 0 Then
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                StatusCode = myview.RowItem("FriendlyName")
            End If
        End If
        Return StatusCode
    End Function
    Public Function GetDesk(ByVal DeskId As Integer) As String
        Dim DeskStatusCode As String = ""
        If (DeskId > 0) Then
            Dim MYSQL As String = "select DESKNUM + '-' + DeskName as FriendlyName from vwJobViewDeskClient"
            MYSQL += " Where JobViewDeskId = " & DeskId
            Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
            If MYDT.Tables(0).Rows.Count > 0 Then
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                DeskStatusCode = myview.RowItem("FriendlyName")
            End If
        End If
        Return DeskStatusCode
    End Function
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.ViewState("JobId"), myjob)

        With myreq
            .JobId = myjob.Id
            .DateCreated = Now
            .UserId = Me.CurrentUser.Id
            .UserCode = (Left(Me.UserInfo.UserInfo.UserName, 10))
            .EndDate = Utils.GetDate(Me.EndDate.Value)
            .StartDate = Utils.GetDate(Me.StartDate.Value)
            .JobPaidInFull = Me.chkJobPaidInFull.Checked
            .SendAmendedNotice = Me.chkSendAmended.Checked
            .EstBalance = Utils.GetDecimal(Me.EstBalance.Value)
            .NOCDate = Utils.GetDate(Me.NOCDate.Value)
            '.JobAlert = Me.chkJobAlert.Checked
            .NOCompBox = Me.chkNOCompBox.Checked
            .JobName = Me.txtJobName.Text
            .JobAdd1 = Me.txtJobAddress1.Text
            .JobAdd2 = Me.txtJobAddress2.Text
            .JobCity = Me.txtJobCity.Text
            .JobState = Me.hdnDrpState.Value
            .JobZip = Me.txtJobZip.Text
            .FileDate = Utils.GetDate(Me.LienSent.Value)
            .BondDate = Utils.GetDate(Me.BondSent.Value)
            .ForeclosureDate = Utils.GetDate(Me.ForecloseDate.Value)
            .BondSuitDate = Utils.GetDate(Me.BondSuitDate.Value)
            .JVStatusCodeId = If(Me.hdnDrpJobStatusID.Value = String.Empty, 0, Convert.ToInt16(Me.hdnDrpJobStatusID.Value))
            .JVDeskNumId = If(Me.hdnDrpDeskID.Value = String.Empty, 0, Convert.ToInt16(Me.hdnDrpDeskID.Value))
            .SNDate = Utils.GetDate(Me.SNDate.Value)

        End With

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(myreq)

        Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
        With mynote
            .JobId = myjob.Id
            .DateCreated = Now()
            .Note = ""
            If myreq.JVStatusCodeId <> myjob.JVStatusCodeId Then
                .Note += "Job Status Changed From:" & GetStatus(myjob.JVStatusCodeId) & " to " & GetStatus(myreq.JVStatusCodeId)
                .Note += vbCrLf
            End If
            If myreq.JVDeskNumId <> myjob.JVDeskNumId Then
                .Note += "Desk Changed From:" & GetDesk(myjob.JVDeskNumId) & " to " & GetDesk(myreq.JVDeskNumId)
                .Note += vbCrLf
            End If

            If myreq.EndDate <> myjob.EndDate Then
                .Note += "End Date Changed From:" & myjob.EndDate & " to " & myreq.EndDate
                .Note += vbCrLf
            End If
            If myreq.StartDate <> myjob.StartDate Then
                .Note += "Start Date Changed From:" & myjob.StartDate & " to " & myreq.StartDate
                .Note += vbCrLf
            End If
            If myreq.EstBalance <> myjob.EstBalance Then
                .Note += "Estimated Balance Changed From:" & myjob.EstBalance & " to " & myreq.EstBalance
                .Note += vbCrLf
            End If
            If myreq.NOCDate <> myjob.NOCDate Then
                .Note += "NOC Date Changed From:" & myjob.NOCDate & " to " & myreq.NOCDate
                .Note += vbCrLf
            End If
            If myreq.FileDate <> myjob.FileDate Then
                .Note += "File Date Changed From:" & myjob.FileDate & " to " & myreq.FileDate
                .Note += vbCrLf
            End If
            If myreq.ForeclosureDate <> myjob.ForeclosureDate Then
                .Note += "Foreclosure Date Changed From:" & myjob.ForeclosureDate & " to " & myreq.ForeclosureDate
                .Note += vbCrLf
            End If
            If myreq.BondDate <> myjob.BondDate Then
                .Note += "Bond Date Changed From:" & myjob.BondDate & " to " & myreq.BondDate
                .Note += vbCrLf
            End If
            If myreq.BondSuitDate <> myjob.BondSuitDate Then
                .Note += "BondSuit Date Changed From:" & myjob.BondSuitDate & " to " & myreq.BondSuitDate
                .Note += vbCrLf
            End If
            If myreq.SNDate <> myjob.SNDate Then
                .Note += "SNDate Changed From:" & myjob.SNDate & " to " & myreq.SNDate
                .Note += vbCrLf
            End If
            'If myreq.JobAlert <> myjob.JobAlertBox Then
            'If Me.chkJobAlert.Checked = True Then
            '    .Note += "Added to Lien Alert"
            '    .Note += vbCrLf
            'ElseIf Me.chkJobAlert.Checked = False Then
            '    .Note += "Removed from Lien Alert"
            '    .Note += vbCrLf
            'End If
            'End If
            If Me.chkJobPaidInFull.Checked = True Then
                If myjob.StatusCode = "CJP" Then
                    .Note += "Job Changed to Active from Paid In Full"
                    .Note += vbCrLf
                Else
                    .Note += "Job Changed to Paid In Full"
                    .Note += vbCrLf
                End If
            End If
            If Me.chkSendAmended.Checked = True Then
                .Note += "Amended Notice Requested"
                .Note += vbCrLf
            End If
            If Me.chkNOCompBox.Checked = True Then
                .Note += "Notice of Completion Requested"
                .Note += vbCrLf
            End If

            .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
            .ClientView = True
            .EnteredByUserId = Me.UserInfo.UserInfo.ID
            .ActionTypeId = 0

            If Strings.Len(.Note.ToString) > 1 Then
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
            End If
        End With

        'Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_UpdateJob
        'MYSPROC.RequestId = myreq.PKID
        'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.ExecNonQuery(MYSPROC)

        With myjob
            .EndDate = Utils.GetDate(Me.EndDate.Value)
            .StartDate = Utils.GetDate(Me.StartDate.Value)
            .EstBalance = Me.EstBalance.Text
            .LienAmt = myreq.EstBalance
            .NOCDate = myreq.NOCDate
            .JobAlertBox = myreq.JobAlert
            .NOCompBox = myreq.NOCompBox

            ' Newly Added 09-12-2016
            .JobName = Me.txtJobName.Text
            .JobAdd1 = Me.txtJobAddress1.Text
            .JobAdd2 = Me.txtJobAddress2.Text
            .JobCity = Me.txtJobCity.Text
            .JobState = Me.hdnDrpState.Value
            .JVDeskNum = Me.hdnDrpDesk.Value
            .JVDeskNumId = If(Me.hdnDrpDeskID.Value = String.Empty, 0, Convert.ToInt16(Me.hdnDrpDeskID.Value))
            .JVStatusCode = Me.hdnDrpJobStatus.Value
            .JVStatusCodeId = If(Me.hdnDrpJobStatusID.Value = String.Empty, 0, Convert.ToInt16(Me.hdnDrpJobStatusID.Value))
            .JobZip = Me.txtJobZip.Text
            .FileDate = Utils.GetDate(Me.LienSent.Value)
            .BondDate = Utils.GetDate(Me.BondSent.Value)
            .LienDate = Utils.GetDate(Me.LienSent.Value)
            .ForeclosureDate = Utils.GetDate(Me.ForecloseDate.Value)
            .BondSuitDate = Utils.GetDate(Me.BondSuitDate.Value)
            .NoticeSent = Utils.GetDate(Me.NoticeSentDate.Value)
            .NOIDate = Utils.GetDate(Me.NOIDate.Value)
            .SNDate = Utils.GetDate(Me.SNDate.Value)
            If (.JVStatusCodeId > 0) Then
                If (CheckISClosed(.JVStatusCodeId) = 1) Then
                    .CloseDate = Utils.GetDate(DateTime.Now.Date)
                Else

                    .CloseDate = SqlDateTime.Null
                End If
            End If

            '----------------------------------------------
            If ((Me.hdnDrpPropertyType.Value) = "Public") Then
                .PublicJob = True
                .FederalJob = False
                .ResidentialBox = False
            ElseIf ((Me.hdnDrpPropertyType.Value) = "Federal") Then
                .FederalJob = True
                .PublicJob = False
                .ResidentialBox = False
            ElseIf ((Me.hdnDrpPropertyType.Value) = "Residential") Then
                .ResidentialBox = True
                .PublicJob = False
                .FederalJob = False
            Else
                .FederalJob = False
                .PublicJob = False
                .ResidentialBox = False

            End If

            If Me.chkJobPaidInFull.Checked = True Then
                If myjob.StatusCodeId <> "60" Then
                    .StatusCodeId = 60
                Else
                    .StatusCodeId = 33
                End If
            End If

        End With
        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)

        'Dim MYSPROC2 As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_LiensDayEnd_UpdateDeadlineDates
        'MYSPROC2.ExpireDays = 1200
        'MYSPROC2.JobId = myjob.Id
        'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.ExecNonQuery(MYSPROC2)

        'Dim MYSPROC3 As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_Jobview_UpdateDeadlineDates
        'MYSPROC3.JobId = myjob.Id
        'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.ExecNonQuery(MYSPROC3)

        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myjob.ClientId, myClient)
        If myjob.IsJobView = True And myClient.NoRentalInfo = False Then
            Dim mysproc As New SPROCS.uspbo_Jobview_UpdateDeadlineDates
            mysproc.JobId = myjob.Id
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.ExecNonQuery(mysproc)
        Else
            Dim mysproc As New SPROCS.uspbo_LiensDayEnd_UpdateDeadlineDates
            mysproc.ExpireDays = 0
            mysproc.JobId = myjob.Id
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.ExecNonQuery(mysproc)
        End If

        ClearData(Me.ViewState("JobId"))
        ''##101
        Session("JobList") = Nothing
        RaiseEvent ItemSaved()

    End Sub

    Protected Sub CancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CancelButton.Click
        ClearData(Me.ViewState("JobId"))
        'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsModalHide", "ModalHide();", True)
        ' ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsActive", "ActivateTab('MainInfo');", True)
        'RaiseEvent ItemSaved()
    End Sub
    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btncancel.Click
        ClearData(Me.ViewState("JobId"))
        RaiseEvent ItemSaved()
    End Sub
    Public Function CheckISClosed(ByVal status As Integer) As Integer
        Dim isClosed As Integer = 0
        If (status > 0) Then
            Dim MYSQL As String = "select isClosed from jobviewstatus"
            MYSQL += " Where id = " & status
            Dim MYDT As System.Data.DataSet = ProviderBase.DAL.GetDataSet(MYSQL)
            If MYDT.Tables.Count > 0 Then
                Dim myview As New HDS.DAL.COMMON.TableView(MYDT.Tables(0))
                If myview.RowItem("isClosed") = "true" Then
                    isClosed = 1
                End If
            End If
        End If
        Return isClosed
    End Function
End Class
