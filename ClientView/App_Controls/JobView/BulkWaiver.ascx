﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls"
    TagPrefix="cc1" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL.CRFDB" %>
<%@ Import Namespace="CRF.CLIENTVIEW.BLL" %>
<%@ Import Namespace="System.Drawing" %>
<%@ Import Namespace="System.Drawing.Printing" %>
<%@ Import Namespace="System.IO" %>
<style>
    #JobFilterBody .radio-custom label {
        width: 75px;
    }


    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 4px;
    }

    .dataTables_length {
        margin-top: 10px;
    }

    .nowordwraptd {
        /*padding-left: 5px;*/
        white-space: nowrap;
        text-overflow: ellipsis;
        overflow: hidden;
    }
    /*input:focus {outline:1px dotted red !important;}*/
</style>

<script type="text/javascript">
    $(document).ready(function () {

        $("div").on("click", "input:text[id*=txtPaymentAmount]", function () {
            var textbox = $(this);
            textbox.focus();
            textbox.select();

        });
        $("div").on("blur", "input:text[id*=txtPaymentAmount]", function () {
            var textbox = $(this);
            if (textbox.val().length > 0 && textbox.val().indexOf("$") == -1) {
                var value = parseFloat(textbox.val())
                textbox.val('$' + value.toFixed(2));
            }
        });
    });

    function textboxonblur() {
        alert("aa");
    }
    function CallDatePicker() {

        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
            changeMonth: true,
            changeYear: true,
            autoclose: true,
            showButtonPanel: true,
        });

        //$(".datepicker").datetimepicker({ useCurrent: false, format: 'DD/MM/YYYY' });
    }
    var JobAddress = '';
    function Successpopup() {

        if (JobAddress) {
            document.getElementById('<%=txtSubject.ClientID%>').value = "New Waiver Document for " + JobAddress;
        }
        else {
            document.getElementById('<%=txtSubject.ClientID%>').value = "New Waiver Document";
        }
        document.getElementById('<%=txtBody.ClientID%>').value = "";
        $("#divModalEmail").modal('show');
        //document.getElementById('<%=btnEmailWaiver.ClientID%>').click();
        return false;
    }
    function fnJobAddress(JAddress) {
        JobAddress = JAddress;
    }
    function Cancelpopup() {
        document.getElementById('<%=btnPrintWaiver.ClientID%>').click();
        return false;
    }
    function SendMail() {
        if (document.getElementById('<%=txtToList.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the 'To' EmailId.";
            return false;
        }
        if (document.getElementById('<%=txtSubject.ClientID%>').value == "") {
            document.getElementById('<%=lblErrorMsg.ClientID%>').innerText = "Please enter the Subject.";
            return false;
        }
        document.getElementById('<%=btnEmailWaiver.ClientID%>').click();
        $("#divModalEmail").modal('hide');
        return false;
    }

    function EmailSendPopup() {
        swal({
            title: "Successfully Sent!",
            text: "Your Waiver Has Been Emailed.",
            type: "success",
            showDoneButton: true,
            closeOnDone: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
        });
    }
    function printAllSendPopup() {
        swal({
            title: "Successfully Print!",
            text: "Job Waiver Data has printed successfully.",
            type: "success",
            showDoneButton: true,
            closeOnDone: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
        });
    }
    function NoprintAllSendPopup() {
        swal({
            title: "Print!",
            text: "There is no data to print.",
            type: "success",
            showDoneButton: true,
            closeOnDone: true,
            showCancelButton: false,
            showConfirmButton: false,
            closeOnConfirm: false,
            closeOnCancel: false
        });
    }
    function AlertSuccessFunc() {
        swal({
            title: "Successfully Submitted!",
            text: "Waiver data has been saved successfully! \n What would you like to do now?",
            confirmButtonClass: "btn btn-success",
            cancelButtonText: "Print",
            confirmButtonText: 'Email',
            type: "success",
            showDoneButton: true,
            closeOnDone: true
        });
    }
    function MaintainMenuOpen() {
        SetHeaderBreadCrumb('RentalView', 'Tools', 'Bulk Waiver');
        MainMenuToggle('liRentalToolsJobView');
        SubMenuToggle('liBulkWaiverJobView');
        //var PageName = window.location.search.substring(1);
        //if (PageName == "lienview.BulkWaiver.JobView") {

        //}
        //else {
        //    SetHeaderBreadCrumb('LienView', 'Tools', 'Bulk Waiver');
        //    MainMenuToggle('liTools');
        //    SubMenuToggle('liBulkWaiver');
        //}

        return false;

    }
    function CallSuccessFunc() {
        var defaults = {
            "stateSave": true,
            "stateDuration": 60 * 10,
            "lengthMenu": [5, 15, 25, 35, 50, 100],
            bAutoWidth: false,
            responsive: true,
            "bSort": false,
            "aaSorting": [[0]],
            "aoColumns": [
                { "bSortable": false }
            ],
            "bFilter": false,
            "dom": "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-4'il><'col-sm-7'p>>",
            language: {
                "sSearchPlaceholder": "Search..",
                "lengthMenu": "_MENU_",
                "search": "_INPUT_",
                "paginate": {
                    "previous": '<i class="icon wb-chevron-left-mini"></i>',
                    "next": '<i class="icon wb-chevron-right-mini"></i>'
                }
            },
        };
        var options = $.extend(true, {}, defaults, $(this).data());
        $('#' + '<%=gvwJobs.ClientID%>').dataTable(options);
        $('.datepicker').datepicker({
            format: "mm/dd/yyyy",
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
        });

    }
</script>
<script type="text/javascript">
    function myFunctionCustomerNameLike(id) {
        if (id == 1) {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionCustomerRefLike(id) {

        if (id == 1) {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = true;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=CustomerRefLike_1.ClientID%>').checked = false;
            document.getElementById('<%=CustomerRefLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionOwnerNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=OwnerNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=OwnerNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionGCNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=GCNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=GCNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=GCNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=GCNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionLenderNameLike(id) {

        if (id == 1) {
            document.getElementById('<%=LenderNameLike_1.ClientID%>').checked = true;
            document.getElementById('<%=LenderNameLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=LenderNameLike_1.ClientID%>').checked = false;
            document.getElementById('<%=LenderNameLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobAddressLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobAddressLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobAddressLike_2.ClientID%>').checked = true;
        }
    }

    function myFunctionJobNoLike(id) {

        if (id == 1) {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = true;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = false;
        }
        else {
            document.getElementById('<%=JobNoLike_1.ClientID%>').checked = false;
            document.getElementById('<%=JobNoLike_2.ClientID%>').checked = true;
        }
    }

    function myNotary(id, value) {
        var array = id.split("_");
        var arrayid = array[3];
        var NotarywithCAid = "ctl33_ctl00_gvwJobs_" + arrayid + "_NotarywithCA"
        var NotarywithoutCAid = "ctl33_ctl00_gvwJobs_" + arrayid + "_NotarywithoutCA"

        if (value == 1) {
            if (document.getElementById(NotarywithoutCAid).checked == false && document.getElementById(NotarywithCAid).checked == false) {

                document.getElementById(NotarywithCAid).checked = false;
                document.getElementById(NotarywithoutCAid).checked = false;
            }
            else if (document.getElementById(NotarywithoutCAid).checked == false && document.getElementById(NotarywithCAid).checked == true) {

                document.getElementById(NotarywithCAid).checked = true;
                document.getElementById(NotarywithoutCAid).checked = false;
            }
            else {
                document.getElementById(NotarywithCAid).checked = true;
                document.getElementById(NotarywithoutCAid).checked = false;
            }
        }
        else {
            if (document.getElementById(NotarywithCAid).checked == false && document.getElementById(NotarywithoutCAid).checked == false) {

                document.getElementById(NotarywithCAid).checked = false;
                document.getElementById(NotarywithoutCAid).checked = false;
            }
            else if (document.getElementById(NotarywithoutCAid).checked == false && document.getElementById(NotarywithCAid).checked == true) {

                document.getElementById(NotarywithCAid).checked = false;
                document.getElementById(NotarywithoutCAid).checked = true;
            }
            else {
                document.getElementById(NotarywithCAid).checked = false;
                document.getElementById(NotarywithoutCAid).checked = true;
            }
        }
    }
</script>
<script runat="server">

    Protected WithEvents btnSaveWaiver As Button
    Protected WithEvents btnPrint As Button
    Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
    Dim myreq As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobWaiverLog
    Dim myview As HDS.DAL.COMMON.TableView
    Dim myviewSorting As HDS.DAL.COMMON.TableView
    Dim myjob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Job
    Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateForms
    Shared VWSIGNERS As HDS.DAL.COMMON.TableView
    Dim streamToPrint As StreamReader 'Added by Shishir 25-05-2016
    Dim printFont As Drawing.Font 'Added by Shishir 25-05-2016
    Dim notary As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If Me.Page.IsPostBack = False Then
            VWSIGNERS = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientSigners
            Me.MultiView1.SetActiveView(Me.View1)
        Else
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "SetDatePicker", "SetDatePicker();", True)
        End If

    End Sub

    Protected Sub btnExitViewer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExitViewer.Click
        Me.MultiView1.SetActiveView(Me.View2)
        If Not Me.gvwJobs.Rows.Count = 0 Then
            Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub btnDownLoad_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownLoad.Click
        Me.DownLoadReport(Me.Session("spreadsheet"))
        Me.MultiView1.SetActiveView(Me.View2)
        If Not Me.gvwJobs.Rows.Count = 0 Then
            Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub btnBackFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBackFilter.Click

        Me.MultiView1.SetActiveView(Me.View1)
        Me.CustomerName.Text = ""
        Me.CustomerNameLike_1.Checked = True
        Me.CustomerNameLike_2.Checked = False
        Me.CustomerRef.Text = ""
        Me.CustomerRefLike_1.Checked = True
        Me.CustomerRefLike_2.Checked = False
        Me.JobName.Text = ""
        Me.JobNameLike_1.Checked = True
        Me.JobNameLike_2.Checked = False
        Me.JobNo.Text = ""
        Me.JobNoLike_1.Checked = True
        Me.JobNoLike_2.Checked = False
        Me.JobAddress.Text = ""
        Me.JobAddressLike_1.Checked = True
        Me.JobAddressLike_2.Checked = False
        Me.JobCity.Text = ""
        Me.JobState.Text = ""
        Me.OwnerName.Text = ""
        Me.OwnerNameLike_1.Checked = True
        Me.OwnerNameLike_2.Checked = False
        Me.GCName.Text = ""
        Me.GCNameLike_1.Checked = True
        Me.GCNameLike_2.Checked = False
        Me.BranchNo.Text = ""
        Me.PONum.Text = ""
        Me.JobStatus.Text = ""
        Me.TempId.Text = ""
        Me.FileNumber.Text = ""
        Me.LenderName.Text = ""
        Me.LenderNameLike_1.Checked = True
        Me.LenderNameLike_2.Checked = False
        Me.Calendar1.Value = DateTime.Now.Date
        Me.Calendar2.Value = DateTime.Now.Date
        Me.chkByAssignDate.Checked = False
        Me.chkPaidInFull.Checked = False
        Me.chkAllJobs.Checked = False
    End Sub
    Protected Sub gvwJobs_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwJobs.RowCreated
        If e.Row.RowType = DataControlRowType.DataRow Then
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "CallDatePicker", "CallDatePicker();", True)
            'btnSaveWaiver = TryCast(e.Row.FindControl("btnSaveWaiver"), Button)
            'AddHandler CType(e.Row.FindControl("btnSaveWaiver"), Button).Click, AddressOf btnSaveWaiver_Click
            ''  btnPrint = TryCast(e.Row.FindControl("btnPrint"), Button)
            '' AddHandler CType(e.Row.FindControl("btnPrint"), Button).Click, AddressOf btnPrint_Click
        End If
    End Sub
    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrint.Click
        btnPrint = TryCast(sender, Button)
        Dim myitemid As String = btnPrint.Attributes("rowno")
        If myitemid = 0 Then
            myitemid = -1
        End If
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobAck(Me.CurrentUser.Id, myitemid, True)
        Me.btnDownLoad.Visible = False
        If IsNothing(sreport) = False Then
            Me.btnDownLoad.Visible = True
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Me.plcReportViewer.Controls.Clear()
            Dim mylit As New Literal
            mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
            If Not (mylit.Text Is Nothing) Then
                Dim IframeTag As String
                IframeTag = mylit.Text
                Dim Filepath As String = ""
                Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                If (Filepath <> "") Then
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                End If
            End If
            'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
            'Me.MultiView1.SetActiveView(Me.View3) 'added by Jaywanti on 06-01-2016
        Else
            Me.plcReportViewer.Controls.Clear()
            Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
            Me.MultiView1.SetActiveView(Me.View3) 'added by Jaywanti on 06-01-2016
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        With mysproc
            .FromReferalDate = Me.Calendar1.Value
            .ThruReferalDate = Me.Calendar2.Value
            If Me.chkByAssignDate.Checked Then
                .ReferalRange = 1
            End If
            'Commented by Mahesh on 21/3/2016
            'If Me.CustomerName.Text.Trim.Length > 0 Then
            '    If Me.CustomerNameLike.SelectedValue.Trim = "Includes" Then
            '        .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
            '    Else
            '        .ClientCustomer = Me.CustomerName.Text.Trim & "%"
            '    End If
            'End If

            If Me.CustomerName.Text.Trim.Length > 0 Then
                If Me.CustomerNameLike_1.Checked Then
                    .ClientCustomer = Me.CustomerName.Text.Trim & "%"
                End If

                If Me.CustomerNameLike_2.Checked Then
                    .ClientCustomer = "%" & Me.CustomerName.Text.Trim & "%"
                End If
            End If


            'If Me.CustomerRef.Text.Trim.Length > 0 Then
            '    If Me.CustomerRefLike.SelectedValue.Trim = "Includes" Then
            '        .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
            '    Else
            '        .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
            '    End If
            'End If

            If Me.CustomerRef.Text.Trim.Length > 0 Then
                If Me.CustomerRefLike_1.Checked Then
                    .ClientCustomerRef = Me.CustomerRef.Text.Trim & "%"
                End If

                If Me.CustomerRefLike_2.Checked Then
                    .ClientCustomerRef = "%" & Me.CustomerRef.Text.Trim & "%"
                End If
            End If


            'If Me.OwnerName.Text.Trim.Length > 0 Then
            '    If Me.OwnerNameLike.SelectedValue.Trim = "Includes" Then
            '        .PropOwner = "%" & Me.OwnerName.Text.Trim & "%"
            '    Else
            '        .PropOwner = Me.OwnerName.Text.Trim & "%"
            '    End If
            'End If
            If Me.OwnerName.Text.Trim.Length > 0 Then
                If Me.OwnerNameLike_1.Checked Then
                    .PropOwner = Me.OwnerName.Text.Trim & "%"
                End If

                If Me.OwnerNameLike_2.Checked Then
                    .PropOwner = "%" & Me.OwnerName.Text.Trim & "%"
                End If
            End If

            'If Me.GCName.Text.Trim.Length > 0 Then
            '    If Me.GCNameLike.SelectedValue.Trim = "Includes" Then
            '        .GeneralContractor = "%" & Me.GCName.Text.Trim & "%"
            '    Else
            '        .GeneralContractor = Me.GCName.Text.Trim & "%"
            '    End If
            'End If
            If Me.GCName.Text.Trim.Length > 0 Then
                If Me.GCNameLike_1.Checked Then
                    .GeneralContractor = Me.GCName.Text.Trim & "%"
                End If

                If Me.GCNameLike_2.Checked Then
                    .GeneralContractor = "%" & Me.GCName.Text.Trim & "%"
                End If
            End If


            'If Me.LenderName.Text.Trim.Length > 0 Then
            '    If Me.LenderNameLike.SelectedValue.Trim = "Includes" Then
            '        .LenderName = "%" & Me.LenderName.Text.Trim & "%"
            '    Else
            '        .LenderName = Me.LenderName.Text.Trim & "%"
            '    End If
            'End If
            If Me.LenderName.Text.Trim.Length > 0 Then
                If Me.LenderNameLike_1.Checked Then
                    .LenderName = Me.LenderName.Text.Trim & "%"
                End If

                If Me.LenderNameLike_2.Checked Then
                    .LenderName = "%" & Me.LenderName.Text.Trim & "%"
                End If
            End If

            'If Me.JobName.Text.Trim.Length > 0 Then
            '    If Me.JobNameLike.SelectedValue.Trim = "Includes" Then
            '        .JobName = "%" & Me.JobName.Text.Trim & "%"
            '    Else
            '        .JobName = Me.JobName.Text.Trim & "%"
            '    End If
            'End If
            If Me.JobName.Text.Trim.Length > 0 Then
                If Me.JobNameLike_1.Checked Then
                    .JobName = Me.JobName.Text.Trim & "%"
                End If

                If Me.JobNameLike_2.Checked Then
                    .JobName = "%" & Me.JobName.Text.Trim & "%"
                End If
            End If


            'If Me.JobAddress.Text.Trim.Length > 0 Then
            '    If Me.JobAddressLike.SelectedValue.Trim = "Includes" Then
            '        .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
            '    Else
            '        .JobAdd1 = Me.JobAddress.Text.Trim & "%"
            '    End If
            'End If
            If Me.JobAddress.Text.Trim.Length > 0 Then
                If Me.JobAddressLike_1.Checked Then
                    .JobAdd1 = Me.JobAddress.Text.Trim & "%"
                End If

                If Me.JobAddressLike_2.Checked Then
                    .JobAdd1 = "%" & Me.JobAddress.Text.Trim & "%"
                End If
            End If


            'If Me.JobNo.Text.Trim.Length > 0 Then
            '    If Me.JobNameLike.SelectedValue.Trim = "Includes" Then
            '        .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
            '    Else
            '        .JobNumber = Me.JobNo.Text.Trim & "%"
            '    End If
            'End If
            If Me.JobNo.Text.Trim.Length > 0 Then
                If Me.JobNoLike_1.Checked Then
                    .JobNumber = Me.JobNo.Text.Trim & "%"
                End If

                If Me.JobNoLike_2.Checked Then
                    .JobNumber = "%" & Me.JobNo.Text.Trim & "%"
                End If
            End If


            If Me.JobCity.Text.Trim.Length > 0 Then
                .JobCity = Me.JobCity.Text.Trim & "%"
            End If

            If Me.JobState.Text.Trim.Length > 0 Then
                .JobState = Me.JobState.Text.Trim & "%"
            End If

            If Me.BranchNo.Text.Trim.Length > 0 Then
                .BRANCHNUMBER = Me.BranchNo.Text.Trim
            End If

            If Me.PONum.Text.Trim.Length > 0 Then
                .PONum = Me.PONum.Text.Trim
            End If

            'If Me.JobState.Text.Trim.Length > 0 Then
            '    .JobState = Me.JobState.Text.Trim
            'End If

            If Me.JobStatus.Text.Trim.Length > 0 Then
                .StatusCode = Me.JobStatus.Text.Trim & "%"
            End If

            If Me.FileNumber.Text.Trim.Length > 0 Then
                .JobId = Me.FileNumber.Text.Trim
            End If

            If Me.TempId.Text.Trim.Length > 0 Then
                .TempId = Me.TempId.Text.Trim
            End If

            If chkAllJobs.Checked Then
                .ActiveOnly = False
            Else
                .ActiveOnly = True
            End If

            If Me.chkPaidInFull.Checked = True Then
                .ExcludePIF = 1
            End If

            .UserId = Me.CurrentUser.Id

        End With
        'GetData(mysproc, 1, "F")
        GetData(mysproc, 1)
        drpSortBy.SelectedValue = "-1"
        Me.MultiView1.SetActiveView(Me.View2)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1, ByVal apage As Integer)
        Try

            myview = TryCast(Me.Session("JobList"), HDS.DAL.COMMON.TableView)

            If asproc.Page < 1 Then
                asproc.Page = 1
            End If

            asproc.PageRecords = 10000
            asproc.UserId = Me.CurrentUser.Id

            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc) 'Gets data from database

            Session("JobListP") = asproc
            Session("JobList") = myview

            Me.gvwJobs.DataSource = myview
            Me.gvwJobs.DataBind()
            If gvwJobs.Rows.Count > 0 Then
                Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
            'Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
            Me.MultiView1.SetActiveView(Me.View2)

            Me.Session("joblist") = myview
        Catch ex As Exception

        End Try


    End Sub


    'Private Sub GetData(ByRef asproc As CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1, ByVal apage As Integer, ByVal adirection As String)
    '    myview = TryCast(Me.Session("JobList"), HDS.DAL.COMMON.TableView)

    '    If asproc.Page < 1 Then
    '        asproc.Page = 1
    '    End If

    '    asproc.PageRecords = 10000
    '    asproc.UserId = Me.CurrentUser.Id
    '    If adirection = "F" Then
    '        asproc.Page = 1
    '    End If
    '    If adirection = "N" Then
    '        asproc.Page += 1
    '    End If
    '    If adirection = "P" Then
    '        asproc.Page -= 1
    '    End If

    '    If asproc.Page < 1 Then
    '        asproc.Page = 1
    '    End If

    '    If adirection = "L" Then
    '        myview = Session("JobList")
    '        asproc.Page = myview.RowItem("pages")
    '    End If

    '    If adirection.Length < 1 And IsNothing(myview) = False Then
    '        myview = Session("JobList")
    '    Else
    '        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(asproc)
    '    End If

    '    If myview.Count < 1 Then
    '        Exit Sub
    '    End If

    '    Session("JobListP") = asproc
    '    Session("JobList") = myview

    '    Dim totrecs As String = myview.RowItem("totalrecords")
    '    Dim currpage As String = myview.RowItem("page")
    '    Dim totpages As Integer = (totrecs / 14 + 1)
    '    totpages = myview.RowItem("pages")

    '    Dim i As Integer = 0

    '    Me.gvwJobs.DataSource = myview
    '    Me.gvwJobs.DataBind()
    '    If gvwJobs.Rows.Count > 0 Then
    '        Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader

    '    End If
    '    'Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
    '    Me.MultiView1.SetActiveView(Me.View2)

    '    Me.Session("joblist") = myview

    'End Sub



    Protected Sub btnViewAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnViewAll.Click
        Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
        Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
        obj.Page = 1
        obj.PageRecords = 10000 '500 Changed by jaywanti
        obj.UserId = Me.CurrentUser.Id ' here hardoded value is there i.e 5
        myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
        Me.gvwJobs.DataSource = myview
        Me.gvwJobs.DataBind()
        If gvwJobs.Rows.Count > 0 Then
            Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader

        End If
        'Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        Me.Session("joblist") = myview
        Me.MultiView1.SetActiveView(Me.View2)
        drpSortBy.SelectedValue = "-1"
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub
    Protected Sub gvwJobs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvwJobs.RowDataBound

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim DR As System.Data.DataRowView = e.Row.DataItem
            'Dim hdnBatchId As HiddenField = CType(e.Row.FindControl("hdnBatchId"), HiddenField)
            'btnSaveWaiver = TryCast(e.Row.FindControl("btnSaveWaiver"), Button)
            'btnSaveWaiver.Attributes("rowno") = DR("JobId")
            'btnPrint = TryCast(e.Row.FindControl("btnPrint"), Button)
            'btnPrint.Attributes("rowno") = hdnBatchId.Value
            'Find the DropDownList in the Row
            Dim drpWaiverType As DropDownList = CType(e.Row.FindControl("drpWaiverType"), DropDownList)
            Dim hdnJobState As HiddenField = CType(e.Row.FindControl("hdnJobState"), HiddenField)
            Dim WaiverPayments As TextBox = CType(e.Row.FindControl("WaiverPayments"), TextBox)
            Dim WaiverDates As TextBox = CType(e.Row.FindControl("WaiverDates"), TextBox)
            Dim InvoicePaymentNo As TextBox = CType(e.Row.FindControl("InvoicePaymentNo"), TextBox)

            If Not hdnJobState Is Nothing Then
                Dim JobState As String
                JobState = hdnJobState.Value
                Dim myforms As HDS.DAL.COMMON.TableView
                myforms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverStateForms(hdnJobState.Value)
                Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateForms
                Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
                drpWaiverType.Items.Clear()
                myforms.MoveFirst()
                Do Until myforms.EOF
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    myform = myforms.FillEntity(myform)
                    LI.Value = myform.Id
                    LI.Text = myform.FormCode & "-" & myform.Description
                    drpWaiverType.Items.Add(LI)
                    If LI.Text.Contains("-Conditional Progress Payment") Then
                        drpWaiverType.SelectedValue = myform.Id
                    End If
                    myforms.MoveNext()

                Loop

                If Not IsNothing(WaiverPayments) And Not IsNothing(WaiverDates) And JobState <> "CA" Then
                    WaiverDates.Visible = False
                    WaiverPayments.Visible = False
                End If

                If Not IsNothing(InvoicePaymentNo) AndAlso JobState <> "NV" Then
                    InvoicePaymentNo.Visible = False
                End If
                ' drpWaiverType.DataSource = objList
                ' drpWaiverType.DataBind()
            End If

            Dim drpMailTo As DropDownList = CType(e.Row.FindControl("drpMailTo"), DropDownList)
            Dim drpPayor As DropDownList = CType(e.Row.FindControl("drpPayor"), DropDownList)
            Dim hdnJobId As HiddenField = CType(e.Row.FindControl("hdnJobId"), HiddenField)
            drpMailTo.Items.Clear()
            drpPayor.Items.Clear()
            'Added By jaywanti 01-06-2016 to bind MailTo & Payor dropdown with seperate SP.
            If Not hdnJobId Is Nothing Then
                Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
                obj.JobId = hdnJobId.Value
                Dim myview As HDS.DAL.COMMON.TableView
                myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
                If Not myview Is Nothing Then
                    Dim dtjobinfo As System.Data.DataTable
                    dtjobinfo = myview.ToTable()
                    If dtjobinfo.Rows.Count > 0 Then
                        Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
                        For count = 0 To dtjobinfo.Rows.Count - 1
                            Dim LI2 As New System.Web.UI.WebControls.ListItem
                            LI2.Value = dtjobinfo.Rows(count)("Id") & "-" & dtjobinfo.Rows(count)("TypeCode")
                            LI2.Text = dtjobinfo.Rows(count)("TypeCode") & "-" & dtjobinfo.Rows(count)("AddressName")
                            drpMailTo.Items.Add(LI2)
                            drpPayor.Items.Add(LI2)
                        Next
                    Else
                        Dim LI1 As New System.Web.UI.WebControls.ListItem
                        LI1.Value = "0"
                        LI1.Text = "--Select--"
                        'objList.Add(LI1)
                        drpMailTo.Items.Add(LI1)
                        drpPayor.Items.Add(LI1)
                    End If
                    'drpMailTo.DataSource = objList
                    'drpMailTo.DataBind()
                    'drpPayor.DataSource = objList
                    'drpPayor.DataBind()
                End If
            End If
            'If Not hdnJobId Is Nothing Then
            '    Dim dsjobinfo As System.Data.DataSet
            '    dsjobinfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetJobByInfo(hdnJobId.Value)
            '    If Not dsjobinfo Is Nothing Then
            '        If dsjobinfo.Tables(3).Rows.Count > 0 Then
            '            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            '            For count = 0 To dsjobinfo.Tables(3).Rows.Count - 1
            '                Dim LI2 As New System.Web.UI.WebControls.ListItem
            '                LI2.Value = dsjobinfo.Tables(3).Rows(count)("Id")
            '                LI2.Text = dsjobinfo.Tables(3).Rows(count)("TypeCode") & "-" & dsjobinfo.Tables(3).Rows(count)("AddressName")
            '                drpMailTo.Items.Add(LI2)
            '                drpPayor.Items.Add(LI2)
            '            Next
            '        Else
            '            Dim LI1 As New System.Web.UI.WebControls.ListItem
            '            LI1.Value = "0"
            '            LI1.Text = "--Select--"
            '            'objList.Add(LI1)
            '            drpMailTo.Items.Add(LI1)
            '            drpPayor.Items.Add(LI1)
            '        End If
            '        'drpMailTo.DataSource = objList
            '        'drpMailTo.DataBind()
            '        'drpPayor.DataSource = objList
            '        'drpPayor.DataBind()
            '    End If
            'End If
            'If Not hdnJobId Is Nothing Then
            '    Dim dsjobinfo As System.Data.DataTable
            '    dsjobinfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobLegalParties(hdnJobId.Value)
            '    If Not dsjobinfo Is Nothing Then
            '        Dim count As Integer = 0

            '        If Not dsjobinfo Is Nothing Then
            '            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            '            If dsjobinfo.Rows.Count > 0 Then
            '                For count = 0 To dsjobinfo.Rows.Count
            '                    Dim LI2 As New System.Web.UI.WebControls.ListItem
            '                    LI2.Value = dsjobinfo.Rows(count)("Id")
            '                    LI2.Text = dsjobinfo.Rows(count)("AddressName")
            '                    drpMailTo.Items.Add(LI2)
            '                    drpPayor.Items.Add(LI2)
            '                Next

            '            Else
            '                Dim LI1 As New System.Web.UI.WebControls.ListItem
            '                LI1.Value = "0"
            '                LI1.Text = "--Select--"
            '                'objList.Add(LI1)
            '                drpMailTo.Items.Add(LI1)
            '                drpPayor.Items.Add(LI1)
            '            End If

            '            'drpMailTo.DataSource = objList
            '            'drpMailTo.DataBind()
            '            'drpPayor.DataSource = objList
            '            'drpPayor.DataBind()
            '        End If
            '    End If
            'End If




            'If Not hdnJobId Is Nothing Then
            '    Dim dsjobinfo As System.Data.DataTable
            '    dsjobinfo = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobLegalParties(hdnJobId.Value)
            '    If Not dsjobinfo Is Nothing Then
            '        Dim count As Integer = 0

            '        If Not dsjobinfo Is Nothing Then
            '            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            '            If dsjobinfo.Rows.Count > 0 Then
            '                For count = 0 To dsjobinfo.Rows.Count
            '                    Dim LILegalParties As New System.Web.UI.WebControls.ListItem
            '                    LILegalParties.Value = dsjobinfo.Rows(count)("Id")
            '                    LILegalParties.Text = dsjobinfo.Rows(count)("TYPECODE") & " - " & dsjobinfo.Rows(count)("AddressName")
            '                    drpMailTo.Items.Add(LILegalParties)
            '                    drpPayor.Items.Add(LILegalParties)
            '                Next

            '            Else
            '                Dim LILegalParties As New System.Web.UI.WebControls.ListItem
            '                LILegalParties.Value = "0"
            '                LILegalParties.Text = "--Select--"
            '                drpMailTo.Items.Add(LILegalParties)
            '                drpPayor.Items.Add(LILegalParties)
            '            End If
            '        End If
            '    End If
            'End If
            Dim drpSignor As DropDownList = CType(e.Row.FindControl("drpSignor"), DropDownList)
            VWSIGNERS.MoveFirst()
            drpSignor.Items.Clear()
            Dim liSig As New ListItem
            Do Until VWSIGNERS.EOF
                liSig = New ListItem
                Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
                csg = VWSIGNERS.FillEntity(csg)
                liSig.Value = csg.Id
                liSig.Text = csg.Signer & "," & csg.SignerTitle
                drpSignor.Items.Add(liSig)
                VWSIGNERS.MoveNext()
            Loop

            Dim txtfrom As TextBox = CType(e.Row.FindControl("txtfrom"), TextBox)
            Dim chkJointCkeck As CheckBox = CType(e.Row.FindControl("chkJointCkeck"), CheckBox)
            Dim chkPaidInFull As CheckBox = CType(e.Row.FindControl("chkPaidInFull"), CheckBox)
            Dim btnPrints As Button = CType(e.Row.FindControl("btnPrint"), Button)
            Dim txtPaymentAmount As TextBox = CType(e.Row.FindControl("txtPaymentAmount"), TextBox)
            Dim txtthroughdate As TextBox = CType(e.Row.FindControl("txtthroughdate"), TextBox)
            Dim txtNote As TextBox = CType(e.Row.FindControl("txtNote"), TextBox)
            Dim txtEmail As TextBox = CType(e.Row.FindControl("txtEmail"), TextBox)
            Dim btnSaveWaivers As Button = CType(e.Row.FindControl("btnSaveWaiver"), Button)

            Dim i As Integer = e.Row.RowIndex * 13 ' here 8 is number of control

            drpWaiverType.Attributes.Add("tabindex", (i + 1).ToString())
            txtPaymentAmount.Attributes.Add("tabindex", (i + 2).ToString())
            chkPaidInFull.InputAttributes.Add("tabindex", (i + 3).ToString())
            txtfrom.Attributes.Add("tabindex", (i + 4).ToString())
            txtthroughdate.Attributes.Add("tabindex", (i + 5).ToString())
            drpMailTo.Attributes.Add("tabindex", (i + 6).ToString())
            drpPayor.Attributes.Add("tabindex", (i + 7).ToString())
            drpSignor.Attributes.Add("tabindex", (i + 8).ToString())
            txtNote.Attributes.Add("tabindex", (i + 9).ToString())
            chkJointCkeck.InputAttributes.Add("tabindex", (i + 10).ToString())
            txtEmail.Attributes.Add("tabindex", (i + 11).ToString())
            btnPrints.Attributes.Add("tabindex", (i + 12).ToString())
            btnSaveWaivers.Attributes.Add("tabindex", (i + 13).ToString())
        End If

    End Sub

    Protected Sub btnPrint_Command(sender As Object, e As CommandEventArgs)
        If (e.CommandName.Equals("Print")) Then
            Try

                Dim btn As Button = DirectCast(sender, Button) 'Added by Jaywanti 11-05-2016
                Dim myitemid As String = btn.Attributes("rowno") '8616 'e.CommandArgument.ToString()
                If myitemid = 0 Then
                    myitemid = -1
                End If
                myreq.JobWaiverLogId = myitemid

                ' Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.GetNewJobAck(Me.CurrentUser.Id, myitemid, True)

                PrintWaiver(myitemid)
            Catch ex As Exception

            End Try
        ElseIf (e.CommandName.Equals("Email")) Then
            Try


                Dim btnEmailWaiver As Button = DirectCast(sender, Button) 'Added by Jaywanti 11-05-2016
                Dim myitemid As String = btnEmailWaiver.Attributes("rowno") '8616 'e.CommandArgument.ToString()
                If myitemid = 0 Then
                    myitemid = -1
                End If

                Me.hdnJobWaiverLogId.Value = myitemid
                Dim txtEmail As TextBox = CType(btnEmailWaiver.Parent.FindControl("txtEmail"), TextBox)
                If Not txtEmail Is Nothing Then
                    Me.txtToList.Text = txtEmail.Text
                End If
                If Me.gvwJobs.Rows.Count > 0 Then
                    Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
                Dim lblJobAddress As String = CType(btnEmailWaiver.Parent.FindControl("lblJobAddress"), Label).Text + " " +
                CType(btnEmailWaiver.Parent.FindControl("lblJobCity"), Label).Text + " " + CType(btnEmailWaiver.Parent.FindControl("lblJobState"), Label).Text
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JobAddress", "fnJobAddress('" + lblJobAddress + "');", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsRebind", "CallSuccessFunc();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsEmailPopup", "Successpopup();", True)
                'EmailWaiver(myitemid)
                'Comment by Jaywanti 22-07-2016

                'Dim txt As String = gv
                'If Not txtEmail Is Nothing Then
                '    If txtEmail.Text <> "" Then
                '        EmailWaiver(myitemid)
                '    End If
                'End If
            Catch ex As Exception

            End Try
        ElseIf (e.CommandName.Equals("Save")) Then
            Try
                Me.hdnJobWaiverLogId.Value = ""
                Me.hdnCustEmail.Value = ""
                Dim btn As Button = DirectCast(sender, Button) 'Added by Jaywanti 11-05-2016
                Dim jobwaiverlogid As String = btn.Attributes("rowno") '8616 'e.CommandArgument.ToString()
                Me.hdnJobWaiverLogId.Value = jobwaiverlogid
                btnSaveWaiver = TryCast(sender, Button)
                Dim myitemid As String = e.CommandArgument.ToString()
                Dim drpWaiverType As DropDownList = CType(btnSaveWaiver.Parent.FindControl("drpWaiverType"), DropDownList)
                Dim txtFrom As TextBox = CType(btnSaveWaiver.Parent.FindControl("txtFrom"), TextBox)
                Dim drpMailTo As DropDownList = CType(btnSaveWaiver.Parent.FindControl("drpMailTo"), DropDownList)
                Dim drpSignor As DropDownList = CType(btnSaveWaiver.Parent.FindControl("drpSignor"), DropDownList)
                Dim chkJointCkeck As CheckBox = CType(btnSaveWaiver.Parent.FindControl("chkJointCkeck"), CheckBox)
                Dim btnPrint As Button = CType(btnSaveWaiver.Parent.FindControl("btnPrint"), Button)
                Dim btnEmail As Button = CType(btnSaveWaiver.Parent.FindControl("btnEmail"), Button)
                Dim txtPaymentAmount As TextBox = CType(btnSaveWaiver.Parent.FindControl("txtPaymentAmount"), TextBox)
                Dim chkPaidInFull As CheckBox = CType(btnSaveWaiver.Parent.FindControl("chkPaidInFull"), CheckBox)
                Dim NotarywithCA As CheckBox = CType(btnSaveWaiver.Parent.FindControl("NotarywithCA"), CheckBox)
                Dim NotarywithoutCA As CheckBox = CType(btnSaveWaiver.Parent.FindControl("NotarywithoutCA"), CheckBox)
                Dim txtthroughdate As TextBox = CType(btnSaveWaiver.Parent.FindControl("txtthroughdate"), TextBox)
                Dim drpPayor As DropDownList = CType(btnSaveWaiver.Parent.FindControl("drpPayor"), DropDownList)
                Dim txtNote As TextBox = CType(btnSaveWaiver.Parent.FindControl("txtNote"), TextBox)
                Dim txtEmail As TextBox = CType(btnSaveWaiver.Parent.FindControl("txtEmail"), TextBox)

                Dim WaiverPayments As TextBox = CType(btnSaveWaiver.Parent.FindControl("WaiverPayments"), TextBox)
                Dim WaiverDates As TextBox = CType(btnSaveWaiver.Parent.FindControl("WaiverDates"), TextBox)
                Dim InvoicePaymentNo As TextBox = CType(btnSaveWaiver.Parent.FindControl("InvoicePaymentNo"), TextBox)

                Dim lblJobAddress As String = CType(btnSaveWaiver.Parent.FindControl("lblJobAddress"), Label).Text + " " +
                   CType(btnSaveWaiver.Parent.FindControl("lblJobCity"), Label).Text + " " + CType(btnSaveWaiver.Parent.FindControl("lblJobState"), Label).Text
                Dim hdnJobId As HiddenField = CType(btnSaveWaiver.Parent.FindControl("hdnJobId"), HiddenField)


                With myreq
                    .JobId = myitemid
                    .FormId = drpWaiverType.SelectedValue
                    '.FormCode = Strings.Left(Me.FormId.SelectedItem.Text, 20) 'Commented By Jaywanti
                    .FormCode = Strings.Left(drpWaiverType.SelectedItem.Text, 50)
                    .PaymentAmt = txtPaymentAmount.Text.Replace("$", "")
                    .JointCheckAgreement = chkJointCkeck.Checked
                    '.Notary = Me.Notary.Checked
                    .StartDate = Nothing
                    .ThroughDate = Nothing
                    If Not txtFrom.Text = "" Then
                        If IsDate(txtFrom.Text) Then
                            .StartDate = Utils.GetDate(txtFrom.Text)
                        End If
                    End If
                    If Not txtthroughdate.Text = "" Then
                        If IsDate(txtthroughdate.Text) Then
                            .ThroughDate = Utils.GetDate(txtthroughdate.Text)
                        End If
                    End If

                    If (NotarywithCA.Checked = True Or NotarywithoutCA.Checked = True) Then
                        .Notary = True
                        If (NotarywithoutCA.Checked = True And NotarywithCA.Checked = False) Then
                            .NotaryType = 1
                        ElseIf (NotarywithCA.Checked = True And NotarywithoutCA.Checked = False) Then
                            .NotaryType = 2
                        End If
                    Else
                        .Notary = False
                    End If

                    .RequestedBy = Left(Me.UserInfo.UserInfo.LoginCode, 10)
                    .RequestedByUserId = Me.CurrentUser.Id
                    If drpSignor.SelectedIndex >= 0 Then
                        .SignerId = drpSignor.SelectedValue
                    End If

                    .WaiverNote = txtNote.Text
                    .IsMarkPIF = chkPaidInFull.Checked
                    .JobWaiverLogId = jobwaiverlogid
                    .Payor = drpPayor.SelectedItem.Text
                    .EmailTo = txtEmail.Text

                    .WaiverDates = WaiverDates.Text
                    .WaiverPayments = WaiverPayments.Text
                    .InvoicePaymentNo = InvoicePaymentNo.Text

                End With
                If Not hdnJobId Is Nothing Then
                    Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
                    obj.JobId = hdnJobId.Value
                    Dim myview As HDS.DAL.COMMON.TableView
                    myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
                    If Not myview Is Nothing Then
                        Dim view As New DataView
                        Dim dtjobinfo As System.Data.DataTable
                        dtjobinfo = myview.ToTable()
                        view.Table = dtjobinfo
                        Dim strquery As String = ""
                        If dtjobinfo.Rows.Count > 0 Then
                            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
                            If drpMailTo.SelectedValue <> "" Then
                                Dim str As String()
                                str = drpMailTo.SelectedValue().ToString().Split("-")
                                For count = 0 To str.Length - 1
                                    If (count = 0) Then
                                        strquery = "Id=" + str(count).Trim()
                                    ElseIf (count = 1) Then
                                        strquery = strquery + " AND TypeCode='" + str(count).Trim() + "'"
                                    End If
                                Next
                                view.RowFilter = strquery
                                dtjobinfo = view.ToTable()
                            End If
                            If (dtjobinfo.Rows.Count > 0) Then
                                myreq.MailToName = dtjobinfo.Rows(0)("AddressName").ToString()
                                myreq.MailToAddr1 = dtjobinfo.Rows(0)("AddressLine1").ToString()
                                myreq.MailToCity = dtjobinfo.Rows(0)("City").ToString()
                                myreq.MailToState = dtjobinfo.Rows(0)("State").ToString()
                                myreq.MailToZip = dtjobinfo.Rows(0)("PostalCode").ToString()
                            End If
                        End If
                    End If
                End If
                'myreq.DatePrinted = Today 'Commented by Shishir 24-05-2016
                myreq.DateCreated = Today
                If jobwaiverlogid > 0 Then
                    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
                Else
                    CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Create(myreq)
                End If
                btnPrint.Attributes("rowno") = myreq.JobWaiverLogId 'Added by Jaywanti 11-05-2016
                btnEmail.Attributes("rowno") = myreq.JobWaiverLogId 'Added by Jaywanti 11-05-2016
                btnSaveWaiver.Attributes("rowno") = myreq.JobWaiverLogId 'Added by Jaywanti 11-05-2016
                Me.hdnJobWaiverLogId.Value = myreq.JobWaiverLogId 'Added by Jaywanti 11-05-2016
                Me.hdnCustEmail.Value = txtEmail.Text
                Me.txtToList.Text = txtEmail.Text 'Added by Jaywanti 22-07-2016

                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myreq.JobId, myjob)

                'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(Me.FormId.SelectedValue, myform) 'Commented by Jaywanti
                CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(drpWaiverType.SelectedValue, myform)
                If myform.IsPIFPrompt = True And chkPaidInFull.Checked = True And myjob.StatusCodeId <> "60" Then
                    Dim mynote As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.JobHistory
                    With mynote
                        .JobId = myjob.Id
                        .DateCreated = Now()
                        .Note += "Job Changed from Active to Paid In Full"
                        .Note += vbCrLf
                        .UserId = Strings.Left(Me.UserInfo.UserInfo.UserName, 10)
                        .ClientView = True
                        .EnteredByUserId = Me.UserInfo.UserInfo.ID
                        .ActionTypeId = 0
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mynote)
                    End With
                    With myjob
                        .StatusCodeId = 60
                        CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Update(myjob)
                    End With

                End If
                'drpWaiverType.Enabled = False
                'txtFrom.Enabled = False
                'drpMailTo.Enabled = False
                'drpSignor.Enabled = False
                'chkJointCkeck.Enabled = False
                'txtPaymentAmount.Enabled = False
                'chkPaidInFull.Enabled = False
                'txtthroughdate.Enabled = False
                'drpPayor.Enabled = False
                'txtNote.Enabled = False
                'txtEmail.Enabled = False
                'btnSaveWaiver.Enabled = False
                btnPrint.Enabled = True
                btnPrintAll.Visible = True
                btnEmail.Enabled = True
                'Added by Shishir 23-05-2016
                btnSaveWaiver.Text = "Update"
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "JobAddress", "fnJobAddress('" + lblJobAddress + "');", True)
                If Me.gvwJobs.Rows.Count > 0 Then
                    Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "jsAlertSuccessFunc", "AlertSuccessFunc();", True)
                'ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Saved", "CallSaveWaiver();", True)
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
            Catch ex As Exception

            End Try
        End If


    End Sub
    Public Shared Function GetSettings() As TABLES.Report_Settings
        Dim myitem As New TABLES.Report_Settings
        ProviderBase.DAL.Read(1, myitem)

        Return myitem

    End Function
    Public Shared Sub EmailToUser(ByVal email As String, ByVal subject As String, ByVal strBody As String, ByVal FileName As String, ByVal pdfcontent As Byte())


        Dim ToArrayList As String()
        ToArrayList = email.Split(";")

        Dim MYSITE As TABLES.Report_Settings = GetSettings()

        Dim msg As New MailMessage
        '  msg.From = New MailAddress("shishir.peeru@rhealtech.com", "ClientView")
        msg.From = New MailAddress("CRFIT@crfsolutions.com", "Job Waiver Central")
        msg.Subject = subject
        For index = 0 To ToArrayList.Length - 1
            msg.To.Add(ToArrayList(index))
        Next
        msg.IsBodyHtml = True
        msg.Body = strBody
        msg.Attachments.Add(New Attachment(New MemoryStream(pdfcontent), FileName))

        'msg.Body = body

        Try
            With MYSITE
                Dim _smtp As New System.Net.Mail.SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
                _smtp.Dispose()
            End With
        Catch ex As Exception
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            MsgBox("error on email:" & vbCrLf & mymsg)

        End Try

    End Sub
    Public Sub PrintWaiver(ByVal JobWaiverLogId As String)
        Try

            Dim count As Integer
            count = 0
            Dim settings As New PrinterSettings()
            Dim Pname As String = settings.PrinterName
            Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(JobWaiverLogId)
            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobWaiverLogId, myreq)

            Me.btnDownLoad.Visible = False
            If IsNothing(sreport) = False Then
                If (myreq.Notary = True) Then
                    PrintNotary(JobWaiverLogId)
                    MergeNotary(sreport)
                End If
                Me.btnDownLoad.Visible = True
                Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
                Me.plcReportViewer.Controls.Clear()
                Dim mylit As New Literal
                mylit = HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no")
                If Not (mylit.Text Is Nothing) Then
                    Dim IframeTag As String
                    IframeTag = mylit.Text
                    Dim Filepath As String = ""
                    Filepath = Me.MyPage.GetFileNameFromIframe(IframeTag)
                    If (Filepath <> "") Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "OpenPdfWindow", "OpenPdfWindow('" + Filepath + "')", True)
                    End If
                End If
                If Me.gvwJobs.Rows.Count > 0 Then
                    Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
                'Me.plcReportViewer.Controls.Add(HDS.WEBLIB.Common.Utils.GetIFrame("pdfvw4", Me.ResolveUrl(s), 600, 600, "no"))
                'Me.MultiView1.SetActiveView(Me.View3) 'added by Jaywanti on 06-01-2016
                Dim strpath As String = Server.MapPath(s)
                PDFLIB.PrintPDF(Pname, strpath)
                count = 1
                'CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(JobWaiverLogId, myreq)
                myreq.DatePrinted = Today
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
            Else
                Me.plcReportViewer.Controls.Clear()
                Me.plcReportViewer.Controls.Add(Me.NoRecordsFound)
                Me.MultiView1.SetActiveView(Me.View3) 'added by Jaywanti on 06-01-2016
            End If
            If (count > 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "printAllSendPopup", "printAllSendPopup();", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NoprintAllSendPopup", "NoprintAllSendPopup();", True)
            End If
            If Me.gvwJobs.Rows.Count > 0 Then
                Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
            End If
        Catch ex As Exception
            Dim mymsg As String = ex.Message

        End Try
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub

    Public Sub PrintNotary(ByVal JobWaiverLogId As String)
        Dim sreport As String = LienView.Provider.PrintJobNotary(JobWaiverLogId)

        If IsNothing(sreport) = False Then
            notary = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
        End If

    End Sub

    Public Sub MergeNotary(ByVal sreport As String)
        Dim s2 As String = Server.MapPath("~/UserData/Output/" & (System.IO.Path.GetFileName(sreport)))
        Dim path As String = ConfigurationManager.AppSettings("path").ToString()
        File.Create(path & (System.IO.Path.GetFileName(sreport))).Close()

        Dim result As String = path & (System.IO.Path.GetFileName(sreport))
        Dim source_files() As String = {s2, notary}
        Dim document As iTextSharp.text.Document = New iTextSharp.text.Document
        Dim copy As iTextSharp.text.pdf.PdfCopy = New iTextSharp.text.pdf.PdfCopy(document, New FileStream(result, FileMode.Create))
        'open the document
        document.Open()
        Dim reader As iTextSharp.text.pdf.PdfReader
        Dim i As Integer = 0
        Do While (i < source_files.Length)
            'create PdfReader object
            reader = New iTextSharp.text.pdf.PdfReader(source_files(i))
            'merge combine pages
            Dim page As Integer = 1
            Do While (page <= reader.NumberOfPages)
                copy.AddPage(copy.GetImportedPage(reader, page))
                page = (page + 1)
            Loop

            i = (i + 1)
        Loop

        'close the document object
        document.Close()
        '-------------------------------------------------------------
        File.Copy(result, sreport, True)
        File.Delete(result)

    End Sub

    Public Sub EmailWaiver(ByVal JobWaiverLogId As String)
        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(JobWaiverLogId)
        If IsNothing(sreport) = False Then
            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
            Dim fileArray As Byte() = System.IO.File.ReadAllBytes(Server.MapPath(s))
            Dim Filename As String = System.IO.Path.GetFileName(sreport)
            EmailToUser(txtToList.Text, txtSubject.Text, txtBody.Text, Filename, fileArray) 'For a timing
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "EmailSendPopup", "EmailSendPopup();", True)
        End If
        If Me.gvwJobs.Rows.Count > 0 Then
            Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
        End If
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
    End Sub

    Protected Sub btnPrintWaiver_Click(sender As Object, e As EventArgs)
        PrintWaiver(Me.hdnJobWaiverLogId.Value)

    End Sub

    Protected Sub btnEmailWaiver_Click(sender As Object, e As EventArgs)

        If Me.txtToList.Text.Trim() <> "" Then
            EmailWaiver(Me.hdnJobWaiverLogId.Value)
        End If
    End Sub

    'Added by Shishir 26-05-2016 
    'This event will loop through records in Grid and filter those records which contains rowno attribute in Submit button.
    'Then check whether that particular record is already printed or not.If not will print those documents.
    Protected Sub btnPrintAll_Click(sender As Object, e As EventArgs)
        Try
            'Dim array_list = New ArrayList()
            Dim pd As New System.Drawing.Printing.PrintDocument
            Dim settings As New PrinterSettings()
            Dim count As Integer = 0
            Dim Pname As String = settings.PrinterName
            Dim rowscount As Integer = gvwJobs.Rows.Count()
            For Each row As GridViewRow In gvwJobs.Rows()
                Dim btn As Button = TryCast(DirectCast(row.FindControl("btnSaveWaiver"), Button), Button)
                If (Not btn.Attributes("rowno") Is Nothing) Then
                    Dim myitemid As String = btn.Attributes("rowno")
                    myreq.JobWaiverLogId = myitemid
                    CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Read(myreq.JobWaiverLogId, myreq)
                    If myreq.DatePrinted = DateTime.MinValue Then
                        Dim sreport As String = CRF.CLIENTVIEW.BLL.LienView.Provider.PrintJobWaiver(myitemid)
                        If IsNothing(sreport) = False Then
                            Dim s As String = "~/UserData/Output/" & (System.IO.Path.GetFileName(sreport))
                            Dim strpath As String = Server.MapPath(s)
                            'For Each item As String In array_list
                            PDFLIB.PrintPDF(Pname, strpath)
                            'Next
                            myreq.DatePrinted = Today
                            CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Update(myreq)
                            count = count + 1
                            'array_list.Add(strpath)
                        End If
                    End If
                End If

                If Me.gvwJobs.Rows.Count > 0 Then
                    Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader
                End If
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
            Next
            If (count > 0) Then
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "printAllSendPopup", "printAllSendPopup();", True)
            Else
                ScriptManager.RegisterStartupScript(Me, Me.GetType(), "NoprintAllSendPopup", "NoprintAllSendPopup();", True)
            End If


        Catch ex As Exception
            ScriptManager.RegisterStartupScript(Me, [GetType](), "showException", "alert('Kindly install Acrobat Reader and ensure that it is set as a default document reader');", True)
        End Try
    End Sub

    'Added by Shishir 10-06-2016
    Protected Sub drpSortBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles drpSortBy.SelectedIndexChanged
        Try
            'ScriptManager.RegisterStartupScript(Me, [GetType](), "SelectSortBy", "alert('DropDown value changed');", True)
            'Session("JobList") = Nothing
            'If Not String.IsNullOrEmpty(TryCast(Me.Session("JobList"), String)) Then
            If Not Me.Session("JobList") Is Nothing Then
                myviewSorting = Session("JobList")
                'ElseIf String.IsNullOrEmpty(TryCast(Me.Session("JobList"), String)) Then
            Else
                Dim mysproc As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
                Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
                obj.Page = 1
                obj.PageRecords = 10000 '500 Changed by jaywanti
                obj.UserId = Me.CurrentUser.Id ' here hardoded value is there i.e 5
                myviewSorting = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)
                Me.Session("JobList") = myviewSorting
            End If


            Dim SortBy As String
            If drpSortBy.SelectedItem.Value = "-1" Then
                SortBy = "ListId ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "1" Then
                SortBy = "ClientCustomer ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "2" Then
                SortBy = "CustRefNum ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "3" Then
                SortBy = "JobNum ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "4" Then
                SortBy = "JobAdd1 ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "5" Then
                SortBy = "JobName ASC"
                myviewSorting.Sort = SortBy
            ElseIf drpSortBy.SelectedItem.Value = "6" Then
                SortBy = "DateAssigned ASC"
                myviewSorting.Sort = SortBy
            End If
            'If myview.Sort = Me.cboSortColumns.Text Then
            '    mdvCust.Sort = Me.cboSortColumns.Text & " DESC"
            'Else
            '    mdvCust.Sort = Me.cboSortColumns.Text
            'End If
            Me.gvwJobs.DataSource = myviewSorting
            Me.gvwJobs.DataBind()
            If gvwJobs.Rows.Count > 0 Then
                Me.gvwJobs.HeaderRow.TableSection = TableRowSection.TableHeader

            End If
            Me.MultiView1.SetActiveView(Me.View2)

            'Me.Session("joblist") = myview
            ScriptManager.RegisterStartupScript(Me, Me.GetType(), "js2", "CallSuccessFunc();", True)
        Catch ex As Exception

        End Try
    End Sub


</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
        <div id="modcontainer" style="width: 100%;" class="margin-bottom-0">
            <asp:MultiView ID="MultiView1" runat="server">
                <asp:View ID="View1" runat="server">
                    <h1 class="panelheader">Job Filter</h1>
                    <div class="body" id="JobFilterBody">
                        <div class="form-horizontal">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CustomerName" TabIndex="1" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="CustomerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerNameLike(1)" TabIndex="2" />
                                        <asp:RadioButton ID="CustomerNameLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerNameLike(2)" TabIndex="3" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer Ref:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="CustomerRef" TabIndex="4" CssClass="form-control" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="CustomerRefLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionCustomerRefLike(1)" TabIndex="5" />
                                        <asp:RadioButton ID="CustomerRefLike_2" runat="server" Text="Includes" onchange="myFunctionCustomerRefLike(2)" TabIndex="6" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Owner Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="OwnerName" CssClass="form-control" meta:resourcekey="OwnerNameResource1" TabIndex="7" MaxLength="45" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="OwnerNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionOwnerNameLike(1)" TabIndex="8" />
                                        <asp:RadioButton ID="OwnerNameLike_2" runat="server" Text="Includes" onchange="myFunctionOwnerNameLike(2)" TabIndex="9" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Contractor Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="GCName" CssClass="form-control" meta:resourcekey="GCNameResource1" MaxLength="45" TabIndex="10" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="GCNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionGCNameLike(1)" TabIndex="11" />
                                        <asp:RadioButton ID="GCNameLike_2" runat="server" Text="Includes" onchange="myFunctionGCNameLike(2)" TabIndex="12" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Lender Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="LenderName" CssClass="form-control" meta:resourcekey="LenderNameResource1" MaxLength="45" TabIndex="13" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="LenderNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionLenderNameLike(1)" TabIndex="14" />
                                        <asp:RadioButton ID="LenderNameLike_2" runat="server" Text="Includes" onchange="myFunctionLenderNameLike(2)" TabIndex="15" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Job Name:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobName" CssClass="form-control" meta:resourcekey="JobNameResource1" MaxLength="45" TabIndex="16" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">

                                        <asp:RadioButton ID="JobNameLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNameLike(1)" TabIndex="17" />
                                        <asp:RadioButton ID="JobNameLike_2" runat="server" Text="Includes" onchange="myFunctionJobNameLike(2)" TabIndex="18" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Job Address:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobAddress" CssClass="form-control" meta:resourcekey="JobAddressResource1" MaxLength="45" TabIndex="19" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="JobAddressLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobAddressLike(1)" TabIndex="20" />
                                        <asp:RadioButton ID="JobAddressLike_2" runat="server" Text="Includes" onchange="myFunctionJobAddressLike(2)" TabIndex="21" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Job #:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobNo" CssClass="form-control" meta:resourcekey="JobNoResource1" MaxLength="45" TabIndex="22" />
                                </div>
                                <div class="col-md-4" style="padding-left: 0px;">
                                    <div class="radio-custom radio-default radio-inline">
                                        <asp:RadioButton ID="JobNoLike_1" runat="server" Checked="true" Text="Starts" onchange="myFunctionJobNoLike(1)" TabIndex="23" />
                                        <asp:RadioButton ID="JobNoLike_2" runat="server" Text="Includes" onchange="myFunctionJobNoLike(2)" TabIndex="24" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Assigned Date:</label>
                                <div class="col-md-5" style="display: flex;">
                                    <SiteControls:Calendar ID="Calendar1" runat="server" ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" Value="TODAY" TabIndex="25" />
                                    &nbsp;&nbsp;Thru&nbsp;
                                                                          <SiteControls:Calendar ID="Calendar2" runat="server" ErrorMessage="From Date" IsReadOnly="false" IsRequired="true" Value="TODAY" TabIndex="26" />
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkByAssignDate" CssClass="redcaption" runat="server" Text=" Use Assigned Date Range" TabIndex="27" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Job City:</label>
                                <div class="col-md-5">
                                    <asp:TextBox runat="server" ID="JobCity" CssClass="form-control" MaxLength="20" TabIndex="28" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Job State:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="JobState" CssClass="form-control" Width="70px" Style="text-align: center" meta:resourcekey="JobStateResource1" MaxLength="2" TabIndex="29" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Branch #:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="BranchNo" CssClass="form-control" meta:resourcekey="BranchNoResource1" MaxLength="10" TabIndex="30" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">PO #:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="PONum" CssClass="form-control" meta:resourcekey="PONoResource1" MaxLength="15" TabIndex="31" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="JobStatus" CssClass="form-control" meta:resourcekey="BranchNoResource1" Width="70px" TabIndex="32" />
                                </div>
                                <div class="col-md-5">
                                    <div class="checkbox-custom checkbox-default" style="min-height: 0; padding-top: 0;">
                                        <asp:CheckBox ID="chkPaidInFull" CssClass="redcaption" runat="server" Text="  Exclude Paid In Full Jobs" TabIndex="33" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">CRF Temp Id #:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="TempId" CssClass="form-control" meta:resourcekey="FileNumberResource1" TabIndex="34" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">CRF File#:</label>
                                <div class="col-md-2">
                                    <asp:TextBox runat="server" ID="FileNumber" CssClass="form-control" meta:resourcekey="FileNumberResource1" TabIndex="35" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 col-sm-3 col-xs-3 control-label align-lable"></label>
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <div class="checkbox-custom checkbox-default">
                                        <asp:CheckBox ID="chkAllJobs" CssClass="redcaption" runat="server" Text="Include Archived Jobs" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="footer">
                            <asp:Panel ID="Panel1" DefaultButton="btnSubmit" runat="server">
                                <asp:Button ID="btnSubmit" runat="server" Text="View Filter" CssClass="btn btn-primary" CausesValidation="False" Style="margin-top: 5px;" />&nbsp;
                                <asp:Button ID="btnViewAll" runat="server" Text="View All Jobs" CssClass="btn btn-primary" CausesValidation="False" Style="margin-top: 5px; display: none;" />
                                <asp:Label ID="Message" CssClass="normalred" runat="server"></asp:Label>&nbsp;
                            </asp:Panel>
                        </div>
                    </div>
                </asp:View>
                <asp:View ID="View2" runat="server">
                    <h1 class="panelheader">Bulk Waiver</h1>
                    <div class="body">
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-md-6" style="text-align: left;">
                                    <asp:LinkButton ID="btnBackFilter" runat="server" CssClass="btn btn-primary" Text="Back to Filter" CausesValidation="False"></asp:LinkButton>
                                    <asp:DropDownList ID="drpSortBy" runat="server" Style="height: 32px; padding-left: 20px; padding: 6px 15px; color: #76838f; border: 1px solid #e4eaec; border-radius: 3px;" AutoPostBack="true" OnSelectedIndexChanged="drpSortBy_SelectedIndexChanged">
                                        <asp:ListItem Enabled="true" Text="-- Sort By --" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="Customer Name" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Cust#" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Job#" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="Job Address" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="Job Name" Value="5"></asp:ListItem>
                                        <asp:ListItem Text="Assigned Date" Value="6"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-6" style="text-align: right; display: none">
                                    <asp:Button ID="btnPrintAll" runat="server" CssClass="btn btn-primary" Visible="false" Text="Print All" CausesValidation="False" OnClick="btnPrintAll_Click"></asp:Button>
                                </div>
                            </div>
                        </div>

                        <div style="width: auto; height: auto; overflow: auto;">
                            <asp:GridView ID="gvwJobs" runat="server" AutoGenerateColumns="False" CssClass="table dataTable table-striped" DataKeyNames="JobId" Width="100%" BorderWidth="1px" Style="font-size: 12px !important;" EmptyDataText="No Matching Records!">
                                <Columns>

                                    <asp:TemplateField HeaderText="">
                                        <%--    <HeaderTemplate>
                                            <table class="row" style="width: 77%;">
                                                <tr>
                                                    <td class="col-md-4" style="font-weight: bold;">Customer Name</td>
                                                    <td class="col-md-4" style="font-weight: bold;">Job#</td>
                                                    <td class="col-md-4" style="font-weight: bold;">Job Name</td>
                                                 
                                                </tr>
                                                <tr>
                                                    <td class="col-md-4" style="font-weight: bold;">Cust#</td>
                                                    <td class="col-md-4" style="font-weight: bold;">Job Address</td>
                                                    <td class="col-md-4" style="font-weight: bold;">AssignedDate</td>
                                                  
                                                </tr>
                                            </table>
                                        </HeaderTemplate>--%>

                                        <ItemTemplate>
                                            <%--     <table class="row">
                                         
                                            </table>--%>
                                            <table class="row">
                                                <tr>
                                                    <td class="col-md-1" style="min-width: 95px; text-align: right; padding-right: 0px;vertical-align:top;"><span style="font-weight: bold;">Customer Name:</span></td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:Label ID="lblCustomerName" runat="server" Text='<%#Eval("ClientCustomer")%>' />
                                                    </td>
                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;vertical-align:top;"><span style="font-weight: bold;">Job#:</span></td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:Label ID="lblJobNum" runat="server" Text='<%#Eval("JobNum")%>' />
                                                    </td>
                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;min-width:90px;vertical-align:top;"><span style="font-weight: bold;">Job Name:</span></td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:Label ID="lblJobName" runat="server" Text='<%#Eval("JobName")%>' />
                                                    </td>

                                                    <%--<td class="col-md-6" colspan="4">
                                                        <a href="Images/ResourceImages/Liens/Notary(Jurat).pdf" target="_blank" title="CA Notary (Jurat)">Print Notary(To be signed in CA)</a>
                                                    </td>--%>

                                                    <td class="col-md-1" style="min-width: 130px; text-align: right; padding-right: 0px;vertical-align:top;">Previous Waiver Dates:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:TextBox ID="WaiverDates" runat="server" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px;" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px; min-width: 130px;">Invoice/Payment App#:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:TextBox ID="InvoicePaymentNo" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px;" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px; min-width: 130px;"></td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;"><span style="font-weight: bold;">Cust#:</span></td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:Label ID="lblCustRefNum" runat="server" Text='<%#Eval("CustRefNum") %>' />
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;min-width: 95px; text-align: right;padding-right: 0px;"><span style="font-weight: bold;">Job Address:</span></td>
                                                    <td class="col-md-1" style="min-width: 200px;vertical-align:top;">
                                                        <asp:Label ID="lblJobAddress" runat="server" Text='<%#Eval("JobAdd1")%>' />&nbsp;&nbsp;
                                                        <asp:Label ID="lblJobCity" runat="server" Text='<%#Eval("JobCity")%>' />&nbsp;&nbsp;
                                                        <asp:Label ID="lblJobState" runat="server" Text='<%#Eval("JobState")%>' />
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;padding-right: 0px;text-align: right;"><span style="font-weight: bold;">Assigned Date:</span></td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:Label ID="lblAssignedDate" runat="server" Text='<%#Utils.FormatDate(Eval("DateAssigned"))%>' />
                                                    </td>
                                                    <%--   <td class="col-md-5" colspan="4">
                                                        <a href="Images/ResourceImages/Liens/Notary(Jurat)1.pdf" target="_blank" title="Notary (Jurat)">Print Notary(To be signed outside of CA)</a>
                                                    </td>--%>

                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px; min-width: 130px;">Unpaid Progress Pmts:</td>
                                                    <td class="col-md-1" style="vertical-align:top;padding-top: 5px;">
                                                        <asp:TextBox ID="WaiverPayments" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px;" runat="server" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">Email:</td>
                                                    <td class="col-md-1" style="vertical-align:top;padding-top: 5px;">
                                                        <asp:TextBox ID="txtEmail" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px;" runat="server" CssClass="form-control" Text=''></asp:TextBox>
                                                    </td>                                                                                                       
                                                    <td colspan="1" class="col-md-1">
                                                        <asp:Button ID="btnEmail" runat="server" CssClass="btn btn-primary" Style="padding-top: 0px; padding-bottom: 0px; margin: 2px;" Enabled="false" Text="Email" Width="80px" CommandName="Email" OnCommand="btnPrint_Command" />
                                                    </td>

                                                </tr>

                                                <tr>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">Waiver Type:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:HiddenField ID="hdnJobState" runat="server" Value='<%#Eval("JobState")%>' />
                                                        <asp:DropDownList ID="drpWaiverType" runat="server" Width="250px"></asp:DropDownList>
                                                        <asp:HiddenField ID="hdnJobId" runat="server" Value='<%#Eval("JobId")%>' />


                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">From Date:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">

                                                        <span style="display: flex;">
                                                            <asp:TextBox ID="txtFrom" runat="server" Style="color: black; font-size: 12px; height: 20px; width: 100px;" CssClass="datepicker form-control"></asp:TextBox>
                                                            <%-- <SiteControls:Calendar ID="txtFrom" runat="server" ErrorMessage="From Date" 
                                                                IsReadOnly="false" IsRequired="FALSE" Value="" />--%>
                                                        </span>
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">Mail To:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:DropDownList ID="drpMailTo" runat="server" Style="min-width: 150px; width: 150px;"></asp:DropDownList>
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">Signer:</td>
                                                    <td class="col-md-3" style="vertical-align:top;">
                                                        <asp:DropDownList ID="drpSignor" runat="server"></asp:DropDownList>
                                                    </td>
                                                    <%--<td class="col-md-1" style="text-align: right; padding-right: 0px;"></td>
                                                    <td class="col-md-1" style="text-align: right; padding-right: 0px;"></td>--%>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px; min-width: 130px;">Joint Check</td>
                                                    <td class="col-md-1" colspan="1">
                                                        <div class="checkbox-custom checkbox-default" style="padding-top: 0px;">
                                                            <asp:CheckBox ID="chkJointCkeck" runat="server" Text=" " />
                                                        </div>

                                                    </td>
                                                    
                                                    <td class="col-md-1">
                                                        <asp:Button ID="btnPrint" runat="server" CssClass="btn btn-primary" Style="padding-top: 0px; padding-bottom: 0px; margin: 2px;" Enabled="false" Text="Print" Width="80px" CommandName="Print" OnCommand="btnPrint_Command" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="col-md-1" style="vertical-align:top;min-width: 99px; text-align: right; padding-right: 0px;">Payment Amount:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <span style="display: flex;">
                                                            <%--<asp:TextBox ID="txtPaymentAmount" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px; width: 100px;" runat="server" CssClass="form-control"></asp:TextBox>--%>
                                                            <cc1:DataEntryBox ID="txtPaymentAmount" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px; width: 100px;" runat="server" CssClass="form-control" DataType="Money"
                                                                BlankOnZeroes="False">$0.00</cc1:DataEntryBox>
                                                            &nbsp;   Paid In Full box : &nbsp;
                                                        <div class="checkbox-custom checkbox-default" style="padding-top: 0px;">

                                                            <asp:CheckBox ID="chkPaidInFull" runat="server" Text=" " />
                                                        </div>
                                                        </span>

                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">Through Date:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <%--<asp:TextBox ID="txtthroughdate" runat="server"></asp:TextBox>--%>
                                                        <span style="display: flex;">
                                                            <asp:TextBox ID="txtthroughdate" runat="server" Style="color: black; font-size: 12px; height: 20px; width: 100px;" CssClass="datepicker form-control"></asp:TextBox>
                                                            <%--  
                                                            <asp:Image runat="Server" ID="Image1" ImageUrl="~/app_themes/vbjuice/img/Calendar.png" Width="23px" />
                                                            <ajaxToolkit:CalendarExtender ID="CalendarExtender2" runat="server" CssClass="calendar" PopupButtonID="Image1" TargetControlID="txtthroughdate" />--%>
                                                            <%--   <SiteControls:Calendar ID="txtthroughdate" runat="server" ErrorMessage="Through Date"
                                                                IsReadOnly="false" IsRequired="FALSE" Value="" />--%>
                                                        </span>
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">Payor:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:DropDownList ID="drpPayor" runat="server" Style="min-width: 150px; width: 150px;"></asp:DropDownList>
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;min-width: 75px; text-align: right; padding-right: 0px;">Waiver Note:</td>
                                                    <td class="col-md-1" style="vertical-align:top;">
                                                        <asp:TextBox ID="txtNote" runat="server" Style="color: black; font-size: 12px; padding-top: 1px; padding-bottom: 1px; height: 20px;" CssClass="form-control"></asp:TextBox>
                                                    </td>
                                                    <td class="col-md-1" style="vertical-align:top;text-align: right; padding-right: 0px;">Print Notary</td>
                                                    <td class="col-md-1">
                                                        <div class="checkbox-custom checkbox-default">
                                                            <asp:CheckBox ID="NotarywithCA" CssClass="checkbox" runat="server" Text=" Print CA Notary"
                                                                OnClick="JavaScript:myNotary(this.id,1);"></asp:CheckBox>
                                                        </div>
                                                    </td>                                                   
                                                    <td class="col-md-1">
                                                        <asp:Button ID="btnSaveWaiver" runat="server" CssClass="btn btn-primary" Style="padding-top: 0px; padding-bottom: 0px; margin: 2px;" Text="Submit" Width="80px" CommandName="Save" CommandArgument='<%#Eval("JobId")%>' OnCommand="btnPrint_Command" />
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td class="col-md-1" colspan="9"></td>
                                                    <td class="col-md-1">
                                                        <div class="checkbox-custom checkbox-default">
                                                            <asp:CheckBox ID="NotarywithoutCA" CssClass="checkbox" runat="server" Text="Print Notary"
                                                                OnClick="JavaScript:myNotary(this.id,2);"></asp:CheckBox>
                                                        </div>
                                                    </td>                                                    
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            <asp:GridView ID="example1" runat="server" CssClass="table dataTable table-striped" AutoGenerateColumns="False" DataKeyNames="JobId" Width="100%" BorderWidth="1px">

                                <Columns>
                                    <asp:TemplateField HeaderText="View">
                                        <ItemTemplate>

                                            <asp:LinkButton ID="btnEditItem" CssClass="icon ti-eye" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" Wrap="False" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                    <asp:BoundField DataField="JobId" SortExpression="JobId" HeaderText="CRFS#">
                                        <ItemStyle HorizontalAlign="Center" Width="40px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="JobName" SortExpression="JobName" HeaderText="Job Name">
                                        <ItemStyle Width="100px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="JobNum" SortExpression="JobNum" HeaderText="Job#">
                                        <ItemStyle Width="60px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CityStateZip" SortExpression="CityStateZip" HeaderText="Job Add">
                                        <ItemStyle Width="120px" Height="15px" Wrap="true" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="BranchNum" SortExpression="BranchNum" HeaderText="Branch">
                                        <ItemStyle HorizontalAlign="Center" Width="45px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="StatusCode" SortExpression="StatusCode" HeaderText="Stat">
                                        <ItemStyle HorizontalAlign="Center" Width="30px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ClientCustomer" SortExpression="ClientCustomer" HeaderText="Customer">
                                        <ItemStyle Height="15px" Wrap="True" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DateAssigned" SortExpression="DateAssigned" HeaderText="Assigned">
                                        <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ClientCode" SortExpression="ClientCode" HeaderText="Client#">
                                        <ItemStyle Width="30px" Height="15px" Wrap="False" />
                                    </asp:BoundField>

                                </Columns>
                                <EmptyDataTemplate><b>No Items found for the specified criteria</b></EmptyDataTemplate>
                            </asp:GridView>

                        </div>

                    </div>
                </asp:View>
                <asp:View ID="View3" runat="server">
                    <div class="body">
                        <asp:Button ID="btnExitViewer" runat="server" CssClass="btn btn-primary" Text="Exit Viewer"></asp:Button>
                        <asp:Button ID="btnDownLoad" Text="DownLoad" CssClass="btn btn-primary" runat="server" />
                        <br />
                        <br />
                        <asp:PlaceHolder ID="plcReportViewer" runat="server"></asp:PlaceHolder>
                        <br />
                    </div>
                </asp:View>
            </asp:MultiView>
        </div>
        <asp:HiddenField ID="hdnJobWaiverLogId" runat="server" Value="" />
        <asp:HiddenField ID="hdnCustEmail" runat="server" Value="" />
        <asp:Button ID="btnPrintWaiver" runat="server" OnClick="btnPrintWaiver_Click" Style="display: none;" />
        <asp:Button ID="btnEmailWaiver" runat="server" OnClick="btnEmailWaiver_Click" Style="display: none;" />
        <asp:UpdateProgress ID="siteUpdateProgress" runat="server">
            <ProgressTemplate>
                <div class="TransparentGrayBackground"></div>
                <asp:Panel ID="alwaysVisibleAjaxPanel" runat="server">
                    <div class="PageUpdateProgress">
                        <asp:Image ID="ajaxLoadNotificationImage" runat="server" ImageUrl="~/images/ajax-loader.gif" AlternateText="[image]" />
                        &nbsp;Please Wait...
                    </div>
                </asp:Panel>
                <ajaxToolkit:AlwaysVisibleControlExtender ID="AlwaysVisibleControlExtender1" runat="server" TargetControlID="alwaysVisibleAjaxPanel"
                    HorizontalSide="Center" HorizontalOffset="150" VerticalSide="Middle" VerticalOffset="0"></ajaxToolkit:AlwaysVisibleControlExtender>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="modal fade modal-primary" id="divModalEmail" aria-hidden="true" onkeypress="return noenter(event)" aria-labelledby="examplePositionCenter" role="dialog" tabindex="-1" style="display: none;" data-backdrop="static">
    <div class="modal-dialog modal-center">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" id="btnclose">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title">Email Send</h4>
            </div>
            <div class="modal-body">
                <div class="body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <div class="col-md-4">To:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtToList" runat="server" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4"></div>
                            <div class="col-md-8">
                                <label style="color: red; text-align: left;">Note:- Multiple email ids can be added separated by semicolon</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Subject:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtSubject" runat="server" CssClass="form-control" Text="New Waiver Document"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-4">Message:</div>
                            <div class="col-md-8">
                                <asp:TextBox ID="txtBody" runat="server" Height="130px" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-12">
                                <asp:Label ID="lblErrorMsg" runat="server" CssClass="normalred"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <%--<asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Send" Style="display: none;" />--%>
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaves" onclick="SendMail();">
                    SEND
                </button>
                <button type="button" class="btn btn-default margin-0" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
