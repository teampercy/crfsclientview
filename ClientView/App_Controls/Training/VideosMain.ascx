<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<head>
    <style type="text/css">
        .style5 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: x-large;
        }

        .style6 {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
        }
    </style>
    <title>Training</title>

    <script type="text/javascript">
        var index = location.href.lastIndexOf(".");
        var View = location.href.substring(index + 1, location.href.length);

        function MaintainMenuOpen() {
            SetHeaderBreadCrumb('', '', 'Training Videos');
            document.getElementById('olBreadCrumb').style.visibility = "hidden";
            if (View == "CollectView") { 
                MainMenuToggle('liCollectViewTraining');
                //$('#liCollectViewTraining').addClass('site-menu-item has-sub active open');
            }
            else {              
                MainMenuToggle('liLienTrainingVideos');
              //  $('#liLienTrainingVideos').addClass('site-menu-item has-sub active open');
            }

            return false;
        }      

    </script>

</head>


<br />

                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p class="style6">
                                                Although ClientView is a very user friendly web interface and just about every user is trained by our Customer Service Dept., 
        we have put together a few training videos that can be used to help train new users from your company as well as little reminders 
        of all the things that can be done in ClientView.  There is an audio track on all of the training videos, however the videos 
        are also closed captioned for the users that do not have speakers connected to their computers.
                                            </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <p class="style6">Just click on the link for the video that you would like to view and it will start streaming to you immediately.</p>

                                        </div>
                                    </div>
                                </div>
                          
