Imports System.Data.Sql
Imports System.Configuration
Imports System.Data
Imports System.Data.SqlClient

Namespace CRFS.ClientView


    Public Class CrfsBll
#Region "BasicFunctionality"
#Region "Variable Declaration"

        'insert lienview docs
        Private JobId As Integer
        Private UserId As Integer
        Private Usercode As String
        Private datecreated As DateTime
        Private filename As String
        Private Doctype As String
        Private Docdesc As String
        Private docimage As String

        'insertcollect view docs

        Private cDebtAccountId As Integer
        Private cUserId As Integer
        Private cUsercode As String
        Private cdatecreated As DateTime
        Private cfilename As String
        Private cDoctype As String
        Private cDocdesc As String
        Private cdocimage As String

        Private vRequestedByUserId As Integer
        Private vBatchNoJobWaiverId As Integer



        Private vclientname As String
        Private vpamt As Double
        Private vispaytodate As Boolean
        Private vdisputedamt As Double
        Private vmailwaiverto As String
        Private vMailtoCU As Boolean
        Private vMailtoGC As Boolean
        Private vMailtoLE As Boolean
        Private vMailtoOW As Boolean
        Private vCheckbyCU As Boolean
        Private vCheckByGC As Boolean
        Private vCheckbyLE As Boolean
        Private vCheckByOT As Boolean
        Private vCheckbyOW As Boolean
        Private vOtherChkIssuerName As String
        Private vJointCheckAgreement As Boolean
        Private vNotary As Boolean
        Private vNotaryType As Integer

        Private vStartDate As String
        Private vThroughDate As String

        Private vSigner As Integer

        Private vadditionalnote As String
        Private vinvoicepay As String
        Private vWaiverDates As String
        Private vWaiverPayments As String

        Private vJobAdd1 As String
        Private vJobAdd2 As String
        Private vJobCity As String
        Private vJobName As String
        Private vJobNum As String
        Private vJobState As String
        Private vJobZip As String


        Private vCustAdd1 As String
        Private vCustCity As String
        Private vCustName As String
        Private vCustNum As String
        Private vCustState As String
        Private vCustZip As String


        Private vGCAdd1 As String
        Private vGCCity As String
        Private vGCName As String
        Private vGCNum As String
        Private vGCState As String
        Private vGCZip As String

        Private vOwnerAdd1 As String
        Private vOwnerCity As String
        Private vOwnerName As String
        Private vOwnerState As String
        Private vOwnerZip As String

        Private vLenderAdd1 As String
        Private vLenderCity As String
        Private vLenderName As String
        Private vLenderState As String
        Private vLenderZip As String
        Private vStateInitials As String
        Private vStateAlert As String
        Private vstate As String
        Private vformcode As String

        Private vContractId As String



#End Region

#Region "Properties"
#Region "lienview"
        Public Property mJobId() As Integer
            Get
                Return JobId
            End Get
            Set(ByVal value As Integer)
                JobId = value
            End Set
        End Property

        Public Property mUserId() As Integer
            Get
                Return UserId
            End Get
            Set(ByVal value As Integer)
                UserId = value
            End Set
        End Property

        Public Property mUsercode() As String
            Get
                Return Usercode
            End Get
            Set(ByVal value As String)
                Usercode = value
            End Set
        End Property

        Public Property mdatecreated() As DateTime
            Get
                Return datecreated
            End Get
            Set(ByVal value As DateTime)
                datecreated = value
            End Set
        End Property

        Public Property mfilename() As String
            Get
                Return filename
            End Get
            Set(ByVal value As String)
                filename = value
            End Set
        End Property


        Public Property mDoctype() As String
            Get
                Return Doctype
            End Get
            Set(ByVal value As String)
                Doctype = value
            End Set
        End Property


        Public Property mDocdesc() As String
            Get
                Return Docdesc
            End Get
            Set(ByVal value As String)
                Docdesc = value
            End Set
        End Property

        Public Property mdocimage() As String
            Get
                Return docimage
            End Get
            Set(ByVal value As String)
                docimage = value
            End Set
        End Property


        Public Property mRequestedByUserId() As Integer
            Get
                Return RequestedByUserId
            End Get
            Set(ByVal value As Integer)
                RequestedByUserId = value
            End Set
        End Property

#End Region

#Region "collect view"
        Public Property mcDebtAccountId() As Integer
            Get
                Return cDebtAccountId
            End Get
            Set(ByVal value As Integer)
                cDebtAccountId = value
            End Set
        End Property

        Public Property mcUserId() As Integer
            Get
                Return cUserId
            End Get
            Set(ByVal value As Integer)
                cUserId = value
            End Set
        End Property

        Public Property mcUsercode() As String
            Get
                Return cUsercode
            End Get
            Set(ByVal value As String)
                cUsercode = value
            End Set
        End Property

        Public Property mcdatecreated() As DateTime
            Get
                Return cdatecreated
            End Get
            Set(ByVal value As DateTime)
                cdatecreated = value
            End Set
        End Property

        Public Property mcfilename() As String
            Get
                Return cfilename
            End Get
            Set(ByVal value As String)
                cfilename = value
            End Set
        End Property


        Public Property mcDoctype() As String
            Get
                Return cDoctype
            End Get
            Set(ByVal value As String)
                cDoctype = value
            End Set
        End Property


        Public Property mcDocdesc() As String
            Get
                Return cDocdesc
            End Get
            Set(ByVal value As String)
                cDocdesc = value
            End Set
        End Property

        Public Property mcdocimage() As String
            Get
                Return cdocimage
            End Get
            Set(ByVal value As String)
                cdocimage = value
            End Set
        End Property

        Public Property BatchNoJobWaiverId() As Integer
            Get
                Return vBatchNoJobWaiverId
            End Get
            Set(ByVal value As Integer)
                vBatchNoJobWaiverId = value
            End Set
        End Property

        Public Property RequestedByUserId() As String
            Get
                Return vRequestedByUserId
            End Get
            Set(ByVal value As String)
                vRequestedByUserId = value
            End Set
        End Property



        Public Property clientname() As String
            Get
                Return vclientname
            End Get
            Set(ByVal value As String)
                vclientname = value
            End Set
        End Property

        Public Property pamt() As Double
            Get
                Return vpamt
            End Get
            Set(ByVal value As Double)
                vpamt = value
            End Set
        End Property

        Public Property ispaytodate() As Boolean
            Get
                Return vispaytodate
            End Get
            Set(ByVal value As Boolean)
                vispaytodate = value
            End Set
        End Property

        Public Property disputedamt() As Double
            Get
                Return vdisputedamt
            End Get
            Set(ByVal value As Double)
                vdisputedamt = value
            End Set
        End Property

        Public Property mailwaiverto() As Boolean
            Get
                Return vmailwaiverto
            End Get
            Set(ByVal value As Boolean)
                vmailwaiverto = value
            End Set
        End Property


        Public Property MailtoCU() As Boolean
            Get
                Return vMailtoCU
            End Get
            Set(ByVal value As Boolean)
                vMailtoCU = value
            End Set
        End Property
        Public Property MailtoGC() As Boolean
            Get
                Return vMailtoGC
            End Get
            Set(ByVal value As Boolean)
                vMailtoGC = value
            End Set
        End Property

        Public Property MailtoLE() As Boolean
            Get
                Return vMailtoLE
            End Get
            Set(ByVal value As Boolean)
                vMailtoLE = value
            End Set
        End Property

        Public Property MailtoOW() As Boolean
            Get
                Return vMailtoOW
            End Get
            Set(ByVal value As Boolean)
                vMailtoOW = value
            End Set
        End Property

        Public Property CheckbyCU() As Boolean
            Get
                Return vCheckbyCU
            End Get
            Set(ByVal value As Boolean)
                vCheckbyCU = value
            End Set
        End Property

        Public Property CheckByGC() As Boolean
            Get
                Return vCheckByGC
            End Get
            Set(ByVal value As Boolean)
                vCheckByGC = value
            End Set
        End Property

        Public Property CheckbyLE() As Boolean
            Get
                Return vCheckbyLE
            End Get
            Set(ByVal value As Boolean)
                vCheckbyLE = value
            End Set
        End Property

        Public Property CheckByOT() As Boolean
            Get
                Return vCheckByOT
            End Get
            Set(ByVal value As Boolean)
                vCheckByOT = value
            End Set
        End Property

        Public Property CheckbyOW() As Boolean
            Get
                Return vCheckbyOW
            End Get
            Set(ByVal value As Boolean)
                vCheckbyOW = value
            End Set
        End Property

        Public Property OtherChkIssuerName() As String
            Get
                Return vOtherChkIssuerName
            End Get
            Set(ByVal value As String)
                vOtherChkIssuerName = value
            End Set
        End Property

        Public Property Notary() As Boolean
            Get
                Return vNotary
            End Get
            Set(ByVal value As Boolean)
                vNotary = value
            End Set
        End Property

        Public Property NotaryType() As Integer
            Get
                Return vNotaryType
            End Get
            Set(ByVal value As Integer)
                vNotaryType = value
            End Set
        End Property


        Public Property JointCheckAgreement() As Boolean
            Get
                Return vJointCheckAgreement
            End Get
            Set(ByVal value As Boolean)
                vJointCheckAgreement = value
            End Set
        End Property

        Public Property StartDate() As String
            Get
                Return vStartDate
            End Get
            Set(ByVal value As String)
                vStartDate = value
            End Set
        End Property

        Public Property ThroughDate() As String
            Get
                Return vThroughDate
            End Get
            Set(ByVal value As String)
                vThroughDate = value
            End Set
        End Property


        Public Property Signer() As Integer
            Get
                Return vSigner
            End Get
            Set(ByVal value As Integer)
                vSigner = value
            End Set
        End Property

        Public Property additionalnote() As String
            Get
                Return vadditionalnote
            End Get
            Set(ByVal value As String)
                vadditionalnote = value
            End Set
        End Property

        Public Property invoicepay() As String
            Get
                Return vinvoicepay
            End Get
            Set(ByVal value As String)
                vinvoicepay = value
            End Set
        End Property

        Public Property WaiverDates() As String
            Get
                Return vWaiverDates
            End Get
            Set(ByVal value As String)
                vWaiverDates = value
            End Set
        End Property

        Public Property WaiverPayments() As String
            Get
                Return vWaiverPayments
            End Get
            Set(ByVal value As String)
                vWaiverPayments = value
            End Set
        End Property

        Public Property JobAdd1() As String
            Get
                Return vJobAdd1
            End Get
            Set(ByVal value As String)
                vJobAdd1 = value
            End Set
        End Property

        Public Property JobAdd2() As String
            Get
                Return vJobAdd2
            End Get
            Set(ByVal value As String)
                vJobAdd2 = value
            End Set
        End Property

        Public Property JobCity() As String
            Get
                Return vJobCity
            End Get
            Set(ByVal value As String)
                vJobCity = value
            End Set
        End Property

        Public Property JobName() As String
            Get
                Return vJobName
            End Get
            Set(ByVal value As String)
                vJobName = value
            End Set
        End Property


        Public Property JobNum() As String
            Get
                Return vJobNum
            End Get
            Set(ByVal value As String)
                vJobNum = value
            End Set
        End Property


        Public Property JobState() As String
            Get
                Return vJobState
            End Get
            Set(ByVal value As String)
                vJobState = value
            End Set
        End Property

        Public Property JobZip() As String
            Get
                Return vJobZip
            End Get
            Set(ByVal value As String)
                vJobZip = value
            End Set
        End Property

        Public Property CustAdd1() As String
            Get
                Return vCustAdd1
            End Get
            Set(ByVal value As String)
                vCustAdd1 = value
            End Set
        End Property

      
        Public Property CustCity() As String
            Get
                Return vCustCity
            End Get
            Set(ByVal value As String)
                vCustCity = value
            End Set
        End Property

        Public Property CustName() As String
            Get
                Return vCustName
            End Get
            Set(ByVal value As String)
                vCustName = value
            End Set
        End Property


        Public Property CustNum() As String
            Get
                Return vCustNum
            End Get
            Set(ByVal value As String)
                vCustNum = value
            End Set
        End Property


        Public Property CustState() As String
            Get
                Return vCustState
            End Get
            Set(ByVal value As String)
                vCustState = value
            End Set
        End Property

        Public Property CustZip() As String
            Get
                Return vCustZip
            End Get
            Set(ByVal value As String)
                vCustZip = value
            End Set
        End Property



        Public Property GCAdd1() As String
            Get
                Return vGCAdd1
            End Get
            Set(ByVal value As String)
                vGCAdd1 = value
            End Set
        End Property



        Public Property GCCity() As String
            Get
                Return vGCCity
            End Get
            Set(ByVal value As String)
                vGCCity = value
            End Set
        End Property

        Public Property GCName() As String
            Get
                Return vGCName
            End Get
            Set(ByVal value As String)
                vGCName = value
            End Set
        End Property


        Public Property GCNum() As String
            Get
                Return vGCNum
            End Get
            Set(ByVal value As String)
                vGCNum = value
            End Set
        End Property


        Public Property GCState() As String
            Get
                Return vGCState
            End Get
            Set(ByVal value As String)
                vGCState = value
            End Set
        End Property

        Public Property GCZip() As String
            Get
                Return vGCZip
            End Get
            Set(ByVal value As String)
                vGCZip = value
            End Set
        End Property


        Public Property OwnerAdd1() As String
            Get
                Return vOwnerAdd1
            End Get
            Set(ByVal value As String)
                vOwnerAdd1 = value
            End Set
        End Property



        Public Property OwnerCity() As String
            Get
                Return vOwnerCity
            End Get
            Set(ByVal value As String)
                vOwnerCity = value
            End Set
        End Property

        Public Property OwnerName() As String
            Get
                Return vOwnerName
            End Get
            Set(ByVal value As String)
                vOwnerName = value
            End Set
        End Property



        Public Property OwnerState() As String
            Get
                Return vOwnerState
            End Get
            Set(ByVal value As String)
                vOwnerState = value
            End Set
        End Property

        Public Property OwnerZip() As String
            Get
                Return vOwnerZip
            End Get
            Set(ByVal value As String)
                vOwnerZip = value
            End Set
        End Property



        Public Property LenderAdd1() As String
            Get
                Return vLenderAdd1
            End Get
            Set(ByVal value As String)
                vLenderAdd1 = value
            End Set
        End Property



        Public Property LenderCity() As String
            Get
                Return vLenderCity
            End Get
            Set(ByVal value As String)
                vLenderCity = value
            End Set
        End Property

        Public Property LenderName() As String
            Get
                Return vLenderName
            End Get
            Set(ByVal value As String)
                vLenderName = value
            End Set
        End Property


        Public Property LenderState() As String
            Get
                Return vLenderState
            End Get
            Set(ByVal value As String)
                vLenderState = value
            End Set
        End Property

        Public Property LenderZip() As String
            Get
                Return vLenderZip
            End Get
            Set(ByVal value As String)
                vLenderZip = value
            End Set
        End Property

        Public Property StateInitials() As String
            Get
                Return vStateInitials
            End Get
            Set(ByVal value As String)
                vStateInitials = value
            End Set
        End Property



        Public Property StateAlert() As String
            Get
                Return vStateAlert
            End Get
            Set(ByVal value As String)
                vStateAlert = value
            End Set
        End Property

        Public Property State() As String
            Get
                Return vstate
            End Get
            Set(ByVal value As String)
                vstate = value
            End Set
        End Property

        Public Property FormCode() As String
            Get
                Return vformcode
            End Get
            Set(ByVal value As String)
                vformcode = value
            End Set
        End Property

        Public Property ContractId() As Integer
            Get
                Return vContractId
            End Get
            Set(ByVal value As Integer)
                vContractId = value
            End Set
        End Property

#End Region

#End Region

#Region "Functions"

#Region "Lien Jobs"
        'markparimal-Upload Document:To Insert documents For Lien Jobs.
        Public Shared Sub InsertJobAttachments(ByVal jobid As Integer, _
                                                             ByVal userid As Integer, _
                                                             ByVal Usercode As String, _
                                                             ByVal datecreated As DateTime, _
                                                             ByVal filename As String, _
                                                             ByVal Doctype As String, _
                                                             ByVal Docdesc As String, _
                                                             ByVal docimage As Byte(), _
                                                             ByVal batchjobid As Integer)




            Dim con As SqlConnection = New SqlConnection()
            con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
            Dim cmd As SqlCommand = New SqlCommand()

            con.Open()
            cmd.CommandText = "uspbo_ClientView_InsertJobAttachments"
            cmd.Connection = con
            cmd.CommandType = CommandType.StoredProcedure

            Dim Para As SqlParameter = New SqlParameter("@jobid", SqlDbType.Int)
            Para.Value = jobid
            cmd.Parameters.Add(Para)

            Dim Para1 As SqlParameter = New SqlParameter("@userid", SqlDbType.Int)
            Para1.Value = userid
            cmd.Parameters.Add(Para1)

            Dim Para2 As SqlParameter = New SqlParameter("@Usercode", SqlDbType.VarChar)
            Para2.Value = Usercode
            cmd.Parameters.Add(Para2)

            Dim Para3 As SqlParameter = New SqlParameter("@datecreated", SqlDbType.DateTime)
            Para3.Value = datecreated
            cmd.Parameters.Add(Para3)

            Dim Para4 As SqlParameter = New SqlParameter("@filename", SqlDbType.VarChar)
            Para4.Value = filename
            cmd.Parameters.Add(Para4)

            Dim Para5 As SqlParameter = New SqlParameter("@Doctype", SqlDbType.VarChar)
            Para5.Value = Doctype
            cmd.Parameters.Add(Para5)


            Dim Para6 As SqlParameter = New SqlParameter("@Docdesc", SqlDbType.VarChar)
            Para6.Value = Docdesc
            cmd.Parameters.Add(Para6)

            Dim Para7 As SqlParameter = New SqlParameter("@docimage", SqlDbType.Image)
            Para7.Value = docimage
            cmd.Parameters.Add(Para7)

            Dim Para8 As SqlParameter = New SqlParameter("@batchjobID", SqlDbType.Int)
            Para8.Value = batchjobid
            cmd.Parameters.Add(Para8)

            cmd.ExecuteNonQuery()

      
        End Sub
        Public Shared Sub UpdateDeadlineDates(ByVal ExpireDays As Integer, _
                                              ByVal JobId As Integer)

            Dim con As SqlConnection = New SqlConnection()
            con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
            Dim cmd As SqlCommand = New SqlCommand()

            con.Open()
            cmd.CommandText = "uspbo_LiensDayEnd_UpdateDeadlineDates"
            cmd.Connection = con
            cmd.CommandType = CommandType.StoredProcedure

            Dim Para As SqlParameter = New SqlParameter("@ExpireDays", SqlDbType.Int)
            Para.Value = JobId
            cmd.Parameters.Add(Para)

            Dim Para1 As SqlParameter = New SqlParameter("@jobid", SqlDbType.Int)
            Para1.Value = JobId
            cmd.Parameters.Add(Para1)

            cmd.ExecuteNonQuery()

        End Sub
        'markparimal-State Alert:To Get State Alert For Selected State.
        Public Sub GetStateAlertByStateInitials(ByVal StateInitials As String)
            Try
                Dim con As SqlConnection = New SqlConnection()
                con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
                Dim ds As New DataSet()
                Dim da As New SqlDataAdapter("uspbo_ClientView_GetStateAlertByStateInitials", con)
                da.SelectCommand.CommandType = CommandType.StoredProcedure
                da.SelectCommand.Parameters.Add(New SqlParameter("@StateInitials", SqlDbType.VarChar))
                da.SelectCommand.Parameters("@StateInitials").Value = StateInitials
                da.Fill(ds)
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim dt As DataTable = ds.Tables(0)
                    StateAlert = dt.Rows(0)("StateAlert")

                End If

            Catch ex As Exception

            End Try
        End Sub
#End Region

#Region "Collect View"
        'markparimal-Upload Document:To Insert documents For Collect Jobs.
        Public Shared Sub InsertDebtAccountAttachments(ByVal DebtAccountId As Integer, _
                                                              ByVal userid As Integer, _
                                                              ByVal Usercode As String, _
                                                              ByVal datecreated As DateTime, _
                                                              ByVal filename As String, _
                                                              ByVal Doctype As String, _
                                                              ByVal Docdesc As String, _
                                                              ByVal docimage As Byte(), _
                                                              ByVal BatchDebtAccountId As Integer)





            Dim con As SqlConnection = New SqlConnection()
            con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
            Dim cmd As SqlCommand = New SqlCommand()

            con.Open()
            cmd.CommandText = "cview_ClientView_InsertDebtAccountAttachments"
            cmd.Connection = con
            cmd.CommandType = CommandType.StoredProcedure

            Dim Para As SqlParameter = New SqlParameter("@DebtAccountId", SqlDbType.Int)
            Para.Value = DebtAccountId
            cmd.Parameters.Add(Para)

            Dim Para1 As SqlParameter = New SqlParameter("@UserId", SqlDbType.Int)
            Para1.Value = userid
            cmd.Parameters.Add(Para1)

            Dim Para2 As SqlParameter = New SqlParameter("@Usercode", SqlDbType.VarChar)
            Para2.Value = Usercode
            cmd.Parameters.Add(Para2)

            Dim Para3 As SqlParameter = New SqlParameter("@datecreated", SqlDbType.DateTime)
            Para3.Value = datecreated
            cmd.Parameters.Add(Para3)

            Dim Para4 As SqlParameter = New SqlParameter("@filename", SqlDbType.VarChar)
            Para4.Value = filename
            cmd.Parameters.Add(Para4)

            Dim Para5 As SqlParameter = New SqlParameter("@Doctype", SqlDbType.VarChar)
            Para5.Value = Doctype
            cmd.Parameters.Add(Para5)


            Dim Para6 As SqlParameter = New SqlParameter("@Docdesc", SqlDbType.VarChar)
            Para6.Value = Docdesc
            cmd.Parameters.Add(Para6)

            Dim Para7 As SqlParameter = New SqlParameter("@docimage", SqlDbType.Image)
            Para7.Value = docimage
            cmd.Parameters.Add(Para7)

            Dim Para8 As SqlParameter = New SqlParameter("@BatchDebtAccountId", SqlDbType.Int)
            Para8.Value = BatchDebtAccountId
            cmd.Parameters.Add(Para8)

            cmd.ExecuteNonQuery()


        End Sub

        'markparimal-client combo box in the collection NewAccount screen:To Fill client combobox.
        Public Shared Function GetClientListForUser(ByVal auserid As String)
            Dim ds As New DataSet()
            Try
                Dim con As SqlConnection = New SqlConnection()
                con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
                Dim da As New SqlDataAdapter("cview_Clientview_GetClientListForUser", con)
                da.SelectCommand.CommandType = CommandType.StoredProcedure
                da.SelectCommand.Parameters.Add(New SqlParameter("@UserId", SqlDbType.VarChar))
                da.SelectCommand.Parameters("@UserId").Value = auserid
                da.Fill(ds)
            Catch ex As Exception

            End Try
            Return ds
        End Function

        'markparimal-client combo box in the collection NewAccount screen:To setSet the BatchDebtAccount.ContractId in database .
        Public Shared Function GetContractInfoByClientId(ByVal ClientId As String)
            Dim ds As New DataSet()
            Dim dt As New DataTable
            Try
                Dim con As SqlConnection = New SqlConnection()
                con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
                Dim da As New SqlDataAdapter("cview_Clientview_GetContractInfoByClientId", con)
                da.SelectCommand.CommandType = CommandType.StoredProcedure
                da.SelectCommand.Parameters.Add(New SqlParameter("@ClientId ", SqlDbType.VarChar))
                da.SelectCommand.Parameters("@ClientId ").Value = ClientId
                da.Fill(ds)
                If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    dt = ds.Tables(0)
                End If

            Catch ex As Exception

            End Try
            Return dt
        End Function

       

#End Region

#Region "Blank Waivers"
        'markparimal-Blank Waivers:To Fill Blank Waiver Grid By UserId.
        Public Shared Function GetBlankWaiversByUserId(ByVal RequestedByUserId As Integer, ByVal sortExp As String, ByVal sortDir As String)
            Dim ds As New DataSet()
            Dim cmd As SqlCommand = New SqlCommand()
            Dim myDataView As New DataView()
            Try
                Dim con As SqlConnection = New SqlConnection()
                con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
                con.Open()
                Dim da As New SqlDataAdapter("uspbo_ClientView_GetBlankWaiverByUserId", con)
                da.SelectCommand.CommandType = CommandType.StoredProcedure
                da.SelectCommand.Parameters.Add(New SqlParameter("@RequestedByUserId", SqlDbType.Int))
                da.SelectCommand.Parameters("@RequestedByUserId").Value = RequestedByUserId
                da.Fill(ds)


                myDataView = ds.Tables(0).DefaultView

                If Not String.IsNullOrEmpty(sortExp) Then
                    myDataView.Sort = String.Format("{0} {1}", sortExp, sortDir)
                End If
                con.Close()


            Catch ex As Exception

            End Try
            Return myDataView
        End Function
        'markparimal-Blank Waivers:. Pull the waiver info by BatchNoJobWaiverId.
        Public Function GetBlankWaiversByBatchNoJobWaiverId(ByVal BatchNoJobWaiverId As Integer)

            Dim con As SqlConnection = New SqlConnection()
            con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()

            Dim cmd As SqlCommand = New SqlCommand()
            con.Open()
            Dim ds As New DataSet()
            Dim da As New SqlDataAdapter("uspbo_ClientView_GetBlankWaiverById", con)
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.SelectCommand.Parameters.Add(New SqlParameter("@BatchNoJobWaiverId", SqlDbType.Int))
            da.SelectCommand.Parameters("@BatchNoJobWaiverId").Value = BatchNoJobWaiverId
            da.Fill(ds)

            If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                Dim dt As DataTable = ds.Tables(0)
                clientname = dt.Rows(0)("ClientName")
                pamt = dt.Rows(0)("PaymentAmt")
                ispaytodate = dt.Rows(0)("IsPaidInFull")
                disputedamt = dt.Rows(0)("DisputedAmt")
                MailtoCU = dt.Rows(0)("MailtoCU")
                MailtoGC = dt.Rows(0)("MailtoGC")
                MailtoLE = dt.Rows(0)("MailtoLE")
                MailtoOW = dt.Rows(0)("MailtoOW")
                CheckbyCU = dt.Rows(0)("CheckbyCU")
                CheckByGC = dt.Rows(0)("CheckByGC")
                CheckbyLE = dt.Rows(0)("CheckbyLE")
                CheckByOT = dt.Rows(0)("CheckByOT")
                CheckbyOW = dt.Rows(0)("CheckbyOW")
                OtherChkIssuerName = dt.Rows(0)("OtherChkIssuerName").ToString()

                JointCheckAgreement = dt.Rows(0)("JointCheckAgreement")
                Notary = dt.Rows(0)("Notary")
                NotaryType = dt.Rows(0)("NotaryType")
                'NotaryType = 1
                StartDate = dt.Rows(0)("StartDate")
                ThroughDate = dt.Rows(0)("ThroughDate").ToString()
                Signer = dt.Rows(0)("SignerId")
                additionalnote = dt.Rows(0)("Note")
                invoicepay = dt.Rows(0)("InvoicePaymentNo")
                WaiverDates = dt.Rows(0)("WaiverDates")
                WaiverPayments = dt.Rows(0)("WaiverPayments")

                JobAdd1 = dt.Rows(0)("JobAdd1")
                JobAdd2 = dt.Rows(0)("JobAdd2")
                JobCity = dt.Rows(0)("JobCity")
                JobName = dt.Rows(0)("JobName")
                JobNum = dt.Rows(0)("JobNum")
                JobState = dt.Rows(0)("JobState")
                JobZip = dt.Rows(0)("JobZip")


                CustAdd1 = dt.Rows(0)("CustAdd1")
                CustCity = dt.Rows(0)("CustCity")
                CustName = dt.Rows(0)("CustName")
                CustNum = dt.Rows(0)("CustNum")
                CustState = dt.Rows(0)("CustState")
                CustZip = dt.Rows(0)("CustZip")

                GCAdd1 = dt.Rows(0)("GCAdd1")
                GCCity = dt.Rows(0)("GCCity")
                GCName = dt.Rows(0)("GCName")
                GCNum = dt.Rows(0)("GCNum")
                GCState = dt.Rows(0)("GCState")
                GCZip = dt.Rows(0)("GCZip")

                OwnerAdd1 = dt.Rows(0)("OwnerAdd1")
                OwnerCity = dt.Rows(0)("OwnerCity")
                OwnerName = dt.Rows(0)("OwnerName")
                OwnerState = dt.Rows(0)("OwnerState")
                OwnerZip = dt.Rows(0)("OwnerZip")

                LenderAdd1 = dt.Rows(0)("LenderAdd1")
                LenderCity = dt.Rows(0)("LenderCity")
                LenderName = dt.Rows(0)("LenderName")
                LenderState = dt.Rows(0)("LenderState")
                LenderZip = dt.Rows(0)("LenderZip")
                State = dt.Rows(0)("StateName")
                FormCode = dt.Rows(0)("FormCode")


            End If

            Return True
        End Function

#End Region
#Region "Job View"
        Public Shared Function UpdateJobView_NoticeRequest1(ByVal JobViewId As String, _
                                                      ByVal submittedbyuserid As String, ByVal EstBalance As Decimal) As Integer
            Dim result As Integer
            Try

                Dim con As SqlConnection = New SqlConnection()
                con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
                Dim cmd As SqlCommand = New SqlCommand()

                con.Open()
                cmd.CommandText = "uspbo_JobView_NoticeRequest"
                cmd.Connection = con
                cmd.CommandType = CommandType.StoredProcedure

                Dim Para As SqlParameter = New SqlParameter("@JobId", SqlDbType.VarChar)
                Para.Value = JobViewId
                cmd.Parameters.Add(Para)

                Dim Para1 As SqlParameter = New SqlParameter("@submittedbyuserid", SqlDbType.VarChar)
                Para1.Value = submittedbyuserid
                cmd.Parameters.Add(Para1)

                Dim Para2 As SqlParameter = New SqlParameter("@EstBalance", SqlDbType.Decimal)
                Para2.Value = EstBalance
                cmd.Parameters.Add(Para2)

                result = cmd.ExecuteNonQuery()
                Return result
            Catch ex As Exception
                Return result
            End Try


        End Function
        Public Shared Function UpdateJobView_NoticeRequest(ByVal JobViewId As String, _
                                                    ByVal submittedbyuserid As String, ByVal EstBalance As Decimal) As Integer
            Dim result As Integer
            Try

                Dim con As SqlConnection = New SqlConnection()
                con.ConnectionString = ConfigurationManager.AppSettings("PORTAL").ToString()
                Dim cmd As SqlCommand = New SqlCommand()

                con.Open()
                cmd.CommandText = "uspbo_JobView_NoticeRequest"
                cmd.Connection = con
                cmd.CommandType = CommandType.StoredProcedure

                Dim Para As SqlParameter = New SqlParameter("@JobId", SqlDbType.VarChar)
                Para.Value = JobViewId
                cmd.Parameters.Add(Para)

                Dim Para1 As SqlParameter = New SqlParameter("@submittedbyuserid", SqlDbType.VarChar)
                Para1.Value = submittedbyuserid
                cmd.Parameters.Add(Para1)

                'Dim Para2 As SqlParameter = New SqlParameter("@EstBalance", SqlDbType.Decimal)
                'Para2.Value = EstBalance
                'cmd.Parameters.Add(Para2)

                result = cmd.ExecuteNonQuery()
                Return result
            Catch ex As Exception
                Return result
            End Try


        End Function
#End Region
#End Region

#End Region
    End Class

End Namespace