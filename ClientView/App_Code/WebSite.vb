Imports Microsoft.VisualBasic
Imports System.Text
Imports System.Net.Mail
Imports System.Collections.Generic
Imports System.Net
Imports System.IO
Imports System.Globalization
Imports System.Xml
Imports CRF.CLIENTVIEW.BLL.CRFDB.TABLES
Imports CRF.CLIENTVIEW.BLL
Imports CRF.CLIENTVIEW.BLL.ProviderBase
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Security.Cryptography
Namespace HDS.WEBSITE.UI

#Region "Base Clases"
    <Serializable()>
    Public Class CurrentUser
        Private _id As Integer = -1
        Private _roles As String = "GUEST"
        Private _useremail As String = String.Empty
        Private _usermanager As Integer
        Private _username As String = "GUEST"
        Private _createdate As DateTime = Now
        Private _user As New Portal_Users
        Private _menuitems As New List(Of HDS.WEBLIB.Navigation.MenuItem)
        Private _usermenu As UserMenu
        Public Sub New()

            Dim MYCOOKIE As HttpCookie = System.Web.HttpContext.Current.Request.Cookies.Get("CLIENTVIEW")
            If IsNothing(MYCOOKIE) = False Then
                If MYCOOKIE.Item("DATA").Length > 0 Then
                    _id = Integer.Parse(MYCOOKIE.Item("DATA"))
                End If
                ProviderBase.DAL.Read(_id, _user)
                _user.LastLoginDate = Now()
                _user.LastMachineId = MYCOOKIE.Item("MACHINEID")
                ProviderBase.DAL.Update(_user)
            End If

            If _user.ID > 1 Then
                _username = _user.UserName
                _roles = _user.RoleList
                _useremail = _user.Email
                _usermanager = _user.IsClientViewManager

            End If
            _usermenu = New UserMenu(Me.Roles, Me._usermanager)
            _createdate = Now
            'Me.Session("userrole") = _usermenu

        End Sub
        Public ReadOnly Property Id()
            Get
                Return _id
            End Get

        End Property

        Public ReadOnly Property UserName()

            Get
                Return _username
            End Get

        End Property
        Public ReadOnly Property UserManager() As Integer

            Get
                Return _usermanager
            End Get

        End Property

        Public ReadOnly Property CreateDate() As Date
            Get
                Return _createdate
            End Get

        End Property
        Public Property Roles() As String
            Get
                Return _roles
            End Get
            Set(ByVal value As String)
                _roles = value
            End Set
        End Property

        Public ReadOnly Property UserInfo()
            Get
                Return _user
            End Get

        End Property
        Public ReadOnly Property MenuItems()
            Get
                Return _usermenu.Items
            End Get
        End Property
        Public ReadOnly Property UserMenu() As UI.UserMenu
            Get
                Return _usermenu
            End Get
        End Property
        Public Function IsInRole(ByVal sroles As String) As Boolean
            Dim uroles As String = Me.Roles
            If uroles.Length < 1 Then uroles = "GUEST"
            'Added 10/17/2013
            If uroles = "GUEST" Then Return False

            If sroles.Length < 1 Then Return True

            If InStr(uroles, "ADMIN") > 0 Then Return True
            If InStr(uroles, "ADMINISTRATOR") > 0 Then Return True
            If InStr(sroles, uroles) > 0 Then Return True
            Return False

        End Function

    End Class
    Public Class UserMenu
        Inherits MenuHelperRoot
        Dim ds As New System.Data.DataSet
        Dim dr As System.Data.DataRow
        Dim myrole As String
        Dim checkrole As Integer

        Public Sub New(ByVal uroles As String, ByVal umanager As Integer)
            Dim mymap As String = System.Web.HttpContext.Current.Request.MapPath("~/app_data/sitemap")
            myrole = uroles
            checkrole = umanager
            ds.ReadXml(mymap)
            Dim sitedr As DataRow = ds.Tables("site").Rows(0)
            For Each dr In ds.Tables("menuitem").Rows
                If FixNull(dr("MenuItem_Id_0")) = String.Empty Then
                    If IsInRole(myrole, FixNull(dr("roles"))) = True Then
                        Dim myitem As New MenuItem(FixNull(dr("url")), FixNull(dr("title")), FixNull(dr("id")), FixNull(dr("iconClass")), Me)

                        If (checkrole = 0 And FixNull(dr("title")) = "Reports") Then

                        Else
                            GetLevel1(FixNull(dr("menuitem_Id")), myitem)
                            Me.Items.Add(myitem)
                        End If
                    End If
                End If
            Next

        End Sub
        ''---without date validation
        'Private Sub GetLevel1(ByVal agroupid As String, ByRef myitem As MenuItem)
        '    For Each dr In ds.Tables("menuitem").Rows
        '        If FixNull(dr("menuitem_Id_0")) = agroupid Then
        '            If IsInRole(myrole, FixNull(dr("roles"))) = True Then
        '                Dim mysubitem As New MenuItem(FixNull(dr("url")), FixNull(dr("title")), FixNull(dr("id")), FixNull(dr("iconClass")), Me)


        '                Dim CheckDate As Date = DateTime.Now.Date
        '                Dim CheckDay As Integer = CheckDate.Day



        '                GetLevel2(FixNull(dr("menuitem_Id")), mysubitem)
        '                myitem.Items.Add(mysubitem)



        '            End If
        '        End If
        '    Next

        'End Sub
        '---with date validation
        Private Sub GetLevel1(ByVal agroupid As String, ByRef myitem As MenuItem)
            For Each dr In ds.Tables("menuitem").Rows
                If FixNull(dr("menuitem_Id_0")) = agroupid Then
                    If IsInRole(myrole, FixNull(dr("roles"))) = True Then
                        Dim mysubitem As New MenuItem(FixNull(dr("url")), FixNull(dr("title")), FixNull(dr("id")), FixNull(dr("iconClass")), Me)


                        Dim CheckDate As Date = DateTime.Now.Date
                        Dim CheckDay As Integer = CheckDate.Day

                        'Note:We have visible "Texas Job Review" Menu in Rental View and Lien View through out all days for month on Test server only and Production server visible only first 14 days. 
                        If ((CheckDay > 14 And (FixNull(dr("id")) = "liTexasJobReviewJobView" Or FixNull(dr("title")) = "Texas Pending List")) Or (checkrole = 0 And (FixNull(dr("title")) = "Place Accounts" Or FixNull(dr("title")) = "Place Jobs" Or FixNull(dr("title")) = "Blank Waivers" Or FixNull(dr("title")) = "Bulk Waivers"))) Then
                            'If ((CheckDay > 14 And (FixNull(dr("title")) = "Texas Pending List")) Or (checkrole = 0 And (FixNull(dr("title")) = "Place Accounts" Or FixNull(dr("title")) = "Place Jobs" Or FixNull(dr("title")) = "Blank Waivers" Or FixNull(dr("title")) = "Bulk Waivers"))) Then

                            'Note:Same as above comment Visible "Texas Job Review" Menu only test server through out all days while on production server visible 1st 14 days
                        ElseIf (CheckDay > 15 And FixNull(dr("id")) = "lisubTexasJobReviewLienView") Then

                        Else
                                GetLevel2(FixNull(dr("menuitem_Id")), mysubitem)
                                myitem.Items.Add(mysubitem)

                            End If

                        End If
                    End If
            Next

        End Sub
        Private Sub GetLevel2(ByVal aparentid As String, ByRef myitem As MenuItem)
            For Each dr In ds.Tables("menuitem").Rows
                If IsDBNull(dr("menuitem_Id_0")) = False Then
                    If FixNull(dr("menuitem_Id_0")) = aparentid Then
                        If IsInRole(myrole, FixNull(dr("roles"))) = True Then
                            Dim mysubitem As New MenuItem(FixNull(dr("url")), FixNull(dr("title")), FixNull(dr("id")), FixNull(dr("iconClass")), Me)
                            myitem.Items.Add(mysubitem)
                        End If
                    End If
                End If
            Next
        End Sub
        Private Function FixNull(ByVal avalue As Object) As String
            If IsDBNull(avalue) = False Then
                Return avalue
            End If
            Return String.Empty

        End Function
        Private Function IsInRole(ByVal uroles As String, ByVal sroles As String) As Boolean

            If uroles.Length < 1 Then uroles = "GUEST"
            'Added 10/17/2013
            If uroles = "GUEST" Then Return False

            If sroles.Length < 1 Then Return True

            'If InStr(uroles, "ADMIN") > 0 Then Return True
            If InStr(uroles, "ADMINISTRATOR") > 0 Then Return True
            Dim SS As String() = Split(sroles, ",")
            Dim S As String
            For Each S In SS
                Dim i As Integer = InStr(UCase(uroles), UCase(S))
                If i > 0 Then Return True
            Next
            Return False

        End Function
    End Class
    Public Class MenuItem
        Public Sub New(ByVal sLink As String, ByVal sText As String, ByVal sId As String, ByVal iconClassText As String, ByVal oRoot As MenuHelperRoot)
            Link = sLink
            Text = sText
            LinkId = sId
            IconClass = iconClassText
            m_Root = oRoot
        End Sub
        Private m_Root As MenuHelperRoot
        Public Link As String = ""
        Public Text As String = ""
        Public LinkId As String = ""
        Public IconClass As String = ""
        Public Items As New System.Collections.Generic.List(Of MenuItem)()
        Public ReadOnly Property RecursiveIsCurrent() As Boolean
            Get
                If Link <> "#" AndAlso m_Root.IsCurrent(Me) Then
                    Return True
                End If
                For Each oItem As MenuItem In Items
                    If oItem.RecursiveIsCurrent Then
                        Return True
                    End If
                Next
                Return False
            End Get
        End Property
        Public Function GetHtml() As String
            Dim oBuilder As New System.Text.StringBuilder()
            oBuilder.AppendLine("<li " + (IIf(RecursiveIsCurrent, " " + m_Root.LICurrentDecoration, "")) + ">")
            oBuilder.AppendLine("<a href=""" + Link + """>" + Text + "</a>")
            If Items.Count > 0 Then
                oBuilder.Append("<ul>")
                For Each oItem As MenuItem In Items
                    oBuilder.AppendLine(oItem.GetHtml())
                Next
                oBuilder.AppendLine("</ul>")
            End If
            oBuilder.AppendLine("</li>")
            Return oBuilder.ToString()
        End Function
    End Class
    Public Class ShoppingCart
        Private _items As New Dictionary(Of Integer, ShoppingCartItem)()
        Private _orderid As String = UCase(GetUniqueId())
        Private _shippingid As String = "1"
        Private _shippingdescr As String
        Private _shippingprice As Decimal
        Private _buyername As String = String.Empty
        Private _buyeremail As String = String.Empty
        Private _buyerphone As String = String.Empty
        Private _shiptoaddress As String = String.Empty
        Private _shiptoname As String = String.Empty
        Private _shiptoaddr1 As String = String.Empty
        Private _shiptoaddr2 As String = String.Empty
        Private _shiptocity As String = String.Empty
        Private _shiptostate As String = String.Empty
        Private _shiptozip As String = String.Empty
        Private _shiptocountry As String = String.Empty
        Private _shiptophone As String = String.Empty
        Public ReadOnly Property OrderId() As String
            Get
                Return _orderid
            End Get
        End Property
        Public Property shippingid() As String
            Get
                Return _shippingid
            End Get
            Set(ByVal value As String)
                _shippingid = value
            End Set
        End Property
        Public Property shippingdescr() As String
            Get
                Return _shippingdescr
            End Get
            Set(ByVal value As String)
                _shippingdescr = value
            End Set
        End Property
        ' Gets the sum total of the items' prices
        Public ReadOnly Property Price() As Decimal
            Get
                Dim sum As Decimal = 0.0
                For Each item As ShoppingCartItem In _items.Values
                    sum += item.UnitPrice * item.Quantity
                Next
                Return sum
            End Get
        End Property
        ' Gets the sum total of the items' prices
        Public ReadOnly Property Total() As Decimal
            Get
                Dim sum As Decimal = Price + shippingprice
                Return sum
            End Get
        End Property
        Public Property shippingprice() As String
            Get
                Return _shippingprice * Me.Items.Count
            End Get
            Set(ByVal value As String)
                _shippingprice = value
            End Set
        End Property
        Public Property buyername() As String
            Get
                Return _buyername
            End Get
            Set(ByVal Value As String)
                _buyername = Strings.Left(Value, 150)
            End Set
        End Property

        Public Property buyeremail() As String
            Get
                Return _buyeremail
            End Get
            Set(ByVal Value As String)
                _buyeremail = Strings.Left(Value, 150)
            End Set
        End Property

        Public Property buyerphone() As String
            Get
                Return _buyerphone
            End Get
            Set(ByVal Value As String)
                _buyerphone = Strings.Left(Value, 150)
            End Set
        End Property
        Public Property shiptoname() As String
            Get
                Return _shiptoname
            End Get
            Set(ByVal Value As String)
                _shiptoname = Strings.Left(Value, 150)
            End Set
        End Property

        Public Property shiptoaddr1() As String
            Get
                Return _shiptoaddr1
            End Get
            Set(ByVal Value As String)
                _shiptoaddr1 = Strings.Left(Value, 150)
            End Set
        End Property
        Public Property shiptoaddr2() As String
            Get
                Return _shiptoaddr2
            End Get
            Set(ByVal Value As String)
                _shiptoaddr2 = Strings.Left(Value, 150)
            End Set
        End Property
        Public Property shiptocity() As String
            Get
                Return _shiptocity
            End Get
            Set(ByVal Value As String)
                _shiptocity = Strings.Left(Value, 50)
            End Set
        End Property

        Public Property shiptostate() As String
            Get
                Return _shiptostate
            End Get
            Set(ByVal Value As String)
                _shiptostate = Strings.Left(Value, 50)
            End Set
        End Property

        Public Property shiptozip() As String
            Get
                Return _shiptozip
            End Get
            Set(ByVal Value As String)
                _shiptozip = Strings.Left(Value, 50)
            End Set
        End Property

        Public Property shiptocountry() As String
            Get
                Return _shiptocountry
            End Get
            Set(ByVal Value As String)
                _shiptocountry = Strings.Left(Value, 50)
            End Set
        End Property
        Public ReadOnly Property Items() As ICollection
            Get
                Return _items.Values
            End Get
        End Property

        ' Adds a new item to the shopping cart
        Public Sub InsertItem(ByVal id As Integer, ByVal title As String, ByVal sku As String, ByVal unitPrice As Decimal, ByVal qty As Integer)
            If _items.ContainsKey(id) Then
                _items(id).Quantity = qty
            Else
                _items.Add(id, New ShoppingCartItem(id, title, sku, unitPrice, qty))
            End If
        End Sub

        ' Removes an item from the shopping cart
        Public Sub DeleteItem(ByVal id As Integer)
            If _items.ContainsKey(id) Then
                _items.Remove(id)
            End If
        End Sub

        ' Removes all items of a specified product from the shopping cart
        Public Sub DeleteProduct(ByVal id As Integer)
            If _items.ContainsKey(id) Then
                _items.Remove(id)
            End If
        End Sub

        ' Updates the quantity for an item
        Public Sub UpdateItemQuantity(ByVal id As Integer, ByVal quantity As Integer)
            If _items.ContainsKey(id) Then
                Dim item As ShoppingCartItem = _items(id)
                item.Quantity = quantity
                If item.Quantity <= 0 Then _
               _items.Remove(id)
            End If
        End Sub

        ' Clears the cart
        Public Sub Clear()
            _items.Clear()
            'Dim myshipvia As New TABLES.Store_ShippingMethod
            'HDS.WEBSITE.BLL.WebSite.Provider.DAL.GetItem(myshipvia.TableObjectName, "shipping", "STANDARD", myshipvia)
            'Me.shippingid = myshipvia.PKID
        End Sub
        Public Shared Function GetUniqueId() As String
            Dim MYID As Guid = System.Guid.NewGuid()
            Dim S As String = DateTime.Now.ToString().GetHashCode().ToString("x").ToUpper
            Return S


        End Function
        Public Function GetUniqueId1() As String
            Dim maxSize As Integer = 20
            Dim minSize As Integer = 15
            Dim chars() As Char = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray
            Dim size As Integer = maxSize
            Dim data(1) As Byte
            Dim crypto As New RNGCryptoServiceProvider
            crypto.GetNonZeroBytes(data)
            size = data(data(0) Mod (maxSize - minSize)) + minSize
            ReDim data(size)
            crypto.GetNonZeroBytes(data)
            Dim result As New StringBuilder(size)
            For Each b As Byte In data
                result.Append(chars(b Mod (chars.Length - 1)))
            Next
            Return result.ToString
        End Function

    End Class
    Public Class ShoppingCartItem

        ' ==========
        ' Private Variables
        ' ==========

        Private _id As Integer = 0
        Private _title As String = ""
        Private _sku As String = ""
        Private _URL As String = ""
        Private _unitPrice As Decimal
        Private _quantity As Integer = 1

        ' ==========
        ' Properties
        ' ==========

        Public Property ID() As Integer
            Get
                Return _id
            End Get
            Private Set(ByVal value As Integer)
                _id = value
            End Set
        End Property

        Public Property Title() As String
            Get
                Return _title
            End Get
            Private Set(ByVal value As String)
                _title = value
            End Set
        End Property
        Public Property URL() As String
            Get
                Return _URL
            End Get
            Private Set(ByVal value As String)
                _URL = value
            End Set
        End Property
        Public Property SKU() As String
            Get
                Return _sku
            End Get
            Private Set(ByVal value As String)
                _sku = value
            End Set
        End Property

        Public Property UnitPrice() As Decimal
            Get
                Return _unitPrice
            End Get
            Private Set(ByVal value As Decimal)
                _unitPrice = value
            End Set
        End Property

        Public Property Quantity() As Integer
            Get
                Return _quantity
            End Get
            Set(ByVal value As Integer)
                _quantity = value
            End Set
        End Property
        Public ReadOnly Property Price() As Decimal
            Get
                Return _unitPrice * _quantity
            End Get
        End Property
        ' ==========
        ' Constructor
        ' ==========
        Public Sub New()

        End Sub
        Public Sub New(ByVal id As Integer, ByVal title As String, ByVal sku As String, ByVal unitPrice As Decimal, ByVal qty As Integer)
            Me.ID = id
            Me.Title = title
            Me.SKU = sku
            Me.UnitPrice = unitPrice
            Me.Quantity = qty
        End Sub
    End Class
    Public Class MenuHelperRoot
        Public Function IsCurrent(ByVal oItem As MenuItem) As Boolean
            If System.Web.HttpContext.Current.Request.Url.ToString().ToLower().IndexOf(oItem.Link.ToLower()) >= 0 Then
                Return True
            End If
            Return False
        End Function
        Public ULDecoration As String = "id=""menu"""
        Public LICurrentDecoration As String = "class=""current"""
        Public Items As New System.Collections.Generic.List(Of MenuItem)()
        Public Function GetHtml() As String
            Dim oBuilder As New System.Text.StringBuilder()
            oBuilder.Append("<ul ")
            oBuilder.Append(ULDecoration)
            oBuilder.Append(">")
            For Each oItem As MenuItem In Items
                oBuilder.Append(oItem.GetHtml())
            Next
            oBuilder.Append("</ul>")
            Return oBuilder.ToString()
        End Function

    End Class
    Public Class Encryptor
        Public Shared Function Encrypt(ByVal vstrTextToBeEncrypted As String,
                                             ByVal vstrEncryptionKey As String) As String
            Dim bytValue() As Byte
            Dim bytKey() As Byte
            Dim bytEncoded() As Byte
            Dim bytIV() As Byte = {121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62}
            Dim intLength As Integer
            Dim intRemaining As Integer
            Dim objMemoryStream As New MemoryStream
            Dim objCryptoStream As CryptoStream
            Dim objRijndaelManaged As RijndaelManaged


            '   **********************************************************************
            '   ******  Strip any null character from string to be encrypted    ******
            '   **********************************************************************

            vstrTextToBeEncrypted = StripNullCharacters(vstrTextToBeEncrypted)

            '   **********************************************************************
            '   ******  Value must be within ASCII range (i.e., no DBCS chars)  ******
            '   **********************************************************************

            bytValue = Encoding.ASCII.GetBytes(vstrTextToBeEncrypted.ToCharArray)

            intLength = Len(vstrEncryptionKey)

            '   ********************************************************************
            '   ******   Encryption Key must be 256 bits long (32 bytes)      ******
            '   ******   If it is longer than 32 bytes it will be truncated.  ******
            '   ******   If it is shorter than 32 bytes it will be padded     ******
            '   ******   with upper-case Xs.                                  ****** 
            '   ********************************************************************

            If intLength >= 32 Then
                vstrEncryptionKey = Strings.Left(vstrEncryptionKey, 32)
            Else
                intLength = Len(vstrEncryptionKey)
                intRemaining = 32 - intLength
                vstrEncryptionKey = vstrEncryptionKey & Strings.StrDup(intRemaining, "X")
            End If

            bytKey = Encoding.ASCII.GetBytes(vstrEncryptionKey.ToCharArray)

            objRijndaelManaged = New RijndaelManaged

            '   ***********************************************************************
            '   ******  Create the encryptor and write value to it after it is   ******
            '   ******  converted into a byte array                              ******
            '   ***********************************************************************

            Try

                objCryptoStream = New CryptoStream(objMemoryStream,
                  objRijndaelManaged.CreateEncryptor(bytKey, bytIV),
                  CryptoStreamMode.Write)
                objCryptoStream.Write(bytValue, 0, bytValue.Length)

                objCryptoStream.FlushFinalBlock()

                bytEncoded = objMemoryStream.ToArray
                objMemoryStream.Close()
                objCryptoStream.Close()
            Catch



            End Try

            '   ***********************************************************************
            '   ******   Return encryptes value (converted from  byte Array to   ******
            '   ******   a base64 string).  Base64 is MIME encoding)             ******
            '   ***********************************************************************

            Return Convert.ToBase64String(bytEncoded)

        End Function
        Public Shared Function Decrypt(ByVal vstrStringToBeDecrypted As String,
                                            ByVal vstrDecryptionKey As String) As String

            Dim bytDataToBeDecrypted() As Byte
            Dim bytTemp() As Byte
            Dim bytIV() As Byte = {121, 241, 10, 1, 132, 74, 11, 39, 255, 91, 45, 78, 14, 211, 22, 62}
            Dim objRijndaelManaged As New RijndaelManaged
            Dim objMemoryStream As MemoryStream
            Dim objCryptoStream As CryptoStream
            Dim bytDecryptionKey() As Byte

            Dim intLength As Integer
            Dim intRemaining As Integer
            Dim intCtr As Integer
            Dim strReturnString As String = String.Empty
            Dim achrCharacterArray() As Char
            Dim intIndex As Integer

            '   *****************************************************************
            '   ******   Convert base64 encrypted value to byte array      ******
            '   *****************************************************************
            Try
                bytDataToBeDecrypted = Convert.FromBase64String(vstrStringToBeDecrypted)

            Catch ex As Exception
                Return Nothing
            End Try

            '   ********************************************************************
            '   ******   Encryption Key must be 256 bits long (32 bytes)      ******
            '   ******   If it is longer than 32 bytes it will be truncated.  ******
            '   ******   If it is shorter than 32 bytes it will be padded     ******
            '   ******   with upper-case Xs.                                  ****** 
            '   ********************************************************************

            intLength = Len(vstrDecryptionKey)

            If intLength >= 32 Then
                vstrDecryptionKey = Strings.Left(vstrDecryptionKey, 32)
            Else
                intLength = Len(vstrDecryptionKey)
                intRemaining = 32 - intLength
                vstrDecryptionKey = vstrDecryptionKey & Strings.StrDup(intRemaining, "X")
            End If

            bytDecryptionKey = Encoding.ASCII.GetBytes(vstrDecryptionKey.ToCharArray)

            ReDim bytTemp(bytDataToBeDecrypted.Length)

            objMemoryStream = New MemoryStream(bytDataToBeDecrypted)

            '   ***********************************************************************
            '   ******  Create the decryptor and write value to it after it is   ******
            '   ******  converted into a byte array                              ******
            '   ***********************************************************************

            Try

                objCryptoStream = New CryptoStream(objMemoryStream,
                   objRijndaelManaged.CreateDecryptor(bytDecryptionKey, bytIV),
                   CryptoStreamMode.Read)

                objCryptoStream.Read(bytTemp, 0, bytTemp.Length)

                objCryptoStream.FlushFinalBlock()
                objMemoryStream.Close()
                objCryptoStream.Close()

            Catch

            End Try

            '   *****************************************
            '   ******   Return decypted value     ******
            '   *****************************************

            Return StripNullCharacters(Encoding.ASCII.GetString(bytTemp))

        End Function
        Private Shared Function StripNullCharacters(ByVal vstrStringWithNulls As String) As String

            Dim intPosition As Integer
            Dim strStringWithOutNulls As String

            intPosition = 1
            strStringWithOutNulls = vstrStringWithNulls

            Do While intPosition > 0
                intPosition = InStr(intPosition, vstrStringWithNulls, vbNullChar)

                If intPosition > 0 Then
                    strStringWithOutNulls = Strings.Left(strStringWithOutNulls, intPosition - 1) &
                                      Strings.Right(strStringWithOutNulls, Len(strStringWithOutNulls) - intPosition)
                End If

                If intPosition > strStringWithOutNulls.Length Then
                    Exit Do
                End If
            Loop

            Return strStringWithOutNulls

        End Function
    End Class
    Public Class BasePage
        Inherits System.Web.UI.Page
        Dim _form1 As System.Web.UI.HtmlControls.HtmlForm
        Dim _metaCopyRight As System.Web.UI.HtmlControls.HtmlMeta
        Dim _metaDescr As System.Web.UI.HtmlControls.HtmlMeta
        Dim _metaKeyWords As System.Web.UI.HtmlControls.HtmlMeta
        Dim _stylesheet As System.Web.UI.HtmlControls.HtmlLink
        Dim _body As System.Web.UI.HtmlControls.HtmlGenericControl
        Dim _store As Portal
        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Response.Cache.SetCacheability(HttpCacheability.ServerAndNoCache)
            Response.Cache.SetExpires(DateTime.Now)
            _form1 = Me.FindControl("myform")
            _form1.Action = Request.RawUrl
            _store = ProviderBase.GetSite

            _metaKeyWords = Me.FindControl("metaKeywords")
            _metaKeyWords.Content = _store.Keywords
            _metaDescr = Me.FindControl("metaDescr")
            _metaDescr.Content = _store.Description
            _body = Me.Controls(0).FindControl("mybody")
            _stylesheet = Me.FindControl("stylesheet")
            Me.Title = _store.siteheading

            'AddSEO()


        End Sub
        Public Sub RedirectToLandingPage()
            Response.Redirect(GetAbsoluteUrl(""), True)

        End Sub
        Public Sub setdefaultbutton(ByVal acontrolid As String)
            _form1.DefaultButton = acontrolid

        End Sub
        Public Sub AddSEO(ByVal atitle As String, ByVal adescription As String, ByVal akeywords As String)
            If atitle.Length > 5 Then
                Me.Title += " - " & atitle
            End If

            If adescription.Length > 5 Then
                Me.Description = adescription
            End If

            If akeywords.Length > 5 Then
                Me.KeyWords += akeywords
            End If
        End Sub
        Public ReadOnly Property WebSiteSettings() As Portal
            Get

                Return _store
            End Get

        End Property
        Public ReadOnly Property GetPageName() As String
            Get

                Return Me.Request.QueryString(0).ToString
            End Get

        End Property

        Public ReadOnly Property CurrentUser() As CurrentUser
            Get

                Dim myuser As CurrentUser = TryCast(Me.Session("CurrentUser"), CurrentUser)
                If IsNothing(myuser) Then
                    myuser = New CurrentUser
                End If
                Me.Session("CurrentUser") = myuser
                Return myuser


            End Get

        End Property
        Public ReadOnly Property IsAuthenticated() As Boolean
            Get
                If CurrentUser.Id = -1 Then
                    Return False
                End If
                Return True
            End Get
        End Property
        Public Property KeyWords() As String
            Get
                Return _metaKeyWords.Content
            End Get
            Set(ByVal value As String)
                _metaKeyWords.Content = value
            End Set
        End Property
        Public Property Description() As String
            Get
                Return _metaDescr.Content
            End Get
            Set(ByVal value As String)
                _metaDescr.Content = value
            End Set
        End Property
        Public ReadOnly Property Body() As HtmlControls.HtmlGenericControl
            Get
                Return _body
            End Get
        End Property
        Public ReadOnly Property CurrentStyleSheet() As HtmlControls.HtmlLink
            Get
                Return _stylesheet
            End Get
        End Property
        Public Function LoadUserControl(ByVal placeholderctlname As String, ByVal usercontrolpath As String) As Boolean
            Dim myctl1 As Web.UI.Control = FindControl("content1")
            Dim myplaceholder As PlaceHolder = Me.FindControl(placeholderctlname)
            If IsNothing(myplaceholder) = False Then
                If System.IO.File.Exists(Me.MapPath(usercontrolpath)) = True Then
                    Dim myctl As System.Web.UI.UserControl = Me.LoadControl(usercontrolpath)
                    myplaceholder.Controls.Clear()
                    myplaceholder.Controls.Add(myctl)
                    Return True
                End If
            End If
            Return False

        End Function
        Public Function GetAbsoluteUrl(ByVal apagename As String) As String

            Dim incoming As HttpContext = HttpContext.Current

            Dim Port As String = incoming.Request.ServerVariables("SERVER_PORT")
            If Port Is Nothing OrElse Port = "80" OrElse Port = "443" Then
                Port = ""
            Else
                Port = ":" + Port
            End If

            Dim Protocol As String = incoming.Request.ServerVariables("SERVER_PORT_SECURE")
            If Protocol Is Nothing OrElse Protocol = "0" Then
                Protocol = "http://"
            Else
                Protocol = "https://"
            End If
            Dim s As String = Protocol + incoming.Request.ServerVariables("SERVER_NAME") + Port + incoming.Request.ApplicationPath
            If Right(s, 1) = "/" Then
                s += apagename
            Else
                s += "/" & apagename
            End If
            Return s
        End Function
        Public Sub RedirectToHomePage()
            Me.Page.Response.Redirect(GetAbsoluteUrl(""), True)
        End Sub
        Public Sub RedirectCurrentPage()
            Dim s As String = Me.Page.Request.Url.ToString.ToUpper
            s = Strings.Replace(s, "DEFAULT.ASPX", "")
            s = Strings.Replace(s, "MAINDEFAULT.ASPX", "")
            Me.Page.Response.Redirect(s, True)
        End Sub
        Public Sub SignOnUser(ByVal AUSERid As Integer, ByVal rememberme As Boolean)

            Response.Cookies("CLIENTVIEW")("Data") = AUSERid
            Response.Cookies("CLIENTVIEW")("MachineId") = ProviderBase.GetUniqueId
            Response.Cookies("CLIENTVIEW")("Time") = DateTime.Now.ToString("G")
            Response.Cookies("CLIENTVIEW").Expires = DateTime.Now.AddDays(1)
            System.Web.HttpContext.Current.Session("CurrentUser") = New CurrentUser

            ' Me.RedirectToHomePage()

        End Sub
        Public Sub SignOffUser(ByVal REDIRECT As Boolean)
            Response.Cookies("CLIENTVIEW")("Data") = -1
            Response.Cookies("CLIENTVIEW")("Time") = DateTime.Now.ToString("G")
            Response.Cookies("CLIENTVIEW").Expires = DateTime.Now.AddDays(-1)
            System.Web.HttpContext.Current.Session("CurrentUser") = Nothing
            Session.Abandon()
            Session.Clear()
            If REDIRECT = True Then
                Me.RedirectToHomePage()
            End If

        End Sub
        Public Function GetFileNameFromIframe(ByVal filepath As String) As String
            Dim str As String
            str = ""
            If (filepath.IndexOf("src =") > 1) Then
                Dim str1 As String
                str1 = filepath.Substring(filepath.IndexOf("src = """) + 7)
                str = str1.Substring(0, str1.IndexOf(""""))
            End If
            Return str
        End Function
    End Class
    Public Class BaseUserControl
        Inherits System.Web.UI.UserControl
        Public Event SaveContentClicked()
        Public Event CancelEditClicked()
        Dim _mypage As BasePage
        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

            _mypage = Me.Page
            'If MyPage.CurrentUser.Roles <> "ADMIN" Then
            '    '' MyPage.RedirectToHomePage()
            'End If


        End Sub
        Public ReadOnly Property MyPage() As BasePage
            Get
                Return _mypage
            End Get
        End Property
        Public Sub SaveContent_Clicked()
            RaiseEvent SaveContentClicked()
        End Sub
        Public Sub CancelContent_Clicked()
            RaiseEvent CancelEditClicked()
        End Sub
        Public Sub RedirectToCurrentPage(ByVal aparam As String, ByVal afilename As String)
            Dim qs As New HDS.WEBLIB.Common.QueryString(Me.Page)
            Response.Redirect(qs.All, True)

        End Sub
        Public Sub DownLoadReport(ByVal afilename As String)
            Me.Page.Response.Redirect("DownLoad.axd?" & afilename, True)

        End Sub
        Public Function NoRecordsFound() As Literal
            Dim mylit As New Literal
            Dim s As String = ""
            s += "<h3>No Records Found for the Selection</h3>"
            s += "<hr/>"
            s += "<br/>"
            s += "<center><p class=""normalred"">Revise your Selection and Try Again</p></center>"
            mylit.Text = s
            Return mylit
        End Function
        Public Function NotAvailable() As Literal
            Dim mylit As New Literal
            Dim s As String = ""
            s += "<h3>This Report is Not Available</h3>"
            s += "<hr/>"
            s += "<br/>"
            s += "<center><p class=""normalred"">Revise your Selection and Try Again</p></center>"
            mylit.Text = s
            Return mylit
        End Function
        Public ReadOnly Property CurrentUser() As UI.CurrentUser
            Get
                Return MyPage.CurrentUser
            End Get
        End Property

        Public ReadOnly Property UserInfo() As CRF.CLIENTVIEW.BLL.ClientUserInfo
            Get
                Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(Me.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
                If IsNothing(myuser) Then
                    myuser = New CRF.CLIENTVIEW.BLL.ClientUserInfo(Me.MyPage.CurrentUser.Id)
                End If
                If myuser.UserInfo.ID <> Me.MyPage.CurrentUser.Id Then
                    myuser = New CRF.CLIENTVIEW.BLL.ClientUserInfo(Me.MyPage.CurrentUser.Id)
                End If
                Me.Session("UserInfo") = myuser
                Return myuser

            End Get

        End Property
    End Class
    Public Class BaseAdminUserControl
        Inherits System.Web.UI.UserControl
        Dim _mypage As BasePage
        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            _mypage = Me.Page
            If InStr(MyPage.CurrentUser.Roles, "ADMIN") < 1 Then
                MyPage.RedirectToHomePage()
            End If
        End Sub
        Public ReadOnly Property MyPage() As BasePage
            Get
                Return _mypage
            End Get
        End Property
        Public ReadOnly Property CurrentUser()
            Get
                Return MyPage.CurrentUser
            End Get
        End Property

    End Class
    Public Class BasePageContentUserControl
        Inherits System.Web.UI.UserControl
        Dim _mypage As BasePage
        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            _mypage = Me.Page
            If MyPage.CurrentUser.Roles <> "ADMIN" Then
                MyPage.RedirectToHomePage()
            End If
        End Sub
        Public ReadOnly Property MyPage() As BasePage
            Get
                Return _mypage
            End Get
        End Property
        'Public Sub AddSEO()
        '    If myitem.title.Length > 5 Then
        '        Me.MyPage.Title = myitem.title
        '    End If
        '    If myitem.description.Length > 5 Then
        '        Me.MyPage.Description = myitem.description
        '    End If
        '    If myitem.keywords.Length > 5 Then
        '        Me.MyPage.KeyWords = myitem.keywords
        '    End If

        'End Sub
    End Class
#End Region
#Region "DEFAULTPAGE"
    Public Class DefaultPage
        Inherits BasePage
        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Dim myctl As String = "app_controls/pages/home.ascx"


            If Me.Page.Request.QueryString.Count > 0 Then
                Dim ss As String() = Strings.Split(Me.Page.Request.QueryString(0), ".")
                Dim s As String = "app_controls/pages/" & ss(0) & ".ascx"
                If System.IO.File.Exists(Request.MapPath(s)) = True Then
                    myctl = s
                Else
                    s = "app_controls/admin/" & ss(0) & ".ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        myctl = s
                    End If

                    s = "app_controls/_custom/collectview/" & ss(0) & ".ascx"
                    If System.IO.File.Exists(Request.MapPath(s)) = True Then
                        myctl = s
                    End If

                End If
                ' Else
                ' myctl = "MainPage.aspx"
                'If Not IsDBNull(Session("UserInfo")) Then
                '    Response.Redirect("MainDefault.aspx", False)
                'End If

                'Return
            End If

            Me.LoadUserControl("ContentPlaceHolder", myctl)
            If Me.Page.IsPostBack = False Then
                LogEvent(Me.Session.SessionID, "")
                AddKeepAlive()
            End If
        End Sub
        Public Function LogEvent(ByVal category As String, ByVal message As String) As Integer
            Dim request As HttpRequest = HttpContext.Current.Request
            Dim mylog As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_EventLogs
            With mylog
                .categorytype = category
                .logdate = Now
                .ipaddress = request.ServerVariables("REMOTE_ADDR")
                If IsNothing(request.UrlReferrer) = False Then
                    .urlreferrer = request.UrlReferrer.ToString
                End If
                .url = request.RawUrl.ToString
                .userid = Me.CurrentUser.Id
                .message = message
                .url = request.Url.ToString
                '.enddate = Now
            End With

            CRF.CLIENTVIEW.BLL.LienView.Provider.DAL.Create(mylog)
            Return mylog.PKID

        End Function
        Private Sub AddKeepAlive()
            Dim int_MilliSecondsTimeOut As Integer = 1 * 60 * 1000
            '2 minutes
            'Math.Max((this.Session.Timeout * 60000) - 30000, 5000);
            Dim path As String = VirtualPathUtility.ToAbsolute("~/SessionKeepAlive.aspx")

            Dim str_Script As String = "<script>var refreshCount=0;if (window && window.setInterval)window.setInterval(function() {refreshCount++;var img=new Image(1,1);img.src='" + path + "?count='+refreshCount;}," + int_MilliSecondsTimeOut.ToString() + ");</script>"
            Page.ClientScript.RegisterStartupScript(GetType(Page), UniqueID + "Reconnect", str_Script)
        End Sub
        '<System.Web.Services.WebMethod()>
        'Public Shared Function DeletFileUploads() As String
        '    Dim str As String = ""
        '    If Not HttpContext.Current.Session("UploadedFileName") Is Nothing Then
        '        HttpContext.Current.Session("UploadedFileName") = Nothing
        '    End If
        '    If Not HttpContext.Current.Session("UploadedFileDataBytes") Is Nothing Then
        '        HttpContext.Current.Session("UploadedFileDataBytes") = Nothing
        '    End If

        '    Return str
        'End Function
        <System.Web.Services.WebMethod()>
        Public Shared Function GetAllJobs() As List(Of Job)
            Dim myviewTemp As HDS.DAL.COMMON.TableView
            Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(HttpContext.Current.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
            Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobList1
            obj.Page = 1
            obj.PageRecords = 10000 '500 Changed by jaywanti
            'obj.UserId = "5" ' here hardoded value is there i.e "5"
            obj.UserId = myuser.UserInfo.ID
            myviewTemp = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)

            Dim dt As New System.Data.DataTable
            dt = myviewTemp.Table
            Dim listJobs As New List(Of Job)
            Dim objJob As Job
            For Each dr As System.Data.DataRow In dt.Rows

                objJob = New Job()
                objJob.CRFS = IIf(IsDBNull(dr("JobId").ToString) = True, "", Strings.Left(dr("JobId").ToString, 7))
                objJob.JobName = IIf(IsDBNull(dr("JobName").ToString) = True, "", Strings.Left(dr("JobName").ToString, 20))
                objJob.Job = IIf(IsDBNull(dr("JobNum").ToString) = True, "", Strings.Left(dr("JobNum").ToString, 14))
                objJob.JobAdd = IIf(IsDBNull(dr("CityStateZip").ToString) = True, "", Strings.Left(dr("CityStateZip").ToString, 20))
                objJob.Branch = IIf(IsDBNull(dr("BranchNum").ToString) = True, "", Strings.Left(dr("BranchNum").ToString, 5))
                objJob.Stat = IIf(IsDBNull(dr("StatusCode").ToString) = True, "", Strings.Left(dr("StatusCode").ToString, 5))
                objJob.Customer = IIf(IsDBNull(dr("ClientCustomer").ToString) = True, "", Strings.Left(dr("ClientCustomer").ToString, 15))
                objJob.Assigned = IIf(IsDBNull(dr("DateAssigned").ToString) = True, "", Utils.FormatDate(dr("DateAssigned").ToString))
                objJob.Client = IIf(IsDBNull(dr("ClientCode").ToString) = True, "", dr("ClientCode").ToString)

                listJobs.Add(objJob)
            Next

            Return listJobs
        End Function


        <System.Web.Services.WebMethod()>
        Public Shared Function GetAllTexasReviewJobs() As List(Of Job)
            Dim myviewTemp As HDS.DAL.COMMON.TableView
            Dim myuser As CRF.CLIENTVIEW.BLL.ClientUserInfo = DirectCast(HttpContext.Current.Session("UserInfo"), CRF.CLIENTVIEW.BLL.ClientUserInfo)
            Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetMonthlyJobListJV
            obj.Page = 1
            obj.PageRecords = 10000 '500 Changed by jaywanti
            'obj.UserId = "5" ' here hardoded value is there i.e "5"
            obj.UserId = myuser.UserInfo.ID
            obj.JobState = myuser.UserInfo.LastJobState
            myviewTemp = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(obj)

            Dim dt As New System.Data.DataTable
            dt = myviewTemp.Table
            Dim listJobs As New List(Of Job)
            Dim objJob As Job
            For Each dr As System.Data.DataRow In dt.Rows

                objJob = New Job()
                objJob.CRFS = IIf(IsDBNull(dr("JobId").ToString) = True, "", Strings.Left(dr("JobId").ToString, 7))
                objJob.JobName = IIf(IsDBNull(dr("JobName").ToString) = True, "", Strings.Left(dr("JobName").ToString, 20))
                objJob.Job = IIf(IsDBNull(dr("JobNum").ToString) = True, "", Strings.Left(dr("JobNum").ToString, 14))
                objJob.JobAdd = IIf(IsDBNull(dr("CityStateZip").ToString) = True, "", Strings.Left(dr("CityStateZip").ToString, 20))
                objJob.Branch = IIf(IsDBNull(dr("BranchNum").ToString) = True, "", Strings.Left(dr("BranchNum").ToString, 5))
                objJob.Stat = IIf(IsDBNull(dr("StatusCode").ToString) = True, "", Strings.Left(dr("StatusCode").ToString, 5))
                objJob.Customer = IIf(IsDBNull(dr("ClientCustomer").ToString) = True, "", Strings.Left(dr("ClientCustomer").ToString, 15))
                objJob.Assigned = IIf(IsDBNull(dr("DateAssigned").ToString) = True, "", Utils.FormatDate(dr("DateAssigned").ToString))
                objJob.Client = IIf(IsDBNull(dr("ClientCode").ToString) = True, "", dr("ClientCode").ToString)

                listJobs.Add(objJob)
            Next

            Return listJobs
        End Function







        <System.Web.Services.WebMethod()>
        Public Shared Function CustSearchList(ByVal StrSearch As String, ByVal type As String, ByVal ClientId As String) As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            Dim myview As HDS.DAL.COMMON.TableView
            If type.ToUpper() = "NAME" Then
                Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetCustomerByNameforClient
                MYSPROC.ClientId = ClientId
                MYSPROC.Reference = StrSearch & "%"
                myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
                myview.Sort = "Name"
            Else
                Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetCustomerByReferenceforClient
                MYSPROC.ClientId = ClientId
                MYSPROC.Reference = StrSearch & "%"
                myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
                myview.Sort = "Name"
            End If

            If myview.Count = 0 Then
                Return objList
            End If

            myview.MoveFirst()
            Do Until myview.EOF
                Dim LI As New System.Web.UI.WebControls.ListItem
                LI.Text = Trim(myview.RowItem("Name")) & ", "
                LI.Text += Trim(myview.RowItem("Address"))
                LI.Text += ",( " & Trim(myview.RowItem("Ref")) & " ) "
                LI.Value = myview.RowItem("ID")
                objList.Add(LI)
                myview.MoveNext()
            Loop

            'Me.Session("CustSearchView") = myview
            Return objList
        End Function

        <System.Web.Services.WebMethod()>
        Public Shared Function GetCustomer(ByVal custId As String) As Object
            Dim myid As String = Strings.Replace(custId, "C", "")
            myid = Strings.Replace(myid, "B", "")
            If Left(custId, 1) = "B" Then
                Dim mynewcust As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mynewcust)
                mynewcust.CustName = Left(mynewcust.CustName, 50)
                mynewcust.CustPhone1 = Utils.FormatPhoneNo(mynewcust.CustPhone1)
                mynewcust.GCPhone2 = Utils.FormatPhoneNo(mynewcust.GCPhone2)
                Return mynewcust
            Else
                Dim mycust As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientCustomer
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mycust)
                mycust.ClientCustomer = Left(mycust.ClientCustomer, 50)
                mycust.Telephone1 = Utils.FormatPhoneNo(mycust.Telephone1)
                mycust.Fax = Utils.FormatPhoneNo(mycust.Fax)
                Return mycust
            End If
        End Function


        <System.Web.Services.WebMethod()>
        Public Shared Function GetGCNames(ByVal txtGC As String, ByVal ClientId As String) As List(Of System.Web.UI.WebControls.ListItem)
            If ClientId = "" Then
                ClientId = "0"
            End If
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)

            Dim aOut As ArrayList = New ArrayList
            Dim myview As HDS.DAL.COMMON.TableView
            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetGeneralContractorList
            MYSPROC.ClientId = ClientId
            MYSPROC.Name = txtGC & "%"
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
            myview.Sort = "Name"
            If myview.Count = 0 Then
                Return objList
            End If

            myview.MoveFirst()
            Do Until myview.EOF
                Dim LI As New System.Web.UI.WebControls.ListItem
                LI.Text = Trim(myview.RowItem("Name")) & ", "
                LI.Text += Trim(myview.RowItem("Address"))
                LI.Value = myview.RowItem("ID")
                objList.Add(LI)
                myview.MoveNext()
            Loop

            Return objList
        End Function
        <System.Web.Services.WebMethod()>
        Public Shared Function FillClientListByUser(ByVal Id As Integer) As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            If Not HttpContext.Current.Session("CClientList") Is Nothing Then
                objList = HttpContext.Current.Session("CClientList")
            Else
                Dim VWCLIENTS As DataView = DirectCast(CRFS.ClientView.CrfsBll.GetClientListForUser(Id), DataSet).Tables(0).DefaultView

                VWCLIENTS.Sort = "ClientName"
                For Each rowView As DataRowView In VWCLIENTS
                    Dim row As DataRow = rowView.Row
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    LI.Text = Trim(row("ClientName"))
                    LI.Value = row("ClientId")
                    objList.Add(LI)
                Next

                HttpContext.Current.Session("CClientList") = objList
            End If

            Return objList
        End Function

        <System.Web.Services.WebMethod()>
        Public Shared Function GetClientList(ByVal Id As Integer) As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            If Not HttpContext.Current.Session("ClientList") Is Nothing Then
                objList = HttpContext.Current.Session("ClientList")
            Else
                Dim VWCLIENTS As HDS.DAL.COMMON.TableView = LienView.Provider.GetClientListForUser(Id)
                VWCLIENTS.MoveFirst()
                VWCLIENTS.Sort = "ClientName"
                Do Until VWCLIENTS.EOF
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    LI.Text = Trim(VWCLIENTS.RowItem("ClientName"))
                    LI.Value = VWCLIENTS.RowItem("ClientId")
                    objList.Add(LI)
                    VWCLIENTS.MoveNext()
                Loop
                HttpContext.Current.Session("ClientList") = objList
            End If

            Return objList
        End Function
        <System.Web.Services.WebMethod()>
        Public Shared Function FillStatusList(ByVal Id As Integer) As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            If Not HttpContext.Current.Session("StatusList") Is Nothing Then
                objList = HttpContext.Current.Session("StatusList")
            Else
                Dim myforms As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.CollectView.Provider.GetCollectionStatus
                myforms.MoveFirst()
                myforms.Sort = "CollectionStatus"
                Do Until myforms.EOF
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    LI.Text = Trim(myforms.RowItem("CollectionStatus")) + "-" + Trim(myforms.RowItem("CollectionStatusDescr"))
                    LI.Value = myforms.RowItem("CollectionStatusId")
                    objList.Add(LI)
                    myforms.MoveNext()
                Loop
                HttpContext.Current.Session("StatusList") = objList
            End If

            Return objList

        End Function
        <System.Web.Services.WebMethod()>
        Public Shared Function GetStateList() As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            If Not HttpContext.Current.Session("StateList") Is Nothing Then
                objList = HttpContext.Current.Session("StateList")
            Else
                Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
                Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
                Dim VWSTATES As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateTable
                VWSTATES.MoveFirst()
                Do Until VWSTATES.EOF
                    mystate = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
                    mystate = VWSTATES.FillEntity(mystate)
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    LI.Text = mystate.StateName
                    LI.Value = mystate.StateInitials
                    objList.Add(LI)
                    VWSTATES.MoveNext()
                Loop
                HttpContext.Current.Session("StateList") = objList
            End If



            Return objList
        End Function
        <System.Web.Services.WebMethod()>
        Public Shared Function GetSidemenuHtml() As String
            Dim obj As New HDS.WEBSITE.UI.BasePage
            Dim myuser As UI.CurrentUser = obj.CurrentUser
            Dim oBuilder As New System.Text.StringBuilder()
            'oBuilder.AppendLine("<div class=""menu"" >")

            oBuilder.AppendLine("<ul class=""site-menu"">")
            Dim myidx As Integer = 0
            For Each oItem As UI.MenuItem In myuser.UserMenu.Items
                myidx += 1
                Dim s As String
                If oItem.Items.Count > 0 Then
                    ''s = "<li class=""site-menu-item has-sub"" id=""[Linkid]""><a href=""javascript:void(0)""><i class=""site-menu-icon wb-layout"" aria-hidden=""true""></i> <span class=""site-menu-title"">[TITLE]</span><span class=""site-menu-arrow""></span></a>"
                    ''Modified by kedar

                    s = "<li class=""site-menu-item has-sub"" style=""background-color:#152e4f;"" id=""[Linkid]""><a href=""[LINK]""><i class=""[IconClass]"" aria-hidden=""true""></i> <span class=""site-menu-title"">[TITLE]</span><span class=""site-menu-arrow""></span></a>"
                Else
                    If oItem.LinkId <> "" Then
                        s = "<li class=""site-menu-item"" style=""background-color:#152e4f;"" id=""[Linkid]""><a href=""[LINK]"" class=""animsition-link""><i class=""[IconClass]"" aria-hidden=""true""></i> <span class=""site-menu-title"">[TITLE]</span></a>"
                    Else
                        s = "<li class=""site-menu-category"" id=""[Linkid]"" >[TITLE]"
                    End If


                End If
                s = Strings.Replace(s, "[TITLE]", oItem.Text)
                s = Strings.Replace(s, "[Linkid]", oItem.LinkId)
                s = Strings.Replace(s, "[LINK]", oItem.Link)
                s = Strings.Replace(s, "[IconClass]", oItem.IconClass)
                oBuilder.Append(s)
                If oItem.Items.Count > 0 Then

                    Dim oBuilder1 As New System.Text.StringBuilder()
                    Dim myidx1 As Integer = 0
                    oBuilder1.AppendLine("<ul  class=""site-menu-sub"">")
                    For Each oItem1 As UI.MenuItem In oItem.Items
                        myidx1 += 1
                        myidx1 += 1
                        Dim s1 As String = "<li class=""site-menu-item"" style=""background-color:#152e4f;"" id=""[Linkid]"" ><a class=""animsition-link"" href=""[URL]"" rel=""[SUBID]""><span class=""site-menu-title"">[TITLE]</span></a>"
                        If oItem1.Items.Count > 0 Then
                            s1 = "<li class=""site-menu-item has-sub"" style=""background-color:#152e4f;""  id=""[Linkid]""><a class=""animsition-link"" href=""[URL]"" rel=""[SUBID]""><span class=""site-menu-title"">[TITLE]</span><span class=""site-menu-arrow""></span></a>"
                        End If
                        s1 = Strings.Replace(s1, "[URL]", oItem1.Link)
                        s1 = Strings.Replace(s1, "[TITLE]", oItem1.Text)
                        s1 = Strings.Replace(s1, "[Linkid]", oItem1.LinkId)
                        s1 = Strings.Replace(s1, "[SUBID]", "submenu-" & myidx1)
                        oBuilder1.AppendLine(s1)
                        If oItem1.Items.Count > 0 Then
                            oBuilder1.AppendLine("<ul class=""site-menu-sub"">")


                            Dim oBuilder2 As New System.Text.StringBuilder()
                            Dim myidx2 As Integer = 0
                            For Each oItem2 As UI.MenuItem In oItem1.Items
                                myidx2 += 1
                                Dim s2 As String = "<li class=""site-menu-item"" style=""background-color:#152e4f;"" id=""[Linkid]""><a class=""animsition-link"" href=""[URL]"" rel=""[SUBID]""><span class=""site-menu-title"">[TITLE]</span></a>"
                                s2 = Strings.Replace(s2, "[URL]", oItem2.Link)
                                s2 = Strings.Replace(s2, "[TITLE]", oItem2.Text)
                                s2 = Strings.Replace(s2, "[Linkid]", oItem2.LinkId)
                                s2 = Strings.Replace(s2, "[SUBID]", "submenu-" & myidx2)
                                oBuilder2.AppendLine(s2)
                                oBuilder2.AppendLine("</li>")
                            Next

                            oBuilder1.AppendLine(oBuilder2.ToString())
                            oBuilder1.AppendLine("</ul>")
                        End If
                        oBuilder1.AppendLine("</li>")

                    Next
                    oBuilder1.AppendLine("</ul>")
                    oBuilder.AppendLine(oBuilder1.ToString())
                End If

                oBuilder.AppendLine("</li>")

            Next
            oBuilder.AppendLine("</ul>")
            'oBuilder.AppendLine("</div>")

            Return oBuilder.ToString()


        End Function

        <System.Web.Services.WebMethod()>
        Public Shared Function GetSignerList() As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            If Not HttpContext.Current.Session("SignerList") Is Nothing Then
                objList = HttpContext.Current.Session("SignerList")
            Else

                Dim VWSIGNERS As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetClientSigners
                VWSIGNERS.MoveFirst()
                Do Until VWSIGNERS.EOF
                    Dim csg As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientSigner
                    csg = VWSIGNERS.FillEntity(csg)
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    LI.Value = csg.Id
                    LI.Text = csg.Signer & "," & csg.SignerTitle
                    objList.Add(LI)
                    VWSIGNERS.MoveNext()
                Loop



                HttpContext.Current.Session("SignerList") = objList
            End If

            Return objList
        End Function
        <System.Web.Services.WebMethod()>
        Public Shared Function GetMailToList(ByVal Jobid As String) As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            '  Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
            Dim aOut As ArrayList = New ArrayList
            Dim myview As HDS.DAL.COMMON.TableView
            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
            MYSPROC.JobId = Jobid
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
            'Dim strquery As String = "IsPrimary=1"
            'myview.RowFilter = strquery

            If myview.Count = 0 Then
                Return objList
            End If

            myview.MoveFirst()
            Do Until myview.EOF
                Dim LI As New System.Web.UI.WebControls.ListItem

                LI.Value = Trim(myview.RowItem("Id")) & "-" & Trim(myview.RowItem("TypeCode"))
                LI.Text = Trim(myview.RowItem("TypeCode")) & "-" & Trim(myview.RowItem("AddressName"))
                objList.Add(LI)
                myview.MoveNext()
            Loop

            'For count = 0 To dtjobinfo.Rows.Count - 1
            '            Dim LI2 As New System.Web.UI.WebControls.ListItem
            '            LI2.Value = dtjobinfo.Rows(count)("Id") & "-" & dtjobinfo.Rows(count)("TypeCode")
            '            LI2.Text = dtjobinfo.Rows(count)("TypeCode") & "-" & dtjobinfo.Rows(count)("AddressName")

            '        Next

            Return objList
        End Function

        <System.Web.Services.WebMethod()>
        Public Shared Function GetMailCheckList(ByVal Jobid As String) As List(Of System.Web.UI.WebControls.ListItem)
            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            '  Dim obj As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
            Dim aOut As ArrayList = New ArrayList
            Dim myview As HDS.DAL.COMMON.TableView
            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetJobLegalParties
            MYSPROC.JobId = Jobid
            myview = CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.GetTableView(MYSPROC)
            'Dim strquery As String = "IsPrimary=1"
            'myview.RowFilter = strquery

            If myview.Count = 0 Then
                Return objList
            End If

            myview.MoveFirst()
            Do Until myview.EOF
                Dim LI As New System.Web.UI.WebControls.ListItem

                LI.Value = Trim(myview.RowItem("Id")) & "-" & Trim(myview.RowItem("TypeCode"))
                LI.Text = Trim(myview.RowItem("TypeCode")) & "-" & Trim(myview.RowItem("AddressName"))
                objList.Add(LI)
                myview.MoveNext()
            Loop

            'For count = 0 To dtjobinfo.Rows.Count - 1
            '            Dim LI2 As New System.Web.UI.WebControls.ListItem
            '            LI2.Value = dtjobinfo.Rows(count)("Id") & "-" & dtjobinfo.Rows(count)("TypeCode")
            '            LI2.Text = dtjobinfo.Rows(count)("TypeCode") & "-" & dtjobinfo.Rows(count)("AddressName")

            '        Next

            Return objList
        End Function
        <System.Web.Services.WebMethod()>
        Public Shared Sub SetSortingJobList(ByVal SortCol As String, ByVal SortDir As String)

            HttpContext.Current.Session("SortCol") = SortCol
            HttpContext.Current.Session("SortDir") = SortDir

        End Sub
        <System.Web.Services.WebMethod()>
        Public Shared Function GetWaiverTypeList(ByVal JobState As String) As List(Of System.Web.UI.WebControls.ListItem)

            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            If Not HttpContext.Current.Session("WaiverList") Is Nothing Then
                If Not HttpContext.Current.Session("JobState") Is Nothing And HttpContext.Current.Session("JobState") = JobState Then
                    objList = HttpContext.Current.Session("WaiverList")
                Else
                    Dim myforms As HDS.DAL.COMMON.TableView
                    myforms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverStateForms(JobState)
                    Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateForms
                    myforms.MoveFirst()
                    Do Until myforms.EOF
                        Dim LI As New System.Web.UI.WebControls.ListItem
                        myform = myforms.FillEntity(myform)
                        LI.Value = myform.Id
                        LI.Text = myform.FormCode & "-" & myform.Description
                        objList.Add(LI)
                        myforms.MoveNext()
                    Loop
                    HttpContext.Current.Session("JobState") = JobState
                    HttpContext.Current.Session("WaiverList") = objList
                End If

            Else
                Dim myforms As HDS.DAL.COMMON.TableView
                myforms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetWaiverStateForms(JobState)
                Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateForms
                myforms.MoveFirst()
                Do Until myforms.EOF
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    myform = myforms.FillEntity(myform)
                    LI.Value = myform.Id
                    LI.Text = myform.FormCode & "-" & myform.Description
                    objList.Add(LI)
                    myforms.MoveNext()
                Loop
                HttpContext.Current.Session("JobState") = JobState
                HttpContext.Current.Session("WaiverList") = objList
            End If

            'Dim li As New ListItem
            'Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
            'Dim mystate As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
            'Dim VWSTATES As HDS.DAL.COMMON.TableView = CRF.CLIENTVIEW.BLL.LienView.Provider.GetStateTable
            'VWSTATES.MoveFirst()
            'Do Until VWSTATES.EOF
            '    li = New ListItem
            '    mystate = New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateInfo
            '    mystate = VWSTATES.FillEntity(mystate)
            '    li.Value = mystate.StateInitials
            '    li.Text = mystate.StateName
            '    Me.StateTableId.Items.Add(li)
            '    VWSTATES.MoveNext()
            'Loop


            Return objList
        End Function

        <System.Web.Services.WebMethod()>
        Public Shared Function GetFormTypeList(ByVal JobState As String) As List(Of System.Web.UI.WebControls.ListItem)

            Dim objList As New List(Of System.Web.UI.WebControls.ListItem)
            If Not HttpContext.Current.Session("FormTypeList") Is Nothing Then
                If Not HttpContext.Current.Session("JobStateForm") Is Nothing And HttpContext.Current.Session("JobStateForm") = JobState Then
                    objList = HttpContext.Current.Session("FormTypeList")
                Else
                    Dim myforms As HDS.DAL.COMMON.TableView
                    myforms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetOtherStateForms(JobState)
                    myforms.Sort = "FriendlyName"
                    Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateFormNotices
                    myforms.MoveFirst()
                    Do Until myforms.EOF
                        Dim LI As New System.Web.UI.WebControls.ListItem
                        myform = myforms.FillEntity(myform)
                        LI.Value = myform.StateFormNoticeId
                        LI.Text = myform.FriendlyName
                        objList.Add(LI)
                        myforms.MoveNext()
                    Loop
                    HttpContext.Current.Session("JobStateForm") = JobState
                    HttpContext.Current.Session("FormTypeList") = objList
                End If

            Else
                Dim myforms As HDS.DAL.COMMON.TableView
                myforms = CRF.CLIENTVIEW.BLL.LienView.Provider.GetOtherStateForms(JobState)
                myforms.Sort = "FriendlyName"
                Dim myform As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.StateFormNotices
                myforms.MoveFirst()
                Do Until myforms.EOF
                    Dim LI As New System.Web.UI.WebControls.ListItem
                    myform = myforms.FillEntity(myform)
                    LI.Value = myform.StateFormNoticeId
                    LI.Text = myform.FriendlyName
                    objList.Add(LI)
                    myforms.MoveNext()
                Loop
                HttpContext.Current.Session("JobStateForm") = JobState
                HttpContext.Current.Session("FormTypeList") = objList
            End If



            Return objList
        End Function

        <System.Web.Services.WebMethod()>
        Public Shared Function GetGCDetails(ByVal GCString As String, ByVal clientId As String) As Object
            Dim myid As String = Strings.Replace(GCString, "C", "")
            myid = Strings.Replace(myid, "B", "")
            If Left(GCString, 1) = "B" Then
                Dim mynewJob As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.BatchJob
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mynewJob)
                mynewJob.GCName = Left(mynewJob.GCName, 50)
                mynewJob.GCPhone1 = Utils.FormatPhoneNo(mynewJob.GCPhone1)
                mynewJob.GCFax = Utils.FormatPhoneNo(mynewJob.GCFax)
                Return mynewJob

                'Me.ViewState("gcid") = -1
            Else
                Dim mygc As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.ClientGeneralContractor
                CRF.CLIENTVIEW.BLL.CRFDB.Provider.DBO.Read(myid, mygc)

                mygc.Telephone1 = Utils.FormatPhoneNo(mygc.Telephone1)
                mygc.Telephone2 = Utils.FormatPhoneNo(mygc.Telephone2)

                Return mygc
            End If
        End Function
    End Class


    Public Class Job
        Public CRFS As String
        Public JobName As String
        Public Job As String
        Public JobAdd As String
        Public Branch As String
        Public Stat As String
        Public Customer As String
        Public Assigned As String
        Public Client As String

        Public Sub New()
            CRFS = ""
            JobName = ""
            Job = ""
            JobAdd = ""
            Branch = ""
            Stat = ""
            Customer = ""
            Assigned = ""
            Client = ""
        End Sub

    End Class

#End Region
End Namespace
Namespace Data
#Region "TexasRequests"
    Public Class TexasRequestProvider
        Inherits ProviderBase
        Public Shared Function GetRequests(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As System.Data.DataTable
            Dim obj As System.Web.HttpContext = System.Web.HttpContext.Current
            Dim myuser As HDS.WEBSITE.UI.CurrentUser = obj.Session("CurrentUser")
            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests
            With MYSPROC
                .FromDate = "01/01/1980"
                .ThruDate = Today
                .UserId = myuser.Id
                .SelectPending = True
            End With
            Return DAL.GetDataSet(MYSPROC).Tables(0)

        End Function
        Public Shared Function GetEmployeeItem(ByVal pkid As String) As Portal_Employee
            Dim myitem As New Portal_Employee
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
    End Class
#End Region
#Region "Management"
    Public Class ManagementProvider
        Inherits ProviderBase
        Public Shared Function GetEmployees(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_Employee)

            Dim myitem As New Portal_Employee
            Dim myitems As New List(Of Portal_Employee)

            '9/24/2014 - added display order
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, "IsVisible=1", "DisplayOrder")
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Employee
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next

            Return myitems

        End Function
        Public Shared Function GetEmployeeItem(ByVal pkid As String) As Portal_Employee
            Dim myitem As New Portal_Employee
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
    End Class
#End Region
#Region "Sales"
    Public Class SalesProvider
        Inherits ProviderBase
        Public Shared Function GetEmployees(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_Employee)

            Dim myitem As New Portal_Employee
            Dim myitems As New List(Of Portal_Employee)

            '9/24/2014 - added display order
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, "IsMarketing=1", "DisplayOrder")
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Employee
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next

            Return myitems

        End Function
        Public Shared Function GetEmployeeItem(ByVal pkid As String) As Portal_Employee
            Dim myitem As New Portal_Employee
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
    End Class
#End Region
#Region "users"
    Public Class UsersProvider
        Inherits ProviderBase
        Public Shared Function GetUsers(ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As List(Of Portal_Users)
            Dim myitem As New Portal_Users
            Dim myitems As New List(Of Portal_Users)

            '9/24/2014 - added display order
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, "isinactive=0", "ID")
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Users
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next

            Return myitems
        End Function
    End Class
#End Region
#Region "Employees"
    Public Class EmployeeProvider
        Inherits ProviderBase
        Public Shared Function GetEmployees(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_Employee)

            Dim myitem As New Portal_Employee
            Dim myitems As New List(Of Portal_Employee)

            '9/24/2014 - added display order
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, "IsVisible=1", "DisplayOrder")
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Employee
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next

            Return myitems

        End Function
        Public Shared Function GetEmployeeItem(ByVal pkid As String) As Portal_Employee
            Dim myitem As New Portal_Employee
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
    End Class
#End Region
#Region "News"
    Public Class NewsProvider
        Inherits ProviderBase

        Public Shared Function GetNews(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_News)

            Dim myitem As New Portal_News
            Dim myitems As New List(Of Portal_News)
            'Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, "IsVisible=1 and IsClientView = 1", asort)
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, "IsVisible=1 and IsClientView = 1", "DATECREATED DESC") '9-11-2011
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_News
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next
            Return myitems

        End Function
        Public Shared Function GetNewsItem(ByVal pkid As String) As Portal_News
            Dim myitem As New Portal_News
            DAL.Read(pkid, myitem)
            Return myitem
        End Function

        Public Shared Function GetEmployees(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_News)

            Dim myitem As New Portal_News
            Dim myitems As New List(Of Portal_News)

            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, "IsVisible=1 and IsEmployeeProfile = 1", asort)
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_News
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next

            Return myitems

        End Function
        Public Shared Function GetEmployeeItem(ByVal pkid As String) As Portal_News
            Dim myitem As New Portal_News
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
    End Class
#End Region

#Region "Report Provider"
    Public Class ReportsProvider
        Inherits ProviderBase
        Public Shared Function GetReports(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As System.Data.DataTable
            'Dim MYSPROC As New CRFDB.SPROCS.Portal_GetReportHistoryforClient
            Dim obj As System.Web.HttpContext = System.Web.HttpContext.Current
            Dim myuser As HDS.WEBSITE.UI.CurrentUser = obj.Session("CurrentUser")

            Dim MYSPROC As New CRF.CLIENTVIEW.BLL.CRFDB.SPROCS.Portal_GetReportHistoryforUserId
            MYSPROC.UserId = myuser.Id
            MYSPROC.CutoffDate = DateAdd(DateInterval.Day, -10, Today)
            Return DAL.GetDataSet(MYSPROC).Tables(0)
        End Function

    End Class
#End Region
#Region "Quotes"
    Public Class QuotesProvider
        Inherits ProviderBase
        Public Shared Function GetQuotes(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_Quotes)
            Dim myitem As New Portal_Quotes

            Dim myitems As New List(Of Portal_Quotes)
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, maxrows, afilter, asort)
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Quotes
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next

            Return myitems

        End Function
        Public Shared Function GetQuotesItem(ByVal pkid As String) As Portal_Quotes
            Dim myitem As New Portal_Quotes
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
        Public Shared Function GetRandomQuote() As Portal_Quotes
            Dim myitem As New Portal_Quotes
            Dim mysql As String = "Select Max(pkid) as maxid from Portal_quote"
            Dim mydr As DataRow = DAL.GetDataSet(mysql).Tables(0).Rows(0)
            mysql = "Select min(pkid) as minid from Portal_quote"
            Dim mydr1 As DataRow = DAL.GetDataSet(mysql).Tables(0).Rows(0)

            Dim myrand As New Random
            Dim myrand1 As Integer = myrand.Next(mydr1(0), mydr(0))
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 1, "PKID <=" & myrand1, "PKID DESC")
            If mydt.Rows.Count > 0 Then
                DAL.FillEntity(mydt.Rows(0), myitem)
            End If

            Return myitem
        End Function
    End Class
#End Region
#Region "Testimonials"
    Public Class TestimonialsProvider
        Inherits ProviderBase
        Public Shared Function GetTestimonials(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_Testimonials)
            Dim myitem As New Portal_Testimonials
            Dim myitems As New List(Of Portal_Testimonials)
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, maxrows, afilter, asort)
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Testimonials
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next

            Return myitems

        End Function
        Public Shared Function GetTestimonialsItem(ByVal pkid As String) As Portal_Testimonials
            Dim myitem As New Portal_Testimonials
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
        Public Shared Function GetRandomTestimonial() As Portal_Testimonials
            Dim myitem As New Portal_Testimonials
            Dim mysql As String = "Select Max(pkid) as maxid from Portal_testimonial"
            Dim mydr As DataRow = DAL.GetDataSet(mysql).Tables(0).Rows(0)
            mysql = "Select min(pkid) as minid from Portal_testimonial"
            Dim mydr1 As DataRow = DAL.GetDataSet(mysql).Tables(0).Rows(0)

            Dim myrand As New Random
            Dim myrand1 As Integer = myrand.Next(mydr1(0), mydr(0))
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 1, "PKID <=" & myrand1, "PKID DESC")
            If mydt.Rows.Count > 0 Then
                DAL.FillEntity(mydt.Rows(0), myitem)
            End If

            Return myitem

        End Function
    End Class
#End Region

#Region "Content"
    Public Class ContentProvider
        Inherits ProviderBase
        Public Shared Function GetContent(ByVal maxrows As Integer, ByVal afilter As String, ByVal asort As String) As List(Of Portal_Content)
            Dim myitem As New Portal_Content
            Dim myitems As New List(Of Portal_Content)
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 0, afilter, asort)
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Content
                DAL.FillEntity(dr, myitem)
                myitems.Add(myitem)
            Next
            Return myitems

        End Function
        Public Shared Function GetContentItem(ByVal pkid As String) As Portal_Content
            Dim myitem As New Portal_Content
            DAL.Read(pkid, myitem)
            Return myitem
        End Function
        Public Shared Function GetContentPageUrl(ByVal pageurl As String) As Portal_Content
            Dim myitem As New Portal_Content
            Dim mydt As DataTable = GetDataTable(myitem.TableObjectName, 1, "category='page' and url='" & pageurl & "'", "")
            For Each dr As DataRow In mydt.Rows
                myitem = New Portal_Content
                DAL.FillEntity(dr, myitem)
            Next

            Return myitem
        End Function
    End Class
    Public Class MembershipProvider
        Public Shared Function ValidateUserByLoginCode(ByVal logincode As String, ByVal loginpassword As String, ByRef user As Portal_Users) As String

            user = GetUserByLogin(logincode)
            If user.ID < 1 Then
                Return "Id Not Is Not On File"
            End If
            If UCase(user.LoginPassword.Trim.ToUpper) <> UCase(loginpassword.Trim.ToUpper) Then
                Return "Invalid Password"
            End If


            Return String.Empty

        End Function
        Public Shared Function GetCurrentUser() As CurrentUser

            Dim myuser As CurrentUser
            myuser = TryCast(System.Web.HttpContext.Current.Session("CurrentUser"), CurrentUser)
            If IsNothing(myuser) Then
                myuser = New CurrentUser
            End If
            System.Web.HttpContext.Current.Session("CurrentUser") = myuser
            System.Web.HttpContext.Current.Session("UserId") = myuser.Id
            Return myuser

        End Function
        Public Shared Function GetUserByEmail(ByVal email As String) As Portal_Users
            Dim myitem As New Portal_Users
            Dim myfilter = ""
            myfilter += " Email='" & email & "' "
            DAL.GetItem(DAL.GetSQL(myitem.TableObjectName, 1, myfilter, ""), myitem)
            Return myitem

        End Function
        Public Shared Function GetUserByLogin(ByVal logincode As String) As Portal_Users
            Dim myitem As New Portal_Users
            Dim myfilter = ""
            myfilter += " LoginCode='" & logincode & "' "
            DAL.GetItem(DAL.GetSQL(myitem.TableObjectName, 1, myfilter, ""), myitem)
            Return myitem

        End Function

        Public Shared Function GetUser(ByVal email As String, ByVal loginpassword As String) As Portal_Users

            Dim myitem As New Portal_Users

            Dim myfilter = ""
            myfilter += " Email='" & email & "' "
            myfilter += " And LoginPassword='" & loginpassword & "' "
            DAL.GetItem(DAL.GetSQL(myitem.TableObjectName, 1, myfilter, ""), myitem)

            Return myitem

        End Function
    End Class
#End Region


End Namespace

