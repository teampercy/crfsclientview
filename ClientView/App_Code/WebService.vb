Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class WebService
    Inherits System.Web.Services.WebService
    <WebMethod()> _
    Public Function GetInfo(ByVal xmlstring As String) As String
        Return CRF.CLIENTVIEW.BLL.WebService.Provider.DoTask(xmlstring)

    End Function

End Class
