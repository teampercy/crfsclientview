﻿Imports System.Net.Mail
Partial Class TestMail
    Inherits System.Web.UI.Page

    Protected Sub btnSendEmail_Click(sender As Object, e As EventArgs)
        Dim msg As New MailMessage

        msg.From = New MailAddress(txtFromEmail.Text, "ClientView")
        'msg.From = New MailAddress("jaywanti.jain@rhealtech.com", "ClientView")
        msg.Subject = "Test Mail"
        msg.To.Add(txtToEmail.Text)
        msg.IsBodyHtml = True
        'msg.Body = body
        msg.Body = txtBody.Text
        Try
            Dim _smtp As New SmtpClient(txtRemoteHost.Text)
            If IsNothing(txtusername.Text) = False And Len(txtusername.Text) > 5 Then
                _smtp.Port = txtportno.Text
                _smtp.Credentials = New System.Net.NetworkCredential(txtusername.Text, txtPassword.Text)
                _smtp.EnableSsl = chkEnable.Checked
            End If

            '_smtp.UseDefaultCredentials = False
            _smtp.Send(msg)
            _smtp.Dispose()
            lblErrorMessage.Text = "Mail Successfully Sent"
        Catch ex As Exception
            lblErrorMessage.Text = "Sending Mail Fail:-" + ex.Message
            Dim mymsg As String = ex.Message
            If IsNothing(ex.InnerException) = False Then
                mymsg += vbCrLf & ex.InnerException.Message
            End If
            'MsgBox("error on email:" & vbCrLf & mymsg)

        End Try
    End Sub
End Class
