<%@ Control Language="VB" AutoEventWireup="false" Inherits="HDS.WEBSITE.UI.BaseUserControl" %>
<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>

<style type="text/css">
    .style2 {
        font-family: Arial, Helvetica, sans-serif;
    }

    .style3 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: large;
    }

    .style6 {
        font-family: Arial, Helvetica, sans-serif;
        font-style: italic;
        font-size: small;
        font-weight: bold;
    }

    .style7 {
        font-family: Arial, Helvetica, sans-serif;
        font-size: small;
    }
</style>
<script type="text/javascript">

    function MaintainMenuOpen() {
        debugger;
        var PageName = window.location.search.substring(1);
        if (PageName == "help.lienresources.JobView") {
            SetHeaderBreadCrumb('RentalView', 'Resources', 'Lien Summary (Private)');
            MainMenuToggle('liRentalViewResources');
            SubMenuToggle('liLienPrivateJobView');
        }
        else {
            SetHeaderBreadCrumb('LienView', 'Resources', 'Lien Summary (Private)');
            MainMenuToggle('liLienViewResources');
            SubMenuToggle('liLienPrivate');
        }
       
        //$('#liLienViewResources').addClass('site-menu-item has-sub active open');
        //$('#liLienPrivate').addClass('site-menu-item active');
        return false;

    }
  
</script>

                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <div class="col-md-12 style3" style="text-align: center;">
                                                State Lien Summaries (Private)
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style7" style="text-align: left;">
                                                The State Lien Summaries in this section are provided to you by the law firm of <b>Seyfarth 
									Shaw LLP</b>, a full service law firm with several offices across the United States.  We 
									feel you will find these summaries extremely helpful and easy to understand.  We also 
									invite you to visit their website at <a href="http://www.seyfarth.com" target="_blank">www.seyfarth.com</a> to learn about the firm and see 
									what they might be able to do for you.
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 style6" style="text-align: left;">
                                                To access the summaries, just click on any of the States on the map below,or you can 
									also use the state links listed below the map.
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: center;">
                                                <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
                                                    codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,0,0"
                                                    width="530" height="410" id="usa_locator" align="center">
                                                    <param name="movie" value="usa_locator.swf">
                                                    <param name="FlashVars" value="xmlfile1=usa_locator_sum.xml">
                                                    <param name="quality" value="high">
                                                    <param name="wmode" value="direct">
                                                    <param name="bgcolor" value="#F5F5F5">
                                                    <embed src="usa_locator.swf?xmlfile1=usa_locator_sum.xml" quality="high" wmode="transparent" bgcolor="#F5F5F5" width="530" height="410" name="usa_locator" align=""
                                                        type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"></embed>
                                                </object>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align: center;">
                                                <table align="center" cellpadding="5" cellspacing="0" border="1">
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/ALPrivateSum.pdf" target="_blank" title="Alabama Lien Summary">Alabama Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MTPrivateSum.pdf" target="_blank" title="Montana Lien Summary">Montana Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/AKPrivateSum.pdf" target="_blank" title="Alaska Lien Summary">Alaska Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NEPrivateSum.pdf" target="_blank" title="Nebraska Lien Summary">Nebraska Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/AZPrivateSum.pdf" target="_blank" target="_blank" title="Arizona Lien Summary">Arizona Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NVPrivateSum.pdf" target="_blank" title="Nevada Lien Summary">Nevada Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/ARPrivateSum.pdf" target="_blank" title="Arkansas Lien Summary">Arkansas Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NHPrivateSum.pdf" target="_blank" title="New Jersey Lien Summary">New Hampshire Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/CAPrivateSum.pdf" target="_blank" title="California Lien Summary">California Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NJPrivateSum.pdf" target="_blank" title="New Jersey Lien Summary">New Jersey Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/COPrivateSum.pdf" target="_blank" title="Colorado Lien Summary">Colorado Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NMPrivateSum.pdf" target="_blank" title="New Mexico Lien Summary">New Mexico Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/CTPrivateSum.pdf" target="_blank" title="Connecticut Lien Summary">Connecticut Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NYPrivateSum.pdf" target="_blank" title="New York Lien Summary">New York Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/DEPrivateSum.pdf" target="_blank" title="Delaware Lien Summary">Delaware Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NCPrivateSum.pdf" target="_blank" title="North Carolina Lien Summary">North Carolina Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/DCPrivateSum.pdf" target="_blank" title="District of Columbia Lien Summary">District of Columbia Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/NDPrivateSum.pdf" target="_blank" title="North Dakota Lien Summary">North Dakota Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/FLPrivateSum.pdf" target="_blank" title="Florida Lien Summary">Florida Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/OHPrivateSum.pdf" target="_blank" title="Ohio Lien Summary">Ohio Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/GAPrivateSum.pdf" target="_blank" title="Georgia Lien Summary">Georgia Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/OKPrivateSum.pdf" target="_blank" title="Oklahoma Lien Summary">Oklahoma Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/HIPrivateSum.pdf" target="_blank" title="Hawaii Lien Summary">Hawaii Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/ORPrivateSum.pdf" target="_blank" title="Oregon Lien Summary">Oregon Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/IDPrivateSum.pdf" target="_blank" title="Idaho Lien Summary">Idaho Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/PAPrivateSum.pdf" target="_blank" title="Pennsylvania Lien Summary">Pennsylvania Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/ILPrivateSum.pdf" target="_blank" title="Illinois Lien Summary">Illinois Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/RIPrivateSum.pdf" target="_blank" title="Rhode Island Lien Summary">Rhode Island Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/INPrivateSum.pdf" target="_blank" title="Iowa Lien Summary">Indiana Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/SCPrivateSum.pdf" target="_blank" title="South Dakota Lien Summary">South Carolina Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/IAPrivateSum.pdf" target="_blank" title="Iowa Lien Summary">Iowa Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/SDPrivateSum.pdf" target="_blank" title="South Dakota Lien Summary">South Dakota Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/KSPrivateSum.pdf" target="_blank" title="Kansas Lien Summary">Kansas Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/TNPrivateSum.pdf" target="_blank" title="Tennessee Lien Summary">Tennessee Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/KYPrivateSum.pdf" target="_blank" title="Kentucky Lien Summary">Kentucky Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/TXPrivateSum.pdf" target="_blank" title="Texas Lien Summary">Texas Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/LAPrivateSum.pdf" target="_blank" title="Louisianna Lien Summary">Louisianna Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/UTPrivateSum.pdf" target="_blank" title="Utah Lien Summary">Utah Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MEPrivateSum.pdf" target="_blank" title="Maine Lien Summary">Maine Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/VTPrivateSum.pdf" target="_blank" title="Vermont Lien Summary">Vermont Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MDPrivateSum.pdf" target="_blank" title="Maryland Lien Summary">Maryland Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/VAPrivateSum.pdf" target="_blank" title="Virginia Lien Summary">Virginia Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MAPrivateSum.pdf" target="_blank" title="Massachusetts Lien Summary">Massachusetts Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/WAPrivateSum.pdf" target="_blank" title="Washington Lien Summary">Washington Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MIPrivateSum.pdf" target="_blank" title="Michigan Lien Summary">Michigan Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/WVPrivateSum.pdf" target="_blank" title="West Virginia Lien Summary">West Virginia Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MNPrivateSum.pdf" target="_blank" title="Minnesota Lien Summary">Minnesota Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/WIPrivateSum.pdf" target="_blank" title="Wisconsin Lien Summary">Wisconsin Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MSPrivateSum.pdf" target="_blank" title="Mississippi Lien Summary">Mississippi Lien Summary</a>
                                                        </td>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/WYPrivateSum.pdf" target="_blank" title="Wyoming Lien Summary">Wyoming Lien Summary</a>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="center" width="38%" class="style2"><a href="Images/ResourceImages/Summary/MOPrivateSum.pdf" target="_blank" title="Missouri Lien Summary">Missouri Lien Summary</a>
                                                        </td>

                                                    </tr>


                                                </table>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12 style6" style="text-align: left;background-color:#f1fbff;">
                                                Information and documents provided in ClientView™, to our clients free of charge, 
									are provided without any warranty, express or implied, as to their legal effect 
									and completeness. CRF Solutions accepts no liability in connection with reliance 
									thereon or any loss sustained by anyone using or relying on the information contained 
									herein. The information and documents offered by CRF Solutions are not a substitute 
									for the advice of an attorney. CRF Solutions strongly recommends you seek assistance 
									from an experienced construction attorney for any legal questions you might have.
                                            </div>
                                        </div>
                                      


                                    </div>
                              



