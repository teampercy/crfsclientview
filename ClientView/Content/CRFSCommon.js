﻿function MainMenuToggle(id)
{
    //alert('4');
  //  $('li').removeClass('active open');
    var liId='#'+id;
    $(liId+' > a').click(function () {
        if ($(liId).hasClass('active open')) {
         //   alert('3');
            $(liId).removeClass('active open');

        }
        else {
            //alert('4');
            $(liId).addClass('site-menu-item has-sub active open');
           // alert('1');
            //alert(id);
            $(this).css('background-color', '#142c4b');
        }
        return false;
    });
    $(liId).addClass('site-menu-item has-sub active open');
    $(liId).find('a:first').css('background-color', '#142c4b');
}
function SubMenuToggle(id) {
   // alert('f');
    var liId = '#' + id;
    $(liId + ' > a').click(function () {
        if ($(liId).hasClass('active')) {
             //  alert('3');
            $(liId).removeClass('active');

        }
        else {
            //alert('4s');
            $(liId).addClass('site-menu-item has-sub active');
            $(this).css('background-color', '#122947');
        }
        return true;
    });
    $(liId).addClass('site-menu-item has-sub active');
    $(liId).find('a:first').css('background-color', '#122947');

}
function SetHeaderBreadCrumb(MainMenuTitle,MainMenuName,MainSubMenuName)
{
    debugger;
    // alert('call ebet');

    //Old Code Start
    //$('#liMainMenuTitle').text(MainMenuTitle);
    //$('#liMainMenu').text(MainMenuName);
    //$('#liMainSubMenuName').text(MainSubMenuName);
    //Old Code End

    if (MainMenuTitle == "RentalView" || MainMenuTitle == "JobView"  )
    {
        $('#liMainMenuTitle').text('');
        $('#liMainMenu').text('');
        $('#liMainSubMenuName').text('');
        $('#olBreadCrumb').text('');
    }
    else
    {
        $('#liMainMenuTitle').text(MainMenuTitle);
        $('#liMainMenu').text(MainMenuName);
        $('#liMainSubMenuName').text(MainSubMenuName);
    }

  
}
function MakeDataTable(TableID, Columns, AllowSearching, DefaultColumnToBeSort) {
    if (AllowSearching === undefined || AllowSearching==null)
        AllowSearching = true;

    if (DefaultColumnToBeSort === undefined || DefaultColumnToBeSort==null) //2D Array
        DefaultColumnToBeSort = [[0, "asc"]];

    //StartLoading();

    var defaults = {
        "stateSave": true,
        "stateDuration": 60 * 10,
        "lengthMenu": [15, 25, 35, 50, 100],
        "searching": AllowSearching,
        bAutoWidth: false,
        responsive: true,
        "bSort": true,
        "aaSorting": DefaultColumnToBeSort,
        "aoColumns": Columns,
        language: {
            "sSearchPlaceholder": "Search..",
            "lengthMenu": "_MENU_",
            "search": "_INPUT_",
            "paginate": {
                "previous": '<i class="icon wb-chevron-left-mini"></i>',
                "next": '<i class="icon wb-chevron-right-mini"></i>'
            }
        }       
    };

    var options = $.extend(true, {}, defaults, $(this).data());

    var dtTable = $(TableID);
   
    $(dtTable).dataTable(options);
   
    return dtTable;

    //  EndLoading();
}

