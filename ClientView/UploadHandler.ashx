﻿<%@ WebHandler Language="VB" Class="UploadHandler" %>

Imports System
Imports System.Web
Imports System.IO
Public Class UploadHandler : Implements IHttpHandler, IRequiresSessionState
    
    Public Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        context.Response.ContentType = "text/plain"
        context.Response.Write("Hello World")
        If context.Request.Files.Count > 0 Then
            Dim files As HttpFileCollection = context.Request.Files
            For i As Integer = 0 To files.Count - 1
                Dim file As HttpPostedFile = files(i)
                Dim fname As String
                If HttpContext.Current.Request.Browser.Browser.ToUpper() = "IE" OrElse HttpContext.Current.Request.Browser.Browser.ToUpper() = "INTERNETEXPLORER" Then
                    Dim testfiles As String() = file.FileName.Split(New Char() {"\"c})
                    fname = testfiles(testfiles.Length - 1)
                Else
                    fname = file.FileName
                End If
                Dim fileData As Byte() = New Byte(file.InputStream.Length - 1) {}
                file.InputStream.Read(fileData, 0, fileData.Length)

                Dim objuploadFile As New UploadFile
                Dim uploadFiles As List(Of UploadFile) ' = New List(Of UploadFile)()
                uploadFiles = context.Session("UploadFiles")
                If uploadFiles Is Nothing Then
                    uploadFiles = New List(Of UploadFile)()
                End If
                objuploadFile.UploadedFileName = fname
                objuploadFile.UploadedFileDataBytes = fileData
                objuploadFile.UploadedFileExtension = System.IO.Path.GetExtension(file.FileName)

                uploadFiles.Add(objuploadFile)
                context.Session("UploadFiles") = uploadFiles

                'context.Session("UploadedFileName") = fname
                'context.Session("UploadedFileDataBytes") = fileData
                'context.Session("UploadedFileExtension") = System.IO.Path.GetExtension(file.FileName)

                ' fname = Path.Combine(context.Server.MapPath("~/uploads/"), fname)
                ' file.SaveAs(fname)
            Next
        End If
        context.Response.ContentType = "text/plain"
        context.Response.Write("File Uploaded Successfully!")
    End Sub
 
    Public ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class