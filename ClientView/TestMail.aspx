﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="TestMail.aspx.vb" Inherits="TestMail" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table width="60%" align="center">
                <tr>
                    <td>&nbsp;&nbsp;</td>
                    <td>
                        <b>Email Test using System.Net.Mail.... </b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:Label runat="server" ID="lblEmailResponse" ForeColor="red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td width="20%">
                        <asp:Label ID="lblRemoteHost" runat="server" Text="Email Host Server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRemoteHost" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Port Number"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtportno" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="SMTP USerName"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtusername" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="SMTP Password"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPassword" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                  <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Enabled SSl"></asp:Label>
                    </td>
                    <td>
                    <asp:CheckBox ID="chkEnable" runat ="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblFromEmail" runat="server" Text="From Email Address"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFromEmail" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblToEmail" runat="server" Text="To Email Address"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtToEmail" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Body"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtBody" runat="server" Text=""></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>
                        <asp:Button ID="btnSendEmail" runat="server" Text="Send Email"
                            OnClick="btnSendEmail_Click" />
                    </td>
                </tr>
                   <tr>
                    <td>Error Message:</td>
                    <td>
                      <asp:Label ID="lblErrorMessage" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
