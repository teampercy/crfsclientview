<%@ Application Language="VB" %>
<%@ Import Namespace="System.Web.Optimization" %>
<%@ Import Namespace="System.Web.Routing" %>
<%@ Import Namespace="System.Web.Http" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        ' CRF.CLIENTVIEW.BLL.Reports.Provider.Execute()
        'RouteTable.Routes.MapHttpRoute(name:="DefaultApi",
         '                              routeTemplate:="api/{controller}/{action}/{id}",
          '                             defaults:=New With {.id = System.Web.Http.RouteParameter.Optional})


    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
        ' CRF.CLIENTVIEW.BLL.Tasks.Provider.StopIt()

    End Sub


    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        'Dim err As Exception = Server.GetLastError()
        'Me.Session("error") = err.Message
        'Server.ClearError()
        'Try
        '    SiteControls.WebSite.Provider.LogEvent(-1, "apperror", "error", err.Message)
        'Catch ex As Exception
        '    Server.ClearError()
        'End Try

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
        Dim s As String = Server.MapPath("~/UserData/output/")
        CRF.CLIENTVIEW.BLL.Common.FileOps.DirectoryClean(s, DateAdd(DateInterval.Day, -1, Today))

    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
    End Sub

</script>