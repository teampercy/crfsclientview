﻿<%--<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Login.aspx.vb" Inherits="Login" %>--%>

<%@ Page Language="VB" Inherits="HDS.WEBSITE.UI.DefaultPage" MaintainScrollPositionOnPostback="true" AutoEventWireup="true" EnableEventValidation="false" %>

<%@ Register Assembly="HDS.WEBLIB" Namespace="HDS.WEBLIB.Controls" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    <meta name="generator" content="hds vbjuice" />
    <meta id="metaKeywords" name="Keywords" content="" />
    <meta id="metaDescr" name="Description" content="" />
    <meta id="metaCopyRight" name="copyright" content="" />
    <title>Login CRFS</title>
    <link rel="apple-touch-icon" href="assets/images/apple-touch-icon.png">
    <link rel="shortcut icon" href="assets/images/favicon.ico">
    <!-- Stylesheets -->
    <link rel="stylesheet" href="global/css/bootstrap.min.css">
    <link rel="stylesheet" href="global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="assets/css/site.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="assets/examples/css/pages/login-v2.css">
    <!-- Fonts -->
    <link rel="stylesheet" href="global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <!--[if lt IE 9]>
    <script src="global/vendor/html5shiv/html5shiv.min.js"></script>
    <![endif]-->
    <!--[if lt IE 10]>
    <script src="global/vendor/media-match/media.match.min.js"></script>
    <script src="global/vendor/respond/respond.min.js"></script>
    <![endif]-->
    <!-- Scripts -->
    <script src="global/vendor/modernizr/modernizr.js"></script>
    <script src="global/vendor/breakpoints/breakpoints.js"></script>
    <script>
        Breakpoints();
  </script>    
</head>
<script runat="server">
    Dim myitem As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal
    Dim myuser As New CRF.CLIENTVIEW.BLL.CRFDB.TABLES.Portal_Users
        
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Page.IsPostBack = False Then
            Try
                If IsNothing(Request.Cookies("MyCookie")) = False Then
                    Me.logincode.Text = Request.Cookies("MyCookie")("Data")
                End If
             
            Catch ex As Exception
                Me.logincode.Value = ""
            End Try
           
            Me.Page.SetFocus(Me.logincode)
            
            'If IsNothing(Page.Request.QueryString("v")) = False Then
            '    If HDS.WEBLIB.Providers.Membership.RegisterUser(Page.Request.QueryString("v")) = True Then
            '        Me.MultiView1.SetActiveView(Me.View2)
            '        Exit Sub
            '    End If
            'End If
          
            If Me.IsAuthenticated = True Then
                Me.SignOffUser(True)
            End If

        End If
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid = False Then Exit Sub
        '  myitem = Me.MyPage.get

        '***^^Created By Pooja On 10/21/2020 ^^***'
        If Me.logincode.Text.Length > 25 Then
            CustomValidator1.ErrorMessage = "User Name must be less than 25 characters, try again"
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        '***^^        ^^***'
        If Me.loginpassword.Text.Length < 1 Then
            CustomValidator1.ErrorMessage = "Password must be greater than 4 characters, try again"
            CustomValidator1.IsValid = False
            Exit Sub
            '***^^Created By Pooja On 10/21/2020 ^^***'
        ElseIf Me.loginpassword.Text.Length > 25 Then
            CustomValidator1.ErrorMessage = "Password must be less than 25 characters, try again"
            CustomValidator1.IsValid = False
            Exit Sub

        End If
        '***^^        ^^***'
        CustomValidator1.ErrorMessage = Data.MembershipProvider.ValidateUserByLoginCode(Me.logincode.Value, Me.loginpassword.Value, myuser)
       
        If CustomValidator1.ErrorMessage.Length > 1 Then
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        Dim MYINFO As New CRF.CLIENTVIEW.BLL.ClientUserInfo(myuser.ID)
        If MYINFO.Client.IsInactive = True Then
            CustomValidator1.ErrorMessage = "You Account Is Inactive, Please Contact Customer Support"
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        If MYINFO.UserInfo.isinactive = True Then
            CustomValidator1.ErrorMessage = "You Account Is Inactive, Please Contact Customer Support"
            CustomValidator1.IsValid = False
            Exit Sub
        End If
        
        Me.Session("UserInfo") = MYINFO
      '  MYINFO.Client.ClientId = 0
            Response.Cookies("MyCookie")("Data") = Me.logincode.Text
            Response.Cookies("MyCookie")("Time") = DateTime.Now.ToString("G")
            Response.Cookies("MyCookie").Expires = DateTime.Now.AddMonths(1)
            Me.SignOnUser(myuser.ID, Me.chkRemember.Checked)
            'Me.MyPage.SignOnUser(myuser.ID, Me.chkRemember.Checked)
            Response.Redirect("~/MainDefault.aspx", False)
            'Me.MyPage.RedirectToHomePage() 'Commented by jaywanti
   
    End Sub
 
</script>
<body class="page-login-v2 layout-full page-dark">
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <!-- Page -->
    <%--<div class="page animsition" data-animsition-in="fade-in" data-animsition-out="fade-out" style="animation-duration: 800ms; opacity: 1">--%>
    <div class="page animsition" data-animsition-in="fade-in" data-animsition-out="fade-out" style="animation-duration: 800ms; opacity: 1">
        <div class="page-content">

            <%--<div class="mydiv">
                <div class="page-brand-info">
                    <div class="brand">
                        <img class="brand-img" src="assets/images/logo@2x.png" alt="...">
                        <h2 class="brand-text font-size-40">CRFS</h2>
                    </div>
                </div>
            </div>--%>
            
            <div class="page-login-main">
                <div class="brand visible-xs">
                    <img class="brand-img" src="assets/images/logo-blue@2x.png" alt="...">
                    <h3 class="brand-text font-size-40">CRFS</h3>
                </div>
                <h3 class="font-size-24">Sign In</h3>
                <%--  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>--%>
                <form runat="server" id="myform" autocomplete="on" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="control-label" for="logincode">User Name</label>
                        <%--     <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email">--%>
                        <span style="display: flex;">
                            <cc1:DataEntryBox ID="logincode" runat="server" CssClass="form-control" IsRequired="true" AutoCompleteType="firstname" TabIndex="1"></cc1:DataEntryBox>
                        </span>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="loginpassword">Password</label>
                        <%-- <input type="password" class="form-control" id="inputPassword" name="password"
            placeholder="Password">--%>
                        <span style="display: flex;">
                            <cc1:DataEntryBox ID="loginpassword" runat="server" CssClass="form-control" TextMode="Password" IsRequired="True" TabIndex="2"></cc1:DataEntryBox>
                        </span>
                    </div>
                    <div class="form-group clearfix">
                        <div class="checkbox-custom checkbox-inline checkbox-primary pull-left">
                            <%-- <input type="checkbox" id="remember" name="checkbox">--%>
                            <asp:CheckBox ID="chkRemember" runat="server" Text=" Remember Me" Checked="True" TabIndex="3" />
                            <%--  <label for="inputCheckbox">Remember me</label>--%>
                        </div>
                        <%-- <a class="pull-right" href="forgot-password.html">Forgot password?</a>--%>
                    </div>
                    <%--  <button type="submit" class="btn btn-primary btn-block">Sign in</button>--%>
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-block" Text="Sign in" OnClick="btnSave_Click" TabIndex="4" />
                    <div class="form-group" style="margin-top: 40px;">
                        <asp:ValidationSummary ID="ValidationSummary1" runat="server" Width="300px" HeaderText="Please Correct the Following Field(s): " />
                    </div>

                    <cc1:CustomValidator ID="CustomValidator1" Display="None" runat="server"> </cc1:CustomValidator>
                </form>
                <%--  <p>No account? <a href="register-v2.html">Sign Up</a></p>--%>
                <footer class="page-copyright" style="width: 100%;">
                    <%--<p>Site Best Viewed With: </p>
                    <p>(Click Link To Download)</p>
                    <div class="social">
                        <a href="https://www.google.com/intl/en/chrome/browser/?hl=en&brand=CHMA&utm_campaign=en&utm_source=en-ha-na-us-bk&utm_medium=ha
                "
                            target="_blank">
                            <img src="Images/ResourceImages/Chrome.jpg" alt="Chrome Logo" class="float-center" style="width: 80px; height: 23px" /></a>
                        &nbsp;&nbsp;<a href="http://mozilla-firefox.todownload.com/?lp=adwords&tg=us&kw=Firefox%20download&mt=e&ad=33229594518&pl=&ds=s&gclid=CMH25p3C4roCFU7ZQgodJTAA_w
                "
                            target="_blank"><img src="Images/ResourceImages/Firefox.jpg" alt="Firefox Logo" class="float-center" style="width: 70px; height: 25px" /></a>
                        &nbsp;&nbsp;<a href="http://windows.microsoft.com/en-us/internet-explorer/ie-9-worldwide-languages
                "
                            target="_blank"><img src="Images/ResourceImages/IE11.png" alt="IE9 Logo" class="float-center" style="width: 105px; height: 25px" /></a>
                    </div>--%>
                    <%--<div class="social">
            <a class="btn btn-icon btn-round social-twitter" href="javascript:void(0)">
              <i class="icon bd-twitter" aria-hidden="true"></i>
            </a>
            <a class="btn btn-icon btn-round social-facebook" href="javascript:void(0)">
              <i class="icon bd-facebook" aria-hidden="true"></i>
            </a>
            <a class="btn btn-icon btn-round social-google-plus" href="javascript:void(0)">
              <i class="icon bd-google-plus" aria-hidden="true"></i>
            </a>
          </div>--%>
                </footer>
            </div>
        </div>
    </div>
    <!-- End Page -->
    <!-- Core  -->
    <script src="global/vendor/jquery/jquery.js"></script>
    <script src="global/vendor/bootstrap/bootstrap.js"></script>
    <script src="global/vendor/animsition/animsition.js"></script>
    <script src="global/vendor/asscroll/jquery-asScroll.js"></script>
    <script src="global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="global/vendor/asscrollable/jquery.asScrollable.all.js"></script>
    <script src="global/vendor/ashoverscroll/jquery-asHoverScroll.js"></script>
    <!-- Plugins -->
    <script src="global/vendor/switchery/switchery.min.js"></script>
    <script src="global/vendor/intro-js/intro.js"></script>
    <script src="global/vendor/screenfull/screenfull.js"></script>
    <script src="global/vendor/slidepanel/jquery-slidePanel.js"></script>
    <script src="global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    <!-- Scripts -->
    <script src="global/js/core.js"></script>
    <script src="assets/js/site.js"></script>
    <script src="assets/js/sections/menu.js"></script>
    <script src="assets/js/sections/menubar.js"></script>
    <script src="assets/js/sections/gridmenu.js"></script>
    <script src="assets/js/sections/sidebar.js"></script>
    <script src="global/js/configs/config-colors.js"></script>
    <script src="assets/js/configs/config-tour.js"></script>
    <script src="global/js/components/asscrollable.js"></script>
    <script src="global/js/components/animsition.js"></script>
    <script src="global/js/components/slidepanel.js"></script>
    <script src="global/js/components/switchery.js"></script>
    <script src="global/js/components/jquery-placeholder.js"></script>
    <script>
        (function (document, window, $) {
            'use strict';
            var Site = window.Site;
            $(document).ready(function () {
                SetImageWidth();
                Site.run();
            });
        })(document, window, jQuery);
        $(window).resize(function () {
            //SetImageWidth();
        });
        function SetImageWidth() {
            var lgwidth = $('.page-login-main').outerWidth();
            var wndWidth = $(window).width();
            $('.mydiv').width(wndWidth - lgwidth);
        }
  </script>
</body>
</html>
